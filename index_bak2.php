<?php

/*
@session_save_path("/var/www/vhosts/sam.chaiyoreadyweb.com/tmp/");
@ini_set('session.gc_probability', 1);
@session_start();*/
//ini_set('session.save_path','/var/www/vhosts/kpt-group.com/httpdocs/tmp/');
// change the following paths if necessary
define('YII_ENABLE_ERROR_HANDLER', true);
define('YII_ENABLE_EXCEPTION_HANDLER', true);

// Turn off all error reporting
 error_reporting(1);

// Report all errors except E_NOTICE
// This is the default value set in php.ini
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
//defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
$app = Yii::createWebApplication($config);

if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
@date_default_timezone_set(@date_default_timezone_get());
//Yii::app()->session->setSavePath("/var/www/vhosts/kpt-group.com/httpdocs/tmp/");
// we need to set this to UTC, regardless of default timezone 
// which is only for display. UTC is what timestamps etc. are using.
Yii::app()->setTimeZone("Asia/Bangkok");

$app->run();
