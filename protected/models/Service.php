<?php

class Service extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	 
	 public $key_word;
	 public $display_perpage;
	 public $current_page;
	 public $row_count;
	 public $start_date;
	 public $end_date;
	 public $verifyCode;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_service';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			/*array('username', 'required'),*/
			array('company_name,full_name,product_code,email,phone_no,subject','length','max'=>250),
			array('start_date,end_date','safe','on'=>'search'),
			array('service_type,status','length','max'=>40),
			array('detail','length','max'=>6000),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(),'on' => 'new'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array(
			'group' => array(self::HAS_ONE, 'UserGroup', array('id' => 'group_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'color' => 'Color Name','command'=>'Command'
		);
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	 
	

	

	/**
	 * Normalizes the user-entered tags.
	 */


	

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		

			if($this->isNewRecord)
			{
				$this->status=0;
				$this->create_date=date("Y-m-d H:i:s");
				$this->create_ip = $_SERVER['REMOTE_ADDR'];
			}	
			
			
			
return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	 
	 
	 
	 public function backend_search()
	{
		$criteria=new CDbCriteria;
		
		$condi = "";	

		if($this->service_type!="")
			$condi .= " and service_type='" . $this->service_type . "'";
			
		if($this->full_name!="")
			$condi .= " and full_name like '%" . $this->full_name . "%'";
			
		if($this->company_name!="")
			$condi .= " and company_name like '%" . $this->company_name . "%'";
			
		if($this->status!="")
			$condi .= " and status='" . $this->status . "'";
			
		if(isset($this->start_date) && $this->start_date!="")
		{
			$condi .= " and create_date>='" . $this->start_date . "'";
		}
		if(isset($this->end_date) && $this->end_date!="")
		{
			$condi .= " and create_date<='" . $this->end_date . "'";
		}
			
		$criteria->condition = " 1=1 $condi ";
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('Service', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'ID desc,full_name,company_name asc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:30),
			'currentPage'=>$this->current_page,),
		));
	}
	 
	public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('Service', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'full_name,company_name asc',
			),
		));
	}
}