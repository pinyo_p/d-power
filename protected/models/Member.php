<?php

class Member extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public $rememberMe;
    public $last_login;
    public $cpassword;
    public $verifyCode;
    public $key_word;
    public $start_date;
    public $end_date;
    public $display_perpage;
    public $current_page;
    public $row_count;
    public $username;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_member';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email,first_name,password,tax_no', 'required', 'message' => ' {attribute} ไม่สามารถเป็นค่าว่างได้', 'on' => 'register'),
            array('prefix,birth_day,province,zipcode,phone_no,country_id,member_type,prefix_spec', 'length', 'max' => 40),
            array('first_name,last_name,email,password,company_name', 'length', 'max' => 250),
            array('address', 'length', 'max' => 4000),
            array('email', 'checkDupUser',),
            array('password', 'checkConfirmPass',),
            array('line_id', 'length', 'max' => 100),
            array('tax_no', 'length', 'max' => 50),
            array('company_type', 'length', 'max' => 3),
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    public function checkConfirmPass($attribute, $params) {
        if ($this->password != $this->cpassword) {
            $this->addError('cpassword', 'รหัสผ่านไม่ตรงกัน');
        }
    }

    public function checkDupUser($attribute, $params) {
        $usg = Member::model()->find("email='" . $this->email . "' and id<>'" . $this->id . "'");
        if (count($usg) > 0) {
            $this->addError('email', 'Email นี้ได้ถูกใช้งานไปแล้ว');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        /*
          return array(
          'author' => array(self::BELONGS_TO, 'User', 'author_id'),
          'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
          'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
          ); */
        return array(
            'p' => array(self::HAS_ONE, 'Provinces', array('province_id' => 'province')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'email' => 'E-mail',
            'first_name' => 'ชื่อ', 'last_name' => 'นามสกุล', 'address' => 'ที่อยู่', 'province' => 'จังหวัด', 'zipcode' => 'รหัสไปรษณีย์', 'prefix' => 'คำนำหน้าชื่อ',
            'line_id' => 'Line ID', 'tax_no' => 'เลขประจำตัวผู้เสียภาษี',
        );
        /*
          array('email,first_name,,,,,,', 'required','message'=>' {attribute} ไม่สามารถเป็นค่าว่างได้', 'on'=>'register'),
         */
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    /**
     * Normalizes the user-entered tags.
     */

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {


        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_ip = $_SERVER['REMOTE_ADDR'];
            $this->status = 0;
        }
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    public function isLogin() {
        $session = new CHttpSession;
        $session->open();
        
        $result = false;

        $lg = $session['user'];
        if ($lg != null) {
            $result = true;        
        }
        return $result;
    }
    
    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function login() {
        if ($this->email == "") {
            $this->addError('email', 'กรุณากรอกชื่อผู้ใช้');
            return false;
        } else if ($this->password == "") {
            $this->addError('password', 'กรุณากรอกรหัสผ่าน');
            return false;
        } else {
            $usg = Member::model()->find("email='" . $this->email . "' and password='" . $this->password . "'");
            $lg = $usg;
            if (count($usg) > 0) {
                Member::model()->updateByPk($usg->id, array('last_login' => date("Y-m-d H:i:s")));
                $ig = new stdClass();
                $ig->id = $lg->id;
                $ig->prefix = $lg->prefix;
                $ig->prefix_spec = $lg->prefix_spec;
                $ig->first_name = $lg->first_name;
                $ig->last_name = $lg->last_name;
                $ig->birth_day = $lg->birth_day;
                $ig->address = $lg->address;
                $ig->province = $lg->province;
                $ig->zipcode = $lg->zipcode;
                $ig->phone_no = $lg->phone_no;
                $ig->country_id = $lg->country_id;
                $ig->email = $lg->email;
                $ig->password = $lg->password;
                $ig->member_type = $lg->member_type;
                $ig->company_name = $lg->company_name;
                $ig->create_date = $lg->create_date;
                $ig->create_ip = $lg->create_ip;
                $ig->last_login = $lg->last_login;
                $ig->status = $lg->status;
                $ig->username = $lg->email;
                $arr['lg'] = $lg;
                return $ig;
            } else {
                $this->addError('email', 'Login Fail!!!');
                return false;
            }
        }
    }

    public function searchById($id) {
        $criteria = new CDbCriteria;

        $criteria->condition = " id = $id";
        $criteria->with = array('p');
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('Member', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'create_date desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

    public function search() {
        $criteria = new CDbCriteria;

        $condi = "";
        if ($this->first_name != "")
            $condi = " and first_name like '%" . $this->first_name . "%'";
        if ($this->last_name != "")
            $condi = " and last_name like '%" . $this->last_name . "%'";
        if ($this->email != "")
            $condi = " and email like '%" . $this->email . "%'";
        if ($this->phone_no != "")
            $condi = " and phone_no like '%" . $this->phone_no . "%'";

        if (isset($this->start_date) && $this->start_date != "") {
            $condi .= " and create_date>='" . $this->start_date . "'";
        }
        if (isset($this->end_date) && $this->end_date != "") {
            $condi .= " and create_date<='" . $this->end_date . "'";
        }

        $criteria->condition = " 1=1 $condi ";
        $criteria->with = array('p');
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('Member', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'create_date desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

}
