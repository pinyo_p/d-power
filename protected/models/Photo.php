<?php

class Photo extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */


	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_photo_image';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('image_src,image_desc,image_name', 'length','max'=>250),
			array('image_desc,', 'length','max'=>4000),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
		);
	}

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
	//	echo "<br />" .  $this->npa_id . "<br />" ;
			return true; 
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
		
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function searchByAlbum()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('`album_id`',$this->album_id,false);
		return new CActiveDataProvider('Photo', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id desc',
			),'pagination' => array(
            'pageSize' => 100000,
			'currentPage'=>0,)
		));
	}
	public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('PhotoAlbum', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id desc',
			),'pagination' => array(
            'pageSize' => 100000,
			'currentPage'=>0,)
		));
	}
}