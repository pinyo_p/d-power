<?php

/**
 * This is the model class for table "tb_product".
 *
 * The followings are the available columns in table 'tb_product':
 * @property integer $id
 * @property integer $brand_id
 * @property integer $group_id
 * @property string $product_name_1
 * @property string $product_name_2
 * @property string $product_name_3
 * @property string $product_name_4
 * @property string $product_name_5
 * @property string $product_desc_1
 * @property string $product_desc_2
 * @property string $product_desc_3
 * @property string $product_desc_4
 * @property string $product_desc_5
 * @property integer $price1
 * @property integer $amount1
 * @property integer $price2
 * @property integer $amount2
 * @property integer $price3
 * @property integer $amount3
 * @property integer $price4
 * @property integer $amount4
 * @property integer $price_discount
 * @property string $product_code
 * @property string $product_code2
 * @property string $pic1
 * @property string $pic2
 * @property string $pic3
 * @property string $pic4
 * @property string $pic5
 * @property string $pic6
 * @property string $pic7
 * @property string $pic8
 * @property string $pic9
 * @property string $pic10
 * @property integer $status
 * @property integer $stock
 * @property integer $is_highlight
 * @property integer $sort_order
 * @property integer $point
 * @property integer $vote_count
 * @property string $create_date
 * @property string $create_by
 * @property string $create_ip
 * @property string $update_date
 * @property string $update_by
 * @property string $update_ip
 * @property integer $serie_id
 * @property string $attach
 * @property string $sale_name
 * @property integer $is_new
 * @property integer $total_shipping
 */
class Product_new extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product_new the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brand_id, group_id, price1, amount1, price2, amount2, price3, amount3, price4, amount4, price_discount, status, stock, is_highlight, sort_order, point, vote_count, serie_id, is_new, total_shipping', 'numerical', 'integerOnly'=>true),
			array('product_name_1, product_name_2, product_name_3, product_name_4, product_name_5, product_code, product_code2, pic1, pic2, pic3, pic4, pic5, pic6, pic7, pic8, pic9, pic10', 'length', 'max'=>250),
			array('create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max'=>40),
			array('attach, sale_name', 'length', 'max'=>500),
			array('product_desc_1, product_desc_2, product_desc_3, product_desc_4, product_desc_5', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, brand_id, group_id, product_name_1, product_name_2, product_name_3, product_name_4, product_name_5, product_desc_1, product_desc_2, product_desc_3, product_desc_4, product_desc_5, price1, amount1, price2, amount2, price3, amount3, price4, amount4, price_discount, product_code, product_code2, pic1, pic2, pic3, pic4, pic5, pic6, pic7, pic8, pic9, pic10, status, stock, is_highlight, sort_order, point, vote_count, create_date, create_by, create_ip, update_date, update_by, update_ip, serie_id, attach, sale_name, is_new, total_shipping', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'brand_id' => 'Brand',
			'group_id' => 'Group',
			'product_name_1' => 'Product Name 1',
			'product_name_2' => 'Product Name 2',
			'product_name_3' => 'Product Name 3',
			'product_name_4' => 'Product Name 4',
			'product_name_5' => 'Product Name 5',
			'product_desc_1' => 'Product Desc 1',
			'product_desc_2' => 'Product Desc 2',
			'product_desc_3' => 'Product Desc 3',
			'product_desc_4' => 'Product Desc 4',
			'product_desc_5' => 'Product Desc 5',
			'price1' => 'Price1',
			'amount1' => 'Amount1',
			'price2' => 'Price2',
			'amount2' => 'Amount2',
			'price3' => 'Price3',
			'amount3' => 'Amount3',
			'price4' => 'Price4',
			'amount4' => 'Amount4',
			'price_discount' => 'Price Discount',
			'product_code' => 'Product Code',
			'product_code2' => 'Product Code2',
			'pic1' => 'Pic1',
			'pic2' => 'Pic2',
			'pic3' => 'Pic3',
			'pic4' => 'Pic4',
			'pic5' => 'Pic5',
			'pic6' => 'Pic6',
			'pic7' => 'Pic7',
			'pic8' => 'Pic8',
			'pic9' => 'Pic9',
			'pic10' => 'Pic10',
			'status' => 'Status',
			'stock' => 'Stock',
			'is_highlight' => 'Is Highlight',
			'sort_order' => 'Sort Order',
			'point' => 'Point',
			'vote_count' => 'Vote Count',
			'create_date' => 'Create Date',
			'create_by' => 'Create By',
			'create_ip' => 'Create Ip',
			'update_date' => 'Update Date',
			'update_by' => 'Update By',
			'update_ip' => 'Update Ip',
			'serie_id' => 'Serie',
			'attach' => 'Attach',
			'sale_name' => 'Sale Name',
			'is_new' => 'Is New',
			'total_shipping' => 'Total Shipping',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('brand_id',$this->brand_id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('product_name_1',$this->product_name_1,true);
		$criteria->compare('product_name_2',$this->product_name_2,true);
		$criteria->compare('product_name_3',$this->product_name_3,true);
		$criteria->compare('product_name_4',$this->product_name_4,true);
		$criteria->compare('product_name_5',$this->product_name_5,true);
		$criteria->compare('product_desc_1',$this->product_desc_1,true);
		$criteria->compare('product_desc_2',$this->product_desc_2,true);
		$criteria->compare('product_desc_3',$this->product_desc_3,true);
		$criteria->compare('product_desc_4',$this->product_desc_4,true);
		$criteria->compare('product_desc_5',$this->product_desc_5,true);
		$criteria->compare('price1',$this->price1);
		$criteria->compare('amount1',$this->amount1);
		$criteria->compare('price2',$this->price2);
		$criteria->compare('amount2',$this->amount2);
		$criteria->compare('price3',$this->price3);
		$criteria->compare('amount3',$this->amount3);
		$criteria->compare('price4',$this->price4);
		$criteria->compare('amount4',$this->amount4);
		$criteria->compare('price_discount',$this->price_discount);
		$criteria->compare('product_code',$this->product_code,true);
		$criteria->compare('product_code2',$this->product_code2,true);
		$criteria->compare('pic1',$this->pic1,true);
		$criteria->compare('pic2',$this->pic2,true);
		$criteria->compare('pic3',$this->pic3,true);
		$criteria->compare('pic4',$this->pic4,true);
		$criteria->compare('pic5',$this->pic5,true);
		$criteria->compare('pic6',$this->pic6,true);
		$criteria->compare('pic7',$this->pic7,true);
		$criteria->compare('pic8',$this->pic8,true);
		$criteria->compare('pic9',$this->pic9,true);
		$criteria->compare('pic10',$this->pic10,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('stock',$this->stock);
		$criteria->compare('is_highlight',$this->is_highlight);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('point',$this->point);
		$criteria->compare('vote_count',$this->vote_count);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('create_ip',$this->create_ip,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('update_ip',$this->update_ip,true);
		$criteria->compare('serie_id',$this->serie_id);
		$criteria->compare('attach',$this->attach,true);
		$criteria->compare('sale_name',$this->sale_name,true);
		$criteria->compare('is_new',$this->is_new);
		$criteria->compare('total_shipping',$this->total_shipping);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}