<?php

class WantMatch extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */


	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_want_match_up';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		
		return array(
			'user' => array(self::HAS_ONE, 'Want', array('id' => 'want_id')),
			'property' => array(self::HAS_ONE, 'Property', array('id' => 'npa_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
		);
	}

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
	//	echo "<br />" .  $this->npa_id . "<br />" ;
			$this->create_date= date("Y-m-d H:i:s");
			return true; 
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
		
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	 
	 public function searchByUserId($id=null)
	{
		$criteria=new CDbCriteria;
		$criteria->compare("want_id",$this->want_id);
		return new CActiveDataProvider('WantMatch', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id desc',
			),
		));
	}
	
	 public function searchByNpaId($id=null)
	{
		$criteria=new CDbCriteria;
		$criteria->compare("npa_id",$id);
		return new CActiveDataProvider('Attach', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'file_name asc',
			),
		));
	}
	public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('Attach', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'file_name asc',
			),
		));
	}
}