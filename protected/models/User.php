<?php

class User extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	 
	 public $key_word;
	 public $display_perpage;
	 public $current_page;
	 public $row_count;
	 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username', 'required'),
			array('username,password,first_name,last_name','length','max'=>250),
			array('group_id','length','max'=>40),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array(
			'group' => array(self::HAS_ONE, 'UserGroup', array('id' => 'group_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'color' => 'Color Name','command'=>'Command'
		);
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	 
	

	

	/**
	 * Normalizes the user-entered tags.
	 */


	

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		

			if($this->isNewRecord)
			{
				$this->status=0;
			}
		/*		$this->create_date=date("Y-m-d H:i:s");
		
				$this->create_by=Yii::app()->user->id;
				$this->create_ip = $_SERVER['REMOTE_ADDR'];
				$this->update_date=date("Y-m-d H:i:s");
				$this->update_by=Yii::app()->user->id;
				$this->update_ip = $_SERVER['REMOTE_ADDR'];
				$this->status=0;
			}
			else
			{
				$this->update_date=date("Y-m-d H:i:s");
				$this->update_by=Yii::app()->user->id;
				$this->update_ip = $_SERVER['REMOTE_ADDR'];
			}
			*/
return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	 
	 public function Login()
	 {
		 $criteria=new CDbCriteria;
		$criteria->compare('username',$this->username,false);
		$criteria->compare('password',$this->password,false);
		$criteria->compare('status','0',false);
		
		return new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'username desc',
			),
		));
	 }
	 
	 public function backend_search()
	{
		$criteria=new CDbCriteria;
		
		$condi = "";	
		if(trim($this->key_word)!="")
			$condi .= " and (username like '%" . $this->key_word . "%' or last_name like '%" . $this->key_word . "%' or first_name like '%" . $this->key_word . "%')" ;
		if($this->group_id!="")
			$condi .= " and group_id='" . $this->group_id . "'";
		if($this->status!="")
			$condi .= " and status='" . $this->status . "'";
		$criteria->condition = " 1=1 $condi ";
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'first_name,last_name desc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:30),
			'currentPage'=>$this->current_page,),
		));
	}
	 
	public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'first_name,last_name desc',
			),
		));
	}
}