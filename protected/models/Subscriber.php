<?php

/**
 * This is the model class for table "tb_subscriber".
 *
 * The followings are the available columns in table 'tb_subscriber':
 * @property integer $id
 * @property string $email
 * @property string $create_date
 * @property string $create_ip
 * @property integer $status
 */
class Subscriber extends CActiveRecord {

    public $display_perpage;
    public $current_page;
    public $row_count;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Subscriber the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_subscriber';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status', 'numerical', 'integerOnly' => true),
            array('email', 'length', 'max' => 500),
            array('create_date, create_ip', 'length', 'max' => 50),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, email, create_date, create_ip, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'email' => 'Email',
            'create_date' => 'Create Date',
            'create_ip' => 'Create Ip',
            'status' => 'Status',
        );
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_ip = Yii::app()->request->getUserHostAddress();           
            $this->status = 1;
        }
        return true;
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('create_ip', $this->create_ip, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function searchAll() {
        $criteria = new CDbCriteria;
        $criteria->compare('status', '1');
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 999999,
                'currentPage' => 0),
        ));
    }
    
    public function listForManage() {
        $criteria = new CDbCriteria;

        $this->row_count = $this->count($criteria);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

}
