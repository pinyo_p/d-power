<?php

class Premission extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    public $menuid;
    public $flag;
    public $menucode;
    public $userid;
    public $groupname;
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_premission';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('menu_id,user_id,assign_by,type', 'length', 'max' => 40),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'menu' => array(self::BELONGS_TO, 'Menu', 'menu_id'),
            'usergroup' => array(self::BELONGS_TO, 'UserGroup', 'user_id'),
        );
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'title' => 'Title',
        );
    }

    public function getMenu($part) {
        $arr_menu = array();
        return $arr_menu;
    }

    public function getlist() {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider('AboutSam', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order asc',
            ),
        ));
    }

    /**
     * Adds a new comment to this post.
     * This method will set status and post_id of the comment accordingly.
     * @param Comment the comment to be added
     * @return boolean whether the comment is saved successfully
     */
    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    /*
      protected function afterFind()
      {
      parent::afterFind();
      $this->_oldTags=$this->tags;
      } */

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function search() {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider('Premission', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order asc',
            ),
        ));
    }
    

}
