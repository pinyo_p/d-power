<?php

class MemberOrder2 extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public $member_id;
    public $display_perpage;
    public $current_page;
    public $row_count;
    public $product_name;
    public $start_date;
    public $end_date;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_order';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.

            return array(
                array('fullname,company_name', 'length', 'max' => 500),
                array('province,zipcode,phone_no,member_id', 'length', 'max' => 40),
                array('address,remark', 'length', 'max' => 4000),
                array('package_no', 'length', 'max' => 4000),
                array('require_taxslipt', 'length', 'max' => 40),
            );
 
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        /*
          return array(
          'author' => array(self::BELONGS_TO, 'User', 'author_id'),
          'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
          'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
          ); */
        return array(
            'Order' => array(self::HAS_ONE, 'Order', array('order_id' => 'id')),
            'Provinces' => array(self::HAS_ONE, 'Provinces', array('province_id' => 'province')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'color' => 'Color Name', 'command' => 'Command'
        );
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    /**
     * Normalizes the user-entered tags.
     */

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {
        $this->create_date = date("Y-m-d H:i:s");
        $this->create_ip = Yii::app()->request->getUserHostAddress();
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function backend_order_search() {
        $criteria = new CDbCriteria;

        $condi = "";

        if (trim($this->fullname) != "")
            $condi .= " and (fullname like '%" . $this->fullname . "%')";
        if (trim($this->company_name) != "")
            $condi .= " and (company_name like '%" . $this->company_name . "%')";
        if (trim($this->address) != "")
            $condi .= " and (address like '%" . $this->address . "%')";
        if (trim($this->province) != "")
            $condi .= " and province = '" . $this->province . "'";
        if (trim($this->start_date) != "")
            $condi .= " and (date(t.create_date) >= '" . $this->start_date . "')";        
        if (trim($this->end_date) != "")
            $condi .= " and (date(t.create_dateK) <= '" . $this->end_date . "')";
        if (trim($this->status) != "")
            $condi .= " and t.status = '" . $this->status . "'";

        $criteria->condition = " t.status<4 $condi ";
        $criteria->with = array('Order', 'Provinces');
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('MemberOrder', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc', //'t.status asc,t.id asc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

    public function backend_shipping_search() {
        $criteria = new CDbCriteria;

        $condi = "";

        if (trim($this->fullname) != "")
            $condi .= " and (fullname like '%" . $this->fullname . "%')";
        if (trim($this->company_name) != "")
            $condi .= " and (company_name like '%" . $this->company_name . "%')";
        if (trim($this->address) != "")
            $condi .= " and (address like '%" . $this->address . "%')";
        if (trim($this->province) != "")
            $condi .= " and province = '" . $this->province . "'";
        if (trim($this->start_date) != "")
            $condi .= " and (date(t.create_date) >= '" . $this->start_date . "')";
        if (trim($this->end_date) != "")
            $condi .= " and (date(t.create_date) <= '" . $this->end_date . "')";
        if (trim($this->status) != "")
            $condi .= " and t.status = '" . $this->status . "'";

        $criteria->condition = " t.status>=4 $condi ";
        $criteria->with = array('Order', 'Provinces');
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('MemberOrder', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',//'t.status asc,t.id asc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

    public function get_history() {
        $criteria = new CDbCriteria;
        $criteria->with = array('Order');
        $criteria->compare('t.member_id', $this->member_id);
        return new CActiveDataProvider('MemberOrder', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ),
            'pagination' => array(
                'pageSize' => 99999,
                'currentPage' => 0),
        ));
    }

    public function get_cart() {
        $criteria = new CDbCriteria;
        $criteria->with = array('Product');
        $criteria->compare('session_id', $this->session_id);
        return new CActiveDataProvider('Order', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ),
        ));
    }

    public function search() {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider('Order', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc',
            ),
        ));
    }
    
    public function search_by_id(){
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        return new CActiveDataProvider('MemberOrder', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ),
        ));
    }

}
