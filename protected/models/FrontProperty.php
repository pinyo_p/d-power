<?php

class FrontProperty extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */

	public $id;
	public $product_code;
	public $product_type;
	public $is_highlight;
	public $document_amount;
	public $land_license;
	public $rawang;
	public $area_rai;
	public $area_ngan;
	public $area_wa;
	public $area_metre;
	public $home_no;
	public $moo;
	public $project;
	public $soi;
	public $road;
	public $tambol;
	public $district;
	public $province;
	public $zipcode;
	public $latitude;
	public $longitude;
	public $enter_th;
	public $product_desc_th;
	public $product_desc2_th;
	public $street_side;
	public $deep;
	public $town_plan;
	public $product_limit_th;
	public $price;
	public $remark_th;
	public $other_th;
	public $more_detail_th;
	public $enter_en;
	public $product_desc_en;
	public $product_desc2_en;
	public $product_limit_en;
	public $remark_en;
	public $other_en;
	public $more_detail_en;
	public $approve_status;
	public $is_show;
	public $sale_type;
	public $publish_date;
	public $end_date;
	public $bid_date;
	public $status;
	public $main_image;
	public $map;
	public $land_map;
	public $building_map;
	public $create_date;
	public $create_by;
	public $create_ip;
	public $last_update;
	public $update_by;
	public $update_ip;
	public $approve_date;
	public $approve_by;
	public $approve_ip;
	public $delete_date;
	public $delete_by;
	public $delete_ip;
	public $is_deleted;
	public $continent;
	public $start_price;
	public $end_price;
	public $area_start;
	public $area_end;
	public $key_address;
	public $key_word;
	public $start_date;
	public $sort_by;
	public $sort_dir;
	public $row_count;
	public $display_perpage;
	public $current_page;
	public $bid_place = 'ห้องประชุมชั้น 27,30   บริษัท บริหารสินทรัพย์สุขุมวิท จำกัด (สำนักงานใหญ่) 123 อาคารซันทาวเวอร์ส เอ ถนนวิภาวดีรังสิต  แขวงจอมพล เขตจตุจักร  กรุงเทพฯ';
	public $npa_image;
	public $license;
	public $vdo;
	public $attach;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_product2';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_code', 'safe', 'on'=>'search'),
			array('product_code', 'required','message'=>' {attribute} ไม่สามารถเป็นค่าว่างได้'),
			array('product_type,is_highlight,continent,document_amount,is_show,status,create_by,update_by,approve_by,delete_by,is_deleted,highlight_order,sort_by,sort_dir,display_perpage','length','max'=>10),
			array('product_code,zipcode','length','max'=>50),
			array('street_side,deep','length','max'=>150),
			array('rawang,soi,latitude,longitude','length','max'=>250),
			array('bid_place,enter_th,product_desc2_th,product_limit_th,remark_th,other_th','length','max'=>150000),
			array('enter_en,product_desc2_en,product_limit_en,remark_en,other_en','length','max'=>150000),
			array('product_desc_th,more_detail_th,land_license,','length','max'=>400000),
			array('product_desc_en,more_detail_en','length','max'=>400000),
			array('area_rai,area_ngan,area_wa,area_metre,price,publish_date,end_date,bid_date,create_date,create_ip,last_update,update_ip,approve_date,approve_ip,delete_date,delete_ip,start_price,end_price,area_start,area_end,start_date,sync_date','length','max'=>40),
			array('tambol,district,province,town_plan,sale_type','length','max'=>40),
			array('main_image,map,land_map,building_map,key_word,home_no,moo,project,road,','length','max'=>500),
			array('product_code', 'checkDupProp',),
			
		);
	}
	public function checkDupProp($attribute,$params)
	 {
		 $pg = FrontProperty::model()->find("product_code='" . $this->product_code . "' and id<>'" . $this->id . "' and status<>'3'");
			if(count($pg)>0){
				$this->addError('product_code',$this->product_code . ' * รหัสทรัพย์นี้้ถูกเพิ่มไปแล้ว');
			}
	 }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		//'StatusOption'=> array(self::BELONGS_TO, 'Options', '', 'on' => 't.status=option_code AND option_name=\'Status\'',),
		
		/*return array(
			'category' => array(self::BELONGS_TO, 'ProductGroup', 'field', 'primaryKey'=>'id'),
		);*/
		return array(
			'zone' => array(self::HAS_ONE, 'ColorZone', array('id' => 'town_plan')),
			'category' => array(self::HAS_ONE, 'ProductGroup', array('id' => 'product_type')),
			'ta' => array(self::HAS_ONE, 'Tambol', array('id' => 'tambol')),
			'd' => array(self::HAS_ONE, 'District', array('id' => 'district')),
			'p' => array(self::HAS_ONE, 'Provinces', array('province_id' => 'province')),
		);
		
		//return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'command'=>'Command',
			'product_code'=>'รหัสทรัพย์'
		);
	}


	
	
	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	 /*
	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}*/

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		$this->approve_status = 0;
		if($this->view_count=="")
			$this->view_count = 0;
		if($this->sale_price=="")
			$this->sale_price = 0;
		/*
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->create_date = date("Y-m-d H:i:s");
				$this->create_ip = $_SERVER['REMOTE_ADDR'];
				$this->create_by = Yii::app()->user->getId();
				$this->main_image = "main_image.jpg";
				$this->map = "image_map.jpg";
				$this->land_map = "image_plan.jpg";
				$this->building_map = "image_build.jpg";
				$this->status = 0;
				$this->approve_status = 0;
				$this->bid_place = "-";
				$this->last_update ="";
				$this->update_ip = "";
				$this->update_by = "";
				$this->approve_date =  "";
				$this->approve_ip =  "";
				$this->approve_by = "";
				$this->delete_date =  "";
				$this->delete_ip =  "";
				$this->delete_by = "";
				$this->is_deleted = 0;
				$this->highlight_order = -1;
				$this->view_count =0;
				if(!is_numeric($this->area_wa))
					$this->area_wa = 0.0;
				if(!is_numeric($this->area_metre))
					$this->area_metre = 0.00;
			}else{
				if(isset($_FILES['image_main']) && $_FILES['image_main']['tmp_name']!="" )
					$this->main_image = "main_image.jpg";
				if(isset($_FILES['image_map'])&& $_FILES['image_map']['tmp_name']!="")
					$this->map = "image_map.jpg";
				if(isset($_FILES['image_plan'])&& $_FILES['image_plan']['tmp_name']!="")
					$this->land_map = "image_plan.jpg";
				if(isset($_FILES['image_build'])&& $_FILES['image_build']['tmp_name']!="")
					$this->building_map = "image_build.jpg";
				$this->last_update = date("Y-m-d H:i:s");
				$this->update_ip = $_SERVER['REMOTE_ADDR'];
				$this->update_by = Yii::app()->user->getId();
			}
			$this->sync_type=0;
			return true;
		}
		else
			return false;
			*/
			return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		$id= $this->id;
//		print_r($_FILES['image_main']);
		$dir = Yii::app()->basePath  .'/../images/npa/' . $id . '/';
		//echo "<br />" . $dir;
		if(!file_exists($dir))
			mkdir($dir,0777);
		if(isset($_FILES['image_main']) && $_FILES['image_main']['tmp_name']!="" )
			copy($_FILES['image_main']['tmp_name'],$dir . 'main_image.jpg');
		if(isset($_FILES['image_map'])&& $_FILES['image_map']['tmp_name']!="")
			copy($_FILES['image_map']['tmp_name'],$dir . 'image_map.jpg');
		if(isset($_FILES['image_plan'])&& $_FILES['image_plan']['tmp_name']!="")
			copy($_FILES['image_plan']['tmp_name'],$dir . 'image_plan.jpg');
		if(isset($_FILES['image_build'])&& $_FILES['image_build']['tmp_name']!="")
			copy($_FILES['image_build']['tmp_name'],$dir . 'image_build.jpg');
		//image_main,image_map,image_plan,image_build
//		echo ;
		
		
		
		//

		//die("<br />" . $_FILES['image_main']['tmp_name']);
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	 public function reorder_highlight($id,$sort_order,$old_order)
	 {
		 if($old_order<$sort_order)
			$sql = "update tb_product2 set highlight_order=highlight_order-1 where highlight_order >= $old_order and highlight_order<=$sort_order and id<>$id";
		 else
		 	$sql = "update tb_product2 set highlight_order=highlight_order+1 where highlight_order >= $sort_order and highlight_order<=$old_order and id<>$id";
		
		
		 $connection=Yii::app()->db;
		 $command=$connection->createCommand($sql);
		 $command->execute(); 
	 }
	public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('FrontProperty', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'product_code asc',
			),
		));
	}
	public function front_search()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array( 'p','category','ta','d' );

		//echo $condi;
		$condi = "";
		if($this->product_code != "")
			$condi .= " and product_code like '%" . $this->product_code ."%'";
		if($this->product_type != "")
			$condi .= " and product_type = '" . $this->product_type ."'";
		if($this->status != "")
		{
			switch($this->status)
			{
				case "1":$condi .= " and status = '1' and sale_type ='0' ";break;
				case "2":$condi .= " and status = '1' and sale_type ='1' ";break;
				case "3":$condi .= " and status = '2' ";break;
			}
		}
		if($this->start_price != "")
			$condi .= " and price >= '" . $this->start_price ."'";
		if($this->end_price != "")
			$condi .= " and price <= '" . $this->end_price ."'";
			
		if($this->start_date != "")
			$condi .= " and publish_date >= '" . $this->start_date ."'";
		if($this->end_date != "")
			$condi .= " and publish_date <= '" . $this->end_date ."'";
		if($this->continent != "")
			$condi .= " and p.continent_id = '" . $this->continent ."'";
		if($this->province != "")
			$condi .= " and p.province_id = '" . $this->province ."'";
		if($this->district != "")
			$condi .= " and district = '" . $this->district ."'";
		if($this->road != "")
			$condi .= " and road like '%" . $this->road ."%'";
		if($this->area_start != "")
			$condi .= " and area_rai >= '" . $this->area_start ."'";
		if($this->area_end != "")
			$condi .= " and area_rai <= '" . $this->area_end ."'";
			
		if(isset($this->key_address))
		{
			$this->key_word = $this->key_address;
		}
			
		if($this->key_word != ""){
			$condi .= " and (category.product_group like '%" . $this->key_word . "%'" ;
			$condi .= " or home_no like '%" . $this->key_word . "%'" ;
		 	$condi .= " or moo like '%" . $this->key_word . "%'" ;
			$condi .= " or ta.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or d.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or p.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or concat('จ.',p.thai_name) like '%" . $this->key_word . "%'" ;
			$condi .= " or project like '%" . $this->key_word . "%'" ;
			$condi .= " or soi like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc_th like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc2_th like '%" . $this->key_word . "%'" ;
			$condi .= " or product_limit_th like '%" . $this->key_word . "%'" ;
			$condi .= " or remark_th like '%" . $this->key_word . "%'" ;
			$condi .= " or other_th like '%" . $this->key_word . "%'" ;
			$condi .= " or more_detail_th like '%" . $this->key_word . "%'" ;
			$condi .= " or enter_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc2_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_limit_en like '%" . $this->key_word . "%'" ;
			$condi .= " or remark_en like '%" . $this->key_word . "%'" ;
			$condi .= " or other_en like '%" . $this->key_word . "%'" ;
			$condi .= " or more_detail_en like '%" . $this->key_word . "%'" ;
			$condi .= ")";
		}
		
		if($this->sort_by!="")
		{
			$sort_order = ($this->sort_dir=="2"?"DESC":"ASC");
			switch($this->sort_by)
			{
				case "1":$criteria->order = " product_code " . $sort_order;break;
				case "2":$criteria->order = " product_type " . $sort_order;break;
				case "3":$criteria->order = " `t`.province " . $sort_order;break;
				case "4":$criteria->order = " price " . $sort_order;break;
				case "5":$criteria->order = " status " . $sort_order . ",sale_type " . $sort_order;break;
			}
			
		}else{
			$criteria->order = " product_code ASC";
		}
		$criteria->condition = " (t.status='1' or t.status='2') and is_show='1'  $condi";
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('FrontProperty', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'product_code asc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:20),
			'currentPage'=>$this->current_page,

        ),
		));
	}
	
	public function front_search_all()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array( 'p','category','ta','d' );

		//echo $condi;
		$condi = "";
		if($this->product_code != "")
			$condi .= " and product_code like '%" . $this->product_code ."%'";
		if($this->product_type != "")
			$condi .= " and product_type = '" . $this->product_type ."'";
		if($this->status != "")
		{
			switch($this->status)
			{
				case "1":$condi .= " and status = '1' and sale_type ='0' ";break;
				case "2":$condi .= " and status = '1' and sale_type ='1' ";break;
				case "3":$condi .= " and status = '2' ";break;
			}
		}
		if($this->start_price != "")
			$condi .= " and price >= '" . $this->start_price ."'";
		if($this->end_price != "")
			$condi .= " and price <= '" . $this->end_price ."'";
			
		if($this->start_date != "")
			$condi .= " and publish_date >= '" . $this->start_date ."'";
		if($this->end_date != "")
			$condi .= " and publish_date <= '" . $this->end_date ."'";
		if($this->continent != "")
			$condi .= " and p.continent_id = '" . $this->continent ."'";
		if($this->province != "")
			$condi .= " and p.province_id = '" . $this->province ."'";
		if($this->district != "")
			$condi .= " and district = '" . $this->district ."'";
		if($this->road != "")
			$condi .= " and road like '%" . $this->road ."%'";
		if($this->area_start != "")
			$condi .= " and area_rai >= '" . $this->area_start ."'";
		if($this->area_end != "")
			$condi .= " and area_rai <= '" . $this->area_end ."'";
			
		if(isset($this->key_address))
		{
			$this->key_word = $this->key_address;
		}
			
		if($this->key_word != ""){
			$condi .= " and (category.product_group like '%" . $this->key_word . "%'" ;
			$condi .= " or home_no like '%" . $this->key_word . "%'" ;
		 	$condi .= " or moo like '%" . $this->key_word . "%'" ;
			$condi .= " or ta.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or d.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or p.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or concat('จ.',p.thai_name) like '%" . $this->key_word . "%'" ;
			$condi .= " or project like '%" . $this->key_word . "%'" ;
			$condi .= " or soi like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc_th like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc2_th like '%" . $this->key_word . "%'" ;
			$condi .= " or product_limit_th like '%" . $this->key_word . "%'" ;
			$condi .= " or remark_th like '%" . $this->key_word . "%'" ;
			$condi .= " or other_th like '%" . $this->key_word . "%'" ;
			$condi .= " or more_detail_th like '%" . $this->key_word . "%'" ;
			$condi .= " or enter_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc2_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_limit_en like '%" . $this->key_word . "%'" ;
			$condi .= " or remark_en like '%" . $this->key_word . "%'" ;
			$condi .= " or other_en like '%" . $this->key_word . "%'" ;
			$condi .= " or more_detail_en like '%" . $this->key_word . "%'" ;
			$condi .= ")";
		}
		
		if($this->sort_by!="")
		{
			$sort_order = ($this->sort_dir=="2"?"DESC":"ASC");
			switch($this->sort_by)
			{
				case "1":$criteria->order = " product_code " . $sort_order;break;
				case "2":$criteria->order = " product_type " . $sort_order;break;
				case "3":$criteria->order = " `t`.province " . $sort_order;break;
				case "4":$criteria->order = " price " . $sort_order;break;
				case "5":$criteria->order = " status " . $sort_order . ",sale_type " . $sort_order;break;
			}
			
		}else{
			$criteria->order = " product_code ASC";
		}
		$criteria->condition = " (t.status='1' or t.status='2') and is_show='1'  $condi";
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('FrontProperty', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'product_code asc',
			),
			'pagination' => array(
            'pageSize' => 300000,
			'currentPage'=>$this->current_page,

        ),
		));
	}
	
	public function backend_search()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array( 'p','category','ta','d' );

		//echo $condi;
		$condi = "";
		if($this->product_code != "")
			$condi .= " and product_code like '%" . $this->product_code ."%'";
		if($this->product_type != "")
			$condi .= " and product_type = '" . $this->product_type ."'";
		if($this->status != "")
		{
			switch($this->status)
			{
				case "1":$condi .= " and status = '1' and sale_type ='0' ";break;
				case "2":$condi .= " and status = '1' and sale_type ='1' ";break;
				case "3":$condi .= " and status = '2' ";break;
				case "4":$condi .= " and status = '0' ";break;
				case "5":$condi .= " and status = '1' ";break;
			}
		}
		if($this->start_price != "")
			$condi .= " and price >= '" . $this->start_price ."'";
		if($this->end_price != "")
			$condi .= " and price <= '" . $this->end_price ."'";
			
		if($this->start_date != "")
			$condi .= " and publish_date >= '" . $this->start_date ."'";
		if($this->end_date != "")
			$condi .= " and publish_date <= '" . $this->end_date ."'";
		if($this->continent != "")
			$condi .= " and p.continent_id = '" . $this->continent ."'";
		if($this->province != "")
			$condi .= " and p.province_id = '" . $this->province ."'";
		if($this->district != "")
			$condi .= " and district = '" . $this->district ."'";
		if($this->road != "")
			$condi .= " and road like '%" . $this->road ."%'";
		if($this->area_start != "")
			$condi .= " and area_rai >= '" . $this->area_start ."'";
		if($this->area_end != "")
			$condi .= " and area_rai <= '" . $this->area_end ."'";
			
			
		if($this->key_word != ""){
			$condi .= " and (category.product_group like '%" . $this->key_word . "%'" ;
			$condi .= " or home_no like '%" . $this->key_word . "%'" ;
		 	$condi .= " or moo like '%" . $this->key_word . "%'" ;
			$condi .= " or ta.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or d.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or p.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or project like '%" . $this->key_word . "%'" ;
			$condi .= " or soi like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc_th like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc2_th like '%" . $this->key_word . "%'" ;
			$condi .= " or product_limit_th like '%" . $this->key_word . "%'" ;
			$condi .= " or remark_th like '%" . $this->key_word . "%'" ;
			$condi .= " or other_th like '%" . $this->key_word . "%'" ;
			$condi .= " or more_detail_th like '%" . $this->key_word . "%'" ;
			$condi .= " or enter_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc2_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_limit_en like '%" . $this->key_word . "%'" ;
			$condi .= " or remark_en like '%" . $this->key_word . "%'" ;
			$condi .= " or other_en like '%" . $this->key_word . "%'" ;
			$condi .= " or more_detail_en like '%" . $this->key_word . "%'" ;
			$condi .= ")";
		}
		
		
		if($this->sort_by!="")
		{
			$sort_order = ($this->sort_dir=="2"?"DESC":"ASC");
			switch($this->sort_by)
			{
				case "1":$criteria->order = " product_code " . $sort_order;break;
				case "2":$criteria->order = " product_type " . $sort_order;break;
				case "3":$criteria->order = " `t`.province " . $sort_order;break;
				case "4":$criteria->order = " price " . $sort_order;break;
				case "5":$criteria->order = " status " . $sort_order . ",sale_type " . $sort_order;break;
			}
			
		}else{
			$criteria->order = " product_code ASC";
		}
		$criteria->condition = " (t.status<>'3')  $condi";
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('FrontProperty', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'product_code asc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:20),
			'currentPage'=>$this->current_page,

        ),
		));
	}
	public function backend_delete_search()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array( 'p','category','ta','d' );

		//echo $condi;
		$condi = "";
		if($this->product_code != "")
			$condi .= " and product_code like '%" . $this->product_code ."%'";
		if($this->product_type != "")
			$condi .= " and product_type = '" . $this->product_type ."'";
		if($this->status != "")
		{
			switch($this->status)
			{
				case "1":$condi .= " and status = '1' and sale_type ='0' ";break;
				case "2":$condi .= " and status = '1' and sale_type ='1' ";break;
				case "3":$condi .= " and status = '2' ";break;
			}
		}
		if($this->start_price != "")
			$condi .= " and price >= '" . $this->start_price ."'";
		if($this->end_price != "")
			$condi .= " and price <= '" . $this->end_price ."'";
			
		if($this->start_date != "")
			$condi .= " and publish_date >= '" . $this->start_date ."'";
		if($this->end_date != "")
			$condi .= " and publish_date <= '" . $this->end_date ."'";
		if($this->continent != "")
			$condi .= " and p.continent_id = '" . $this->continent ."'";
		if($this->province != "")
			$condi .= " and p.province_id = '" . $this->province ."'";
		if($this->district != "")
			$condi .= " and district = '" . $this->district ."'";
		if($this->road != "")
			$condi .= " and road like '%" . $this->road ."%'";
		if($this->area_start != "")
			$condi .= " and area_rai >= '" . $this->area_start ."'";
		if($this->area_end != "")
			$condi .= " and area_rai <= '" . $this->area_end ."'";
			
			
		if($this->key_word != ""){
			$condi .= " and (category.product_group like '%" . $this->key_word . "%'" ;
			$condi .= " or home_no like '%" . $this->key_word . "%'" ;
		 	$condi .= " or moo like '%" . $this->key_word . "%'" ;
			$condi .= " or ta.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or d.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or p.thai_name like '%" . $this->key_word . "%'" ;
			$condi .= " or project like '%" . $this->key_word . "%'" ;
			$condi .= " or soi like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc_th like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc2_th like '%" . $this->key_word . "%'" ;
			$condi .= " or product_limit_th like '%" . $this->key_word . "%'" ;
			$condi .= " or remark_th like '%" . $this->key_word . "%'" ;
			$condi .= " or other_th like '%" . $this->key_word . "%'" ;
			$condi .= " or more_detail_th like '%" . $this->key_word . "%'" ;
			$condi .= " or enter_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_desc2_en like '%" . $this->key_word . "%'" ;
			$condi .= " or product_limit_en like '%" . $this->key_word . "%'" ;
			$condi .= " or remark_en like '%" . $this->key_word . "%'" ;
			$condi .= " or other_en like '%" . $this->key_word . "%'" ;
			$condi .= " or more_detail_en like '%" . $this->key_word . "%'" ;
			$condi .= ")";
		}
		
		if($this->sort_by!="")
		{
			$sort_order = ($this->sort_dir=="2"?"DESC":"ASC");
			switch($this->sort_by)
			{
				case "1":$criteria->order = " product_code " . $sort_order;break;
				case "2":$criteria->order = " product_type " . $sort_order;break;
				case "3":$criteria->order = " `t`.province " . $sort_order;break;
				case "4":$criteria->order = " price " . $sort_order;break;
				case "5":$criteria->order = " status " . $sort_order . ",sale_type " . $sort_order;break;
			}
			
		}else{
			$criteria->order = " product_code ASC";
		}
		$criteria->condition = " (t.status='3'  )  $condi";
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('FrontProperty', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'product_code asc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:20),
			'currentPage'=>$this->current_page,

        ),
		));
	}
	
	public function show_highlight()
	{
		$criteria=new CDbCriteria;
		$criteria->condition = " (t.status='1' or t.status='2') and is_highlight=1 ";
		$criteria->order = " highlight_order asc ";
		return new CActiveDataProvider('FrontProperty', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'product_code asc',
			),'pagination' => array(
            'pageSize' => 100,
			'currentPage'=>0,)
		));
	}
	public function show_outter()
	{
		$criteria=new CDbCriteria;
		$criteria->condition = "  is_assign=1 ";
		$criteria->order = " id desc ";
		return new CActiveDataProvider('Property', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'product_code asc',
			),'pagination' => array(
            'pageSize' => 100,
			'currentPage'=>0,)
		));
	}
	public function SyncData()
	{
		$criteria=new CDbCriteria;
		$condi = " (sync_type<>1 or sync_type is null) and (SUBSTRING(sync_date, 1, 10) <= '" . date('Y-m-d') . "' or trim(ifnull(sync_date,''))='') and status<>'0' ";
		$criteria->condition = $condi;
		$criteria->order = " id asc ";
		return new CActiveDataProvider('FrontProperty', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'product_code asc',
			),'pagination' => array(
            'pageSize' => 100000,
			'currentPage'=>0,)
		));
	}
}