<?php

/**
 * This is the model class for table "tb_send_news".
 *
 * The followings are the available columns in table 'tb_send_news':
 * @property integer $id
 * @property integer $content_id
 * @property integer $subscriber_id
 * @property string $send_date
 * @property string $send_ip
 * @property string $send_by
 */
class SendNews extends CActiveRecord {

    public $display_perpage;
    public $current_page;
    public $row_count;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SendNews the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_send_news';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content_id, subscriber_id', 'numerical', 'integerOnly' => true),
            array('send_date, send_ip, send_by', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, content_id, subscriber_id, send_date, send_ip, send_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'subscriber' => array(self::BELONGS_TO, 'Subscriber', array('subscriber_id' => 'id')),
            'content' => array(self::BELONGS_TO, 'Content', array('content_id' => 'id')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'content_id' => 'Content',
            'subscriber_id' => 'Subscriber',
            'send_date' => 'Send Date',
            'send_ip' => 'Send Ip',
            'send_by' => 'Send By',
        );
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->send_date = date("Y-m-d H:i:s");
            $this->send_by = Yii::app()->user->id;
            $this->send_ip = Yii::app()->request->getUserHostAddress();           
        }
        return true;
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('content_id', $this->content_id);
        $criteria->compare('subscriber_id', $this->subscriber_id);
        $criteria->compare('send_date', $this->send_date, true);
        $criteria->compare('send_ip', $this->send_ip, true);
        $criteria->compare('send_by', $this->send_by, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function listForManage() {
        $criteria = new CDbCriteria;
        $criteria->with = array('content','subscriber');  
        
        $this->row_count = $this->count($criteria);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

}
