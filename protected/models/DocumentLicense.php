<?php

class DocumentLicense extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */

	public $id;
	public $npa_id;
	public $license_name;
	public $license_no;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_license_map';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('npa_id, license_name,license_no', 'required'),
			array('npa_id, license_name,license_no', 'safe', 'on'=>'search'),
			array('license_no', 'length','max'=>4000),
			array('license_name,', 'length','max'=>250),
			array('sync_date','length','max'=>40),
			array('npa_id,sync_type','length','max'=>11),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'document' => array(self::HAS_ONE, 'MasterDocument', array('id' => 'license_name')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
		);
	}

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
	//	echo "<br />" .  $this->npa_id . "<br />" ;
			return true; 
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
		
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;


		$criteria->compare('license_name',$this->group_name);

		return new CActiveDataProvider('DocumentLicense', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'license_name asc',
			),
		));
	}
	public function searchByNpaId($id=null)
	{
		$criteria=new CDbCriteria;
		$criteria->compare("npa_id",$id);
		$criteria->compare("sync_type",0);
		return new CActiveDataProvider('DocumentLicense', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id asc',
			),
		));
	}
}