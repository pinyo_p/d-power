<?php

/**
 * This is the model class for table "tb_content".
 *
 * The followings are the available columns in table 'tb_content':
 * @property integer $id
 * @property string $title_1
 * @property string $title_2
 * @property string $title_3
 * @property string $title_4
 * @property string $title_5
 * @property string $content_1
 * @property string $content_2
 * @property string $content_3
 * @property string $content_4
 * @property string $content_5
 * @property string $content_code
 * @property string $create_date
 * @property string $create_by
 * @property string $create_ip
 * @property string $update_date
 * @property string $update_by
 * @property string $update_ip
 * @property integer $status
 * @property integer $sort_order
 * @property integer $view_count
 * @property string $attr1
 * @property string $attr2
 * @property string $attr3
 * @property string $attr4
 * @property string $attr5
 */
class Content extends CActiveRecord {

    public $display_perpage;
    public $current_page;
    public $row_count;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Content the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_content';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content_code,title_2,sort_order,status', 'required','message'=>'ข้อมูล {attribute} ไม่สามารถเป็นค่าว่างได้'),
            array('status, sort_order, view_count', 'numerical', 'integerOnly' => true),
            array('title_1, title_2, title_3, title_4, title_5', 'length', 'max' => 250),
            array('content_code, create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max' => 45),
            array('attr1', 'length', 'max' => 500),
            array('attr2, attr3, attr4, attr5', 'length', 'max' => 40),
            array('short_content_1, short_content_2, short_content_3, short_content_4, short_content_5, content_1, content_2, content_3, content_4, content_5', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title_1, title_2, title_3, title_4, title_5, content_1, content_2, content_3, content_4, content_5, content_code, create_date, create_by, create_ip, update_date, update_by, update_ip, status, sort_order, view_count, attr1, attr2, attr3, attr4, attr5', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title_1' => 'ชื่อเรื่อง (ภาษาอังกฤษ)',
            'title_2' => 'ชื่อเรื่อง (ภาษาไทย)',
            'title_3' => 'Title 3',
            'title_4' => 'Title 4',
            'title_5' => 'Title 5',
            'short_content_1' => 'เนื้อเรื่องย่อ (ภาษาอังกฤษ)',
            'short_content_2' => 'เนื้อเรื่องย่อ (ภาษาไทย)',
            'short_content_3' => 'Short Content 3',
            'short_content_4' => 'Short Content 4',
            'short_content_5' => 'Short Content 5',
            'content_1' => 'เนื้อเรื่องทั้งหมด (ภาษาอังกฤษ)',
            'content_2' => 'เนื้อเรื่องทั้งหมด (ภาษาไทย)',
            'content_3' => 'Content 3',
            'content_4' => 'Content 4',
            'content_5' => 'Content 5',
            'content_code' => 'หมวดหมู่',
            'create_date' => 'Create Date',
            'create_by' => 'Create By',
            'create_ip' => 'Create Ip',
            'update_date' => 'Update Date',
            'update_by' => 'Update By',
            'update_ip' => 'Update Ip',
            'status' => 'Status',
            'sort_order' => 'จัดเรียง',
            'view_count' => 'View Count',
            'attr1' => 'Attr1',
            'attr2' => 'Attr2',
            'attr3' => 'Attr3',
            'attr4' => 'Attr4',
            'attr5' => 'Attr5',
        );
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = Yii::app()->request->getUserHostAddress();
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = $_SERVER['REMOTE_ADDR'];
            $this->status = 1;
            $this->view_count = 0;
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
        }
        return true;
    }
    
    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title_1', $this->title_1, true);
        $criteria->compare('title_2', $this->title_2, true);
        $criteria->compare('title_3', $this->title_3, true);
        $criteria->compare('title_4', $this->title_4, true);
        $criteria->compare('title_5', $this->title_5, true);
        $criteria->compare('short_content_1', $this->short_content_1, true);
        $criteria->compare('short_content_2', $this->short_content_2, true);
        $criteria->compare('short_content_3', $this->short_content_3, true);
        $criteria->compare('short_content_4', $this->short_content_4, true);
        $criteria->compare('short_content_5', $this->short_content_5, true);
        $criteria->compare('content_1', $this->content_1, true);
        $criteria->compare('content_2', $this->content_2, true);
        $criteria->compare('content_3', $this->content_3, true);
        $criteria->compare('content_4', $this->content_4, true);
        $criteria->compare('content_5', $this->content_5, true);
        $criteria->compare('content_code', $this->content_code, true);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('create_by', $this->create_by, true);
        $criteria->compare('create_ip', $this->create_ip, true);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('update_by', $this->update_by, true);
        $criteria->compare('update_ip', $this->update_ip, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('view_count', $this->view_count);
        $criteria->compare('attr1', $this->attr1, true);
        $criteria->compare('attr2', $this->attr2, true);
        $criteria->compare('attr3', $this->attr3, true);
        $criteria->compare('attr4', $this->attr4, true);
        $criteria->compare('attr5', $this->attr5, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchByCodeForIndex() {
        $criteria = new CDbCriteria;
        $criteria->compare('content_code', $this->content_code);
        $criteria->compare('status', '1');
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 't.sort_order asc',),
            'pagination' => array('pageSize' => 999999, 'currentPage' => 0,)
        ));
    }
    
    public function searchAll(){
        $criteria = new CDbCriteria;
        $criteria->compare('content_code', $this->content_code);
        $criteria->addCondition("status != '4' ");

        return new CActiveDataProvider('Content', array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 't.id desc',),
            'pagination' => array('pageSize' => 999999, 'currentPage' => 0,)
        ));
    }
    
    public function searchNewsForIndex(){
        $criteria = new CDbCriteria;
        $criteria->compare('content_code', 'News');
        $criteria->addCondition("status != '4' ");

        return new CActiveDataProvider('Content', array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 't.id desc',),
            'pagination' => array('pageSize' => 2, 'currentPage' => 0,)
        ));
    }
    
    public function searchByCode() {
        $criteria = new CDbCriteria;

        $criteria->compare('content_code', $this->content_code);

        return new CActiveDataProvider('Content', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => '`sort_order` asc',
            ),
        ));
    }
    
    public function searchForManage($title,$contentCode,$status) {
        $criteria = new CDbCriteria;

        $condi = "";
        if (trim($title) != ""){
            $condi .= " and (";
            $condi .= "     title_1 like '%".$title."%'";
            $condi .= "     or title_2 like '%".$title."%'";
            $condi .= "     or title_3 like '%".$title."%'";
            $condi .= "     or title_4 like '%".$title."%'";
            $condi .= "     or title_5 like '%".$title."%'";
            $condi .= " )";
        }
        if (trim($contentCode) != "0")
            $condi .= " and content_code = '" . $contentCode . "'";
        if (trim($status) != "all")
            $condi .= " and status = '" . $status . "'";
        
        $criteria->condition = " 1=1 ".$condi;
        $this->row_count = $this->count($criteria);
      
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }
    
    public function searchNewsForManage($title,$content) {
        $criteria = new CDbCriteria;

        $condi = "";
        if (trim($title) != ""){
            $condi .= " and (";
            $condi .= "     title_1 like '%".$title."%'";
            $condi .= "     or title_2 like '%".$title."%'";
            $condi .= "     or title_3 like '%".$title."%'";
            $condi .= "     or title_4 like '%".$title."%'";
            $condi .= "     or title_5 like '%".$title."%'";
            $condi .= " )";
        }
        if (trim($content) != ""){
            $condi .= " and (";
            $condi .= "     short_content_1 like '%".$content."%'";
            $condi .= "     or short_content_2 like '%".$content."%'";
            $condi .= "     or short_content_3 like '%".$content."%'";
            $condi .= "     or short_content_4 like '%".$content."%'";
            $condi .= "     or short_content_5 like '%".$content."%'";
            $condi .= "     or content_1 like '%".$content."%'";
            $condi .= "     or content_2 like '%".$content."%'";
            $condi .= "     or content_3 like '%".$content."%'";
            $condi .= "     or content_4 like '%".$content."%'";
            $condi .= "     or content_5 like '%".$content."%'";
            $condi .= " )";
        }
 
        $criteria->condition = " content_code='News' ".$condi;
        $this->row_count = $this->count($criteria);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }


}
