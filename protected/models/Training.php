<?php

class Training extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_training';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject_en,subject_th,language,teacher','length','max'=>250),
			array('start_date,end_date,times,days,duration,status,training_code','length','max'=>40),
			array('start_date2,end_date2,start_date3,end_date3,start_date4,end_date4,start_date5,end_date5,start_date6,end_date6,start_date7,end_date7,start_date8,end_date8,start_date9,end_date9,start_date10,end_date10,start_date11,end_date11,start_date12,end_date12,','length','max'=>40),
			array('price','length','max'=>100),
			array('benefit_en,benefit_th,object_en,object_th,who_interest_en,who_interest_th,course_en,course_th,payment_en,payment_th,contact_en,contact_th','length','max'=>65535),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'color' => 'Color Name','command'=>'Command'
		);
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	 
	

	

	/**
	 * Normalizes the user-entered tags.
	 */


	

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		
			if($this->isNewRecord)
			{
				$this->create_date=date("Y-m-d H:i:s");
				$this->create_by=Yii::app()->user->id;
				$this->create_ip = $_SERVER['REMOTE_ADDR'];
				$this->update_date=date("Y-m-d H:i:s");
				$this->update_by=Yii::app()->user->id;
				$this->update_ip = $_SERVER['REMOTE_ADDR'];
			}
			else
			{
				$this->update_date=date("Y-m-d H:i:s");
				$this->update_by=Yii::app()->user->id;
				$this->update_ip = $_SERVER['REMOTE_ADDR'];
			}
			
return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('Training', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id asc',
			),
		));
	}
	public function front_show()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('Training', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id asc',
			),
		));
	}
}