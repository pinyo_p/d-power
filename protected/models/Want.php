<?php

class Want extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id


	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	 public $price;
	 public $start_date;
	 public $end_date;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_want';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name,last_name,email', 'required'),
			array('product_type','length','max'=>11),
			array('start_price,end_price','length','max'=>45),
			array('first_name,last_name','length','max'=>250),
			array('phone_no,email,road','length','max'=>150),
			array('create_date,create_ip','length','max'=>20),
			array('province,district,tambol,area_rai_from,area_rai_to,area_ngan_from,area_ngan_to,area_wa_from,area_wa_to,status','length','max'=>10),
			array('more_detail,','length','max'=>5000),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		
		return array(
			'category' => array(self::HAS_ONE, 'ProductGroup', array('id' => 'product_type')),
			'ta' => array(self::HAS_ONE, 'Tambol', array('id' => 'tambol')),
			'd' => array(self::HAS_ONE, 'District', array('id' => 'district')),
			'p' => array(self::HAS_ONE, 'Provinces', array('province_id' => 'province')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'title' => 'Title',
		);
	}

	public function getlist()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('AboutSam', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sort_order asc',
			),
		));
	}

	protected function beforeSave()
	{
		if($this->isNewRecord)
		{
			$this->create_date = date("Y-m-d H:i:s");
			$this->create_ip = $_SERVER['REMOTE_ADDR'];
			$this->status = 0;
		}
		return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$condi = " 1=1 ";
		if($this->first_name != "")
			$condi .= " and first_name like '%" . $this->first_name . "%' ";
		if($this->last_name != "")
			$condi .= " and last_name like '%" . $this->last_name . "%' ";
		if($this->phone_no != "")
			$condi .= " and phone_no like '%" . $this->phone_no . "%' ";
		if($this->email != "")
			$condi .= " and email like '%" . $this->email . "%' ";
		if($this->province != "")
			$condi .= " and first_name ='" . $this->province . "' ";
		if($this->district != "")
			$condi .= " and district = '" . $this->district . "' ";
		if($this->tambol != "")
			$condi .= " and tambol ='" . $this->tambol . "' ";
		if($this->road != "")
			$condi .= " and road like '%" . $this->road . "%' ";
		if($this->status != "")
			$condi .= " and status ='" . $this->status . "' ";
		if($this->product_type != "")
			$condi .= " and product_type ='" . $this->product_type . "' ";
		if($this->price != "")
			$condi .= " and start_price <='" . $this->price . "' and end_price >='" . $this->price . "' ";
		
		if($this->start_date != "")
			$condi .= " and substring(create_date,1,10) >='" . $this->start_date . "' ";
		if($this->end_date != "")
			$condi .= " and substring(create_date,1,10)<='" . $this->end_date . "' ";

		
		$criteria->condition= $condi;

		return new CActiveDataProvider('Want', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id asc',
			),
			'pagination' => array(
            'pageSize' => 300000,
			'currentPage'=>1,

        ),
		));
	}
	
	public function search2($in)
	{
		$criteria=new CDbCriteria;
		$condi = " id in ($in) ";
		
		$criteria->condition= $condi;

		return new CActiveDataProvider('Want', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id asc',
			),
			'pagination' => array(
            'pageSize' => 300000,
			'currentPage'=>1,

        ),
		));
	}
}