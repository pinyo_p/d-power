<?php

class BannerDay extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public $key_word;
    public $display_perpage;
    public $current_page;
    public $row_count;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_banner_day';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('day_id', 'required'),
            array('day_id', 'numerical', 'integerOnly' => true),
            array('img_src', 'length', 'max' => 500),
            array('create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, day_id, img_src, create_date, create_by, create_ip, update_date, update_by, update_ip', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        /*
          return array(
          'author' => array(self::BELONGS_TO, 'User', 'author_id'),
          'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
          'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
          ); */
        return array(
            'day' => array(self::HAS_ONE, 'day', array('id' => 'day_id')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'day_id' => 'Day',
            'img_src' => 'Img Src',
            'create_date' => 'Create Date',
            'create_by' => 'Create By',
            'create_ip' => 'Create Ip',
            'update_date' => 'Update Date',
            'update_by' => 'Update By',
            'update_ip' => 'Update Ip',
        );
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    /**
     * Normalizes the user-entered tags.
     */

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {


        if ($this->isNewRecord) {

            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = $_SERVER['REMOTE_ADDR'];
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = $_SERVER['REMOTE_ADDR'];
        }

        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $condi = "";
        if (trim($this->day_id) != "")
            $condi .= " and day_id='" . $this->day_id . "'";

        $criteria->condition = " 1=1 $condi ";
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('BannerDay', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 30),
                'currentPage' => $this->current_page,),
        ));
    }
    
    public function searchAll() {
        $criteria = new CDbCriteria;
       
        $this->row_count = $this->count($criteria);
        
        return new CActiveDataProvider('BannerDay', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 30),
                'currentPage' => $this->current_page,),
        ));
    }

}
