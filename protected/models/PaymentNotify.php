<?php

/**
 * This is the model class for table "tb_payment_notify".
 *
 * The followings are the available columns in table 'tb_payment_notify':
 * @property integer $id
 * @property integer $member_id
 * @property integer $order_id
 * @property double $transfer_amount
 * @property string $transfer_date
 * @property string $transfer_time
 * @property integer $bank_id
 * @property string $member_bank
 * @property string $transfer_document
 * @property string $remark
 */
class PaymentNotify extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PaymentNotify the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_payment_notify';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('order_id, transfer_amount, transfer_date, bank_id', 'required', 'message' => ' {attribute} ไม่สามารถเป็นค่าว่างได้'),
            array('member_id, order_id, bank_id', 'numerical', 'integerOnly' => true),
            array('transfer_amount', 'numerical'),
            array('transfer_date, transfer_time', 'length', 'max' => 45),
            array('member_bank, transfer_document', 'length', 'max' => 300),
            array('remark', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, member_id, order_id, transfer_amount, transfer_date, transfer_time, bank_id, member_bank, transfer_document, remark', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'member_id' => 'Member',
            'order_id' => 'Order',
            'transfer_amount' => 'ยอดเงินที่โอน',
            'transfer_date' => 'วันที่โอนเงิน',
            'transfer_time' => 'Transfer Time',
            'bank_id' => 'Bank',
            'member_bank' => 'Member Bank',
            'transfer_document' => 'Transfer Document',
            'remark' => 'Remark',
        );
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {        
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('member_id', $this->member_id);
        $criteria->compare('order_id', $this->order_id);
        $criteria->compare('transfer_amount', $this->transfer_amount);
        $criteria->compare('transfer_date', $this->transfer_date, true);
        $criteria->compare('transfer_time', $this->transfer_time, true);
        $criteria->compare('bank_id', $this->bank_id);
        $criteria->compare('member_bank', $this->member_bank, true);
        $criteria->compare('transfer_document', $this->transfer_document, true);
        $criteria->compare('remark', $this->remark, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
