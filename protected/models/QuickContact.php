<?php

class QuickContact extends CActiveRecord {

    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */
    public $display_perpage;
    public $current_page;
    public $row_count;
    public $verifyCode;
    public $start_date;
    public $end_date;

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_contact';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status, type, ref_id', 'numerical', 'integerOnly' => true),
            array('fullname, company_name, position, website, topic, subject', 'length', 'max' => 500),
            array('tel, mobile, fax, email', 'length', 'max' => 250),
            array('address, detail', 'length', 'max' => 5000),
            array('create_date, create_ip, update_date, update_ip', 'length', 'max' => 20),
            array('is_subscribe', 'length', 'max' => 1),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, fullname, company_name, position, tel, mobile, fax, email, website, address, topic, subject, detail, create_date, create_ip, update_date, update_ip, status, type, ref_id, is_subscribe', 'safe', 'on' => 'search'),
            //array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.

        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'fullname' => 'Fullname',
            'company_name' => 'Company Name',
            'position' => 'Position',
            'tel' => 'Tel',
            'mobile' => 'Mobile',
            'fax' => 'Fax',
            'email' => 'Email',
            'website' => 'Website',
            'address' => 'Address',
            'topic' => 'Topic',
            'subject' => 'Subject',
            'detail' => 'Detail',
            'create_date' => 'Create Date',
            'create_ip' => 'Create Ip',
            'update_date' => 'Update Date',
            'update_ip' => 'Update Ip',
            'status' => 'Status',
            'type' => 'Type',
            'ref_id' => 'Ref',
            'is_subscribe' => 'Is Subscribe',
        );
    }

    /**
     * Adds a new comment to this post.
     * This method will set status and post_id of the comment accordingly.
     * @param Comment the comment to be added
     * @return boolean whether the comment is saved successfully
     */
    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    /*
      protected function afterFind()
      {
      parent::afterFind();
      $this->_oldTags=$this->tags;
      } */

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {

        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_ip = Yii::app()->request->getUserHostAddress();
            $this->status = 0;
        }
        $this->update_date = date("Y-m-d H:i:s");
        $this->update_ip = Yii::app()->request->getUserHostAddress();
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $condi = "";


        if ($this->subject != "")
            $condi .= " and subject like '%" . $this->subject . "%'";
        if ($this->fullname != "")
            $condi .= " and fullname like '%" . $this->fullname . "%'";
        if ($this->company_name != "")
            $condi .= " and company_name like '%" . $this->company_name . "%'";
        if ($this->detail != "")
            $condi .= " and detail like '%" . $this->detail . "%'";
        if ($this->status != "")
            $condi .= " and status='" . $this->status . "'";

        if ($this->type != "")
            $condi .= " and type='" . $this->type . "'";

        if (isset($this->start_date) && $this->start_date != "") {
            $condi .= " and create_date>='" . $this->start_date . "'";
        }

        if (isset($this->end_date) && $this->end_date != "") {
            $condi .= " and create_date<='" . $this->end_date . "'";
        }

        $criteria->condition = " 1=1 $condi ";
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('Contact', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => '`id` desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

}
