<?php

class RelateProduct extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	 
	 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_relate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array(
			'product' => array(self::HAS_ONE, 'Product', array('id' => 'product_relate_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'color' => 'Color Name','command'=>'Command'
		);
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	 
	

	

	/**
	 * Normalizes the user-entered tags.
	 */


	

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		

				$this->create_date=date("Y-m-d H:i:s");
		
				$this->create_by=Yii::app()->user->id;
				$this->create_ip = $_SERVER['REMOTE_ADDR'];

			
return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	 
	 public function Login()
	 {
		 $criteria=new CDbCriteria;
		$criteria->compare('username',$this->username,false);
		$criteria->compare('password',$this->password,false);
		$criteria->compare('status','0',false);
		
		return new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'username desc',
			),
		));
	 }
	 
	 public function backend_search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('product_id',$this->product_id);		

		return new CActiveDataProvider('RelateProduct', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id desc',
			),
		));
	}
	 
	 public function front_search()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array('product');
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('product.status','1');

		return new CActiveDataProvider('RelateProduct', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'t.id desc',
			),
		));
	}
	 
	public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'first_name,last_name desc',
			),
		));
	}
}