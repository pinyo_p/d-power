<?php

class Newsletter extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */
public $cemail;
public $row_count;
	public $display_perpage;
	public $current_page;
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_newsletter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					 
			array('fullname,email,cemail', 'required','message'=>' {attribute} ไม่สามารถเป็นค่าว่างได้'),
			array('is_news,is_npa,is_notice,is_ebook,is_activity', 'length','max'=>11),
			array('fullname,email', 'length','max'=>500),
			
			array('cemail', 'checkConfirmPass',),
		);
	}
	public function checkConfirmPass($attribute,$params)
	 {
			if($this->email!=$this->cemail){
				$this->addError('cemail', 'Email ไม่ตรงกัน');
			}
	 }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'fullname'=>'ชื่อ - นามสกุล',
			'cemail'=>'Confirm Email',
		);
	}

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
	//	echo "<br />" .  $this->npa_id . "<br />" ;
			$this->create_date = date("Y-m-d H:i:s");
			$this->create_ip = $_SERVER['REMOTE_ADDR'];
			return true; 
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
		
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */

	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('`email`',$this->email,true);
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('Newsletter', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id desc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:20),
			'currentPage'=>$this->current_page,)
		));
	}
}