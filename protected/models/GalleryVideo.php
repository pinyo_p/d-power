<?php

/**
 * This is the model class for table "tb_gallery_video".
 *
 * The followings are the available columns in table 'tb_gallery_video':
 * @property integer $id
 * @property integer $gallery_id
 * @property integer $sort_order
 * @property integer $is_cover
 * @property string $url
 * @property string $title_keyword
 * @property string $meta_keyword
 * @property string $meta_desc
 * @property string $create_date
 * @property string $create_by
 * @property string $create_ip
 * @property string $update_date
 * @property string $update_by
 * @property string $update_ip
 */
class GalleryVideo extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GalleryVideo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_gallery_video';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('gallery_id, sort_order, is_cover', 'numerical', 'integerOnly' => true),
            array('url', 'length', 'max' => 500),
            array('title_keyword, meta_keyword, meta_desc', 'length', 'max' => 250),
            array('create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, gallery_id, sort_order, is_cover, url, title_keyword, meta_keyword, meta_desc, create_date, create_by, create_ip, update_date, update_by, update_ip', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'gallery' => array(self::BELONGS_TO, 'Gallery', array('gallery_id' => 'id')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'gallery_id' => 'Gallery',
            'sort_order' => 'Sort Order',
            'is_cover' => 'Is Cover',
            'url' => 'Url',
            'title_keyword' => 'Title Keyword',
            'meta_keyword' => 'Meta Keyword',
            'meta_desc' => 'Meta Desc',
            'create_date' => 'Create Date',
            'create_by' => 'Create By',
            'create_ip' => 'Create Ip',
            'update_date' => 'Update Date',
            'update_by' => 'Update By',
            'update_ip' => 'Update Ip',
        );
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = Yii::app()->request->getUserHostAddress();
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
        }
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('gallery_id', $this->gallery_id);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('is_cover', $this->is_cover);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('title_keyword', $this->title_keyword, true);
        $criteria->compare('meta_keyword', $this->meta_keyword, true);
        $criteria->compare('meta_desc', $this->meta_desc, true);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('create_by', $this->create_by, true);
        $criteria->compare('create_ip', $this->create_ip, true);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('update_by', $this->update_by, true);
        $criteria->compare('update_ip', $this->update_ip, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAlbumForIndex() {
        $criteria = new CDbCriteria;
        $criteria->with = array('gallery');
        $criteria->condition = "code='gallery_video' and is_cover = '1'";
        $criteria->addCondition("gallery.status = '1'");

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ), 'pagination' => array(
                'pageSize' => 1,
                'currentPage' => 0,)
        ));
    }

    public function searchCoverByGalleryID($gallery_id) {
        $criteria = new CDbCriteria;

        $criteria->condition = "gallery_id=:gallery_id and is_cover = '1'";
        $criteria->params = array("gallery_id" => $gallery_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id asc',
            ), 'pagination' => array(
                'pageSize' => 1,
                'currentPage' => 0,)
        ));
    }

    public function searchByGalleryID($gallery_id, $pageSize = 999999) {
        $criteria = new CDbCriteria;
        $criteria->with = array('gallery');
        $criteria->condition = "gallery_id=:gallery_id";
        $criteria->params = array("gallery_id" => $gallery_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.is_cover desc,t.id desc',
            ), 'pagination' => array(
                'pageSize' => $pageSize,
                'currentPage' => 0,)
        ));
    }

    public function searchByGalleryIDForManage($gallery_id, $pageSize = 999999) {
        $criteria = new CDbCriteria;
        $criteria->with = array('gallery');
        $criteria->condition = "gallery_id=:gallery_id";
        $criteria->params = array("gallery_id" => $gallery_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ), 'pagination' => array(
                'pageSize' => $pageSize,
                'currentPage' => 0,)
        ));
    }

}
