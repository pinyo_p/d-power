<?php

/**
 * This is the model class for table "tb_banner_intro".
 *
 * The followings are the available columns in table 'tb_banner_intro':
 * @property integer $id
 * @property string $img_src
 * @property integer $sort_order
 * @property integer $status
 * @property string $create_date
 * @property string $create_by
 * @property string $create_ip
 * @property string $update_date
 * @property string $update_by
 * @property string $update_ip
 */
class BannerIntro extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return BannerIntro the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_banner_intro';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sort_order, status', 'numerical', 'integerOnly' => true),
            array('img_src', 'length', 'max' => 250),
            array('create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, img_src, sort_order, status, create_date, create_by, create_ip, update_date, update_by, update_ip', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'img_src' => 'Img Src',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'create_date' => 'Create Date',
            'create_by' => 'Create By',
            'create_ip' => 'Create Ip',
            'update_date' => 'Update Date',
            'update_by' => 'Update By',
            'update_ip' => 'Update Ip',
        );
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = Yii::app()->request->getUserHostAddress();
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
            $this->status = 0;
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
        }
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->addCondition("status != '4' ");
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order asc,id asc',
            ),
        ));
    }

}
