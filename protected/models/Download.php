<?php

class Download extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public $filename;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_download';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public $display_perpage;
    public $current_page;
    public $row_count;

    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type_id, status, download_count', 'numerical', 'integerOnly' => true),
            array('filename_1, filename_2, filename_3, filename_4', 'length', 'max' => 250),
            array('filename_5', 'length', 'max' => 2550),
            array('filesrc, cover', 'length', 'max' => 500),
            array('create_date, create_ip, create_by, update_date, update_ip, update_by', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, type_id, filename_1, filename_2, filename_3, filename_4, filename_5, filesrc, cover, status, download_count, create_date, create_ip, create_by, update_date, update_ip, update_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        /*
          return array(
          'author' => array(self::BELONGS_TO, 'User', 'author_id'),
          'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
          'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
          ); */
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'color' => 'Color Name', 'command' => 'Command'
        );
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    /**
     * Normalizes the user-entered tags.
     */

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {
        if ($this->isNewRecord) {


            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = Yii::app()->request->getUserHostAddress();
            $this->status = 0;
            $this->download_count = 0;
        }
        $this->update_date = date("Y-m-d H:i:s");
        $this->update_by = Yii::app()->user->id;
        $this->update_ip = Yii::app()->request->getUserHostAddress();
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function backend_search() {
        $criteria = new CDbCriteria;

        $condi = "";

        if (trim($this->filename) != "")
            $condi .= " and (filename_1 like '%" . $this->filename . "%' or filename_2 like '%" . $this->filename . "%')";


        $criteria->condition = " type_id='" . $this->type_id . "' $condi ";
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('Download', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

    public function search_by_type_id($type_id) {
        $criteria = new CDbCriteria;


        $criteria->condition = " type_id='" . $type_id . "'";
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('Download', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

    public function search() {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider('Download', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc',
            ),
        ));
    }

}
