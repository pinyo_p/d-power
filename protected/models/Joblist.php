<?php

class Joblist extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */
	public $id;
	public $title;
	public $content;
	public $show_in_menu;
	public $status;
	public $order;


	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_joblist';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					 array('publish_date','length','max'=>45),
					 array('position,education,experience,type','length','max'=>250),
					 array('skill,jobdetail','length','max'=>600000),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'link' => 'Link',
			'sort_order' => 'Order',
		);
	}

	public function getlistByPart($part_name)
	{
		$criteria=new CDbCriteria;
		$criteria->condition = " part='" . $part_name . "'";
		return new CActiveDataProvider('Banner', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sort_order asc',
			),
		));
	}
	/**
	 * Adds a new comment to this post.
	 * This method will set status and post_id of the comment accordingly.
	 * @param Comment the comment to be added
	 * @return boolean whether the comment is saved successfully
	 */
	
	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	 /*
	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}*/

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if($this->isNewRecord)
		{
			$this->create_date = date("Y-m-d H:i:s");
			$this->create_ip = $_SERVER['REMOTE_ADDR'];
			$this->create_by = Yii::app()->user->getId();
			$this->status=0;
		}else{
			$this->update_date = date("Y-m-d H:i:s");
			$this->update_ip = $_SERVER['REMOTE_ADDR'];
			$this->update_by = Yii::app()->user->getId();
		}
		return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	 public function front_search()
	{
		$criteria=new CDbCriteria;

		$criteria->condition = " (publish_date<='" . date("Y-m-d") . "' or publish_date is null or publish_date='')";
		return new CActiveDataProvider('Joblist', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'`id` desc',
			),
		));
	}
	public function search()
	{
		$criteria=new CDbCriteria;


		return new CActiveDataProvider('Joblist', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'`id` desc',
			),
		));
	}
}