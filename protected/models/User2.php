<?php

class User2 extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */ 
	public $username;
	public $password;
	public $rememberMe;
	public $last_login;
	public $cpassword;
	public $key_word;
	public $verifyCode;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function login()
	{
		if($this->username==""){
			$this->addError('username', 'กรุณากรอกชื่อผู้ใช้');
			return false;
		}
		else if($this->password==""){
			$this->addError('password', 'กรุณากรอกรหัสผ่าน');
			return false;
		}
		else{
			$usg = User::model()->find("username='" . $this->username . "' and password='" . $this->password . "'" );
			if(count($usg)>0){
				return $usg;
			}else{
				$this->addError('username', 'Login Fail!!!');
				return false;
			}
		}
		
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					 array('username,password,cpassword,first_name,last_name,address,province,zipcode,mobile_no,email', 'required','message'=>' {attribute} ไม่สามารถเป็นค่าว่างได้'),
					array('username', 'safe', 'on'=>'search'),
				
			array('username,password','length','max'=>128),
			array('first_name,last_name,email,position,','length','max'=>500),
			array('url','length','max'=>250),
			array('address,comment','length','max'=>5000),
			array('mobile_no,fax_no,employee_id','length','max'=>50),
			array('create_date,create_ip,expire_date','length','max'=>50),
			array('department,group_id,status,province,create_by,is_expire,zipcode','length','max'=>10),
			array('username', 'checkDupUser',),
			array('password', 'checkConfirmPass',),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}
	public function checkConfirmPass($attribute,$params)
	 {
			if($this->password!=$this->cpassword){
				$this->addError('cpassword', 'รหัสผ่านไม่ตรงกัน');
			}
	 }
	/**
	 * @return array customized attribute labels (name=>label)
	 */

	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'first_name'=>'ชื่อ',
			'last_name'=>'นามสกุล',
			'address'=>'ที่อยู่',
			'mobile_no'=>'เบอร์โทรศัพท์',
			'fax_no'=>'เบอร์แฟกส์',
			'zipcode'=>'รหัสไปรษณีย์',
			'group_id'=>'กลุ่มผู้ใช้งาน',
			'province'=>'จังหวัด',
			'cpassword'=>'Confirm Password',
			'group.group_name' => 'กลุ่มผู้ใช้งาน','command'=>'Command','comment'=>'หมายเหตุ','expire_date'=>'วันหมดอายุ'
		);
	}

	
	
	public function updateLogin($user)
	{
		return $user->save();
	}
	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	 /*
	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}*/

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		$this->last_login = date("Y-m-d H:i:s");
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->employee_id = 0;
				$this->department = "";
				$this->position = "";
				$this->status= 0;
				$this->last_login_ip = "";
				$this->create_date = date("Y-m-d H:i:s");
				$this->create_ip = $_SERVER['REMOTE_ADDR'];
				$this->create_by = Yii::app()->user->getId();
				if($this->expire_date!="")
					$this->is_expire = 1;
				else
					$this->is_expire = 0;
				/*
				$this->create_time=$this->update_time=time();
				$this->author_id=Yii::app()->user->id;
				
				$this->status=0;
				$this->comment = '';*/
			}
			
			return true;
		}
		else
			return false;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		parent::afterDelete();
		//Comment::model()->deleteAll('post_id='.$this->id);
		//Tag::model()->updateFrequency($this->tags, '');
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	 public function getData()
	 {
		$criteria=new CDbCriteria;
		$criteria->select = array('*');
		
		$data2 = $this->search()->data;
		return $data2;
	 }
	 
	 
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		//'StatusOption'=> array(self::BELONGS_TO, 'Options', '', 'on' => 't.status=option_code AND option_name=\'Status\'',),
		
		/*return array(
			'category' => array(self::BELONGS_TO, 'ProductGroup', 'field', 'primaryKey'=>'id'),
		);*/
		return array(
			'group' => array(self::HAS_ONE, 'UserGroup', array('id' => 'group_id')),
			'p' => array(self::HAS_ONE, 'Provinces', array('province_id' => 'province')),
		);
	}
	 public function checkDupUser($attribute,$params)
	 {
		 $usg = User::model()->find("username='" . $this->username . "' and id<>'" . $this->id . "'");
			if(count($usg)>0){
				$this->addError('username', 'Username นีิได้ถูกใช้งานไปแล้ว');
			}
	 }
	public function search()
	{
		$criteria=new CDbCriteria;
//		$criteria->compare('username',$this->username);
		$condi = " `t`.part='" . $this->part . "'" ;
		if($this->username!="")
			$condi .= " and username like '%" . $this->username . "%'";
		if($this->first_name!="")
			$condi .= " and first_name like '%" . $this->first_name . "%'";
		if($this->last_name!="")
			$condi .= " and last_name like '%" . $this->last_name . "%'";
		if($this->group_id!="")
			$condi .= " and group_id ='" . $this->group_id . "'";
			
		$criteria->condition = $condi;
 		$criteria->with = array('group','p');
    	$criteria->together = true;
		return new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'username asc',
			),
		));
	}
}