<?php

class District extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */
	public $id;
	public $province_id;
	public $thai_name;
	public $eng_name;
	public $continent_id;
	public $continent_id2;
	public $province_id2;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'district';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					 array('thai_name', 'required'),
			array('thai_name,eng_name,','length','max'=>500),
			array('province_id,','length','max'=>40),
			array('thai_name', 'checkDupUser',),
		);
	}
public function checkDupUser($attribute,$params)
	 {
		 $usg = District::model()->find("thai_name='" . $this->thai_name . "' and province_id = '" . $this->province_id . "' and id<>'" . $this->id . "'");
			if(count($usg)>0){
				$this->addError('thai_name', '* อำเภอนี้้ถูกเพิ่มไปแล้ว');
			}
	 }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array(
			'p' => array(self::HAS_ONE, 'Provinces', array('province_id' => 'province_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'province_id' => 'Id',
			'thai_name' => 'Thainame Name','eng_name'=>'Eng Name'
		);
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	 
	 /*
	
	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		
			return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	 public function searchByProvince()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('province_id',$this->province_id);
		$criteria->limit = "100";
		return new CActiveDataProvider('District', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'thai_name asc',
			),
			'pagination' => array(
            'pageSize' => 100,
			'currentPage'=>0,

        ),
		));
	}
	public function search()
	{
		$criteria=new CDbCriteria;

$criteria->limit = "50";
		$criteria->compare('thai_name',$this->group_name);

		return new CActiveDataProvider('District', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'thai_name asc',
			),
		));
	}
}