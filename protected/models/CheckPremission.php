<?php

class Premission extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_premission';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
//        return array(
//            array('menu_id,user_id,assign_by,type', 'length', 'max' => 40),
//        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
//        return array(
//            'menu' => array(self::BELONGS_TO, 'Menu', 'menu_id'),
//            'usergroup' => array(self::BELONGS_TO, 'UserGroup', 'user_id'),
//        );
//        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'title' => 'Title',
        );
    }

    

}
