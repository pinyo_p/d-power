<?php

class FrontAboutSam extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */
	public $id;
	public $title;
	public $content;
	public $show_in_menu;
	public $status;
	public $order;
	public $attach;
	public $row_count;
	public $display_perpage;
	public $current_page;
	public $pic_th;
	public $pic_en;
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_about_sam2';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.

		return array(
			array('title_th, ', 'required','message'=>' {attribute} ไม่สามารถเป็นค่าว่างได้'),

			array('title_th, content_th', 'safe', 'on'=>'search'),
			array('title_th,title_en','length','max'=>250),
			array('show_in_menu,','length','max'=>40),
			array('status,sort_order,parent_id','length','max'=>10),
			array('content_th,content_en,','length','max'=>600000),
			array('publish_date,expire_date,','length','max'=>40),
			array('attr1,attr2,attr3,attr4','length','max'=>45),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'title_th' => 'หัวข้อ',
		);
	}

	public function getlist()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('FrontAboutSam', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sort_order asc',
			),
		));
	}
	/**
	 * Adds a new comment to this post.
	 * This method will set status and post_id of the comment accordingly.
	 * @param Comment the comment to be added
	 * @return boolean whether the comment is saved successfully
	 */
	
	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	 /*
	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}*/

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if($this->need_sync=="")
			$this->need_sync = 1;
		return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	 public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('FrontAboutSam', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sort_order asc',
			),
		));
	}
	public function showError($attr,$msg){
		$this->addError($attr,$msg);
	}
	public function checked()
	{
		$this->checking_date = date("Y-m-d H:i:s");
		$this->checking_ip = $_SERVER['REMOTE_ADDR'];
		$this->checking_by = Yii::app()->user->getId();
		$this->status=1;
		$this->save();
	}
	public function approve()
	{
		$this->approve_date = date("Y-m-d H:i:s");
		$this->approve_ip = $_SERVER['REMOTE_ADDR'];
		$this->approve_by = Yii::app()->user->getId();
		$this->status=2;
		$this->save();
	}
	public function searchByPartAndGroup()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('part',$this->part,false);
		$criteria->compare('`group`',$this->group,false);
		$criteria->offset = $this->current_page *  $this->display_perpage;
		$criteria->compare('title_th',$this->title_th,true);
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('FrontAboutSam', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id desc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:20),
			'currentPage'=>$this->current_page,)
		));
	}
	public function searchByPartAndParent()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('part',$this->part,false);
		$criteria->compare('`parent_id`',$this->parent_id,false);
		$criteria->offset = $this->current_page *  $this->display_perpage;
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('FrontAboutSam', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sort_order asc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:20),
			'currentPage'=>$this->current_page,)
		));
	}
	
	
	public function frontSearchByPart()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('part',$this->part,false);
		$criteria->compare('status',2);
		$criteria->compare('show_in_menu',1,false);
		return new CActiveDataProvider('FrontAboutSam', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sort_order asc,id desc',
			),'pagination' => array(
            'pageSize' => 100,
			'currentPage'=>0,)
		));
	}
	
	public function getActivity($objType)
	{
		$this_year = date("Y");
		$criteria=new CDbCriteria;
		$criteria->compare('attr1',$this_year,true);
		$criteria->compare('attr2',$objType);
		$criteria->compare('`group`',14);
		
		return new CActiveDataProvider('FrontAboutSam', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'attr1 asc',
			),'pagination' => array(
            'pageSize' => 100,
			'currentPage'=>0,)
		));
	}
	
	public function frontSearchByPartAndGroup()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('part',$this->part,false);
		$criteria->compare('`group`',$this->group,false);
		$criteria->compare('status',2);
		
		$criteria->compare('show_in_menu',1,false);
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('FrontAboutSam', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sort_order asc,id desc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:20),
			'currentPage'=>$this->current_page,)
		));
	}
}