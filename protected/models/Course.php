<?php

class Course extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	 public $key_word;
	 public $display_perpage;
	 public $current_page;
	 public $row_count;
	 public $verifyCode;
	 public $start_date;
	 public $end_date;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fullname,company_name,email,tel,company_type,mobile,fax,sale_name,sale_company','length','max'=>500),
			array('training_id,training_date,training_person,status','length','max'=>40),
			array('address','length','max'=>4000),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.

		return array(
			'course' => array(self::HAS_ONE, 'Training', array('id' => 'training_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'color' => 'Color Name','command'=>'Command'
		);
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	 
	

	

	/**
	 * Normalizes the user-entered tags.
	 */


	

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		
			if($this->isNewRecord)
			{
				$this->create_date=date("Y-m-d H:i:s");
				$this->create_by=Yii::app()->user->id;
				$this->create_ip = $_SERVER['REMOTE_ADDR'];
				$this->update_date=date("Y-m-d H:i:s");
				$this->update_by=Yii::app()->user->id;
				$this->update_ip = $_SERVER['REMOTE_ADDR'];
				$this->status=0;
			}
			else
			{
				$this->update_date=date("Y-m-d H:i:s");
				$this->update_by=Yii::app()->user->id;
				$this->update_ip = $_SERVER['REMOTE_ADDR'];
			}
			
return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('Training', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id asc',
			),
		));
	}
	 public function backend_search()
	{
		$criteria=new CDbCriteria;
		
		$condi = "";	

			
		if($this->training_id!="")
			$condi .= " and training_id='" . $this->training_id . "'";
		if($this->fullname!="")
			$condi .= " and fullname like '%" . $this->fullname . "%'";
		if($this->company_name!="")
			$condi .= " and company_name like '%" . $this->company_name . "%'";
		if($this->sale_name!="")
			$condi .= " and sale_name like '%" . $this->sale_name . "%'";	
		if($this->status!="")
			$condi .= " and status='" . $this->status . "'";
			
		if(isset($this->start_date) && $this->start_date!="")
		{
			$condi .= " and create_date>='" . $this->start_date . "'";
		}
		
		if(isset($this->end_date) && $this->end_date!="")
		{
			$condi .= " and create_date<='" . $this->end_date . "'";
		}
		
		$criteria->condition = " 1=1 $condi ";
		$this->row_count = $this->count($criteria);
		return new CActiveDataProvider('Course', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id desc,fullname,company_name asc',
			),
			'pagination' => array(
            'pageSize' => ($this->display_perpage!=""?$this->display_perpage:30),
			'currentPage'=>$this->current_page,),
		));
	}
	public function front_show()
	{
		$criteria=new CDbCriteria;

		return new CActiveDataProvider('Training', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id asc',
			),
		));
	}
}