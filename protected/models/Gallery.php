<?php

/**
 * This is the model class for table "tb_gallery".
 *
 * The followings are the available columns in table 'tb_gallery':
 * @property integer $id
 * @property integer $status
 * @property string $code
 * @property string $name_1
 * @property string $name_2
 * @property string $name_3
 * @property string $name_4
 * @property string $name_5
 * @property string $description_1
 * @property string $description_2
 * @property string $description_3
 * @property string $description_4
 * @property string $description_5
 * @property string $detail_1
 * @property string $detail_2
 * @property string $detail_3
 * @property string $detail_4
 * @property string $detail_5
 * @property string $title_keyword
 * @property string $meta_keyword
 * @property string $meta_desc
 * @property string $publish_date
 * @property string $expired_date
 * @property string $url_friendly
 * @property string $create_date
 * @property string $create_by
 * @property string $create_ip
 * @property string $update_date
 * @property string $update_by
 * @property string $update_ip
 */
class Gallery extends CActiveRecord {

    public $searchName;
    public $display_perpage;
    public $current_page;
    public $row_count;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Gallery the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_gallery';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name_2', 'required','message'=>'ข้อมูล {attribute} ไม่สามารถเป็นค่าว่างได้'),
            array('status,is_index', 'numerical', 'integerOnly' => true),
            array('code', 'length', 'max' => 50),
            array('name_1, name_2, name_3, name_4, name_5, url_friendly', 'length', 'max' => 255),
            array('title_keyword, meta_keyword, meta_desc', 'length', 'max' => 250),
            array('publish_date, expired_date, create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max' => 40),
            array('description_1, description_2, description_3, description_4, description_5, detail_1, detail_2, detail_3, detail_4, detail_5', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, status, code, name_1, name_2, name_3, name_4, name_5, description_1, description_2, description_3, description_4, description_5, detail_1, detail_2, detail_3, detail_4, detail_5, title_keyword, meta_keyword, meta_desc, publish_date, expired_date, url_friendly, create_date, create_by, create_ip, update_date, update_by, update_ip', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'status' => 'Status',
            'code' => 'Code',
            'code' => 'Is Index',
            'name_1' => 'ชื่ออัลบั้ม (ภาษาอังกฤษ)',
            'name_2' => 'ชื่ออัลบั้ม (ภาษาไทย)',
            'name_3' => 'Name 3',
            'name_4' => 'Name 4',
            'name_5' => 'Name 5',
            'description_1' => 'Description 1',
            'description_2' => 'Description 2',
            'description_3' => 'Description 3',
            'description_4' => 'Description 4',
            'description_5' => 'Description 5',
            'detail_1' => 'Detail 1',
            'detail_2' => 'Detail 2',
            'detail_3' => 'Detail 3',
            'detail_4' => 'Detail 4',
            'detail_5' => 'Detail 5',
            'title_keyword' => 'Title Keyword',
            'meta_keyword' => 'Meta Keyword',
            'meta_desc' => 'Meta Desc',
            'publish_date' => 'Publish Date',
            'expired_date' => 'Expired Date',
            'url_friendly' => 'Url Friendly',
            'create_date' => 'Create Date',
            'create_by' => 'Create By',
            'create_ip' => 'Create Ip',
            'update_date' => 'Update Date',
            'update_by' => 'Update By',
            'update_ip' => 'Update Ip',
        );
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = Yii::app()->request->getUserHostAddress();
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
        }
        return true;
    }
    
    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('status', $this->status);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('is_index', $this->is_index);
        $criteria->compare('name_1', $this->name_1, true);
        $criteria->compare('name_2', $this->name_2, true);
        $criteria->compare('name_3', $this->name_3, true);
        $criteria->compare('name_4', $this->name_4, true);
        $criteria->compare('name_5', $this->name_5, true);
        $criteria->compare('description_1', $this->description_1, true);
        $criteria->compare('description_2', $this->description_2, true);
        $criteria->compare('description_3', $this->description_3, true);
        $criteria->compare('description_4', $this->description_4, true);
        $criteria->compare('description_5', $this->description_5, true);
        $criteria->compare('detail_1', $this->detail_1, true);
        $criteria->compare('detail_2', $this->detail_2, true);
        $criteria->compare('detail_3', $this->detail_3, true);
        $criteria->compare('detail_4', $this->detail_4, true);
        $criteria->compare('detail_5', $this->detail_5, true);
        $criteria->compare('title_keyword', $this->title_keyword, true);
        $criteria->compare('meta_keyword', $this->meta_keyword, true);
        $criteria->compare('meta_desc', $this->meta_desc, true);
        $criteria->compare('publish_date', $this->publish_date, true);
        $criteria->compare('expired_date', $this->expired_date, true);
        $criteria->compare('url_friendly', $this->url_friendly, true);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('create_by', $this->create_by, true);
        $criteria->compare('create_ip', $this->create_ip, true);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('update_by', $this->update_by, true);
        $criteria->compare('update_ip', $this->update_ip, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ), 'pagination' => array(
                'pageSize' => 999999,
                'currentPage' => 0,)
        ));
    }
    
    public function searchGalleryPictureForManage() {
        $criteria = new CDbCriteria;

        $condi = "";
        if (trim($this->searchName) != ""){
            $condi .= " and (";
            $condi .= "     name_1 like '%".$this->searchName."%'";
            $condi .= "     or name_2 like '%".$this->searchName."%'";
            $condi .= "     or name_3 like '%".$this->searchName."%'";
            $condi .= "     or name_4 like '%".$this->searchName."%'";
            $condi .= "     or name_5 like '%".$this->searchName."%'";
            $condi .= " )";
        }

        $criteria->condition = " code='gallery_image' ".$condi;
        $this->row_count = $this->count($criteria);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }
    
    public function searchGalleryVideoForManage() {
        $criteria = new CDbCriteria;

        $condi = "";
        if (trim($this->searchName) != ""){
            $condi .= " and (";
            $condi .= "     name_1 like '%".$this->searchName."%'";
            $condi .= "     or name_2 like '%".$this->searchName."%'";
            $condi .= "     or name_3 like '%".$this->searchName."%'";
            $condi .= "     or name_4 like '%".$this->searchName."%'";
            $condi .= "     or name_5 like '%".$this->searchName."%'";
            $condi .= " )";
        }

        $criteria->condition = " code='gallery_video' ".$condi;
        $this->row_count = $this->count($criteria);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }
    
    

}
