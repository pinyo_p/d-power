<?php

class Jobbee extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */
	public $id;
	public $title;
	public $content;
	public $show_in_menu;
	public $status;
	public $order;
	public $start_date;
	public $end_date;
	public $key_word;
	public $verifyCode;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_jobbee';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
					 array('fullname,email,age,phoneno,sex,jobid', 'required','message'=>' {attribute} ไม่สามารถเป็นค่าว่างได้'),
					 array('resume', 'required','message'=>'กรุณาอับโหลด {attribute} '),
					 array('age,phoneno','length','max'=>45),
					 array('fullname,email,','length','max'=>250),
					 array('sex,jobid','length','max'=>11),
					 array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
					 array('email', 'email','checkMX'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		
		return array(
			'jobs' => array(self::HAS_ONE, 'Joblist',array( 'id'=>'jobid')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'link' => 'Link',
			'sort_order' => 'Order',
			'verifyCode'=>'Verification Code',
		);
	}

	/**
	 * Adds a new comment to this post.
	 * This method will set status and post_id of the comment accordingly.
	 * @param Comment the comment to be added
	 * @return boolean whether the comment is saved successfully
	 */
	
	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	 /*
	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}*/

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{

			if($this->isNewRecord)
			{
				$this->create_date = date("Y-m-d H:i:s");
				$this->create_ip = $_SERVER['REMOTE_ADDR'];
				$this->status=0;
			}

			return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$condi = "";
		if(isset($this->jobid) && $this->jobid!="")
		{
			$condi .= " and jobid='" . $this->jobid . "'";
		}
		if(isset($this->start_date) && $this->start_date!="")
		{
			$condi .= " and create_date>='" . $this->start_date . "'";
		}
		if(isset($this->end_date) && $this->end_date!="")
		{
			$condi .= " and create_date<='" . $this->end_date . "'";
		}
		if(isset($this->status) && $this->status!="")
		{
			$condi .= " and status='" . $this->status . "'";
		}
		
		if(isset($this->key_word) && $this->key_word!="")
		{
			$condi .= " and (fullname like '%" . $this->key_word . "%' or email like '%" . $this->key_word . "%' or phoneno like '%" . $this->key_word . "%')";
		}
		
		$criteria->condition = " 1=1  $condi";
		return new CActiveDataProvider('Jobbee', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'`status` asc,`id` desc',
			),
			'pagination' => array(
            'pageSize' => 40,
			'currentPage'=>1,),
		));
	}
}