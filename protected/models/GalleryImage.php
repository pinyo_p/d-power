<?php

/**
 * This is the model class for table "tb_gallery_img".
 *
 * The followings are the available columns in table 'tb_gallery_img':
 * @property integer $id
 * @property string $gallery_id
 * @property string $file_name
 * @property string $file_type
 * @property integer $is_cover
 * @property integer $is_index
 * @property string $name_1
 * @property string $name_2
 * @property string $name_3
 * @property string $name_4
 * @property string $name_5
 * @property string $description_1
 * @property string $description_2
 * @property string $description_3
 * @property string $description_4
 * @property string $description_5
 * @property string $detail_1
 * @property string $detail_2
 * @property string $detail_3
 * @property string $detail_4
 * @property string $detail_5
 * @property string $title_keyword
 * @property string $meta_keyword
 * @property string $meta_desc
 * @property string $url_friendly
 * @property string $create_date
 * @property string $create_by
 * @property string $create_ip
 * @property string $update_date
 * @property string $update_by
 * @property string $update_ip
 */
class GalleryImage extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GalleryImage the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_gallery_img';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('is_cover', 'numerical', 'integerOnly' => true),
            array('gallery_id', 'length', 'max' => 11),
            array('file_name, name_1, name_2, name_3, name_4, name_5, url_friendly', 'length', 'max' => 255),
            array('file_type, create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max' => 40),
            array('title_keyword, meta_keyword, meta_desc', 'length', 'max' => 250),
            array('description_1, description_2, description_3, description_4, description_5, detail_1, detail_2, detail_3, detail_4, detail_5', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, gallery_id, file_name, file_type, is_cover, is_index, name_1, name_2, name_3, name_4, name_5, description_1, description_2, description_3, description_4, description_5, detail_1, detail_2, detail_3, detail_4, detail_5, title_keyword, meta_keyword, meta_desc, url_friendly, create_date, create_by, create_ip, update_date, update_by, update_ip', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'gallery' => array(self::BELONGS_TO, 'Gallery', array('gallery_id' => 'id')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'gallery_id' => 'Gallery',
            'file_name' => 'File Name',
            'file_type' => 'File Type',
            'is_cover' => 'Is Cover',
            'name_1' => 'Name 1',
            'name_2' => 'Name 2',
            'name_3' => 'Name 3',
            'name_4' => 'Name 4',
            'name_5' => 'Name 5',
            'description_1' => 'Description 1',
            'description_2' => 'Description 2',
            'description_3' => 'Description 3',
            'description_4' => 'Description 4',
            'description_5' => 'Description 5',
            'detail_1' => 'Detail 1',
            'detail_2' => 'Detail 2',
            'detail_3' => 'Detail 3',
            'detail_4' => 'Detail 4',
            'detail_5' => 'Detail 5',
            'title_keyword' => 'Title Keyword',
            'meta_keyword' => 'Meta Keyword',
            'meta_desc' => 'Meta Desc',
            'url_friendly' => 'Url Friendly',
            'create_date' => 'Create Date',
            'create_by' => 'Create By',
            'create_ip' => 'Create Ip',
            'update_date' => 'Update Date',
            'update_by' => 'Update By',
            'update_ip' => 'Update Ip',
        );
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = Yii::app()->request->getUserHostAddress();
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
        }
        return true;
    }
    
    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }
    
    public function searchAlbumForIndex() {
        $criteria = new CDbCriteria;
        $criteria->with = array('gallery');
        $criteria->condition = "code='gallery_image' and is_cover = '1'";
        $criteria->addCondition("gallery.status = '1' and gallery.is_index = '1'");

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ), 'pagination' => array(
                'pageSize' => 9,
                'currentPage' => 0,)
        ));
    }

    public function searchCoverByGalleryID($gallery_id) {
        $criteria = new CDbCriteria;

        $criteria->condition = "gallery_id=:gallery_id and is_cover = '1'";
        $criteria->params = array("gallery_id" => $gallery_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id asc',
            ), 'pagination' => array(
                'pageSize' => 1,
                'currentPage' => 0,)
        ));
    }
    
    public function searchByGalleryID($gallery_id,$pageSize = 999999) {
        $criteria = new CDbCriteria;
        $criteria->with = array('gallery');
        $criteria->condition = "gallery_id=:gallery_id";
        $criteria->params = array("gallery_id" => $gallery_id);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.is_cover desc,t.id desc',
            ), 'pagination' => array(
                'pageSize' => $pageSize,
                'currentPage' => 0,)
        ));
    }
    
    public function searchByGalleryIDForManage($gallery_id,$pageSize = 999999) {
        $criteria = new CDbCriteria;
        $criteria->with = array('gallery');
        $criteria->condition = "gallery_id=:gallery_id";
        $criteria->params = array("gallery_id" => $gallery_id);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ), 'pagination' => array(
                'pageSize' => $pageSize,
                'currentPage' => 0,)
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('gallery_id', $this->gallery_id, true);
        $criteria->compare('file_name', $this->file_name, true);
        $criteria->compare('file_type', $this->file_type, true);
        $criteria->compare('is_cover', $this->is_cover);
        $criteria->compare('name_1', $this->name_1, true);
        $criteria->compare('name_2', $this->name_2, true);
        $criteria->compare('name_3', $this->name_3, true);
        $criteria->compare('name_4', $this->name_4, true);
        $criteria->compare('name_5', $this->name_5, true);
        $criteria->compare('description_1', $this->description_1, true);
        $criteria->compare('description_2', $this->description_2, true);
        $criteria->compare('description_3', $this->description_3, true);
        $criteria->compare('description_4', $this->description_4, true);
        $criteria->compare('description_5', $this->description_5, true);
        $criteria->compare('detail_1', $this->detail_1, true);
        $criteria->compare('detail_2', $this->detail_2, true);
        $criteria->compare('detail_3', $this->detail_3, true);
        $criteria->compare('detail_4', $this->detail_4, true);
        $criteria->compare('detail_5', $this->detail_5, true);
        $criteria->compare('title_keyword', $this->title_keyword, true);
        $criteria->compare('meta_keyword', $this->meta_keyword, true);
        $criteria->compare('meta_desc', $this->meta_desc, true);
        $criteria->compare('url_friendly', $this->url_friendly, true);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('create_by', $this->create_by, true);
        $criteria->compare('create_ip', $this->create_ip, true);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('update_by', $this->update_by, true);
        $criteria->compare('update_ip', $this->update_ip, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
