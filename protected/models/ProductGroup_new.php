<?php

/**
 * This is the model class for table "tb_product_group".
 *
 * The followings are the available columns in table 'tb_product_group':
 * @property integer $id
 * @property integer $brand_id
 * @property string $logo
 * @property string $group_1
 * @property string $group_2
 * @property string $group_3
 * @property string $group_4
 * @property string $group_5
 * @property string $group_desc_1
 * @property string $group_desc_2
 * @property string $group_desc_3
 * @property string $group_desc_4
 * @property string $group_desc_5
 * @property string $create_date
 * @property string $create_by
 * @property string $create_ip
 * @property string $update_date
 * @property string $update_by
 * @property string $update_ip
 * @property integer $sort_order
 */
class ProductGroup_new extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProductGroup_new the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_product_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brand_id, sort_order', 'numerical', 'integerOnly'=>true),
			array('logo, group_1, group_2, group_3, group_4, group_5', 'length', 'max'=>250),
			array('group_desc_1, group_desc_2, group_desc_3, group_desc_4, group_desc_5', 'length', 'max'=>1000),
			array('create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max'=>40),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, brand_id, logo, group_1, group_2, group_3, group_4, group_5, group_desc_1, group_desc_2, group_desc_3, group_desc_4, group_desc_5, create_date, create_by, create_ip, update_date, update_by, update_ip, sort_order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'brand_id' => 'Brand',
			'logo' => 'Logo',
			'group_1' => 'Group 1',
			'group_2' => 'Group 2',
			'group_3' => 'Group 3',
			'group_4' => 'Group 4',
			'group_5' => 'Group 5',
			'group_desc_1' => 'Group Desc 1',
			'group_desc_2' => 'Group Desc 2',
			'group_desc_3' => 'Group Desc 3',
			'group_desc_4' => 'Group Desc 4',
			'group_desc_5' => 'Group Desc 5',
			'create_date' => 'Create Date',
			'create_by' => 'Create By',
			'create_ip' => 'Create Ip',
			'update_date' => 'Update Date',
			'update_by' => 'Update By',
			'update_ip' => 'Update Ip',
			'sort_order' => 'Sort Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('brand_id',$this->brand_id);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('group_1',$this->group_1,true);
		$criteria->compare('group_2',$this->group_2,true);
		$criteria->compare('group_3',$this->group_3,true);
		$criteria->compare('group_4',$this->group_4,true);
		$criteria->compare('group_5',$this->group_5,true);
		$criteria->compare('group_desc_1',$this->group_desc_1,true);
		$criteria->compare('group_desc_2',$this->group_desc_2,true);
		$criteria->compare('group_desc_3',$this->group_desc_3,true);
		$criteria->compare('group_desc_4',$this->group_desc_4,true);
		$criteria->compare('group_desc_5',$this->group_desc_5,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('create_ip',$this->create_ip,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('update_ip',$this->update_ip,true);
		$criteria->compare('sort_order',$this->sort_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}