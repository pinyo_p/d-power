<?php

class Product extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public $product_name;
    public $product_desc;
    public $product_code;
    public $display_perpage;
    public $current_page;
    public $row_count;
    public $show_highlight;
    public $group_name;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_product';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('product_name_2', 'required','message'=>'ข้อมูล {attribute} ไม่สามารถเป็นค่าว่างได้'),
            array('product_name_1,product_name_2,product_name_3,product_name_4,product_name_5,product_code,product_code2', 'length', 'max' => 250),
            array('product_desc_1,product_desc_2,product_desc_3,product_desc_4,product_desc_5', 'length', 'max' => 600000),
            array('sale_name', 'length', 'max' => 500),
            array('price1,amount1,price2,amount2,price3,amount3,price4,amount4,stock,is_highlight,sort_order,brand_id,group_id,serie_id,is_new,weight_kg', 'length', 'max' => 40),
            array('is_fresh_type,is_for_restaurant', 'length', 'max' => 1),
        );
    }



    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'color' => 'Color Name', 'command' => 'Command',
            'product_name_2' => 'ชื่อสินค้า (ภาษาไทย)',
            'product_code' => 'รหัสสินค้า',
        );
    }

        /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'brand' => array(self::HAS_ONE, 'Brand', array('id' => 'brand_id')),
            'group' => array(self::HAS_ONE, 'ProductGroup', array('id' => 'group_id')),
            //'carts' => array(self::HAS_MANY, 'Cart', array('product_id' => 'id')),
            /*'cartSum' => array(self::STAT, 'Order', 'product_id',
                'select'=> 'SUM(amount)',
                'condition'=>'amount>0'),*/
            
        );
    }
    
    /**
     * @return string the URL that shows the detail of the post
     */
    /**
     * Normalizes the user-entered tags.
     */

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = $_SERVER['REMOTE_ADDR'];
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = $_SERVER['REMOTE_ADDR'];
            $this->status = 0;
            $this->total_shipping = 0;
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = $_SERVER['REMOTE_ADDR'];
        }
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function front_group_search() {
        $criteria = new CDbCriteria;

        $condi = "";
        if (trim($this->brand_id) != "")
            $condi .= " and brand_id = '" . $this->brand_id . "'";
        if (trim($this->group_id) != "")
            $condi .= " and group_id = '" . $this->group_id . "'";
        if (trim($this->is_highlight != ""))
            $condi .= " and is_highlight = '" . $this->is_highlight . "'";

        $criteria->condition = " status=1 $condi ";
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

    public function front_serie_search() {
        $criteria = new CDbCriteria;

        $condi = "";
        if (trim($this->brand_id) != "")
            $condi .= " and brand_id = '" . $this->brand_id . "'";
        if (trim($this->group_id) != "")
            $condi .= " and group_id = '" . $this->group_id . "'";
        if (trim($this->serie_id) != "")
            $condi .= " and serie_id = '" . $this->serie_id . "'";
        $criteria->condition = " status=1 $condi ";
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

    public function backend_search() {
        $criteria = new CDbCriteria;

        $condi = "";
        if (trim($this->product_name) != "")
            $condi .= " and (product_name_1 like '%" . $this->product_name . "%' or product_name_2 like '%" . $this->product_name . "%')";
        if (trim($this->product_code) != "")
            $condi .= " and (product_code like '%" . $this->product_code . "%' or product_code2 like '%" . $this->product_code . "%')";
        if (trim($this->status) != "")
            $condi .= " and status = '" . $this->status . "'";
        if (trim($this->brand_id) != "")
            $condi .= " and brand_id = '" . $this->brand_id . "'";
        if (trim($this->group_id) != "")
            $condi .= " and group_id = '" . $this->group_id . "'";
        if (trim($this->serie_id) != "")
            $condi .= " and serie_id = '" . $this->serie_id . "'";
        $criteria->condition = " 1=1 $condi ";
        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

    public function search_highlight() {
        $criteria = new CDbCriteria;
        $criteria->condition = " status=1 and is_highlight=1 ";
        return new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc',
            ),
        ));
    }

    public function index_search_highlight() {
        $criteria = new CDbCriteria;
        $criteria->condition = " status=1 and is_highlight=1 ";
        return new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc',
            ),
            'pagination' => array(
                'pageSize' => 5,
                'currentPage' => 0,),
        ));
    }
    
    public function search_best_seller_for_index(){
        $criteria = new CDbCriteria;
        $criteria->condition = "status=1 and total_shipping > 0";

        return new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.total_shipping desc',
            )
            ,
            'pagination' => array(
                'pageSize' => 5,
                'currentPage' => 0,),
        ));
    }
    
    public function search_best_seller_for_productgroup(){
        $criteria = new CDbCriteria;
        $criteria->condition = "status=1 and total_shipping > 0";

        return new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.total_shipping desc',
            )
            ,
            'pagination' => array(
                'pageSize' => 10,
                'currentPage' => 0,),
        ));
    }

    public function search_last() {
        $criteria = new CDbCriteria;
        $criteria->condition = " status=1 and is_new=1";
        return new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'update_date desc',
            )
            ,
            'pagination' => array(
                'pageSize' => 8,
                'currentPage' => 1,),
        ));
    }

    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('group_id',$this->group_id);
        $criteria->compare('status', 1);
		$this->row_count = $this->count($criteria);
        return new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
    }

}
