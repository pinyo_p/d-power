<?php

/**
 * This is the model class for table "tb_shipment_rate".
 *
 * The followings are the available columns in table 'tb_shipment_rate':
 * @property integer $id
 * @property string $kg_from
 * @property string $kg_to
 * @property integer $fee_rate
 */
class ShipmentRate extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ShipmentRate the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_shipment_rate';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('kg_from, fee_rate', 'required','message'=>'ข้อมูล {attribute} ไม่สามารถเป็นค่าว่างได้'),
            array('fee_rate', 'numerical', 'integerOnly' => true),
            array('kg_from, kg_to', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, kg_from, kg_to, fee_rate', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'kg_from' => 'จากน้ำหนัก (กก.)',
            'kg_to' => 'ถึงน้ำหนัก (กก.)',
            'fee_rate' => 'อัตราค่าขนส่ง',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('kg_from', $this->kg_from, true);
        $criteria->compare('kg_to', $this->kg_to, true);
        $criteria->compare('fee_rate', $this->fee_rate);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAll() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
}
