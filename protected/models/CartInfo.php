<?php

/**
 * This is the model class for table "tb_cart_info".
 *
 * The followings are the available columns in table 'tb_cart_info':
 * @property integer $id
 * @property string $session_id
 * @property integer $shipping_fee
 */
class CartInfo extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CartInfo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_cart_info';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('shipping_fee, order_id, box_weight, product_weight, total_weight', 'numerical', 'integerOnly' => true),
            array('session_id', 'length', 'max' => 250),
            array('shipping_type', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, session_id, shipping_fee, order_id, box_weight, product_weight, total_weight, shipping_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'session_id' => 'Session',
            'shipping_fee' => 'Shipping Fee',
            'order_id' => 'Order ID',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('session_id', $this->session_id, true);
        $criteria->compare('shipping_fee', $this->shipping_fee);
        $criteria->compare('order_id', $this->order_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchShippingFee($session_id) {
        $criteria = new CDbCriteria;

        $criteria->compare('session_id', $session_id);
        $criteria->compare('order_id', 0);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function searchByOrderID($order_id) {
        $criteria = new CDbCriteria;

        $criteria->compare('order_id', $order_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
