<?php

/**
 * This is the model class for table "tb_shipping_cost".
 *
 * The followings are the available columns in table 'tb_shipping_cost':
 * @property integer $id
 * @property string $shipping_type
 * @property integer $weight_from
 * @property integer $weight_to
 * @property string $cost_price
 */
class ShippingCost extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ShippingCost the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_shipping_cost';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('shipping_type, weight_from, weight_to, cost_price', 'required','message'=>'ข้อมูล {attribute} ไม่สามารถเป็นค่าว่างได้'),
            array('weight_from, weight_to,sort_order', 'numerical', 'integerOnly' => true),
            array('shipping_type', 'length', 'max' => 50),
            array('cost_price,sort_order', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, shipping_type, weight_from, weight_to, cost_price,sort_order', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'shipping_type' => 'Shipping Type',
            'weight_from' => 'จากน้ำหนัก',
            'weight_to' => 'ถึงน้ำหนัก',
            'cost_price' => 'อัตราค่าขนส่ง',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('shipping_type', $this->shipping_type, true);
        $criteria->compare('weight_from', $this->weight_from);
        $criteria->compare('weight_to', $this->weight_to);
        $criteria->compare('cost_price', $this->cost_price, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function searchByType($type) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->addCondition("shipping_type = '".$type."' ");
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order asc',
            ),
            'pagination' => array(
                'pageSize' => 99999,
                'currentPage' => 0),
        ));
    }

}
