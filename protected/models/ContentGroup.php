<?php

class ContentGroup extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */
	const STATUS_DRAFT=1;
	const STATUS_PUBLISHED=2;
	const STATUS_ARCHIVED=3;

	private $_oldTags;
	public $group_name;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_content_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_name_th', 'required','message'=>' {attribute} ไม่สามารถเป็นค่าว่างได้'),
			array('group_name_en,','length','max'=>250),
			array('group_name_th,group_name_en', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'group_name_th' => 'กลุ่มเนื้อหา','group_name_en'=>'กลุ่มเนื้อหา (ภาษาอังกฤษ)','command'=>'Command'
		);
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	 
	 /*
	public function getUrl()
	{
		return Yii::app()->createUrl('post/view', array(
			'id'=>$this->id,
			'title'=>$this->title,
		));
	}
	*/

	/**
	 * @return array a list of links that point to the post list filtered by every tag of this post
	 */
	 
	 /*
	public function getTagLinks()
	{
		$links=array();
		foreach(Tag::string2array($this->tags) as $tag)
			$links[]=CHtml::link(CHtml::encode($tag), array('post/index', 'tag'=>$tag));
		return $links;
	}
	*/

	/**
	 * Normalizes the user-entered tags.
	 */
	public function normalizeTags($attribute,$params)
	{
		$this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
	}

	/**
	 * Adds a new comment to this post.
	 * This method will set status and post_id of the comment accordingly.
	 * @param Comment the comment to be added
	 * @return boolean whether the comment is saved successfully
	 */
	
	public function addGroup($group)
	{
		/*if(Yii::app()->params['commentNeedApproval'])
			$comment->status=Comment::STATUS_PENDING;
		else
			$comment->status=Comment::STATUS_APPROVED;
		$comment->post_id=$this->id;*/
		
		return $group->save();
	}
	public function updateGroup($group)
	{
		return $group->update();
	}
	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	 /*
	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}*/

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	 
	 public function checkDupUser($attribute,$params)
	 {
		 $usg = UserGroup::model()->find("group_name='" . $this->group_name . "'");
			if(count($usg)>0){
				$this->addError('group_name', 'ชื่อกลุ่มนี้ได้ถูกใช้งานไปแล้ว');
			}
	 }
	protected function beforeSave()
	{
		
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				/*
				$this->create_time=$this->update_time=time();
				$this->author_id=Yii::app()->user->id;
				*/
				$this->create_date = date("Y-m-d H:i:s");
				$this->create_ip = $_SERVER['REMOTE_ADDR'];
				$this->create_by = Yii::app()->user->getId();
			}else{
				$this->update_date = date("Y-m-d H:i:s");
				$this->update_ip = $_SERVER['REMOTE_ADDR'];
				$this->update_by = Yii::app()->user->getId();

			}
			return true;
		}
		else
			return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;


		$criteria->compare('group_name',$this->group_name);

		return new CActiveDataProvider('UserGroup', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'group_name asc',
			),
		));
	}
	public function searchByPart()
	{
		$criteria=new CDbCriteria;


		$criteria->compare('part', Yii::app()->user->getValue("part"));

		return new CActiveDataProvider('UserGroup', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'group_name asc',
			),
		));
	}
	public function searchByPartAndParent()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('part',$this->part,false);
		$criteria->compare('`parent_id`',$this->parent_id,false);
		return new CActiveDataProvider('ContentGroup', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sort_order asc',
			),
		));
	}
}