<?php

class Branch extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_branch';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('name_2', 'required'),
            array('sort_order, status', 'numerical', 'integerOnly' => true),
            array('name_1, name_2, name_3, name_4, name_5, main_image, map_image', 'length', 'max' => 500),
            array('address_1, address_2', 'length', 'max' => 4000),
            array('phone_no, fax_no, latitude, longitude, create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max' => 45),
            array('email', 'length', 'max' => 250),
            array('address_3, address_4, address_5', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name_1, name_2, name_3, name_4, name_5, address_1, address_2, address_3, address_4, address_5, phone_no, fax_no, email, main_image, map_image, latitude, longitude, create_date, create_by, create_ip, update_date, update_by, update_ip, sort_order, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        /*
          return array(
          'author' => array(self::BELONGS_TO, 'User', 'author_id'),
          'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
          'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
          ); */
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name_1' => 'Name 1',
            'name_2' => 'Name 2',
            'name_3' => 'Name 3',
            'name_4' => 'Name 4',
            'name_5' => 'Name 5',
            'address_1' => 'Address 1',
            'address_2' => 'Address 2',
            'address_3' => 'Address 3',
            'address_4' => 'Address 4',
            'address_5' => 'Address 5',
            'phone_no' => 'Phone No',
            'fax_no' => 'Fax No',
            'email' => 'Email',
            'main_image' => 'Main Image',
            'map_image' => 'Map Image',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'create_date' => 'Create Date',
            'create_by' => 'Create By',
            'create_ip' => 'Create Ip',
            'update_date' => 'Update Date',
            'update_by' => 'Update By',
            'update_ip' => 'Update Ip',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
        );
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    /**
     * Normalizes the user-entered tags.
     */

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {


        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = $_SERVER['REMOTE_ADDR'];
            $this->status = 0;
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = $_SERVER['REMOTE_ADDR'];
        }
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function search() {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider('Branch', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc',
            ),
        ));
    }

}
