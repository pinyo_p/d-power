<?php

/**
 * This is the model class for table "tb_config".
 *
 * The followings are the available columns in table 'tb_config':
 * @property integer $id
 * @property string $cfg_group
 * @property string $cfg_code
 * @property string $cfg_value
 * @property string $cfg_description
 */
class Config extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Config the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_config';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cfg_group, cfg_code', 'length', 'max' => 200),
            array('cfg_value, cfg_description', 'length', 'max' => 1000),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, cfg_group, cfg_code, cfg_value, cfg_description', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'cfg_group' => 'Cfg Group',
            'cfg_code' => 'Cfg Code',
            'cfg_value' => 'Cfg Value',
            'cfg_description' => 'Cfg Description',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('cfg_group', $this->cfg_group, true);
        $criteria->compare('cfg_code', $this->cfg_code, true);
        $criteria->compare('cfg_value', $this->cfg_value, true);
        $criteria->compare('cfg_description', $this->cfg_description, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function searchByCode($groupCode) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('cfg_code', $groupCode, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
