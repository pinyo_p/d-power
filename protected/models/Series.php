<?php

class Series extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_post':
	 * @var integer $id
	 * @var string $title
	 * @var string $content
	 * @var string $tags
	 * @var integer $status
	 * @var integer $create_time
	 * @var integer $update_time
	 * @var integer $author_id
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	 public $brand_id;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_series';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('name_en,name_th','length','max'=>250),
			array('group_id,sort_order','length','max'=>40),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		/*
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
			'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);*/
		return array(
			'group' => array(self::HAS_ONE, 'ProductGroup', array('id' => 'group_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'color' => 'Color Name','command'=>'Command'
		);
	}

	/**
	 * @return string the URL that shows the detail of the post
	 */
	 
	 /*
	public function getUrl()
	{
		return Yii::app()->createUrl('post/view', array(
			'id'=>$this->id,
			'title'=>$this->title,
		));
	}
	*/

	/**
	 * @return array a list of links that point to the post list filtered by every tag of this post
	 */
	 
	 /*
	public function getTagLinks()
	{
		$links=array();
		foreach(Tag::string2array($this->tags) as $tag)
			$links[]=CHtml::link(CHtml::encode($tag), array('post/index', 'tag'=>$tag));
		return $links;
	}
	*/

	/**
	 * Normalizes the user-entered tags.
	 */
	public function normalizeTags($attribute,$params)
	{
		$this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
	}

	protected function beforeSave()
	{
		$this->update_date=date("Y-m-d H:i:s");
				$this->update_by=Yii::app()->user->id;
				$this->update_ip = $_SERVER['REMOTE_ADDR'];

			return true;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		/*
		parent::afterDelete();
		Comment::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrequency($this->tags, '');
		*/
	}

	/**
	 * Retrieves the list of posts based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	
	public function getDataByBrandAndGroupId()
	{
		$criteria = new CDbCriteria();
		$criteria->select = "serie_id";
		$criteria->compare('brand_id',$this->brand_id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->group = "serie_id";
		$data = Product::model()->findAll($criteria);
		$group = array();
		$serie = array();
		foreach($data as $row)
		{
			if(trim($row->serie_id)!="")
				$serie[] = $row->serie_id;
		}
		if(count($serie)>0)
			$serie_in = implode(',',$serie);
		else
			$serie_in = "-1";
		
		
		$criteria=new CDbCriteria;
		$criteria->condition="id in (" . $serie_in . ")";
		return new CActiveDataProvider('Series', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'sort_order asc',
			),
			'pagination' => array(
            'pageSize' => 200,
			'currentPage'=>0,),
		));
	}
	
	public function search()
	{
		$criteria=new CDbCriteria;
	
		$condi = "";
		if($this->brand_id != "")
			$condi = " and group.brand_id='" . $this->brand_id . "'";
		if($this->group_id != "")
			$condi = " and t.group_id='" . $this->group_id . "'";
		
		$criteria->condition=" 1=1 $condi ";
		$criteria->with = array('group');
		return new CActiveDataProvider('Series', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'t.sort_order asc',
			),
			'pagination' => array(
            'pageSize' => 200,
			'currentPage'=>0,),
		));
	}

}