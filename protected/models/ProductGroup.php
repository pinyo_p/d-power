<?php

class ProductGroup extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public $brand_id;
    public $display_perpage;
    public $current_page;
    public $row_count;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tb_product_group';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('brand_id, sort_order,parent_id', 'numerical', 'integerOnly' => true),
            array('logo, group_1, group_2, group_3, group_4, group_5', 'length', 'max' => 250),
            array('group_desc_1, group_desc_2, group_desc_3, group_desc_4, group_desc_5', 'length', 'max' => 1000),
            array('create_date, create_by, create_ip, update_date, update_by, update_ip', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, brand_id, logo, group_1, group_2, group_3, group_4, group_5, group_desc_1, group_desc_2, group_desc_3, group_desc_4, group_desc_5, create_date, create_by, create_ip, update_date, update_by, update_ip, sort_order,parent_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        /*
          return array(
          'author' => array(self::BELONGS_TO, 'User', 'author_id'),
          'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition'=>'comments.status='.Comment::STATUS_APPROVED, 'order'=>'comments.create_time DESC'),
          'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
          ); */
        return array(
            'brand' => array(self::HAS_ONE, 'Brand', array('id' => 'brand_id')),
            'parent_product_group' => array(self::BELONGS_TO,'ProductGroup',array('parent_id' => 'id'))
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'color' => 'Color Name', 'command' => 'Command'
        );
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    /*
      public function getUrl()
      {
      return Yii::app()->createUrl('post/view', array(
      'id'=>$this->id,
      'title'=>$this->title,
      ));
      }
     */

    /**
     * @return array a list of links that point to the post list filtered by every tag of this post
     */
    /*
      public function getTagLinks()
      {
      $links=array();
      foreach(Tag::string2array($this->tags) as $tag)
      $links[]=CHtml::link(CHtml::encode($tag), array('post/index', 'tag'=>$tag));
      return $links;
      }
     */

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags($attribute, $params) {
        $this->tags = Tag::array2string(array_unique(Tag::string2array($this->tags)));
    }

    /**
     * Adds a new comment to this post.
     * This method will set status and post_id of the comment accordingly.
     * @param Comment the comment to be added
     * @return boolean whether the comment is saved successfully
     */
    public function addGroup($group) {
        /* if(Yii::app()->params['commentNeedApproval'])
          $comment->status=Comment::STATUS_PENDING;
          else
          $comment->status=Comment::STATUS_APPROVED;
          $comment->post_id=$this->id; */
        return $group->save();
    }

    public function updateGroup($group) {
        return $group->update();
    }

    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    /*
      protected function afterFind()
      {
      parent::afterFind();
      $this->_oldTags=$this->tags;
      } */

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {

        if ($this->isNewRecord) {
            $this->create_date = date("Y-m-d H:i:s");
            $this->create_by = Yii::app()->user->id;
            $this->create_ip = Yii::app()->request->getUserHostAddress();
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            $this->update_by = Yii::app()->user->id;
            $this->update_ip = Yii::app()->request->getUserHostAddress();
        }
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
        ///Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        /*
          parent::afterDelete();
          Comment::model()->deleteAll('post_id='.$this->id);
          Tag::model()->updateFrequency($this->tags, '');
         */
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function searchByBrand() {
        $criteria = new CDbCriteria();
        $criteria->select = "group_id";
        $criteria->compare('brand_id', $this->brand_id);
        $criteria->group = "group_id";
        $data = Product::model()->findAll($criteria);
        $group = array();
        foreach ($data as $row) {
            $group[] = $row->group_id;
        }
        if (count($group) > 0)
            $group_in = implode(',', $group);
        else
            $group_in = "-1";
        $criteria = new CDbCriteria;
        $criteria->condition = "id in (" . $group_in . ")";
        return new CActiveDataProvider('ProductGroup', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order desc,group_2 asc',
            ),
        ));
    }

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('brand_id', $this->brand_id);
        $criteria->compare('logo', $this->logo, true);
        $criteria->compare('group_1', $this->group_1, true);
        $criteria->compare('group_2', $this->group_2, true);
        $criteria->compare('group_3', $this->group_3, true);
        $criteria->compare('group_4', $this->group_4, true);
        $criteria->compare('group_5', $this->group_5, true);
        $criteria->compare('group_desc_1', $this->group_desc_1, true);
        $criteria->compare('group_desc_2', $this->group_desc_2, true);
        $criteria->compare('group_desc_3', $this->group_desc_3, true);
        $criteria->compare('group_desc_4', $this->group_desc_4, true);
        $criteria->compare('group_desc_5', $this->group_desc_5, true);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('create_by', $this->create_by, true);
        $criteria->compare('create_ip', $this->create_ip, true);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('update_by', $this->update_by, true);
        $criteria->compare('update_ip', $this->update_ip, true);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('parent_id', $this->parent_id);

        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order asc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 20),
                'currentPage' => $this->current_page,),
        ));
   
    }
    
     public function searchAll() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('parent_product_group');

        $this->row_count = $this->count($criteria);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.parent_id asc,t.sort_order asc',
            ),
            'pagination' => array(
                'pageSize' => ($this->display_perpage != "" ? $this->display_perpage : 9999),
                'currentPage' => $this->current_page,),
        ));
   
    }
    
    

}
