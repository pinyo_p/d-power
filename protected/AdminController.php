<?php

class AdminController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public $layout='//layouts/admin/main';
	
	function init()
    {
		/*
        parent::init();
        $app = Yii::app();
        if (isset($_POST['lang']))
        {
            $app->language = $_POST['lang'];
            $app->session['_lang'] = $app->language;
        }
        else if (isset($app->session['_lang']))
        {
            $app->language = $app->session['_lang'];
        }

		if(strtolower($app->language)=="en")
			Yii::app()->setLanguage("en");
		else
			Yii::app()->setLanguage("th");
			
		*/
		
			
    }
	public function actions()
	{
		
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		
		$model=new LoginForm;
		$this->layout= false;
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/');
		}
		// display the login form
		$this->render('login',array('model'=>$model));
		
		/*$this->layout = false;
		$model = new LoginForm();
		$model->username='admin';
		$model->password='admin';
		$model->login();
		$this->redirect(Yii::app()->request->baseUrl . "/index.php/admin/");
		**/
		
	}
	
	public function actionMemberPolicy()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'MemberPolicy'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionCredit()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'credit'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionReward()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'reward'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionServiceDetail()
	{
		$id = $_GET['id'];
		$model = Service::model()->findByPk($id);
		$this->render('service_detail',array('model'=>$model));
	}
	
	public function actionContactDetail()
	{
		$id = $_GET['id'];
		$model = Contact::model()->findByPk($id);
		$this->render('contact_detail',array('model'=>$model));
	}
	
	public function actionQuotationForm()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'quotationform'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionPromotion()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'promotion'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionRepair()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'repair'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	public function actionFaq()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'faq'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}

	
	public function actionFooter()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'Footer'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	public function actionSitemap()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'Sitemap'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}

	public function actionHistory()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'History'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionPopup()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'popup'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('popup',array('model'=>$model));
	}
	public function actionIntro()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'intro'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('intro',array('model'=>$model));
	}
	
	public function actionDayList()
	{
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Day::model()->deleteByPk($id);
			}
		}
		$model = new Day();
		$data = $model->search()->data;
		$this->render('daylist',array('model'=>$model,'data'=>$data));
	}
	
	
	
	public function actionTrainingList()
	{
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Training::model()->deleteByPk($id);
			}
		}
		$model = new Training();
		$data = $model->search()->data;
		$this->render('traininglist',array('model'=>$model,'data'=>$data));
	}
	
	public function actionTraining()
	{
		
		if(isset($_GET['id']))
			$model = Training::model()->findByPk($_GET['id']);
		else
			$model = new Training();
		if(isset($_POST['Training']))
		{
			$model->attributes=$_POST['Training'];
			
			if($model->validate() && $model->save())
			{
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/traininglist');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('training',array('model'=>$model));
	}
	public function actiondeleteTraining()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Training::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionDay()
	{
		
		if(isset($_GET['id']))
			$model = Day::model()->findByPk($_GET['id']);
		else
			$model = new Day();
		if(isset($_POST['Day']))
		{
			$model->attributes=$_POST['Day'];
			
			if($model->validate() && $model->save())
			{
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/daylist');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('day',array('model'=>$model));
	}
	public function actiondeleteDay()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Day::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionBannerDayList()
	{
		$model = new BannerDay();
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				BannerDay::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['BannerDay']))
			{
				$model->attributes=$_POST['BannerDay'];
				$model->day_id = $_POST['BannerDay']['day_id'];
			}
		}
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		$banner = Content::model()->find("content_code='banner_day'");
		$param['banner'] = $banner;
		$this->render('bannerdaylist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}

	public function actionBannerDay()
	{
		
		if(isset($_GET['id']))
			$model = BannerDay::model()->findByPk($_GET['id']);
		else
			$model = new BannerDay();
		if(isset($_POST['BannerDay']))
		{
			$model->attributes=$_POST['BannerDay'];
			if($model->validate() && $model->save())
			{
				if(isset($_FILES['img_src']) && $_FILES['img_src']['name']!="")
				{
					$file_name = $model->day_id . '_' . $model->id . '_' .$_FILES['img_src']['name']; 
					$model->img_src = $file_name;
					copy($_FILES['img_src']['tmp_name'],Yii::app()->basePath . '/../images/banner_day/' . $file_name);
					$model->save();	
				}
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/BannerDayList');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		
		$this->render('bannerday',array('model'=>$model));
	}
	
	public function actionchooseBannerDay()
	{
		$id = $_REQUEST['id'];
		$banner = BannerDay::model()->findByPk($id);
		Content::model()->updateAll(array("content_th"=>$banner->img_src),"content_code='banner_day'");
		echo "OK";
	}
	
	public function actionupdateBannerDay()
	{
		$status = $_REQUEST['status'];
		$s_date = $_REQUEST['s_date'];
		$e_date = $_REQUEST['e_date'];
		Content::model()->updateAll(array("attr1"=>$status,"attr2"=>$s_date,"attr3"=>$e_date),"content_code='banner_day'");
		echo "OK";
	}
	
	public function actiondeleteBannerDay()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			BannerDay::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionContactUs()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'ContactUs'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	public function actionOrderShippingDetail()
	{
		$id = $_REQUEST['id'];
		$m = new MemberOrder();
		$m->id = $id;
		$model = $m->search_by_id()->data;
		$this->layout =false;
		$this->render('shippingdetail',array('model'=>$model[0]));
	}
	
	public function actionPolicy()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'Policy'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionSalePolicy()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'SalePolicy'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionProtection()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'Protection'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	public function actionShipping()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'Shipping'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionBannerList()
	{
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Banner::model()->deleteByPk($id);
			}
		}
		$model = new Banner();
		$data = $model->search()->data;
		$this->render('bannerlist',array('model'=>$model,'data'=>$data));
	}
	
	public function actionBanner()
	{
		
		if(isset($_GET['id']))
			$model = Banner::model()->findByPk($_GET['id']);
		else
			$model = new Banner();
		if(isset($_POST['Banner']))
		{
			$model->attributes=$_POST['Banner'];
			
			if($model->validate() && $model->save())
			{
				if(isset($_FILES['pic']) && $_FILES['pic']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic']['name']; 
					$model->img_src = $file_name;
					copy($_FILES['pic']['tmp_name'],Yii::app()->basePath . '/../images/banner/' . $file_name);
					$model->save();	
				}
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/bannerlist');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('banner',array('model'=>$model));
	}
	public function actiondeleteBanner()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Banner::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionBrandLink()
	{
		$model = Content::model()->find('content_code=:content_code',array(':content_code'=>'BrandLink'));
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
			}else{
				echo "Error";
			}
		}
		$this->render('member_policy',array('model'=>$model));
	}
	
	public function actionPartner()
	{
		
		if(isset($_GET['id']))
			$model = Partner::model()->findByPk($_GET['id']);
		else
			$model = new Partner();
		if(isset($_POST['Partner']))
		{
			$model->attributes=$_POST['Partner'];
			
			// validate user input and redirect to the previous page if valid
			if($model->validate() )
			{
				$model->save();
				if(isset($_FILES['logo']) && $_FILES['logo']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['logo']['name']; 
					$model->logo = $file_name;
					copy($_FILES['logo']['tmp_name'],Yii::app()->basePath . '/../images/partner/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['map']) && $_FILES['map']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['map']['name']; 
					$model->map = $file_name;
					copy($_FILES['map']['tmp_name'],Yii::app()->basePath . '/../images/partner/' . $file_name);
					$model->save();	
				}
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/partnerlist');
			}else{
				echo "Error";
			}
		}
		$this->render('partner',array('model'=>$model));
	}
	public function actionPartnerList()
	{
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Partner::model()->deleteByPk($id);
			}
		}
		$model = new Partner();
		$data = $model->search()->data;
		$this->render('partnerlist',array('data'=>$data));
	}
	public function actiondeletePartner()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Partner::model()->deleteByPk($id);
			echo trim("OK");
		}
	}
	
	public function actionBranchList()
	{
		
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Branch::model()->deleteByPk($id);
			}
		}
		$model = new Branch();
		$data = $model->search()->data;
		$this->render('branchlist',array('model'=>$model,'data'=>$data));
	}
	public function actionBranch()
	{
		
		if(isset($_GET['id']))
			$model = Branch::model()->findByPk($_GET['id']);
		else
			$model = new Branch();
		if(isset($_POST['Branch']))
		{
			$model->attributes=$_POST['Branch'];
			
			if($model->validate() && $model->save())
			{
				
				if(isset($_FILES['main_image']) && $_FILES['main_image']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['main_image']['name']; 
					$model->main_image = $file_name;
					copy($_FILES['main_image']['tmp_name'],Yii::app()->basePath . '/../images/branch/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['map_image']) && $_FILES['map_image']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['map_image']['name']; 
					$model->map_image = $file_name;
					copy($_FILES['map_image']['tmp_name'],Yii::app()->basePath . '/../images/branch/' . $file_name);
					$model->save();	
				}
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/branchlist');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('branch',array('model'=>$model));
	}
	public function actiondeleteBranch()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Branch::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionBankList()
	{
		
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Bank::model()->deleteByPk($id);
			}
		}
		$model = new Bank();
		$data = $model->search()->data;
		$this->render('banklist',array('model'=>$model,'data'=>$data));
	}
	public function actionBank()
	{
		
		if(isset($_GET['id']))
			$model = Bank::model()->findByPk($_GET['id']);
		else
			$model = new Bank();
		if(isset($_POST['Bank']))
		{
			$model->attributes=$_POST['Bank'];
			
			if($model->validate() && $model->save())
			{
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/banklist');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('bank',array('model'=>$model));
	}
	public function actiondeleteBank()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Bank::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionNewsList()
	{
		
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Content::model()->deleteByPk($id);
			}
		}
		$model = new Content();
		$model->content_code= 'News';
		$data = $model->searchByCode()->data;
		$this->render('newslist',array('model'=>$model,'data'=>$data));
	}
	public function actionNews()
	{
		
		if(isset($_GET['id']))
			$model = Content::model()->findByPk($_GET['id']);
		else
			$model = new Content();
		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
			$model->content_code='News';
			if($model->validate() && $model->save())
			{
				if(isset($_FILES['files']) && $_FILES['files']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['files']['name']; 
					$model->attr1 = $file_name;
					copy($_FILES['files']['tmp_name'],Yii::app()->basePath . '/../images/content/' . $file_name);
					$model->save();	
				}
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/newslist');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('news',array('model'=>$model));
	}
	public function actiondeleteNews()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Content::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionBrandGroupList()
	{
		
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				BrandGroup::model()->deleteByPk($id);
			}
		}
		$model = new BrandGroup();
		$data = $model->search()->data;
		$this->render('brandgrouplist',array('model'=>$model,'data'=>$data));
	}
	public function actionBrandGroup()
	{
		
		if(isset($_GET['id']))
			$model = BrandGroup::model()->findByPk($_GET['id']);
		else
			$model = new BrandGroup();
		if(isset($_POST['BrandGroup']))
		{
			$model->attributes=$_POST['BrandGroup'];
			if($model->validate() && $model->save())
			{
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/BrandGroupList');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('brandgroup',array('model'=>$model));
	}
	public function actiondeleteBrandGroup()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			BrandGroup::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	
	public function actionProductGroupList()
	{
		
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				ProductGroup::model()->deleteByPk($id);
			}
		}
		$model = new ProductGroup();
		if(isset($_POST['ProductGroup']['brand_id']) && $_POST['ProductGroup']['brand_id']!="")
		{
			$model->brand_id = $_POST['ProductGroup']['brand_id'];
		}
		
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		
		$this->render('productgrouplist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	public function actionProductGroup()
	{
		
		if(isset($_GET['id']))
			$model = ProductGroup::model()->findByPk($_GET['id']);
		else
			$model = new ProductGroup();
		if(isset($_POST['ProductGroup']))
		{
			$model->attributes=$_POST['ProductGroup'];
			if($model->validate() && $model->save())
			{
				if(isset($_FILES['logo']) && $_FILES['logo']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['logo']['name']; 
					$model->logo = $file_name;
					copy($_FILES['logo']['tmp_name'],Yii::app()->basePath . '/../images/group/' . $file_name);
					$model->save();	
				}
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/ProductGroupList');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('productgroup',array('model'=>$model));
	}
	public function actiondeleteProductGroup()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			ProductGroup::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionBrandList()
	{
		
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Brand::model()->deleteByPk($id);
			}
		}
		$model = new Brand();
		$data = $model->search()->data;
		$this->render('brandlist',array('model'=>$model,'data'=>$data));
	}
	public function actionBrand()
	{
		
		if(isset($_GET['id']))
			$model = Brand::model()->findByPk($_GET['id']);
		else
			$model = new Brand();
		if(isset($_POST['Brand']))
		{
			$model->attributes=$_POST['Brand'];
			
			if($model->validate() && $model->save())
			{
				if(isset($_FILES['brand_logo']) && $_FILES['brand_logo']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['brand_logo']['name']; 
					$model->brand_logo = $file_name;
					copy($_FILES['brand_logo']['tmp_name'],Yii::app()->basePath . '/../images/brand/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['brand_banner']) && $_FILES['brand_banner']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['brand_banner']['name']; 
					$model->brand_banner = $file_name;
					copy($_FILES['brand_banner']['tmp_name'],Yii::app()->basePath . '/../images/brand/' . $file_name);
					$model->save();	
				}
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/brandlist');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('brand',array('model'=>$model));
	}
	public function actiondeleteBrand()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Brand::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionProductList()
	{
		$model = new Product();
		$param['series'] = array();
		$param['group'] = array();
		
		
		
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Product::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['Product']))
			{
				$model->attributes=$_POST['Product'];
				$model->product_name = $_POST['Product']['product_name'];
				$model->product_code = $_POST['Product']['product_code'];
				$model->status = $_POST['Product']['status'];
				$model->serie_id = $_POST['Product']['serie_id'];
				if($model->group_id != ""){
					$param['series'] = CHtml::listData(Series::model()->findAll("group_id=:group_id", array(':group_id'=>$model->group_id,)), 'id', 'name_th');
				}
			}
		}
		
		if($model->brand_id!="")
		{
			$param['group'] = CHtml::listData(ProductGroup::model()->findAll("brand_id=:brand_id", array(':brand_id'=>$model->brand_id,)), 'id', 'group_th');
		}
		
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->backend_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		
		$this->render('productlist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	public function actionchangeProductStatus()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Product::model()->updateByPk($id,array('status'=>$_POST['status']));
		}
		echo "OK";
	}
	public function actionProduct()
	{
		$param['series']=array();
		$param['group'] = array();
		if(isset($_GET['id']))
			$model = Product::model()->findByPk($_GET['id']);
			if(isset($model)){
				if($model->group_id!="" && $model->group_id!="0")
				{
					if($model->group_id != ""){
						$param['series'] = CHtml::listData(Series::model()->findAll("group_id=:group_id", array(':group_id'=>$model->group_id,)), 'id', 'name_th');
					}
				}
			}
		else
			$model = new Product();
			
		if($model->brand_id!="")
		{
			$param['group'] = CHtml::listData(ProductGroup::model()->findAll("brand_id=:brand_id", array(':brand_id'=>$model->brand_id,)), 'id', 'group_th');
		}
		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];
			
			if($model->validate() && $model->save())
			{
				if(isset($_FILES['pic1']) && $_FILES['pic1']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic1']['name']; 
					$model->pic1 = $file_name;
					copy($_FILES['pic1']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				
				if(isset($_FILES['pic2']) && $_FILES['pic2']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic2']['name']; 
					$model->pic2 = $file_name;
					copy($_FILES['pic2']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				
				if(isset($_FILES['pic3']) && $_FILES['pic3']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic3']['name']; 
					$model->pic3 = $file_name;
					copy($_FILES['pic3']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				
				if(isset($_FILES['pic4']) && $_FILES['pic4']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic4']['name']; 
					$model->pic4 = $file_name;
					copy($_FILES['pic4']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['pic5']) && $_FILES['pic5']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic5']['name']; 
					$model->pic5 = $file_name;
					copy($_FILES['pic5']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['pic6']) && $_FILES['pic6']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic6']['name']; 
					$model->pic6 = $file_name;
					copy($_FILES['pic6']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['pic7']) && $_FILES['pic7']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic7']['name']; 
					$model->pic7 = $file_name;
					copy($_FILES['pic7']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['pic8']) && $_FILES['pic8']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic8']['name']; 
					$model->pic8 = $file_name;
					copy($_FILES['pic8']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['pic9']) && $_FILES['pic9']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic9']['name']; 
					$model->pic9 = $file_name;
					copy($_FILES['pic9']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['pic10']) && $_FILES['pic10']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['pic10']['name']; 
					$model->pic10 = $file_name;
					copy($_FILES['pic10']['tmp_name'],Yii::app()->basePath . '/../images/product/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['attach']) && $_FILES['attach']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['attach']['name']; 
					$model->attach = $file_name;
					copy($_FILES['attach']['tmp_name'],Yii::app()->basePath . '/../attach/product/' . $file_name);
					$model->save();	
				}
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/ProductList');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('product',array('model'=>$model,'param'=>$param));
	}
	public function actiondeleteProduct()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Product::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionUserGroupList()
	{
		
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				UserGroup::model()->deleteByPk($id);
			}
		}
		$model = new UserGroup();
		$data = $model->search()->data;
		$this->render('usergrouplist',array('model'=>$model,'data'=>$data));
	}
	public function actionUserGroup()
	{
		
		if(isset($_GET['id']))
			$model = UserGroup::model()->findByPk($_GET['id']);
		else
			$model = new UserGroup();
		if(isset($_POST['UserGroup']))
		{
			$model->attributes=$_POST['UserGroup'];
			if($model->validate() && $model->save())
			{
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/UserGroupList');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('usergroup',array('model'=>$model));
	}
	public function actiondeleteUserGroup()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			UserGroup::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionUserList()
	{
		$model = new User();
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				User::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['User']))
			{
				$model->attributes=$_POST['User'];
				$model->key_word = $_POST['User']['key_word'];
				$model->group_id = $_POST['User']['group_id'];
				$model->status = $_POST['User']['status'];
			}
		}
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->backend_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		
		$this->render('userlist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	public function actionchangeUserStatus()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			User::model()->updateByPk($id,array('status'=>$_POST['status']));
		}
		echo "OK";
	}
	public function actionUser()
	{
		
		if(isset($_GET['id']))
			$model = User::model()->findByPk($_GET['id']);
		else
			$model = new User();
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->last_login='';
			if($model->validate() && $model->save())
			{
				
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/UserList');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('user',array('model'=>$model));
	}
	public function actiondeleteUser()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			User::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionSetting()
	{
		$model = Setting::model()->findByPk(1);
		if(isset($_POST['Setting']))
		{
			$model->attributes=$_POST['Setting'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->save())
			{
				if(isset($_FILES['logo']) && $_FILES['logo']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['logo']['name']; 
					$model->logo = $file_name;
					copy($_FILES['logo']['tmp_name'],Yii::app()->basePath . '/../images/logo/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['line']) && $_FILES['line']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['line']['name']; 
					$model->line = $file_name;
					copy($_FILES['line']['tmp_name'],Yii::app()->basePath . '/../images/logo/' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['instragram']) && $_FILES['instragram']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['instragram']['name']; 
					$model->instragram = $file_name;
					copy($_FILES['instragram']['tmp_name'],Yii::app()->basePath . '/../images/logo/' . $file_name);
					$model->save();	
				}
			}else{
				echo "Error";
			}
		}
		$this->render('setting',array('model'=>$model));
	}
	public function actionProductRelateList()
	{
		$model = new RelateProduct();
		$model->product_id=$_GET['id'];
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				RelateProduct::model()->deleteByPk($id);
			}
		}
		
		$data = $model->backend_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		
		
		$this->render('productrelatelist',array('model'=>$model,'data'=>$data));
	}
	public function actionProductRelate()
	{
		$model = new Product();
		$data = array();
		if(isset($_POST['act']) && $_POST['act']=="addall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				$relate = new RelateProduct();
				$relate->product_id = $_GET['id'];
				$relate->product_relate_id = $id;
				$relate->save();
			}
			$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/ProductRelateList/' . $_GET['id'] );
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['Product']))
			{
				$model->attributes=$_POST['Product'];
				$model->product_name = $_POST['Product']['product_name'];
				$model->product_code = $_POST['Product']['product_code'];
				$model->status = $_POST['Product']['status'];
			}
		}
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		if(isset($_POST['act']) && $_POST['act']=="search")
			$data = $model->backend_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		
		$this->render('productrelate',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	public function actiondeleteProductRelate()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			RelateProduct::model()->deleteByPk($id);
			echo "OK";
		}
	}
	
	public function actionJobbeeList()
	{
		$model = new Jobbee();
		if(isset($_POST['act']) && $_POST['act']=="deleteAll"){
			if(isset($_POST['jobbee_id']) && count($_POST['jobbee_id'])>0){
			$mid = $_POST['jobbee_id'];
			foreach($mid as $m){
				Jobbee::model()->deleteAll('id = :id',array(":id"=>$m));
			}
			}
		}
		if(isset($_POST['Jobbee']))
		{
			$model->attributes=$_POST['Jobbee'];
			$model->key_word = $_POST['Jobbee']['key_word'];
			$model->start_date = $_POST['Jobbee']['start_date'];
			$model->end_date = $_POST['Jobbee']['end_date'];
			$model->status =  $_POST['Jobbee']['status'];
		}
		$data = $model->search()->data;
		$this->render('jobbeelist',array('model'=>$model,'data'=>$data));
	}
	public function actionChangeJobbedStatus()
	{
		$id = $_REQUEST['id'];
		/*
		$model = Jobbee::model()->findByPk($id);
		$model->status = $_REQUEST['status'];
		$model->save();*/
		Jobbee::model()->updateByPk($id,array('status'=>$_REQUEST['status']));
		echo "OK";
	}
	public function actionDeleteJobbee()
	{
		$id = $_REQUEST['id'];
		$model = Jobbee::model()->findByPk($id);
		$model->delete();
		echo "OK";
	}
	public function actionJobList()
	{
		$model = new Joblist();
		if(isset($_POST['Joblist']))
		{
			$model->attributes=$_POST['Joblist'];
			if($model->validate()){
				$model->save();
			}
		}
		$data =  $model->search()->data;
		$this->render('joblist',array('model'=>$model,'data'=>$data));
	}
	public function actionJob()
	{
		if(isset($_REQUEST['id'])){
		$id = $_REQUEST['id'];
		$model = Joblist::model()->findByPk($id);
		}else{
			$model = New Joblist();
		}
		if(isset($_POST['Joblist']))
		{
			$model->attributes=$_POST['Joblist'];
			if($model->validate()){
				$model->save();
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/JobList/');
			}else{
				print_r($model->getErrors());
			}
		}
		$this->render('job',array('model'=>$model));
	}
	public function actionDeleteJob()
	{
		$id = $_REQUEST['id'];
		/*$model = Joblist::model()->findByPk($id);
		$model->delete($id);
		
		*/
		Joblist::model()->deleteByPk($id);
		echo "OK";
	}
	public function actionOrderList()
	{
		$model = new MemberOrder();
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Product::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['MemberOrder']))
			{
				$model->attributes=$_POST['MemberOrder'];
				$model->fullname = $_POST['MemberOrder']['fullname'];
				$model->company_name = $_POST['MemberOrder']['company_name'];
				$model->address = $_POST['MemberOrder']['address'];
				$model->province = $_POST['MemberOrder']['province'];
				$model->start_date = $_POST['MemberOrder']['start_date'];
				$model->end_date = $_POST['MemberOrder']['end_date'];
				$model->status = $_POST['MemberOrder']['status'];

			}
		}
	
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->backend_order_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		
		$this->render('orderlist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	public function actionchangeOrderStatus()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			MemberOrder::model()->updateByPk($id,array('status'=>$_POST['status']));
		}
		echo "OK" ;
	}
	public function actionShippingList()
	{
		$model = new MemberOrder();
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Product::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['MemberOrder']))
			{
				$model->attributes=$_POST['MemberOrder'];
				$model->fullname = $_POST['MemberOrder']['fullname'];
				$model->company_name = $_POST['MemberOrder']['company_name'];
				$model->address = $_POST['MemberOrder']['address'];
				$model->province = $_POST['MemberOrder']['province'];
				$model->start_date = $_POST['MemberOrder']['start_date'];
				$model->end_date = $_POST['MemberOrder']['end_date'];
				$model->status = $_POST['MemberOrder']['status'];

			}
		}
	
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->backend_shipping_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		
		$this->render('shippinglist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	public function actionProductOrderList()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$id = (isset($_GET['id'])?$_GET['id']:0);
		$count = Order::model()->count('order_id=:order_id',array(':order_id'=>$id));
		$model = new Order();
		$model->order_id= $id;
		$data = $model->get_cart_by_order_id()->data;
		$param['count_item'] = $count;
		$this->render('cart',array('param'=>$param,'data'=>$data));
	}
	
	public function actionCatalogList()
	{
		$model = new Download();
		$model->type_id=1;
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Download::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['Download']))
			{
				$model->attributes=$_POST['Download'];
				$model->filename = $_POST['Download']['filename'];
			}
		}
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;

		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->backend_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		
		$this->render('cataloglist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	
	
	
	public function actionCatalog()
	{
		
		if(isset($_GET['id']))
			$model = Download::model()->findByPk($_GET['id']);
		else
			$model = new Download();
		$model->type_id=1;
		if(isset($_POST['Download']))
		{
			$model->attributes=$_POST['Download'];
			$model->type_id=1;
			$model->cover = "";
			$model->filesrc = "";
			if($model->validate() && $model->save())
			{
				if(isset($_FILES['cover']) && $_FILES['cover']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['cover']['name']; 
					$model->cover = $file_name;
					copy($_FILES['cover']['tmp_name'],Yii::app()->basePath . '/../download/cover_' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['file']) && $_FILES['file']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['file']['name']; 
					$model->filesrc = $file_name;
					copy($_FILES['file']['tmp_name'],Yii::app()->basePath . '/../download/file_' . $file_name);
					$model->save();	
				}
				
				
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/CatalogList');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('catalog',array('model'=>$model));
	}
	
	public function actionManualList()
	{
		$model = new Download();
		$model->type_id=2;
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Download::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['Download']))
			{
				$model->attributes=$_POST['Download'];
				$model->filename = $_POST['Download']['filename'];
			}
		}
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;

		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->backend_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		
		$this->render('manualist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	public function actionManual()
	{
		
		if(isset($_GET['id']))
			$model = Download::model()->findByPk($_GET['id']);
		else
			$model = new Download();
		$model->type_id=2;
		if(isset($_POST['Download']))
		{
			$model->attributes=$_POST['Download'];
			$model->type_id=2;
			$model->cover = "";
			$model->filesrc = "";
			if($model->validate() && $model->save())
			{
				if(isset($_FILES['cover']) && $_FILES['cover']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['cover']['name']; 
					$model->cover = $file_name;
					copy($_FILES['cover']['tmp_name'],Yii::app()->basePath . '/../download/cover_' . $file_name);
					$model->save();	
				}
				if(isset($_FILES['file']) && $_FILES['file']['name']!="")
				{
					$file_name = $model->id . '_' .$_FILES['file']['name']; 
					$model->filesrc = $file_name;
					copy($_FILES['file']['tmp_name'],Yii::app()->basePath . '/../download/file_' . $file_name);
					$model->save();	
				}
				
				
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/ManualList');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('manual',array('model'=>$model));
	}
	public function actionDeleteDownload()
	{
		$id = $_REQUEST['id'];
		/*$model = Joblist::model()->findByPk($id);
		$model->delete($id);
		
		*/
		Download::model()->deleteByPk($id);
		echo "OK";
	}
	public function actionSeriesList()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Series::model()->deleteByPk($id);
			}
		}
		$model = new Series();
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['Series']))
			{
				$model->attributes=$_POST['Series'];
				$model->group_id = $_POST['Series']['group_id'];
				$model->brand_id=$_POST['Series']['brand_id'];
			}
		}
		$data = $model->search()->data;
		$this->render('serieslist',array('model'=>$model,'data'=>$data));
	}
	public function actionSeries()
	{
		
		if(isset($_GET['id']))
			$model = Series::model()->findByPk($_GET['id']);
		else
			$model = new Series();
		
		//echo $model->group_id;
		$param['group'] = array();
		$model->brand_id = $model->group['brand_id'];
		
		if($model->group['brand_id']!="")
		{
			$param['group'] = CHtml::listData(ProductGroup::model()->findAll("brand_id=:brand_id", array(':brand_id'=>$model->brand_id,)), 'id', 'group_th');
		}
		
		if(isset($_POST['Series']))
		{
			$model->attributes=$_POST['Series'];
			
			if($model->validate() && $model->save())
			{
				
				$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/serieslist');
			}else{
				print_r($model->getErrors());
				echo "Error";
			}
		}
		$this->render('series',array('model'=>$model,'param'=>$param));
	}
	public function actionDeleteSeries()
	{
		$id = $_REQUEST['id'];

		Series::model()->deleteByPk($id);
		echo "OK";
	}
	
	public function actionGetGroup()
	{
		$brand_id=0;
		if(isset($_REQUEST['Series']['brand_id']))
			$brand_id = $_REQUEST['Series']['brand_id'];
		if(isset($_REQUEST['Product']['brand_id']))
			$brand_id = $_REQUEST['Product']['brand_id'];
		$data = CHtml::listData(ProductGroup::model()->findAll("brand_id=:brand_id ", array(':brand_id'=>$brand_id)), 'id', 'group_th');
		echo "<option value=''>ทุก Group</option>";
		while ($row = current($data)) {
	
				echo "<option value='" . key($data) . "'>" . $row . "</option>";

			next($data);
		}

		//echo "<option value='0'>Test</option>";
		
	}
	
	public function actionGetSeries()
	{
		$group_id=0;
		if(isset($_REQUEST['Product']['group_id']))
			$group_id = $_REQUEST['Product']['group_id'];
		$data = CHtml::listData(Series::model()->findAll("group_id=:group_id ", array(':group_id'=>$group_id)), 'id', 'name_th');
		echo "<option value=''>เลือก Series</option>";
		while ($row = current($data)) {
	
				echo "<option value='" . key($data) . "'>" . $row . "</option>";

			next($data);
		}

		//echo "<option value='0'>Test</option>";
		
	}
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	 
	
	public function actionPremissionUsers()
	{  
		$id= $_REQUEST['id'];
		$user = UserGroup::model()->findByPk($id);
		$model = new Premission();
		
		if(isset($_POST['mm']))
		{
			Premission::model()->deleteAll('user_id=:user_id', array(':user_id'=>$id));
			foreach($_POST['mm'] as $mm)
			{
				$prem = new Premission();
				$prem->menu_id = $mm;
				$prem->user_id = $id;
				$prem->assign_by = Yii::app()->user->getName();
				$prem->type = 0;
				$prem->save();
			//echo $mm;
			}
			
			$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/UserGroupList/');
		}
		$menu = Menu::model()->getMenu()->data;
		$premission = Premission::model()->findAll('user_id=:user_id', array(':user_id'=>$id));
//		$data = $premission->data;
		$this->render('premission',array('model'=>$model,'user'=>$user,'premission'=>$premission,'menu'=>$menu));
	}
	
	public function actionServiceList()
	{
		$model = new Service();
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Service::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['Service']))
			{
				$model->attributes=$_POST['Service'];
				$model->start_date = $_POST['Service']['start_date'];
				$model->end_date =$_POST['Service']['end_date'];
				/*$model->full_name = (isset($_POST['Service']['full_name'])?$_POST['Service']['full_name']:"");
				$model->company_name = (isset($_POST['Service']['company_name'])?$_POST['Service']['company_name']:"");
				$model->status = (isset($_POST['Service']['status'])?$_POST['Service']['status']:"");*/
			}
		}
		$model->scenario = "search";
		$model->display_perpage = "30";
		$model->service_type = (isset($_GET['id'])?$_GET['id']:0);
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;

		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->backend_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		$param['title'] = "Technicial Support";
		if($model->service_type=="2")
			$param['title'] = "Repair";
		$this->render('servicelist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	public function actionchangeServiceStatus()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Service::model()->updateByPk($id,array('status'=>$_POST['status']));
		}
		echo "OK" ;
	}
	public function actionDeleteService()
	{
		$id = $_REQUEST['id'];
		Service::model()->deleteByPk($id);
		echo "OK";
	}
	
	public function actionTraineeList()
	{
		$model = new Course();
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Service::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['Course']))
			{
				$model->attributes=$_POST['Course'];
				$model->start_date = $_POST['Course']['start_date'];
				$model->end_date = $_POST['Course']['end_date'];
				/*$model->full_name = (isset($_POST['Service']['full_name'])?$_POST['Service']['full_name']:"");
				$model->company_name = (isset($_POST['Service']['company_name'])?$_POST['Service']['company_name']:"");
				$model->status = (isset($_POST['Service']['status'])?$_POST['Service']['status']:"");*/
			}
		}
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;

		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->backend_search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		$param['title'] = "Training";

		$this->render('traineelist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	
	public function actionContactList()
	{
		$model = new Contact();
		if(isset($_POST['act']) && $_POST['act']=="deleteall" && isset($_POST['p_id'])){
			$pid = $_POST['p_id'];
			foreach($pid as $id){
				Contact::model()->deleteByPk($id);
			}
		}
		if(isset($_POST['act']) && $_POST['act']=="search"){
			if(isset($_POST['Contact']))
			{
				$model->attributes=$_POST['Contact'];
				$model->status = $_POST['Contact']['status'];
				$model->start_date = $_POST['Contact']['start_date'];
				$model->end_date = $_POST['Contact']['end_date'];
				/*$model->full_name = (isset($_POST['Service']['full_name'])?$_POST['Service']['full_name']:"");
				$model->company_name = (isset($_POST['Service']['company_name'])?$_POST['Service']['company_name']:"");
				$model->status = (isset($_POST['Service']['status'])?$_POST['Service']['status']:"");*/
			}
		}
		$model->type = $_GET['id'];
		$model->display_perpage = "30";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;

		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		if($_REQUEST['id']=="1")
			$param['title'] = "Contact List";
		else
			$param['title'] = "Quotation Request List";

		$this->render('contactlist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	
	public function actiondeleteContact()
	{
		$id = $_REQUEST['id'];
		Contact::model()->deleteByPk($id);
		echo "OK";
	}
	
	public function actionchangeContactStatus()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Contact::model()->updateByPk($id,array('status'=>$_POST['status']));
		}
		echo "OK" ;
	}
	
	
	public function actionchangeTraineeStatus()
	{
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			Course::model()->updateByPk($id,array('status'=>$_POST['status']));
		}
		echo "OK" ;
	}
	
	public function actionExportMember()
	{
		$this->layout=false;
		$model = new Member();
		$model->display_perpage = 99999;
			$model->first_name = $_REQUEST['first_name'];
			$model->last_name = $_REQUEST['last_name'];
			$model->email = $_REQUEST['email'];
			$model->phone_no = $_REQUEST['phone_no'];
			$model->start_date = $_REQUEST['start_date'];
			$model->end_date = $_REQUEST['end_date'];

		$data = $model->search()->data;
		$head = "<html><head></head><body>";
		$body = "";
		$prefix[0] = " ";
		$prefix[1] = " นาย ";
		$prefix[2] = " นาง ";
		$prefix[3] = " นางสาว ";
		$prefix[4] = " ";
		$no =1;
		$body .= "<table border='1'><tr><th>No.</th><th>Prefix</th><th>First Name</th><th>Last Name</th><th>Birth Day</th><th>Address</th><th>Province</th><th>Zipcode</th><th>Phone No</th><th>Email</th><th>Member Type</th><th>Register Date</th><th>Last Login</th></tr>";
		foreach($data as $row)
		{
			$member_type= $row->member_type;
			if($row->member_type=="1")
				$member_type = "บุคคลทั่วไป";
			else
				$member_type = "นิติบุคคล [" . $row->company_name . "]";
			$body .= "<tr>";
			$body .= "<td>$no</td>";
			$body .= "<td>" . $prefix[$row->prefix] . $row->prefix_spec . "</td>";
			$body .= "<td>" .  $row->first_name . "</td>";
			$body .= "<td>" .  $row->last_name . "</td>";
			$body .= "<td>" .  $row->birth_day . "</td>";
			$body .= "<td>" .  $row->address . "</td>";
			$body .= "<td>" .  $row->p['thai_name'] . "</td>";
			$body .= "<td>" .  $row->zipcode . "</td>";
			$body .= "<td>" .  $row->phone_no . "</td>";
			$body .= "<td>" .  $row->email . "</td>";
			$body .= "<td>" .  $member_type . "</td>";
			$body .= "<td>" .  $row->create_date . "</td>";
			$body .= "<td>" .  $row->last_login . "</td>";
			$body .= "</tr>";
			$no++;
		}
		$body .= "</table>";
		$foot = "</body></html>";
		
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Content-Disposition: attachment; filename=memberlist_' . date("YmdHis") . '.xls');
		header('Pragma: no-cache');				
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		echo "\xEF\xBB\xBF"; // UTF-8 BOM
		$html = $head . $body . $foot;
		echo $html ;
		Yii::app()->end();
	}
	
	public function actionExportService()
	{
		$this->layout=false;
		$model = new Service();

			$model->full_name = $_REQUEST['full_name'];
			$model->company_name = $_REQUEST['company_name'];
			$model->product_code = $_REQUEST['product_code'];
			$model->status = $_REQUEST['status'];
			$model->start_date = $_REQUEST['start_date'];
			$model->end_date = $_REQUEST['end_date'];
			$model->service_type = $_REQUEST['service_type'];
		$data = $model->backend_search()->data;
		$head = "<html><head></head><body>";
		$body = "";
		$prefix[0] = " ";
		$prefix[1] = " นาย ";
		$prefix[2] = " นาง ";
		$prefix[3] = " นางสาว ";
		$prefix[4] = " ";
		$status[0] = "มาใหม่";
		$status[1] = "ติดต่อแล้ว";
		$no =1;
		$body .= "<table border='1'><tr><th>No.</th><th>Name</th><th>Company Name</th><th>Product Code</th><th>Detail</th><th>E-Mail</th><th>Phone no.</th><th>Register Date</th><th>Status</th></tr>";
		foreach($data as $row)
		{
			
			$body .= "<tr>";
			$body .= "<td>$no</td>";
			$body .= "<td>" . $row->full_name . "</td>";
			$body .= "<td>" .  $row->company_name . "</td>";
			$body .= "<td>" .  $row->product_code . "</td>";
			$body .= "<td>" .  $row->detail . "</td>";
			$body .= "<td>" .  $row->email . "</td>";
			$body .= "<td>" .  $row->phone_no . "</td>";
			$body .= "<td>" .  $row->create_date . "</td>";
			$body .= "<td>" .  $status[$row->status] . "</td>";
			$body .= "</tr>";
			$no++;
		}
		$body .= "</table>";
		$foot = "</body></html>";
		
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Content-Disposition: attachment; filename=service_requestlist_' . date("YmdHis") . '.xls');
		header('Pragma: no-cache');				
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		echo "\xEF\xBB\xBF"; // UTF-8 BOM
		$html = $head . $body . $foot;
		echo $html ;
		Yii::app()->end();
	}
	
	public function actionExportTrainee()
	{
		$this->layout=false;
		$model = new Course();

		$model->fullname = $_REQUEST['fullname'];
		$model->company_name = $_REQUEST['company_name'];
		$model->sale_name = $_REQUEST['sale_name'];
		$model->status = $_REQUEST['status'];
		$model->start_date = $_REQUEST['start_date'];
		$model->end_date = $_REQUEST['end_date'];
		$model->training_id = $_REQUEST['training_id'];
		$data = $model->backend_search()->data;
		$head = "<html><head></head><body>";
		$body = "";
		$prefix[0] = " ";
		$prefix[1] = " นาย ";
		$prefix[2] = " นาง ";
		$prefix[3] = " นางสาว ";
		$prefix[4] = " ";
		$status[0] = "มาใหม่";
		$status[1] = "ติดต่อแล้ว";
		$no =1;
		$body .= "<table border='1'><tr><th>No.</th><th>Subject</th><th>Training Date</th><th>Training Person</th><th>Name</th><th>Company Name</th><th>Company Type</th><th>Address</th><th>E-Mail</th><th>Tel</th><th>Mobile</th><th>Fax</th><th>Sale name</th><th>Sale company</th><th>Register Date</th><th>Status</th></tr>";
		foreach($data as $row)
		{
			
			$body .= "<tr>";
			$body .= "<td>$no</td>";
			$body .= "<td>" . $row->course['subject_th'] . "</td>";
			$body .= "<td>" .  $row->training_date . "</td>";
			$body .= "<td>" .  $row->training_person . "</td>";
			$body .= "<td>" .  $row->fullname . "</td>";
			$body .= "<td>" .  $row->company_name . "</td>";
			$body .= "<td>" .  $row->company_type . "</td>";
			$body .= "<td>" .  $row->address . "</td>";
			$body .= "<td>" .  $row->email . "</td>";
			$body .= "<td>" .  $row->tel . "</td>";
			$body .= "<td>" .  $row->mobile . "</td>";
			$body .= "<td>" .  $row->fax . "</td>";
			$body .= "<td>" .  $row->sale_name . "</td>";
			$body .= "<td>" .  $row->sale_company . "</td>";
			$body .= "<td>" .  $row->create_date . "</td>";
			$body .= "<td>" .  $status[$row->status] . "</td>";
			$body .= "</tr>";
			$no++;
		}
		$body .= "</table>";
		$foot = "</body></html>";
		
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Content-Disposition: attachment; filename=traineelist_' . date("YmdHis") . '.xls');
		header('Pragma: no-cache');				
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		echo "\xEF\xBB\xBF"; // UTF-8 BOM
		$html = $head . $body . $foot;
		echo $html ;
		Yii::app()->end();
	}
	
	public function actionServiceRequestList()
	{
		$this->layout=false;
		$model = new Member();

			$model->first_name = $_REQUEST['first_name'];
			$model->last_name = $_REQUEST['last_name'];
			$model->email = $_REQUEST['email'];
			$model->phone_no = $_REQUEST['phone_no'];
			$model->start_date = $_REQUEST['start_date'];
			$model->end_date = $_REQUEST['end_date'];

		$data = $model->search()->data;
		$head = "<html><head></head><body>";
		$body = "";
		$prefix[0] = " ";
		$prefix[1] = " นาย ";
		$prefix[2] = " นาง ";
		$prefix[3] = " นางสาว ";
		$prefix[4] = " ";
		$no =1;
		$body .= "<table border='1'><tr><th>No.</th><th>Prefix</th><th>First Name</th><th>Last Name</th><th>Birth Day</th><th>Address</th><th>Province</th><th>Zipcode</th><th>Phone No</th><th>Email</th><th>Member Type</th><th>Register Date</th><th>Last Login</th></tr>";
		foreach($data as $row)
		{
			$member_type= $row->member_type;
			if($row->member_type=="1")
				$member_type = "บุคคลทั่วไป";
			else
				$member_type = "นิติบุคคล [" . $row->company_name . "]";
			$body .= "<tr>";
			$body .= "<td>$no</td>";
			$body .= "<td>" . $prefix[$row->prefix] . $row->prefix_spec . "</td>";
			$body .= "<td>" .  $row->first_name . "</td>";
			$body .= "<td>" .  $row->last_name . "</td>";
			$body .= "<td>" .  $row->birth_day . "</td>";
			$body .= "<td>" .  $row->address . "</td>";
			$body .= "<td>" .  $row->p['thai_name'] . "</td>";
			$body .= "<td>" .  $row->zipcode . "</td>";
			$body .= "<td>" .  $row->phone_no . "</td>";
			$body .= "<td>" .  $row->email . "</td>";
			$body .= "<td>" .  $member_type . "</td>";
			$body .= "<td>" .  $row->create_date . "</td>";
			$body .= "<td>" .  $row->last_login . "</td>";
			$body .= "</tr>";
			$no++;
		}
		$body .= "</table>";
		$foot = "</body></html>";
		
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Content-Disposition: attachment; filename=memberlist_' . date("YmdHis") . '.xls');
		header('Pragma: no-cache');				
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		echo "\xEF\xBB\xBF"; // UTF-8 BOM
		$html = $head . $body . $foot;
		echo $html ;
		Yii::app()->end();
	}
	
	public function actionExportContact()
	{
		$this->layout=false;
		$model = new Contact();

		$model->fullname = $_REQUEST['fullname'];
		$model->company_name = $_REQUEST['company_name'];
		$model->subject = $_REQUEST['subject'];
		$model->status = $_REQUEST['status'];
		$model->start_date = $_REQUEST['start_date'];
		$model->end_date = $_REQUEST['end_date'];
		$model->detail = $_REQUEST['detail'];
		$model->type = $_REQUEST['type'];

		
		$data = $model->search()->data;
		$head = "<html><head></head><body>";
		$body = "";
		$prefix[0] = " ";
		$prefix[1] = " นาย ";
		$prefix[2] = " นาง ";
		$prefix[3] = " นางสาว ";
		$prefix[4] = " ";
		$status[0] = "มาใหม่";
		$status[1] = "ติดต่อแล้ว";
		$no =1;
		$body .= "<table border='1'><tr><th>No.</th>
				<th >หัวข้อ</th>
              <th >รายละเอียด</th>
              <th >ชื่อ - นามสกุล</th>
              <th >ชื่อบริษัท</th>
			   <th >ตำแหน่ง</th>
              <th >เบอร์มือถือ</th>
			   <th >fax</th>
			   <th >Email</th>
			   <th >ที่อยู่</th>
			    <th >website</th>
				<th >วันที่ลงทะเบียน</th>
			  <th>Status</th>
			   
			  </tr>";
		foreach($data as $row)
		{
			
			$body .= "<tr>";
			$body .= "<td>$no</td>";
			$body .= "<td>" . $row->subject. "</td>";
			$body .= "<td>" . str_replace(chr(13),"<br />",$row->detail). "</td>";
			$body .= "<td>" .  $row->fullname . "</td>";
			$body .= "<td>" .  $row->company_name . "</td>";
			$body .= "<td>" .  $row->position . "</td>";
			$body .= "<td>" .  $row->tel . "</td>";
			$body .= "<td>" .  $row->fax . "</td>";
			$body .= "<td>" .  $row->email . "</td>";
			$body .= "<td>" .  $row->address . "</td>";
			$body .= "<td>" .  $row->website . "</td>";
			$body .= "<td>" .  $row->create_date . "</td>";
			$body .= "<td>" .  $status[$row->status] . "</td>";
			$body .= "</tr>";
			$no++;
		}
		$body .= "</table>";
		$foot = "</body></html>";
		
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Content-Disposition: attachment; filename=contactlist_' . date("YmdHis") . '.xls');
		header('Pragma: no-cache');				
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		echo "\xEF\xBB\xBF"; // UTF-8 BOM
		$html = $head . $body . $foot;
		echo $html ;
		Yii::app()->end();
	}
	
	public function actiondeleteTrainee()
	{
		$id = $_REQUEST['id'];
		Course::model()->deleteByPk($id);
		echo "OK";
	}
	
	public function actionMemberList()
	{
		$model = new Member();
		if(isset($_POST['act']) && $_POST['act']=="deleteAll"){
			if(isset($_POST['member_id']) && count($_POST['member_id'])>0){
			$mid = $_POST['member_id'];
			foreach($mid as $m){
				Member::model()->deleteAll('id = :id',array(":id"=>$m));
			}
			}
		}
		if(isset($_POST['Member']))
		{
			$model->attributes=$_POST['Member'];
			$model->first_name = $_POST['Member']['first_name'];
			$model->last_name = $_POST['Member']['last_name'];
			$model->email = $_POST['Member']['email'];
			$model->phone_no = $_POST['Member']['phone_no'];
			$model->start_date = $_POST['Member']['start_date'];
			$model->end_date = $_POST['Member']['end_date'];
		}
		$model->display_perpage = "10";
		//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
		$param['display_perpage'] = $model->display_perpage;
		
		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		$data = $model->search()->data;
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;	
		
		$this->render('memberlist',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	public function actionDeleteMember()
	{
		$id = $_REQUEST['id'];
		Member::model()->deleteByPk($id);
		echo "OK";
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}