<?php

class MainLabel  {

    public static $url_friendly = "URL Fiendly";
    public static $key_word = "Meta Keyword";
    public static $title_keyword = "Title";
    public static $meta_desc = "Meta Description";
    public static $subject = "หัวข้อ";
    public static $enable = "ใช้งาน";
    public static $disable = "ไม่ใช้งาน";
    public static $edit = "แก้ไข";
    public static $delete = "ลบ";
    public static $cancel = "ยกเลิก";
    public static $add = "เพิ่ม";
    public static $confirm_delete = "ต้องการลบรายการนี้ใช่หรือไม่??";
    public static $search = "ค้นหา ";
    public static $insert_complete = "เพิ่มข้อมูลเสร็จสิ้น ";
    public static $update_complete = "ปรับปรุงข้อมูลเสร็จสิ้น ";
    public static $delete_complete = "ลบข้อมูลเสร็จสิ้น ";
    public static $submit_complete = "ส่งข้อมูลเสร็จสมบูรณ์ ";
    public static $status = "สถานะ ";
    public static $require_url_friendly = " URL Friendly ไม่สามารถเป็นค่าว่างได้";
    public static $publish_date = " วันที่ใช้งาน";
    public static $optional = " ตัวเลือกเพิ่มเติม";
    public static $attach = " ไฟล์แนบ";
    public static $video = " วิดีโอ";
    public static $agent = " ตัวแทน";
    public static $option = " บริการเสริม";
    public static $gallery_name = " อัลบั้มรูป";
    public static $ticket = " ตั๋วเครื่องบิน";
    public static $hotel = " ที่พัก";
    public static $bus = " รถบริการรับ-ส่ง";
    public static $grouptour = " ทัวร์เต็มรูปแบบ";
    public static $dinner = " อาหารค่ำ";
    public static $setting = "ตั้งค่าต่างๆ";
    public static $day = "วัน";
    public static $night = "คืน";
    public static $price = "ราคา";
    public static $DeleteImage = "ลบรูปภาพ";
    public static $DeleteAttach = "ลบไฟล์แนบ";
    public static $otherdetail = "รายละเอียดเพิ่มเติม";
    public static $detail = "รายละเอียด";

}

?>