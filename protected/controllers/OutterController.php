<?php
class OutterController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	 function init()
    {
        parent::init();
        $app = Yii::app();
        if (isset($_POST['lang']))
        {
            $app->language = $_POST['lang'];
            $app->session['_lang'] = $app->language;
        }
        else if (isset($app->session['_lang']))
        {
            $app->language = $app->session['_lang'];
        }

		if(strtolower($app->language)=="en")
			Yii::app()->setLanguage("en");
		else
			Yii::app()->setLanguage("th");
			
			
			
    }
	public $layout='//layouts/outter/main';
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	public function actionPropertyList()
	{
		$model = new ItemPremission();
		$model->group_id = '2';
		$model->user_id=Yii::app()->user->getValue("id");
		$data = $model->searchByUserIdAndGroupId()->data;
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='OUT';
		$tracking->group_id=0;
		$tracking->type=2;
		$tracking->parent_id=0;
		$tracking->save();
		$this->render('propertylist',array('model'=>$model,'data'=>$data));
	}
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		//$this->layout = '//layouts/column2';
		$model = new AboutSam();
		$part = "OUT";
		$group = 2;
		$model->group = $group;
		$model->part = $part;
		$data = $model->frontSearchByPartAndGroup()->data;
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='OUT';
		$tracking->group_id=0;
		$tracking->type=0;
		$tracking->parent_id=0;
		$tracking->save();
		$this->render('index',array('data'=>$data));
	}
	public function actionDownloadList()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		//$this->layout = '//layouts/column2';
		$session=new CHttpSession;
		$session->open();
		$user = $session['user'];
		$mm = new Download();
		$part = 'OUT';
		$mm->part = $part;
		if(isset($_REQUEST['id']))
			$mm->group= $_REQUEST['id'];	
		$param['display_perpage'] = 20;
		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		$mm->current_page = $current_page;
		$mm->display_perpage=20;
		
		$mm->user_id=$user->id;
		$data = $mm->frontSearchByPartAndGroup()->data;
		$param['row_amount'] = $mm->row_count;
		$param['max_page']  = ceil($param['row_amount'] / ($mm->display_perpage==0?1:$mm->display_perpage));
		if($current_page>=$param['max_page'])
			$current_page = $param['max_page']-1;
		
		$param['page'] = $current_page;
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='OUT';
		$tracking->group_id=$mm->group;
		$tracking->type=3;
		$tracking->parent_id=0;
		$tracking->save();
		$this->render('download',array('data'=>$data,'param'=>$param));
	}
	public function actionDetail()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		//$this->layout = '//layouts/column2';
		$id = $_REQUEST['id'];
		$model = AboutSam::model()->findByPk($id);
		$model->attach= Attach::model()->searchByNpaId($id)->data;
		$tracking = new Tracking();
		$tracking->ref_id=$id;
		$tracking->part='OUT';
		$tracking->group_id=$model->group;
		$tracking->type=1;
		$tracking->parent_id=$model->parent_id;
		$tracking->save();
		$this->render('detail',array('model'=>$model));
	}
	public function actionDownloadIt()
	{
		$id= $_REQUEST['id'];
		$model = Download::model()->findByPk($id);
		$filename =  $model->filesrc;
		$model->download_count = $model->download_count+1 ;
		$model->save();
		
		$tracking = new Tracking();
		$tracking->ref_id=$id;
		$tracking->part='OUT';
		$tracking->group_id=$model->group;
		$tracking->type=3;
		$tracking->parent_id=0;
		$tracking->save();
		

		$this->redirect(Yii::app()->baseUrl .'/download/out/' . $filename);

	}
	public function actionGroup()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		//$this->layout = '//layouts/column2';
		$id = $_REQUEST['id'];
		$model = new AboutSam();
		$model->part = 'OUT';
		$group = $id;
		$model->group = $group;
		
		$model2 = ContentGroup::model()->findByPk($id);
		$param['parent']=$model2->parent_id;
		
		$param['parent']=$id;
		$param['display_perpage'] = 20;
		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		$model->current_page = $current_page;
		$model->display_perpage=20;
		$data = $model->frontSearchByPartAndGroup()->data;
		// ($model->row_count==0?1:$model->row_count);
		
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $model->display_perpage);
		if($current_page>=$param['max_page'])
			$current_page = $param['max_page']-1;
		
		$param['page'] = $current_page;
		
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='OUT';
		$tracking->group_id=$id;
		$tracking->type=1;
		$tracking->parent_id=$model2->parent_id;
		$tracking->save();
		$param['type']="group";
		$this->render("group",array('model'=>$model,'param'=>$param,'data'=>$data));
	}
	public function actionParent()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		//$this->layout = '//layouts/column2';
		$id = $_REQUEST['id'];
		$model = new AboutSam();
		$model->part = 'OUT';
		$model->parent_id = $id;
		
		$param['parent']=$id;
		$param['display_perpage'] = 20;
		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		$model->current_page = $current_page;
		$model->display_perpage=20;
		$data = $model->searchByPartAndParent()->data;
		// ($model->row_count==0?1:$model->row_count);
		
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $model->display_perpage);
		if($current_page>=$param['max_page'])
			$current_page = $param['max_page']-1;
		
		$param['page'] = $current_page;
		$tracking = new Tracking();
			$tracking->ref_id=0;
			$tracking->part='OUT';
			$tracking->group_id=0;
			$tracking->type=1;
			$tracking->parent_id=$id;
			$tracking->save();
			$param['type']="parent";
		$this->render("group",array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	
	
	/**
	* Propert Management
	*/
	
	
function thai_date(){  
$time = time();
	$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");  
	$thai_month_arr=array(  
		"0"=>"",  
		"1"=>"มกราคม",  
		"2"=>"กุมภาพันธ์",  
		"3"=>"มีนาคม",  
		"4"=>"เมษายน",  
		"5"=>"พฤษภาคม",  
		"6"=>"มิถุนายน",   
		"7"=>"กรกฎาคม",  
		"8"=>"สิงหาคม",  
		"9"=>"กันยายน",  
		"10"=>"ตุลาคม",  
		"11"=>"พฤศจิกายน",  	
		"12"=>"ธันวาคม"                    
	);   
    $thai_date_return="วัน".$thai_day_arr[date("w",$time)];  
    $thai_date_return.= "ที่ ".date("j",$time);  
    $thai_date_return.=" เดือน".$thai_month_arr[date("n",$time)];  
    $thai_date_return.= " พ.ศ.".(date("Yํ",$time)+543);  
    $thai_date_return.= "  ".date("H:i",$time)." น.";  
    return $thai_date_return;  
} 
}