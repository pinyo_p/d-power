<?php
class NpaController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	 function init()
    {
        parent::init();
        $app = Yii::app();
        if (isset($_POST['lang']))
        {
            $app->language = $_POST['lang'];
            $app->session['_lang'] = $app->language;
        }
        else if (isset($app->session['_lang']))
        {
            $app->language = $app->session['_lang'];
        }

		if(strtolower($app->language)=="en")
			Yii::app()->setLanguage("en");
		else
			Yii::app()->setLanguage("th");
			
			
			
    }
	 public $layout='//layouts/npa/main';
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		
		$model = FrontProperty::model();
		if(isset($_POST['FrontProperty']))
			$model->attributes=$_POST['FrontProperty'];
		
		$setting = Setting::model()->findByPk(1);
		
		$param['popup'] = Popup::model()->find("display_npa=1 and publish_date<= '" .date("Y-m-d"). "'");
		if(count($param['popup'])<=0)
		{
			$param['popup']->display_npa=0;
			$param['popup']->margin =0;
		}
		$param['setting'] = $setting;
		$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:$setting->npa_perpage);
		$param['display_perpage'] = $model->display_perpage;
		/*if($model->key_address=="")
			$model->key_address = "จ.กรุงเทพมหานคร";*/

		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		
		$model->current_page = $current_page;
		
		
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_address = $_REQUEST['key_address'];
		/*else	
			$model->key_address = "จ.กรุงเทพมหานคร";*/
		if(isset($_POST['FrontProperty'])){
			$model->attributes=$_POST['FrontProperty'];
			$model->key_address = $_POST['FrontProperty']['key_address'];
		}
		
		$data2 = $model->front_search()->data;
		$data = $model->show_highlight()->data;
		$condi = "";
		if(isset($model->key_address)&& trim($model->key_address)!=""){
			$condi .= " and (provinces.thai_name like '%" . $model->key_address . "%'";
			$condi .= " or district.thai_name like '%" . $model->key_address . "%'";
			$condi .= " or tambol.thai_name like '%" . $model->key_address . "%'";
			$condi .= " or concat('จ.',provinces.thai_name) like '%" . $model->key_address . "%'" ;
			$condi .= ")";
		}
		/*else{
			$condi .= " and (provinces.thai_name like '%จ.กรุงเทพมหานคร%')";
		}*/
		$page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($page<0)
			$page = 0 ;
		
		$criteria=new CDbCriteria;
		$criteria->select = array('t.*,category.product_group as product_group,tambol.thai_name as tambol_name,district.thai_name as district_name,provinces.thai_name as province_name');
		$criteria->join ='LEFT JOIN product_group `category` ON `category`.id = t.product_type ';
		$criteria->join .='LEFT JOIN tambol on tambol.id=t.tambol ';
		$criteria->join .='LEFT JOIN district on district.id=t.district ';
		$criteria->join .='LEFT JOIN provinces on provinces.province_id=t.province ';
		$criteria->condition = " (t.status='1' or t.status='4') and is_show='1' $condi ";
		
	//	$data2 = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $criteria)->queryAll();
		$param['row_amount'] = $model->row_count;
		$param['max_page']  = ceil($param['row_amount'] / $param['display_perpage']);
		if($page>=$param['max_page'])
			$page = $param['max_page']-1;
		$param['page'] = $current_page;
		$criteria->offset = $current_page *  $param['display_perpage'];
		$criteria->limit = $param['display_perpage'];
		$criteria->order = " highlight_order asc ";
		
//		$data2 = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $criteria)->queryAll();
		$mar = new Marquee();
		$mar->part = "NPA";
		$param['marquee'] = $mar->searchByPart()->data;
		$param['setting'] = Setting::model()->findByPk(1);
		$model->key_word ="";
		
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='NPA';
		$tracking->group_id=0;
		$tracking->type=0;
		$tracking->parent_id=0;
		$tracking->save();
		
		$this->render('index',array('data'=>$data,'model'=>$model,'data2'=>$data2,'param'=>$param));
		
		
	}
	
	public function actionPopup()
	{
		$popup = Popup::model()->find('display_npa=1');
		if(count($popup)>0){
        	echo $popup->popup_th;
		}else{
			echo "";
		}
	}
	public function actionShowVdo()
	{
		$id = $_REQUEST['id'];
		$vdo = Vdo::model()->findByPk($id);
		$this->layout = false;
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='NPA';
		$tracking->group_id=0;
		$tracking->type=3;
		$tracking->parent_id=0;
		$tracking->save();
		$this->render('showvdo',array('vdo'=>$vdo));
	}
	public function actionDisclaimer()
	{
		$model = Disclaimer::model()->findByPk(1);
			$this->render('disclamer',array('model'=>$model));
	}
	public function actionSendToFriend()
	{
		$id = $_REQUEST['id'];
		$this->layout = false;
		$model = FrontProperty::model()->findByPk($id);
		
		if(isset($_POST['to']))
		{
			$mail_body = "คุณ " . $_POST['name'] . " [" . $_POST['from'] . "]" ;
			$mail_body .= "<br />ได้ส่งข้อความหาคุณ";
			$mail_body .= "<br /><br />" . Yii::app()->request->baseUrl . "/index.php/npa/detail/" . $id;
			$mail_body .= "<br /><br />" . $_POST['msg'];
			
			$headers="From: {" . $_POST['from'] . "}\r\nReply-To: {" . $_POST['from'] . "}";
			foreach($_POST['to'] as $to){
				mail($to,"NPA Send to friend",$mail_body,$headers);
			}
			Yii::app()->user->setFlash('contact','ข้อความของคุณได้ถูกส่งเข้าเมลล์ของเพื่อนแล้ว.');
			$this->refresh();
			//print_r($_POST);
		}
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='NPA';
		$tracking->group_id=0;
		$tracking->type=4;
		$tracking->parent_id=0;
		$tracking->save();
		$this->render('sendtofriend',array('model'=>$model));
	}
	public function actionMap_Preview()
	{
		$this->layout = false;
		$id= (isset($_GET['id'])?$_GET['id']:"");
		$param['province'] = array();
		$param['district'] = array();
		$param['tambol'] = array();
		if(isset($_GET['type']) && $_GET['type']=="1")
		{		
			$model = Property::model()->findByPk($id);
			$model2 = new Property();

		}else
		{
			$model = FrontProperty::model()->findByPk($id);
			$model2 = new FrontProperty();
		}
		$npaimage = new NpaImage();
		$image = $npaimage->searchByNpaId($id)->data;
		$mod_doc = new DocumentLicense();
		$doc = $mod_doc->searchByNpaId($id)->data;
		$param['setting'] = Setting::model()->findByPk(1);
		
		$this->render('map_preview',array('model'=>$model,'model2'=>$model2,'image'=>$image,'doc'=>$doc,'param'=>$param));
	}
	public function actionAddToCompare()
	{
		 $session=new CHttpSession;
		 $session->open();
		$ssid = $session->getSessionID();
		$model = new Compare();
		$model->sessionid = $ssid;
		$model->itemid = $_POST['id'];
		$model->save();
			echo "OK";
	}
	public function actionDeleteCompare()
	{
		$id= $_REQUEST['id'];
		 $model = Compare::model()->findByPk($id);
		$model->delete();
			echo "OK";
	}
	public function actionDeleteCompareByNpaId()
	{
		$id= $_REQUEST['id'];
		 $session=new CHttpSession;
		 $session->open();
		$ssid = $session->getSessionID();
		 Compare::model()->deleteAll('itemid = ? and sessionid=?' , array($id,$ssid));
			echo "OK";
	}
	
	public function actionDetail()
	{
		$id= (isset($_GET['id'])?$_GET['id']:"");
		$param['province'] = array();
		$param['district'] = array();
		$param['tambol'] = array();
		if(isset($_GET['type']) && $_GET['type']=="1")
		{		
			$model = Property::model()->findByPk($id);
			$mod_doc = new DocumentLicense();
		}else
		{
			$model = FrontProperty::model()->findByPk($id);
			$mod_doc = new FrontDocumentLicense();
		}
		$model2 = new FrontProperty();
		$npaimage = new NpaImage();
		$image = $npaimage->searchByNpaId($id)->data;
		
		$doc = $mod_doc->searchByNpaId($id)->data;
		$param['setting'] = Setting::model()->findByPk(1);
		$view = "detail";
		$setting = Setting::model()->findByPk(1);
		switch($setting->npa_view_template)
		{
			case "1": $view = "detail1";break;
			case "2": $view = "detail2";break;
			case "3": $view = "detail3";break;
			case "4": $view = "detail4";break;
			case "5": $view = "detail5";break;
		}
		$tracking = new Tracking();
		$tracking->ref_id=$model->id;
		$tracking->part='NPA';
		$tracking->group_id=0;
		$tracking->type=0;
		$tracking->parent_id=0;
		$tracking->save();
		$this->render($view,array('model'=>$model,'model2'=>$model2,'image'=>$image,'doc'=>$doc,'param'=>$param));
	}
	
	public function actionMap()
	{
		$this->layout=false;
		$id= (isset($_GET['id'])?$_GET['id']:"");
		if(isset($_GET['type']) && $_GET['type']=="1")
		{		
			$model = Property::model()->findByPk($id);
		}else
		{
			$model = FrontProperty::model()->findByPk($id);
		}
		$this->render('map',array('model'=>$model));
	}
	
	public function actionAcceptDisclaimer()
	{
		$session=new CHttpSession;
		$session->open();
		$session['accept'] = "1";
		if(isset($session['refer']) && $session['refer'] != "" && $session['refer'] != "/")
			$this->redirect($session['refer']);
		else
			$this->redirect(Yii::app()->request->baseUrl . "/index.php/npa/");
	}
	
	public function actionWant()
	{
		$model = new Want();
		if(isset($_POST['ajax']) && $_POST['ajax']==='Want')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		//print_r($_POST);
		if(isset($_POST['Want']))
		{
			$model->attributes=$_POST['Want'];
			if($model->validate()){
				$mail_setting = MailSetting::model()->findByPk(2);
				
				$message        = new YiiMailMessage;
				$body = $mail_setting->mail_body;
				$body .= "<br /><hr /><br />";
				$body .= "คุณ. " . $model->first_name . ' ' . $model->last_name . "<br />" ;
				$body .= "เบอร์โทร " . $model->phone_no . "<br />" ;
				$body .= "Email " . $model->email . "<br />" ;
				$subject = $mail_setting->mail_subject;
				$email = $model->email;
				$message->message->setBody($body, 'text/html','utf-8');
				$message->setSubject('=?utf-8?B?'.base64_encode($subject).'?=');
				//$message->subject = $subject;
				//$message->setSubject($subject);
				$message->addTo($email);
				$message->addCC($mail_setting->sender);
				//$message->addCC($mail_setting->sender);
				//$message->from   = ($mail_setting->sender);
				$message->from   = $mail_setting->sender;
				
				Yii::app()->mail->send($message);
				if(!$model->save())
				{
					print_r($model->getErrors());
				}else{
					
					$user_id = $model->id;
					$m_product  = new FrontProperty();
					$m_product->product_type = $_POST['Want']['product_type'];
					$m_product->start_price = $_POST['Want']['start_price'];
					$m_product->end_price = $_POST['Want']['end_price'];
					$m_product->province = $_POST['Want']['province'];
					$m_product->district = $_POST['Want']['district'];
					$m_product->tambol = $_POST['Want']['tambol'];
					$m_product->road = $_POST['Want']['road'];
					$p_data = $m_product->front_search_all()->data;
					if(count($p_data)>0){
						
						$url = "";
						foreach($p_data as $row)
						{
							$npa_id= $row->id;
							$match = new WantMatch();
							$match->want_id = $user_id;
							$match->npa_id = $npa_id;
							$u = Yii::app()->createAbsoluteUrl('npa/detail/' . $npa_id);
							$url .= " <a href='$u' target='_blank'>$u</a> <br /><br />" ;
							$match->save();
						//	echo $row->id . "<br />";
							
						}
						$mail_setting = MailSetting::model()->findByPk(2);
				
						$message        = new YiiMailMessage;
						$body = $mail_setting->mail_body;
						$body .= "<br /><hr />จากที่<br />";
						$body .= "คุณ. " . $model->first_name . ' ' . $model->last_name . "<br />" ;
						$body .= "เบอร์โทร " . $model->phone_no . "<br />" ;
						$body .= "Email " . $model->email . "<br />" ;
						$body .= "ได้ฝากความต้องการทรัพย์ไว้กับทางบริษัท <br />ทางบริษัทมีสินทรัพย์ที่เหมาะสมสำหรับคุณ ดังนี้ ";
						$body .= "<br /><br />";
						$body .= $url;
						$subject = $mail_setting->mail_subject;
						$email = $model->email;
						$message->message->setBody($body, 'text/html','utf-8');
						$message->setSubject('=?utf-8?B?'.base64_encode($subject).'?=');
						//$message->subject = $subject;
						//$message->setSubject($subject);
						$message->addTo($email);
						$message->addCC($mail_setting->sender);
						//$message->addCC($mail_setting->sender);
						//$message->from   = ($mail_setting->sender);
						$message->from   = $mail_setting->sender;
						
						Yii::app()->mail->send($message);
						Want::model()->updateByPk($user_id,array('status'=>1));
					}
					$this->redirect(Yii::app()->request->baseUrl . "/index.php/npa/");
				}
			}
			
		}
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='NPA';
		$tracking->group_id=0;
		$tracking->type=2;
		$tracking->parent_id=0;
		$tracking->save();
		$this->render('want',array('model'=>$model));
	}
	
	public function actionCompare()
	{
		  $this->layout = false;
		 $session=new CHttpSession;
		 $session->open();
		$ssid = $session->getSessionID();
		 $model = new Compare();
		 $model->sessionid = $ssid;
		 $data = $model->findbysessionid()->data;

		$this->render('compare',array('model'=>$model,'data'=>$data));
	}
	public function actionBeforeCompare()
	{
		 $this->layout = false;
		 $session=new CHttpSession;
		 $session->open();
		$ssid = $session->getSessionID();
		 $model = new Compare();
		 $model->sessionid = $ssid;
		 $data = $model->findbysessionid()->data;

		$this->render('beforecompare',array('model'=>$model,'data'=>$data));
	}
	
	public function actionMapSearch()
	{
		$model = FrontProperty::model();
		if(isset($_POST['FrontProperty']))
			$model->attributes=$_POST['FrontProperty'];
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];	
		$param['data3'] = $model->front_search_all()->data;
		$model = new FrontProperty();
		$param['province'] = array();
		$param['district'] = array();
		$param['tambol'] = array();
		$setting = Setting::model()->findByPk(1);
		$param['setting'] = $setting;
		$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:$setting->npa_perpage);
		$param['display_perpage'] = 20;
		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		if(isset($_POST['FrontProperty']))
			$model->attributes=$_POST['FrontProperty'];
		
		$model->current_page = $current_page;
		if($model->continent != ""){
			$param['province'] = CHtml::listData(Provinces::model()->findAll("continent_id=:continent_id", array(':continent_id'=>$model->continent,)), 'province_id', 'thai_name');
		}
		if($model->province != ""){
			$param['district'] = CHtml::listData(District::model()->findAll("province_id=:province_id", array(':province_id'=>$model->province,)), 'id', 'thai_name');
		}
		if($model->district != ""){
			$param['tambol'] = CHtml::listData(Tambol::model()->findAll("district_id=:district_id", array(':district_id'=>$model->district,)), 'id', 'thai_name');
		}
		
		$data = $model->front_search()->data;
		$param['row_amount'] = $model->row_count;

		if(isset($_POST['rdshowtype'])){
			if($_POST['rdshowtype']=="1")
				$page = "search1";
			else if($_POST['rdshowtype']=="2")
				$page = "search2";
			else if($_POST['rdshowtype']=="3")
				$page = "map_search2";
			else if($_POST['rdshowtype']=="4")
				$page = "search3";
		}
		$param['max_page']  = ceil($param['row_amount'] / $model->display_perpage);
		if($current_page>=$param['max_page'])
			$current_page = $param['max_page']-1;
		
		$param['page'] = $current_page;
		$param['marquee'] = Marquee::model()->searchByPart()->data;
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='NPA';
		$tracking->group_id=3;
		$tracking->type=1;
		$tracking->parent_id=0;
		$tracking->save();
		
		$this->render('map_search2',array('model'=>$model,'data'=>$data,'param'=>$param));
	}
	
	public function actionShowSearch()
	{

		$criteria=new CDbCriteria;
		$model = FrontProperty::model();
		
		if(isset($_POST['FrontProperty']))
			$model->attributes=$_POST['FrontProperty'];
		if(isset($_REQUEST['key_address']) && $_REQUEST['key_address']!="")
			$model->key_word = $_REQUEST['key_address'];	
		$param['data3'] = $model->front_search_all()->data;
		$model = new FrontProperty();
		
		$model = FrontProperty::model();
		$setting = Setting::model()->findByPk(1);
		$param['setting'] = $setting;
		$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:$setting->npa_perpage);
		$param['display_perpage'] = 20;
		$current_page = (isset($_REQUEST['page'])&&$_REQUEST['page']!=""?($_REQUEST['page']-1):0);
		if($current_page<0)
			$current_page = 0 ;
		if(isset($_REQUEST['perpage']) && $_REQUEST['perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['perpage'];
		}
		if(isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage']!="")
		{
			$param['display_perpage'] = $_REQUEST['display_perpage'];
		}
		
		$page = "search1";
		$param['province'] = array();
		$param['district'] = array();
		$param['tambol'] = array();
		
		if(isset($_POST['FrontProperty']))
			$model->attributes=$_POST['FrontProperty'];
		
		$model->current_page = $current_page;
		if($model->continent != ""){
			$param['province'] = CHtml::listData(Provinces::model()->findAll("continent_id=:continent_id", array(':continent_id'=>$model->continent,)), 'province_id', 'thai_name');
		}
		if($model->province != ""){
			$param['district'] = CHtml::listData(District::model()->findAll("province_id=:province_id", array(':province_id'=>$model->province,)), 'id', 'thai_name');
		}
		if($model->district != ""){
			$param['tambol'] = CHtml::listData(Tambol::model()->findAll("district_id=:district_id", array(':district_id'=>$model->district,)), 'id', 'thai_name');
		}

		$data = $model->front_search()->data;
		$param['row_amount'] = $model->row_count;

		if(isset($_POST['rdshowtype'])){
			if($_POST['rdshowtype']=="1")
				$page = "search1";
			else if($_POST['rdshowtype']=="2")
				$page = "search2";
			else if($_POST['rdshowtype']=="3")
				$page = "map_search2";
			else if($_POST['rdshowtype']=="4")
				$page = "search3";
		}
		$param['max_page']  = ceil($param['row_amount'] / ($model->display_perpage==0?1:$model->display_perpage));
		if($current_page>=$param['max_page'])
			$current_page = $param['max_page']-1;
		
		$param['page'] = $current_page;
		$mar = new Marquee();
		$mar->part = 'NPA';
		
		$param['marquee'] = $mar->searchByPart()->data;
		$tracking = new Tracking();
		$tracking->ref_id=0;
		$tracking->part='NPA';
		$tracking->group_id=(isset($_POST['rdshowtype'])?$_POST['rdshowtype']:"1");
		$tracking->type=1;
		$tracking->parent_id=0;
		$tracking->save();
		
		$this->render($page,array('model'=>$model,'data'=>$data,'param'=>$param));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	function thai_date(){  
$time = time();
	$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");  
	$thai_month_arr=array(  
		"0"=>"",  
		"1"=>"มกราคม",  
		"2"=>"กุมภาพันธ์",  
		"3"=>"มีนาคม",  
		"4"=>"เมษายน",  
		"5"=>"พฤษภาคม",  
		"6"=>"มิถุนายน",   
		"7"=>"กรกฎาคม",  
		"8"=>"สิงหาคม",  
		"9"=>"กันยายน",  
		"10"=>"ตุลาคม",  
		"11"=>"พฤศจิกายน",  
		"12"=>"ธันวาคม"                    
	);   
    $thai_date_return="วัน".$thai_day_arr[date("w",$time)];  
    $thai_date_return.= "ที่ ".date("j",$time);  
    $thai_date_return.=" เดือน".$thai_month_arr[date("n",$time)];  
    $thai_date_return.= " พ.ศ.".(date("Yํ",$time)+543);  
    $thai_date_return.= "  ".date("H:i",$time)." น.";  
    return $thai_date_return;  
} 
}