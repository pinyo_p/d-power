<?php

ob_start();

class AdminController extends Controller {

    /**
     * Declares class-based actions.
     */
    public $layout = '//layouts/admin/main';

    function init() {
        /*
          parent::init();
          $app = Yii::app();
          if (isset($_POST['lang']))
          {
          $app->language = $_POST['lang'];
          $app->session['_lang'] = $app->language;
          }
          else if (isset($app->session['_lang']))
          {
          $app->language = $app->session['_lang'];
          }

          if(strtolower($app->language)=="en")
          Yii::app()->setLanguage("en");
          else
          Yii::app()->setLanguage("th");

         */
    }

    public function actions() {

        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $headers = "From: {$model->email}\r\nReply-To: {$model->email}";
                mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {

        $model = new LoginForm;
        $this->layout = false;
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/');
        }
        // display the login form
        $this->render('login', array('model' => $model));

        /* $this->layout = false;
          $model = new LoginForm();
          $model->username='admin';
          $model->password='admin';
          $model->login();
          $this->redirect(Yii::app()->request->baseUrl . "/index.php/admin/");
         * */
    }

    public function actionMemberPolicy() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'MemberPolicy'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionCredit() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'credit'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionReward() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'reward'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionServiceDetail() {
        $id = $_GET['id'];
        $model = Service::model()->findByPk($id);
        $this->render('service_detail', array('model' => $model));
    }

    public function actionContactDetail() {
        $id = $_GET['id'];
        $model = Contact::model()->findByPk($id);
        $this->render('contact_detail', array('model' => $model));
    }

    public function actionQuotationForm() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'quotationform'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionPromotionx() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'promotion'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionRepair() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'repair'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionFaq() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'faq'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionFooter() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'Footer'));
        if (isset($_POST['Content'])) {
            if (!$this->checkUserPermission('footer', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model->attributes = $_POST['Content'];
            $model->title_1 = 'footer';
            $model->title_2 = 'footer';
            $model->sort_order = 0;
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionSitemap() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'Sitemap'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionHistory() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'History'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionPopup() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'popup'));
        if (isset($_POST['Content'])) {
            if (!$this->checkUserPermission('popup', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

            $model->attributes = $_POST['Content'];

            $model->title_1 = 'popup';
            $model->title_2 = 'popup';
            $model->sort_order = 0;

            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('popup', array('model' => $model));
    }

    public function actionIntro() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'intro'));
        if (isset($_POST['Content'])) {
            if (!$this->checkUserPermission('intro', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model->attributes = $_POST['Content'];
            $model->title_1 = 'intro';
            $model->title_2 = 'intro';
            $model->sort_order = 0;
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('intro', array('model' => $model));
    }

    public function actionTrainingDesign() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'training'));
        $model2 = Content::model()->find('content_code=:content_code', array(':content_code' => 'training2'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        if (isset($_POST['Content2_content_th'])) {
            Content::model()->updateByPk($model2->id, array('content_en' => $_POST['Content2_content_en'], 'content_th' => $_POST['Content2_content_th']));
        }
        $model2 = Content::model()->find('content_code=:content_code', array(':content_code' => 'training2'));
        $t = new Training();
        $data = $t->front_show()->data;
        $this->render('trainingdesign', array('model' => $model, 'model2' => $model2, 'data' => $data));
    }

    public function actionDayList() {
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('day', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Day::model()->deleteByPk($id);
            }
        }
        $model = new Day();
        $data = $model->search()->data;
        $this->render('daylist', array('model' => $model, 'data' => $data));
    }

    public function actionTrainingList() {
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Training::model()->deleteByPk($id);
            }
        }
        $model = new Training();
        $data = $model->search()->data;
        $this->render('traininglist', array('model' => $model, 'data' => $data));
    }

    public function actionTraining() {

        if (isset($_GET['id']))
            $model = Training::model()->findByPk($_GET['id']);
        else
            $model = new Training();
        if (isset($_POST['Training'])) {
            $model->attributes = $_POST['Training'];

            if ($model->validate() && $model->save()) {

                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/traininglist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('training', array('model' => $model));
    }

    public function actiondeleteTraining() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            Training::model()->deleteByPk($id);
            echo "OK";
        }
    }

    public function actionDay() {

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('day', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = Day::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('day', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new Day();
        }

        if (isset($_POST['Day'])) {
            $model->attributes = $_POST['Day'];

            if ($model->validate() && $model->save()) {

                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/daylist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('day', array('model' => $model));
    }

    public function actiondeleteDay() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('day', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                Day::model()->deleteByPk($id);
                echo "OK";
            }
        }
    }

    public function actionrecoveryuserpwd() {
        if (isset($_POST['username'])) {
            $model = new User();
            $model->username = $_REQUEST["username"];
            $data = $model->searchByUsername()->data;
            if (count($data) > 0) {
                echo "รหัสผ่านคือ : " . $data[0]->password;
            } else {
                echo "ไม่พบชื่อ Username ที่ระบุ\nกรุณาตรวจสอบใหม่";
            }
        }
    }

    public function actionBannerDayList() {
        $model = new BannerDay();

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('banner_day', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                BannerDay::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['BannerDay'])) {
                $model->attributes = $_POST['BannerDay'];
                $model->day_id = $_POST['BannerDay']['day_id'];
            }
        }
        $model->display_perpage = "30";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        if (isset($_POST['act']) && $_POST['act'] == "search")
            $data = $model->search()->data;
        else
            $data = $model->searchAll()->data;

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;
        $banner = Content::model()->find("content_code='banner_day'");
        $param['banner'] = $banner;

        $this->render('bannerdaylist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionBannerDay() {

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('banner_day', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = BannerDay::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('banner_day', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new BannerDay();
        }

        if (isset($_POST['BannerDay'])) {
            $model->attributes = $_POST['BannerDay'];
            if ($model->validate() && $model->save()) {
                if (isset($_FILES['img_src']) && $_FILES['img_src']['name'] != "") {
                    $file_name = $model->day_id . '_' . $model->id . '_' . $_FILES['img_src']['name'];
                    $model->img_src = $file_name;
                    copy($_FILES['img_src']['tmp_name'], Yii::app()->basePath . '/../images/banner_day/' . $file_name);
                    $model->save();
                }

                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/BannerDayList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }

        $this->render('bannerday', array('model' => $model));
    }

    public function actionchooseBannerDay() {
        if (!$this->checkUserPermission('banner_day', '4')) {
            echo "DENIED";
        } else {
            $id = $_REQUEST['id'];
            $banner = BannerDay::model()->findByPk($id);
            Content::model()->updateAll(array("content_2" => $banner->img_src), "content_code='banner_day'");
            echo "OK";
        }
    }

    public function actionupdateBannerDay() {
        $status = $_REQUEST['status'];
        $s_date = $_REQUEST['s_date'];
        $e_date = $_REQUEST['e_date'];
        Content::model()->updateAll(array("attr1" => $status, "attr2" => $s_date, "attr3" => $e_date), "content_code='banner_day'");
        echo "OK";
    }

    public function actiondeleteBannerDay() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('banner_day', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                BannerDay::model()->deleteByPk($id);
                echo "OK";
            }
        }
    }

    public function actionContactUs() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'ContactUs'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionOrderShippingDetail() {
        $id = $_REQUEST['id'];
        $m = new MemberOrder();
        $m->id = $id;
        $model = $m->search_by_id()->data;
        $this->layout = false;
        $this->render('shippingdetail', array('model' => $model[0]));
    }

    public function actionPolicy() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'Policy'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionSalePolicy() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'SalePolicy'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionProtection() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'Protection'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionShipping() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'Shipping'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionBannerList() {
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('banner', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                $modelBanner = Banner::model()->findByPk($id);
                $pathName = "/images/banner/" . $id . "/";
                $this->deleteFile($pathName, $modelBanner->img_src);
                Banner::model()->deleteByPk($id);
            }
        }
        $model = new Banner();
        $data = $model->search()->data;
        $this->render('bannerlist', array('model' => $model, 'data' => $data));
    }

    public function actionBannerSlideList() {
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('banner_slide', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                $modelBanner = BannerIntro::model()->findByPk($id);
                $pathName = "/images/banner_intro/" . $id . "/";
                $this->deleteFile($pathName, $modelBanner->img_src);
                BannerIntro::model()->deleteByPk($id);
            }
        }
        $model = new BannerIntro();
        $data = $model->search()->data;

        $this->render('bannerslidelist', array('model' => $model, 'data' => $data));
    }

    public function actionBanner() {

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('banner', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = Banner::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('banner', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new Banner();
        }
        if (isset($_POST['Banner'])) {
            $model->attributes = $_POST['Banner'];

            if ($model->validate() && $model->save()) {
                if (isset($_FILES['pic']) && $_FILES['pic']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic']['name'];
                    $model->img_src = $file_name;

                    $path = Yii::app()->basePath . "/../images/banner/" . $model->id;
                    if (!file_exists($path))
                        mkdir($path, 0777);

                    copy($_FILES['pic']['tmp_name'], Yii::app()->basePath . '/../images/banner/' . $model->id . '/' . $file_name);
                    $model->save();
                }
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/bannerlist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('banner', array('model' => $model));
    }

    public function actionShipmentRateList() {
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('shipment_rate', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                ShippingCost::model()->deleteByPk($id);
            }
        }

        $shipType = 'in_reg';
        if (isset($_POST) && count($_POST) > 0) {
            $shipType = $_POST["ddlType"];
        } else {
            if (isset($_GET["shiptype"])) {
                $_POST["ddlType"] = $_GET["shiptype"];
            } else {
                $_POST["ddlType"] = $shipType;
            }
        }

        $model = new ShippingCost();
        $data = $model->searchByType($_POST["ddlType"])->data;
        $this->render('shipmentratelist', array('model' => $model, 'data' => $data));
    }

    public function actionShipmentRate() {
        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('shipment_rate', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = ShippingCost::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('shipment_rate', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new ShippingCost();
            $model->weight_from = 0;
            $model->weight_to = 0;
            $model->sort_order = 0;
            $model->cost_price = 0;
        }
        if (isset($_POST['ShippingCost'])) {
            $model->attributes = $_POST['ShippingCost'];
            $model->shipping_type = $_GET["shiptype"];
            if ($model->validate() && $model->save()) {
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/shipmentratelist?shiptype=' . $_GET["shiptype"]);
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('shipmentrate', array('model' => $model));
    }

    public function actiondeleteShipmentRate() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('shipment_rate', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                ShippingCost::model()->deleteByPk($id);
                echo "OK";
            }
        }
    }

    public function actionBannerSlide() {
        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('banner_slide', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = BannerIntro::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('banner_slide', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new BannerIntro();
        }

        if (isset($_POST['BannerIntro'])) {
            $model->attributes = $_POST['BannerIntro'];

            if ($model->validate() && $model->save()) {
                if (isset($_FILES['pic']) && $_FILES['pic']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic']['name'];
                    $model->img_src = $file_name;

                    $path = Yii::app()->basePath . "/../images/banner_intro/" . $model->id;
                    if (!file_exists($path))
                        mkdir($path, 0777);

                    copy($_FILES['pic']['tmp_name'], Yii::app()->basePath . '/../images/banner_intro/' . $model->id . '/' . $file_name);
                    $model->save();
                }
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/bannerslidelist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('bannerslide', array('model' => $model));
    }

    public function actiondeleteBanner() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('banner', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];

                $model = Banner::model()->findByPk($id);

                $pathName = "/images/banner/" . $id . "/";
                $this->deleteFile($pathName, $model->img_src);
                Banner::model()->deleteByPk($id);

                echo "OK";
            }
        }
    }

    public function actiondeleteBannerSlide() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('banner_slide', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];

                $model = BannerIntro::model()->findByPk($id);

                $pathName = "/images/banner_intro/" . $id . "/";
                $this->deleteFile($pathName, $model->img_src);
                BannerIntro::model()->deleteByPk($id);

                echo "OK";
            }
        }
    }

    public function actionBrandLink() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'BrandLink'));
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
                $model->save();
            } else {
                echo "Error";
            }
        }
        $this->render('member_policy', array('model' => $model));
    }

    public function actionPartner() {
        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('partner', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = Partner::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('partner', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new Partner();
            $model->sort_order = 0;
        }

        if (isset($_POST['Partner'])) {
            $model->attributes = $_POST['Partner'];

            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->save()) {
                if (isset($_FILES['logo']) && $_FILES['logo']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['logo']['name'];
                    $model->logo = $file_name;
                    copy($_FILES['logo']['tmp_name'], Yii::app()->basePath . '/../images/partner/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['map']) && $_FILES['map']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['map']['name'];
                    $model->map = $file_name;
                    copy($_FILES['map']['tmp_name'], Yii::app()->basePath . '/../images/partner/' . $file_name);
                    $model->save();
                }
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/partnerlist');
            } else {
                echo "Error";
            }
        }
        $this->render('partner', array('model' => $model));
    }

    public function actionPartnerList() {
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('partner', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Partner::model()->deleteByPk($id);
            }
        }
        $model = new Partner();
        $data = $model->search()->data;
        $this->render('partnerlist', array('data' => $data));
    }

    public function actiondeletePartner() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('partner', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                Partner::model()->deleteByPk($id);
                echo trim("OK");
            }
        }
    }

    public function actionBranchList() {
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('branch', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Branch::model()->deleteByPk($id);
            }
        }
        $model = new Branch();
        $data = $model->search()->data;

        $this->render('branchlist', array('model' => $model, 'data' => $data));
    }

    public function actionBranch() {

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('branch', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = Branch::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('branch', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new Branch();
        }

        if (isset($_POST['Branch'])) {
            $model->attributes = $_POST['Branch'];

            if ($model->validate() && $model->save()) {

                if (isset($_FILES['main_image']) && $_FILES['main_image']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['main_image']['name'];
                    $model->main_image = $file_name;
                    copy($_FILES['main_image']['tmp_name'], Yii::app()->basePath . '/../images/branch/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['map_image']) && $_FILES['map_image']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['map_image']['name'];
                    $model->map_image = $file_name;
                    copy($_FILES['map_image']['tmp_name'], Yii::app()->basePath . '/../images/branch/' . $file_name);
                    $model->save();
                }
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/branchlist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('branch', array('model' => $model));
    }

    public function actiondeleteBranch() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('branch', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                Branch::model()->deleteByPk($id);
                echo "OK";
            }
        }
    }

    public function actionBankList() {

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Bank::model()->deleteByPk($id);
            }
        }
        $model = new Bank();
        $data = $model->search()->data;
        $this->render('banklist', array('model' => $model, 'data' => $data));
    }

    public function actionBank() {

        if (isset($_GET['id']))
            $model = Bank::model()->findByPk($_GET['id']);
        else
            $model = new Bank();
        if (isset($_POST['Bank'])) {
            $model->attributes = $_POST['Bank'];

            if ($model->validate() && $model->save()) {

                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/banklist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('bank', array('model' => $model));
    }

    public function actiondeleteBank() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            Bank::model()->deleteByPk($id);
            echo "OK";
        }
    }

    public function actionContentList() {
        $model = new Content();

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('content', '3')) {
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            } else {
                $pid = $_POST['p_id'];
                foreach ($pid as $id) {
                    Content::model()->deleteByPk($id);
                }
            }
        }

        $model->display_perpage = "10";
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }

        $model->current_page = $current_page;

        $data = $model->searchForManage('', '0', 'all')->data;

        if (isset($_POST['act']) && $_POST['act'] == "search") {
            $searchTitle = $_POST["txtTitle"];
            $searchCode = $_POST["ddlContentCode"];
            $searchStatus = $_POST["ddlStatus"];
            $data = $model->searchForManage($searchTitle, $searchCode, $searchStatus)->data;
        }

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;


        // Content code
        $param["content_code"] = Content::model()->findAll(array(
            'select' => 't.content_code',
            'condition' => "t.content_code not in ('News','banner_day','popup','intro','Footer')",
            'group' => 't.content_code',
            'distinct' => true,
        ));

        $this->render('contentlist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionContent() {
        $model = new Content();

        if (isset($_GET['id'])) {
            $model = Content::model()->findByPk($_GET['id']);
            if (!$this->checkUserPermission('content', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
        } else {
            $model->sort_order = 0;
            if (!$this->checkUserPermission('content', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
        }

        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];

            if ($model->validate() && $model->save()) {
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/ContentList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }

        $this->render('content', array('model' => $model));
    }

    public function actionDeleteContent() {
        if (isset($_POST['id'])) {
            if (!isset($_REQUEST["code"])) {
                if (!$this->checkUserPermission('content', '3')) {
                    echo "DENIED";
                } else {
                    $id = $_POST['id'];
                    Content::model()->deleteByPk($id);
                    echo "OK";
                }
            } else {
                switch ($_REQUEST["code"]) {
                    case "news":
                        if (!$this->checkUserPermission('news', '3')) {
                            echo "DENIED";
                        } else {
                            $id = $_POST['id'];
                            Content::model()->deleteByPk($id);
                            echo "OK";
                        }
                        break;
                }
            }
        }
    }

    public function actionContentNewsList() {
        $model = new Content();

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('news', '3')) {
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            } else {
                $pid = $_POST['p_id'];
                foreach ($pid as $id) {
                    Content::model()->deleteByPk($id);
                }
            }
        }

        $model->display_perpage = "10";
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }

        $model->current_page = $current_page;

        $data = $model->searchNewsForManage('', '')->data;

        if (isset($_POST['act']) && $_POST['act'] == "search") {
            $searchTitle = $_POST["txtTitle"];
            $searchContent = $_POST["txtContent"];
            $data = $model->searchNewsForManage($searchTitle, $searchContent)->data;
        }

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('contentnewslist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionNewsList() {

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('news', '3')) {
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            } else {
                $pid = $_POST['p_id'];
                foreach ($pid as $id) {
                    Content::model()->deleteByPk($id);
                }
            }
        }
        $model = new Content();
        $model->content_code = 'News';
        $data = $model->searchByCode()->data;
        $this->render('newslist', array('model' => $model, 'data' => $data));
    }

    public function actionNews() {
        $model = new Content();

        if (isset($_GET['id'])) {
            $model = Content::model()->findByPk($_GET['id']);
            if (!$this->checkUserPermission('news', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
        } else {
            $model->sort_order = 0;
            if (!$this->checkUserPermission('news', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
        }

        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            $model->content_code = 'News';
            $model->status = '1';
            if ($model->validate() && $model->save()) {
                if (isset($_FILES['files']) && $_FILES['files']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['files']['name'];
                    $model->attr1 = $file_name;
                    copy($_FILES['files']['tmp_name'], Yii::app()->basePath . '/../images/content/' . $file_name);
                    $model->save();
                }
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/contentnewslist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('news', array('model' => $model));
    }

    public function actiondeleteNews() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('news', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                Content::model()->deleteByPk($id);
                echo "OK";
            }
        }
    }

    #------------------------- Photo Gallery -------------------#

    public function actionAlbumPictureList() {
        $model = new Gallery();

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('album_photo', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Gallery::model()->deleteByPk($id);

                $modelGallerPicture = new GalleryImage();
                $dataGalleryPicture = $modelGallerPicture->searchByGalleryID($id)->data;
                if (count($dataGalleryPicture) > 0) {
                    foreach ($dataGalleryPicture as $picture) {
                        GalleryImage::model()->deleteByPk($picture->id);
                    }
                }
            }
        }

        $model->display_perpage = "10";
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }

        $model->current_page = $current_page;

        $data = $model->searchGalleryPictureForManage()->data;

        if (isset($_POST['act']) && $_POST['act'] == "search") {
            $model->searchName = $_POST["txtTitle"];
            $data = $model->searchGalleryPictureForManage()->data;
        }

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('albumpicturelist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionAlbumPicture() {
        $model = new Gallery();
        $model->code = 'gallery_image';

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('album_photo', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = Gallery::model()->findByPk($_GET['id']);
        } else {
            if (!$this->checkUserPermission('album_photo', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model->is_index = 1;
        }

        if (isset($_POST['Gallery'])) {
            $model->attributes = $_POST['Gallery'];

            if ($model->validate() && $model->save()) {
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/AlbumPictureList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }

        $this->render('albumpicture', array('model' => $model));
    }

    public function actiondeleteAlbumPicture() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('album_photo', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                Gallery::model()->deleteByPk($id);

                $modelGallerPicture = new GalleryImage();
                $dataGalleryPicture = $modelGallerPicture->searchByGalleryID($id)->data;
                if (count($dataGalleryPicture) > 0) {
                    foreach ($dataGalleryPicture as $picture) {
                        GalleryImage::model()->deleteByPk($picture->id);
                    }
                }
                echo "OK";
            }
        }
    }

    public function actionAlbumPictureManagePicture() {
        $galleryid = (isset($_REQUEST['galleryid']) ? $_REQUEST['galleryid'] : "0");

        $model = new GalleryImage();

        if (isset($_FILES['img_gal'])) {
            if (!$this->checkUserPermission('album_photo', '4'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

            $idxControl = 0;
            foreach ($_FILES['img_gal']['tmp_name'] as $key => $tmp_name) {

                $file_name = $_FILES['img_gal']['name'][$key];
                $file_temp = $_FILES['img_gal']['tmp_name'][$key];

                if ($file_name != "") {
                    $fileName = $this->uploadFile2("/images/album/" . $galleryid . "/", $file_name, $file_temp, "gal_");
                    if ($fileName != "") {
                        $photo = new GalleryImage();
                        $photo->gallery_id = $galleryid;
                        $photo->file_name = $fileName;
                        $photo->file_type = 'file';
                        $photo->is_cover = 0;
                        $photo->name_1 = $_POST["img_title"][$idxControl];
                        $photo->name_2 = $_POST["img_title"][$idxControl];
                        $photo->description_1 = $_POST["img_desc"][$idxControl];
                        $photo->description_2 = $_POST["img_desc"][$idxControl];
                        $photo->save();
                    }
                }
                $idxControl ++;
            }
        }

        $photos = $model->searchByGalleryIDForManage($galleryid)->data;

        $this->render('albumpicturemanagepicture', array('photos' => $photos));
    }

    public function actiondeletePhoto() {
        if (!$this->checkUserPermission('album_photo', '5'))
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

        $this->layout = false;

        $galleryid = (isset($_REQUEST['galleryid']) ? $_REQUEST['galleryid'] : "0");
        $id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : "0");


        $photo = GalleryImage::model()->findByPk($id);

        $pathName = "/images/album/" . $galleryid . "/";
        if (isset($photo->file_name)) {
            $this->deleteFile($pathName, $photo->file_name);
            GalleryImage::model()->deleteByPk($id);
        }

        header('Location:' . Yii::app()->request->baseUrl . '/index.php/admin/AlbumPictureManagePicture/galleryid/' . $galleryid);
    }

    public function actionupdatePhotoCover() {
        $this->layout = false;

        $galleryid = (isset($_REQUEST['galleryid']) ? $_REQUEST['galleryid'] : "0");
        $id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : "0");

        GalleryImage::model()->updateAll(array('is_cover' => '0'), "gallery_id='" . $galleryid . "' and id<>" . $id);

        $photo = GalleryImage::model()->findByPk($id);
        $isCover = 1;
        if ($photo->is_cover == 1)
            $isCover = 0;
        $photo->is_cover = $isCover;
        $photo->save();

        header('Location:' . Yii::app()->request->baseUrl . '/index.php/admin/AlbumPictureManagePicture/galleryid/' . $galleryid);
    }

    #------------------------- Video Gallery -------------------#

    public function actionAlbumVideoList() {
        $model = new Gallery();

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('album_video', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Gallery::model()->deleteByPk($id);

                $modelGallerVideo = new GalleryVideo();
                $dataGalleryVideo = $modelGallerVideo->searchByGalleryID($id)->data;
                if (count($dataGalleryVideo) > 0) {
                    foreach ($dataGalleryVideo as $video) {
                        GalleryVideo::model()->deleteByPk($video->id);
                    }
                }
            }
        }

        $model->display_perpage = "10";
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }

        $model->current_page = $current_page;

        $data = $model->searchGalleryVideoForManage()->data;

        if (isset($_POST['act']) && $_POST['act'] == "search") {
            $model->searchName = $_POST["txtTitle"];
            $data = $model->searchGalleryVideoForManage()->data;
        }

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('albumvideolist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionAlbumVideo() {
        $model = new Gallery();
        $model->code = 'gallery_video';

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('album_video', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = Gallery::model()->findByPk($_GET['id']);
        } else {
            if (!$this->checkUserPermission('album_video', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model->is_index = 1;
        }

        if (isset($_POST['Gallery'])) {
            $model->attributes = $_POST['Gallery'];

            if ($model->validate() && $model->save()) {
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/AlbumVideoList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }

        $this->render('albumvideo', array('model' => $model));
    }

    public function actiondeleteAlbumVideo() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('album_video', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                Gallery::model()->deleteByPk($id);

                $modelGallerVideo = new GalleryVideo();
                $dataGalleryVideo = $modelGallerVideo->searchByGalleryID($id)->data;
                if (count($dataGalleryVideo) > 0) {
                    foreach ($dataGalleryVideo as $video) {
                        GalleryVideo::model()->deleteByPk($video->id);
                    }
                }
                echo "OK";
            }
        }
    }

    public function actionAlbumVideoManageVideo() {
        $galleryid = (isset($_REQUEST['galleryid']) ? $_REQUEST['galleryid'] : "0");

        $model = new GalleryVideo();

        if (isset($_POST['img_gal'])) {
            if (!$this->checkUserPermission('album_video', '4'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

            foreach ($_POST['img_gal'] as $url) {
                if ($url != "") {
                    parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
                    $youtube_id = $my_array_of_vars['v'];

                    if ($youtube_id != "") {
                        $vid = new GalleryVideo();
                        $vid->gallery_id = $galleryid;
                        $vid->is_cover = 0;
                        $vid->url = $youtube_id;
                        $vid->save();
                    }
                }
            }
        }

        $videos = $model->searchByGalleryIDForManage($galleryid)->data;
        $this->render('albumvideomanagevideo', array('videos' => $videos));
    }

    public function actiondeleteVideo() {
        if (!$this->checkUserPermission('album_video', '5'))
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

        $this->layout = false;

        $galleryid = (isset($_REQUEST['galleryid']) ? $_REQUEST['galleryid'] : "0");
        $id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : "0");

        GalleryVideo::model()->deleteByPk($id);

        header('Location:' . Yii::app()->request->baseUrl . '/index.php/admin/AlbumVideoManageVideo/galleryid/' . $galleryid);
    }

    public function actionupdateVideoCover() {
        $this->layout = false;

        $galleryid = (isset($_REQUEST['galleryid']) ? $_REQUEST['galleryid'] : "0");
        $id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : "0");

        GalleryVideo::model()->updateAll(array('is_cover' => '0'), "gallery_id='" . $galleryid . "' and id<>" . $id);

        $video = GalleryVideo::model()->findByPk($id);
        $isCover = 1;
        if ($video->is_cover == 1)
            $isCover = 0;
        $video->is_cover = $isCover;
        $video->save();

        header('Location:' . Yii::app()->request->baseUrl . '/index.php/admin/AlbumVideoManageVideo/galleryid/' . $galleryid);
    }

    #------------------------- Promotion -----------------------#

    public function actionPromotionList() {

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Content::model()->deleteByPk($id);
            }
        }
        $model = new Content();
        $model->content_code = 'Promotion';
        $data = $model->searchByCode()->data;
        $this->render('promotionlist', array('model' => $model, 'data' => $data));
    }

    public function actionPromotion() {

        if (isset($_GET['id']))
            $model = Content::model()->findByPk($_GET['id']);
        else
            $model = new Content();
        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];
            $model->content_code = 'Promotion';
            if ($model->validate() && $model->save()) {
                if (isset($_FILES['files']) && $_FILES['files']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['files']['name'];
                    $model->attr1 = $file_name;
                    copy($_FILES['files']['tmp_name'], Yii::app()->basePath . '/../images/content/' . $file_name);
                    $model->save();
                }
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/promotionlist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('promotion', array('model' => $model));
    }

    public function actiondeletePromotion() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            Content::model()->deleteByPk($id);
            echo "OK";
        }
    }

    #----------------- End Promotion --------------------#

    public function actionBrandGroupList() {

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                BrandGroup::model()->deleteByPk($id);
            }
        }
        $model = new BrandGroup();
        $data = $model->search()->data;
        $this->render('brandgrouplist', array('model' => $model, 'data' => $data));
    }

    public function actionBrandGroup() {

        if (isset($_GET['id']))
            $model = BrandGroup::model()->findByPk($_GET['id']);
        else
            $model = new BrandGroup();
        if (isset($_POST['BrandGroup'])) {
            $model->attributes = $_POST['BrandGroup'];
            if ($model->validate() && $model->save()) {

                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/BrandGroupList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('brandgroup', array('model' => $model));
    }

    public function actiondeleteBrandGroup() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            BrandGroup::model()->deleteByPk($id);
            echo "OK";
        }
    }

    public function actionProductGroupList() {
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('product_category', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                ProductGroup::model()->deleteByPk($id);
            }
        }
        $model = new ProductGroup();
        if (isset($_POST['ProductGroup']['brand_id']) && $_POST['ProductGroup']['brand_id'] != "") {
            $model->brand_id = $_POST['ProductGroup']['brand_id'];
        }

        $model->display_perpage = "10";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->searchAll()->data;

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('productgrouplist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionProductGroup() {

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('product_category', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = ProductGroup::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('product_category', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new ProductGroup();
        }
        if (isset($_POST['ProductGroup'])) {
            $model->attributes = $_POST['ProductGroup'];
            if ($model->validate() && $model->save()) {
                if (isset($_FILES['logo']) && $_FILES['logo']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['logo']['name'];
                    $model->logo = $file_name;
                    copy($_FILES['logo']['tmp_name'], Yii::app()->basePath . '/../images/group/' . $file_name);
                    $model->save();
                }
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/ProductGroupList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('productgroup', array('model' => $model));
    }

    public function actiondeleteProductGroup() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('product_category', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                ProductGroup::model()->deleteByPk($id);
                echo "OK";
            }
        }
    }

    public function actionBrandList() {

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Brand::model()->deleteByPk($id);
            }
        }
        $model = new Brand();
        $data = $model->search()->data;
        $this->render('brandlist', array('model' => $model, 'data' => $data));
    }

    public function actionBrand() {

        if (isset($_GET['id']))
            $model = Brand::model()->findByPk($_GET['id']);
        else
            $model = new Brand();
        if (isset($_POST['Brand'])) {
            $model->attributes = $_POST['Brand'];

            if ($model->validate() && $model->save()) {
                if (isset($_FILES['brand_logo']) && $_FILES['brand_logo']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['brand_logo']['name'];
                    $model->brand_logo = $file_name;
                    copy($_FILES['brand_logo']['tmp_name'], Yii::app()->basePath . '/../images/brand/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['brand_banner']) && $_FILES['brand_banner']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['brand_banner']['name'];
                    $model->brand_banner = $file_name;
                    copy($_FILES['brand_banner']['tmp_name'], Yii::app()->basePath . '/../images/brand/' . $file_name);
                    $model->save();
                }
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/brandlist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('brand', array('model' => $model));
    }

    public function actiondeleteBrand() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            Brand::model()->deleteByPk($id);
            echo "OK";
        }
    }

    public function actionProductList() {
        $model = new Product();
        $param['series'] = array();
        $param['group'] = array();

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('product', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Product::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['Product'])) {
                $model->attributes = $_POST['Product'];
                $model->product_name = $_POST['Product']['product_name'];
                $model->product_code = $_POST['Product']['product_code'];
                $model->status = $_POST['Product']['status'];
                //$model->serie_id = $_POST['Product']['serie_id'];
                if ($model->group_id != "") {
                    $param['series'] = CHtml::listData(Series::model()->findAll("group_id=:group_id", array(':group_id' => $model->group_id,)), 'id', 'name_th');
                }
            }
        }

        if ($model->brand_id != "") {
            $param['group'] = CHtml::listData(ProductGroup::model()->findAll("brand_id=:brand_id", array(':brand_id' => $model->brand_id,)), 'id', 'group_2');
        }
        $param['group'] = CHtml::listData(ProductGroup::model()->findAll(), 'id', 'group_2');
        $model->display_perpage = "10";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->backend_search()->data;

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('productlist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionchangeProductStatus() {
        if (!$this->checkUserPermission('product', '4')) {
            echo "DENIED";
        } else {
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                Product::model()->updateByPk($id, array('status' => $_POST['status']));
            }
            echo "OK";
        }
    }

    public function actionProduct() {
        $param['series'] = array();
        $param['group'] = array();
        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('product', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = Product::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('product', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
        }
        if (isset($model)) {
            if ($model->group_id != "" && $model->group_id != "0") {
                if ($model->group_id != "") {
                    $param['series'] = CHtml::listData(Series::model()->findAll("group_id=:group_id", array(':group_id' => $model->group_id,)), 'id', 'name_th');
                }
            }
        } else {
            $model = new Product();
            $model->sort_order = 0;
            $model->stock = 0;
        }

        if ($model->brand_id != "") {
            $param['group'] = CHtml::listData(ProductGroup::model()->findAll("brand_id=:brand_id", array(':brand_id' => $model->brand_id,)), 'id', 'group_2');
        }
        $param['group'] = CHtml::listData(ProductGroup::model()->findAll(), 'id', 'group_2');
        if (isset($_POST['Product'])) {
            $model->attributes = $_POST['Product'];

            if ($model->validate() && $model->save()) {
                if (isset($_FILES['pic1']) && $_FILES['pic1']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic1']['name'];
                    $model->pic1 = $file_name;
                    copy($_FILES['pic1']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }

                if (isset($_FILES['pic2']) && $_FILES['pic2']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic2']['name'];
                    $model->pic2 = $file_name;
                    copy($_FILES['pic2']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }

                if (isset($_FILES['pic3']) && $_FILES['pic3']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic3']['name'];
                    $model->pic3 = $file_name;
                    copy($_FILES['pic3']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }

                if (isset($_FILES['pic4']) && $_FILES['pic4']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic4']['name'];
                    $model->pic4 = $file_name;
                    copy($_FILES['pic4']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['pic5']) && $_FILES['pic5']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic5']['name'];
                    $model->pic5 = $file_name;
                    copy($_FILES['pic5']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['pic6']) && $_FILES['pic6']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic6']['name'];
                    $model->pic6 = $file_name;
                    copy($_FILES['pic6']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['pic7']) && $_FILES['pic7']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic7']['name'];
                    $model->pic7 = $file_name;
                    copy($_FILES['pic7']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['pic8']) && $_FILES['pic8']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic8']['name'];
                    $model->pic8 = $file_name;
                    copy($_FILES['pic8']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['pic9']) && $_FILES['pic9']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic9']['name'];
                    $model->pic9 = $file_name;
                    copy($_FILES['pic9']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['pic10']) && $_FILES['pic10']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['pic10']['name'];
                    $model->pic10 = $file_name;
                    copy($_FILES['pic10']['tmp_name'], Yii::app()->basePath . '/../images/product/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['attach']) && $_FILES['attach']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['attach']['name'];
                    $model->attach = $file_name;
                    copy($_FILES['attach']['tmp_name'], Yii::app()->basePath . '/../attach/product/' . $file_name);
                    $model->save();
                }

                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/ProductList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('product', array('model' => $model, 'param' => $param));
    }

    public function actiondeleteProduct() {
        if (isset($_POST['id'])) {
            if (!$this->checkUserPermission('product', '3')) {
                echo "DENIED";
            } else {
                $id = $_POST['id'];
                Product::model()->deleteByPk($id);
                echo "OK";
            }
        }
    }

    public function actionUserGroupList() {

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('user_group', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                UserGroup::model()->deleteByPk($id);
            }
        }
        $model = new UserGroup();
        $data = $model->search()->data;
        $this->render('usergrouplist', array('model' => $model, 'data' => $data));
    }

    public function actionUserGroup() {

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('user_group', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = UserGroup::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('user_group', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new UserGroup();
        }
        if (isset($_POST['UserGroup'])) {
            $model->attributes = $_POST['UserGroup'];
            if ($model->validate() && $model->save()) {

                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/UserGroupList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('usergroup', array('model' => $model));
    }

    public function actiondeleteUserGroup() {
        if (!$this->checkUserPermission('user_group', '3')) {
            echo "DENIED";
        } else {
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                UserGroup::model()->deleteByPk($id);
                echo "OK";
            }
        }
    }

    public function actionUserList() {
        $model = new User();
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('user', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                User::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['User'])) {
                $model->attributes = $_POST['User'];
                $model->key_word = $_POST['User']['key_word'];
                $model->group_id = $_POST['User']['group_id'];
                $model->status = $_POST['User']['status'];
            }
        }
        $model->display_perpage = "30";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->backend_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('userlist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionchangeUserStatus() {
        if (!$this->checkUserPermission('user', '4')) {
            echo "DENIED";
        } else {
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                User::model()->updateByPk($id, array('status' => $_POST['status']));
            }
            echo "OK";
        }
    }

    public function actionUser() {

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('user', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = User::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('user', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new User();
        }
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->last_login = '';
            if ($model->validate() && $model->save()) {


                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/UserList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('user', array('model' => $model));
    }

    public function actiondeleteUser() {
        if (!$this->checkUserPermission('user', '3')) {
            echo "DENIED";
        } else {
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                User::model()->deleteByPk($id);
                echo "OK";
            }
        }
    }

    public function actionSetting() {
        $model = Setting::model()->findByPk(1);
        if (isset($_POST['Setting'])) {
            if (!$this->checkUserPermission('site_config', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

            $model->attributes = $_POST['Setting'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->save()) {
                if (isset($_FILES['logo']) && $_FILES['logo']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['logo']['name'];
                    $model->logo = $file_name;
                    copy($_FILES['logo']['tmp_name'], Yii::app()->basePath . '/../images/logo/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['line']) && $_FILES['line']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['line']['name'];
                    $model->line = $file_name;
                    copy($_FILES['line']['tmp_name'], Yii::app()->basePath . '/../images/logo/' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['instragram']) && $_FILES['instragram']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['instragram']['name'];
                    $model->instragram = $file_name;
                    copy($_FILES['instragram']['tmp_name'], Yii::app()->basePath . '/../images/logo/' . $file_name);
                    $model->save();
                }
            } else {
                echo "Error";
            }
        }
        $this->render('setting', array('model' => $model));
    }

    public function actionConfigCatalog() {
        $modelConfig = new Config();

        if (isset($_POST) && count($_POST) > 0) {
            if (!$this->checkUserPermission('cart_config', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $txtShippingFee = trim($_POST["txtShippingFee"]);
            if ($txtShippingFee == '')
                $txtShippingFee = 0;
            Config::model()->updateAll(array('cfg_value' => $_POST["txtShippingFee"]), "cfg_code='SHIPPING_FEE' and cfg_group='CATALOG'");
        }
        if (isset($_POST) && count($_POST) > 0) {
            if (!$this->checkUserPermission('cart_config', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $txtDollarRate = trim($_POST["txtDollarRate"]);
            if ($txtDollarRate == '')
                $txtDollarRate = 0;
            Config::model()->updateAll(array('cfg_value' => $_POST["txtDollarRate"]), "cfg_code='EXCHANGE_RATE_DOLLAR' and cfg_group='CATALOG'");
        }
        if (isset($_POST) && count($_POST) > 0) {
            if (!$this->checkUserPermission('cart_config', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $txtBoxWeight = trim($_POST["txtBoxWeight"]);
            if ($txtBoxWeight == '')
                $txtBoxWeight = 0;
            Config::model()->updateAll(array('cfg_value' => $_POST["txtBoxWeight"]), "cfg_code='BOX_WEIGHT' and cfg_group='SHIPPING'");
        }
        if (isset($_POST) && count($_POST) > 0) {
            if (!$this->checkUserPermission('cart_config', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $txtScoreRate = trim($_POST["txtScoreRate"]);
            if ($txtScoreRate == '')
                $txtScoreRate = 0;
            Config::model()->updateAll(array('cfg_value' => $_POST["txtScoreRate"]), "cfg_code='SCORE_RATE' and cfg_group='CATALOG'");
        }
        if (isset($_POST) && count($_POST) > 0) {
            if (!$this->checkUserPermission('cart_config', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $txtWeightOver = trim($_POST["txtWeightOver"]);
            if ($txtWeightOver == '')
                $txtWeightOver = 0;
            Config::model()->updateAll(array('cfg_value' => $_POST["txtWeightOver"]), "cfg_code='WEIGHT_OVER' and cfg_group='SHIPPING'");
        }

        $param["shipping_fee"] = 0;
        $param["dollar_rate"] = 0;
        $param["score_rate"] = 0;
        $param["box_weight"] = 0;
        $param["weight_over"] = 0;

        $dataShipping = $modelConfig->searchByCode('SHIPPING_FEE')->data;
        if (count($dataShipping) > 0)
            $param["shipping_fee"] = $dataShipping[0]["cfg_value"];
        $dataDollarRate = $modelConfig->searchByCode('EXCHANGE_RATE_DOLLAR')->data;
        if (count($dataDollarRate) > 0)
            $param["dollar_rate"] = $dataDollarRate[0]["cfg_value"];
        $dataScoreRate = $modelConfig->searchByCode('SCORE_RATE')->data;
        if (count($dataScoreRate) > 0)
            $param["score_rate"] = $dataScoreRate[0]["cfg_value"];
        $dataBoxWeight = $modelConfig->searchByCode('BOX_WEIGHT')->data;
        if (count($dataBoxWeight) > 0)
            $param["box_weight"] = $dataBoxWeight[0]["cfg_value"];
        $dataWeightOver = $modelConfig->searchByCode('WEIGHT_OVER')->data;
        if (count($dataWeightOver) > 0)
            $param["weight_over"] = $dataWeightOver[0]["cfg_value"];

        $this->render('configcatalog', array('param' => $param));
    }

    public function actionProductRelateList() {
        if (!$this->checkUserPermission('product', '5'))
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

        $model = new RelateProduct();
        $model->product_id = $_GET['id'];
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                RelateProduct::model()->deleteByPk($id);
            }
        }

        $data = $model->backend_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);

        $this->render('productrelatelist', array('model' => $model, 'data' => $data));
    }

    public function actionProductRelate() {
        $model = new Product();

        $data = array();
        if (isset($_POST['act']) && $_POST['act'] == "addall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                $relate = new RelateProduct();
                $relate->product_id = $_GET['id'];
                $relate->product_relate_id = $id;
                $relate->save();
            }
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/ProductRelateList/' . $_GET['id']);
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['Product'])) {
                $model->attributes = $_POST['Product'];
                $model->product_name = $_POST['Product']['product_name'];
                $model->product_code = $_POST['Product']['product_code'];
                $model->status = $_POST['Product']['status'];
            }
        } else {
            $model->status = "";
        }
        $model->display_perpage = "10";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        //if (isset($_POST['act']) && $_POST['act'] == "search")
        $data = $model->backend_search()->data;

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('productrelate', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actiondeleteProductRelate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            RelateProduct::model()->deleteByPk($id);
            echo "OK";
        }
    }

    public function actionNewsletterConfig() {
        $modelConfig = new Config();

        if (isset($_POST) && count($_POST) > 0) {
            if (!$this->checkUserPermission('newsletter_config', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $txtSenderEmail = trim($_POST["txtSenderEmail"]);
            $txtSenderName = trim($_POST["txtSenderName"]);
            Config::model()->updateAll(array('cfg_value' => $txtSenderEmail), "cfg_code='SENDER_EMAIL' and cfg_group='NEWSLETTER'");
            Config::model()->updateAll(array('cfg_value' => $txtSenderName), "cfg_code='SENDER_NAME' and cfg_group='NEWSLETTER'");
        }

        $param["sender_email"] = "";
        $param["sender_name"] = "";

        $dataConfig = $modelConfig->searchByCode('SENDER_EMAIL')->data;
        if (count($dataConfig) > 0)
            $param["sender_email"] = $dataConfig[0]["cfg_value"];
        $dataConfig = $modelConfig->searchByCode('SENDER_NAME')->data;
        if (count($dataConfig) > 0)
            $param["sender_name"] = $dataConfig[0]["cfg_value"];

        $this->render('newsletterconfig', array('param' => $param));
    }

    public function actionSubscriberList() {
        $model = new Subscriber();

        $model->display_perpage = "20";
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }

        $model->current_page = $current_page;
        $data = $model->listForManage()->data;

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('subscriberlist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionSendNewsList() {
        $model = new SendNews();

        $model->display_perpage = "20";
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }

        $model->current_page = $current_page;
        $data = $model->listForManage()->data;

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('sendnewslist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionNewsletterChooseNews() {
        if (!$this->checkUserPermission('newsletter', '4'))
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

        $model = new Content();

        if (isset($_POST['act']) && $_POST['act'] == "sendnews" && isset($_POST['p_id'])) {
            if (Yii::allow_send_email()) {
                // Config value for send email
                $senderEmail = "";
                $senderName = "";
                $modelConfig = new Config();
                $dataConfig = $modelConfig->searchByCode('SENDER_EMAIL')->data;
                if (count($dataConfig) > 0)
                    $senderEmail = $dataConfig[0]["cfg_value"];
                $dataConfig = $modelConfig->searchByCode('SENDER_NAME')->data;
                if (count($dataConfig) > 0)
                    $senderName = $dataConfig[0]["cfg_value"];

                $pid = $_POST['p_id'];
                foreach ($pid as $id) {
                    // News
                    $dataNews = Content::model()->findByPk($id);
                    $newsTitle = $dataNews->title_2;
                    $newsContent = $dataNews->content_2;

                    // All subscriber
                    $modelSubscriber = new Subscriber();
                    $dataSubscriber = $modelSubscriber->searchAll()->data;
                    foreach ($dataSubscriber as $rowSubscriber) {
                        // Send email -------
                        $message = new YiiMailMessage;

                        $body = $newsContent;
                        //$body = str_replace("/fileman/","http://www.".$_SERVER['SERVER_NAME']."/fileman/",$body);
                        $body = str_replace("/fileman/", Yii::getDomain() . "/fileman/", $body);
                        $subject = Yii::prefixMailSubject().$newsTitle;

                        /* $message->message->setBody($body, 'text/html', 'utf-8');
                          $message->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');

                          $message->addTo($rowSubscriber->email);
                          //$message->from = 'online@bangkokinterfood.co.th';

                          //$message->setFrom = "online@bangkokinterfood.co.th";//$senderEmail;
                          $message->setFrom(array('seksit.w@softdebut.com' => 'John Doe'));

                          if (Yii::allow_send_email())
                          Yii::app()->mail->send($message);

                          $send_news = new SendNews();
                          $send_news->content_id = $id;
                          $send_news->subscriber_id = $rowSubscriber->id;
                          $send_news->save(); */

                        $mail_to = $rowSubscriber->email;
                        $mail_subject = '=?utf-8?B?' . base64_encode($subject) . '?='; // email subject line
                        $mail_body = $body;

                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        //$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        $headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
                        $headers .= 'To: ' . $rowSubscriber->email . "\r\n";
                        $headers .= 'From:  ' . $senderName . ' <' . $senderEmail . '>' . "\r\n";

                        mail($mail_to, $mail_subject, $mail_body, $headers);
                    }
                }
            }
            Yii::app()->clientScript->registerScript('finish', 'alert("ส่งจดหมายข่าวเสร็จสมบูรณ์");location.href="' . Yii::app()->request->baseUrl . "/index.php/admin/SendNewsList" . '"');
        }

        $model->display_perpage = "10";
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }

        $model->current_page = $current_page;

        $data = $model->searchNewsForManage('', '')->data;

        if (isset($_POST['act']) && $_POST['act'] == "search") {
            $searchTitle = $_POST["txtTitle"];
            $searchContent = $_POST["txtContent"];
            $data = $model->searchNewsForManage($searchTitle, $searchContent)->data;
        }

        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('newsletterchoosenews', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionJobbeeList() {
        $model = new Jobbee();
        if (isset($_POST['act']) && $_POST['act'] == "deleteAll") {
            if (isset($_POST['jobbee_id']) && count($_POST['jobbee_id']) > 0) {
                $mid = $_POST['jobbee_id'];
                foreach ($mid as $m) {
                    Jobbee::model()->deleteAll('id = :id', array(":id" => $m));
                }
            }
        }
        if (isset($_POST['Jobbee'])) {
            $model->attributes = $_POST['Jobbee'];
            $model->key_word = $_POST['Jobbee']['key_word'];
            $model->start_date = $_POST['Jobbee']['start_date'];
            $model->end_date = $_POST['Jobbee']['end_date'];
            $model->status = $_POST['Jobbee']['status'];
        }
        $data = $model->search()->data;
        $this->render('jobbeelist', array('model' => $model, 'data' => $data));
    }

    public function actionChangeJobbedStatus() {
        $id = $_REQUEST['id'];
        /*
          $model = Jobbee::model()->findByPk($id);
          $model->status = $_REQUEST['status'];
          $model->save(); */
        Jobbee::model()->updateByPk($id, array('status' => $_REQUEST['status']));
        echo "OK";
    }

    public function actionDeleteJobbee() {
        $id = $_REQUEST['id'];
        $model = Jobbee::model()->findByPk($id);
        $model->delete();
        echo "OK";
    }

    public function actionJobList() {
        $model = new Joblist();
        if (isset($_POST['Joblist'])) {
            $model->attributes = $_POST['Joblist'];
            if ($model->validate()) {
                $model->save();
            }
        }
        $data = $model->search()->data;
        $this->render('joblist', array('model' => $model, 'data' => $data));
    }

    public function actionJob() {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = Joblist::model()->findByPk($id);
        } else {
            $model = New Joblist();
        }
        if (isset($_POST['Joblist'])) {
            $model->attributes = $_POST['Joblist'];
            if ($model->validate()) {
                $model->save();
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/JobList/');
            } else {
                print_r($model->getErrors());
            }
        }
        $this->render('job', array('model' => $model));
    }

    public function actionDeleteJob() {
        $id = $_REQUEST['id'];
        /* $model = Joblist::model()->findByPk($id);
          $model->delete($id);

         */
        Joblist::model()->deleteByPk($id);
        echo "OK";
    }

    public function actionOrderList() {
        $model = new MemberOrder();
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Product::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['MemberOrder'])) {
                $model->attributes = $_POST['MemberOrder'];
                $model->fullname = $_POST['MemberOrder']['fullname'];
                $model->company_name = $_POST['MemberOrder']['company_name'];
                $model->address = $_POST['MemberOrder']['address'];
                $model->province = $_POST['MemberOrder']['province'];
                $model->start_date = $_POST['MemberOrder']['start_date'];
                $model->end_date = $_POST['MemberOrder']['end_date'];
                $model->status = $_POST['MemberOrder']['status'];
            }
        } else {
            $model->status = 0;
        }

        $model->display_perpage = "30";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->backend_order_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('orderlist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionchangeOrderStatus() {
        switch ($_GET["id"]) {
            case "0":
                if (!$this->checkUserPermission('order', '4')) {
                    echo "DENIED";
                } else {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];

                        // รอการจัดส่ง
                        if ($_POST['status'] == "4") {
                            // Update total shipping & stock
                            $modelOrder = new Order();
                            $modelOrder->order_id = $id;
                            $dataOrder = $modelOrder->get_cart_by_order_id()->data;
                            foreach ($dataOrder as $order) {
                                $orderAmount = $order->amount;
                                $totalShipping = new CDbExpression('(`total_shipping`+:orderAmount)', array(':orderAmount' => $orderAmount));
                                $updateStock = new CDbExpression('(`stock`-:orderAmount)', array(':orderAmount' => $orderAmount));
                                Product::model()->updateByPk($order->product_id, array(
                                    'total_shipping' => $totalShipping,
                                    'stock' => $updateStock
                                ));
                            }

                            // Update score
                            $memberID = $dataOrder[0]->member_id;
                            $totalScore = 0;
                            $modelCartInfo = CartInfo::model()->find('order_id=:order_id', array(':order_id' => $id));
                            if (count($modelCartInfo) > 0) {
                                $totalScore = $modelCartInfo->score;
                                if ($totalScore > 0) {
                                    $updateScore = new CDbExpression('(`total_score`+:score)', array(':score' => $totalScore));
                                    Member::model()->updateByPk($memberID, array(
                                        'total_score' => $updateScore
                                    ));
                                }
                            }
                        }
                        // Update order status
                        MemberOrder::model()->updateByPk($id, array('status' => $_POST['status']));
                    }
                    echo "OK";
                }
                break;
            case "1":
                if (!$this->checkUserPermission('shipping', '4')) {
                    echo "DENIED";
                } else {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];

                        // รอการจัดส่ง
                        if ($_POST['status'] == "7") {
                            // Update total shipping & stock
                            $modelOrder = new Order();
                            $modelOrder->order_id = $id;
                            $dataOrder = $modelOrder->get_cart_by_order_id()->data;
                            foreach ($dataOrder as $order) {
                                $orderAmount = $order->amount;
                                $totalShipping = new CDbExpression('(`total_shipping`-:orderAmount)', array(':orderAmount' => $orderAmount));
                                $updateStock = new CDbExpression('(`stock`+:orderAmount)', array(':orderAmount' => $orderAmount));
                                Product::model()->updateByPk($order->product_id, array(
                                    'total_shipping' => $totalShipping,
                                    'stock' => $updateStock
                                ));
                            }

                            // Update score
                            $memberID = $dataOrder[0]->member_id;
                            $totalScore = 0;
                            $modelCartInfo = CartInfo::model()->find('order_id=:order_id', array(':order_id' => $id));
                            if (count($modelCartInfo) > 0) {
                                $totalScore = $modelCartInfo->score;
                                if ($totalScore > 0) {
                                    $updateScore = new CDbExpression('(`total_score`-:score)', array(':score' => $totalScore));
                                    Member::model()->updateByPk($memberID, array(
                                        'total_score' => $updateScore
                                    ));
                                }
                            }
                        }

                        // Update order status
                        MemberOrder::model()->updateByPk($id, array('status' => $_POST['status']));
                    }
                    echo "OK";
                }
                break;
        }
    }

    public function actionShippingList() {
        $model = new MemberOrder();
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Product::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['MemberOrder'])) {
                $model->attributes = $_POST['MemberOrder'];
                $model->fullname = $_POST['MemberOrder']['fullname'];
                $model->company_name = $_POST['MemberOrder']['company_name'];
                $model->address = $_POST['MemberOrder']['address'];
                $model->province = $_POST['MemberOrder']['province'];
                $model->start_date = $_POST['MemberOrder']['start_date'];
                $model->end_date = $_POST['MemberOrder']['end_date'];
                $model->status = $_POST['MemberOrder']['status'];
            }
        } else {
            $model->status = '4';
        }

        $model->display_perpage = "30";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;

        $data = $model->backend_shipping_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('shippinglist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionProductOrderList() {
        $id = (isset($_GET['id']) ? $_GET['id'] : 0);
        $count = Order::model()->count('order_id=:order_id', array(':order_id' => $id));
        $model = new Order();
        $model->order_id = $id;
        $data = $model->get_cart_by_order_id()->data;

        $shippingFee = 0;
        $score = 0;
        $totalWeight = 0;
        $shipType = 'in_reg';
        $modelCartInfo = new CartInfo();
        $dataCartInfo = $modelCartInfo->searchByOrderID($id)->data;
        if (count($dataCartInfo) > 0) {
            $shippingFee = $dataCartInfo[0]->shipping_fee;
            $score = $dataCartInfo[0]->score;
            $totalWeight = $dataCartInfo[0]->total_weight;
            $shipType = $dataCartInfo[0]->shipping_type;
        }

        $param['shipping_fee'] = $shippingFee;
        $param['count_item'] = $count;
        $param["score"] = $score;
        $param['total_weight'] = floatval($totalWeight) / 1000;
        $param['shiptype'] = $shipType;

        $this->render('cart', array('param' => $param, 'data' => $data));
    }

    public function actionProductShippingList() {
        $id = (isset($_GET['id']) ? $_GET['id'] : 0);
        $count = Order::model()->count('order_id=:order_id', array(':order_id' => $id));
        $model = new Order();
        $model->order_id = $id;
        $data = $model->get_cart_by_order_id()->data;

        $shippingFee = 0;
        $score = 0;
        $totalWeight = 0;
        $shipType = 'in_reg';

        $modelCartInfo = new CartInfo();
        $dataCartInfo = $modelCartInfo->searchByOrderID($id)->data;
        if (count($dataCartInfo) > 0) {
            $shippingFee = $dataCartInfo[0]->shipping_fee;
            $score = $dataCartInfo[0]->score;
            $totalWeight = $dataCartInfo[0]->total_weight;
            $shipType = $dataCartInfo[0]->shipping_type;
        }

        $param['shipping_fee'] = $shippingFee;
        $param['count_item'] = $count;
        $param["score"] = $score;
        $param['total_weight'] = floatval($totalWeight) / 1000;
        $param['shiptype'] = $shipType;

        $this->render('cartshipping', array('param' => $param, 'data' => $data));
    }

    public function actionCatalogList() {
        $model = new Download();
        $model->type_id = 1;
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            if (!$this->checkUserPermission('catalogue', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Download::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['Download'])) {
                $model->attributes = $_POST['Download'];
                $model->filename = $_POST['Download']['filename'];
            }
        }
        $model->display_perpage = "10";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;

        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->backend_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('cataloglist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionCatalog() {

        if (isset($_GET['id'])) {
            if (!$this->checkUserPermission('catalogue', '2'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = Download::model()->findByPk($_GET['id']);
        }else {
            if (!$this->checkUserPermission('catalogue', '1'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            $model = new Download();
        }

        $model->type_id = 1;
        if (isset($_POST['Download'])) {
            $model->attributes = $_POST['Download'];
            $model->type_id = 1;
            $model->cover = "";
            $model->filesrc = "";
            if ($model->validate() && $model->save()) {
                if (isset($_FILES['cover']) && $_FILES['cover']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['cover']['name'];
                    $model->cover = $file_name;
                    copy($_FILES['cover']['tmp_name'], Yii::app()->basePath . '/../download/cover_' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['file']) && $_FILES['file']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['file']['name'];
                    $model->filesrc = $file_name;
                    copy($_FILES['file']['tmp_name'], Yii::app()->basePath . '/../download/file_' . $file_name);
                    $model->save();
                }



                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/CatalogList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('catalog', array('model' => $model));
    }

    public function actionManualList() {
        $model = new Download();
        $model->type_id = 2;
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Download::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['Download'])) {
                $model->attributes = $_POST['Download'];
                $model->filename = $_POST['Download']['filename'];
            }
        }
        $model->display_perpage = "30";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;

        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->backend_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('manualist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionManual() {

        if (isset($_GET['id']))
            $model = Download::model()->findByPk($_GET['id']);
        else
            $model = new Download();
        $model->type_id = 2;
        if (isset($_POST['Download'])) {
            $model->attributes = $_POST['Download'];
            $model->type_id = 2;
            $model->cover = "";
            $model->filesrc = "";
            if ($model->validate() && $model->save()) {
                if (isset($_FILES['cover']) && $_FILES['cover']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['cover']['name'];
                    $model->cover = $file_name;
                    copy($_FILES['cover']['tmp_name'], Yii::app()->basePath . '/../download/cover_' . $file_name);
                    $model->save();
                }
                if (isset($_FILES['file']) && $_FILES['file']['name'] != "") {
                    $file_name = $model->id . '_' . $_FILES['file']['name'];
                    $model->filesrc = $file_name;
                    copy($_FILES['file']['tmp_name'], Yii::app()->basePath . '/../download/file_' . $file_name);
                    $model->save();
                }



                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/ManualList');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('manual', array('model' => $model));
    }

    public function actionDeleteDownload() {
        if (!$this->checkUserPermission('catalogue', '3')) {
            echo "DENIED";
        } else {
            $id = $_REQUEST['id'];
            /* $model = Joblist::model()->findByPk($id);
              $model->delete($id);

             */
            Download::model()->deleteByPk($id);
            echo "OK";
        }
    }

    public function actionSeriesList() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Series::model()->deleteByPk($id);
            }
        }
        $model = new Series();
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['Series'])) {
                $model->attributes = $_POST['Series'];
                $model->group_id = $_POST['Series']['group_id'];
                $model->brand_id = $_POST['Series']['brand_id'];
            }
        }
        $data = $model->search()->data;
        $this->render('serieslist', array('model' => $model, 'data' => $data));
    }

    public function actionSeries() {

        if (isset($_GET['id']))
            $model = Series::model()->findByPk($_GET['id']);
        else
            $model = new Series();

        //echo $model->group_id;
        $param['group'] = array();
        $model->brand_id = $model->group['brand_id'];

        if ($model->group['brand_id'] != "") {
            $param['group'] = CHtml::listData(ProductGroup::model()->findAll("brand_id=:brand_id", array(':brand_id' => $model->brand_id,)), 'id', 'group_2');
        }

        if (isset($_POST['Series'])) {
            $model->attributes = $_POST['Series'];

            if ($model->validate() && $model->save()) {

                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/serieslist');
            } else {
                print_r($model->getErrors());
                echo "Error";
            }
        }
        $this->render('series', array('model' => $model, 'param' => $param));
    }

    public function actionDeleteSeries() {
        $id = $_REQUEST['id'];

        Series::model()->deleteByPk($id);
        echo "OK";
    }

    public function actionGetGroup() {
        $brand_id = 0;
        if (isset($_REQUEST['Series']['brand_id']))
            $brand_id = $_REQUEST['Series']['brand_id'];
        if (isset($_REQUEST['Product']['brand_id']))
            $brand_id = $_REQUEST['Product']['brand_id'];
        $data = CHtml::listData(ProductGroup::model()->findAll("brand_id=:brand_id ", array(':brand_id' => $brand_id)), 'id', 'group_2');
        echo "<option value=''>ทุก Group</option>";
        while ($row = current($data)) {

            echo "<option value='" . key($data) . "'>" . $row . "</option>";

            next($data);
        }

        //echo "<option value='0'>Test</option>";
    }

    public function actionGetSeries() {
        $group_id = 0;
        if (isset($_REQUEST['Product']['group_id']))
            $group_id = $_REQUEST['Product']['group_id'];
        $data = CHtml::listData(Series::model()->findAll("group_id=:group_id ", array(':group_id' => $group_id)), 'id', 'name_th');
        echo "<option value=''>เลือก Series</option>";
        while ($row = current($data)) {

            echo "<option value='" . key($data) . "'>" . $row . "</option>";

            next($data);
        }

        //echo "<option value='0'>Test</option>";
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionPremissionUsers() {
        if (!$this->checkUserPermission('user_group', '4'))
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');

        $id = $_REQUEST['id'];
        $user = UserGroup::model()->findByPk($id);
        $model = new Premission();

        if (isset($_POST['mm'])) {
            Premission::model()->deleteAll('user_id=:user_id', array(':user_id' => $id));
            foreach ($_POST['mm'] as $mm) {
                $prem = new Premission();
                $prem->menu_id = $mm;
                $prem->user_id = $id;
                $prem->assign_by = Yii::app()->user->getName();
                $prem->type = 0;
                $prem->save();
                //echo $mm;
            }

            //$this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/UserGroupList/');
        }
        $menu = Menu::model()->getMenu()->data;
        $premission = Premission::model()->findAll('user_id=:user_id', array(':user_id' => $id));
//		$data = $premission->data;
        $this->render('premission', array('model' => $model, 'user' => $user, 'premission' => $premission, 'menu' => $menu));
    }

    public function actionServiceList() {
        $model = new Service();
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Service::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['Service'])) {
                $model->attributes = $_POST['Service'];
                $model->start_date = $_POST['Service']['start_date'];
                $model->end_date = $_POST['Service']['end_date'];
                /* $model->full_name = (isset($_POST['Service']['full_name'])?$_POST['Service']['full_name']:"");
                  $model->company_name = (isset($_POST['Service']['company_name'])?$_POST['Service']['company_name']:"");
                  $model->status = (isset($_POST['Service']['status'])?$_POST['Service']['status']:""); */
            }
        }
        $model->scenario = "search";
        $model->display_perpage = "30";
        $model->service_type = (isset($_GET['id']) ? $_GET['id'] : 0);
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;

        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->backend_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;
        $param['title'] = "Technicial Support";
        if ($model->service_type == "2")
            $param['title'] = "Repair";
        $this->render('servicelist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionchangeServiceStatus() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            Service::model()->updateByPk($id, array('status' => $_POST['status']));
        }
        echo "OK";
    }

    public function actionDeleteService() {
        $id = $_REQUEST['id'];
        Service::model()->deleteByPk($id);
        echo "OK";
    }

    public function actionTraineeList() {
        $model = new Course();
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Service::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['Course'])) {
                $model->attributes = $_POST['Course'];
                $model->start_date = $_POST['Course']['start_date'];
                $model->end_date = $_POST['Course']['end_date'];
                /* $model->full_name = (isset($_POST['Service']['full_name'])?$_POST['Service']['full_name']:"");
                  $model->company_name = (isset($_POST['Service']['company_name'])?$_POST['Service']['company_name']:"");
                  $model->status = (isset($_POST['Service']['status'])?$_POST['Service']['status']:""); */
            }
        }
        $model->display_perpage = "30";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;

        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->backend_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;
        $param['title'] = "Training";

        $this->render('traineelist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionContactList() {
        $model = new Contact();
        if (isset($_POST['act']) && $_POST['act'] == "deleteall" && isset($_POST['p_id'])) {
            switch ($_GET["id"]) {
                case "0":
                    if (!$this->checkUserPermission('contact', '3'))
                        $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
                    break;
                case "1":
                    if (!$this->checkUserPermission('request_qoutation', '3'))
                        $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
                    break;
            }
            $pid = $_POST['p_id'];
            foreach ($pid as $id) {
                Contact::model()->deleteByPk($id);
            }
        }
        if (isset($_POST['act']) && $_POST['act'] == "search") {
            if (isset($_POST['Contact'])) {
                $model->attributes = $_POST['Contact'];
                $model->status = $_POST['Contact']['status'];
                $model->start_date = $_POST['Contact']['start_date'];
                $model->end_date = $_POST['Contact']['end_date'];
                /* $model->full_name = (isset($_POST['Service']['full_name'])?$_POST['Service']['full_name']:"");
                  $model->company_name = (isset($_POST['Service']['company_name'])?$_POST['Service']['company_name']:"");
                  $model->status = (isset($_POST['Service']['status'])?$_POST['Service']['status']:""); */
            }
        } else {
            $model->status = "0";
        }
        $model->type = $_GET['id'];
        $model->display_perpage = "30";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;

        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;
        if ($_REQUEST['id'] == "0")
            $param['title'] = "Contact List";
        else
            $param['title'] = "Quotation Request List";

        $this->render('contactlist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actiondeleteContact() {
        switch ($_GET["id"]) {
            case "0":
                if (!$this->checkUserPermission('contact', '3')) {
                    echo "DENIED";
                } else {
                    $id = $_REQUEST['id'];
                    Contact::model()->deleteByPk($id);
                    echo "OK";
                }
                break;
            case "1":
                if (!$this->checkUserPermission('request_qoutation', '3')) {
                    echo "DENIED";
                } else {
                    $id = $_REQUEST['id'];
                    Contact::model()->deleteByPk($id);
                    echo "OK";
                }
                break;
        }
    }

    public function actionchangeContactStatus() {
        switch ($_GET["id"]) {
            case "0":
                if (!$this->checkUserPermission('contact', '3')) {
                    echo "DENIED";
                } else {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                        Contact::model()->updateByPk($id, array('status' => $_POST['status']));
                    }
                    echo "OK";
                }
                break;
            case "1":
                if (!$this->checkUserPermission('request_qoutation', '3')) {
                    echo "DENIED";
                } else {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                        Contact::model()->updateByPk($id, array('status' => $_POST['status']));
                    }
                    echo "OK";
                }
                break;
        }
    }

    public function actionchangeTraineeStatus() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            Course::model()->updateByPk($id, array('status' => $_POST['status']));
        }
        echo "OK";
    }

    public function actionExportMember() {
        if (!$this->checkUserPermission('member', '4'))
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
        $this->layout = false;
        $model = new Member();
        $model->display_perpage = 99999;
        $model->first_name = $_REQUEST['first_name'];
        $model->last_name = $_REQUEST['last_name'];
        $model->email = $_REQUEST['email'];
        $model->phone_no = $_REQUEST['phone_no'];
        $model->start_date = $_REQUEST['start_date'];
        $model->end_date = $_REQUEST['end_date'];

        $data = $model->search()->data;
        $head = "<html><head></head><body>";
        $body = "";
        $prefix[0] = " ";
        $prefix[1] = " นาย ";
        $prefix[2] = " นาง ";
        $prefix[3] = " นางสาว ";
        $prefix[4] = " ";
        $no = 1;
        $body .= "<table border='1'><tr><th>No.</th><th>Prefix</th><th>First Name</th><th>Last Name</th><th>Birth Day</th><th>Address</th><th>Province</th><th>Zipcode</th><th>Phone No</th><th>Email</th><th>Member Type</th><th>Register Date</th><th>Last Login</th></tr>";
        foreach ($data as $row) {
            $member_type = $row->member_type;
            if ($row->member_type == "1")
                $member_type = "บุคคลทั่วไป";
            else
                $member_type = "นิติบุคคล [" . $row->company_name . "]";
            $body .= "<tr>";
            $body .= "<td>$no</td>";
            $body .= "<td>" . $prefix[$row->prefix] . $row->prefix_spec . "</td>";
            $body .= "<td>" . $row->first_name . "</td>";
            $body .= "<td>" . $row->last_name . "</td>";
            $body .= "<td>" . $row->birth_day . "</td>";
            $body .= "<td>" . $row->address . "</td>";
            $body .= "<td>" . $row->p['thai_name'] . "</td>";
            $body .= "<td>" . $row->zipcode . "</td>";
            $body .= "<td>" . $row->phone_no . "</td>";
            $body .= "<td>" . $row->email . "</td>";
            $body .= "<td>" . $member_type . "</td>";
            $body .= "<td>" . $row->create_date . "</td>";
            $body .= "<td>" . $row->last_login . "</td>";
            $body .= "</tr>";
            $no++;
        }
        $body .= "</table>";
        $foot = "</body></html>";

        header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Disposition: attachment; filename=memberlist_' . date("YmdHis") . '.xls');
        header('Pragma: no-cache');
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        $html = $head . $body . $foot;
        echo $html;
        Yii::app()->end();
    }

    public function actionExportService() {
        $this->layout = false;
        $model = new Service();

        $model->full_name = $_REQUEST['full_name'];
        $model->company_name = $_REQUEST['company_name'];
        $model->product_code = $_REQUEST['product_code'];
        $model->status = $_REQUEST['status'];
        $model->start_date = $_REQUEST['start_date'];
        $model->end_date = $_REQUEST['end_date'];
        $model->service_type = $_REQUEST['service_type'];
        $data = $model->backend_search()->data;
        $head = "<html><head></head><body>";
        $body = "";
        $prefix[0] = " ";
        $prefix[1] = " นาย ";
        $prefix[2] = " นาง ";
        $prefix[3] = " นางสาว ";
        $prefix[4] = " ";
        $status[0] = "มาใหม่";
        $status[1] = "ติดต่อแล้ว";
        $no = 1;
        $body .= "<table border='1'><tr><th>No.</th><th>Name</th><th>Company Name</th><th>Product Code</th><th>Detail</th><th>E-Mail</th><th>Phone no.</th><th>Register Date</th><th>Status</th></tr>";
        foreach ($data as $row) {

            $body .= "<tr>";
            $body .= "<td>$no</td>";
            $body .= "<td>" . $row->full_name . "</td>";
            $body .= "<td>" . $row->company_name . "</td>";
            $body .= "<td>" . $row->product_code . "</td>";
            $body .= "<td>" . $row->detail . "</td>";
            $body .= "<td>" . $row->email . "</td>";
            $body .= "<td>" . $row->phone_no . "</td>";
            $body .= "<td>" . $row->create_date . "</td>";
            $body .= "<td>" . $status[$row->status] . "</td>";
            $body .= "</tr>";
            $no++;
        }
        $body .= "</table>";
        $foot = "</body></html>";

        header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Disposition: attachment; filename=service_requestlist_' . date("YmdHis") . '.xls');
        header('Pragma: no-cache');
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        $html = $head . $body . $foot;
        echo $html;
        Yii::app()->end();
    }

    public function actionExportTrainee() {
        $this->layout = false;
        $model = new Course();

        $model->fullname = $_REQUEST['fullname'];
        $model->company_name = $_REQUEST['company_name'];
        $model->sale_name = $_REQUEST['sale_name'];
        $model->status = $_REQUEST['status'];
        $model->start_date = $_REQUEST['start_date'];
        $model->end_date = $_REQUEST['end_date'];
        $model->training_id = $_REQUEST['training_id'];
        $data = $model->backend_search()->data;
        $head = "<html><head></head><body>";
        $body = "";
        $prefix[0] = " ";
        $prefix[1] = " นาย ";
        $prefix[2] = " นาง ";
        $prefix[3] = " นางสาว ";
        $prefix[4] = " ";
        $status[0] = "มาใหม่";
        $status[1] = "ติดต่อแล้ว";
        $no = 1;
        $body .= "<table border='1'><tr><th>No.</th><th>Subject</th><th>Training Date</th><th>Training Person</th><th>Name</th><th>Company Name</th><th>Company Type</th><th>Address</th><th>E-Mail</th><th>Tel</th><th>Mobile</th><th>Fax</th><th>Sale name</th><th>Sale company</th><th>Register Date</th><th>Status</th></tr>";
        foreach ($data as $row) {

            $body .= "<tr>";
            $body .= "<td>$no</td>";
            $body .= "<td>" . $row->course['subject_th'] . "</td>";
            $body .= "<td>" . $row->training_date . "</td>";
            $body .= "<td>" . $row->training_person . "</td>";
            $body .= "<td>" . $row->fullname . "</td>";
            $body .= "<td>" . $row->company_name . "</td>";
            $body .= "<td>" . $row->company_type . "</td>";
            $body .= "<td>" . $row->address . "</td>";
            $body .= "<td>" . $row->email . "</td>";
            $body .= "<td>" . $row->tel . "</td>";
            $body .= "<td>" . $row->mobile . "</td>";
            $body .= "<td>" . $row->fax . "</td>";
            $body .= "<td>" . $row->sale_name . "</td>";
            $body .= "<td>" . $row->sale_company . "</td>";
            $body .= "<td>" . $row->create_date . "</td>";
            $body .= "<td>" . $status[$row->status] . "</td>";
            $body .= "</tr>";
            $no++;
        }
        $body .= "</table>";
        $foot = "</body></html>";

        header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Disposition: attachment; filename=traineelist_' . date("YmdHis") . '.xls');
        header('Pragma: no-cache');
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        $html = $head . $body . $foot;
        echo $html;
        Yii::app()->end();
    }

    public function actionServiceRequestList() {
        $this->layout = false;
        $model = new Member();

        $model->first_name = $_REQUEST['first_name'];
        $model->last_name = $_REQUEST['last_name'];
        $model->email = $_REQUEST['email'];
        $model->phone_no = $_REQUEST['phone_no'];
        $model->start_date = $_REQUEST['start_date'];
        $model->end_date = $_REQUEST['end_date'];

        $data = $model->search()->data;
        $head = "<html><head></head><body>";
        $body = "";
        $prefix[0] = " ";
        $prefix[1] = " นาย ";
        $prefix[2] = " นาง ";
        $prefix[3] = " นางสาว ";
        $prefix[4] = " ";
        $no = 1;
        $body .= "<table border='1'><tr><th>No.</th><th>Prefix</th><th>First Name</th><th>Last Name</th><th>Birth Day</th><th>Address</th><th>Province</th><th>Zipcode</th><th>Phone No</th><th>Email</th><th>Member Type</th><th>Register Date</th><th>Last Login</th></tr>";
        foreach ($data as $row) {
            $member_type = $row->member_type;
            if ($row->member_type == "1")
                $member_type = "บุคคลทั่วไป";
            else
                $member_type = "นิติบุคคล [" . $row->company_name . "]";
            $body .= "<tr>";
            $body .= "<td>$no</td>";
            $body .= "<td>" . $prefix[$row->prefix] . $row->prefix_spec . "</td>";
            $body .= "<td>" . $row->first_name . "</td>";
            $body .= "<td>" . $row->last_name . "</td>";
            $body .= "<td>" . $row->birth_day . "</td>";
            $body .= "<td>" . $row->address . "</td>";
            $body .= "<td>" . $row->p['thai_name'] . "</td>";
            $body .= "<td>" . $row->zipcode . "</td>";
            $body .= "<td>" . $row->phone_no . "</td>";
            $body .= "<td>" . $row->email . "</td>";
            $body .= "<td>" . $member_type . "</td>";
            $body .= "<td>" . $row->create_date . "</td>";
            $body .= "<td>" . $row->last_login . "</td>";
            $body .= "</tr>";
            $no++;
        }
        $body .= "</table>";
        $foot = "</body></html>";

        header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Disposition: attachment; filename=memberlist_' . date("YmdHis") . '.xls');
        header('Pragma: no-cache');
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        $html = $head . $body . $foot;
        echo $html;
        Yii::app()->end();
    }

    public function actionExportContact() {
        switch ($_REQUEST['type']) {
            case "0":
                if (!$this->checkUserPermission('contact', '3')) {
                    $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
                }
                break;
            case "1":
                if (!$this->checkUserPermission('request_qoutation', '3')) {
                    $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
                }
                break;
        }


        $this->layout = false;
        $model = new Contact();

        $model->fullname = $_REQUEST['fullname'];
        $model->company_name = $_REQUEST['company_name'];
        $model->subject = $_REQUEST['subject'];
        $model->status = $_REQUEST['status'];
        $model->start_date = $_REQUEST['start_date'];
        $model->end_date = $_REQUEST['end_date'];
        $model->detail = $_REQUEST['detail'];
        $model->type = $_REQUEST['type'];


        $data = $model->search()->data;
        $head = "<html><head></head><body>";
        $body = "";
        $prefix[0] = " ";
        $prefix[1] = " นาย ";
        $prefix[2] = " นาง ";
        $prefix[3] = " นางสาว ";
        $prefix[4] = " ";
        $status[0] = "มาใหม่";
        $status[1] = "ติดต่อแล้ว";
        $no = 1;
        $body .= "<table border='1'><tr><th>No.</th>
				<th >หัวข้อ</th>
              <th >รายละเอียด</th>
              <th >ชื่อ - นามสกุล</th>
              <th >ชื่อบริษัท</th>
			   <th >ตำแหน่ง</th>
              <th >เบอร์มือถือ</th>
			   <th >fax</th>
			   <th >Email</th>
			   <th >ที่อยู่</th>
			    <th >website</th>
				<th >วันที่ลงทะเบียน</th>
			  <th>Status</th>
			   
			  </tr>";
        foreach ($data as $row) {

            $body .= "<tr>";
            $body .= "<td>$no</td>";
            $body .= "<td>" . $row->subject . "</td>";
            $body .= "<td>" . str_replace(chr(13), "<br />", $row->detail) . "</td>";
            $body .= "<td>" . $row->fullname . "</td>";
            $body .= "<td>" . $row->company_name . "</td>";
            $body .= "<td>" . $row->position . "</td>";
            $body .= "<td>" . $row->tel . "</td>";
            $body .= "<td>" . $row->fax . "</td>";
            $body .= "<td>" . $row->email . "</td>";
            $body .= "<td>" . $row->address . "</td>";
            $body .= "<td>" . $row->website . "</td>";
            $body .= "<td>" . $row->create_date . "</td>";
            $body .= "<td>" . $status[$row->status] . "</td>";
            $body .= "</tr>";
            $no++;
        }
        $body .= "</table>";
        $foot = "</body></html>";

        header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Disposition: attachment; filename=contactlist_' . date("YmdHis") . '.xls');
        header('Pragma: no-cache');
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        $html = $head . $body . $foot;
        echo $html;
        Yii::app()->end();
    }

    public function actiondeleteTrainee() {
        $id = $_REQUEST['id'];
        Course::model()->deleteByPk($id);
        echo "OK";
    }

    public function actionMemberList() {
        $model = new Member();

        if (isset($_POST['act']) && $_POST['act'] == "deleteAll") {
            if (!$this->checkUserPermission('member', '3'))
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/admin/accessdenied');
            if (isset($_POST['member_id']) && count($_POST['member_id']) > 0) {
                $mid = $_POST['member_id'];
                foreach ($mid as $m) {
                    Member::model()->deleteAll('id = :id', array(":id" => $m));
                }
            }
        }
        if (isset($_POST['Member'])) {
            $model->attributes = $_POST['Member'];
            $model->first_name = $_POST['Member']['first_name'];
            $model->last_name = $_POST['Member']['last_name'];
            $model->email = $_POST['Member']['email'];
            $model->phone_no = $_POST['Member']['phone_no'];
            $model->start_date = $_POST['Member']['start_date'];
            $model->end_date = $_POST['Member']['end_date'];
        }
        $model->display_perpage = "10";
        //$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        if (isset($_REQUEST['key_address']) && $_REQUEST['key_address'] != "")
            $model->key_word = $_REQUEST['key_address'];
        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;

        $this->render('memberlist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionMemberDetail() {
        $this->layout = false;

        $id = $_REQUEST['id'];
        $model = Member::model()->find('id=:id', array(':id' => $id));
        $param["ProvinceName"] = "";
        if (count($model) > 0) {
            $modelProvince = Provinces::model()->find('province_id=:province_id', array(':province_id' => $model->province));
            if (count($modelProvince) > 0)
                $param["ProvinceName"] = $modelProvince->thai_name;
        }
        $this->render('memberdetail', array('model' => $model, 'param' => $param));
    }

    public function actionDeleteMember() {
        if (!$this->checkUserPermission('member', '3')) {
            echo "DENIED";
        } else {
            $id = $_REQUEST['id'];
            Member::model()->deleteByPk($id);
            echo "OK";
        }
    }

    public function actionDeleteSpec() {
        $id = $_REQUEST['id'];
        $arr['result'] = "OK";
        Product::model()->updateByPk($id, array('attach' => ''));
        echo json_encode($arr);
    }

    public function actionDeleteProductImage() {
        $id = $_REQUEST['id'];
        $img_id = $_REQUEST['image_id'];
        $arr['result'] = "OK";

        Product::model()->updateByPk($id, array('pic' . $img_id => ''));
        echo json_encode($arr);
    }

    public function actionAccessDenied() {
        $this->render('accessdenied');
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function deleteFile($midPath, $fileName) {
        $path = Yii::app()->basePath . "/../" . $midPath . $fileName;
        if (file_exists($path)) {
            unlink($path);
        }
    }

    public function uploadFile2($p, $file_name, $file_tmp_name, $prefix = "") {
        $filename = "";
        $path = Yii::app()->basePath . "/../" . $p;
        if (!file_exists($path))
            mkdir($path, 0777);
        $f = $file_name;
        if ($f != "") {
            $ext = pathinfo($f, PATHINFO_EXTENSION);
            $filename = $prefix . $this->generateRandomString() . "." . $ext;
            copy($file_tmp_name, $path . "/" . $filename);
        }
        return $filename;
    }

    public function checkUserPermission($menuCode, $attNo) {
        $result = false;

        $sql = "SELECT menuid, flag, m.menu_code menucode, user_id userid, g.group_name groupname ";
        $sql .= "FROM ( ";
        $sql .= "    SELECT TRIM( SUBSTRING_INDEX( p.menu_id,  '_', 1 ) ) menuid, TRIM( SUBSTRING_INDEX( p.menu_id,  '_', -1 ) ) flag, p.user_id ";
        $sql .= "    FROM tb_premission p ";
        $sql .= "    WHERE p.user_id ='" . Yii::app()->user->getValue("group_id") . "' AND p.menu_id LIKE  '%\_%' ";
        $sql .= ")tb ";
        $sql .= "   LEFT JOIN tb_menu m ON tb.menuid = m.id ";
        $sql .= "   LEFT JOIN tb_user_group g ON tb.user_id = g.id ";
        $sql .= "WHERE m.menu_code =  '" . $menuCode . "' and flag =  '" . $attNo . "' ";

        $results = Premission::model()->findAllBySql($sql);

        return (count($results) > 0) ? true : false;
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

}
