<?php
ob_clean();
require_once(Yii::app()->basePath . '/components/php_dom.php');

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    function init() {
        parent::init();
        $app = Yii::app();

//        if (isset($_POST['lang'])) {
//            $app->language = $_POST['lang'];
//            $app->session['_lang'] = $app->language;
//        } else if (isset($app->session['_lang'])) {
//            $app->language = $app->session['_lang'];
//        }
//
//        if (strtolower($app->language) == "en")
//            Yii::app()->setLanguage("en");
//        else
//            Yii::app()->setLanguage("th");
//--------------------
// Init language -----
        if (isset($_COOKIE['lang'])) {
            $app->language = $_COOKIE['lang'];
        } else {
            $app->language = "th";
        }

// Init currency
        if (!isset(Yii::app()->session['currency'])) {
            Yii::app()->session['currency'] = "baht";
        }

// Init dollar exchange rate
        if (!isset(Yii::app()->session['dollar_ext_rate'])) {
            try {
//Yii::app()->session['dollar_ext_rate'] = file_get_html('http://themoneyconverter.com/USD/THB.aspx')->find('div[class=switch-table]', 0)->find('b', 0)->innertext;
                $modelConfig = new Config();
                $dataDollarRate = $modelConfig->searchByCode("EXCHANGE_RATE_DOLLAR")->data;
                if (count($dataDollarRate) > 0)
                    Yii::app()->session['dollar_ext_rate'] = $dataDollarRate[0]["cfg_value"];
            } catch (Exception $e) {
                Yii::app()->session['dollar_ext_rate'] = "32.56";
                echo "Get Currency api service error !!!";
            }
        }
    }

    public function actions() {
        return array(
// captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
// They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionSetLang() {
        if (isset($_POST['lang'])) {
            $app->language = strtolower($_POST['lang']);
            $app->session['_lang'] = $app->language;
        } else if (isset($app->session['_lang'])) {
            $app->language = $app->session['_lang'];
        }
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->layout = "main2column";

        $modelProduct = new Product();

// Recommend product
        $data["product_recommend"] = $modelProduct->index_search_highlight()->data;

// Best seller product  
        $data["product_best_seller"] = $modelProduct->search_best_seller_for_index()->data;

        $p = Content::model()->find('content_code=:content_code', array(':content_code' => 'popup'));
        $i = Content::model()->find('content_code=:content_code', array(':content_code' => 'Intro'));
        $b = array();
        $bm = new Banner();
        $b = $bm->search()->data;
        $param['banner'] = $b;
        $param['popup'] = $p;
        $this->saveStat(0, 0);

        if (isset(Yii::app()->session['open_intro'])) {
            $this->render('index', array('data' => $data, 'param' => $param));
        } else {
            Yii::app()->session['open_intro'] = Yii::app()->getSession()->sessionID;
            if ($i->attr1 == "1") {
                $this->layout = false;
                $this->render('intro', array('model' => $i));
            } else {
                $this->render('index', array('data' => $data, 'param' => $param));
            }
        }
    }

    public function actionIndex2() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $model = new Product();
        $n_model = new Content();
        $data1 = $model->search_highlight()->data;
        $data = $model->search_last()->data;
        $data2 = $n_model->search_for_index()->data;
//	$b = Content::model()->find('content_code=:content_code',array(':content_code'=>'Banner'));
        $p = Content::model()->find('content_code=:content_code', array(':content_code' => 'popup'));
        $bd = Content::model()->find('content_code=:content_code', array(':content_code' => 'banner_day'));
        $b = array();
        $bm = new Banner();
        $b = $bm->search()->data;
        $param['banner'] = $b;
        $param['popup'] = $p;
        $param['banner_day'] = $bd;
        $this->saveStat(0, 0);
        $this->render('index', array('model' => $model, 'data' => $data, 'data1' => $data1, 'data2' => $data2, 'param' => $param));
    }

    public function actionPopup() {
        $p = Content::model()->find('content_code=:content_code', array(':content_code' => 'popup'));
        echo Yii::text_lang(array(
            $p->content_1,
            $p->content_2,
            $p->content_3,
            $p->content_4,
            $p->content_5));
    }

    public function actionBrand() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $model = new Brand();
        $brand = Brand::model()->find('id=:id', array(':id' => $_GET['id']));
        $p_model = new ProductGroup();
        $p_model->brand_id = $_GET['id'];
        $data = $p_model->searchByBrand()->data;
        $this->saveStat($_GET['id'], 1);
        $this->render('brand', array('brand' => $brand, 'data' => $data));
    }

    public function actionGroup() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $model = new Product();
        if (isset($_GET['type']) && strtolower($_GET['type']) == "promotion")
            $model->is_highlight = '1';
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            if (isset($_REQUEST['brand_id'])) {
                $brand_id = $_REQUEST['brand_id'];
                $model->brand_id = $brand_id;
                $model->group_id = $id;
            }
        } else {
            $id = 0;
        }
        $model->display_perpage = "40";
//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        $model->current_page = $current_page;
        $data = $model->front_group_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;


        $group = ProductGroup::model()->find('id=:id', array(':id' => $id));

        if (isset($_REQUEST['group_id']))
            $param['group_id'] = $_REQUEST['group_id'];
        else
            $param['group_id'] = "";

        if (isset($_REQUEST['brand_id']))
            $param['brand_id'] = $_REQUEST['brand_id'];
        else
            $param['brand_id'] = "";

        $series = array();
        $view = "all_group";
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            if (isset($_REQUEST['brand_id'])) {
                $serie = new Series();
                $serie->group_id = $_GET['id'];
                $serie->brand_id = $_GET['brand_id'];
                $series = $serie->getDataByBrandAndGroupId()->data;
                $view = "group";
            }
        }
        $id = (isset($_GET['id']) ? $_GET['id'] : 0);
        $this->saveStat($id, 2);
        $this->render($view, array('group' => $group, 'model' => $model, 'data' => $data, 'series' => $series, 'param' => $param));
    }

    public function actionNewsList() {
        $this->layout = "main2column";

        $model = new Content();
        $model->content_code = 'News';

        $criteria = new CDbCriteria;
        $criteria->compare('content_code', $model->content_code);
        $criteria->addCondition("status = '1' ");

        $news = new CActiveDataProvider('Content', array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 't.id desc',),
            'pagination' => array('pageSize' => 6),
        ));

        $this->render("newslist", array('news' => $news));
    }

    public function actionNewsDetail() {
        $this->layout = "main2column";

        $id = (isset($_REQUEST['id']) && $_REQUEST['id'] != "" ? $_REQUEST['id'] : 0);
        $model = new Content();

        $data["news"] = Content::model()->findByPk($id);
        $this->render("newsdetail", array('data' => $data));
    }

    public function actionPromotionList() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $model = new Content();
        $model->content_code = 'promotion';
        $model->display_perpage = "40";
//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        $model->current_page = $current_page;
        $data = $model->getAllPromotion()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;


        $this->saveStat(0, 4);

        $this->render("all_promotion", array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionSeries() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $model = new Product();
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            if (isset($_REQUEST['brand_id'])) {
                $brand_id = $_REQUEST['brand_id'];
                $model->brand_id = $brand_id;
                $model->group_id = $_GET['group_id'];
                $model->serie_id = $_GET['id'];
            }
        } else {
            $id = 0;
        }
        $model->display_perpage = "40";
//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
        $model->current_page = $current_page;
        $data = $model->front_serie_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;


        $group = ProductGroup::model()->find('id=:id', array(':id' => $_GET['group_id']));
        $serie = Series::model()->find('id=:id', array(':id' => $id));
        if (isset($_REQUEST['group_id']))
            $param['group_id'] = $_REQUEST['group_id'];
        else
            $param['group_id'] = "";

        if (isset($_REQUEST['brand_id']))
            $param['brand_id'] = $_REQUEST['brand_id'];
        else
            $param['brand_id'] = "";

        if (isset($_REQUEST['id']))
            $param['serie_id'] = $_REQUEST['id'];
        else
            $param['serie_id'] = "";


        $this->saveStat($id, 4);
        $this->render("series", array('group' => $group, 'model' => $model, 'data' => $data, 'serie' => $serie, 'param' => $param));
    }

    public function actionProduct() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $model = new Product();
        $data1 = $model->search_highlight()->data;
        $data = $model->search_last()->data;
        $this->saveStat(0, 5);
        $this->render('product', array('model' => $model, 'data' => $data, 'data1' => $data1));
    }

    public function actionProductDetail() {
        $this->layout = "main2column";

        if (isset($_REQUEST['id']))
            $id = $_REQUEST['id'];
        else
            $id = 0;
        $model = Product::model()->find('id=:id', array(':id' => $id));
        $m_relate = new RelateProduct();
        $m_relate->product_id = $id;
        $relate = $m_relate->front_search()->data;
        $this->saveStat($id, 5);
        $this->render('productdetail', array('model' => $model, 'relate' => $relate));
    }

    public function actionAddToCart() {
        $this->layout = false;

        $modelMember = new Member();
        if ($modelMember->isLogin() == false) {
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/memberlogin");
        }

        $id = $_GET['id'];
        $model = Product::model()->find("id=:id", array(":id" => $id));
        $amount = 1;
        $cart_data = Yii::app()->getSession()->sessionID;

        if (isset($_REQUEST['amount']))
            $amount = $_REQUEST['amount'];
        if (!is_numeric($amount))
            $amount = 0;
        if (ceil($amount) < 1)
            $amount = 1;

        $product = Product::model()->findByPk($id);
        if ($amount > $product->stock && $product->stock >= 0) {
            echo "<script language='javascript'>\n";
            echo "alert('จำนวนสินค้าที่มีในสต๊อกอาจไม่เพียงพอตามจำนวนที่ท่านต้องการ กรุณาติดต่อฝ่ายขายเพื่อยืนยันจำนวนสินค้าดังกล่าว');\n";
            echo "top.TB_closeWindowButton.click();\n";
            echo "</script>";
            die();
        } else {

            if (!isset(Yii::app()->request->cookies['cart'])) {
                $cookie = new CHttpCookie('cart', $cart_data);
                $cookie->expire = time() + 60 * 60 * 24 * 180;
                Yii::app()->request->cookies['cart'] = $cookie;
            }
            $cart_id = Yii::app()->request->cookies['cart']->value;
//Yii::app()->request->cookies->clear();
            Yii::app()->request->cookies['cart'] = new CHttpCookie('cart', $cart_id);
            $count = Order::model()->count('product_id=:product_id and session_id=:cart_id and status=0 and order_id=0', array('product_id' => $id, ':cart_id' => $cart_id));
            if ($count <= 0) {
                $order = new Order();
            } else {
                $order = Order::model()->find('product_id=:product_id and session_id=:cart_id and status=0 and order_id=0', array('product_id' => $id, ':cart_id' => $cart_id));
            }
            $amount = $amount + (ceil($order->amount));
            $order->amount = $amount;
            $order->product_id = $id;
            $order->session_id = $cart_id;
            $order->order_id = 0;
            $price = 0;
            if ($order->amount >= $model->amount1)
                $price = $model->price1;
            if ($order->amount >= $model->amount2 && $model->amount2 != "0")
                $price = $model->price2;
            if ($order->amount >= $model->amount3 && $model->amount3 != "0")
                $price = $model->price3;
            if ($order->amount >= $model->amount4 && $model->amount4 != "0")
                $price = $model->price4;
            $order->price = $price;
            $order->member_id = 0;
            $order->status = 0;
            if (!$order->save())
                print_r($order->getErrors());



            $modelCartInfo = new CartInfo();
            $dataCartInfo = $modelCartInfo->searchShippingFee($cart_id)->data;

// น้ำหนักรวมสินค้าใน cart ไม่รวมสินค้าสดและร้านอาหารที่จัดส่งฟรี (ก.) + น้ำหนักกล่อง (ก.)
            $weightInfo = $this->calTotalOrderWeight($cart_id);
            $weightIsOver = ($weightInfo['total_weight'] > 2000);

// หาประเภทการส่ง
            $shippingType = "";
            if (!$weightIsOver) {
                $shippingType = "in_reg";
            } else {
                $shippingType = "in_ems";

// Get exist shipping type
                $modelCartInfo = CartInfo::model()->find('session_id=:cartid and order_id=0', array(':cartid' => $cart_id));
                if (count($modelCartInfo) > 0) {
                    if ($modelCartInfo->shipping_type != "in_reg")
                        $shippingType = $modelCartInfo->shipping_type;
                }
            }

// หาอัตราค่าจัดส่ง
            $shipping_fee = $this->getShippingFee($weightInfo['total_weight'], $shippingType);

// Get score 
            $score = $this->calOrderScore($cart_id);

// Save shipping fee
            if (count($dataCartInfo) == 0) {
// insert new info
$modelCartInfo = new CartInfo();
                $modelCartInfo->session_id = $cart_id;
                $modelCartInfo->shipping_fee = floatval($shipping_fee);
                $modelCartInfo->shipping_type = $shippingType;
                $modelCartInfo->score = $score;
                $modelCartInfo->box_weight = $weightInfo['box_weight'];
                $modelCartInfo->product_weight = $weightInfo['product_weight'];
                $modelCartInfo->total_weight = $weightInfo['total_weight'];
                $modelCartInfo->save();
            } else {
                $modelCartInfo = CartInfo::model()->find('session_id=:cartid and order_id=0', array(':cartid' => $cart_id));
                if (count($modelCartInfo) > 0) {
                    $modelCartInfo->shipping_fee = floatval($shipping_fee);
                    $modelCartInfo->shipping_type = $shippingType;
                    $modelCartInfo->score = $score;
                    $modelCartInfo->box_weight = $weightInfo['box_weight'];
                    $modelCartInfo->product_weight = $weightInfo['product_weight'];
                    $modelCartInfo->total_weight = $weightInfo['total_weight'];
                    $modelCartInfo->save();
                }
            }

//echo count($order);
            /* $data = $cookie;
              $data .= $amount . "|" . $_GET['id'];
              Yii::app()->request->cookies['cart'] = new CHttpCookie('cart',$data); */
//$cart_id;

            $this->render('addtocart', array('model' => $model));
        }
    }

    public function actionCart() {
        $this->layout = "main2column";

        $model = new Member();
        if ($model->isLogin() == false) {
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/memberlogin");
        }

        $cart_data = Yii::app()->getSession()->sessionID;
        if (!isset(Yii::app()->request->cookies['cart'])) {
            $cookie = new CHttpCookie('cart', $cart_data);
            $cookie->expire = time() + 60 * 60 * 24 * 180;
            Yii::app()->request->cookies['cart'] = $cookie;
        }
        $cart_id = Yii::app()->request->cookies['cart']->value;
//	Yii::app()->request->cookies->clear();
        Yii::app()->request->cookies['cart'] = new CHttpCookie('cart', $cart_id);
        $count = Order::model()->count('session_id=:cart_id and status=0 and order_id=0', array(':cart_id' => $cart_id));
        $model = new Order();
        $model->session_id = $cart_id;
        $data = $model->get_cart()->data;
        $param['count_item'] = $count;

// Get shipping fee
        $modelCartInfo = new CartInfo();
        $dataCartInfo = $modelCartInfo->searchShippingFee($cart_id)->data;
        $param['shipping_fee'] = 0;
        if (count($dataCartInfo) > 0)
            $param['shipping_fee'] = $dataCartInfo[0]["shipping_fee"];

// จำนวนแต้มในครั้งนี้
        $modelCartInfo = CartInfo::model()->find('session_id=:session_id and order_id=0', array(':session_id' => $cart_id));
        $param['score'] = $modelCartInfo->score;

// อัตราการซือต่อ 1 แต้ม
        $dataScoreRate = Config::model()->searchByCode('SCORE_RATE')->data;
        if (count($dataScoreRate) > 0)
            $param["score_rate"] = $dataScoreRate[0]["cfg_value"];

// น้ำหนักรวม
        $param['total_weight'] = $modelCartInfo->total_weight / 1000;
        $param['shipping_type'] = $modelCartInfo->shipping_type;

        $this->render('cart', array('param' => $param, 'data' => $data));
    }

    public function actionEditProfile() {
        $this->layout = "main2column";

        $session = new CHttpSession;
        $session->open();
        $param = array();
        if (isset($session['user'])) {
            $user = $session['user'];

            $model = Member::model()->findByPk($user->id);
            if (isset($_POST['Member'])) {
                $model->attributes = $_POST['Member'];
                $model->cpassword = $_POST['Member']['cpassword'];
                $model->member_type = $_POST['Member']['member_type'];
                if ($model->validate() && $model->save()) {
                    $param['alert'] = "บันทึกเสร็จสิ้น";
                }
            }
            $model->cpassword = $model->password;
            $this->render('editprofile', array('model' => $model, 'param' => $param));
        } else {
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/memberlogin");
        }
    }

    public function actionPaymentNotify() {
        $this->layout = "main2column";

        $session = new CHttpSession;
        $session->open();
        $param = array();
        if (isset($session['user'])) {
            $user = $session['user'];

            $model = new PaymentNotify();
            $model->transfer_amount = 0;

            if (isset($_POST) && count($_POST) > 0) {
                $model->attributes = $_POST['PaymentNotify'];

                if ($model->validate() && $model->save()) {

                    // Upload file
                    if (isset($_FILES['file1']) && $_FILES['file1']['name'] != "") {
                        $file_name = $model->id . '_' . $_FILES['file1']['name'];
                        $model->transfer_document = $file_name;

                        copy($_FILES['file1']['tmp_name'], Yii::app()->basePath . '/../images/payment/' . $file_name);
                        $model->save();
                    }

                    // Send email
                    $modelMember = Member::model()->findByPk($user->id);
                    $mBank = Bank::model()->findByPk($model->bank_id);

                    if (Yii::allow_send_email()) {
                        $senderEmail = "";
                        $senderName = "";
                        $modelConfig = new Config();
                        $dataConfig = $modelConfig->searchByCode('SENDER_EMAIL')->data;
                        if (count($dataConfig) > 0)
                            $senderEmail = $dataConfig[0]["cfg_value"];
                        $dataConfig = $modelConfig->searchByCode('SENDER_NAME')->data;
                        if (count($dataConfig) > 0)
                            $senderName = $dataConfig[0]["cfg_value"];

                        $body = "==============<br/>";
                        $body .= "แจ้งการชำระเงิน<br/>";
                        $body .= "==============<br/>";
                        $body .= "<br/>";
                        $body .= "คุณ " . $modelMember->first_name . " " . $modelMember->last_name . "<br/>";
                        $body .= "ได้ทำการแจ้งการชำระเงิน รหัสใบสั่งซื้อ : " . substr("000000000" . $model->order_id, -9) . " โดยมีรายละเอียดดังนี้<br/>";
                        $body .= "&nbsp;&nbsp;&nbsp;จำนวนเงินที่ชำระ : " . number_format(Yii::currencyAmount($model->transfer_amount), 2, ".", ",") . " บาท<br/>";
                        $body .= "&nbsp;&nbsp;&nbsp;วันเวลา : " . $model->transfer_date . " " . $model->transfer_time . "<br/>";
                        $body .= "&nbsp;&nbsp;&nbsp;โอนเข้าบัญชี : " . $mBank->bank_name . " เลขบัญชี : " . $mBank->acc_no . "<br/>";
                        $body .= "&nbsp;&nbsp;&nbsp;ธนาคาร/สาขา ที่ลูกค้าโอนเข้า : " . $model->member_bank . "<br/>";
                        $body .= "&nbsp;&nbsp;&nbsp;หมายเหตุ : " . $model->remark . "<br/>";
                        if ($model->transfer_document != "") {
                            $body .= "&nbsp;&nbsp;&nbsp;หลักฐาน/สลิปการโอน :<br/>";
                            $body .= "<img src='http://biffoodservice.com/images/payment/" . $model->transfer_document . "' style='width:400px;' />";
                        }
                        $body .= "<br/><br/>สามารถเข้าดูรายละเอียดการชำระเงินได้ที่นี่ <a href='http://biffoodservice.com/index.php/admin/'>คลิกที่นี่</a>";
                        $body .= "<br/><br/><br/>";
                        $body .= "Regards,<br/>";
                        $body .= "Biffoodservice.com";

                        $subject = Yii::prefixMailSubject() . "แจ้งเตือนการชำระเงินสั่งซื้อสินค้า";

                        $mail_to = $senderEmail;
                        $mail_subject = '=?utf-8?B?' . base64_encode($subject) . '?='; // email subject line
                        $mail_body = $body;

                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
                        //$headers .= 'To: ' . $mail_to . "\r\n";
                        $headers .= 'From:  ' . $senderName . ' <' . $senderEmail . '>' . "\r\n";
                        
                        mail($mail_to, $mail_subject, $mail_body, $headers);
                    }

                    $param['alert'] = "ส่งข้อมูลการชำระเงินเรียบร้อย";

                    // clear value
                    $model = new PaymentNotify();
                } else {
                    //print_r($model->getErrors());
                }
            }

            $param['userid'] = $user->id;
            $this->render('paymentnotify', array('model' => $model, 'param' => $param));
        } else {
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/memberlogin");
        }
    }

    public function actionUpdateCart() {
        $id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : 0);
        $amount = $_REQUEST['val'];
        $product_id = $_REQUEST['product_id'];
        $product = Product::model()->findByPk($product_id);
        if ($amount > $product->stock && $product->stock >= 0) {
            $data = array('result' => 'error', 'msg' => 'จำนวนสินค้าที่มีในสต๊อกอาจไม่เพียงพอตามจำนวนที่ท่านต้องการ กรุณาติดต่อฝ่ายขายเพื่อยืนยันจำนวนสินค้าดังกล่าว');
        } else {
            if ($amount >= $product->amount1)
                $price = $product->price1;

            if ($amount >= $product->amount2 && $product->amount2 != "0")
                $price = $product->price2;

            if ($amount >= $product->amount3 && $product->amount3 != "0")
                $price = $product->price3;

            if ($amount >= $product->amount4 && $product->amount4 != "0")
                $price = $product->price4;

            Order::model()->updateByPk($id, array('amount' => $amount, 'price' => $price));


            $cart_id = Yii::app()->request->cookies['cart']->value;

            $modelCartInfo = CartInfo::model()->find('session_id=:cartid and order_id=0', array(':cartid' => $cart_id));

// น้ำหนักรวมสินค้าใน cart ไม่รวมสินค้าสดและร้านอาหารที่จัดส่งฟรี (ก.) + น้ำหนักกล่อง (ก.)
            $weightInfo = $this->calTotalOrderWeight($cart_id);
            $weightIsOver = ($weightInfo['total_weight'] > 2000);

// หาประเภทการส่ง
            $shippingType = "";
            if (!$weightIsOver) {
                $shippingType = "in_reg";
            } else {
                $shippingType = "in_ems";

// Get exist shipping type
                $modelCartInfo = CartInfo::model()->find('session_id=:cartid and order_id=0', array(':cartid' => $cart_id));
                if (count($modelCartInfo) > 0) {
                    if ($modelCartInfo->shipping_type != "in_reg")
                        $shippingType = $modelCartInfo->shipping_type;
                }
            }

// หาอัตราค่าจัดส่ง
            $shipping_fee = $this->getShippingFee($weightInfo['total_weight'], $shippingType);

// Get score 
            $score = $this->calOrderScore($cart_id);

// Save shipping fee
            if (count($modelCartInfo) > 0) {
                $modelCartInfo->shipping_fee = floatval($shipping_fee);
                $modelCartInfo->shipping_type = $shippingType;
                $modelCartInfo->score = $score;
                $modelCartInfo->box_weight = $weightInfo['box_weight'];
                $modelCartInfo->product_weight = $weightInfo['product_weight'];
                $modelCartInfo->total_weight = $weightInfo['total_weight'];
                $modelCartInfo->save();
            }

            $data = array('result' => 'success', 'totalweight' => (floatval($weightInfo['total_weight']) / 1000), 'weightover' => $weightIsOver, 'shiptype' => $shippingType, 'shippingfee' => $shipping_fee, 'score' => $score, 'amount' => $amount, 'price' => number_format($price, 2, ".", ","), 'total' => number_format(($price * $amount), 2, ".", ","));
        }
        echo json_encode($data);
    }

    public function actionCheckCart() {
        $ids = (isset($_REQUEST['id']) ? $_REQUEST['id'] : 0);
        $amount = $_REQUEST['val'];
        $product_id = $_REQUEST['product_id'];
        $pass = true;
        $data = array();
        $i = 0;
        foreach ($ids as $id) {
            $product = Product::model()->findByPk($product_id[$i]);
            if ($amount[$i] > $product->stock && $product->stock >= 0) {
                $pass = false;
            }
            $i++;
        }
        if (!$pass) {
            $data = array('result' => 'error');
        } else {
            $data = array('result' => 'success');
        }
        echo json_encode($data);
    }

    public function actionDeleteCart() {
        $id = 0;
        if (isset($_REQUEST['id']))
            $id = $_REQUEST['id'];
        Order::model()->deleteByPk($id);

        $cart_id = Yii::app()->request->cookies['cart']->value;

        $modelCartInfo = CartInfo::model()->find('session_id=:cartid and order_id=0', array(':cartid' => $cart_id));

// น้ำหนักรวมสินค้าใน cart ไม่รวมสินค้าสดและร้านอาหารที่จัดส่งฟรี (ก.) + น้ำหนักกล่อง (ก.)
        $weightInfo = $this->calTotalOrderWeight($cart_id);
        $weightIsOver = ($weightInfo['total_weight'] > 2000);

// หาประเภทการส่ง
        $shippingType = "";
        if (!$weightIsOver) {
            $shippingType = "in_reg";
        } else {
            $shippingType = "in_ems";

// Get exist shipping type
            $modelCartInfo = CartInfo::model()->find('session_id=:cartid and order_id=0', array(':cartid' => $cart_id));
            if (count($modelCartInfo) > 0) {
                if ($modelCartInfo->shipping_type != "in_reg")
                    $shippingType = $modelCartInfo->shipping_type;
            }
        }

// หาอัตราค่าจัดส่ง
        $shipping_fee = $this->getShippingFee($weightInfo['total_weight'], $shippingType);

// Get score 
        $score = $this->calOrderScore($cart_id);

// Save shipping fee
        if (count($modelCartInfo) > 0) {
            $modelCartInfo->shipping_fee = floatval($shipping_fee);
            $modelCartInfo->shipping_type = $shippingType;
            $modelCartInfo->score = $score;
            $modelCartInfo->box_weight = $weightInfo['box_weight'];
            $modelCartInfo->product_weight = $weightInfo['product_weight'];
            $modelCartInfo->total_weight = $weightInfo['total_weight'];
            $modelCartInfo->save();
        }

        echo "OK";
    }

    public function actionCartUpdateShipping() {
        $shippingType = 0;
        if (isset($_REQUEST['shiptype']))
            $shippingType = $_REQUEST['shiptype'];

// Update shipping fee
        $cart_id = Yii::app()->request->cookies['cart']->value;

        $modelCartInfo = CartInfo::model()->find('session_id=:cartid and order_id=0', array(':cartid' => $cart_id));

// น้ำหนักรวมสินค้าใน cart ไม่รวมสินค้าสดและร้านอาหารที่จัดส่งฟรี (ก.) + น้ำหนักกล่อง (ก.)
        $weightInfo = $this->calTotalOrderWeight($cart_id);

// หาอัตราค่าจัดส่ง
        $shipping_fee = $this->getShippingFee($weightInfo['total_weight'], $shippingType);

// Get score 
        $score = $this->calOrderScore($cart_id);

// Save shipping fee
        if (count($modelCartInfo) > 0) {
            $modelCartInfo->shipping_fee = floatval($shipping_fee);
            $modelCartInfo->shipping_type = $shippingType;
            $modelCartInfo->score = $score;
            $modelCartInfo->box_weight = $weightInfo['box_weight'];
            $modelCartInfo->product_weight = $weightInfo['product_weight'];
            $modelCartInfo->total_weight = $weightInfo['total_weight'];
            $modelCartInfo->save();
        }

        echo "OK";
    }

    public function actionCartLogin() {
        $session = new CHttpSession;
        $session->open();

        $this->layout = "main2column";
        $loginInfo = "";

        $model = new Member();

        if ($model->isLogin()) {
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/cartshipping");
        } else {
            if (isset($_POST['Member'])) {
                $model->attributes = $_POST['Member'];
                $lg = $model->login();
                if (!$lg) {
                    $loginInfo = "ไม่สามารถเข้าระบบได้ เนื่องจากไม่มีบัญชีผู้ใช้ตามที่ระบุ !!!";
                } else {

                    $session['user'] = $lg;
                    $_SESSION['user'] = $lg;
                    $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/cartshipping");
                }
            }
        }

        $this->render('cartlogin', array('model' => $model, 'loginInfo' => $loginInfo));
    }

//  public function actionCartLogin() {
//        $model = new Member();
//
//        if (isset($_POST['Member'])) {
//            $model->attributes = $_POST['Member'];
//
//            $lg = $model->login();
//            if (!$lg) {
//                 echo "Login Fail!";
//                
//            } else {
//                echo "loged in";
//                $session = new CHttpSession;
//                $session->open();
//                $session['user'] = $lg;
//                $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/cartshipping");
//            }
//        }
//        $this->render('cartlogin', array('model' => $model));
//    }
//    
    public function actionCartShipping() {
        $this->layout = "main2column";

        $model = new MemberOrder();

        if (isset($_REQUEST['rd_address'])) {
            $cart_data = Yii::app()->getSession()->sessionID;

            if (!isset(Yii::app()->request->cookies['cart'])) {
                $cookie = new CHttpCookie('cart', $cart_data);
                $cookie->expire = time() + 60 * 60 * 24 * 180;
                Yii::app()->request->cookies['cart'] = $cookie;
            }
            $cart_id = Yii::app()->request->cookies['cart']->value;
//Yii::app()->request->cookies->clear();
            Yii::app()->request->cookies['cart'] = new CHttpCookie('cart', $cart_id);

            $session = new CHttpSession;
            $session->open();
            $lg = $session['user'];
            if (!isset($lg))
                $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/cartlogin");

            $model->verifyCode = $_POST['MemberOrder']['verifyCode'];

            if (isset($_POST['MemberOrder']) && $_REQUEST['rd_address'] == "2") {
                $model->attributes = $_POST['MemberOrder'];
            } else {
//var_dump($lg);                
                $model->fullname = $lg->first_name . ' ' . $lg->last_name;
                $model->company_name = $lg->company_name;
                $model->province = $lg->province;
                $model->zipcode = $lg->zipcode;
                $model->phone_no = $lg->phone_no;
                $model->address = $lg->address;
                $model->remark = $_POST['MemberOrder']["remark"];
                $model->require_taxslipt = $_POST['MemberOrder']["require_taxslipt"];
            }

            $model->member_id = $lg->id;
            $model->status = 0;
            if ($model->validate()) {
                Yii::app()->session['model_cart'] = $model;
                $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/confirmorder");
//$this->redirect(array("confirmorder",'captcha'=>$model->verifyCode));
                /* if ($model->save()) {
                  Order::model()->updateAll(array('member_id' => $lg->id, 'status' => 1, 'order_id' => $model->id), "session_id='" . $cart_id . "' and status=0 and order_id=0");
                  CartInfo::model()->updateAll(array('order_id' => $model->id), "session_id='" . $cart_id . "' and order_id=0");
                  //Yii::app()->request->cookies->clear();
                  $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/cartthankyou");
                  //User::model()->updateAll(array('last_login'=>date("Y-m-d H:i:s")),"username='" . Yii::app()->user->getId() ."'");
                  } */
            }
        }

        $this->render('cartshipping', array('model' => $model));
    }

    public function actionConfirmOrder() {
        $this->layout = "main2column";

        $session = new CHttpSession;
        $session->open();
        $lg = $session['user'];
        if (!isset($lg))
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/cartlogin");

        $model = new MemberOrder();

        if (isset($_POST) && count($_POST) > 0) {
            $model = Yii::app()->session['model_cart'];

            if ($model->save(false)) {
//echo "model save";
                $cart_id = Yii::app()->request->cookies['cart']->value;

                Order::model()->updateAll(array('member_id' => $lg->id, 'status' => 1, 'order_id' => $model->id), "session_id='" . $cart_id . "' and status=0 and order_id=0");
                CartInfo::model()->updateAll(array('order_id' => $model->id), "session_id='" . $cart_id . "' and order_id=0");
//Yii::app()->request->cookies->clear();
// Order Info
                $modelMember = Member::model()->findByPk($lg->id);

// Send email -------------------------
                if (Yii::allow_send_email()) {
// Config value for send email
                    $senderEmail = "";
                    $senderName = "";
                    $modelConfig = new Config();
                    $dataConfig = $modelConfig->searchByCode('SENDER_EMAIL')->data;
                    if (count($dataConfig) > 0)
                        $senderEmail = $dataConfig[0]["cfg_value"];
                    $dataConfig = $modelConfig->searchByCode('SENDER_NAME')->data;
                    if (count($dataConfig) > 0)
                        $senderName = $dataConfig[0]["cfg_value"];

                    $body = "==============<br/>";
                    $body .= "New Order<br/>";
                    $body .= "==============<br/>";
                    $body .= "<br/>";
                    $body .= "คุณ " . $modelMember->first_name . " " . $modelMember->last_name . "<br/>";
                    $body .= "ได้สั่งซื้อสินค้า รหัสใบสั่งซื้อ : " . substr("000000000" . $model->id, -9) . "<br/>";
                    $body .= "สามารถเข้าดูรายละเอียดการสั่งซื้อได้โดย <a href='http://biffoodservice.com/index.php/admin/'>คลิกที่นี่</a>";
                    $body .= "<br/><br/><br/>";
                    $body .= "Regards,<br/>";
                    $body .= "Biffoodservice.com";

                    $subject = Yii::prefixMailSubject() . "แจ้งเตือนสั่งซื้อสินค้า";

                    $mail_to = $senderEmail;
                    $mail_subject = '=?utf-8?B?' . base64_encode($subject) . '?='; // email subject line
                    $mail_body = $body;

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
//$headers .= 'To: ' . $mail_to . "\r\n";
                    $headers .= 'From:  ' . $senderName . ' <' . $senderEmail . '>' . "\r\n";

                    mail($mail_to, $mail_subject, $mail_body, $headers);
                }

                $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/cartthankyou/" . $model->id);
//User::model()->updateAll(array('last_login'=>date("Y-m-d H:i:s")),"username='" . Yii::app()->user->getId() ."'");
            } else {
                print_r($model->getErrors());
                echo "not save";
            }
        }
        $this->render('confirmorder', array('model' => $model));
    }

    public function actionCartThankYou() {
        $this->layout = "main2column";
        $this->render('cartthankyou');
    }

    public function actionService() {
        $model = new Service();
        $model->setscenario('new');
        if (isset($_POST['Service'])) {
            $model->attributes = $_POST['Service'];
            $ref = "";
            if (isset($_GET['course_id'])) {
                $model->attr1 = $_GET['course_id'];
                $ref = "http://www.kpt-group.com/index.php/site/trainingdetail/" . $_GET['course_id'];
                $ref = "<a href='$ref' target='_blank'>$ref</a>";
            }
            if ($model->validate() && $model->save()) {
                $mail_setting = Setting::model()->findByPk(1);
//$mail_setting->email = "pinyo_p@windowslive.com";
                $message = new YiiMailMessage;
//	$body = $mail_setting->mail_body;
                $body = "";
                $body .= "<br /><hr /><br />";
                $body .= "คุณ. " . $model->full_name . " <br />";
                $body .= "บริษัท. " . $model->company_name . " <br />";
                $body .= "email. " . $model->email . " <br />";
                $body .= "tel. " . $model->phone_no . " <br />";
                $body .= "product code. " . $model->product_code . " <br />";
                $body .= "-----------------------------<br />Detail<br />-------------------------<br />";
                $body .= "" . $model->detail . " <br />";
                $body .= "ทางเราได้รับข้อความแล้ว จะดำเนินการติดต่อกลับโดยเร็ว <br />";
                $body .= $ref;
                $subject = " Support [" . $model->subject . "]";
                $email = $model->email;
                $message->message->setBody($body, 'text/html', 'utf-8');
                $message->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');
                $message->addTo($email);
                if ($model->service_type == "2")
                    $message->addCC($mail_setting->repair_email);
                else
                    $message->addCC($mail_setting->support_email);




//$message->from   = ($mail_setting->sender);
                $message->from = $mail_setting->support_email;
                if (Yii::allow_send_email())
                    Yii::app()->mail->send($message);
                $this->render('servicethank');
                Yii::app()->end();
            }
        }
        if (isset($_GET['id']) && $_GET['id'] != "") {
            $model->service_type = $_GET['id'];
        } else {
            $model->service_type = 1;
        }
        $this->saveStat(0, 6);
        $this->render('service', array('model' => $model));
    }

    public function actionCourse() {
        $model = new Course();
        $param['date'] = array();
        if (isset($_POST['Course'])) {
            $model->attributes = $_POST['Course'];
            $ref = "";
            $train = "";


            if (isset($_GET['id'])) {
                $model->training_id = $_GET['id'];
                $training = Training::model()->findByPk($model->training_id);
                $train = $training->subject_th;
                $ref = "http://www.kpt-group.com/index.php/site/trainingdetail/" . $_GET['id'];
                $ref = "<a href='$ref' target='_blank'>$ref</a>";
            }
            if ($model->validate() && $model->save()) {

                $j_fullname = $_POST['name'];
                $j_position = $_POST['position'];
                $j_email = $_POST['email'];
                $j_phone_no = $_POST['phoneno'];
                $joiner = "";
                $no = 1;
                for ($i = 0; $i < count($j_fullname); $i++) {
                    if (trim($j_fullname[$i]) != "") {
                        $j = new Joinner();
                        $j->fullname = $j_fullname[$i];
                        $j->position = $j_position[$i];
                        $j->email = $j_email[$i];
                        $j->phoneno = $j_phone_no[$i];
                        $j->course_id = $model->id;
                        $j->training_id = $model->training_id;
                        $j->save();

                        $joiner .= "<table><tr><td>ชื่อ/สกุล : " . $j->fullname . " </td><td>ตำแหน่ง : " . $j->position . "</td><tr><td>email : " . $j->email . "</td><td>เบอร์โทรศัพท์ : " . $j->phoneno . "</td></tr></table>";

                        $no++;
                    }
                }
                $mail_setting = Setting::model()->findByPk(1);
//$mail_setting->email = "pinyo_p@windowslive.com";
                $message = new YiiMailMessage;
//	$body = $mail_setting->mail_body;
                $body = "";
                $body .= "<br /><hr /><br />";
                $body .= "คุณ. " . $model->fullname . " <br />";
                $body .= "บริษัท. " . $model->company_name . " <br />";
                $body .= "ประเภทบริษัท. " . $model->company_type . " <br />";
                $body .= "email. " . $model->email . " <br />";
                $body .= "tel. " . $model->tel . " <br />";
                $body .= "โทรศัพท์มือถือ. " . $model->mobile . " <br />";
                $body .= "โทรสาร. " . $model->fax . " <br />";
                $body .= "ชื่อพนักงานขาย. " . $model->sale_name . " <br />";
                $body .= "บริษัท. " . $model->sale_company . " <br />";
                $body .= "หลักสูตร " . $train . " <br />";
                $body .= "วันที่" . $model->training_date . " <br />";
                $body .= "จำนวนคนที่เข้าอบรม " . $model->training_person . " <br />";
                if (trim($joiner) != "") {
                    $body .= " รายชื่อผู้เข้าร่วมประชุม  <br /> $joiner ";
                }
//	$body .= "product code. " . $model->training_id . " <br />" ;
                $body .= "ทางเราได้รับข้อความแล้ว จะดำเนินการติดต่อกลับโดยเร็ว <br />";
                $body .= $ref;
                $subject = Yii::prefixMailSubject() . "ขอบคุณที่สนใจลงทะเบียนอบรมกับเรา";
                $email = $model->email;
                $message->message->setBody($body, 'text/html', 'utf-8');
                $message->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');
//$message->subject = $subject;
//$message->setSubject($subject);
                $message->addTo($email);
                $message->addCC($mail_setting->training_email);
//$message->from   = ($mail_setting->sender);
                $message->from = $mail_setting->training_email;
                if (Yii::allow_send_email())
                    Yii::app()->mail->send($message);
                $this->render('servicethank');
                Yii::app()->end();
            }
        }
        if (isset($_GET['id']) && $_GET['id'] != "") {
            $model->training_id = $_GET['id'];
            $arr = array();
            $train = Training::model()->findByPk($model->training_id);
            $d[] = $this->showDate($train->start_date, $train->end_date);
            $d[] = $this->showDate($train->start_date2, $train->end_date2);
            $d[] = $this->showDate($train->start_date3, $train->end_date3);
            $d[] = $this->showDate($train->start_date4, $train->end_date4);
            $d[] = $this->showDate($train->start_date5, $train->end_date5);
            $d[] = $this->showDate($train->start_date6, $train->end_date6);
            $d[] = $this->showDate($train->start_date7, $train->end_date7);
            $d[] = $this->showDate($train->start_date8, $train->end_date8);
            $d[] = $this->showDate($train->start_date9, $train->end_date9);
            $d[] = $this->showDate($train->start_date10, $train->end_date10);
            $d[] = $this->showDate($train->start_date11, $train->end_date11);
            $d[] = $this->showDate($train->start_date12, $train->end_date12);
            foreach ($d as $r) {
                if ($r != "")
                    $arr[$r] = $r;
            }
            $param['date'] = $arr;
        }

        $this->saveStat(0, 6);
        $this->render('course', array('model' => $model, 'param' => $param));
    }

    function showDate($start_date, $end_date) {
        $s = "";
        if ($start_date != "" && $end_date != "") {
            $s = "$start_date - $end_date";
        }
        return $s;
    }

    public function actionJobsList() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $model = new Joblist();
        $data = $model->front_search()->data;
        $this->saveStat(0, 7);
        $this->render('jobslist', array('model' => $model, 'data' => $data));
    }

    public function actionDownload() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $session = new CHttpSession;
        $session->open();
        $need_login = false;
        if (isset($session['user']) || !$need_login) {

            $model = new Download();
            $model->display_perpage = 8;
            $data = $model->search_by_type_id(1)->data;
            $data2 = $model->search_by_type_id(2)->data;
            $this->saveStat(0, 8);
            $this->render('download', array('model' => $model, 'data' => $data, 'data2' => $data2));
        } else {
            $model = new Member();
            if (isset($_POST['Member'])) {
                $model->attributes = $_POST['Member'];
// validate user input and redirect to the previous page if valid

                $lg = $model->login();
                if (!$lg) {
// echo "Login Fail!";
                } else {
                    $session['user'] = $lg;
                    $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/Download");
                }
            }
            $this->render('memberlogin', array('model' => $model));
        }
    }

    public function actionCatalog() {
        $model = new Download();
        $model->type_id = 1;
        /*
          if(isset($_POST['act']) && $_POST['act']=="search"){
          if(isset($_POST['Download']))
          {
          $model->attributes=$_POST['Download'];
          $model->filename = $_POST['Download']['filename'];
          }
          } */
        $model->display_perpage = "40";
//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;

        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->backend_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;
        $this->saveStat(0, 9);
        $this->render('cataloglist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionManual() {
        $model = new Download();
        $model->type_id = 2;
        /*
          if(isset($_POST['act']) && $_POST['act']=="search"){
          if(isset($_POST['Download']))
          {
          $model->attributes=$_POST['Download'];
          $model->filename = $_POST['Download']['filename'];
          }
          } */
        $model->display_perpage = "40";
//$model->display_perpage = ($model->display_perpage!="" && $model->display_perpage!="0"?$model->display_perpage:1);
        $param['display_perpage'] = $model->display_perpage;

        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;

        if (isset($_REQUEST['perpage']) && $_REQUEST['perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['perpage'];
        }
        if (isset($_REQUEST['display_perpage']) && $_REQUEST['display_perpage'] != "") {
            $param['display_perpage'] = $_REQUEST['display_perpage'];
        }


        $model->current_page = $current_page;
        $data = $model->backend_search()->data;
        $page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($page < 0)
            $page = 0;
        $param['row_amount'] = $model->row_count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($page >= $param['max_page'])
            $page = $param['max_page'] - 1;
        $param['page'] = $current_page;
        $this->saveStat(0, 10);
        $this->render('manuallist', array('model' => $model, 'data' => $data, 'param' => $param));
    }

    public function actionDownloadIt() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $id = (isset($_GET['id']) ? $_GET['id'] : 0);
        $data = Download::model()->findByPk($id);
        $count = $data->download_count;
        $count++;
        Download::model()->updateByPk($id, array('download_count' => $count));
        $this->saveStat($id, 8);
        $this->redirect(Yii::app()->request->baseUrl . '/download/file_' . $data->filesrc);
    }

    public function actionJobsDetail() {
        $id = (isset($_GET['id']) ? $_GET['id'] : 0);
        $model = Joblist::model()->findByPk($id);
        $this->saveStat(0, 7);
        $this->render('jobsdetail', array('model' => $model));
    }

    public function actionJobApply() {

        $id = $_REQUEST['id'];
        $model = new Jobbee();
        $mail_setting = Setting::model()->findByPk(1);
        if (isset($_POST['Jobbee'])) {
            $model->attributes = $_POST['Jobbee'];
            if (isset($_FILES['resume']))
                $model->resume = $_FILES['resume']['name'];

// validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->save()) {

                $id = $model->id;
                $dir = Yii::app()->basePath . '/../attach/jobs/';
                if (isset($_FILES['resume']) && $_FILES['resume']['name'] != "") {
                    $file_resume = $id . '_resume_' . $_FILES['resume']['name'];
                    $model->resume = $file_resume;
                    copy($_FILES['resume']['tmp_name'], $dir . $file_resume);
                }
                if (isset($_FILES['picture']) && $_FILES['picture']['name'] != "") {
                    $file_picture = $id . '_picture_' . $_FILES['picture']['name'];
                    $model->apply_image = $file_picture;
                    copy($_FILES['picture']['tmp_name'], $dir . $file_picture);
                }



//$mail_setting->email = "pinyo_p@windowslive.com";
                $message = new YiiMailMessage;
//	$body = $mail_setting->mail_body;
                $body = "";
                $body .= "<br /><hr /><br />";
                $body .= "คุณ. " . $model->fullname . "<br />";
                $body .= "เบอร์โทร " . $model->phoneno . "<br />";
                $body .= "Email " . $model->email . "<br />";
                $subject = Yii::prefixMailSubject() . "ขอบคุณที่ร่วมงานกับเรา";
                $email = $_POST['Jobbee']['email'];
                $message->message->setBody($body, 'text/html', 'utf-8');
                $message->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');
//$message->subject = $subject;
//$message->setSubject($subject);
                $message->addTo($email);
                $message->addCC($mail_setting->hr_email);
//$message->from   = ($mail_setting->sender);
                $message->from = $mail_setting->hr_email;

                if (Yii::allow_send_email())
                    Yii::app()->mail->send($message);
                $model->save();
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/jobthankyou/');
            } else {
                
            }
        }
        $model->jobid = $id;
        /* $tracking = new Tracking();
          $tracking->ref_id=$id;
          $tracking->part='COP';
          $tracking->group_id=1;
          $tracking->type=3;
          $tracking->parent_id=0;
          $tracking->save(); */

        $this->render('jobapply', array('model' => $model, 'setting' => $mail_setting));
    }

    public function actionJobThankYou() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $this->render('jobsthank');
    }

    public function actionMemberRegister() {
        $this->layout = "main2column";

        $model = new Member();
        $model->scenario = "register";
        $mail_setting = Setting::model()->findByPk(1);

        if (isset($_POST['Member'])) {
            $mail_setting = Setting::model()->findByPk(1);
            $model->attributes = $_POST['Member'];
            $model->cpassword = $_POST['Member']['cpassword'];
            $model->member_type = $_POST['Member']['member_type'];

            if ($model->validate() && $model->save()) {
                $message = new YiiMailMessage;

                $body = "";
                $body .= "<br /><hr /><br />";
                $body .= "คุณ. " . $model->first_name . ' ' . $model->last_name . "<br />";
                $body .= "Email " . $model->email . "<br />";
                $body .= "รหัสผ่านของคุณคือ " . $model->password . "<br />";

                $subject = Yii::prefixMailSubject() . "Register";
                $message->message->setBody($body, 'text/html', 'utf-8');
                $message->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');

                $message->addTo($model->email);
//$message->addCC($mail_setting->email);
                $message->from = $mail_setting->email;

                if (Yii::allow_send_email())
                    Yii::app()->mail->send($message);

                $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/memberthankyou/');
            }
        }
        $this->render('register', array('model' => $model));
    }

    public function actionMemberThankYou() {
        $this->layout = "main2column";
        $this->render('memberthankyou');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Display content
     */
    public function actioncontent() {
        $this->layout = "main2column";

        $code = (isset($_REQUEST['code']) ? $_REQUEST['code'] : "");
        $code = str_replace("_", " ", $code);
        $id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : "0");

        $modelContent = new Content();

// Search content by content_code for display in left site menu
        $modelContent->content_code = $code;
        $data["contents"] = $modelContent->searchByCodeForIndex()->data;

// Get default content (first access)
        if ($id == 0) {
            if (count($data["contents"]) > 0)
                $id = $data["contents"][0]->id;
        }

// Get single content
        $modelContent->id = $id;
        $data["content"] = $modelContent->search()->data;

        $data["content_group_title"] = $code;

// Render to view
        $this->render('content', array('data' => $data));
    }

    /**
     * Insert contact
     */
    public function actionContactNew() {
        $this->layout = false;

        $topic = (isset($_REQUEST['topic']) ? $_REQUEST['topic'] : "");
        $fullName = (isset($_REQUEST['fullName']) ? $_REQUEST['fullName'] : "");
        $email = (isset($_REQUEST['email']) ? $_REQUEST['email'] : "");
        $mobile = (isset($_REQUEST['mobile']) ? $_REQUEST['mobile'] : "");
        $tel = (isset($_REQUEST['tel']) ? $_REQUEST['tel'] : "");
        $subject = (isset($_REQUEST['subject']) ? $_REQUEST['subject'] : "");
        $detail = (isset($_REQUEST['detail']) ? $_REQUEST['detail'] : "");
        $issubscribe = (isset($_REQUEST['issubscribe']) ? $_REQUEST['issubscribe'] : "");
        $issubscribe = (($issubscribe == 'true') ? "1" : "0");

        $model = new QuickContact();
        $model->topic = $topic;
        $model->fullname = $fullName;
        $model->email = $email;
        $model->mobile = $mobile;
        $model->tel = $tel;
        $model->subject = $subject;
        $model->detail = $detail;
        $model->is_subscribe = $issubscribe;
        $model->create_date = date("Y-m-d H:i:s");
        $model->create_ip = Yii::app()->request->getUserHostAddress();
        ;

        $model->company_name = " ";
        $model->position = " ";
        $model->fax = " ";
        $model->website = " ";
        $model->address = " ";
        $model->website = " ";
        $model->update_date = " ";
        $model->update_ip = " ";
        $model->status = "0";
        $model->type = "1";
        $model->ref_id = "0";

        $model->save();

        $data["result"] = "success";
        echo json_encode($data);
    }

    public function actionSubscribeNew() {
        $this->layout = false;

        $email = (isset($_REQUEST['email']) ? $_REQUEST['email'] : "");

        $model = new Subscriber();
        $model->email = $email;
        $model->save();

        $data["result"] = "success";
        echo json_encode($data);
    }

    /**
     * Displays the contact page
     */
    public function actionContent_bak150210() {
        $id = (isset($_GET['id']) ? $_GET['id'] : 0);

        $model = Content::model()->find('id=:id', array(':id' => $id));
        $count = $model->view_count;
        $count++;
        Content::model()->updateByPk($id, array('view_count' => $count));
        $this->saveStat($id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionAboutUs() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'History'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionPolicy() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'Policy'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionCredit() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'credit'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionReward() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'reward'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionQuotationForm() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'quotationform'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionPromotion() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'promotion'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionSalePolicy() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'SalePolicy'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionProtect() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'Protection'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionSiteMap() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'Sitemap'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionMemberPolicy() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'MemberPolicy'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionRepair() {
        /*
          $model = Content::model()->find('content_code=:content_code',array(':content_code'=>'repair'));
          $this->saveStat($model->id,11);
          $this->render('content',array('model'=>$model));
         */
        $content = Content::model()->find('content_code=:content_code', array(':content_code' => 'repair'));
        $model = new Service();
        $model->service_type = 2;
        $model->setscenario('new');
        if (isset($_POST['Service'])) {
            $model->attributes = $_POST['Service'];
            $ref = "";

            if (isset($_GET['course_id'])) {
                $model->attr1 = $_GET['course_id'];
                $ref = "http://www.kpt-group.com/index.php/site/trainingdetail/" . $_GET['course_id'];
                $ref = "<a href='$ref' target='_blank'>$ref</a>";
            }
            if ($model->validate() && $model->save()) {


                $fix_product = $_POST['fix_product'];
                $fix_model = $_POST['fix_model'];
                $fix_serial = $_POST['fix_serial'];
                $fix_detail = $_POST['fix_detail'];
                $fix_amount = $_POST['fix_amount'];
                $fix_file = $_FILES['fix_file'];
                $data = "<table>\n";
                $data .= ' <tr>
                          <td width="250" align="center" valign="top" bgcolor="#f8f8f8" class="txt_bold" colspan="7">รายการซ่อม </td>
                        
                      </tr><tr><th  style="text-align:center">No.</th><th style="text-align:center">ภาพประกอบ</th><th style="text-align:center">Product</th><th style="text-align:center">Model</th><th style="text-align:center">S/N</th><th style="text-align:center">อาการเสีย</th><th style="text-align:center">จำนวน</th>
                          
                          </tr>';
                $dir = Yii::app()->basePath . '/../images/repair/' . $model->id;
                mkdir($dir);
                $i = 1;
                foreach ($fix_product as $key => $product) {
                    /* 	echo $key;
                      echo $product; */

                    if (trim($product) != "") {

                        $img = "";
                        if (isset($fix_file['tmp_name'][$key]) && $fix_file['name'][$key] != "") {
                            copy($fix_file['tmp_name'][$key], $dir . "/" . $fix_file['name'][$key]);
                            $img = "<a href='" . Yii::app()->request->baseUrl . "/images/repair/" . $model->id . "/" . $fix_file['name'][$key] . "' target='_blank'><img src='" . Yii::app()->request->baseUrl . "/images/repair/" . $model->id . "/" . $fix_file['name'][$key] . "' height='50' /></a>";
                        }
                        $data .= "<tr>\n";
                        $data .= "<td>$i</td>";
                        $data .= "<td>$img</td>\n";
                        $data .= "<td>" . $product . "</td>\n";
                        $data .= "<td>" . $fix_model[$key] . "</td>\n";
                        $data .= "<td>" . $fix_serial[$key] . "</td>\n";
                        $data .= "<td>" . $fix_detail[$key] . "</td>\n";
                        $data .= "<td>" . $fix_amount[$key] . "</td>\n";

                        $data .= "</tr>\n";
                        $i++;
                    }
                }
                $data .= "</table>";

                $model->detail .= "<hr />" . $data;
                $model->scenario = 'backend_update';
                $model->save();
                $mail_setting = Setting::model()->findByPk(1);
//$mail_setting->email = "pinyo_p@windowslive.com";
                $message = new YiiMailMessage;
//	$body = $mail_setting->mail_body;
                $body = "";
                $body .= "<br /><hr /><br />";
                $body .= "คุณ. " . $model->full_name . " <br />";
                $body .= "บริษัท. " . $model->company_name . " <br />";
                $body .= "email. " . $model->email . " <br />";
                $body .= "tel. " . $model->phone_no . " <br />";
                $body .= "product code. " . $model->product_code . " <br />";
                $body .= "-----------------------------<br />Detail<br />-------------------------<br />";
                $body .= "" . $model->detail . " <br />";
                $body .= "ทางเราได้รับข้อความแล้ว จะดำเนินการติดต่อกลับโดยเร็ว <br />";
                $body .= $ref;
                $subject = " Support [" . $model->subject . "]";
                $email = $model->email;
                $message->message->setBody($body, 'text/html', 'utf-8');
                $message->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');
                $message->addTo($email);
                if ($model->service_type == "2")
                    $message->addCC($mail_setting->repair_email);
                else
                    $message->addCC($mail_setting->support_email);




//$message->from   = ($mail_setting->sender);
                if ($model->service_type == "2")
                    $message->from = $mail_setting->repair_email;
                else
                    $message->from = $mail_setting->support_email;
                if (Yii::allow_send_email())
                    Yii::app()->mail->send($message);
                $this->render('servicethank');
                Yii::app()->end();
            }
        }
        if (isset($_GET['id']) && $_GET['id'] != "") {
            $model->service_type = $_GET['id'];
        } else {
            $model->service_type = 2;
        }
        $this->saveStat(0, 6);
        $this->render('repair', array('model' => $model, 'content' => $content));
    }

    public function actionTraining() {
        $model = new Training();
        $data = $model->front_show()->data;
        $t1 = Content::model()->find('content_code=:content_code', array(':content_code' => 'training'));
        $t2 = Content::model()->find('content_code=:content_code', array(':content_code' => 'training2'));
        $this->render('traininglist', array('model' => $model, 'data' => $data, 't1' => $t1, 't2' => $t2));
        /*
          $model = Content::model()->find('content_code=:content_code',array(':content_code'=>'training'));
          $this->saveStat($model->id,11);
          $this->render('content',array('model'=>$model));
         */
    }

    public function actionTrainingDetail() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        if (isset($_REQUEST['id']))
            $id = $_REQUEST['id'];
        else
            $id = 0;
        $model = Training::model()->find('id=:id', array(':id' => $id));
        $this->saveStat($id, 6);
        $this->render('trainingdetail', array('model' => $model));
    }

    public function actionFaq() {
        $model = Content::model()->find('content_code=:content_code', array(':content_code' => 'faq'));
        $this->saveStat($model->id, 11);
        $this->render('content', array('model' => $model));
    }

    public function actionBranch() {
        $model = new Branch();
        $data = $model->search()->data;
        $this->saveStat(0, 12);
        $this->render('branchlist', array('model' => $model, 'data' => $data));
    }

    public function actionPartner() {
        $model = new Partner();
        $data = $model->search()->data;
        $this->saveStat(0, 13);
        $this->render('partnerlist', array('model' => $model, 'data' => $data));
    }

    public function actionContact() {
        $this->layout = "main2column";

        $model = new Contact();
        if (isset($_POST['Contact'])) {
            $model->attributes = $_POST['Contact'];
            $model->type = 0;
            if (isset($_REQUEST['product_id']) && $_REQUEST['product_id'] != "") {
                $model->type = 1;
                $model->ref_id = $_REQUEST['product_id'];
            }
            if ($model->validate() && $model->save()) {

                $mail_setting = Setting::model()->findByPk(1);
                $message = new YiiMailMessage;
//	$body = $mail_setting->mail_body;
                $model->detail = str_replace("\n", "<br />", $model->detail);
                $body = "";
                $body .= "<br /><hr /><br />";
                $body .= "คุณ. " . $model->fullname . "<br />";
                $body .= "บริษัท. " . $model->company_name . "<br />";
                $body .= "ตำแหน่งงาน. " . $model->position . "<br />";
                $body .= "เบอร์โทร " . $model->tel . "<br />";
                $body .= "เบอร์โทรสาร " . $model->fax . "<br />";
                $body .= "Email " . $model->email . "<br />";
                $body .= "เวบไซต์ " . $model->website . "<br />";
                $body .= "------------------------------------<br />";
                $body .= " " . $model->detail . "<br />";
                $body .= "------------------------------------<br />";
                $subject = Yii::prefixMailSubject() . "ขอบคุณที่ส่งข้อความถึงเรา";
                $email = $_POST['Contact']['email'];
                $message->message->setBody($body, 'text/html', 'utf-8');
                $message->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');
//$message->subject = $subject;
//$message->setSubject($subject);
                $message->addTo($email);
                $message->addCC($mail_setting->email);

//$message->from   = ($mail_setting->sender);
                $message->from = $mail_setting->email;
//$message->from   ="pinyo@chaiyohosting.com";
//Yii::app()->mail->send($message);
//$this->redirect(Yii::app()->request->baseUrl . "/index.php/site/Contact");
                Yii::app()->clientScript->registerScript('finish', 'alert("บันทึกคำขอใบเสนอราคาเสร็จสมบูรณ์");location.href="' . Yii::app()->request->baseUrl . "/index.php/site/Contact" . '"');
            }
        }
        $contact = Content::model()->find('content_code=:content_code', array(':content_code' => 'ContactUs'));
        $this->saveStat($contact->id, 11);

        if (isset($_REQUEST['product_id']) && $_REQUEST['product_id'] != "") {
            $product_link = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl . "/index.php/site/productdetail/" . $_REQUEST['product_id'];
            $detail = "Request Quotation for product #" . $_REQUEST['product_id'] . "\n -----------------------------\n";
            $detail .= "$product_link\n-------------------------------------\n";
            $model->detail = $detail;
        }

        $this->render('contact', array('model' => $model, 'contact' => $contact));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

// if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

// collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
// validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
// display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionVote() {
        $result = array();
        $session = new CHttpSession;
        $session->open();
        $product_id = $_REQUEST['product_id'];
        $point = $_REQUEST['point'];

        $product = Product::model()->findByPk($product_id);
        $member_id = 0;
        $vote = new Vote();
        $vote->product_id = $product_id;
        $vote->member_id = 0;
        $vote->vote_point = $point;
        $vote->save();
        $product_point = $product->point;
        $product_vote_count = $product->vote_count;
        $product_point += $point;
        $product_vote_count++;
        Product::model()->updateByPk($product_id, array("point" => $product_point, "vote_count" => $product_vote_count));
        $result['result'] = "success";
        $result['count'] = $product_vote_count;
        $result['point'] = $product_point;
        /*
          if(!isset($session['user'])){
          $result['result'] = "fail";
          $result['message']= "need login";
          }else{
          $user = $session['user'];
          $product_id = $_REQUEST['product_id'];
          $point = $_REQUEST['point'];
          $product = Product::model()->findByPk($product_id);
          $member_id=$user['id'];
          $v = Vote::model()->count("member_id=:member_id and product_id=:product_id",array(":member_id"=>$member_id,":product_id"=>$product_id));
          if($v>0){
          $result['result'] = "fail";
          $result['message']= "already vote";
          }else{
          $vote = new Vote();
          $vote->product_id = $product_id;
          $vote->member_id = $member_id;
          $vote->vote_point = $point;
          $vote->save();
          $product_point = $product->point;
          $product_vote_count = $product->vote_count;
          $product_point += $point;
          $product_vote_count++;
          Product::model()->updateByPk($product_id,array("point"=>$product_point,"vote_count"=>$product_vote_count));
          $result['result'] = "success";
          $result['count']= $product_vote_count;
          $result['point']= $product_point;
          }
          }
         */
        echo json_encode($result);
    }

    public function actionMemberLogin() {
        $this->layout = "main2column";

        $loginInfo = "";

        $model = new Member();
        if (isset($_POST['Member'])) {
            $model->attributes = $_POST['Member'];

            $lg = $model->login();
            if (!$lg) {
                $loginInfo = "ไม่สามารถเข้าระบบได้ เนื่องจากไม่มีบัญชีผู้ใช้ตามที่ระบุ !!!";
// echo "Login Fail!";
            } else {
                $session = new CHttpSession;
                $session->open();
                $session['user'] = $lg;
                $_SESSION['user'] = $lg;
                $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/");
            }
        }
        $this->render('memberlogin', array('model' => $model, 'loginInfo' => $loginInfo));
    }

    public function actionAjaxLogin() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $model = new Member();
        $model->email = $_REQUEST['username'];
        $model->password = $_REQUEST['password'];
// validate user input and redirect to the previous page if valid
        $result = array();

        $lg = $model->login();
        if (!$lg) {
// echo "Login Fail!";
            $result['result'] = "fail";
        } else {
            $session = new CHttpSession;
            $session->open();


            $session['user'] = $lg;
            $_SESSION['user'] = $lg;
            $result['result'] = "success";
            $result['data'] = $lg;
        }
        echo json_encode($result);
    }

    public function actionHistory() {
        $this->layout = "main2column";

        $session = new CHttpSession;
        $session->open();
        if (isset($session['user'])) {
            $user = $session['user'];
            $model = new MemberOrder();

            $model->member_id = $user->id;
            $data = $model->get_history()->data;

// จำนวนแต้มสะสม
            $modelMember = Member::model()->findByPk($user->id);
            $params["total_score"] = $modelMember->total_score;

            $this->render('history', array('model' => $model, 'data' => $data, 'params' => $params));
        } else {
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/");
        }
    }

    public function actionTrackOrder() {
        $this->layout = "main2column";

        $session = new CHttpSession;
        $session->open();
        if (isset($session['user'])) {
            $user = $session['user'];

// หา order id ล่าสุด
            $modelCart = Order::model()->findAll('member_id=:member_id', array(':member_id' => $user->id), array('order' => 'order_id desc'));
            if (count($modelCart) > 0) {
                $params["last_order"] = $modelCart[count($modelCart) - 1]->order_id;
            } else {
                $params["last_order"] = "0";
            }

// Post
            if (isset($_POST) && count($_POST) > 0) {
                $orderID = $_POST["txtOrderID"];
                $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/historydetail/code/track/id/" . $orderID);
            }

            $this->render('trackorder', array('params' => $params));
        } else {
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/");
        }
    }

    public function actionHistoryDetail() {
        $this->layout = "main2column";

        $session = new CHttpSession;
        $session->open();
        if (isset($session['user'])) {
            $user = $session['user'];
            $model = new Order();

            $model->member_id = $user->id;

            if (isset($_GET["code"])) {
                $model->order_id = $_GET['id'];
                $param["track"] = "1";

                $modelOrder = MemberOrder::model()->find('id=:order_id', array(':order_id' => intval($_GET['id'])));
                $param["order_status"] = $modelOrder->status;
                switch ($modelOrder->status) {
                    case "0":
                        $param["order_status_detail"] = "รอติดต่อกลับจากทางเว็บไซต์";
                        break;
                    case "1":
                        $param["order_status_detail"] = "รอลูกค้าชำระเงิน";
                        break;
                    case "2":
                        $param["order_status_detail"] = "ได้รับแจ้งการชำระเงินแล้ว";
                        break;
                    case "3":
                        $param["order_status_detail"] = "รายการสั่งซื้อถูกยกเลิก";
                        break;
                    case "4":
                        $param["order_status_detail"] = "รอดำเนินการจัดส่งสินค้า";
                        break;
                    case "5":
                        $param["order_status_detail"] = "จัดส่งสินค้าเรียบร้อยแล้ว";
                        break;
                    case "6":
                        $param["order_status_detail"] = "จัดส่งสินค้าไม่สำเร็จ ขออภัยในความไม่สะดวก";
                        break;
                    case "7":
                        $param["order_status_detail"] = "ยกเลิกการจัดส่งสินค้า";
                        break;
                }
            } else {
                $model->order_id = $_REQUEST['id'];
            }
            $data = $model->get_cart_by_order_id()->data;
            $param['count_item'] = count($data);

// Shipping fee
            $param["shipping_fee"] = 0;
            $modelCartInfo = new CartInfo();
            $dataCartInfo = $modelCartInfo->searchByOrderID($model->order_id)->data;
            if (count($dataCartInfo) > 0) {
                $param["shipping_fee"] = $dataCartInfo[0]["shipping_fee"];
            }

// Score
            $param["score"] = 0;
            $param['total_weight'] = 0;
            $param['shiptype'] = 'in_reg';

            $modelCartInfo = new CartInfo();
            $dataCartInfo = $modelCartInfo->searchByOrderID($model->order_id)->data;
            if (count($dataCartInfo) > 0) {
                $param["score"] = $dataCartInfo[0]["score"];
                $param['total_weight'] = floatval($dataCartInfo[0]->total_weight) / 1000;
                $param['shiptype'] = $dataCartInfo[0]->shipping_type;
            }

// Score rate
            $dataScoreRate = Config::model()->searchByCode('SCORE_RATE')->data;
            if (count($dataScoreRate) > 0)
                $param["score_rate"] = $dataScoreRate[0]["cfg_value"];

            // เลขที่พัสดุ
            $param['package_no'] = MemberOrder::model()->findByPk($_GET["id"])->package_no;
            
            $this->render('historydetail', array('model' => $model, 'data' => $data, 'param' => $param));
        } else {
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/");
        }
    }

    public function actionQuotation() {
        $this->render('quotation');
    }

    public function actionQuotation2() {
        include(Yii::app()->basePath . "/../mpdf/mpdf.php");
        $filename = Yii::app()->basePath . "/../quotation.html";

        $quotation_no = "";
        $quotation_date = "";
        $customer_name = "";
        $contact_attn = "";
        $tel = "";
        $fax = "";
        $cc = "";
        $payment = "";
        $delivery = "";
        $vat = "";
        $total = "";
        $grand_total = "";
        $confirm_by = "";
        $confirm_date = "";
        $quote_by = "";
        $sale_name = "";
        $item = "<tr height='300'>
<td class='bor_left '>&nbsp;<br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</td>
<td class='bor_left '></td>
<td class='bor_left '> </td>
<td class='bor_left '> </td>
<td class='bor_left '> </td>
<td class='bor_left  bor_right'> </td>
</tr>";

        $handle = fopen($filename, "rb");
        $contents = fread($handle, filesize($filename));
        fclose($handle);
        $html = $contents;
        $logo = "<img width='117' height='83' src='" . Yii::app()->request->baseUrl . "/admin/images/logo_knp.jpg'>";


        $html = str_replace("<#LOGO#>", $logo, $html);
        $html = str_replace("<#QUOTATION_NO#>", $quotation_no, $html);
        $html = str_replace("<#QUOTATION_DATE#>", $quotation_date, $html);
        $html = str_replace("<#CUSTOMER_NAME#>", $customer_name, $html);
        $html = str_replace("<#CONTACT_ATTN#>", $contact_attn, $html);
        $html = str_replace("<#TEL#>", $tel, $html);
        $html = str_replace("<#FAX#>", $fax, $html);
        $html = str_replace("<#CC#>", $cc, $html);
        $html = str_replace("<#PAYMENT#>", $payment, $html);
        $html = str_replace("<#DELIVERY#>", $delivery, $html);
        $html = str_replace("<#VAT#>", $vat, $html);
        $html = str_replace("<#TOTAL#>", $total, $html);
        $html = str_replace("<#GRAND_TOTAL#>", $grand_total, $html);
        $html = str_replace("<#CONFIRM_BY#>", $confirm_by, $html);
        $html = str_replace("<#CONFIRM_DATE#>", $confirm_date, $html);
        $html = str_replace("<#QUOTED_BY#>", $quoted_by, $html);
        $html = str_replace("<#SALE_NAME#>", $sale_name, $html);
        $html = str_replace("<#ITEM#>", $item, $html);

        $mpdf = new mPDF('UTF-8');

        $mpdf->SetAutoFont();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }

    public function actionQuotation12() {

        $session = new CHttpSession;
        $session->open();
        if (isset($session['user'])) {
            $user = $session['user'];
            $model = new Quotation();
            $model->product_id = $_REQUEST['id'];
            $model->quotation_no = '';
            $model->member_id = $user->id;
            $model->save();
            $id = $model->id;
            $quotation = sprintf("KNP%1$05d", $id);
            $model->quotation_no = $quotation;
            $model->save();
            $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/Quotation13/" . $id);
        }
    }

    public function actionQuotation13() {
        include(Yii::app()->basePath . "/../mpdf/mpdf.php");
        $filename = Yii::app()->basePath . "/../quotation.html";
        $q_id = $_GET['id'];
        $quotation = Quotation::model()->find("id=:id", array("id" => $q_id));
        $member_id = $quotation->member_id;
        $product_id = $quotation->product_id;
        $user = Member::model()->findByPk($member_id);
        $product = Product::model()->findByPk($product_id);
        $quotation_no = $quotation->quotation_no;
        $quotation_date = date("d/m/Y");
        $customer_name = $user->first_name . ' ' . $user->last_name;
        $contact_attn = "";
        $tel = $user->phone_no;
        $fax = "";
        $cc = "";
        $payment = "";
        $delivery = "";
        $vat = "";
        $total = "";
        $grand_total = "";
        $confirm_by = "";
        $confirm_date = "";
        $quote_by = "";
        $item = "";
        $sale_name = $product->sale_name;
        $i = 0;
        $i++;
        $item .= "<tr height='300'>
<td class='bor_left txt_cen'>$i

</td>
<td class='bor_left '>" . $product->product_name_th . "</td>
<td class='bor_left txt_cen'>1</td>
<td class='bor_left txt_right'>" . number_format($product->price1, 0, ".", ",") . "</td>
<td class='bor_left '> </td>
<td class='bor_left txt_right bor_right'>" . number_format($product->price1, 0, ".", ",") . "</td>
</tr>";

        for ($ii = $i; $ii <= 20; $ii++) {
            $item .= "<tr height='300'>
<td class='bor_left '><br />

</td>
<td class='bor_left '></td>
<td class='bor_left '> </td>
<td class='bor_left '> </td>
<td class='bor_left '> </td>
<td class='bor_left  bor_right'> </td>
</tr>";
        }
        $total = $product->price1;
        $vat = (($total * 7) / 100);
        $grand_total = $total + $vat;
//<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />		
        $handle = fopen($filename, "rb");
        $contents = fread($handle, filesize($filename));
        fclose($handle);
        $html = $contents;
        $logo = "<img width='117' height='83' src='" . Yii::app()->request->baseUrl . "/admin/images/logo_knp.jpg'>";


        $html = str_replace("<#LOGO#>", $logo, $html);
        $html = str_replace("<#QUOTATION_NO#>", $quotation_no, $html);
        $html = str_replace("<#QUOTATION_DATE#>", $quotation_date, $html);
        $html = str_replace("<#CUSTOMER_NAME#>", $customer_name, $html);
        $html = str_replace("<#CONTACT_ATTN#>", $contact_attn, $html);
        $html = str_replace("<#TEL#>", $tel, $html);
        $html = str_replace("<#FAX#>", $fax, $html);
        $html = str_replace("<#CC#>", $cc, $html);
        $html = str_replace("<#PAYMENT#>", $payment, $html);
        $html = str_replace("<#DELIVERY#>", $delivery, $html);
        $html = str_replace("<#VAT#>", $vat, $html);
        $html = str_replace("<#TOTAL#>", number_format($total, 0, ".", ","), $html);
        $html = str_replace("<#GRAND_TOTAL#>", number_format($grand_total, 2, ".", ","), $html);
        $html = str_replace("<#CONFIRM_BY#>", $confirm_by, $html);
        $html = str_replace("<#CONFIRM_DATE#>", $confirm_date, $html);
        $html = str_replace("<#QUOTED_BY#>", $quoted_by, $html);
        $html = str_replace("<#SALE_NAME#>", $sale_name, $html);
        $html = str_replace("<#ITEM#>", $item, $html);

        $mpdf = new mPDF('UTF-8');

        $mpdf->SetAutoFont();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }

    public function actionQuotation1() {
        $model = new Member();
        if (isset($_POST['Member'])) {
            $model->attributes = $_POST['Member'];
// validate user input and redirect to the previous page if valid

            $lg = $model->login();
            if (!$lg) {
// echo "Login Fail!";
            } else {
                $session = new CHttpSession;
                $session->open();
                $session['user'] = $lg;
                $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/quotation12/" . $_REQUEST['id']);
            }
        }
        $this->render('quotation1', array('model' => $model));
    }

    public function actionTest() {
        
    }

    public function actionShowSearch() {
        $model = new Content();
        $models = $model->search()->data;
//$criteria = new CDbCriteria;
        $key = $_REQUEST['keysearch'];


//$criteria->condition = " (title_th like '%$key%' or content_th like '%$key%')";
        $sql = "select title_th as title,content_th as content,id,attr1 as image,update_date,'content' as part
from tb_content
where (title_th like '%$key%' or content_th like '%$key%')  and tb_content.content_code='News'
union
SELECT concat(product_group.group_th,' [',t.product_code,']') as title,
t.product_desc_th as content,t.id,t.pic1 as image,t.update_date as update_date ,'product' as part
FROM tb_product t
left join tb_product_group product_group on product_group.id = t.group_id
where product_group.group_th like '%$key%' or product_code like '%$key%' or product_desc_th like '%$key%'
				";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();   // a non-query SQL statement execution
// or execute an SQL query and fetch the result set
        $data = $command->query();
        $count = count($data);

        $page_size = 20;

        $param['display_perpage'] = $page_size;
        $current_page = (isset($_REQUEST['page']) && $_REQUEST['page'] != "" ? ($_REQUEST['page'] - 1) : 0);
        if ($current_page < 0)
            $current_page = 0;
// ($model->row_count==0?1:$model->row_count);

        $param['row_amount'] = $count;
        $param['max_page'] = ceil($param['row_amount'] / $param['display_perpage']);
        if ($current_page >= $param['max_page'])
            $current_page = $param['max_page'] - 1;

        $param['page'] = $current_page;


// $count = AboutSam::model()->count($criteria);
        /* $pages = new CPagination($count);
          $pages->pageSize = 10;
          $pages->applyLimit($criteria);
          $criteria->order = " id desc "; */
        $start = ($current_page) * $page_size;
        if ($start <= 0)
            $start = 0;
        $sql .= " limit $start,$page_size";
//echo $sql;
        $models = Yii::app()->db->createCommand($sql)->queryAll(); // AboutSam::model()->findAll($criteria);
        $id = 0;
        Yii::app()->db->active = false;
        $this->render('showsearch', array('data' => $models,
            'models' => $models,
            'param' => $param,
            'group' => $id));
    }

    public function actionProductDownload() {
// renders the view file 'protected/views/site/index.php'
// using the default layout 'protected/views/layouts/main.php'
        $session = new CHttpSession;
        $session->open();
        if (isset($_REQUEST['id']))
            $id = $_REQUEST['id'];
        else
            $id = 0;
        $product = Product::model()->find('id=:id', array(':id' => $id));
        $need_login = false;
        if (isset($session['user']) || !$need_login) {
            $file = Yii::app()->request->baseUrl . '/attach/product/' . $product->attach;


            $this->redirect($file);
            exit;
        } else {
            $model = new Member();
            if (isset($_POST['Member'])) {
                $model->attributes = $_POST['Member'];
// validate user input and redirect to the previous page if valid

                $lg = $model->login();
                if (!$lg) {
// echo "Login Fail!";
                } else {
                    $session['user'] = $lg;
                    $this->redirect(Yii::app()->request->baseUrl . "/index.php/site/ProductDownload/" . $product->id);
                }
            }
            $this->render('memberlogin', array('model' => $model));
        }
    }

    public function actionProductGroup() {
        $groupID = (isset($_REQUEST['id']) ? $_REQUEST['id'] : "0");

        $modelProductGroup = new ProductGroup();
        $data["product_group_all"] = ProductGroup::model()->findAll('parent_id=0', array('order' => 'sort_order asc')); //$modelProductGroup->searchAll()->data;
// Link from index
        if ($groupID == 0) {
// Search first group id
            $groupID = $data["product_group_all"][0]->id;
        }


        $data["product_group"] = ProductGroup::model()->findByPk($groupID);

        $modelProduct = new Product();
        $modelProduct->group_id = $groupID;
        $data["product"] = $modelProduct->search()->data;

// Data for listview
        $criteria = new CDbCriteria;
        $criteria->compare('group_id', $groupID);
        $criteria->compare('status', 1);
        $products = new CActiveDataProvider('Product', array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'sort_order asc',),
            'pagination' => array('pageSize' => 8),
        ));

        $modelBestSeller = new ProductBestSeller();
        $dataBestSeller = $modelBestSeller->search_best_seller_for_productgroup()->data;
        foreach ($dataBestSeller as $x)
            $arrBestSeller[] = $x->id;

        $this->render('productgroup', array('data' => $data, 'products' => $products, 'arrBestSeller' => $arrBestSeller));
    }

//    public function actionProductGroup() {
//        $groupID = (isset($_REQUEST['id']) ? $_REQUEST['id'] : "0");
//
//        $modelProductGroup = new ProductGroup();
//        $data["product_group_all"] = $modelProductGroup->search()->data;
//
//        // Link from index
//        if ($groupID == 0) {
//            // Search first group id
//            $groupID = $data["product_group_all"][0]->id;
//        }
//
//        $data["product_group"] = ProductGroup::model()->findByPk($groupID);
//
//        $modelProduct = new Product();
//        $modelProduct->group_id = $groupID;
//        $data["product"] = $modelProduct->search()->data;
//
//        $this->render('productgroup', array('data' => $data));
//    }

    public function actionGalleryImage() {
        $this->layout = "main2column";

        $criteria = new CDbCriteria;
        $criteria->compare('code', 'gallery_image');
        $criteria->addCondition("status = '1' ");

        $gallerys = new CActiveDataProvider('Gallery', array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 't.id desc',),
            'pagination' => array('pageSize' => 6),
        ));

        $this->render("galleryimage", array('gallerys' => $gallerys));
    }

    public function actionGalleryImageDetail() {
        $this->layout = "main2column";

        $id = (isset($_GET['id']) && $_GET['id'] != "" ? $_GET['id'] : 0);

        $data["gallery"] = Gallery::model()->findByPk($id);

        $modelGalleryImage = new GalleryImage();
        $data["gallery_image"] = $modelGalleryImage->searchByGalleryID($id)->data;

        $this->render("galleryimagedetail", array('data' => $data));
    }

    public function actionGalleryVideo() {
        $this->layout = "main2column";

        $criteria = new CDbCriteria;
        $criteria->compare('code', 'gallery_video');
        $criteria->addCondition("status = '1' ");

        $gallerys = new CActiveDataProvider('Gallery', array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 't.id desc',),
            'pagination' => array('pageSize' => 6),
        ));

        $this->render("galleryvideo", array('gallerys' => $gallerys));
    }

    public function actionGalleryVideoDetail() {
        $this->layout = "main2column";

        $id = (isset($_GET['id']) && $_GET['id'] != "" ? $_GET['id'] : 0);

        $data["gallery"] = Gallery::model()->findByPk($id);

        $modelGalleryVideo = new GalleryVideo();
        $data["gallery_video"] = $modelGalleryVideo->searchByGalleryID($id)->data;

        $this->render("galleryvideodetail", array('data' => $data));
    }

    public function actionVideoContent() {
        $this->layout = "main2column";

        $id = (isset($_REQUEST['id']) && $_REQUEST['id'] != "" ? $_REQUEST['id'] : 0);
        $gallery_id = (isset($_REQUEST['gallery_id']) && $_REQUEST['gallery_id'] != "" ? $_REQUEST['gallery_id'] : 0);

        $data["gallery"] = Gallery::model()->findByPk($gallery_id);
        $data["video"] = GalleryVideo::model()->findByPk($id);

        $this->render("videocontent", array('data' => $data));
    }

    public function actionSearch() {
        $this->layout = "main2column";

        $sql = "select id,type,name1,name2,name3,name4,name5,desc1,desc2,desc3,desc4,desc5,detail1,detail2,detail3,detail4,detail5,picture,create_date from ("
                . "select p.id,'Product' as type,p.product_name_1 as name1,"
                . "p.product_name_2 as name2,p.product_name_3 as name3,"
                . "p.product_name_4 as name4,p.product_name_5 as name5,"
                . "product_desc_1 as desc1,product_desc_2 as desc2,product_desc_3 as desc3,"
                . "product_desc_4 as desc4,product_desc_5 as desc5,'' as detail1,'' as detail2,"
                . "'' as detail3,'' as detail4,'' as detail5,p.pic1 as picture,p.create_date "
                . "from tb_product as p "
                . "where p.status != '4' "
                . "union "
                . "select c.id,'News' as type,c.title_1 as name1,c.title_2 as name2,"
                . "c.title_3 as name3,c.title_4 as name4,c.title_5 as name5,"
                . "c.short_content_1 as desc1,c.short_content_2 as desc2,"
                . "c.short_content_3 as desc3,c.short_content_4 as desc4, "
                . "c.short_content_5 as desc5,content_1 as detail1,content_2 as detail2,"
                . "content_3 as detail3,content_4 as detail4,content_5 as detail5,concat(c.id,'.jpg')as picture,c.create_date "
                . "from tb_content as c "
                . "where c.status != '4' and c.content_code='News' "
                . ")tb "
                . "where 1=1 ";

// Search condition --------------
        if (isset($_GET['search_type']) && trim($_GET['search_type']) != "")
            $sql .= "and type = '" . $_GET['search_type'] . "' ";

        if (isset($_GET['start_date']) && trim($_GET['start_date']) != "")
            $sql .= "and DATE_FORMAT(create_date,'%Y%m%d') >= '" . date_format(date_create($_GET['start_date']), 'Ymd') . "' ";

        if (isset($_GET['end_date']) && trim($_GET['end_date']) != "")
            $sql .= "and DATE_FORMAT(create_date,'%Y%m%d') <= '" . date_format(date_create($_GET['end_date']), 'Ymd') . "' ";

        if (isset($_GET['keyword']) && trim($_GET['keyword']) != "") {
            $sql .= "and ( ";
            $sql .= "name1 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or name2 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or name3 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or name4 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or name5 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or desc1 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or desc2 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or desc3 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or desc4 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or desc5 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or detail1 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or detail2 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or detail3 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or detail4 like '%" . $_GET['keyword'] . "%' ";
            $sql .= "or detail5 like '%" . $_GET['keyword'] . "%' ";
            $sql .= ") ";
        }
// -------------------------------

        $total = Yii::app()->db->createCommand("SELECT COUNT(*) FROM (" . $sql . ")tb")->queryScalar();

        $content = new CSqlDataProvider($sql, array(
            'totalItemCount' => $total,
            'sort' => array('defaultOrder' => 'id desc',),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $this->render("search", array('content' => $content, 'total' => $total));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        /*
          Yii::app()->user->logout();
          $this->redirect(Yii::app()->homeUrl);
         */
        $session = new CHttpSession;
        $session->open();
        if (isset($session['user']))
            unset($session['user']);
        $this->redirect(Yii::app()->request->baseUrl . "/");
    }

    public function actionForgetPass() {
        /*
          Yii::app()->user->logout();
          $this->redirect(Yii::app()->homeUrl);
         */
        $this->layout = "main2column";

        $model = new Member();
        if (isset($_POST['Member'])) {
// validate user input and redirect to the previous page if valid
            $email = $_POST['Member']['email'];
            $user = Member::model()->find('email=:email', array(':email' => $email));
            if (isset($user) && count($user) > 0) {
                $mail_setting = Setting::model()->findByPk(1);
//$mail_setting->email = "pinyo_p@windowslive.com";
                $message = new YiiMailMessage;
//	$body = $mail_setting->mail_body;
                $body = "";
                $body .= "ถึง คุณ " . $user->first_name . ' ' . $user->last_name . "<br /><br/>";
                $body .= "Email : " . $user->email . "<br />";
                $body .= "รหัสผ่านของคุณคือ : " . $user->password . "<br />";
                $body .= "<br/><br/><br/>";
                $body .= "Regards,<br/>";
                $body .= "BIF Ecommerce System";
                $subject = Yii::prefixMailSubject() . "ลืมรหัสผ่าน";
                $message->message->setBody($body, 'text/html', 'utf-8');
                $message->setSubject('=?utf-8?B?' . base64_encode($subject) . '?=');
//$message->subject = $subject;
//$message->setSubject($subject);
                $message->addTo($user->email);
//$message->from   = ($mail_setting->sender);
                $message->from = $mail_setting->email;

                if (Yii::allow_send_email())
                    Yii::app()->mail->send($message);
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/forgetfinish');
            }
        }
        $this->render('forgetpass', array('model' => $model));
    }

    public function actionForgetFinish() {
        $this->layout = "main2column";
        $this->render('forgetfinish');
    }

    public function actionLang() {
        $code = $_REQUEST['code'];
        $_COOKIE['lang'] = $code;
        setcookie("lang", $code);
        Yii::app()->request->cookies['lang'] = new CHttpCookie('lang', $code);
//$this->redirect(Yii::app()->request->baseUrl . "/");
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function actionCurrency() {
        $code = $_REQUEST['code'];
        Yii::app()->session['currency'] = $code;
//$this->redirect(Yii::app()->request->baseUrl . "/");
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function summaryOrder($orderID) {
        $sum = 0;

// Cart
        $modelCart = Order::model()->findAll('order_id=:order_id', array(':order_id' => $orderID));
        foreach ($modelCart as $rowCart) {
            $sum += $rowCart->amount * $rowCart->price;
        }

// Shipping fee
        $modelCartInfo = CartInfo::model()->find('order_id=:order_id', array(':order_id' => $orderID));
        if (count($modelCartInfo) > 0)
            $sum += $modelCartInfo->shipping_fee;

        return $sum;
    }

// คำนวนแต้มต่อยอดซื้อ
    public function calOrderScore($cart_id) {
        $totalScore = 0;
        $totalOrder = 0;

// หาจำนวนสั่งซื้อรวม
        $modelCart = new Order();
        $modelCart = Order::model()->findAll('session_id=:cart_id and status=0 and order_id=0', array(':cart_id' => $cart_id));
        foreach ($modelCart as $rowCart) {
            $amount = $rowCart->amount;
            $price = $rowCart->price;
            $totalOrder += $price * $amount;
        }

// หาจำนวนสั่งซื้อต่อ 1 แต้ม
        $dataScoreRate = Config::model()->searchByCode('SCORE_RATE')->data;
        $scoreRate = 0;
        if (count($dataScoreRate) > 0)
            $scoreRate = $dataScoreRate[0]["cfg_value"];

        $totalScore = floor(($totalOrder) / $scoreRate);

        return $totalScore;
    }

// คำนวณอัตราน้ำหนักที่จะจัดส่งทั้งหมด (หน่วย ก.)
    public function calTotalOrderWeight($cart_id) {
        $totalOrder = 0;
        $totalGeneral = 0;
        $totalFresh = 0;
        $totalRestaurant = 0;

        $totalWeight = 0;
        $weightGeneral = 0;
        $weightFresh = 0;
        $weightRestaurant = 0;

// ของสด สั่งตั้งแต่ 2000 ขึ้นไป ส่งฟรี
// รายการสินค้าสำหรับร้านอาหาร สั่งตั้งแต่ 2000 ขึ้นไป ส่งฟรี
// คิดตามน้ำหนัก
// loop รายการสินค้าแต่ละรายการออกมาเพื่อหาประเภท และราคา
        $modelCart = new Order();
        $modelCart = Order::model()->findAll('session_id=:cart_id and status=0 and order_id=0', array(':cart_id' => $cart_id));
        foreach ($modelCart as $rowCart) {
            $product_id = $rowCart->product_id;
            $amount = $rowCart->amount;
            $price = $rowCart->price;
            $sumPrice = $price * $amount;

// หาประเภทสินค้าสด,สำหรับร้านอาหาร และน้ำหนัก
            $modelProduct = new Product();
            $modelProduct = Product::model()->find('id=:product_id', array(':product_id' => $product_id));
            $isFresh = $modelProduct->is_fresh_type;
            $isRestaurant = $modelProduct->is_for_restaurant;
            $sumWeight = $modelProduct->weight_kg * $amount;

            if ($isFresh == 1) {
                $totalFresh += $sumPrice;
                $weightFresh += $sumWeight;
            } else {
                if ($isRestaurant == 1) {
                    $totalRestaurant += $sumPrice;
                    $weightRestaurant += $sumWeight;
                } else {
                    $totalGeneral += $sumPrice;
                    $weightGeneral += $sumWeight;
                }
            }
        }

        if ($totalFresh < 2000) {
            $totalOrder += $totalFresh;
            $totalWeight += $weightFresh;
        }
        if ($totalRestaurant < 2000) {
            $totalOrder += $totalRestaurant;
            $totalWeight += $weightRestaurant;
        }
        $totalOrder += $totalGeneral;
        $totalWeight += $weightGeneral;

// คำนวณหาน้ำหนักกล่อง
        $modelConfig = Config::model()->find('cfg_group=:cfg_group and cfg_code=:cfg_code', array(':cfg_group' => 'SHIPPING', ':cfg_code' => 'BOX_WEIGHT'));

// น้ำหนักกล่อง
        $result['box_weight'] = $modelConfig->cfg_value;

// น้ำหนักสินค้าทั้งหมด
        $result['product_weight'] = ($totalWeight * 1000);

// น้ำหนักรวมที่จะจัดส่ง
        $result['total_weight'] = $result['box_weight'] + $result['product_weight'];

        return $result;
    }

// คำนวณหาราคาจัดส่ง
    public function getShippingFee($weight, $shipType) {
        $result = 0;


        $productWeight = $weight;
        $fullWeight = 0;
        $remainWeight = 0;
        $maxWeight = 20000;

        $fullWeight = floor(floatval($productWeight) / $maxWeight);
        $remainWeight = floatval($productWeight) % $maxWeight;

        // Remain weight
        $modelShipping = ShippingCost::model()->find('shipping_type=:shipping_type and :weight_from >= weight_from and :weight_to <= weight_to', array(':shipping_type' => $shipType, ':weight_from' => $remainWeight, ':weight_to' => $remainWeight));
        if (count($modelShipping) > 0) {
            $result = $modelShipping->cost_price;
        }

        // Full box weight (max 2 kg.)
        if ($fullWeight > 0) {
            $modelShipping = ShippingCost::model()->find('shipping_type=:shipping_type and :weight_from >= weight_from and :weight_to <= weight_to', array(':shipping_type' => $shipType, ':weight_from' => $maxWeight, ':weight_to' => $maxWeight));
            if (count($modelShipping) > 0) {
                $result += $modelShipping->cost_price * $fullWeight;
            }
        }

        return $result;
    }

// คำนวณหาอัตราค่าขนส่ง
    public function getShipmentFee($cart_id) {
        $fee = 0;

        $totalOrder = 0;
        $totalGeneral = 0;
        $totalFresh = 0;
        $totalRestaurant = 0;

        $totalWeight = 0;
        $weightGeneral = 0;
        $weightFresh = 0;
        $weightRestaurant = 0;

// ของสด สั่งตั้งแต่ 2000 ขึ้นไป ส่งฟรี
// รายการสินค้าสำหรับร้านอาหาร สั่งตั้งแต่ 2000 ขึ้นไป ส่งฟรี
// คิดตามน้ำหนัก
// loop รายการสินค้าแต่ละรายการออกมาเพื่อหาประเภท และราคา
        $modelCart = new Order();
        $modelCart = Order::model()->findAll('session_id=:cart_id and status=0 and order_id=0', array(':cart_id' => $cart_id));
        foreach ($modelCart as $rowCart) {
            $product_id = $rowCart->product_id;
            $amount = $rowCart->amount;
            $price = $rowCart->price;
            $sumPrice = $price * $amount;

// หาประเภทสินค้าสด,สำหรับร้านอาหาร และน้ำหนัก
            $modelProduct = new Product();
            $modelProduct = Product::model()->find('id=:product_id', array(':product_id' => $product_id));
            $isFresh = $modelProduct->is_fresh_type;
            $isRestaurant = $modelProduct->is_for_restaurant;
            $sumWeight = $modelProduct->weight_kg * $amount;

            if ($isFresh == 1) {
                $totalFresh += $sumPrice;
                $weightFresh += $sumWeight;
            } else {
                if ($isRestaurant == 1) {
                    $totalRestaurant += $sumPrice;
                    $weightRestaurant += $sumWeight;
                } else {
                    $totalGeneral += $sumPrice;
                    $weightGeneral += $sumWeight;
                }
            }
        }

        if ($totalFresh < 2000) {
            $totalOrder += $totalFresh;
            $totalWeight += $weightFresh;
        }
        if ($totalRestaurant < 2000) {
            $totalOrder += $totalRestaurant;
            $totalWeight += $weightRestaurant;
        }
        $totalOrder += $totalGeneral;
        $totalWeight += $weightGeneral;

// หาอัตราค่าขนส่งจากน้ำหนัก
//$modelShipRate = ShipmentRate::model()->find(':kgfrom >= kg_from and :kgto <= kg_to',array(':kgfrom' => $totalWeight, ':kgto' => $totalWeight));
        $modelShipRate = ShipmentRate::model()->find(':kgfrom >= kg_from and :kgto <= kg_to', array(':kgfrom' => $totalWeight, ':kgto' => $totalWeight));
        if (count($modelShipRate) > 0) {
            $fee = $modelShipRate->fee_rate;
        }

        return $fee;
    }

    public function saveStat($ref_id, $type) {
        $c = Stat::model()->count('ref_id=:ref_id and type=:type', array(':ref_id' => $ref_id, ':type' => $type));
        $amount = 0;
        if ($c > 0) {
            $stat = Stat::model()->find('ref_id=:ref_id and type=:type', array(':ref_id' => $ref_id, ':type' => $type));
            $amount = $stat->amount;
        } else {
            $stat = new Stat();
        }
        $amount++;
        $stat->ref_id = $ref_id;
        $stat->type = $type;
        $stat->amount = $amount;
        $stat->save();
    }

    function str_thai_date($str_date) {
        $thai_day_arr = array("อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์");
        $thai_month_arr = array(
            "0" => "",
            "01" => "ม.ค.",
            "02" => "ก.พ.",
            "03" => "มี.ค.",
            "04" => "เม.ย.",
            "05" => "พ.ค.",
            "06" => "มิ.ย.",
            "07" => "ก.ค.",
            "08" => "ส.ค.",
            "09" => "ก.ย.",
            "10" => "ต.ค.",
            "11" => "พ.ย.",
            "12" => "ธ.ค."
        );
        $d = explode("-", $str_date);
        if (count($d) > 1) {
            $thai_date_return = ceil($d[2]);
            $thai_date_return.=" " . $thai_month_arr[$d[1]] . " ";
            $thai_date_return.= ($d[0] + 543);
            return $thai_date_return;
        } else {
            return "";
        }
    }

    function thai_date() {
        $time = time();
        $thai_day_arr = array("อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์");
        $thai_month_arr = array(
            "0" => "",
            "1" => "มกราคม",
            "2" => "กุมภาพันธ์",
            "3" => "มีนาคม",
            "4" => "เมษายน",
            "5" => "พฤษภาคม",
            "6" => "มิถุนายน",
            "7" => "กรกฎาคม",
            "8" => "สิงหาคม",
            "9" => "กันยายน",
            "10" => "ตุลาคม",
            "11" => "พฤศจิกายน",
            "12" => "ธันวาคม"
        );
        $thai_date_return = "วัน" . $thai_day_arr[date("w", $time)];
        $thai_date_return.= "ที่ " . date("j", $time);
        $thai_date_return.=" " . $thai_month_arr[date("n", $time)];
        $thai_date_return.= " พ.ศ." . (date("Yํ", $time) + 543);
//$thai_date_return.= "  ".date("H:i",$time)." น.";  
        return $thai_date_return;
    }

    function formatTitle2Tone($title) {
        $formatTitle = $title;
        $codes = explode(" ", $title);
        if (count($codes) > 1) {
            $formatTitle = $codes[0];
            $flag = 0;
            for ($i = 1; $i < count($codes); $i++) {
                if ($flag == 0)
                    $formatTitle .= " <span class='txt-org'>" . $codes[$i];
                else
                    $formatTitle .= " " . $codes[$i];
                $flag++;
            }
            $formatTitle .= "</span>";
        }
        return $formatTitle;
    }

}
