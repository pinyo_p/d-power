<?php
function setDate($start_date,$end_date)
{
	$str = "";
	if($start_date != "" && $end_date !="")
		$str = $start_date . ' - ' . $end_date;
	return $str;
}
function showDate($row)
{
	$arr_date[] = setDate($row->start_date,$row->end_date);
	$arr_date[] = setDate($row->start_date2,$row->end_date2);
	$arr_date[] = setDate($row->start_date3,$row->end_date3);
	$arr_date[] = setDate($row->start_date4,$row->end_date4);
	$arr_date[] = setDate($row->start_date5,$row->end_date5);
	$arr_date[] = setDate($row->start_date6,$row->end_date6);
	$arr_date[] = setDate($row->start_date7,$row->end_date7);
	$arr_date[] = setDate($row->start_date8,$row->end_date8);
	$arr_date[] = setDate($row->start_date9,$row->end_date9);
	$arr_date[] = setDate($row->start_date10,$row->end_date10);
	$arr_date[] = setDate($row->start_date11,$row->end_date11);
	$arr_date[] = setDate($row->start_date12,$row->end_date12);
	$arr = array();
	foreach($arr_date as $d)
	{
		if($d != "")
			$arr[] =$d;
	}
	$data = implode("<br />",$arr);
	return $data;
}
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/tab/tabs.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/tab/general.css" type="text/css" media="screen" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" class="main-column-bg">
<div class="main-column-content">
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="main-content-header">ร่วมงานกับเรา</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="jobs_detail">
                  
                  <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td colspan="2" align="left" class="head">
                  <?php echo $model->tranining_code . ' : ' .  Yii::d($model->subject_en,$model->subject_th);?>
                  </td>
                </tr>
                  	<tr>
                    	 <th width="20%" align="left"><strong>วันที่อบรม :</strong></th>
                  		<td><?php echo showDate($model) ;?></td>
                    </tr>
                    <tr>
                    	 <th width="20%" align="left"><strong>จำนวนชั่วโมง :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->times ;?></td>
                    </tr>
                    <tr>
                    	 <th width="20%" align="left"><strong>จำนวนวัน :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->days ;?></td>
                    </tr>
                    <tr>
                    	 <th width="20%" align="left"><strong>ระหว่างเวลา :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->duration ;?></td>
                    </tr>
                    <tr>
                    	 <th width="20%" align="left"><strong>ค่าใช้จ่าย :</strong></th>
                  		<td class="job-content-txt"><?php echo number_format($model->price,0,".",",") ;?></td>
                    </tr>
                    <tr>
                    	 <th width="20%" align="left"><strong>ภาษา :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->language ;?></td>
                    </tr>
                    
                    <tr>
                    	 <th width="20%" align="left"><strong>รายละเอียดหลักสูตร :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->benefit_th ;?></td>
                    </tr>
                    <tr>
                    	 <th width="20%" align="left"><strong>วัตถุประสงค์ :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->object_th ;?></td>
                    </tr>
                    <tr>
                    	 <th width="20%" align="left"><strong>หัวข้อการอบรม :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->course_th ;?></td>
                    </tr>
                    <tr>
                    	 <th width="20%" align="left"><strong>คุณสมบัติผู้เข้าอบรม :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->who_interest_th ;?></td>
                    </tr>
                    
                    
                    <tr>
                    	 <th width="20%" align="left"><strong>วิธีการสมัครและชำระเงิน :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->payment_th ;?></td>
                    </tr>
                    <tr>
                    	 <th width="20%" align="left"><strong>ติดต่อสอบถาม :</strong></th>
                  		<td class="job-content-txt"><?php echo $model->contact_th ;?></td>
                    </tr>
                    <tr>
                     <td align="center" colspan="2">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/traininglist/"><img width="133" height="32" src="<?php echo Yii::app()->request->baseUrl; ?>/images/back.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Course/<?php echo $model->id;?>"><img width="133" height="32" src="<?php echo Yii::app()->request->baseUrl; ?>/images/apply.png"></a>
                
                    <br></td>
                    </tr>
                   </table>
            
            
                </td>
                </tr>
                </table>    
                </div>
                </td>
                </tr>
                </table>