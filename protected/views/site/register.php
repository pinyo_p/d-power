<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'register-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?> 

<link href="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.11.2.custom/jquery-ui.css" rel="stylesheet" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.11.2.custom/jquery-ui.js"></script>

<script language="javascript">
    function toggle(obj)
    {
        val = $(obj).val();
        if (val == 2){
            $("#tr_company").show('fast');
            $("#tr_company_type").show('fast');
        }else{
            $("#tr_company").hide('fast');
            $("#tr_company_type").hide('fast');
        }
    }
    $(document).ready(function () {
        $('#btnCancel').click(function () {
            location.href = "<?php echo Yii::app()->request->baseUrl; ?>";
        });

        $('#btnRegister').click(function () {
            $("#register-form").submit();
        });
    });</script>

<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                สมัครสมาชิก <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/>

        <fieldset class="register-box-group">
            <legend>รายละเอียดข้อมูลส่วนตัว</legend>
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left">ประเภทสมาชิก</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <select name='Member[member_type]' id='Member_member_type' onchange="toggle(this)">
                            <option value="1" <?php if(isset($_POST['Member']['member_type']) && $_POST['Member']['member_type']=="1") echo "selected='selected'"; ?>>บุคคลทั่วไป</option>
                            <option value="2" <?php if(isset($_POST['Member']['member_type']) && $_POST['Member']['member_type']=="2") echo "selected='selected'"; ?>>นิติบุคคล</option>
                        </select>
                    </div>
                </div>
                <div class="register-box-row" id="tr_company" style="display:<?php if(!isset($_POST['Member']['member_type']) || (isset($_POST['Member']['member_type']) && $_POST['Member']['member_type']=='1')) echo 'none'; ?>;">
                    <div class="register-box-left">ชื่อบริษัท,หจก.,ร้านอาหาร</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'company_name',array('style'=>'width:300px')); ?><span class="form-error-info"><?php echo $form->error($model, 'company_name'); ?></span>
                    </div>
                </div>
                <div class="register-box-row" id="tr_company_type" style="display:<?php if(!isset($_POST['Member']['member_type']) || (isset($_POST['Member']['member_type']) && $_POST['Member']['member_type']=='1')) echo 'none'; ?>;">
                    <div class="register-box-left">ประเภท</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->dropDownList($model, 'company_type', array('' => '-- ไม่ระบุ --', 
                            'THA' => 'ร้านอาหารไทย', 'CHS' => 'ร้านอาหารจีน', 'JPN' => 'ร้านอาหารญี่ปุ่น', 'ITS' => 'ร้านอาหารอิตาเลี่ยน',
                            'FT' => 'ร้านอาหารสไตล์ฟิวชั่น', 'KOR' => 'ร้านอาหารเกาหลี', 'BG' => 'ร้านเบเกอรี่', 'HT' => 'โรงแรม', 
                            'RS' => 'ภัตตาคาร', 'OT' => 'อื่นๆ'),array('style' => 'width:200px;')); ?><span class="form-error-info"><?php echo $form->error($model, 'company_type'); ?></span>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">
                        <?php echo $form->radioButton($model, 'prefix', array('value' => 1, 'uncheckValue' => null)); ?>
                        นาย
                        <?php echo $form->radioButton($model, 'prefix', array('value' => 2, 'uncheckValue' => null)); ?>
                        นาง
                        <?php echo $form->radioButton($model, 'prefix', array('value' => 3, 'uncheckValue' => null)); ?>
                        นางสาว
                        <?php echo $form->radioButton($model, 'prefix', array('value' => 4, 'uncheckValue' => null)); ?>
                        อื่นๆ
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'prefix_spec',array('style'=>'width:300px')); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">
                        ชื่อ <span>*</span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'first_name',array('style'=>'width:300px')); ?> <span class="form-error-info"><?php echo $form->error($model, 'first_name'); ?></span>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">
                        นามสกุล
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'last_name',array('style'=>'width:300px')); ?> <span class="form-error-info"><?php echo $form->error($model, 'last_name'); ?></span>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">
                        วันเกิด
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
                            'name' => 'Member[birth_day]',
                            'value' => $model->birth_day,
                            'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat' => 'yy-mm-dd',
                                'altFormat' => 'yy-mm-dd',
                                'changeMonth' => 'true',
                                'changeYear' => 'true',
                                'showOn' => "both",
                                'yearRange' => "'" . date("Y") - 70 . ':' . date("Y") - 10 . "'",
                                'buttonImage' => Yii::app()->request->baseUrl . "/images/carlendar.png",
                                'buttonImageOnly' => "true",
                            ),
                            'htmlOptions' => array(
                                'style' => 'height:20px;'
                            ),
                                )
                        );
                        ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left" style="vertical-align: top;">
                        ที่อยู่
                    </div>
                    <div class="register-box-mid" style="vertical-align: top;">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textArea($model, 'address', array('rows' => 5, 'cols' => 60)); ?> <span class="form-error-info"><?php echo $form->error($model, 'address'); ?></span>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">
                        จังหวัด
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php
                        echo $form->dropDownList($model, 'province', CHtml::listData(Provinces::model()->findAll(array('order' => 'thai_name ASC')), 'province_id', 'thai_name'), array('empty' => '-- เลือกจังหวัด --',
                        ));
                        ?> <span class="form-error-info"><?php echo $form->error($model, 'province'); ?></span>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">
                        รหัสไปรษณีย์
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'zipcode',array('style'=>'width:300px')); ?> <span class="form-error-info"><?php echo $form->error($model, 'zipcode'); ?></span>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">
                        เบอร์โทรศัพท์ <span>*</span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'phone_no',array('style'=>'width:300px')); ?> <span class="form-error-info"><?php echo $form->error($model, 'phone_no'); ?></span> (เช่น 023587421)
                    </div>
                </div>
                
                <div class="register-box-row">
                    <div class="register-box-left">
                        Line ID <span></span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'line_id',array('style'=>'width:300px')); ?> <span class="form-error-info"><?php echo $form->error($model, 'line_id'); ?></span> 
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">
                        เลขประจำตัวผู้เสียภาษี  <span>*</span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'tax_no',array('style'=>'width:300px')); ?> <span class="form-error-info"><?php echo $form->error($model, 'tax_no'); ?></span>
                    </div>
                </div>
                
            </div>
        </fieldset>

        <br/>

        <fieldset class="register-box-group">
            <legend>ข้อมูลการเข้าสู่ระบบ</legend>
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left">อีเมล์ <span>*</span></div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'email',array('style'=>'width:300px')); ?><span class="form-error-info"><?php echo $form->error($model, 'email'); ?></span>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">รหัสผ่าน <span>*</span></div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->passwordField($model, 'password',array('style'=>'width:300px')); ?><span class="form-error-info"><?php echo $form->error($model, 'password'); ?></span>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">ยืนยันรหัสผ่าน <span>*</span></div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->passwordField($model, 'cpassword',array('style'=>'width:300px')); ?><span class="form-error-info"><?php echo $form->error($model, 'cpassword'); ?></span>
                    </div>
                </div>
            </div>
        </fieldset>

        <br/>

        <fieldset class="register-box-group">
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left">Verify Code</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php if (CCaptcha::checkRequirements()): ?>
                            <div class="row">
                                <?php echo $form->labelEx($model, ''); ?>
                                <div>
                                    <?php $this->widget('CCaptcha'); ?><br />
                                    <?php echo $form->textField($model, 'verifyCode'); ?>
                                </div>
                                <div class="form">
                                    <span class="form-error-info"><?php echo $form->error($model, 'verifyCode'); ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </fieldset>

        <br/><br/>

        <div class="register-box-row">
            <div class="register-box-left"></div>
            <div class="register-box-mid"></div>
            <div class="register-box-right">
                <div class = "register-button">
                    <button id="btnRegister" name="btnRegister">สมัคร</button>
                    &nbsp;
                    <button id="btnCancel" onclick="return false;" name="btnCancel">ยกเลิก</button>
                </div>
            </div>
        </div>

    </div>
</div>

<?php $this->endWidget(); ?>