<script language="javascript">
    var oldAmount;
    function changeIt(objTxt, objId, objProductId)
    {
        $.post('<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/UpdateCart/',
                {
                    id: objId,
                    val: $(objTxt).val(),
                    product_id: objProductId
                },
        function (data) {
            if (data.result == "success") {
                var currency = $("#ddlCurrency").val();
                var dollarExtRate = $('#hidDollarExtRate').val();
                var shippingFee = data.shippingfee;
                var shippingType = data.shiptype;
                var weightIsOver = data.weightover;
                var totalWeight = data.totalweight;

                $("#price_" + objId).text(data.price);
                $("#total_" + objId).text(data.total);
                var dataTota = data.total.replace(",", "");

                if (currency == "dollar") {
                    $("#price_" + objId).text((parseFloat(data.price) / parseFloat(dollarExtRate)).toFixed(2));
                    $("#total_" + objId).text((parseFloat(dataTota) / parseFloat(dollarExtRate)).toFixed(2));
                }

                var total = 0;
                $('.sub_total').each(
                        function () {
                            val = $(this).text();
                            val = val.replace(",", "");
                            //total += parseInt(val);
                            total += parseFloat(val);
                        });

                var xTotal = total;
                var xShippingFee = parseInt(shippingFee);//parseInt($("#shipping_fee").text());
                $("#shipping_fee").text(addCommas(roundNumber(parseFloat(shippingFee), 2)));
                $('#score').text(data.score);
                var xSummery = xTotal + xShippingFee;
                $("#grand_total").text(addCommas(roundNumber(parseFloat(total), 2)));
                $("#net_total").text(addCommas(roundNumber(parseFloat(xSummery), 2)));

                // Choose shipping type
                if (weightIsOver === true) {
                    $('#rowChooseShippingType').show();
                } else if (weightIsOver === false) {
                    $('#rowChooseShippingType').hide();                    
                }
                //$('#ddlShippingType').val(shippingType);              
                $('input:radio[name="rbShippingType"]').filter('[value="'+shippingType+'"]').attr('checked', true);
                $('#lblTotalWeight').text(totalWeight);
            } else {
                alert(data.msg);
                $(objTxt).val(oldAmount);
                $(objTxt).focus();
            }
//													   alert(data.total);
        }, 'json');

    }
    function roundNumber(number, decimal_points) {
        if (!decimal_points)
            return Math.round(number);
        if (number == 0) {
            var decimals = "";
            for (var i = 0; i < decimal_points; i++)
                decimals += "0";
            return "0." + decimals;
        }

        var exponent = Math.pow(10, decimal_points);
        var num = Math.round((number * exponent)).toString();
        return num.slice(0, -1 * decimal_points) + "." + num.slice(-1 * decimal_points)
    }
    function setAmount(obj)
    {
        oldAmount = $(obj).val();
    }
    function deleteIt(objId)
    {
        $.post('<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/DeleteCart/', {id: objId},
        function (data)
        {
            if (data == "OK")
                location.reload();
            else
                alert(data);
        });
    }
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    function submitme()
    {
        var valid = false;
        var msg = "";
        var ids = new Array();
        var product_ids = new Array();
        var amount_ids = new Array();
        /*
         myCars[0]="Saab";       
         myCars[1]="Volvo";
         myCars[2]="BMW";*/
        var i = 0;
        $('.row_id').each(
                function () {
                    objId = $(this).val();
                    ids[i] = objId;
                    amount_ids[i] = $("#amount_" + objId).val();
                    product_ids[i] = $("#product_id_" + objId).val();
                    i++;
                });
        $.post('<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/CheckCart/',
                {
                    id: ids,
                    val: amount_ids,
                    product_id: product_ids
                }, function (data) {
            if (data.result == "error") {
                msg = data.msg;
            } else {
                location.href = '<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/cartlogin';
            }
        }, 'json');

        /*
         if(valid)
         location.href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/cartlogin';
         else
         alert('not valid');*/
        //m=setTimeout("submitnow()",100);

        //location.href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/cartlogin';
    }
    function submitnow()
    {

    }

    $(document).ready(function () {
        /*$('#ddlShippingType').change(function () {
            var shipType = $(this).val();
            $.post('<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/CartUpdateShipping/', {shiptype: shipType},
            function (data)
            {
                if (data == "OK")
                    location.reload();
                else
                    alert(data);
            });
        });*/
        $('input[name="rbShippingType"]').change(function(){
            var shipType = $(this).val();
            $.post('<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/CartUpdateShipping/', {shiptype: shipType},
            function (data)
            {
                if (data == "OK")
                    location.reload();
                else
                    alert(data);
            });
        });
    });
</script>
<input type="hidden" id="hidDollarExtRate" value="<?php echo Yii::app()->session['dollar_ext_rate']; ?>" />
<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ยืนยันรายการสั่งซื้อ <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/><br/>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" class="txt_bold">จำนวนสินค้าในตะกร้า : <span class="txt_green"><?php echo $param['count_item']; ?></span> รายการ</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <div class="cart-table">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr class="txt_bold bg-gray">
                                <td align="center">รูปภาพสินค้า</td>
                                <td width="200" align="center">ชื่อสินค้า</td>
                                <td width="100" align="center">ราคา/หน่วย</td>
                                <td width="100" align="center">จำนวน</td>
                                <td width="100" align="center">รวมเป็นราคา</td>
                                <td width="100" align="center">ยกเลิกรายการ</td>
                            </tr>
                            <?php
                            $grand_total = 0;
                            $shipping_fee = $param['shipping_fee'];

                            foreach ($data as $row) {
                                $grand_total += ($row->price * $row->amount);
                                ?>

                                <tr>
                                    <td align="center">
                                        <input type="hidden" class='row_id' value="<?php echo $row->id; ?>" />
                                        <input type="hidden" class='product_id' id='product_id_<?php echo $row->id; ?>' value="<?php echo $row->product_id; ?>" />
                                        <?php
                                        $file = Yii::app()->request->baseUrl . '/images/product/' . $row->Product['pic1'];
                                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $row->Product['pic1']) && $row->Product['pic1'] != "") {
                                            ?>
                                            <img src="<?php echo $file; ?>" width="120"  />
                                            <?php
                                        } else {
                                            ?>
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
                                            <?php
                                        }
                                        ?>

                                    </td>
                                    <td width="200" align="center" class="txt_green txt_bold">
                                        <?php
                                        echo Yii::text_lang(array(
                                            $row->Product['product_name_1'],
                                            $row->Product['product_name_2'],
                                            $row->Product['product_name_3'],
                                            $row->Product['product_name_4'],
                                            $row->Product['product_name_5']));
                                        ?>
                                    </td>
                                    <td width="100" align="center" class="txt_bold"><span id='price_<?php echo $row->id; ?>'><?php echo number_format(Yii::currencyAmount($row->price), 2, ".", ","); ?></span></td>
                                    <td width="100" align="center"><input name="amount_<?php echo $row->id; ?>" type="text" id="amount_<?php echo $row->id; ?>" size="2" onchange="changeIt(this,<?php echo $row->id; ?>,<?php echo $row->product_id; ?>)" onfocus="setAmount(this)" value="<?php echo $row->amount; ?>" class="product-detail-amount" />
                                    </td>
                                    <td width="100" align="center" class="txt_bold"><span id='total_<?php echo $row->id; ?>' class="sub_total"><?php
                                            echo number_format(Yii::currencyAmount(($row->price * $row->amount)), 2, ".", ",");
                                            ?></span></td>
                                    <td width="100" align="center"><a href="javascript:deleteIt('<?php echo $row->id; ?>')"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/x.png" width="17" height="16" /></a></td>
                                </tr>
                                <?php
                            }
                            $summary_total = $grand_total + $shipping_fee;
                            if ($grand_total == 0)
                                $summary_total = 0;
                            ?>

                            <tr class="txt_bold bg-gray">
                                <td colspan="6">                                    
                                    <div>น้ำหนักสินค้าที่จะจัดส่ง (ไม่รวมสินค้าจัดส่งฟรี) : <span style="color:#0000ff" id="lblTotalWeight"><?php echo $param["total_weight"]; ?></span> กก.</div>
                                    <?php
                                    $showChoose = "none";
                                    if (floatval($param["total_weight"]) > 2.0)
                                        $showChoose = "";
                                    ?>
                                    <div id="rowChooseShippingType" style="display:<?php echo $showChoose; ?>;">
                                        <table>
                                            <tr>
                                                <td valign="top" style="border:none;">เลือกรูปแบบการจัดส่ง (กรณีน้ำหนักสินค้าที่จะจัดส่งเกิน 2 กก.) :</td>
                                                <td valign="top" style="border:none;">
                                                    <div><input type="radio" name="rbShippingType" value="in_ems" <?php echo ($param['shipping_type'] == "in_ems") ? "checked='checked'" : ""; ?> /> จัดส่งพัสดุแบบ EMS : มีเลขที่พัสดุ และมีการประกันของหาย</div>
                                                    <div><input type="radio" name="rbShippingType" value="in_gen" <?php echo ($param['shipping_type'] == "in_gen") ? "checked='checked'" : ""; ?> /> จัดส่งพัสดุแบบธรรมดา : ไม่มีเลขที่พัสดุ และไม่มีการประกันของหาย</div>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--<select id="ddlShippingType" name="ddlShippingType" class="product-detail-shippingtype">
                                            <option value="in_ems" <?php echo ($param['shipping_type'] == "in_ems") ? "selected='selected'" : ""; ?>>จัดส่งพัสดุแบบ EMS : มีเลขที่พัสดุ และมีการประกันของหาย</option>
                                            <option value="in_gen" <?php echo ($param['shipping_type'] == "in_gen") ? "selected='selected'" : ""; ?>>จัดส่งพัสดุแบบธรรมดา : ไม่มีเลขที่พัสดุ และไม่มีการประกันของหาย</option>
                                        </select>  -->
                                    </div>

                                </td>
                            </tr>

                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="400" align="center"><table width="80%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="50%" align="center"><a style="display:none;" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/cart/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/edit.png" width="102" height="33" /></a></td>
                                        <td width="50%" align="center">
                                            <?php if (count($data) > 0) { ?>
                                                <a id='cart_login' href="javascript:submitme()"  ><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/sumit.png" width="102" height="33" /></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </table></td>
                            <td width="20" valign="top">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="">
                                        <td width="60%" height="30" class="txt_bold hline">รวมยอดสั่งซื้อ</td>
                                        <td width="10" height="30" class="txt_bold hline">|</td>
                                        <td height="30" align="right" class="txt_price hline"><span id="grand_total"><?php echo number_format(Yii::currencyAmount($grand_total), 2, ".", ","); ?></span> <?php echo Yii::currencySign(); ?> </td>
                                    </tr>                                    
                                    <tr>
                                        <td width="60%" height="30" class="txt_bold hline">ค่าจัดส่ง</td>
                                        <td width="10" height="30" class="txt_bold hline">|</td>
                                        <td height="30" align="right" class="txt_price hline"><span id="shipping_fee"><?php echo number_format(Yii::currencyAmount($shipping_fee), 2, ".", ","); ?></span> <?php echo Yii::currencySign(); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="60%" height="30" class="txt_bold dhline">รวมราคาสุทธิ</td>
                                        <td width="10" height="30" class="txt_bold dhline">|</td>
                                        <td height="30" align="right" class="txt_price dhline"><span id="net_total"><?php echo number_format(Yii::currencyAmount($summary_total), 2, ".", ","); ?></span> <?php echo Yii::currencySign(); ?></td>
                                    </tr> 
                                    <tr>
                                        <td colspan='3'>&nbsp;</td>
                                    </tr>                                
                                    <tr>
                                        <td colspan="3"><div style='height:1px;background:#dddddd;'></div></td>
                                    </tr>
                                    <tr>
                                        <td colspan='3'>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="60%" height="30" class="txt_bold dhline">จำนวนแต้ม</td>
                                        <td width="10" height="30" class="txt_bold dhline">|</td>
                                        <td height="30" align="right" class="txt_price dhline"><span id="score"><?php echo $param['score']; ?></span></td>
                                    </tr> 
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <br/><br/>
                        <u>หมายเหตุ : </u>
                        <div>จำนวนแต้ม 1 แต้ม ต่อการซื้อทุกๆ <?php echo $param["score_rate"]; ?> บาท</div>
                        <div>รายการสินค้าจัดส่งฟรี ได้แก่ ของสด สั่งตั้งแต่ 2000 ขึ้นไปและรายการสินค้าสำหรับร้านอาหาร สั่งตั้งแต่ 2000 ขึ้นไป</div>
                        <div>กรณีน้ำหนักสินค้าจัดส่งเกิน 2 กก. ให้เลือกรูปแบบการจัดส่งแบบพัสดุ EMS หรือแบบพัสดุธรรมดา</div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="right">&nbsp;</td>
            </tr>
        </table>


    </div>
</div>