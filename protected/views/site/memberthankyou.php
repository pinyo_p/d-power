<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                สมัครสมาชิก <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/>
        <div class="register-box-thankyou">
            <div class="box-info">
                <div class="register-box-thankyou-info">สมัครสมาชิกเสร็จสมบูรณ์</div>
                <div class="register-box-thankyou-subinfo">ขอขอบคุณที่สมัครสมาชิกกับทางเว็บไซต์เรา <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/MemberLogin">คลิกที่นี่</a> เพื่อเข้าสู่หน้า Login</div>
                <div class="register-box-thankyou-subinfo"></div>
            </div>
        </div>
    </div>
</div>