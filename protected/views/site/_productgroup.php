<li class="product-box-content-list-item">
    <div class="product-img">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/productdetail/<?php echo $data->id; ?>" class="link-more">
            <?php
            $file = Yii::app()->request->baseUrl . '/images/product/' . $data->pic1;
            if (file_exists(Yii::app()->basePath . '/../images/product/' . $data->pic1) && $data->pic1 != "") {
                ?>
                <img src="<?php echo $file; ?>" height="73" />
                <?php
            } else {
                ?>
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/product_default.jpg" height="73" />
                <?php
            }
            ?>
        </a>
    </div>
    <div class="product-detail">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="txt-org"><?php echo $data->product_code; ?></td>
                <td align="right">
                    <div style="height:10px;">
                <?php if($data->is_highlight == "1"){ ?>
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/reccom.png" width="82" height="13"  alt=""/>
                    <?php }else{ ?>
                    <?php if (in_array($data->id, $arrBestSeller)) { ?>
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/bestsell.png" width="82" height="13"  alt=""/>
                    <?php } ?>
                    <?php } ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="txt-bold"><div style="height:15px;"><?php echo Yii::text_lang(array($data->product_name_1, $data->product_name_2, $data->product_name_3, $data->product_name_4, $data->product_name_5)); ?></div></td>
            </tr>
            <tr>
                <td colspan="2"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/productdetail/<?php echo $data->id; ?>" class="link-more"><div style="padding-top:5px;">ดูรายละเอียด</div></a></td>
            </tr>
            <tr>
                <td class="txt-bold" style="width:50%;">
                    <?php
                    if ($data->price_discount != null && $data->price_discount != 0) {
                        echo "<span class='txt-discount'>ปกติ ".Yii::currencySign() . number_format(Yii::currencyAmount($data->price1), 2, ".", ',') . "</span>";
                    } else {
                        echo Yii::currencySign()." " . number_format(Yii::currencyAmount($data->price1), 2, ".", ',');
                    }
                    ?>
                </td>
                <td class="txt-red txt-bold">
                    <?php
                    if ($data->price_discount != null && $data->price_discount != 0) {
                        echo "<span class='txt-red'>พิเศษ ".Yii::currencySign()." " . number_format(Yii::currencyAmount($data->price_discount), 0, ".", ',') . "</span>";
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <?php
                if ($data->stock != null && $data->stock > 0) {
                    ?>  
                    <td colspan="2">จำนวน <input type="text" value="1" id="txtAmt<?php echo $data->id; ?>"> <button type="button" class="btnBuy" productid="<?php echo $data->id; ?>">สั่งซื้อ</button></td>
                <?php } else { ?>
                    <td colspan="2"><span class="txt-red txt-bold">ไม่มีสินค้า</span></td>
                <?php } ?>
            </tr>
        </table>
    </div>
</li>
