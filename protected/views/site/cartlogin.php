<script language="javascript">
    $(document).ready(function(){
       $('#btnLogin').click(function(){
           // validate 
           var email = $('#Member_email').val().trim();
           var pass = $('#Member_password').val().trim();
           
           if(email == ""){
               alert('กรุณากรอก email');
               $('#Member_email').focus();
               return;
           }
           if(pass == ""){
               alert('กรุณากรอกรหัสผ่าน');
               $('#Member_password').focus();
               return;
           }
           $("#login-form").submit();
       }); 
    });
</script>

<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ยืนยันรายการสั่งซื้อ <span class="txt-org"></span>
            </div>
        </div>
        <br/>
        
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
        ?>
        
        <?php echo "<div class='box-form-error-info'>".$loginInfo."</div>"; ?>
        <fieldset class="register-box-group">
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left">Email</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'email'); ?>
                        <?php //echo $form->error($model, 'email'); ?>
                    </div>
                </div>
            </div>
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left">Password</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->passwordField($model, 'password'); ?>
                        <?php //echo $form->error($model, 'password'); ?>
                    </div>
                </div>
            </div>
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left"></div>
                    <div class="register-box-mid"></div>
                    <div class="register-box-right">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow.png" width="12" height="12" /> <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/forgetpass" class="link_w">ลืมรหัสผ่าน</a>
                        &nbsp;&nbsp;
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow.png" width="12" height="12" /> <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/memberregister" class="link_w">หากท่านยังไม่ได้เป็นสมาชิก คลิกที่นี่</a>
                    </div>
                </div>
            </div>
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left"></div>
                    <div class="register-box-mid"></div>
                    <div class="register-box-right">
                        <button id="btnLogin" name="btnLogin" onclick="return false;">Login</button>
                    </div>
                </div>
            </div>
        </fieldset>

        <?php $this->endWidget(); ?>


    </div>
</div>