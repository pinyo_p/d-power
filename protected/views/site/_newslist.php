<li class="<?php echo $index % 2 ? '' : 'bg-gray' ?>">
    <div class="newsbox-left">
        <?php
        $file = Yii::app()->request->baseUrl . '/images/content/' . $data->attr1;
        if (file_exists(Yii::app()->basePath . '/../images/content/' . $data->attr1)) {
            ?>
            <img src="<?php echo $file; ?>" width="227" alt="" />
            <?php
        } else {
            ?>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/news_no_image.jpg" width="227" alt="" />
            <?php
        }
        ?>
    </div>
    <div class="newsbox-right">
        <div class="news-content-box-detail-title">
            <div class="news-content-box-detail-title-txt">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/NewsDetail/id/<?php echo $data->id; ?>">
                    <?php
                    echo Yii::text_lang(array(
                        $data->title_1,
                        $data->title_2,
                        $data->title_3,
                        $data->title_4,
                        $data->title_5));
                    ?>
                </a>
            </div>
            <div class="news-content-box-detail-title-more">
                <?php echo date_format(date_create($data->create_date), 'd-m-Y'); ?>
            </div>
        </div>

        <div class="news-content-box-detail-content">
            <?php
            echo Yii::text_lang(array(
                $data->short_content_1,
                $data->short_content_2,
                $data->short_content_3,
                $data->short_content_4,
                $data->short_content_5));
            ?>
        </div>
    </div>
</li>
