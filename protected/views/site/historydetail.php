<script type="text/javascript">
    $(document).ready(function () {
        $('#btnBack').click(function () {
            window.history.back();
        });
    });
</script>

<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                <?php echo (isset($param["track"]) && $param["track"] == "1") ? "ติดตามคำสั่งซื้อ" : "รายการสินค้า"; ?> <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/>


        <?php
        if (isset($param["track"]) && $param["track"] == "1") {
            ?>  
            <div class="register-box-thankyou">
                <div class="box-info" style="text-align:left;">
                    <div style="font-size:16px;"><b>สถานะคำสั่งซื้อปัจจุบัน : </b><?php echo $param["order_status_detail"]; ?></div>
                    <?php if($param['package_no'] != ""){ ?>
                    <br/>
                    <div style="font-size:16px;"><b>เลขที่พัสดุที่จัดส่ง : </b><?php echo $param['package_no'];?></div>
                    <?php } ?>
                        <?php if ($param["order_status"] == "5") { ?>
                        <br/>
                        <div>ตรวจสอบสถานะจัดส่งพัสดุได้โดย <a href="http://track.thailandpost.co.th/tracking/default.aspx" target="_blank">คลิกที่นี่</a></div>
                    <?php } ?>
                    
                </div>
            </div>
            <br/><br/>
        <?php } ?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" class="txt_bold">จำนวนสินค้าในตะกร้า : <span class="txt_green"><?php echo $param['count_item']; ?></span> รายการ</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><div class="cart-table">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr class="txt_bold bg-gray">
                                <td align="center">รูปภาพสินค้า</td>
                                <td width="200" align="center">ชื่อสินค้า</td>
                                <td width="100" align="center">ราคา/หน่วย</td>
                                <td width="100" align="center">จำนวน</td>
                                <td width="100" align="center">รวมเป็นราคา</td>
                            </tr>
                            <?php
                            $grand_total = 0;
                            $shipping_fee = $param["shipping_fee"];

                            foreach ($data as $row) {
                                $grand_total += (ceil($row->price) * ceil($row->amount));
                                ?>

                                <tr>
                                    <td align="center">
                                        <input type="hidden" class='row_id' value="<?php echo $row->id; ?>" />
                                        <input type="hidden" class='product_id' id='product_id_<?php echo $row->id; ?>' value="<?php echo $row->product_id; ?>" />
                                        <?php
                                        $file = Yii::app()->request->baseUrl . '/images/product/' . $row->Product['pic1'];
                                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $row->Product['pic1']) && $row->Product['pic1'] != "") {
                                            ?>
                                            <img src="<?php echo $file; ?>" height="75" width="93"  />
                                            <?php
                                        } else {
                                            ?>
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
                                            <?php
                                        }
                                        ?>

                                    </td>
                                    <td width="200" align="center" class="txt_green txt_bold">
                                        <?php
                                        echo Yii::text_lang(array(
                                            $row->Product['product_name_1'],
                                            $row->Product['product_name_2'],
                                            $row->Product['product_name_3'],
                                            $row->Product['product_name_4'],
                                            $row->Product['product_name_5']));
                                        ?>
                                    </td>
                                    <td width="100" align="center" class="txt_bold"><span id='price_<?php echo $row->id; ?>'><?php echo number_format(Yii::currencyAmount($row->price), 2, ".", ","); ?></span></td>
                                    <td width="100" align="center"><input name="amount_<?php echo $row->id; ?>" type="text" readonly="readonly" style="border:none;" id="amount_<?php echo $row->id; ?>" size="2"  value="<?php echo $row->amount; ?>" />
                                    </td>
                                    <td width="100" align="center" class="txt_bold"><span id='total_<?php echo $row->id; ?>' class="sub_total"><?php
                                            echo number_format((Yii::currencyAmount($row->price) * $row->amount), 2, ".", ",");
                                            ?></span></td>

                                </tr>
                                <?php
                                $summary_total = $grand_total + $shipping_fee;
                                if ($grand_total == 0)
                                    $summary_total = 0;
                            }
                            ?>
                            <tr class="txt_bold bg-gray">
                                <td style="text-align:left;">น้ำหนักสินค้าที่จัดส่ง </td>
                                <td colspan="4" style="text-align:left;"><?php echo $param['total_weight']; ?> กก.</td>
                            </tr>
                            <tr class="txt_bold bg-gray">
                                <td style="text-align:left;">รูปแบบการจัดส่ง </td>
                                <td colspan="4" style="text-align:left;">
                                    <?php
                                    switch ($param["shiptype"]) {
                                        case "in_reg":echo "จัดส่งพัสดุแบบลงทะเบียน";
                                            break;
                                        case "in_ems":echo "จัดส่งพัสดุแบบ EMS";
                                            break;
                                        case "in_gen":echo "จัดส่งพัสดุแบบธรรมดา";
                                            break;
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="400" align="center"><table width="80%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="50%" align="center"></td>
                                        <td width="50%" align="center"></td>
                                    </tr>
                                </table></td>
                            <td width="20" valign="top">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="">
                                        <td width="60%" height="30" class="txt_bold hline">รวมยอดสั่งซื้อ</td>
                                        <td width="10" height="30" class="txt_bold hline">|</td>
                                        <td height="30" align="right" class="txt_price hline"><span id="grand_total"><?php echo number_format(Yii::currencyAmount($grand_total), 2, ".", ","); ?></span> <?php echo Yii::currencySign(); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="60%" height="30" class="txt_bold hline">ค่าจัดส่ง</td>
                                        <td width="10" height="30" class="txt_bold hline">|</td>
                                        <td height="30" align="right" class="txt_price hline"><span id="shipping_fee"><?php echo number_format(Yii::currencyAmount($shipping_fee), 2, ".", ","); ?></span> <?php echo Yii::currencySign(); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="60%" height="30" class="txt_bold dhline">รวมราคาสุทธิ</td>
                                        <td width="10" height="30" class="txt_bold dhline">|</td>
                                        <td height="30" align="right" class="txt_price dhline"><span id="net_total"><?php echo number_format(Yii::currencyAmount($summary_total), 2, ".", ","); ?></span> <?php echo Yii::currencySign(); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan='3'>&nbsp;</td>
                                    </tr>                                
                                    <tr>
                                        <td colspan="3"><div style='height:1px;background:#dddddd;'></div></td>
                                    </tr>
                                    <tr>
                                        <td colspan='3'>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="60%" height="30" class="txt_bold dhline">จำนวนแต้ม</td>
                                        <td width="10" height="30" class="txt_bold dhline">|</td>
                                        <td height="30" align="right" class="txt_price dhline"><span id="net_total"><?php echo $param["score"]; ?></span></td>
                                    </tr> 
                                </table></td>
                        </tr>
                    </table>
                    <div>
                        <br/><br/>
                        <u>หมายเหตุ : </u>
                        <div>จำนวนแต้ม 1 แต้ม ต่อการซื้อทุกๆ <?php echo $param["score_rate"]; ?> บาท</div>
                        <div>รายการสินค้าจัดส่งฟรี ได้แก่ ของสด สั่งตั้งแต่ 2000 ขึ้นไปและรายการสินค้าสำหรับร้านอาหาร สั่งตั้งแต่ 2000 ขึ้นไป</div>
                        <div>กรณีน้ำหนักสินค้าจัดส่งเกิน 2 กก. ให้เลือกรูปแบบการจัดส่งแบบพัสดุ EMS หรือแบบพัสดุธรรมดา</div>
                    </div>
                </td>
            </tr>
        </table>



        <br/><br/>
        <button id="btnBack" onclick="return false;">« ย้อนกลับ</button>

    </div>
</div>