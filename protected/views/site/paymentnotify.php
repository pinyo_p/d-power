
<link href="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.11.2.custom/jquery-ui.css" rel="stylesheet" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.11.2.custom/jquery-ui.js"></script>


<script language="javascript">

<?php
if (isset($param['alert']) && $param['alert'] != '')
    echo "alert('" . $param['alert'] . "');\n";
?>

</script>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'PaymentNofity',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?> 

<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ข้อมูลการโอนเงิน <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/>

        <fieldset class="register-box-group">
            <div class="register-box">

                <div class="register-box-row">
                    <div class="register-box-left">
                        เลขที่ใบสั่งซื้อ <span>*</span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <select id="ddlOrder" name="PaymentNotify[order_id]">
                            <?php
                            $mMemberOrder = MemberOrder::model()->findAll("member_id=:member_id and status in ('0','1')", array(':member_id' => $param['userid']));
                            foreach ($mMemberOrder as $row) {
                                $orderID = $row["id"];

                                // ราคาซื้อ
                                $total = Yii::app()->db->createCommand("select sum(amount*price) as total from tb_cart where status='1' and order_id='" . $orderID . "' ")->queryScalar();
                                // ค่าจัดส่ง
                                $shipFee = Yii::app()->db->createCommand("select shipping_fee from tb_cart_info where order_id='" . $orderID . "' ")->queryScalar();

                                $totalAmount = $total + $shipFee;

                                echo "<option value='" . $row["id"] . "'>" . substr("000000000" . $row["id"], -9) . "&nbsp;&nbsp;(ยอดซื้อ " . number_format(Yii::currencyAmount($totalAmount), 2, ".", ",") . " บาท)</option>";
                            }
                            ?> 
                        </select>
                        <span class="form-error-info"><?php echo $form->error($model, 'order_id'); ?></span>
                    </div>
                </div>      

                <div class="register-box-row">
                    <div class="register-box-left">
                        ยอดเงินที่โอน <span>*</span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'transfer_amount', array('style' => 'width:200px')); ?> <span class="form-error-info"><?php echo $form->error($model, 'transfer_amount'); ?></span>
                    </div>
                </div>                
                <div class="register-box-row">
                    <div class="register-box-left">
                        วันที่โอนเงิน <span>*</span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
                            'name' => 'PaymentNotify[transfer_date]',
                            'value' => $model->transfer_date,
                            'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat' => 'yy-mm-dd',
                                'altFormat' => 'yy-mm-dd',
                                'changeMonth' => 'true',
                                'changeYear' => 'true',
                                'showOn' => "both",
                                'yearRange' => "'" . date("Y") - 70 . ':' . date("Y") - 10 . "'",
                                'buttonImage' => Yii::app()->request->baseUrl . "/images/carlendar.png",
                                'buttonImageOnly' => "true",
                            ),
                            'htmlOptions' => array(
                                'style' => 'height:15px;width:100px;'
                            ),
                                )
                        );
                        ?> <span class="form-error-info"><?php echo $form->error($model, 'transfer_date'); ?></span>
                    </div>
                </div>  
                <div class="register-box-row">
                    <div class="register-box-left">
                        เวลา (โดยประมาณ) <span></span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'transfer_time', array('style' => 'width:100px')); ?> <span class="form-error-info"><?php echo $form->error($model, 'transfer_time'); ?></span>
                    </div>
                </div>  
                <div class="register-box-row">
                    <div class="register-box-left">
                        โอนเข้าบัญชี <span></span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php
                        echo $form->dropDownList($model, 'bank_id', CHtml::listData(Bank::model()->findAllbySql("select id,concat(bank_name,' - ',acc_type,' (เลขที่บัญชี ',acc_no,')')as bank_name from tb_bank"), 'id', 'bank_name'), array('empty' => null,
                        ));
                        ?>
                    </div>
                </div>  
                <div class="register-box-row">
                    <div class="register-box-left">
                        ธนาคาร/สาขาต้นทางที่โอน <span></span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'member_bank', array('style' => 'width:350px')); ?>                     
                    </div>
                </div> 
                <div class="register-box-row">
                    <div class="register-box-left">
                        หลักฐาน/รูปใบสลิป <span></span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <input type="file" name="file1" id="file1" />             
                    </div>
                </div>  
                <div class="register-box-row">
                    <div class="register-box-left">
                        หมายเหตุ <span></span>
                    </div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textArea($model, 'remark', array('rows' => 5, 'cols' => 60)); ?> <span class="form-error-info"><?php echo $form->error($model, 'remark'); ?></span>         
                    </div>
                </div>  
            </div>
        </fieldset>

        <br/>


        <div class="register-box-row">
            <div class="register-box-left"></div>
            <div class="register-box-mid"></div>
            <div class="register-box-right">
                <div class = "register-button">
                    <button id="btnRegister" name="btnRegister" style="width:120px;">แจ้งการชำระเงิน</button>

                </div>
            </div>
        </div>

    </div>
</div>





<?php $this->endWidget(); ?>