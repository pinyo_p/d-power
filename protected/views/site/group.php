   <script language="javascript">
   	function gotoPage(objPage)
	{
		$("#goto_page").val(objPage);
		$("#productlist-form").submit();
	}
   </script>
   <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'productlist-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'method'=>'post',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <input type="hidden" name="group_id" value="<?php echo $param['group_id'];?>" />
   
<div class="product-box">

<div class="cata-box">
<div class="cata-box-row">
<div class="cata-title">
<div class="cata-title-txt">
All Results
</div>
</div>

<div class="cata-menu">
<ul>
	<?php
              		foreach($this->category as $cate){
              			?>
              				<li><a href="<?php echo Yii::app()->createUrl("site/group",array("id"=>$cate->id)); ?>"><?php echo $cate->group_2;?></a></li>
              			<?php
					}
              	?>
</ul>
</div>
</div>

<div class="cata-box-row">
<div class="cata-title">
<div class="cata-title-txt">
สินค้ายอดนิยม
</div>
</div>

<div class="recom-box">
<div class="recom-box-row">
<div class="recom-img">
<img src="images/06.jpg" alt=""/> </div>
<div class="recom-title">
Dpower แบตเตอรี่สำรอง 20000mAh 
รุ่น I200 (สีขาว)
</div>
<div class="recom-price">
795 บาท
</div>
</div>

<div class="recom-box-row">
<div class="recom-img">
<img src="images/06.jpg" alt=""/> </div>
<div class="recom-title">
Dpower แบตเตอรี่สำรอง 20000mAh 
รุ่น I200 (สีขาว)
</div>
<div class="recom-price">
795 บาท
</div>
</div>

<div class="recom-box-row">
<div class="recom-img">
<img src="images/06.jpg" alt=""/> </div>
<div class="recom-title">
Dpower แบตเตอรี่สำรอง 20000mAh 
รุ่น I200 (สีขาว)
</div>
<div class="recom-price">
795 บาท
</div>
</div>

<div class="recom-box-row">
<div class="recom-img">
<img src="images/06.jpg" alt=""/> </div>
<div class="recom-title">
Dpower แบตเตอรี่สำรอง 20000mAh 
รุ่น I200 (สีขาว)
</div>
<div class="recom-price">
795 บาท
</div>
</div>

<div class="recom-box-row">
<div class="recom-img">
<img src="images/06.jpg" alt=""/> </div>
<div class="recom-title">
Dpower แบตเตอรี่สำรอง 20000mAh 
รุ่น I200 (สีขาว)
</div>
<div class="recom-price">
795 บาท
</div>
</div>

</div>
</div>
</div>

<div class="product-item">
<div class="product-sort">
<strong>แสดงรายการสินค้าตาม</strong> จัดเรียงตาม:
<select>
<option>ราคา</option>
</select>

</div>

<div class="product-list">
<ul>
<?php
foreach($data as $row){
	?>
	<li onclick="location.href='<?php echo Yii::app()->createUrl("site/productdetail",array("id"=>$row->id)); ?>';">
		<div class="recom-box-row">
			<div class="recom-img">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/product/<?php echo $row->pic1;?>" alt=""/> 
			</div>
			<div class="recom-title">
				<?php echo $row->product_name_1;?> 
			</div>
			<div class="recom-price">
				<?php echo number_format($row->price1,2,".",",");?> 
			</div>
		</div>
	</li>

	<?php
}
?>



</ul>
</div>

<div class="page-bar">
<div class="page-total">
สินค้าทั้งหมด  <?php echo $param['row_amount'];?> รายการ
</div>
 <?php 
 $page =  $param['page']+1;

								  if($param['max_page']>1){
									  ?>
									  <input name="page" type="hidden" id="goto_page" size="2" />
                     <div class="page-num">
					<ul>
					<li onclick="gotoPage('<?php echo ($page-1);?>')"></li>
					<?php
					$start_page = ($page>2?$page-2:1);
					 $end_page = ($param['max_page']>($page+2)?$page+2:$param['max_page']);
					 if($end_page<=5)
					 	$end_page = 5;
					else if(($end_page - $page)<5)
						$start_page = ($end_page -4);		
					if($end_page>$param['max_page'])
						$end_page = $param['max_page'];	
					 for($i=$start_page-1;$i<ceil($end_page);$i++)
					 {
					 	//<li>1</li>
					 	$class = "";
						 if(($i+1)==$page)
						  	$class = ' id="select"';
						else
							$class = "  onclick=\"gotoPage('" . ($i+1) ."')\" ";
					 	?>
					 	<li <?php echo $class;?>><?php echo ($i+1);?></li>
					 	<?php
					 }
					?>
					<?php
					if(($page+1) > $param['max_page']){
						$page--;
						?>
						<?php
					}
					
					?>
					
					<li onclick="gotoPage('<?php echo ($page+1);?>')"></li>
					</ul>
					</div>
                   <?php
								  }
								  ?>


</div>

</div>

</div>

                            <?php $this->endWidget(); ?>