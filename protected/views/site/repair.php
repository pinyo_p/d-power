  <?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'Repair';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>        
            <table width="100%"><tr><td class="main-column-bg-index ">
            <div class="main-column-content-index">
             

             
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="main-content-header">แจ้งปัญหาการซ่อม</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="center"><h3>แจ้งปัญหาการซ่อม</h3></td>
                </tr>
                <tr>
                  <td class="add_data">
                    <?php echo Yii::d($content->content_en,$content->content_th);?>
                  
                  <p><br />
                  </p>
                      <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
                      
                      <tr style="position:absolute; visibility:hidden" >
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ประเภทการลงทะเบียน : <span class="txt_red">* </span></td>
                          <td valign="top">
                          <?php echo $form->dropDownList($model,'service_type', array('1'=>'Technical Support','2'=>'Repair','3'=>'Training'), array('empty'=>'เลือกประเภท',																																												  				 
	  )); ?>
                          </td>
                        </tr>
                      <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">เรื่องที่ต้องการแจ้งปัญหา : <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'subject'); ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">รหัสสินค้าที่ต้องการแจ้งปัญหา : <span class="txt_red">* </span></td>
                          <td valign="top"><p style="display:none;">
                            <select name="select6" id="select6">
                              <option>-เลือกแบรนด์สินค้า-</option>
                            </select>
                            &nbsp;
                            <select name="select7" id="select7">
                              <option>-เลือกชื่อสินค้า-</option>
                            </select>
                            
                          </p>
                          <?php echo $form->textField($model,'product_code'); ?>
                          </td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ชื่อบริษัทลูกค้า : <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'company_name'); ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ชื่อ-นามสกุล ลูกค้า : <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'full_name'); ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">อีเมล์ : <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'email'); ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">เบอร์โทรศัพท์ติดต่อกลับ : <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'phone_no'); ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">หมายเหตุ : <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textArea($model, 'detail', array('row' => 10,'col'=>200,'style'=>'width:300px;height:100px;')); ?></td>
                        </tr>
                      
                       
                      </table>
                      <br /><br />
                     <table style="margin-left:10px"> <tr>
                          <td width="250" align="center" valign="top" bgcolor="#f8f8f8" class="txt_bold" colspan="7">รายการซ่อม </td>
                        
                      </tr>
                          <tr><th  style="text-align:center">No.1</th><th style="text-align:center">Product</th><th style="text-align:center">Model</th><th style="text-align:center">S/N</th><th style="text-align:center">อาการเสีย</th><th style="text-align:center">จำนวน</th>
                          <th style="text-align:center">ภาพประกอบ</th>
                          </tr>
                          <?php 
						  for($i=1;$i<=10;$i++)
						  {
							  ?>
 <tr>
                       <td>
                       <?php echo $i;?>
                       </td>
                       <td>
                       <input type="text" id='fix_product_<?php echo $i;?>' name='fix_product[]' style="width:100px" />
                       </td>
                       <td>
                       <input type="text" id='fix_model_<?php echo $i;?>' name='fix_model[]' style="width:50px" />
                       </td>
                        <td>
                       <input type="text" id='fix_serial_<?php echo $i;?>' name='fix_serial[]'  style="width:100px"/>
                       </td>
                        <td>
                       <input type="text" id='fix_detail_<?php echo $i;?>' name='fix_detail[]' />
                       </td>
                       <td>
                       <input type="text" id='fix_amount_<?php echo $i;?>' name='fix_amount[]' style="width:50px" />
                       </td>
                       <td>
                       <input type="file" id='fix_file_<?php echo $i;?>' name='fix_file[]'  style="width:150px"/>
                       </td>
                       </tr>
              
                       <?php
						  }
						  ?>
                      </table>
                    </td>
                </tr>
                <tr>
                  <td align="center"><br /></td>
                </tr>
                <tr>
                  <td align="center"><br /></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="center">
                   <?php if(CCaptcha::checkRequirements()): ?>
                          <center>
	<div>
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?><br />
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
        <div class="form">
		<?php echo $form->error($model,'verifyCode'); ?>
        </div>
	</div>
    </center>
	<?php endif; ?>
                  </td>
                </tr>
                <tr>
                  <td align="center">&nbsp;</td>
                </tr>
                <tr>
                  <td align="center"><input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/send.png" width="84" height="27" /></td>
                </tr>
                <tr>
                  <td align="center"><br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br /></td>
                </tr>
              </table>
            
              
                        </div>
                </td>
                </tr>
                </table>    
                 <?php $this->endWidget(); ?>