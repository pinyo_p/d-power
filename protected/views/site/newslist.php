<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ข่าวสาร <span class="txt-org">และโปรโมชั่น</span>
            </div>
        </div>

        <div class="news-content-box-detail">
            <ul>
                <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $news,
                    'itemView' => '_newslist',
                    'summaryText'=>'',
                    'pager' => array(
                        'header' => '',
                    ),
                ));
                ?>          
            </ul>
        </div>

    </div>
    <!-- InstanceEndEditable -->
</div>



