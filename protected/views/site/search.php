<link href="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.11.2.custom/jquery-ui.css" rel="stylesheet" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.11.2.custom/jquery-ui.js"></script>


<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                Advance <span class="txt-org">Search</span>
            </div>
        </div>

        <div class="news-content-box-detail">
            <br/>

            <div class="search-box">
                <form action="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/search" method="get" id="frm_search2" name="frm_search2">
                    <div class="search-box-row">
                        <div class="search-box-left">ประเภท</div>
                        <div class="search-box-mid">:</div>
                        <div class="search-box-right">
                            <select name="search_type" style="width:100%;
                                    ">
                                <option value="">-- ทั้งหมด --</option>
                                <option value="Product" <?php echo (isset($_GET['search_type']) && $_GET['search_type'] == "Product")? "selected='selected'":""; ?> >สินค้า (Product)</option>
                                <option value="News" <?php echo (isset($_GET['search_type']) && $_GET['search_type'] == "News")? "selected='selected'":""; ?> >ข่าวสาร (News & Promotion)</option>
                            </select>
                        </div>
                    </div>
                    <div class="search-box-row">
                        <div class="search-box-left">ช่วงเวลา</div>
                        <div class="search-box-mid">:</div>
                        <div class="search-box-right">
                            <input name="start_date" class="datepicker" type="text" placeholder="yyyy-mm-dd" id = "dp1424094366070" value="<?php echo (isset($_GET['start_date']) && $_GET['start_date'] != "")? $_GET["start_date"]:""; ?>">
                            &nbsp;&nbsp;&nbsp;
                            ถึง&nbsp; 
                            <input name = "end_date" class = "datepicker" type = "text" placeholder = "yyyy-mm-dd" id = "dp1424094366071" value="<?php echo (isset($_GET['end_date']) && $_GET['end_date'] != "")? $_GET["end_date"]:""; ?>"> 
                        </div>
                    </div>
                    <div class = "search-box-row">
                        <div class = "search-box-left">คำค้น</div>
                        <div class = "search-box-mid">:</div>


                        <div class = "search-box-right">
                            <input name = "keyword" type = "text" style = "width:70%;"
                                   placeholder = "ใส่คำค้นหา"
                                   value="<?php echo (isset($_GET['keyword']) && $_GET['keyword'] != "")? $_GET["keyword"]:""; ?>">
                            <div class = "search-button">
                                <button onclick = "searchit2()">ค้นหา</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                
<!--                                <button onclick = "window.location.href = '<?php //echo Yii::app()->request->baseUrl; ?>/index.php/site/search/'">ยกเลิก</button>-->
                            </div>
                        </div>

                    </div>
                </form>
            </div>

            <div class = "search-resualt">
                <div class = "search-resualt-txt">
                    ผลการค้นหา : <?php echo (isset($_GET) && count($_GET) > 0)? $total : "0"; ?> รายการ
                </div>                
            </div>
            <div search-resualt-table>
                <?php        
                //if ((isset($_POST) && count($_POST) > 0) || $_GET["page"] != "") {
                if ((isset($_GET["search_type"]) && count($_GET) > 0)) {
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider' => $content,
                        'itemView' => '_search',
                        'summaryText' => '',
                        'pager' => array(
                            'header' => '',
                        ),
                    ));  
                }
                ?> 
            </div>

        </div>

    </div>
</div>


<script type = "text/javascript">
    $(document).ready(function () {
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "<?php echo Yii::app()->request->baseUrl; ?>/images/carlendar.png",
            buttonImageOnly: true,
            buttonText: "Select date"
        });
    });
    
    function searchi2t() {
        $("#frm_search2").submit();
    }
</script>