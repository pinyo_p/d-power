<script language="javascript">
    $(document).ready(function(){
       $('#btnSend').click(function(){
           var email = $('#Member_email').val().trim();
           if(email == ""){
               alert("คุณยังไม่ได้กรอก Email");
               $('#Member_email').focus();
               return false;
           }
           $("#login-form").submit();
       });
    });
</script>

<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ลืมรหัสผ่าน <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/>

        <fieldset class="register-box-group">
            <legend>กรุณาใส่ E-mail address ที่คุณลงทะเบียนไว้</legend>
            <div class="register-box">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                ));
                ?>

                <div class="register-box-row">
                    <div class="register-box-left">E-Mail</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'email'); ?>
&nbsp;&nbsp;
  
                        <button  id="btnSend" name="btnSend" onclick="return false;">Send</button>
                    </div>
                </div>
                
                <?php $this->endWidget(); ?>
                

                <div style="padding:20px;text-align:center;">ระบบจะทำการส่งรหัสผ่านไปยัง E-mail address ของคุณ</div>
            </div>
        </fieldset>


    </div>
</div>
