   <script language="javascript">
   	function gotoPage(objPage)
	{
		$("#page").val(objPage);
		$("#productlist-form").submit();
	}
   </script>
   <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'productlist-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'method'=>'post',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <input type="hidden" name="group_id" value="<?php echo $param['group_id'];?>" />
    <input type="hidden" name="brand_id" value="<?php echo $param['brand_id'];?>" />

            <table width="100%">
            
            <tr>
            
            <td class="main-column-bg-index ">
            <div class="main-column-content-index">
              
              
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              
                <tr>
                  <td>
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  
                  
                    <tr>
                      <td class="main-content-header">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>สินค้าหมวด <?php
						  	if(count($group)>0)
								echo Yii::d($group->group_en,$group->group_th);
							else if(isset($_GET['type']) && strtolower($_GET['type'])=="promotion")
								echo "Promotion";
							else
								echo 'ทุกหมวด';
                          ?></td>
                          <td align="right" class="moreproduct">&nbsp;</td>
                        </tr>
                      </table>
                      </td>
                    </tr>
                    <tr>
                      <td align="center">
                      <div class="product_box">
                        <?php
				  foreach($data as $row){
				  ?>
                    <div class="box1">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top" class="boxtxt">
                          <p class="txt_green">
                          <?php
						 echo str_replace(",",", ",Yii::d($row->product_name_en,$row->product_name_th));
						  ?>
                          </p>
                            <p class="txt_center">
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ProductDetail/<?php echo $row->id;?>" class="link-more" target="_blank">
                            <?php
			  $file = Yii::app()->request->baseUrl . '/images/product/' . $row->pic1;
			  if(file_exists(Yii::app()->basePath . '/../images/product/' .  $row->pic1) && $row->pic1 != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="73" />
                  <?php
			  }else{
			  ?>
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
              <?php
			  }
			  ?>
              </a>
                            </p>
                            
                            </td>
                        </tr>
                        <tr>
                          <td class="txt_price txt_center">ราคา <?php echo number_format($row->price1,0,".",',');?> บาท</td>
                        </tr>
                        <tr>
                          <td class="bg-gray">
                          
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ProductDetail/<?php echo $row->id;?>" class="link-more" target="_blank">ดูรายละเอียด</a></td>
                              <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/AddToCart/<?php echo $row->id;?>/?keepThis=true&amp;TB_iframe=true&amp;height=250&amp;width=350" title="" class="thickbox link-cart">หยิบใส่ตะกร้า</a></td>
                            </tr>
                          </table>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <?php
				  }
				  ?>
                      </div>
                      
                      </td>
                    </tr>
                   
                    <tr>
                      <td><br />
                        &nbsp;</td>
                    </tr>
                    <tr>
                      <td>
                      
                       <?php

								  if($param['max_page']>1){
									  ?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                           <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page =  $param['page']+1;?> / <?php echo $param['max_page'];?></td>
                          <td>
                          <div class="nav_page">
                             <ul>
                                      <li onclick="gotoPage('<?php echo $page-1;?>')">&laquo;</li>
                                      <?php 
$start_page = ($page>2?$page-2:1);
									 $end_page = ($param['max_page']>($page+2)?$page+2:$param['max_page']);
									 if($end_page<=5)
									 	$end_page = 5;
									else if(($end_page - $page)<5)
										$start_page = ($end_page -4);		
									if($end_page>$param['max_page'])
										$end_page = $param['max_page'];	
									  for($i=$start_page-1;$i<ceil($end_page);$i++)
									  {
										  $class = "";
										  if(($i+1)==$page)
										  	$class = '  class="navselect"';
										else
											$class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
										  ?>
                                          <li <?php echo $class;?>><?php echo $i+1;?></li>
                                          <?php
									  }
									  ?>
                                      <li onclick="gotoPage('<?php echo $page+1;?>')">&raquo;</li>
                                    </ul>
                          </div>
                          
                          </td>
                          <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                          <table width="200"  style="border:none;">
                          <tr>
                          
                          <td style="border:none;">
                          <span class="txt_pink">ไปหน้าที่ :</span></td>
                          
                          <td style="border:none;">
                                    <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                    <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png" width="39" height="22" /></a>
                                    
                                    </td>
                                    
                        </tr>
                      </table>
                      </tr>
                      </table>
                      <?php
								  }
								  ?>
                      </td>
                    </tr>
                  </table>
                  
                  </td>
                </tr>
              </table>
            
              
                        
                
                
                        </div>
                </td>
                </tr>
                </table>    
                            <?php $this->endWidget(); ?>