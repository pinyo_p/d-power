<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" class="main-column-bg"><div class="main-column-content">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="main-content-header"><?php echo Yii::d('KPT Branch','สาขาของ KPT Group');?></td>
                        </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>
                        
                        
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    
                      <?php 
					  foreach($data as $row)
					  {
						  ?>
                          <tr>
                      <td class="bor-branch">
                      
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="center">
                              <?php
			  $file = Yii::app()->request->baseUrl . '/images/branch/' . $row->main_image;
			  if(file_exists(Yii::app()->basePath . '/../images/branch/' .  $row->main_image) && $row->main_image != ""){
				  ?>
                   <img src="<?php echo $file; ?>"  height="182"/>
                  <?php
			  }
			  ?>
                              </td>
                              <td align="center"><p><?php
			  $file = Yii::app()->request->baseUrl . '/images/branch/' . $row->map_image;
			  if(file_exists(Yii::app()->basePath . '/../images/branch/' .  $row->map_image) && $row->map_image != ""){
				  ?>
                   <img src="<?php echo $file; ?>"  width="155" height="110"/>
                  <?php
			  }
			  ?></p>
                                <p><a href="<?php echo $file;?>" target="_blank" class="link_red">ดาวน์โหลดแผนที่</a></p></td>
                              <td align="center"><p><img src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $row->latitude . ',' . $row->longitude;?>&zoom=10&size=155x110&maptype=roadmap&markers=color:blue%7Clabel:S%7C<?php echo $row->latitude . ',' . $row->longitude;?>&sensor=false" width="155" height="110" /></p>
                                <p><a href="https://maps.google.com/?q=<?php echo $row->latitude;?>,<?php echo $row->longitude;?>" target="_blank" class="link_red">ดูแผนที่่่ <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/googlemap.png" width="24" height="24" /> Google Map</a></p></td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><p class="txt_green txt_bold"><?php echo Yii::d($row->name_en,$row->name_th);?></p>
                            <p><?php echo Yii::d($row->address_en,$row->address_th);?>
                            </p>
                            <p>โทรศัพท์ : <?php echo $row->phone_no;?>   โทรสาร  : <?php echo $row->fax_no;?>      อีเมลล์ : <?php echo $row->email;?></p></td>
                        </tr>
                      </table>
                      </td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <?php }?>
                  </table>
                        
                        </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
                </div></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>