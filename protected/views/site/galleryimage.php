<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                Photo <span class="txt-org">Gallery</span>
            </div>
        </div>

        <div class="news-content-box-detail">
            <ul>
                <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $gallerys,
                    'itemView' => '_galleryimage',
                    'summaryText'=>'',
                    'pager' => array(
                        'header' => '',
                    ),
                ));
                ?>          
            </ul>
        </div>

    </div>
    <!-- InstanceEndEditable -->
</div>