   <script language="javascript">
   function submitme()
   {
	   $("#jobs").submit();
   }
   </script>
            <table width="100%"><tr><td class="main-column-bg-index ">
            <div class="main-column-content-index">
        <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jobs',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<style type="text/css">
.errorMessage{
	color:red;
}
</style>     

            
              
              
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="main-content-header">กรอกใบสมัครงานออนไลน์</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>
                  <table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
		            <tr>
		              <td class="add_data">
					  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		                <tr>
		                  <td width="200" valign="top" bgcolor="#f8f8f8" class="txt_bold" align="right">ชื่อ - นามสกุล : <span class="txt_red">* </span></td>
		                  <td valign="top"><?php echo $form->textField($model,'fullname'); ?><?php echo $form->error($model,'fullname'); ?></td>
		                  </tr>
		                <tr>
		                  <td width="200" valign="top" bgcolor="#f8f8f8" class="txt_bold" align="right">อายุ : <span class="txt_red">* </span></td>
		                  <td valign="top"><?php echo $form->textField($model,'age'); ?><?php echo $form->error($model,'age'); ?></td>
		                  </tr>
		                <tr>
		                  <td width="200" valign="top" bgcolor="#f8f8f8" class="txt_bold" align="right">เพศ : <span class="txt_red">* </span></td>
		                  <td valign="top">
                          <?php echo $form->radioButtonList($model,'sex',array('1'=>'ชาย','0'=>'หญิง',),array('separator'=>''));?><?php echo $form->error($model,'sex'); ?>
		                    </td>
		                  </tr>
		                <tr>
		                  <td width="200" valign="top" bgcolor="#f8f8f8" class="txt_bold" align="right">อีเมล์ : <span class="txt_red">* </span></td>
		                  <td valign="top"><?php echo $form->textField($model,'email'); ?><?php echo $form->error($model,'email'); ?></td>
		                  </tr>
		                <tr>
		                  <td width="200" valign="top" bgcolor="#f8f8f8" class="txt_bold" align="right">เบอร์โทรศัพท์ : <span class="txt_red">* </span></td>
		                  <td valign="top"><?php echo $form->textField($model,'phoneno'); ?><?php echo $form->error($model,'phoneno'); ?></td>
		                  </tr>
		                <tr>
		                  <td width="200" valign="top" bgcolor="#f8f8f8" align="right"><span class="txt_bold">ตำแหน่งที่ต้องการสมัคร : </span><span class="txt_red">*</span><br />
		                    (เพื่อไว้พิจารณาเรียกสัมภาษณ์) </td>
		                  <td valign="top">
                          <?php echo $form->dropDownList($model,'jobid', CHtml::listData(Joblist::model()->findAll(array('order' => 'position ASC')), 'id', 'position'), array('empty'=>'เลือกตำแหน่ง',	 )); ?><?php echo $form->error($model,'jobid'); ?></td>
		                  </tr>
		                <tr>
		                  <td width="200" valign="top" bgcolor="#f8f8f8" align="right"><span class="txt_bold">ส่งข้อมูลประวัติ : </span><span class="txt_red">*</span><br />
		                    (Upload Resume File) </td>
		                  <td valign="top"><p>
		                    <input type="file" name="resume" id="fileField" /><?php echo $form->error($model,'resume'); ?>
		                  </p>
		                    <p>* ไฟล์ไม่เกิน 3 MB </p></td>
		                  </tr>
		                <tr>
		                  <td width="200" valign="top" bgcolor="#f8f8f8" align="right"><span class="txt_bold">ส่งรูปภาพผู้สมัคร : </span><br />
		                    (Upload Picture File) </td>
		                  <td valign="top"><p>
		                    <input type="file" name="picture" id="fileField2" />
		                  </p>
		                    <p>* ไฟล์ไม่เกิน 3 MB </p></td>
		                  </tr>
		                </table>
						</td>
		              </tr>
		            <tr>
		              <td align="center" colspan="2">
                      <br /><br />
                      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		                <tr>
		                  <td>&nbsp;</td>
		                  <td>
                          
                          <?php if(CCaptcha::checkRequirements()): ?>
                          <center>
	<div>
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?><br />
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
        <div class="form">
		<?php echo $form->error($model,'verifyCode'); ?>
        </div>
	</div>
    </center>
	<?php endif; ?>
    
                          </td>
		                  </tr>
		                
		                </table></td>
		              </tr>
		            </table>
		          
                	</td>
                </tr>
                <tr>
                  <td align="center"><br /></td>
                </tr>
               
              
                <tr>
                  <td align="center" valign="top" style="vertical-align:top">
                   <a href='javascript:submitme()'><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_apply.png" width="55" height="26" /></a>&nbsp; <a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/jobsdetail/<?php echo $model->jobid ?>'><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_cancel.png" width="55" height="26" /></a></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="center"><br />
                    
Tel : <?php echo $setting->hr_tel;?>  Fax : <?php echo $setting->hr_fax;?><br />
                    <br />
                    ส่งรายละเอียดประวัติส่วนตัว (resume) มาที่ <b>E-mail : <a href="<?php echo $setting->hr_email;?>"><font color="black"><?php echo $setting->hr_email;?></font></a></b></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table>
            
     	  <?php $this->endWidget(); ?>       
            
              
                        </div>
                </td>
                </tr>
                </table>    