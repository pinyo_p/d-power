<?php
$urlDetail = "";
$type = "";
$imgUrl = "";
$description = Yii::text_lang(array(
            $data["desc1"],
            $data["desc2"],
            $data["desc3"],
            $data["desc4"],
            $data["desc5"]));

if ($data["type"] == "Product") {
    $description = "";
    $urlDetail = Yii::app()->request->baseUrl . "/index.php/site/productdetail/" . $data["id"];
    $type = "สินค้า";
    $imgUrl = Yii::app()->request->baseUrl . "/images/product/" . $data["id"] . "/" . $data["picture"];

    $file = Yii::app()->request->baseUrl . '/images/product/' . $data["id"] . '/' . $data["picture"];
    if (file_exists(Yii::app()->basePath . '/../images/product/' . $data["id"] . '/' . $data["picture"]) && $data["picture"] != "") {
        $imgUrl = Yii::app()->request->baseUrl . "/images/product/" . $data["id"] . "/" . $data["picture"];
    } else {
        $imgUrl = Yii::app()->request->baseUrl . "/images/news_no_image.jpg";
    }
} else {
    $urlDetail = Yii::app()->request->baseUrl . "/index.php/site/NewsDetail/id/" . $data["id"];
    $type = "ข่าวสาร";

    $file = Yii::app()->request->baseUrl . '/images/news/thumb/' . $data["id"] . '.jpg';
    if (file_exists(Yii::app()->basePath . '/../images/news/thumb/' . $data["id"] . '.jpg') && $data["picture"] != "") {
        $imgUrl = Yii::app()->request->baseUrl . "/images/news/thumb/" . $data["id"] . ".jpg";
    } else {
        $imgUrl = Yii::app()->request->baseUrl . "/images/news_no_image.jpg";
    }
}
?>

<div class="search-result-item<?php echo $index % 2 ? '' : '_rowbg' ?>">
    <table style="width:100%;">
        <tr>
            <td valign="top" width="130">
                <img src="<?php echo $imgUrl; ?>" width="130" />
            </td>
            <td width="20"></td>
            <td valign="top">
                <div style="padding-bottom:10px;font-size:16px;color:#086b2a">
                    <?php
                    echo "<span class='color:blue;font-weight:bold;'>[" . $type . "]</span> "
                    . Yii::text_lang(array(
                        $data["name1"],
                        $data["name2"],
                        $data["name3"],
                        $data["name4"],
                        $data["name5"]));
                    ?>
                </div>
                <div>
                    <?php
                    echo $description;
                    ?>
                </div>
                <table style="width:100%;border-collapse:collapse;">
                    <tr style="">
                        <td style="text-align:left;padding-top:10px;"><i><div>Publish Date : <?php echo date_format(date_create($data["create_date"]), 'd-m-Y'); ?></div></i></td>
                        <td style="text-align:right;padding-top:10px;"><div class="search-result-item-more"><a href="<?php echo $urlDetail; ?>">รายละเอียด »</a></div></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>




</div>

