<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                Video <span class="txt-org">Gallery</span>
            </div>
        </div>
        <div class="news-content-box-detail">
            <?php
            $modelGalVideo = new GalleryVideo();
            $dataImgIcon = $modelGalVideo->searchCoverByGalleryID($data["gallery"]->id)->data;
            ?>
            <div style="padding: 20px 0px 10px 0px;">
                <div class="galleryimg-content-box-icon">
                    <?php
                    if (count($dataImgIcon) > 0) {
                        ?>
                        <img src="http://img.youtube.com/vi/<?php echo $dataImgIcon[0]->url ?>/0.jpg" width="50" height="50"  />
                    <?php } else { ?>
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/no_video_gallery_icon.jpg" width="50" height="50" />
                    <?php } ?>
                </div>
                <div class="galleryimg-content-box-title-txt">
                    <?php
                    echo Yii::text_lang(array(
                        $data["gallery"]->name_1,
                        $data["gallery"]->name_2,
                        $data["gallery"]->name_3,
                        $data["gallery"]->name_4,
                        $data["gallery"]->name_5));
                    ?>
                    <div>
                        <?php
                        echo Yii::text_lang(array(
                            $data["gallery"]->description_1,
                            $data["gallery"]->description_2,
                            $data["gallery"]->description_3,
                            $data["gallery"]->description_4,
                            $data["gallery"]->description_5));
                        ?>
                    </div>
                </div>
                <br/>
            </div>

            <?php
            $idx = 0;
            foreach ($data["gallery_video"] as $row) {

                // Get youtube data
                $url = "http://gdata.youtube.com/feeds/api/videos/" . $row->url;
                $doc = new DOMDocument;
                $doc->load($url);
                $title = $doc->getElementsByTagName("title")->item(0)->nodeValue;
                $desc = $doc->getElementsByTagName("content")->item(0)->nodeValue;

                if ($idx == 0) {
                    ?>
                    <div class="galleryvideo-content-box-thumb-firstitem">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/VideoContent/gallery_id/<?php echo $data["gallery"]->id; ?>/id/<?php echo $row->id; ?>">
                            <img src="http://img.youtube.com/vi/<?php echo $row->url ?>/0.jpg" />
                        </a>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/VideoContent/gallery_id/<?php echo $data["gallery"]->id; ?>/id/<?php echo $row->id; ?>">
                        <?php echo $title; ?>
                        </a>
                    </div>
                    <?php
                } else {
                    ?>
                    <div  class="galleryvideo-content-box-thumb-moreitem">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/VideoContent/gallery_id/<?php echo $data["gallery"]->id; ?>/id/<?php echo $row->id; ?>">
                            <img src="http://img.youtube.com/vi/<?php echo $row->url ?>/0.jpg" />
                        </a>
                        <div class="galleryvideo-content-box-thumb-moreitem-title">
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/VideoContent/gallery_id/<?php echo $data["gallery"]->id; ?>/id/<?php echo $row->id; ?>">
                                <?php echo $title; ?>
                            </a>
                        </div>
                    </div>

                    <?php
                }
                $idx++;
            }
            ?>
            <br/><br/>
            <div class="galleryimg-content-box-footer">
                <div class="galleryimg-content-box-footer-publish">
                    <i>Publish Date : <?php echo date_format(date_create($data["gallery"]->publish_date), 'd-m-Y'); ?></i>
                </div>
                <br/>
                <div class="galleryimg-content-box-footer-backbutton" onclick="history.back();">« ย้อนกลับ</div>
            </div>
            
            <br/><br/><br/><br/><br/><br/>
            <div style="text-align:center;">
            <span class='st_facebook_large' displayText='Facebook'></span>
            <span class='st_twitter_large' displayText='Tweet'></span>
            <span class='st_googleplus_large' displayText='Google +'></span>
            <span class='st_linkedin_large' displayText='LinkedIn'></span>
            <span class='st_pinterest_large' displayText='Pinterest'></span>
            <span class='st_email_large' displayText='Email'></span>
            </div>
        </div>
    </div>
    <!-- InstanceEndEditable -->
</div>