<script language="javascript">
    $(document).ready(function(){
       $('#btnTrack').click(function(){
           // validate 
           var orderNo = $('#txtOrderID').val().trim();

           if(orderNo == ""){
               alert('กรุณากรอกหมายเลขคำสั่งซื้อ');
               $('#txtOrderID').focus();
               return;
           }

           $("#login-form").submit();
       }); 
    });
</script>

<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ติดตามคำสั่งซื้อ <span class="txt-org"></span>
            </div>
        </div>
        <br/>
        
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
        ?>
        
        <br/>
        <fieldset class="register-box-group">
            <?php if($params["last_order"] != "0"){ ?>
            <div class="register-box">                
                <div class="register-box-row">
                    <div class="register-box-left">รายการสั่งซื้อสุดท้ายของคุณ</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php
                        echo "<a href='".Yii::app()->request->baseUrl."/index.php/site/historydetail/code/track/id/".$params["last_order"]."'>".substr("000000000".$params["last_order"],-9)."</a>";
                        ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left">หมายเลขคำสั่งซื้อของคุณ</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <input type="text" id="txtOrderID" name="txtOrderID" />
                    </div>
                </div>
            </div>
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left"></div>
                    <div class="register-box-mid"></div>
                    <div class="register-box-right">
                        <button id="btnTrack" name="btnTrack" onclick="return false;">ติดตาม</button>
                    </div>
                </div>
            </div>
        </fieldset>

        <?php $this->endWidget(); ?>


    </div>
</div>




