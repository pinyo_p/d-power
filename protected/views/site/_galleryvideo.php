<?php
$modelGalVideo = new GalleryVideo();
$dataImgIcon = $modelGalVideo->searchCoverByGalleryID($data->id)->data;
$dataImgs = $modelGalVideo->searchByGalleryID($data->id, '5')->data;
?>
<div style="padding: 20px 0px 10px 0px;">
    <div class="galleryimg-content-box-icon">
        <?php
        if (count($dataImgIcon) > 0) {
            ?>
            <img src="http://img.youtube.com/vi/<?php echo $dataImgIcon[0]->url  ?>/0.jpg" width="50" height="50"  />
        <?php } else { ?>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/no_video_gallery_icon.jpg" width="50" height="50" />
        <?php } ?>
    </div>
    <div class="galleryimg-content-box-title-txt">
        <?php
        echo Yii::text_lang(array(
            $data->name_1,
            $data->name_2,
            $data->name_3,
            $data->name_4,
            $data->name_5));
        ?>
        <div>
            <?php
            echo Yii::text_lang(array(
                $data->description_1,
                $data->description_2,
                $data->description_3,
                $data->description_4,
                $data->description_5));
            ?>
        </div>
    </div>
    <br/>
    <?php
    if (count($dataImgs) > 0) {
        ?>
        <div class="galleryvideo-content-box-thumb" >
            <div class="galleryvideo-content-box-thumb-bigitem">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/VideoContent/gallery_id/<?php echo $data->id; ?>/id/<?php echo $dataImgs[0]->id; ?>">
                    <img src="http://img.youtube.com/vi/<?php echo $dataImgs[0]->url  ?>/0.jpg" />
                </a>
            </div>
            <div class="galleryvideo-content-box-thumb-smallitem">
                <?php
                $idx = 0;
                foreach ($dataImgs as $row) {
                    if ($idx > 0) {
                        ?>
                        <div>
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/VideoContent/gallery_id/<?php echo $data->id; ?>/id/<?php echo $row->id; ?>">
                                <img src="http://img.youtube.com/vi/<?php echo $row->url  ?>/0.jpg" />
                            </a>
                        </div>
                        <?php
                    }
                    $idx++;
                }
                ?>   
            </div>
        </div>


        <div class="galleryimg-content-box-footer">
            <div class="galleryimg-content-box-footer-publish">
                <i>Publish Date : <?php echo date_format(date_create($data->publish_date), 'd-m-Y'); ?></i>
            </div>
            <div class="galleryimg-content-box-footer-readmore">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/GalleryVideoDetail/<?php echo $data->id; ?>">อ่านต่อ »</a>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div style="height:1px;border-top:solid 1px #ccc;"></div>
<div style="height:10px;"></div>
