<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ลืมรหัสผ่าน <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/>
        <div class="register-box-thankyou">
            <div class="box-info">
                <div class="register-box-thankyou-info">ระบบได้ทำการส่งข้อมูลรหัสผ่านไปยัง Email เรียบร้อยแล้ว<br/>กรุณาตรวจสอบ Email ของท่าน</div>
                <div class="register-box-thankyou-subinfo"></div>
            </div>
        </div>
    </div>
</div>