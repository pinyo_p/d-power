<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>   

<script type="text/javascript">
    $(document).ready(function () {
        $('#btnSubmit').click(function () {
            // validate
            if ($('#Contact_fullname').val().trim() == '') {
                alert('กรุณากรอก ชื่อ - นามสกุล ');
                $('#Contact_fullname').focus();
                return false;
            }
            if ($('#Contact_company_name').val().trim() == '') {
                alert('กรุณากรอก บริษัท ');
                $('#Contact_company_name').focus();
                return false;
            }
            if ($('#Contact_tel').val().trim() == '') {
                alert('กรุณากรอก เบอร์โทรศัพท์');
                $('#Contact_tel').focus();
                return false;
            }
        });

        $('#btnCancel').click(function () {
            window.history.back();
        });
    });
</script>

<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ขอใบเสนอราคา <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/>


        <fieldset class="register-box-group">
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left">ชื่อ - นามสกุล <span>*</span></div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'fullname', array('style' => 'width:300px')); ?> <?php echo $form->error($model, 'fullname'); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">บริษัท <span>*</span></div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'company_name', array('style' => 'width:300px')); ?>  <?php echo $form->error($model, 'company_name'); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">ตำแหน่งงาน</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'position', array('style' => 'width:300px')); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">เบอร์โทรศัพท์ <span>*</span></div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'tel', array('style' => 'width:300px')); ?> <?php echo $form->error($model, 'tel'); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">โทรสาร</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'fax', array('style' => 'width:300px')); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">อีเมล์</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'email', array('style' => 'width:300px')); ?> <?php echo $form->error($model, 'email'); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">เว็บไซต์</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'website', array('style' => 'width:300px')); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">หัวข้อ</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textField($model, 'subject', array('style' => 'width:300px')); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left" style="vertical-align: top">ที่อยู่</div>
                    <div class="register-box-mid" style="vertical-align: top">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textArea($model, 'address', array('row' => 10, 'col' => 200, 'style' => 'width:400px;height:100px;')); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left" style="vertical-align: top">ข้อความ</div>
                    <div class="register-box-mid" style="vertical-align: top">:</div>
                    <div class="register-box-right">
                        <?php echo $form->textArea($model, 'detail', array('row' => 10, 'col' => 200, 'style' => 'width:400px;height:100px;')); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">Verify Code</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php if (CCaptcha::checkRequirements()): ?>
                            <div>   
                                <div>
                                    <?php $this->widget('CCaptcha'); ?><br />
                                    <?php echo $form->textField($model, 'verifyCode'); ?>
                                </div>
                                <div class="form">
                                    <span style="color:red"><?php echo $form->error($model, 'verifyCode'); ?></span>
                                </div>
                            </div>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </fieldset>

        <br/><br/>

        <center>

            <input id="btnSubmit" type="submit" class="button_1" value="ส่งข้อความ" style="width:100px" />
            &nbsp;
            <button id="btnCancel" onclick="return false;" name="btnCancel" style="width:100px">ยกเลิก</button>

        </center>

    </div>
</div>


<?php $this->endWidget(); ?>