          
            <table width="100%"><tr><td class="main-column-bg-index ">
            <div class="main-column-content-index">
             

              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="main-content-header"><p>แบรนด์สินค้าของเรา</p></td>
                </tr>
                <tr>
                  <td>
                  <?php
				  $brand_banner = Content::model()->find("content_code=:content_code",array(":content_code"=>"BrandLink"));
				  echo $brand_banner->content_th;
				  ?>
                    <br /></td>
                </tr>
                <tr>
                  <td class="main-content-header"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>สินค้ายอดนิยมจากทุกแบรนด์</td>
                      <td align="right" class="moreproduct"><a class="moreproduct" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Group">ดูสินค้าทั้งหมด &gt;&gt;</a></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td align="center"><div class="product_box">
                  <?php
				  foreach($data1 as $row){
				  ?>
                    <div class="box1">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top" class="boxtxt"><p class="txt_green">
                          <?php
						  echo Yii::d($row->product_name_en,$row->product_name_th);
						  ?>
                          </p>
                            <p class="txt_center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ProductDetail/<?php echo $row->id;?>" class="link-more">
                            <?php
			  $file = Yii::app()->request->baseUrl . '/images/product/' . $row->pic1;
			  if(file_exists(Yii::app()->basePath . '/../images/product/' .  $row->pic1) && $row->pic1 != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="73" />
                  <?php
			  }else{
			  ?>
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
              <?php
			  }
			  ?></a>
                            </p></td>
                        </tr>
                        <tr>
                          <td class="txt_price txt_center">ราคา <?php echo number_format($row->price1,0,".",',');?> บาท</td>
                        </tr>
                        <tr>
                          <td class="bg-gray"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ProductDetail/<?php echo $row->id;?>" class="link-more">ดูรายละเอียด</a></td>
                              <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/AddToCart/<?php echo $row->id;?>/?keepThis=true&amp;TB_iframe=true&amp;height=250&amp;width=350" title="" class="thickbox link-cart">หยิบใส่ตะกร้า</a></td>
                            </tr>
                          </table>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <?php
				  }
				  ?>
                  </div></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="main-content-header"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>สินค้าใหม่</td>
                          <td align="right" class="moreproduct"><a class="moreproduct" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Group">ดูสินค้าทั้งหมด &gt;&gt;</a></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td><div class="product_box">
                        <?php
				  foreach($data as $row){
					  
				  ?>
                    <div class="box1">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top" class="boxtxt"><p class="txt_green">
                          <?php
						  echo Yii::d($row->product_name_en,$row->product_name_th);
						  ?>
                          </p>
                            <p class="txt_center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ProductDetail/<?php echo $row->id;?>" class="link-more">
                            <?php
			  $file = Yii::app()->request->baseUrl . '/images/product/' . $row->pic1;
			  if(file_exists(Yii::app()->basePath . '/../images/product/' .  $row->pic1) && $row->pic1 != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="73" />
                  <?php
			  }else{
			  ?>
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
              <?php
			  }
			  ?></a>
                            </p></td>
                        </tr>
                        <tr>
                          <td class="txt_price txt_center">ราคา <?php echo number_format($row->price1,0,".",',');?> บาท</td>
                        </tr>
                        <tr>
                          <td class="bg-gray"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ProductDetail/<?php echo $row->id;?>" class="link-more">ดูรายละเอียด</a></td>
                              <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/AddToCart/<?php echo $row->id;?>/?keepThis=true&amp;TB_iframe=true&amp;height=250&amp;width=350" title="" class="thickbox link-cart">หยิบใส่ตะกร้า</a></td>
                            </tr>
                          </table>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <?php
					  
				  }
				  ?>
                      </div></td>
                    </tr>
                    
                  </table></td>
                </tr>
                <tr>
                  <td><br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br /></td>
                </tr>
              </table>
            
              
                        </div>
                </td>
                </tr>
                </table>    