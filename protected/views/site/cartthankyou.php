<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                 <span class="txt-org"></span>
            </div>
        </div>
        <br/>
        <div class="register-box-thankyou">
            <div class="box-info">
                <div class="register-box-thankyou-info">เลขใบสั่งซื้อของคุณคือ : <span class="txt-org"><?php echo substr("000000000".$_REQUEST["id"],-9) ?></span><br/></div>
                <div class="register-box-thankyou-info">ขอขอบคุณที่สนใจสั่งสินค้ากับทางเรา<br/>ทางบริษัทจะติดต่อกลับโดยเร็ว</div>
                <div class="register-box-thankyou-subinfo"></div>
            </div>
        </div>
    </div>
</div>
