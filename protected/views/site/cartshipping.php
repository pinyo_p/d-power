<?php
$session = new CHttpSession;
$session->open();
$user = $session['user'];

$optAddress = "";
if (isset($_POST['MemberOrder']) && $_REQUEST['rd_address'] == "2")
    $optAddress = "checked='checked'";
?>

<script language="javascript">
    $(function () {
        $(".cart-shipping-address-info").hide();
    });
    function hide_address()
    {
        $(".cart-shipping-address-info").hide('fade');
    }

    function show_address()
    {
        $(".cart-shipping-address-info").show('fade');
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'new_property',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<input id="hidWeightOver" name="hidWeightOver" />
<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ข้อมูลการจัดส่งสินค้า <span class="txt-org"></span>

            </div>
        </div>
        <br/><br/><br/>

        <fieldset class="register-box-group">
            <legend>กรอกข้อมูลที่อยู่ที่ต้องการให้จัดส่งสินค้า</legend>
            <div class="register-box">
                <p>
                    <label>
                        <input name="rd_address" type="radio" onclick="hide_address()" id="RadioGroup1_0" value="1" checked="checked" />
                        ใช้ชื่อ - ที่อยู่ตามบัญชีผู้ใช้</label>
                    <br />
                    <label>
                        <input type="radio" name="rd_address" onclick="show_address()" value="2" id="RadioGroup1_1" <?php echo $optAddress; ?> />
                        ใช้ชื่อ - ที่อยู่ใหม่</label>
                    <br />
                </p>

                <div class="cart-shipping-address-info">
                    <div class="register-box-row">
                        <div class="register-box-left">ชื่อ - นามสกุล <span>*</span></div>
                        <div class="register-box-mid">:</div>
                        <div class="register-box-right">
                            <?php echo $form->textField($model, 'fullname'); ?>
                        </div>
                    </div>
                    <div class="register-box-row">
                        <div class="register-box-left">ชื่อบริษัท</div>
                        <div class="register-box-mid">:</div>
                        <div class="register-box-right">
                            <?php echo $form->textField($model, 'company_name'); ?>
                        </div>
                    </div>
                    <div class="register-box-row">
                        <div class="register-box-left" style="vertical-align: top;">ที่อยู่ <span>*</span></div>
                        <div class="register-box-mid" style="vertical-align: top;">:</div>
                        <div class="register-box-right">
                            <?php echo $form->textArea($model, 'address', array('rows' => 5, 'cols' => 60)); ?>
                        </div>
                    </div>
                    <div class="register-box-row">
                        <div class="register-box-left">จังหวัด</div>
                        <div class="register-box-mid">:</div>
                        <div class="register-box-right">
                            <?php
                            echo $form->dropDownList($model, 'province', CHtml::listData(Provinces::model()->findAll(array('order' => 'thai_name ASC')), 'province_id', 'thai_name'), array('empty' => 'เลือกจังหวัด',
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="register-box-row">
                        <div class="register-box-left" style="vertical-align: top;">รหัสไปรษณีย์</div>
                        <div class="register-box-mid">:</div>
                        <div class="register-box-right">
                            <?php echo $form->textField($model, 'zipcode'); ?>
                        </div>
                    </div>
                    <div class="register-box-row">
                        <div class="register-box-left" style="vertical-align: top;">เบอร์โทรศัพท์ติดต่อกลับ</div>
                        <div class="register-box-mid">:</div>
                        <div class="register-box-right">
                            <?php echo $form->textField($model, 'phone_no'); ?>
                        </div>
                    </div>
                    <div class="register-box-row" style='display:none;'>
                        <div class="register-box-left" style="vertical-align: top;">หมายเหตุ</div>
                        <div class="register-box-mid" style="vertical-align: top;">:</div>
                        <div class="register-box-right">

                        </div>
                    </div>

                </div>

            </div>
        </fieldset>
        <br/>
        <fieldset class="register-box-group">
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left" style='vertical-align: top'>หมายเหตุ / เพิ่มเติม</div>
                    <div class="register-box-mid" style='vertical-align: top'>:</div>
                    <div class="register-box-right">
                        <?php echo $form->textArea($model, 'remark', array('rows' => 5, 'cols' => 60)); ?>
                    </div>
                </div>
                <div class="register-box-row">
                    <div class="register-box-left">ต้องการใบกำกับภาษี</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">                    
                        <?php echo $form->checkBox($model, 'require_taxslipt', array('value' => 1, 'uncheckValue' => 0)); ?>
                    </div>
                </div>
            </div>
        </fieldset>
        <br/>
        <fieldset class="register-box-group">
            <div class="register-box">
                <div class="register-box-row">
                    <div class="register-box-left">Verify Code</div>
                    <div class="register-box-mid">:</div>
                    <div class="register-box-right">
                        <?php if (CCaptcha::checkRequirements()): ?>
                            <div class="row">
                                <?php echo $form->labelEx($model, ''); ?>
                                <div>
                                    <?php $this->widget('CCaptcha'); ?><br />
                                    <?php echo $form->textField($model, 'verifyCode'); ?>
                                </div>
                                <div class="form">
                                    <span class="form-error-info"><?php echo $form->error($model, 'verifyCode'); ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </fieldset>

        <br/><br/>
        <center>
            <button id="btnSave" name="btnRegister">บันทึก</button>
        </center>

    </div>
</div>


<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        if ($("#RadioGroup1_1").is(":checked")) {
            $("#RadioGroup1_1").trigger("click");
        }
    });
</script>

