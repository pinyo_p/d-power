<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
        <link  href="<?php echo Yii::app()->baseUrl; ?>/css/add2cart.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <br/><br/><br/>
        <center>
            <div class="box-addtocart">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center"><span class="textlarger"><b>คุณได้ทำการเพิ่มสินค้าลงตะกร้าเรียบร้อยแล้ว</b></span></td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center"><span class="txt_bold txt_green">
                                <?php
                                echo Yii::text_lang(array(
                                    $model->product_name_1,
                                    $model->product_name_2,
                                    $model->product_name_3,
                                    $model->product_name_4,
                                    $model->product_name_5));
                                ?>
                            </span></td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <?php
                            $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic1;
                            if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic1) && $model->pic1 != "") {
                                ?>
                                <img src="<?php echo $file; ?>" height="73" />
                                <?php
                            } else {
                                ?>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="136" height="94" />
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center"><span class="textlarger textred">ราคา <?php echo Yii::currencySign(); ?> <?php
                                echo number_format(Yii::currencyAmount($model->price1), 2, ".", ",");
                                ?></span></td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <a href="javascript:window.history.back();" onClick="" class="link-cart">เลือกสินค้าเพิ่ม</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/cart" class="link-cart">ดูตะกร้าสินค้า</a></td>
                    </tr>
                </table>
            </div>
        </center>
    </body>
</html>
