<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'new_property',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                 รายละเอียดการชำระเงิน<span class="txt-org"></span>
            </div>
        </div>
        <br/><br/><br/>
        
        <div class="register-box-thankyou">
            <div class="box-info">
                <div class="register-box-thankyou-info">
               </div>
                <div class="register-box-thankyou-subinfo" style="text-align:left;">
                    เวลาทำการ จ - ศ. ตั้งแต่ 08.00 - 16.00 น. (เว้นวันหยุดนักขัตฤกษ์) <br/>
                    การสั่งซื้อสินค้าหลังจากเวลา 15.00 น. ระบบจะคิดเป็นรายการในวันใหม่ 
                    <br/><br/>
                    <u>เงื่อนไขการจัดส่ง</u> 
                    <ul>
                        <li>จัดส่งสินค้าภายใน 3 วัน </li>
                    </ul>
                    
                    
                    <u>ช่องทางการชำระเงิน</u><br/>
                    <ul>
                        <li>ธนาคารกสิกรไทย เลขที่บัญชี 075-2-86555-8</li>
                        <li>ธนาคารไทยพาณิชย์ เลขที่บัญชี 021-2-59069-8</li>
                    </ul>
                   
                    หากชำระเงินแล้ว สามารถแจ้งการชำระเงินได้ผ่านช่องทางดังนี้
                    <ul>
                    <li>แจ้งสลิปใบเสร็จยืนยัน ใน LINE ID : bif_online</li>
                    <li>แจ้งผ่านเมนู "แจ้งชำระเงิน" ในส่วนเมนูของสมาชิก</li>
                    </ul>
                    <br/>
                    หรือสอบถามข้อมูลเพิ่มเติมได้ที่ : <a href="mailto:online@bangkokinterfood.co.th">online@bangkokinterfood.co.th</a>
                 
                    
                </div>
            </div>
        </div>
        
        <br/><br/>
        <center>
            <button id="btnSave" name="btnRegister">ยันยันการสั่งซื้อ</button>
            <button id="btnCancel" name="btnRegister" style="display:none;">ย้อนกลับ</button>
        </center>
        
    </div>
</div>
<?php $this->endWidget(); ?>