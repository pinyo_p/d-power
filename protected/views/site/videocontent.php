<?php

// Get youtube data
$url = "http://gdata.youtube.com/feeds/api/videos/" . $data["video"]->url;
$doc = new DOMDocument;
$doc->load($url);
$title = $doc->getElementsByTagName("title")->item(0)->nodeValue;
$desc = $doc->getElementsByTagName("content")->item(0)->nodeValue;
?>

<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                Video <span class="txt-org">Gallery</span>
            </div>
        </div>
        <div class="news-content-box-detail">
            <?php
            $modelGalVideo = new GalleryVideo();
            $dataImgIcon = $modelGalVideo->searchCoverByGalleryID($data["gallery"]->id)->data;
            ?>
            <div style="padding: 20px 0px 10px 0px;">
                <div class="galleryimg-content-box-title-txt">
                    <?php echo $title;?>
                </div>
                <br/>
            </div>
            
            <div style="width:830px;padding-left:17px;">
                <iframe width="100%" height="500" src="http://www.youtube.com/embed/<?php echo $data["video"]->url; ?>" frameborder="0" allowfullscreen></iframe>
                <br/><br/>
                <?php echo $desc; ?> 
            </div>



            <br/><br/>
            <div class="galleryimg-content-box-footer">
                <div class="galleryimg-content-box-footer-publish">
                    <i>Publish Date : <?php echo date_format(date_create($data["gallery"]->publish_date), 'd-m-Y'); ?></i>
                </div>
                <br/>
                <div class="galleryimg-content-box-footer-backbutton" onclick="history.back();">« ย้อนกลับ</div>
            </div>
        </div>
    </div>
    <!-- InstanceEndEditable -->
</div>