<script language="javascript">
i=0;
function add_register()
{
	i++;
	html = "<table width='100%'><tr><td colspan='2'>No. " + i + "</td></tr><tr><td><div id='dva" + i + "' class='dva'>ชื่อ <input type='text' id='name_" + i + "' name='name[]' /></td><td>ตำแหน่ง <input type='text' id='position_" + i + "' name=position[] /></td></tr><tr><td>อีเมลล์ <input type='text' id='email_" + i + "' name='email[]' /></td><td> เบอร์โทรศัพท์<input type='text' id='phoneno_" + i + "' name='phoneno[]' /></td></tr></table></div>";
	$( "#dva" ).append(html);
}
$(function(){
add_register();
		   });
</script>

  <?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'User';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>        
            <table width="100%"><tr><td class="main-column-bg-index ">
            <div class="main-column-content-index">
             

             
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="main-content-header">ใบสมัครเข้าร่วมอบรม</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="center"><h3>Training</h3></td>
                </tr>
                <tr>
                  <td class="add_data"><p><br />
                  </p>
                      <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ชื่อ-นามสกุล ลูกค้า : <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'fullname'); ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">อีเมล์ : <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'email'); ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ชื่อบริษัท: <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'company_name'); ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ประเภทธุรกิจ: <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'company_type'); ?></td>
                        </tr>
                          <tr>
                        <td align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ที่อยู่ : <span class="txt_red">* </span></td>
                        <td valign="top"><?php echo $form->textArea($model, 'address', array('row' => 10,'col'=>50)); ?></td>
                      </tr>
                      
                      <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">เบอร์โทรศัพท์: <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'tel'); ?></td>
                        </tr>
                      <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">เบอร์มือถือ: <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'mobile'); ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">เบอร์โทรสาร: <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'fax'); ?></td>
                        </tr>
                        
                         <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ชื่อผู้ขาย: <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'sale_name'); ?></td>
                        </tr>
                         <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">บริษัท: <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'sale_company'); ?></td>
                        </tr>
                        
                      <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">หลักสูตร  : <span class="txt_red">* </span></td>
                          <td valign="top">
                          <?php echo $form->dropDownList($model,'training_id',  CHtml::listData(Training::model()->findAll(array('order' => 'subject_th ASC')), 'id', 'subject_th'), array('empty'=>'เลือก หัวข้อ', 'style'=>'width: 400px'																																											  				 
	  )); ?>
                          </td>
                        </tr>
                      
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">วันที่ :  <span class="txt_red">* </span></td>
                          <td valign="top">
                            <?php echo $form->dropDownList($model,'training_date',  $param['date'], array('empty'=>'เลือก วันที่จะเข้าอบรม',																																												  				 
	  )); ?>
                            
                          </td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">จำนวนคน ที่เข้าอบรม : <span class="txt_red">* </span></td>
                          <td valign="top"><?php echo $form->textField($model,'training_person'); ?></td>
                        </tr>
                        <tr valign="top">
                        <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold" style="vertical-align:top">ผู้เข้าอบรม :</td>
                        <td valign="top" width="500"><div id='dva' class="dva"></div><input type="button" onclick='add_register()' value="เพิ่มผู้เข้าร่วม" />
                        </td>
                        </tr>
                        
                      

                      </table>
                    </td>
                </tr>
                <tr>
                  <td align="center"><br /></td>
                </tr>
                <tr>
                  <td align="center"><br /></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="center">
                   <?php if(CCaptcha::checkRequirements()): ?>
                          <center>
	<div>
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?><br />
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
        <div class="form">
		<?php echo $form->error($model,'verifyCode'); ?>
        </div>
	</div>
    </center>
	<?php endif; ?>
                  </td>
                </tr>
                <tr>
                  <td align="center">&nbsp;</td>
                </tr>
                <tr>
                  <td align="center"><input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/send.png" width="84" height="27" /></td>
                </tr>
                <tr>
                  <td align="center">
                    <br /></td>
                </tr>
              </table>
            
              
                        </div>
                </td>
                </tr>
                </table>    
                 <?php $this->endWidget(); ?>