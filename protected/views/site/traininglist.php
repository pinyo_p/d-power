          <?php
		  function setDate($start_date,$end_date,$i)
		  {
			  	$e = "";
				$s = "";
				if(trim($start_date)!=""&&trim($end_date)!=""){
					$d = explode("-",$start_date);
					$d2 = explode("-",$end_date);

					if($d[1]==$i)
					{
						$s = $d[2] . ' - ' . $d2[2] ;
						if($start_date == $end_date)
							$s = $d[2];
					}
					
				}
				return $s;
		  }
		  function showDate($row,$i)
		  {
				$s[] = setDate($row->start_date,$row->end_date,$i);
				$s[] = setDate($row->start_date2,$row->end_date2,$i);
				$s[] = setDate($row->start_date3,$row->end_date3,$i);
				$s[] = setDate($row->start_date4,$row->end_date4,$i);
				$s[] = setDate($row->start_date5,$row->end_date5,$i);
				$s[] = setDate($row->start_date6,$row->end_date6,$i);
				$s[] = setDate($row->start_date7,$row->end_date7,$i);
				$s[] = setDate($row->start_date8,$row->end_date8,$i);
				$s[] = setDate($row->start_date9,$row->end_date9,$i);
				$s[] = setDate($row->start_date10,$row->end_date10,$i);
				$s[] = setDate($row->start_date11,$row->end_date11,$i);
				$s[] = setDate($row->start_date12,$row->end_date12,$i);
				$arr = array();
				foreach($s as $r)
				{
					if(trim($r) != "")
						$arr[] = $r;
				}
				$str = implode(",",$arr);
				return $str;
				
		  }
		  ?>
            <table width="100%"><tr><td class="main-column-bg-index ">
            <div class="main-column-content-index">
             

            
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="main-content-header">Training</td>
                </tr>
                <tr>
                  <td>&nbsp;
                  <?php

					  echo Yii::d($t1->content_en,$t1->content_th);
				  ?>
                  </td>
                </tr>
                <tr>
                <td>
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                      <tr>
                        <td align="center" class="trainstatus">สถานะการอบรม : <span class="open">เตรียมการอบรม</span> | <span class="regis">เปิดให้ลงทะเบียน</span> | <span class="regis_close">ปิดลงทะเบียน</span> | <span class="training">อยู่ในระหว่างอบรม</span> | <span class="closed">ปิดอบรมแล้ว</span></td>
                      </tr>
                </table><br />

                </td>
                </tr>
                <tr>
                 <td class="job-content-header" colspan="16">
                  Training
                  </td>
                 </tr>
                 <tr><td class="training_table">
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                  
                  </tr>
                    <tr>
                      <th>Code</th>
                      <th>Name</th>
           <th >Days</th>
            <th >Fee(฿)</th>
             	 	 	
            <?php
			for($i = 1;$i<=12;$i++)
			{
				?>
                <th ><?php echo date("M",mktime(0,0,0,$i,1,2011)); ?></th>
                <?php
			}
			?>
                    </tr>
					<?php
					


					  foreach($data as $row){
					  ?>
                    <tr valign="top">
                    	<td> <?php echo $row->training_code; ?> </td>
                      <td class="job-content-txt">
                      
                      <a class='link_green' target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/trainingdetail/<?php echo $row->id;?>"><?php echo Yii::d($row->subject_en,$row->subject_th);?>
                </a><br />
 
                       
                        
                      
                        </td>
                        <td align="center" class="job-content-txt">
              <?php echo $row->days; ?>
              </td>
              <td align="center" class="job-content-txt" width="100">
              <?php echo number_format($row->price,0,".",","); ?>
              </td>
             <?php
			 
			 $c = "open";
			switch($row->status)
			{
				case "0":$c="open";break;
				case "1":$c="regis";break;
				case "2":$c="regis_close";break;
				case "3":$c="training";break;
				case "4":$c="closed";break;
			}
			 
			for($i = 1;$i<=12;$i++)
			{
				
				$s = showDate($row,$i);
				
				?>
                <td style="white-space:nowrap"><span class="<?php echo $c;?>"><?php echo $s; ?></span></td>
                <?php
			}
			?>
                    </tr>
                   
                     <?php
					  }
					  ?>
                  </table></td>
                </tr>
              </table>
              <br /><br />
               <?php
					   echo Yii::d($t2->content_en,$t2->content_th);

				  ?>
              
                        </div>
                        
                      
                </td>
                </tr>
                </table>    