
<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                ประวัติการสั่งซื้อสินค้า <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/>

        <table width="100%" border="0" cellpadding="3" cellspacing="3" class="quotation_add">
            <tr>
                <td width="15%" align="center" class="quotation-header2">หมายเลขใบสั่งซื้อ</td>
                <td width="12%" align="center" class="quotation-header2">รายการสินค้า</td>
                <td width="10%" align="center" class="quotation-header2">วันที่</td>
                <td class="quotation-header2">หมายเหตุ</td>
                <td width="18%" align="center" class="quotation-header2">สถานะ</td>
                <td align="right" class="quotation-header2">รวม</td>
            </tr>
            <?php
            $i = 0;
            $sumOrder = 0;
            foreach ($data as $row) {
                $i++;
                $bg = "";
                if ($i % 2 == 0) {
                    ?>
                    <tr bgcolor="#eeeeee">
                        <?php
                    } else {
                        ?>
                    <tr>
                        <?php
                        $bg = 'bgcolor="#f7f7f7"';
                    }
                    ?>

                    <td width="15%" align="center" <?php echo $bg; ?>><?php echo sprintf("KPT%04d", $row->id); ?></td>
                    <td width="12%" align="center" <?php echo $bg; ?>><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/historydetail/<?php echo $row->id; ?>">รายการสินค้า</a></td>
                    <td width="10%" align="center" <?php echo $bg; ?>><?php echo substr($row->create_date, 0, 10); ?></td>
                    <td <?php echo $bg; ?>><?php echo $row->remark;?></td>
                    <td width="18%" align="center" <?php echo $bg; ?>><strong>
                            <?php
                            switch ($row->status) {
                                case "0":echo "รายการใหม่";
                                    break;
                                case "1":echo "ติดต่อแล้ว";
                                    break;
                                case "2":echo "ชำระเงินแล้ว";
                                    break;
                                case "3":echo "ยกเลิก";
                                    break;
                                case "4":echo "รอจัดส่ง";
                                    break;
                                case "5":echo "จัดส่งแล้ว";
                                    break;
                                case "6":echo "จัดส่งไม่สำเร็จ";
                                    break;
                                case "7":echo "ยกเลิกการจัดส่ง";
                                    break;
                            }
                            ?>
                        </strong></td>
                        <td align="right" <?php echo $bg; ?>>
                            <?php $sumOrder += $this->summaryOrder($row->id);  ?>
                            <?php echo number_format(Yii::currencyAmount($this->summaryOrder($row->id)), 2, ".", ","); ?>
                        </td>
                </tr>
                <?php
            }
            ?>

        </table>

        <?php
        if (count($data) == 0){
            echo "<div style='padding:10px;text-align:center;'>-- ไม่มีประวัติการสั่งซื้อสินค้า --</div>";
        }else{
            echo "<div style='padding-top:20px;font-size:16px;'>";
            echo "<table style='width:100%;'>";
            echo "<tr><td style='text-align:right;font-weight:bold;'>รวมทั้งสิ้น</td><td style='text-align:right;'>".number_format(Yii::currencyAmount($sumOrder), 2, ".", ",")."</td></tr>";
            echo "<tr><td style='text-align:right;font-weight:bold;'>จำนวนแต้มสะสม</td><td style='text-align:right;'>".$params["total_score"]."</td></tr>";
            echo "</table>";
            echo "</div>";
        }
        ?>




    </div>
</div>
