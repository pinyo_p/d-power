<?php
$modelGalImg = new GalleryImage();
$dataImgIcon = $modelGalImg->searchCoverByGalleryID($data->id)->data;
$dataImgs = $modelGalImg->searchByGalleryID($data->id, '5')->data;
?>
<div style="padding: 20px 0px 10px 0px;">
    <div class="galleryimg-content-box-icon">
        <?php
        if (count($dataImgIcon) > 0) {
            ?>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/album/<?php echo $data->id; ?>/<?php echo $dataImgIcon[0]->file_name; ?>" width="50" height="50"  />
        <?php } else { ?>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/no_image_gallery_icon.jpg" width="50" height="50" />
        <?php } ?>
    </div>
    <div class="galleryimg-content-box-title-txt">
        <?php
        echo Yii::text_lang(array(
            $data->name_1,
            $data->name_2,
            $data->name_3,
            $data->name_4,
            $data->name_5));
        ?>
        <div>
            <?php
            echo Yii::text_lang(array(
                $data->description_1,
                $data->description_2,
                $data->description_3,
                $data->description_4,
                $data->description_5));
            ?>
        </div>
    </div>
    <br/>
    <?php
    if ($dataImgs > 0) {
        $boxTitle = "<div style='font-size:15px;'>" . Yii::text_lang(array(
                    $dataImgs[0]->name_1,
                    $dataImgs[0]->name_2,
                    $dataImgs[0]->name_3,
                    $dataImgs[0]->name_4,
                    $dataImgs[0]->name_5)) . "</div><div style='font-size:11px;'>" .
                Yii::text_lang(array(
                    $dataImgs[0]->description_1,
                    $dataImgs[0]->description_2,
                    $dataImgs[0]->description_3,
                    $dataImgs[0]->description_4,
                    $dataImgs[0]->description_5))
                . "</div>";
        ?>
        <div class="galleryimg-content-box-thumb" >
            <div class="galleryimg-content-box-thumb-bigitem">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/images/album/<?php echo $data->id; ?>/<?php echo $dataImgs[0]->file_name; ?>" title="<?php echo $boxTitle ?>" data-lightbox="roadtrip<?php echo $data->id; ?>">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/album/<?php echo $data->id; ?>/<?php echo $dataImgs[0]->file_name; ?>" />
                </a>
            </div>
            <div class="galleryimg-content-box-thumb-smallitem">
                <?php
                $idx = 0;
                foreach ($dataImgs as $row) {
                    if ($idx > 0) {
                        $boxTitle = "<div style='font-size:15px;'>" . Yii::text_lang(array(
                                    $row->name_1,
                                    $row->name_2,
                                    $row->name_3,
                                    $row->name_4,
                                    $row->name_5)) . "</div><div style='font-size:11px;'>" .
                                Yii::text_lang(array(
                                    $row->description_1,
                                    $row->description_2,
                                    $row->description_3,
                                    $row->description_4,
                                    $row->description_5))
                                . "</div>";
                        ?>
                
                        <div>
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/images/album/<?php echo $data->id; ?>/<?php echo $row->file_name; ?>" title="<?php echo $boxTitle; ?>" data-lightbox="roadtrip<?php echo $data->id; ?>">
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/album/<?php echo $data->id; ?>/<?php echo $row->file_name; ?>" />
                            </a>
                        </div>
               
                        <?php
                    }
                    $idx++;
                }
                ?>   
            </div>
        </div>


        <div class="galleryimg-content-box-footer">
            <div class="galleryimg-content-box-footer-publish">
                <i>Publish Date : <?php echo date_format(date_create($data->publish_date), 'd-m-Y'); ?></i>
            </div>
            <div class="galleryimg-content-box-footer-readmore">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/GalleryImageDetail/<?php echo $data->id; ?>">อ่านต่อ »</a>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div style="height:1px;border-top:solid 1px #ccc;"></div>
<div style="height:10px;"></div>
