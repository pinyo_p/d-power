<script type="text/javascript">
    $(document).ready(function () {
        // Recommend product
        $('.btnBuy').click(function () {
            var productID = $(this).attr('productid');
            var orderAmt = $('#txtAmt' + productID).val().trim();

            if (orderAmt == "") {
                alert('กรุณาระบุจำนวนสินค้าด้วย !!!');
                return;
            } else {
                if (isNaN(orderAmt)) {
                    alert('กรุณาระบุจำนวนสินค้าเป็นตัวเลข !!!');
                    return;
                } else {
                    orderAmt = parseInt(orderAmt);
                    if (orderAmt < 1) {
                        alert('กรุณาระบุจำนวนสินค้ามากกว่า 0 !!!');
                        return;
                    } else {
                        var url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/AddToCart/" + productID + "/?amount=" + orderAmt + "&keepThis=true&amp;TB_iframe=true&amp;height=250&amp;width=350";
                        window.location.href = url;
                    }
                }
            }
        });
    });
</script>

<div class="mainsub-menu-list">
    <div class="mainsub-menu-title">
        <div class="mainsub-menu-title-txt">
            สินค้า<span class="txt-org">ของเรา</span>
        </div>
    </div>
    <ul>
        <?php
        foreach ($data["product_group_all"] as $row) {
            echo "<li onclick=\"location.href = '" . Yii::app()->request->baseUrl . "/index.php/site/ProductGroup/id/" . $row->id . "'\">";
            echo Yii::text_lang(array($row->group_1, $row->group_2, $row->group_3, $row->group_4, $row->group_5));
            echo "</li>";
            $ProductSubGroup = ProductGroup::model()->findAll('parent_id=:parent_id', array(':parent_id' => $row->id));
            if (count($ProductSubGroup) > 0) {                
                foreach ($ProductSubGroup as $rowSub) {
                    echo "<li style='padding-left:35px;' onclick=\"location.href = '" . Yii::app()->request->baseUrl . "/index.php/site/ProductGroup/id/" . $rowSub->id . "'\">";                    
                    echo "-&nbsp;&nbsp;".Yii::text_lang(array($rowSub->group_1, $rowSub->group_2, $rowSub->group_3, $rowSub->group_4, $rowSub->group_5));                    
                    echo "</li>";
                }                
            }
        }
        ?>
    </ul>
</div>

<div class="product-box">
    <div class="product-box-title">
        <div class="product-box-title-txt">
            <?php
            echo $this->formatTitle2Tone(Yii::text_lang(array(
                        $data["product_group"]->group_1,
                        $data["product_group"]->group_2,
                        $data["product_group"]->group_3,
                        $data["product_group"]->group_4,
                        $data["product_group"]->group_5
            )));
            ?>
        </div>

    </div>

    <div class="product-box-content">
        <ul class="product-box-content-list">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $products,
                'itemView' => '_productgroup',
                'viewData' => array('arrBestSeller' => $arrBestSeller),
                'summaryText' => '',
                'template' => "{items}{pager}",
                'pager' => array(
                    'header' => '',
                ),
            ));
            ?>  
        </ul>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.pager').attr('style', 'text-align:left;padding-top:30px;');

        $('.items').attr('style', 'overflow:auto;');
    });
</script>