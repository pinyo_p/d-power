<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/tab/tabs.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/tab/general.css" type="text/css" media="screen" />

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.bpopup.min.js"></script>

<style type="text/css">
    img.rating
    {
        width:100px;
        height:25px;
        background-repeat:no-repeat;
        background-image:url(<?php echo Yii::app()->request->baseUrl; ?>/images/shot_1296500864.png);
        /*background-position:0px -20px;*/
        /*
        <?php
        $icon_posi = 0;
        $point = $model->point;
        $count = ($model->vote_count * 5);
        if ($point == 0)
            $point = 0;
        if ($count == 0)
            $count = 5;
        $vote = round((($point * 5) / $count), 1);

        echo $vote;
        $icon_posi = floor($vote) * 44;
        ?>
        */
        background-position:0px -<?php echo $icon_posi; ?>px;
    }
</style>
<script language="javascript">
    function voteit()
    {
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/vote", {
            product_id:<?php echo $_GET['id']; ?>,
            member_id: 1,
            point: $("#vote").val()
        }, function (data) {
            if (data.result == "fail")
            {
                if (data.message == "need login")
                {
                    if (confirm("กรุณา Login ก่อนดำเนินการ vote\nต้องการ Login เลยหรือไม่"))
                    {
                        window.open('<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/memberLogin', 'member_login');
                    }
                } else if (data.message == "already vote")
                {
                    alert('คุณสามารถ vote ได้เพียงครั้งเดียวเท่านั้น');
                }
            } else {
                location.reload();
            }
        }, 'json');

    }

</script>
<?php

function ShowPicCover($objImage, $width, $height, $main_pic = false) {
    $file = Yii::app()->request->baseUrl . "/images/product/" . $objImage;
    $str_image = "";
    $p = "";
    if ($main_pic)
        $p = " <br /><br /> คลิกเพื่อดูภาพขยาย";
    if (file_exists(Yii::app()->basePath . '/../images/product/' . $objImage) && $objImage != "") {
        $str_image = "<a href='" . $file . "' class='thickbox' rel='gallery' style='padding:2px' target='_blank'><img src='" . $file . "' width='$width'  />$p</a>";
    } else {
        $str_image = "<img src='" . Yii::app()->request->baseUrl . "/images/news_no_image.jpg' width='$width'  />";
    }
    return $str_image;
}

function ShowPic($objImage, $width, $height, $main_pic = false) {
    $file = Yii::app()->request->baseUrl . "/images/product/" . $objImage;
    $str_image = "";
    $p = "";
    if ($main_pic)
        $p = " <br /><br /> คลิกเพื่อดูภาพขยาย";
    if (file_exists(Yii::app()->basePath . '/../images/product/' . $objImage) && $objImage != "") {
        $str_image = "<a href='" . $file . "' class='thickbox' rel='gallery' style='padding:2px' target='_blank'><img src='" . $file . "' width='$width'  />$p</a>";
    }
    return $str_image;
}

function ShowPrice($objPrice, $objAmount) {
    $str_price = "";
    if (trim($objAmount) != "" && $objAmount > 0) {
        $str_price = "<tr>
                          <td width='15%' height='24' align='right' class='product_price_tab2'>$objAmount</td>
                          <td align='right' class='product_price_tab2'>" . Yii::currencySign() . " " . number_format(Yii::currencyAmount($objPrice), 2, ".", ",") . "</td>
                        </tr>";
    }
    return $str_price;
}
?>
<script language="javascript">
    $(document).ready(function () {
        $('#btnBack').click(function () {
            window.history.back();
        });
    });
    function opencart()
    {
        var objAmount = $("#amount").val();
        var url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/AddToCart/<?php echo $model->id; ?>/?amount=" + objAmount + "&keepThis=true&amp;TB_iframe=true&amp;height=250&amp;width=350";

        //$("#cart").attr('href', url);
        //$("#cart").click();alert('click');

        window.location.href = url;
    }
</script>
<a href="#" title="" class="thickbox link-cart" id="cart" style="display:none;" ></a>


<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                รายละเอียดสินค้า <span class="txt-org"></span>
            </div>
        </div>
        <br/><br/><br/>


        <table border="0" style="width:100%;">
            <tr>
                <td width="35%" class="productdetail" valign="top">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center">
                                <?php echo ShowPicCover($model->pic1, 240, 195, true); ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center"></td>
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" class="productpic">
                                <?php
                                echo ShowPic($model->pic2, 70, 57);
                                echo ShowPic($model->pic3, 70, 57);
                                echo ShowPic($model->pic4, 70, 57);
                                echo ShowPic($model->pic5, 70, 57);
                                echo ShowPic($model->pic6, 70, 57);
                                echo ShowPic($model->pic7, 70, 57);
                                echo ShowPic($model->pic8, 70, 57);
                                echo ShowPic($model->pic9, 70, 57);
                                echo ShowPic($model->pic10, 70, 57);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center"><span class="textred textlarger">ราคา <?php echo Yii::currencySign(); ?> <?php echo number_format(Yii::currencyAmount($model->price1), 2, ".", ","); ?></span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center"><table width="80%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="24" colspan="2" class="product_price_tab"><b>จำนวน                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ราคาต่อหน่วย</b></td>
                                    </tr>
                                    <?php
                                    echo ShowPrice($model->price1, $model->amount1);
                                    echo ShowPrice($model->price2, $model->amount2);
                                    echo ShowPrice($model->price3, $model->amount3);
                                    echo ShowPrice($model->price4, $model->amount4);
                                    ?>
                                </table></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                        if ($model->stock > 0) {
                            ?>
                            <tr>
                                <td align="center">
                                    จำนวนซื้อ :
                                    <input name="amount" type="text" id="amount" size="10" value="1" class="product-detail-amount" />
                                    &nbsp;<a href="javascript:;" onclick="opencart()" title="" class="link-cart">หยิบใส่ตะกร้า</a>
                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center">
      <!--a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/quotation/<?php echo $_GET['id']; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_price.jpg" width="102" height="35" /></a-->

                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Contact/?product_id=<?php echo $_GET['id']; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_price.jpg" width="102" height="35" style="display:none;" /></a>&nbsp;&nbsp;&nbsp;
                                    <?php
                                    $file = Yii::app()->request->baseUrl . '/attach/product/' . $model->attach;
                                    if (file_exists(Yii::app()->basePath . '/../attach/product/' . $model->attach) && $model->attach != "") {
                                        ?>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ProductDownload/<?php echo $model->id; ?>" target="_blank">
                                            <!--<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_download.jpg" width="102" height="35" />-->
                                            <button id="btnDownload" type="button">Download</button>
                                        </a>
                                        <?php
                                    }
                                    ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table> 
                </td>
                <td valign="top">
                    <table width="100%" border="0">
                        <tr>
                            <td>
                                <span class="product-detail-title">
                                    <?php
                                    echo Yii::text_lang(array(
                                        $model->product_name_1,
                                        $model->product_name_2,
                                        $model->product_name_3,
                                        $model->product_name_4,
                                        $model->product_name_5));
                                    ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp; 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td valign="baseline"><b>คะแนนสินค้า : <img class="rating" src="" width="100" height="15"  style="border:none; " /></b>&nbsp;(<?php echo $vote; ?> จาก 3) &nbsp;</td>
                                        <td valign="baseline"><strong>ให้คะแนน : &nbsp;
                                                <select name="vote" id="vote">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                                <input type="submit" name="button" id="button" value="ตกลง" onclick='voteit()' style="border:solid 1px #CCC; cursor:pointer" />
                                            </strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp; 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>สถานะสินค้า : <?php
                                    if ($model->status == 1) {
                                        echo "มีสินค้า";
                                    } else if ($model->status == 2) {
                                        echo "ยกเลิก";
                                    }
                                    ?>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp; 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="container">
                                    <ul class="menu">
                                        <li id="news" class="active">รายละเอียด</li>
                                        <li id="tutorials">สินค้าใกล้เคียง</li>
                                    </ul>

                                    <span class="clear"></span>
                                    <div class="content news">
                                        <ul>
                                            <li>
                                                <?php
                                                echo Yii::text_lang(array(
                                                    $model->product_desc_1,
                                                    $model->product_desc_2,
                                                    $model->product_desc_3,
                                                    $model->product_desc_4,
                                                    $model->product_desc_5,));
                                                ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="content tutorials">
                                        <ul>
                                            <li>
                                                <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td align="center">
                                                            <div class="product_box">
                                                                <?php
                                                                foreach ($relate as $row) {
                                                                    ?>
                                                                    <div class="box1">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td valign="top" class="boxtxt">
                                                                                    <p class="txt_green">

                                                                                        <?php
                                                                                        echo Yii::text_lang(array(
                                                                                            $row->product['product_name_1'],
                                                                                            $row->product['product_name_2'],
                                                                                            $row->product['product_name_3'],
                                                                                            $row->product['product_name_4'],
                                                                                            $row->product['product_name_5']));
                                                                                        ?>

                                                                                    </p>
                                                                                    <p class="txt_center">
                                                                                        <?php
                                                                                        $file = Yii::app()->request->baseUrl . '/images/product/' . $row->product['pic1'];
                                                                                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $row->product['pic1']) && $row->product['pic1'] != "") {
                                                                                            ?><br />
                                                                                            <img src="<?php echo $file; ?>" style="max-height:85px;max-width:124px;" />
                                                                                            <?php
                                                                                        } else {
                                                                                            echo '<img src="' . Yii::app()->request->baseUrl . '/images/logo_knp.jpg" style="max-height:85px;max-width:124px;" />';
                                                                                        }
                                                                                        ?>
                                                                                    </p>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="txt_price txt_center">ราคา <?php echo Yii::currencySign(); ?> <?php echo number_format(Yii::currencyAmount($row->product['price1']), 2, ".", ","); ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bg-gray">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ProductDetail/<?php echo $row->product['id']; ?>" class="link-more" target="_blank">ดูรายละเอียด</a></td>
                                                                                            <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/AddToCart/<?php echo $row->product['id']; ?>/?keepThis=true&amp;TB_iframe=true&amp;height=250&amp;width=350" title="" class="thickbox link-cart">หยิบใส่ตะกร้า</a></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                        </ul>
                                    </div>
                                </div>



                                <br/>
                                <span class='st_facebook_large' displayText='Facebook'></span>
                                <span class='st_twitter_large' displayText='Tweet'></span>
                                <span class='st_googleplus_large' displayText='Google +'></span>
                                <span class='st_linkedin_large' displayText='LinkedIn'></span>
                                <span class='st_pinterest_large' displayText='Pinterest'></span>
                                <span class='st_email_large' displayText='Email'></span>



                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <br/><br/>
        <button id="btnBack" onclick="return false;">« ย้อนกลับ</button>

    </div>
</div>

<div id="popup_info" style="display:none;min-height: 40px;">
    <span class="button b-close"><span>X</span></span>
    <span id="popup_info_text"></span>
</div>

<script type="text/javascript">
    $(document).ready(function () {


<?php
$alertMsg = "";
$isAlert = false;

// สินค้าในหมวดไดฟุกุ
if ($model->group_id == "182") {   
    if ($alertMsg != "")
        $alertMsg .= "<hr/>";
    if (Yii::app()->language == "th")
        $alertMsg = "รายการสินค้านี้ ส่งสินค้าภายใน 7 วัน";
    else
        $alertMsg = "This product will be sent within 7 days.";
    $isAlert = ($model->group_id == "182");
}

// สินค้าประเภทของสด
if ($model->is_fresh_type == "1") {
    if ($alertMsg != "")
        $alertMsg .= "<hr/>";
    if (Yii::app()->language == "th")
        $alertMsg .= "สินค้านี้เป็นประเภทอาหารสด ต้องสั่งซื้อขั้นต่ำ 2,000 บาท";
    else
        $alertMsg .= "Product is fresh type, Minimum purchase require 2,000 baht.";
    $isAlert = true;
}

// สินค้าสำหรับร้านอาหาร
if ($model->is_for_restaurant == "1") {
    if ($alertMsg != "")
        $alertMsg .= "<hr/>";
    if (Yii::app()->language == "th")
        $alertMsg .= "รายการสินค้านี้สำหรับร้านอาหาร <br/>สอบถามรายละเอียดเพิ่มเติมได้ที่ เบอร์โทร 088-755-1083";
    else
        $alertMsg .= "Product for restaurant, For more information tel. 088-755-1083.";
    $isAlert = true;
}

// Show popup
if ($isAlert) {
    echo "alertInfo('" . $alertMsg . "');";
}
?>
    });

    function alertInfo(msg) {
        $('#popup_info_text').html(msg);
        $('#popup_info').bPopup({easing: 'easeOutBack', speed: 450, transition: 'slideDown'});
    }
</script>    

