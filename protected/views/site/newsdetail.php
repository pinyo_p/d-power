<div class="mainbox-content">
    <!-- InstanceBeginEditable name="maincontent" -->
    <div class="news-content-box">
        <div class="news-content-box-title">
            <div class="news-content-box-title-txt">
                <?php
                echo Yii::text_lang(array(
                    $data["news"]->title_1,
                    $data["news"]->title_2,
                    $data["news"]->title_3,
                    $data["news"]->title_4,
                    $data["news"]->title_5));
                ?>
            </div>Publish Date : <?php echo date_format(date_create($data["news"]->create_date), 'd-m-Y'); ?>
        </div>
        <br/><div class="sep_line"></div><br/>
        <div class="news-content-box-detail">
            <?php
            echo Yii::text_lang(array(
                $data["news"]->content_1,
                $data["news"]->content_2,
                $data["news"]->content_3,
                $data["news"]->content_4,
                $data["news"]->content_5));
            ?>
            <br/><br/><br/><br/><br/><br/>
            <div style="text-align:center;">
            <span class='st_facebook_large' displayText='Facebook'></span>
            <span class='st_twitter_large' displayText='Tweet'></span>
            <span class='st_googleplus_large' displayText='Google +'></span>
            <span class='st_linkedin_large' displayText='LinkedIn'></span>
            <span class='st_pinterest_large' displayText='Pinterest'></span>
            <span class='st_email_large' displayText='Email'></span>
            </div>
        </div>

    </div>
    <!-- InstanceEndEditable -->
</div>



