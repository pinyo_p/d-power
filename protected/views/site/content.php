<?php
$totalContents = count($data["contents"]);
?>
<div class="mainbox-content">
    <?php if ($totalContents > 1) { ?>
        <div class="mainsub-menu-list">
            <div class="mainsub-menu-title">
                <div class="mainsub-menu-title-txt">
                    <?php echo $this->formatTitle2Tone($data["content_group_title"]); ?>
                </div>
            </div>
            <ul>
                <?php
                foreach ($data["contents"] as $row) {
                    echo "<li class='menu_content' contentid='" . $row->id . "' contentcode='" . $_REQUEST['code'] . "'>" . Yii::text_lang(array(
                        $row->title_1,
                        $row->title_2,
                        $row->title_3,
                        $row->title_4,
                        $row->title_5)) . "</a></li>";
                }
                ?>
            </ul>
        </div>
    <?php } ?>
    <div class='mainsub-content-box<?php echo ($totalContents == 1) ? "-full" : ""; ?>'>
        <?php if (count($data["content"]) > 0) { ?>
            <div class="mainsub-content-box-title">
                <?php
                echo Yii::text_lang(array(
                    $data["content"][0]->title_1,
                    $data["content"][0]->title_2,
                    $data["content"][0]->title_3,
                    $data["content"][0]->title_4,
                    $data["content"][0]->title_5));
                ?>
            </div>

            <div class="mainsub-content-box-detail">
                <?php
                echo Yii::text_lang(array(
                    $data["content"][0]->content_1,
                    $data["content"][0]->content_2,
                    $data["content"][0]->content_3,
                    $data["content"][0]->content_4,
                    $data["content"][0]->content_5));
                ?>
            </div>
            <br/><br/><br/><br/><br/><br/>
            <div style="text-align:center;">
            <span class='st_facebook_large' displayText='Facebook'></span>
            <span class='st_twitter_large' displayText='Tweet'></span>
            <span class='st_googleplus_large' displayText='Google +'></span>
            <span class='st_linkedin_large' displayText='LinkedIn'></span>
            <span class='st_pinterest_large' displayText='Pinterest'></span>
            <span class='st_email_large' displayText='Email'></span>
            </div>
        <?php } ?>
    </div>
</div>

<script type='text/javascript'>
    $(document).ready(function () {
        // Left menu click
        $('.menu_content').click(function () {
            var contentID = $(this).attr('contentid');
            var contentCode = $(this).attr('contentcode');
            location.href = '<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/content/code/' + contentCode + '/id/' + contentID;
        });

        // Submit contact form
        $('#btnSubmitContact').click(function () {
            var topic = $('#txtTopic').val().trim();
            var fullName = $('#txtFullName').val().trim();
            var email = $('#txtEmail').val().trim();
            var mobile = $('#txtMobile').val().trim();
            var tel = $('#txtTel').val().trim();
            var subject = $('#txtSubject').val().trim();
            var detail = $('#txtDetail').val().trim();
            var isSubscribe = $('#chkSubscribe').is(':checked');

            // validate data
            /*if (topic == "") {
             alert("กรุณากรอกเรื่อง");
             $('#txtTopic').focus();
             return;
             }
             if (fullName == "") {
             alert("กรุณากรอกชื่อ");
             $('#txtFullName').focus();
             return;
             }
             if (email == "") {
             alert("กรุณากรอก Email");
             $('#txtEmail').focus();
             return;
             }
             if (subject == "") {
             alert("กรุณากรอกหัวข้อ");
             $('#txtSubject').focus();
             return;
             }
             if (detail == "") {
             alert("กรุณากรอกข้อความ");
             $('#txtDetail').focus();
             return;
             }*/
            // Post data
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ContactNew",
                    {topic: topic, fullName: fullName, email: email, mobile: mobile, tel: tel, subject: subject, detail: detail, issubscribe: isSubscribe},
            function (data) {
                if (data.result == "success")
                {
                    alert("<?php echo Label::$submit_complete; ?>");
                    // Clear data
                    $('#txtTopic').val('');
                    $('#txtFullName').val('');
                    $('#txtEmail').val('');
                    $('#txtMobile').val('');
                    $('#txtTel').val('');
                    $('#txtSubject').val('');
                    $('#txtDetail').val('');
                } else {
                    alert(data.message);
                }
            }, 'json');
        });

    });
</script>


