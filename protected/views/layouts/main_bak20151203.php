<?php
// Product group
$modelProductGroup = new ProductGroup();
$data["product_group"] = ProductGroup::model()->findAll('parent_id=0', array('order' => 'sort_order asc')); //$modelProductGroup->searchAll()->data;
// Latest news (2 records)
$modelNews = new Content();
$data["news"] = $modelNews->searchNewsForIndex()->data;

// Banner
$modelBanner = new Banner();
$data["banner"] = $modelBanner->search()->data;

// Banner Intro
$modelBannerIntro = new BannerIntro();
$data["banner_intro"] = $modelBannerIntro->search()->data;

// Video gallery
$modelVideoGallery = new GalleryVideo();
$data["img_gallery"] = $modelVideoGallery->searchAlbumForIndex()->data;

// Footer
$modelFooter = new Content();
$modelFooter->content_code = 'Footer';
$data["footer"] = $modelFooter->searchAll()->data;
?>

<!doctype html>
<html><!-- InstanceBegin template="/Templates/tem1.dwt" codeOutsideHTMLIsLocked="false" -->
    <head> 
        <title><?php echo Yii::app()->name; ?></title>

        <meta name="keywords" content="flour, rice flour, แป้งข้าวเหนียว, แป้งข้าวจ้าว, แป้งมัน, สาคู, โมจิ, แป้งผสม, อาหารแช่แข็ง" />
        <meta name="description" content="flour, rice flour, แป้งข้าวเหนียว, แป้งข้าวจ้าว, แป้งมัน, สาคู, โมจิ, แป้งผสม, อาหารแช่แข็ง" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" /> 

        <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/fav.png">
        <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon">

        <!-- **************************************************************************************** -->

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/css.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/flexnav.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/lightbox.css" rel="stylesheet" />
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.0.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/lightbox.min.js"></script>

        <!-- ********** Banner Slide **********--> 
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/banner-slide.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/superslide.2.1.js"></script>
        <!-- *******************************-->

        <!-- ********** Product slide **********-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jcarousel.basic.css">
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jcarousel.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jcarousel.basic.js"></script> 
        <!-- *******************************-->

        <!-- ********** Language switcher **********-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/msdropdown/dd.css" />
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/msdropdown/jquery.dd.min.js"></script>
        <!-- *******************************-->

        <!-- ********** Popup thickbox **********-->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/thickbox.js"></script>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/thickbox.css" type="text/css" media="screen" />
        <!-- *******************************-->

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/submenu.css" rel="stylesheet" type="text/css" />

        <!-- ********** Social Share **********-->
        <script type="text/javascript">var switchTo5x = true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "1e42a888-a1a0-4026-905c-d2fe55a5c11c", doNotHash: true, doNotCopy: false, hashAddressBar: false});</script>
        <!-- *******************************-->
        
        <!-- InstanceBeginEditable name="head" -->
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <div>
            <?php
            $bd = Content::model()->find('content_code=:content_code', array(':content_code' => 'banner_day'));
            if ($bd->attr1 == "1" && ($bd->attr2 == "" || $bd->attr2 <= date("Y-m-d")) && ($bd->attr3 == "" || $bd->attr3 >= date("Y-m-d"))) {
                echo "<img src='" . Yii::app()->request->baseUrl . "/images/banner_day/" . $bd->content_2 . "' /";
            }
            ?>
        </div>
        <div class="banner-block"></div>

        <div class="sub-banner">

            <div class="slider">
                <div class="bd">
                    <ul>
                        <?php
                        foreach ($data["banner_intro"] as $row) {
                            echo "<li><a href='#'><img src='" . Yii::app()->request->baseUrl . "/images/banner_intro/" . $row->id . "/" . $row->img_src . "' /></a></li>";
                        }
                        ?>                        
                    </ul>
                </div>
                <div class="hd">
                    <ul>
                    </ul>
                </div>
                <div class="pnBtn prev"> <span class="blackBg"></span> <a class="arrow" href="javascript:void(0)"></a> </div>
                <div class="pnBtn next"> <span class="blackBg"></span> <a class="arrow" href="javascript:void(0)"></a> </div>
            </div>
        </div>

        <div class="mainsite">
            <div class="header">
                <div class="logo"> <a href="/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" width="149" height="70"  alt=""/></a> </div>
                <div class="header-row">


                    <div class="memberinfo" style="padding-right: 10px;">
                        <?php
                        $session = new CHttpSession;
                        $session->open();

                        if (isset($session['user']->username)) {

                            $user = (array) $session['user'];
                            ?>
                            สวัสดี คุณ <span class=""><?php echo $user['first_name']; ?>&nbsp;</span> | 
                            <a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/editprofile' class="link_w txt_bold">ข้อมูลส่วนตัว</a> | 
                            <a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/history' class="link_w txt_bold">ประวัติการสั่งซื้อ</a> | 
                            <a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/trackorder' class="link_w txt_bold">ติดตามคำสั่งซื้อ</a> |
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/logout" class="link_w txt_bold">Logout</a>
                            <div class="memberinfo-cart">
                                <a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/cart' class="link_w txt_bold">
                                    สินค้าในตะกร้า : <span class="txt_green txt_bold"><?php
                        $cart_data = Yii::app()->getSession()->sessionID;
                        if (!isset(Yii::app()->request->cookies['cart'])) {
                            $cookie = new CHttpCookie('cart', $cart_data);
                            $cookie->expire = time() + 60 * 60 * 24 * 180;
                            Yii::app()->request->cookies['cart'] = $cookie;
                        }
                        $cart_id = Yii::app()->request->cookies['cart']->value;
                        //Yii::app()->request->cookies->clear();
                        Yii::app()->request->cookies['cart'] = new CHttpCookie('cart', $cart_id);
                        $count = Order::model()->count('session_id=:cart_id and status=0 and order_id=0', array(':cart_id' => $cart_id));
                        $modelOrder = new Order();
                        $modelOrder->session_id = $cart_id;
                        $dataOrder = $modelOrder->get_cart()->data;
                        echo count($dataOrder);
                            ?></span> รายการ</a></div>
                                    <?php } else { ?>
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/memberlogin" class="link_w txt_bold"><?php echo Yii::y('member_login'); ?></a>
                            | 
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/memberregister" class="link_w txt_bold">สมัครสมาชิก</a> 
                        <?php } ?>
                    </div>

                    <div class="curency">
                        <select name="ddlCurrency" id="ddlCurrency" style="width:100px;border:none;outline:none;">
                            <option value='baht' <?php echo (Yii::app()->session['currency'] == 'baht') ? "selected='selected'" : ""; ?>>฿ (Thai Baht)</option>
                            <option value='dollar' <?php echo (Yii::app()->session['currency'] == 'dollar') ? "selected='selected'" : ""; ?>>$ (US Dollar)</option>
                        </select>
                    </div>

                    <div class="lang">

                        <select name="ddlLanguage" id="ddlLanguage" style="width:100px;border:none;outline:none;">
                            <option value='th' <?php echo (Yii::app()->language == 'th') ? "selected='selected'" : ""; ?> data-image="<?php echo Yii::app()->request->baseUrl; ?>/images/th.png" data-imagecss="" >Thai</option>
                            <option value='en' <?php echo (Yii::app()->language == 'en') ? "selected='selected'" : ""; ?> data-image="<?php echo Yii::app()->request->baseUrl; ?>/images/en.png" data-imagecss="" >English</option>
                        </select>
                    </div>

                    <!--                    <div class="login">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/login.png" width="20" height="17"  alt=""/> เข้าสู่ระบบ <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/drop-arrow.png" width="7" height="4"  alt=""/> 
                                        </div>-->
                </div>

                <div class="header-row">
                    <div class="menu-box">
                        <nav>
                            <ul class="flexnav">
                                <li class="ver-menu-right"><a href="/">Home</a></li>
                                <li class="ver-menu-left ver-menu-right"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/content/code/About_us">About us</a></li>
                                <li class="ver-menu-left ver-menu-right"><a href="#">Product</a>
                                    <ul style="z-index: 9999;padding-right:10px;width:380px;">
                                        <?php
                                        foreach ($data["product_group"] as $row) {
                                            $ProductSubGroup = ProductGroup::model()->findAll('parent_id=:parent_id', array(':parent_id' => $row->id));
                                            if (count($ProductSubGroup) > 0)
                                                echo "<li class='show_subgroup' productgroupid='" . $row->id . "'>";
                                            else
                                                echo "<li>";
                                            echo "<a href='" . Yii::app()->request->baseUrl . "/index.php/site/ProductGroup/id/" . $row->id . "'>";
                                            echo Yii::text_lang(array($row->group_1, $row->group_2, $row->group_3, $row->group_4, $row->group_5));
                                            echo "</a>";
                                            echo "</li>";

                                            if (count($ProductSubGroup) > 0) {
                                                echo "<div class='main_menu_subgroup_" . $row->id . "' style='display:none;'>";
                                                foreach ($ProductSubGroup as $rowSub) {
                                                    echo "<li style='width:380px;'>";
                                                    echo "&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;<a href='" . Yii::app()->request->baseUrl . "/index.php/site/ProductGroup/id/" . $rowSub->id . "'>";
                                                    echo Yii::text_lang(array($rowSub->group_1, $rowSub->group_2, $rowSub->group_3, $rowSub->group_4, $rowSub->group_5));
                                                    echo "</a>";
                                                    echo "</li>";
                                                }
                                                echo "</div>";
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li class="ver-menu-left ver-menu-right"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/NewsList">News & Promotion</a></li>
                                <li class="ver-menu-left ver-menu-right"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Forum">Webbaord</a></li>
                                <li class="ver-menu-left ver-menu-right"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/content/code/Pick_info">Pick info</a></li>
                                <li class="ver-menu-left"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/content/code/Contact_us">Contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>

            <div class="mainbox">
                <div class="mainbox-row">
                    <div class="product-slide">

                        <div><div class="wrapper">

                                <div class="jcarousel-wrapper">
                                    <div class="jcarousel">
                                        <ul>

                                            <?php
                                            foreach ($data["banner"] as $row) {
                                                ?>                       
                                                <li><a href="<?php echo $row["url"]; ?>" target="_blank"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/banner/<?php echo $row["id"]; ?>/<?php echo $row["img_src"]; ?>" width="289" height="234" alt=""></a></li>
                                            <?php } ?>


                                            <li><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/product-slide.png" width="289" height="234" alt=""></a></li>
                                            <li><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/product-slide.png" width="289" height="234" alt=""></a></li>
                                            <li><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/product-slide.png" width="289" height="234" alt=""></a></li>
                                        </ul>
                                    </div>

                                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                                    <a href="#" class="jcarousel-control-next">&rsaquo;</a>

                                </div>
                            </div></div>

                    </div>
                    <div class="ads-right">

                        <div class="news-box">
                            <div class="news-box-title">
                                <div class="news-box-title-text">
                                    News & <span class="txt-org">Promotion</span>
                                </div>
                                <div class="news-more">
                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/NewsList">อ่านข่าวทั้งหมด »</a> </div>
                            </div>

                            <div class="list-news">
                                <ul>
                                    <?php
                                    foreach ($data["news"] as $row) {
                                        ?>
                                        <li>
                                            <div class="box-left">
                                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/NewsDetail/id/<?php echo $row->id; ?>">
                                                    <?php
                                                    $file = Yii::app()->request->baseUrl . '/images/content/' . $row->attr1;
                                                    if (file_exists(Yii::app()->basePath . '/../images/content/' . $row->attr1)) {
                                                        ?>
                                                        <img src="<?php echo $file; ?>" alt="" />
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/news_no_image.jpg" alt="" />
                                                        <?php
                                                    }
                                                    ?>
                                                </a>
                                            </div>
                                            <div class="box-right">
                                                <div class="title-news">
                                                    <div class="title-news-txt"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/NewsDetail/id/<?php echo $row->id; ?>">
                                                            <?php
                                                            echo Yii::text_lang(array(
                                                                $row->title_1,
                                                                $row->title_2,
                                                                $row->title_3,
                                                                $row->title_4,
                                                                $row->title_5));
                                                            ?>
                                                        </a></div>
                                                    <div class="title-news-content">
                                                        <?php
                                                        echo Yii::text_lang(array(
                                                            $row->short_content_1,
                                                            $row->short_content_2,
                                                            $row->short_content_3,
                                                            $row->short_content_4,
                                                            $row->short_content_5));
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="title-date"><?php echo date_format(date_create($row->create_date), 'd-m-Y'); ?></div>
                                            </div>
                                        </li>

                                    <?php } ?>
                                </ul>
                            </div>
                        </div>


                        <div class="youtube-box">
                            <div class="youtube-box-title">
                                <div class="youtube-box-title-txt">
                                    Video <span class="txt-org">Gallery</span>
                                </div>
                                <div class="youtube-more">
                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/GalleryVideo/">วิดีโอทั้งหมด »</a> 
                                </div>
                            </div>
                            <div class="youtube-box-content">
                                <?php
                                if (count($data["img_gallery"]) > 0) {
                                    ?>
                                    <iframe width="440" height="248" src="http://www.youtube.com/embed/<?php echo $data["img_gallery"][0]->url; ?>" frameborder="0" allowfullscreen></iframe>
                                <?php } else { ?>
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/no_video.jpg" style="border:solid 1px #cccccc;" width="440" height="248"/> 
                                <?php } ?>
                            </div>

                        </div>

                    </div>
                </div> 

                <div class="clear-row">
                </div>

                <div class="mainbox-row">
                    <div class="mainbox-content-full">
                        <?php echo $content; ?>
                    </div>
                </div>

            </div>

            <div class="footer">
                <div class="footer-box">
                    <div class="footer-box-address">
<!--                        <p class="txt-bold">Copyright by BIFshoponline. All right reserved.</p>
                        <p>383 Ladya Rd., Somdejchaophaya
                            Klongsan, Bangkok 10600 Thailand</p>
                        <p><span class="txt-bold">Tel :</span> +662 438-3786 , +662 861-4805-10 </p>
                        <p><span class="txt-bold">Fax :</span> +662 437-7937 , +662 861-4829 </p>
                        <p><span class="txt-bold">E-mail :</span></p>
                        <p>  <span class="txt-bold">For Overseas market :</span> export @ bangkokinterfood.co.th</p>
                        <p> <span class="txt-bold">For Domestic market :</span> marketing @ bangkokinterfood.co.th</p>-->
                        <?php
                        if (count($data["footer"]) > 0) {
                            echo Yii::text_lang(array(
                                $data["footer"][0]->content_1,
                                $data["footer"][0]->content_2,
                                $data["footer"][0]->content_3,
                                $data["footer"][0]->content_4,
                                $data["footer"][0]->content_5));
                        }
                        ?>
                    </div>
                    <div class="footer-box-follow">
                        <p class="txt-bold">Follow me :</p>
<!--                        <a href="https://www.facebook.com/pages/Bangkok-Inter-Food-CoLtd/216450991777657" target="_blank"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/facebook.jpg" width="50" height="50" border="0"  alt=""/></a> -->
                        <a href="https://www.facebook.com/biffoodservice" target="_blank"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/facebook.jpg" width="50" height="50" border="0"  alt=""/></a> 
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line.jpg" width="50" height="50"  alt=""/> 
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ig.jpg" width="50" height="50"  alt=""/>
                        <p></p>
                    </div>
                    <div class="footer-box-link">
                        <p>Quick link : <select id="ddlQuickLink">
                                <option value="home">Home</option>
                                <option value="aboutus">About us</option>
                                <option value="product">Product</option>
                                <option value="news">News &amp; Promotion</option>
                                <option value="webboard">Webbaord</option>
                                <option value="pickinfo">Pick info</option>
                                <option value="contactus">Contatc us</option>
                            </select></p>
                        <p><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand.jpg" width="329" height="102"  alt=""/></p>
                    </div>
                </div>
            </div>

        </div>

        <script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript">
        </script>
        <script type="text/javascript">
            _uacct = "UA-2475233-1";
            urchinTracker();
        </script>

        <!-------------------------------------------------------- submenu -------------------------------------------------------------->
        <div class="cartmenu">      
            <div class="cart" id="sideMenuCart" title="จำนวนสินค้าในตะกร้า" ><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/cart.png" width="" height=""></div>

            <div class="search" id="sideSearchButton" title="ค้นหาสินค้า"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/search.png" width="" height=""></div>
        </div>
        <!-------------------------------------------------------- submenu -------------------------------------------------------------->

        <script type="text/javascript">
            jQuery(".slider .bd li").first().before(jQuery(".slider .bd li").last());
            jQuery(".slider").hover(function () {
                jQuery(this).find(".arrow").stop(true, true).fadeIn(300)
            }, function () {
                jQuery(this).find(".arrow").fadeOut(300)
            });
            jQuery(".slider").slide(
                    {titCell: ".hd ul", mainCell: ".bd ul", effect: "leftLoop", autoPlay: true, vis: 3, autoPage: true, trigger: "click"}
            );
        </script>


        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.flexnav.min.js" type="text/javascript"></script>
        <script type="text/javascript">$(".flexnav").flexNav();</script>

        <script type="text/javascript">
            $(document).ready(function () {
                // Search ------------------------------------------
                $('#sideSearchButton').click(function () {
                    location.href = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Search";
                });
                // Cart --------------------------------------------
                $('#sideMenuCart').click(function () {
                    location.href = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/cart";
                });
                // Quick link --------------------------------------
                $('#ddlQuickLink').change(function () {
                    var val = $(this).val();
                    var url;
                    switch (val) {
                        case "home":
                            url = "<?php echo Yii::app()->request->baseUrl; ?>";
                            break;
                        case "aboutus":
                            url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/content/code/About_us";
                            break;
                        case "product":
                            url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ProductGroup";
                            break;
                        case "news":
                            url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/NewsList";
                            break;
                        case "webboard":
                            url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Forum";
                            break;
                        case "pickinfo":
                            url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/content/code/Pick_info";
                            break;
                        case "contactus":
                            url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/content/code/Contact_us";
                            break;
                    }
                    location.href = url;
                });

                // Language switcher -------------------------------
                $("#ddlLanguage").msDropdown();
                $("#ddlLanguage").change(function () {
                    var url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Lang/code/" + $(this).val();
                    location.href = url;
                });

                // Currency switcher -------------------------------
                $("#ddlCurrency").msDropdown();
                $("#ddlCurrency").change(function () {
                    var url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Currency/code/" + $(this).val();
                    location.href = url;
                });

                // Sub product group mouse hover
                $('.show_subgroup').hover(function () {
                    $('[class^="main_menu_subgroup_"]').hide();
                    var subgroupid = $(this).attr('productgroupid');
                    $('.main_menu_subgroup_' + subgroupid).show();
                });
            });
        </script>
    </body>
    <!-- InstanceEnd -->
</html>
