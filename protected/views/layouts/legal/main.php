<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SAM</title>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/css.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl;?>/css/npa_dt.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-latest.js"></script>
<script language="javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ddsmoothmenu.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/amazon_scroller.js"></script> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/slide.js"></script>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/thickbox.js"></script>
<link href='<?php echo Yii::app()->request->baseUrl; ?>/images/favicon2.ico' rel='shortcut icon' type='image/x-icon'/>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ddsmoothmenu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/thickbox.css" type="text/css" media="screen" />
<style type="text/css">
@media print{
	.noPrint{
		display:none;
	}
}
</style>
<script type="text/javascript">
function printPreview() {
	var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	if(navigator.appName == "Microsoft Internet Explorer"){
	if(!document._wb){
		var obj="<object id='_wb' width='0' height='0' ";
		obj+=" classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>";
		document.body.insertAdjacentHTML(
			"beforeEnd",obj
		); 
	}
	_wb.ExecWB(7,1)
	}else if(is_chrome){
		window.print();
	}else{
		var windowWidth=850;
		var windowHeight=550;
		var myleft=(screen.width)?(screen.width-windowWidth)*0.5:100;    
   		var mytop=(screen.height)?(screen.height-windowHeight)*0.5:100;
		var feature='left='+myleft+',top='+eval(mytop-50)+',width='+windowWidth+',height='+windowHeight+',';
		feature+='menubar=yes,status=no,location=no,toolbar=no,scrollbars=yes';
		window.open(window.location+'?print_preview=1','welcome',feature);
	}
}
function getQuerystring(key, default_)
{
  if (default_==null) default_=""; 
  key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return default_;
  else
    return qs[1];
}
$(function(){
		   $(".PrintIt").hide();
		   if(getQuerystring('print_preview')=="1"){
		   		$(".noPrint").hide();
				$(".PrintIt").show();
		   }
		   });
</script>
<body>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="noPrint">
    <td align="center"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/bannertop.jpg" width="940" height="72" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" class="header"><table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><a href="<?php echo Yii::app()->request->baseUrl;?>/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" width="154" height="100" /></a></td>
        <td valign="bottom"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td width="327" align="right" class="top_panel2 txt_bold">
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/member.png" alt="" width="16" height="15" /> 
                <?php 
				$session=new CHttpSession;
				$session->open();
				$user = $session['user'];
				if(isset($user)){
					?>
                     ยินดีต้อนรับ คุณ. <?php echo $user->first_name . ' '  . $user->last_name;?> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/MemberLogout/">ออกจากระบบ</a>
                    <?php
				}else{
					?>
                    <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/MemberLogin/">สมาชิกเข้าสู่ระบบ</a>
                    <?php
				}
				?>
               
                
                &nbsp;&nbsp;</td>
                <td width="78" align="center" class="top_panel2 txt_bold">E-mail :&nbsp;&nbsp;<a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/icon-mail.png" width="16" height="16" /></a></td>
                <td align="right" width="174" class="top_panel2 txt_bold">ภาษา : <?php 
				$model = new Lang();
				$setting = Setting::model()->findByPk(1);
				
				$dlang = $model->search()->data;
				foreach($dlang as $row){
				?>
                <a href="#">
                <?php
				
				if($setting->lang_use_icon=="1"){
				?>
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/flag/<?php echo $row->lang_icon;?>" width="16" height="11" /> </a>
                <?php
				}
				if($setting->lang_use_text=="1")
					echo $row->lang_txt;
				}
				?></a></td>
                <td align="right"  width="200" class="top_panel txt_bold"><input name="textfield" type="text" id="textfield" value="ค้นหาข้อมูลภายในเว็บไซต์" />
                  <a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/b_search.png" width="36" height="20" /></a></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="31"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/menu_left.png" width="31" height="36" /></td>
                    <td class="menu_top">
                    <strong>
          <script type="text/javascript"> 
ddsmoothmenu.init({
mainmenuid: "smoothmenu_main",
orientation: 'h', 
classname: 'ddsmoothmenu', 
contentsource: "markup"
})
          </script>
          </strong>
          <div id="smoothmenu_main" class="ddsmoothmenu">
            <ul>
              <li><strong><a href="<?php echo Yii::app()->request->baseUrl; ?>/">หน้าแรก</a></strong></li>
              <li class="bor_left"><strong><a href="javascript:">รู้จัก SAM</a></strong>
                <ul>
                  <?php
                  
				  
						$mm = new ContentGroup();
						$part = 'COP';
						$mm->part = $part;
						$mm->parent_id= '1';	
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li><a href='"  . Yii::app()->request->baseUrl . "/index.php/site/group/" . $row->id ."' target='_blank'>" . $row->group_name_th . "</a></li>\n";
						}
						 ?>
                         <!--
                         <li><a href="<?php echo Yii::app()->request->baseUrl ;?>/index.php/site/detail/42" target='_blank'>โครงสร้างองค์กร</a></li>
                         <li><a href="<?php echo Yii::app()->request->baseUrl ;?>/index.php/site/detail/138" target='_blank'>รายชื่อกรรรมการ/ผู้บริหาร</a></li>
                         <li><a href="<?php echo Yii::app()->request->baseUrl ;?>/index.php/site/detail/85" target='_blank'>เงื่อนไขการใช้บริการ</a></li>
                         <li><a href="<?php echo Yii::app()->request->baseUrl ;?>/index.php/site/detail/38" target='_blank'>ที่มาขององค์กร</a></li>
                         -->
                         
                </ul>
              </li>
              <li class="bor_left"><strong><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/parent/2">ข่าวสาร</a></strong>
                <ul>
                 <?php
						$mm = new ContentGroup();
						$part = 'COP';
						$mm->part = $part;
						$mm->parent_id= '2';	
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li><a href='"  . Yii::app()->request->baseUrl . "/index.php/site/group/" . $row->id ."' target='_blank'>" . $row->group_name_th . "</a></li>\n";
						}
						 ?>
                         
                         </ul>
              </li>
              <li class="bor_left"><strong><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/parent/3">ประกาศ</a></strong>
              <ul>
                 <?php
						$mm = new ContentGroup();
						$part = 'COP';
						$mm->part = $part;
						$mm->parent_id= '3';	
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li><a href='"  . Yii::app()->request->baseUrl . "/index.php/site/group/" . $row->id ."' target='_blank'>" . $row->group_name_th . "</a></li>\n";
						}
						 ?>
                         
                         </ul>
              </li>
              <li class="bor_left"><strong><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/parent/4">ประชาสัมพันธ์ </a></strong>
               <ul>
                 <?php
						$mm = new ContentGroup();
						$part = 'COP';
						$mm->part = $part;
						$mm->parent_id= '4';	
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li><a href='"  . Yii::app()->request->baseUrl . "/index.php/site/group/" . $row->id ."' target='_blank'>" . $row->group_name_th . "</a></li>\n";
						}
						 ?>
                         <li><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/ebook/">
                         วารสาร</a></li>
                         <li><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/photoGallery/">
                         แกลอรี่รูปภาพ</a></li>
                         <li><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/videoGallery/">
                         วิดีโอ</a></li>
                         </ul>
              </li>
              <li class="bor_left"><strong><a href="javascript:">ติดต่อเรา</a></strong>
              <ul>
               <?php
						$mm = new ContentGroup();
						$part = 'COP';
						$mm->part = $part;
						$mm->parent_id= '6';	
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li><a href='"  . Yii::app()->request->baseUrl . "/index.php/site/group/" . $row->id ."' target='_blank'>" . $row->group_name_th . "</a></li>\n";
						}
						 ?>
                         <li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/site/linkexchange/'>แลกลิ้ง</a></li>
                         <li><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/site/joblist/'>ร่วมงานกับเรา</a></li>
              </ul>
              </li>
              
            </ul>
          </div>
          </td>
                    <td width="11"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/menu_right.png" width="11" height="36" /></td>
                  </tr>
                </table></td>
                <td>&nbsp;</td>
                <td class="npa_bg txt_bold"><a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/npa/' target="_blank" style='color:white;'>ทรัพย์สินพร้อมขาย (NPA)</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="bg_main">
	<table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr> 
        <td class="npamain_content">
<table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" class="npa_main"><table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top" class="">
	<?php echo $content; ?>

	<div class="clear"></div>
</td>
            </tr>
          </table></td>
      </tr>
      </table>
	 </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="footer"><table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td class="txt_footer"><p class="txt_bold">Copyright © 2012 บริษัท บริหารสินทรัพย์สุขุมวิท จำกัด. All rights reserved.</p>
          <p>            สำนักงานใหญ่  :  123 อาคารซันทาวเวอร์ส เอ ชั้น 27-30 ถนนวิภาวดีรังสิต  แขวงจอมพล เขตจตุจักร  กรุงเทพฯ 10900</p>
          <p> โทรศัพท์ :  0-2686-1800, 0-2620-8999, 0-2610-2222   โทรสาร :  0-2617-8230-33, 0-2617-8235, 0-2617-8244-46</p></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>

