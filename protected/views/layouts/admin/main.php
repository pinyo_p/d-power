<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/fav.png">
        <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/admin/css/style.css" rel="stylesheet" type="text/css" />
        <?php
        $cs = Yii::app()->clientScript;
        $cs->scriptMap = array(
            'jquery.js' => Yii::app()->request->baseUrl . '/protected/vendors/js/jquery-1.8.2.min.js',
            'jquery-ui.min.js' => Yii::app()->request->baseUrl . '/protected/vendors/js/jquery-ui-1.8.24.custom.min.js',
        );
        Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/protected/vendors/css/eggplant/jquery-ui-1.8.24.custom.css', 'screen'
        );
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        ?>
        <?php
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/Admin/login');
        }
        $premission = array();
        $premission = Premission::model()->findAll('user_id=:user_id', array('user_id' => Yii::app()->user->getValue("group_id")));
        ?>
        <script language="javascript">
            $(function () {
                $(".hide_it").hide("fade");
<?php
if (count($premission) > 0) {
    foreach ($premission as $prem) {
        //echo "$('.lmm_"  . $prem->menu_id ."').attr('style','visibility:visible;position:static');\n";
        echo "$('.lmm_" . $prem->menu_id . "').show('fade');\n";
    }
}
?>

            });
        </script>
    </head>

    <body>
        <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td width="170" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/1_banner-block copy.png" width="170" /></td>
                        </tr>
                    </table>
                    <table id="status" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="2" valign="top" class="text1">วันที่ <?php echo date("Y-m-d"); ?></td>
                        </tr>
                        <tr>
                            <td width="65" rowspan="2" align="center" valign="middle" class="text1"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/avatar.png" width="50" height="45" /></td>
                            <td width="104" valign="middle" class="text2"><?php echo Yii::app()->user->getValue("first_name"); ?></td>
                        </tr>
                        <tr>
                            <td valign="middle" class="text1"><form id="form1" name="form1" method="post" action="">
                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/logout" style="text-decoration:none;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logout.png" width="58" height="21" border="0" /></a>
                                </form></td>
                        </tr>
                    </table>




                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="menucategory">
                        <tr>
                            <td valign="middle" class="head"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_1.png" width="19" height="19" /> จัดการเนื้อหาในเว็บไซต์ </td>
                        </tr>
                        <tr>
                            <td><ul>
                                    <li class="hide_it lmm_1"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ContentList">เนื้อหาเว็บไซต์</a></li>
                                    <li class="hide_it lmm_2"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ContentNewsList">ข่าวสาร & โปรโมชั่น</a></li>
                                    <li class="hide_it lmm_3"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/AlbumPictureList">อัลบั้มรูปภาพ</a></li>
                                    <li class="hide_it lmm_4"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/AlbumVideoList">อัลบั้ม Video</a></li>
<!--                                    <li class="hide_it lmm_1"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/MemberPolicy">ประโยชน์จากการสมัครสมาชิก</a></li>-->
<!--                                    <li class="hide_it lmm_2"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/History">ประวัติของเรา</a></li>-->
                                    <li class="hide_it lmm_5"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/PartnerList">กลุ่มบริษัทในเครือ</a></li>
<!--                                    <li class="hide_it lmm_2"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ContactUs">ติดต่อเรา</a></li>-->
                                    <li class="hide_it lmm_6"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BranchList">สาขา</a></li>
                                    <li class="hide_it lmm_5_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BankList">บัญชีธนาคาร</a></li>
<!--                                    <li class="hide_it lmm_6"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Policy">เงื่อนไขการใช้งานเว็บไซต์ </a></li>-->
                                    <li class="hide_it lmm_6_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Credit">คำขอเปิดบัญชีเครดิต</a></li>
<!--                                    <li class="hide_it lmm_6"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Reward">แลกคะแนนรับของรางวัล</a></li>-->
<!--                                    <li class="hide_it lmm_6"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/QuotationForm">ใบเสนอราคาและใบสั่งซื้อ</a></li>-->
                                    <li class="hide_it lmm_6_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/PromotionList">โปรโมชั่น</a></li>




<!--                                    <li class="hide_it lmm_7"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/SalePolicy">ข้อกำหนดและเงื่อนไขการขาย</a></li>-->
<!--                                    <li class="hide_it lmm_8"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Protection">การป้องกันข้อมูล</a></li>-->
<!--                                    <li class="hide_it lmm_9"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Shipping">การจัดส่งสินค้า</a></li>-->
                                    <li class="hide_it lmm_10_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Repair">Repair</a></li>
                                    <li class="hide_it lmm_11_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/TrainingList">Training</a></li>
                                    <li class="hide_it lmm_11_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/TrainingDesign">ออกแบบหน้าจอ Training</a></li>
<!--                                    <li class="hide_it lmm_12"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Faq">FAQ</a></li>-->
                                    <li class="hide_it lmm_7"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/DayList">วันสำคัญ</a></li>
                                    <li class="hide_it lmm_8"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BannerDayList">แบนเนอร์วันสำคัญ</a></li>


                                    <li class="hide_it lmm_28"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BannerSlideList">แบนเนอร์ Slide</a></li>
                                    <li class="hide_it lmm_9"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BannerList">แบนเนอร์</a></li>
                                    <li class="hide_it lmm_10"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Popup">Popup</a></li>
                                    <li class="hide_it lmm_11"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Intro">Intro Page</a></li>
                                    <li class="hide_it lmm_12"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Footer">Footer</a></li>
<!--                                    <li class="hide_it lmm_20"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Sitemap">Site map</a></li>-->

                                    <li class="hide_it lmm_22_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BrandLink">ตั้งค่า Brand Link</a></li>
                                    <li class="hide_it lmm_13"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/CatalogList">Catalogue</a></li>
                                    <li class="hide_it lmm_24_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ManualList">คู่มือสินค้า</a></li>
                                    <li class="hide_it lmm_24_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ServiceList/1">ผู้ลงทะเบียน Technicial Support</a></li>
                                    <li class="hide_it lmm_24_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ServiceList/2">ผู้ลงทะเบียน Repair</a></li>
                                    <li class="hide_it lmm_24_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/TraineeList">ผู้ลงทะเบียน Training</a></li>

                                    <li class="hide_it lmm_14"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ContactList/0">รายชื่อผู้ส่งคำขอติดต่อ</a></li>
                                    <li class="hide_it lmm_15"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ContactList/1">ผู้ขอใบเสนอราคา</a></li> 

                                    <li class="hide_it lmm_16"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Setting">ตั้งค่าเว็บไซต์</a></li>
                                </ul></td>
                        </tr>
                    </table>


                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="menucategory">
                        <tr>
                            <td valign="middle" class="head"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_2.png" width="22" height="22" /> จัดการสินค้า</td>

                        </tr>
                        <tr>
                            <td><ul>
                                    <li class="hide_it lmm_25_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BrandGroupList">ประเภทสินค้า</a></li>
                                    <li class="hide_it lmm_26_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BrandList">แบรนด์สินค้า</a></li>
                                    <li class="hide_it lmm_17"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ProductGroupList">หมวดหมู่สินค้า</a></li>   
                                    <li class="hide_it lmm_28_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/SeriesList">Series สินค้า</a></li>          
                                    <li class="hide_it lmm_18"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ProductList">รายการสินค้า</a></li>
                                    <li class="hide_it lmm_19"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/OrderList">รายการสั่งซื้อ</a></li>
                                    <li class="hide_it lmm_20"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ShippingList">รายการที่จัดส่ง</a></li>
                                    <li class="hide_it lmm_29"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ShipmentRateList">อัตราค่าขนส่ง</a></li>
                                    <li class="hide_it lmm_21"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ConfigCatalog">ตั้งค่าระบบซื้อขาย</a></li>
                                </ul></td>
                        </tr>
                    </table>









                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="menucategory">
                        <tr>
                            <td valign="middle" class="head"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_2.png" width="19" height="19" />การรับข่าวสาร</td>
                        </tr>
                        <tr>
                            <td><ul>
                                    <li class="hide_it lmm_22"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/SendNewsList">จดหมายข่าวสาร</a></li>
                                    <li class="hide_it lmm_23"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/SubscriberList">ข้อมูลผู้รับข่าวสาร</a></li>
                                    <li class="hide_it lmm_24"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/NewsletterConfig">ตั้งค่าระบบรับข่าวสาร</a></li>
                                </ul></td>
                        </tr>
                    </table>           




                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="menucategory">
                        <tr>
                            <td valign="middle" class="head"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_3.png" width="17" height="21" /> ผู้ใช้งาน</td>
                        </tr>
                        <tr>
                            <td><ul>
                                    <li class="hide_it lmm_25"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/UserGroupList">กลุ่มผู้ใช้งาน</a></li>
                                    <li class="hide_it lmm_26"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/UserList">ผู้ใช้งาน</a></li>
                                    <!--li><a href="#">แก้ไขรหัสผ่าน</a></li-->
                                    <!--li><a href="#">แก้ไขข้อมูลส่วนตัว</a></li-->
                                </ul></td>
                        </tr>
                    </table> 







                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="menucategory">
                        <tr>
                            <td valign="middle" class="head"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_4.png" width="19" height="19" /> อื่นๆ</td>
                        </tr>
                        <tr>
                            <td><ul>
                                    <li class="hide_it lmm_27"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/MemberList">รายชื่อสมาชิก</a></li>
                                    <li class="hide_it lmm_33_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/JobList">ตำแหน่งงาน</a></li>
                                    <li class="hide_it lmm_34_disabled"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/JobbeeList">ผู้สมัครงาน</a></li>
                                </ul></td>
                        </tr>
                    </table>


                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="stat">
                        <tr>
                            <td colspan="2" valign="top"><span class="text1">สถิติการเข้าชมเว็บไซต์</span><br /></td>
                        </tr>
                        <tr>
                            <td valign="top"><span class="text3">เริ่มนับสถิติ</span></td>
                            <td align="right" valign="top"><span class="text2">21 ก.ค. 55</span></td>
                        </tr>
                        <tr>
                            <td valign="top"><span class="text3">จำนวนผู้เยี่ยมชม</span></td>
                            <td align="right" valign="top"><span class="text2"><?php
                                    $sql = 'select sum(`amount`) as `sum` from tb_static';
                                    $sum = Yii::app()->db->createCommand($sql)->queryScalar();
                                    echo number_format($sum, 0, ".", ",");
                                    /* $stat = Stat::model()->findBySql(, array());
                                     */
                                    ?> ครั้ง</span></td>
                        </tr>
                        <tr>
                            <td valign="top"><span class="text3">จำนวนผู้ออนไลน์</span></td>
                            <td align="right" valign="top"><span class="text2"><?php
                                    $sql = "SELECT COUNT(*) FROM tb_online_user";
                                    $count = Yii::app()->db->createCommand($sql)->queryScalar();
                                    echo $count;
                                    ?> คน</span></td>
                        </tr>
                        <tr>
                            <td valign="top"><span class="text3">วันที่ Run เว็บ</span></td>
                            <td align="right" valign="top"><span class="text2">21 ก.ค. 55</span></td>
                        </tr>
                    </table>
                </td>
                <td width="15" rowspan="5" valign="top">&nbsp;</td>
                <td rowspan="5" valign="top" class="main_bg">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="head_tab">
                        <tr>
                            <td width="54"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin"><img border="0" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/head_home.png" width="54" height="32" /></a></td>
                            <td>
                                <table width="99%" border="0" align="left" cellpadding="0" cellspacing="0" class="topic">
                                    <tr>
                                        <td width="5" height="32"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/head_tab_left.png" width="5" height="32" /></td>
                                        <td class="bg">&nbsp;<img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/arrow.png" width="11" height="6" />&nbsp;
                                            <?php
                                            echo CHtml::encode($this->breadcrumbs);
                                            ?>
                                        </td>
                                        <td width="37" align="right"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/head_tab_right.png" width="37" height="32" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?php echo $content; ?>
                            </td>
                        </tr>
                    </table> 
                </td>
            </tr>

        </table>







        <br />
        <br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="footer">

                    <?php
                    $content = Content::model()->find('content_code=:content_code', array(':content_code' => 'Footer'));
                    echo Yii::text_lang(array($content->content_1, $content->content_2, $content->content_3, $content->content_4, $content->content_5));
                    ?>

                </td>
            </tr>
        </table>
    </body>
</html>
