<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>บริษัท บริหารสินทรัพย์สุขุมวิท จำกัด</title>
<link href="<?php echo Yii::app()->request->baseUrl;?>/css/css.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/ddsmoothmenu.js"></script>
<script language="javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/amazon_scroller.js"></script> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/slide.js"></script>
<link href='<?php echo Yii::app()->request->baseUrl; ?>/images/favicon2.ico' rel='shortcut icon' type='image/x-icon'/>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/thickbox.js"></script>
<link href="<?php echo Yii::app()->request->baseUrl;?>/css/ddsmoothmenu_out.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/css/thickbox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/basic-jquery-slider.css">
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/basic-jquery-slider.js"></script>
<?php
$session=new CHttpSession;
				$session->open();
				$user = $session['user'];
				$login = false;
				if(!isset($user)){
					?>
                    <script language="javascript">
					window.location.href='<?php echo Yii::app()->request->baseUrl;?>/index.php/site/MemberLogin/';
					</script>
                    <?php
				}else{
					if($user->part!="OUT"){
					?>
                    <script language="javascript">
					alert("ชื่อผู้ใช้ของคุณไม่สามารถเข้าส่วนนี้ได้");
					window.location.href='<?php echo Yii::app()->request->baseUrl;?>/index.php/site/MemberLogin/';
					</script>
                    <?php
					}
				}
?>
<style type="text/css">
<?php
$setting = Setting::model()->findByPk(1);
$bg = CopIndex::model()->find("part='COP' and `group`='3' and is_selected='1'");

?>
<?php
	if(isset($bg->imgsrc)){
	?>
	body{
	background-image:url('<?php echo Yii::app()->request->baseUrl. '/images/COP/index/' . $bg->imgsrc;?>');
	background-position:top;
	background-repeat:repeat-x;
	}

	<?php
	}?>
</style>
<script>
      $(document).ready(function() {
        
        $('#banner').bjqs({
          'animation' : 'fade',
          'width' : 940,
		  'animationDuration': 650,
		  'rotationSpeed': 2000,
          'height' : 290,
		  'showControls': false,
		  'useCaptions': false,
		  'centerMarkers': false,
        });
        
      });
	  function setLang(objLang){
		  $.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/site/SetLang/',{lang:objLang},function(data){
																											  location.reload();
																											  });
	  }
    </script>
</head> 

<body>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php
  if($setting->show_banner_day=="1"){
$bday = CopIndex::model()->find("part='COP' and `group`>='2001' and `group`<='2009' and is_selected='1'");
if(count($bday)>0){
?>
  <tr>
  

    <td align="center" ><img  src="<?php echo Yii::app()->request->baseUrl. '/images/COP/index/' . $bday->imgsrc;?>" width="940" height="72" /></td>
  </tr>
  <?php
}
  }
?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" class="header"><table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr><td>
       <?php
	 	$icon = CopIndex::model()->find("part='COP' and `group`=2 and is_selected='1'");
	  ?>
        <td><a href="<?php echo Yii::app()->request->baseUrl;?>" ><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/COP/index/<?php echo $icon->imgsrc;?>" width="154" height="100" /></a></td>

        <td valign="bottom"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
            <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td width="327" align="right" class="top_panel2 txt_bold">
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/member.png" alt="" width="16" height="15" /> 
                <?php 
				
				if(isset($user)){
					$login = true;
					?>
                      <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/MemberLogout/">ออกจากระบบ</a>
                    <?php
				}else{
					?>
                    <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/MemberLogin/"><?php echo Yii::y('member_login');?></a>
                    <?php
				}
				?>
               
                
                &nbsp;&nbsp;</td>
                <td width="78" align="center" class="top_panel2 txt_bold"><?php if($setting->webmail_show=="1"){
					?>
                 E-mail :&nbsp;&nbsp; <a href='<?php echo $setting->webmail_link;?>' target="_blank"><img src="<?php echo Yii::app()->baseUrl;?>/images/icon-mail.png" /></a>
                <?php
				}?></td>
                <td align="right" width="174" class="top_panel2 txt_bold"><?php echo Yii::y('language');?> : <?php 
				$model = new Lang();
				$setting = Setting::model()->findByPk(1);
				
				$dlang = $model->search()->data;
				foreach($dlang as $row){
				?>
                <a href="javascript:setLang('<?php echo $row->lang_code;?>')">
                <?php
				
				if($setting->lang_use_icon=="1"){
				?>
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/flag/<?php echo $row->lang_icon;?>" width="16" height="11" /> 
                <?php
				}
				if($setting->lang_use_text=="1")
					echo $row->lang_txt;
				}
				?></a></td>
                <td align="right"  class="top_panel txt_bold"> <form action="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/showsearch/" method="get" target="_blank">

                <input name="keysearch" type="text" id="keysearch" style="width:130px" title="ค้นหาข้อมูลภายในเว็บไซต์" />
                  <input type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/b_search.png" width="36" height="20" />
                                  </form></td>
              </tr>
              
              
              
            </table></td>
          </tr>
          
            <?php
			if(isset($user)){
					$login = true;
					?><tr>
            <td style="padding-left:50px;"><b>
                     ยินดีต้อนรับ <?php echo $user->first_name . ' '  . $user->last_name;?> 
                     </b>
                     </td>
          </tr>
                    <?php
			}
				
			?>
            
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="31"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/menu_left.png" width="31" height="36" /></td>
                    <td class="menu_top"><strong>
                      <script type="text/javascript"> 
ddsmoothmenu.init({
mainmenuid: "smoothmenu_main",
orientation: 'h', 
classname: 'ddsmoothmenu', 
contentsource: "markup"
})
            </script>
                      </strong>
                      <div id="smoothmenu_main" class="ddsmoothmenu">
                        <ul>
                          <li><strong><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/parent/1">ประกาศ</a></strong>
                         <ul>
                         <?php
						$part = 'OUT';
						
						$mm = new ContentGroup();
						$mm->part = $part;
						$mm->parent_id=1;
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li style='text-align:left;padding-left:15px;'><a href='"  . Yii::app()->request->baseUrl . "/index.php/outter/group/" . $row->id ."' target='_blank'>&#8226;&nbsp;&nbsp;" . $row->group_name_th . "</a></li>\n";
						}
						 ?>
                         </ul>
                          </li>
                          <li class="bor_left"><strong><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/parent/2">บทความ</a></strong>
                           <ul>
                         <?php
						
						$mm = new ContentGroup();
						$mm->part = $part;
						$mm->parent_id=2;
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li style='text-align:left;padding-left:15px;'><a href='"  . Yii::app()->request->baseUrl . "/index.php/outter/group/" . $row->id ."' target='_blank'>&#8226;&nbsp;&nbsp;" . $row->group_name_th . "</a></li>\n";
						}
						 ?>
                         </ul>
                          </li>
                          <li class="bor_left"><strong><a href="<?php echo Yii::app()->request->baseUrl . '/index.php/outter/DownloadList/'?>">ดาวน์โหลด</a></strong>
                            <ul>
                            <li style='text-align:left;padding-left:15px;'><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/DownloadList/1'>&#8226;&nbsp;&nbsp;แบบฟอร์มคดี ฝ่ายกฎหมาย</a></li>
                            <li style='text-align:left;padding-left:15px;'><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/DownloadList/2'>&#8226;&nbsp;&nbsp;หนังสือมอบอำนาจ</a></li>
                            <li style='text-align:left;padding-left:15px;'><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/DownloadList/3'>&#8226;&nbsp;&nbsp;แบบฟอร์มการเบิกจ่าย</a></li>
                            
                            <li style='text-align:left;padding-left:15px;'><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/DownloadList/5'>&#8226;&nbsp;&nbsp;อัตราค่าวิชาชีพและค่าใช้จ่ายดำเนินคดี</a></li>
                             <li style='text-align:left;padding-left:15px;'><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/DownloadList/6'>&#8226;&nbsp;&nbsp;รายละเอียดประกอบการโอนเงินค่าจ้าง</a></li>
                             <li style='text-align:left;padding-left:15px;'><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/DownloadList/4'>&#8226;&nbsp;&nbsp;โปรแกรมที่เกี่ยวข้อง</a></li>
                            </ul>
                          </li>
                          <li class="bor_left"><strong><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/parent/3">ผลการประเมิน</a></strong>
                           <ul>
                         <?php
						
						$mm = new ContentGroup();
						$mm->part = $part;
						$mm->parent_id=3;
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li style='text-align:left;padding-left:15px;'><a href='"  . Yii::app()->request->baseUrl . "/index.php/outter/group/" . $row->id ."' target='_blank'>&#8226;&nbsp;&nbsp;" . $row->group_name_th . "</a></li>\n";
						}
						 ?>
                         </ul>
                          </li>
                          <li class="bor_left"><strong><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/parent/4">คำถามที่พบบ่อย</a></strong>
                           <ul>
                         <?php
						/*
						$mm = new ContentGroup();
						$mm->part = $part;
						$mm->parent_id=4;
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li style='text-align:left;padding-left:15px;'><a href='"  . Yii::app()->request->baseUrl . "/index.php/outter/group/" . $row->id ."' target='_blank'>&#8226;&nbsp;&nbsp;" . $row->group_name_th . "</a></li>\n";
						}*/
						 ?>
                         </ul>
                          </li>
                          <li class="bor_left"><strong><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/parent/5">ติดต่อ</a></strong>
                          		<ul>
                         <?php
						/*
						$mm = new ContentGroup();
						$mm->part = $part;
						$mm->parent_id=5;
						$data = $mm->searchByPartAndParent()->data;
						foreach($data as $row){
						 echo "<li style='text-align:left;padding-left:15px;'><a href='"  . Yii::app()->request->baseUrl . "/index.php/outter/group/" . $row->id ."' target='_blank'>&#8226;&nbsp;&nbsp;" . $row->group_name_th . "</a></li>\n";
						}*/
						 ?>
                         </ul>
                        </ul>
                      </div></td>
                    <td width="11"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/menu_right.png" width="11" height="36" /></td>
                  </tr>
                </table></td>
                <td>&nbsp;</td>
                <td class="npa_bg txt_bold"><a  class="npa_bg txt_bold" style="color:white;" href='<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/propertylist/'>รายการทรัพย์ของท่าน</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="bg_main">
    
    <table width="940" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td>
    <?php
$bmodel = new Banner();
	$bdata = $bmodel->getlistByPart('COP')->data;
	$i = 0;
	if(count($bdata)>0){
?>
    <div class="container">
    
    <div id="banner">
        <!-- start Basic Jquery Slider -->
        <ul class="bjqs">
        
         <?php 
	
		foreach($bdata as $row){
			$i++;
			$url = str_replace(strtolower("http://http://"),strtolower("http://"),"http://" . $row->link);
	?><li>
    <a href="<?php echo $url;?>" target="_blank"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/slide/COP/<?php echo $row->img;?>" alt="" width="940" height="290" /></a></li>
    <?php 
		}
	
		?>
        
        </ul>
        <!-- end Basic jQuery Slider -->
      </div>
    </div>
    <?php }?>
    </td>
  </tr>
</table>
                        <center>
                        <img src="<?php echo Yii::app()->request->baseUrl;?>/images/PN_long.jpg" />
                        </center>
                        </td>
  </tr>
  <tr>
    <td class="main_content"><table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" class="marq_news txt_bold txt_gray"><marquee scrolldelay="90" direction="left">
          <?php
		  $mm = new Marquee();
		  $mm->part = 'COP';
		  $mdata = $mm->searchByPart()->data;
		foreach($mdata as $marquee){
			echo '<span class="sidebox_npaheader">&#8226;</span> ' . $marquee->text_th . ' &nbsp;&nbsp;&nbsp;&nbsp;';
		}
		?>
        </marquee></td>
      </tr>
      <tr>
        <td valign="top" class="bg_w">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="osboxcontent">
          <tr>
            <td valign="top" class="column-left">
              
          <?php echo $content; ?></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="footer"><table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td class="txt_footer"><p class="txt_bold">Copyright © 2012 บริษัท บริหารสินทรัพย์สุขุมวิท จำกัด. All rights reserved.</p>
          <p>            สำนักงานใหญ่  :  123 อาคารซันทาวเวอร์ส เอ ชั้น 27-30 ถนนวิภาวดีรังสิต  แขวงจอมพล เขตจตุจักร  กรุงเทพฯ 10900</p>
          <p> โทรศัพท์ :  0-2686-1800, 0-2620-8999, 0-2610-2222   โทรสาร :  0-2617-8230-33, 0-2617-8235, 0-2617-8244-46</p></td>
      </tr>
    </table></td>
  </tr>
</table>
</body></html>
