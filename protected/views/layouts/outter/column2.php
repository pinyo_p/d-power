﻿<?php $this->beginContent('//layouts/main');
$criteria=new CDbCriteria;
		$criteria->select = array('t.*,category.product_group as product_group,tambol.thai_name as tambol_name,district.thai_name as district_name,provinces.thai_name as province_name');
		$criteria->join ='LEFT JOIN product_group `category` ON `category`.id = t.product_type ';
		$criteria->join .='LEFT JOIN tambol on tambol.id=t.tambol ';
		$criteria->join .='LEFT JOIN district on district.id=t.district ';
		$criteria->join .='LEFT JOIN provinces on provinces.province_id=t.province ';
		$criteria->condition = "`t`.`status` <> 3 and `t`.`status` <> 0 and is_highlight=1 ";
		$criteria->order = 't.highlight_order asc';
		$model = Property::model();
		$highlight = $model->getCommandBuilder()
				   ->createFindCommand($model->tableSchema, $criteria)
				   ->queryAll();

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="220" valign="top" class="column-left"><div class="sideboxcomlumnbox sideboxcontent_bor">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="sideboxheader">LOGIN</td>
                </tr>
                <tr>
                  <td class="sideboxcontent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td height="30"><input name="textfield2" type="text" id="textfield2" value="username" size="15" /></td>
                          <td height="30"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/sumit.png" width="58" height="23" /></a></td>
                        </tr>
                        <tr>
                          <td height="30"><input name="textfield3" type="text" id="textfield3" value="password" size="15" /></td>
                          <td height="30"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/cancle.png" width="57" height="23" /></a></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="txt_center">
                          <td width="50%" height="30"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow1.png" width="6" height="7" /> <a href="#" class="link_side">สมัครสมาชิก</a></td>
                          <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow1.png" width="6" height="7" /> <a href="#" class="link_side">ลืมรหัสผ่าน</a></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
              </table>
            </div>
              <div class="sideboxcomlumnbox sideboxcontent_bor">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="sideboxheader">ทรัพย์เด่น</td>
                  </tr>
                  <tr>
                    <td class="sideboxcontent"><div class="recomm_npa">
                      <ul>
                      <?php
				foreach($highlight as $row)
				{
				?>
                        <li>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td height="30" class="txt_pink txt_bold">รหัสสินทรัพย์ : <?php echo $row['product_code'];?></td>
                            </tr>
                            <tr>
                              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="105" valign="top"><?php
echo CHtml::image(Yii::app()->request->baseUrl . "/images/npa/" . $row['id'] ."/main_image.jpg", "",array("width"=>"98px"));
?></td>
                                  <td valign="top">เลขที่ <?php echo $row['home_no'];?> <?php echo g($row['moo'],'หมู่ที่');?> <?php echo g($row['project'],'หูม่บ้าน/โครงการ');?>
    <?php echo g($row['soi'],'ซอย');?> <?php echo g($row['road'],'ถนน');?> <?php echo g($row['tambol_name'],'ต.');?> 
    <?php echo g($row['district_name'],'อ.');?> <?php echo g($row['province_name'],'จ.');?></td>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
                        </li>
                       <?php
				}
				?>
                      </ul>
                    </div></td>
                  </tr>
                </table>
              </div>
              <div class="sideboxcomlumnbox  txt_center"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/video.png" width="217" height="156" /></div>
              <div class="sideboxcomlumnbox  txt_center"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/download.png" width="220" height="58" /></a></div>
              <div class="sideboxcomlumnbox sideboxcontent_bor">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="txt_16 txt_gray txt_bold"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/enews.png" width="68" height="44" /> จดหมายข่าว</td>
                  </tr>
                  <tr>
                    <td height="40" class="txt_center"><input name="textfield4" type="text" id="textfield4" size="15" />
                      <a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/recivenews.png" width="58" height="24" /></a></td>
                  </tr>
                </table>
              </div></td>
            <td width="20" valign="top">&nbsp;</td>
            <td valign="top" class="column-center">
			<?php echo $content; ?>
</td>
          </tr>
        </table>
<?php $this->endContent(); ?>