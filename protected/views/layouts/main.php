<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/ico.png" type="image/x-icon" />
<title>D-Power</title>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/css.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mmenu.all.css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/flexnav.css" media="screen, projection" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-11.0.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mmenu.min.all.js"></script>
<script type="text/javascript">
	$(function() {
		$('nav#menu').mmenu();
	});
</script>
<script type="text/javascript">
         jQuery(document).ready(function( $ ) {
			 
             $("#menu").mmenu({
				
               "extensions": [
                  "iconbar",
                  "theme-dark"
               ],
               "offCanvas": {
                  "position": "right",
                  "zposition": "front"
               },
			   "navbar": {
                  "title": "Menu"
               },
               "navbars": [
                  {
                     "position": "top",
                     "content": [
                        "prev",
                        "title",
                        "close"
                     ]
                  }
               ]
            });
		 
      </script>
      
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/flaticon.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/totop.css">
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/animated-header.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/animated-header.js"></script>


<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/flickity-docs.css" media="screen">    

<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/device.css" rel="stylesheet" type="text/css"> 

</head>

<body>
<div id="page">

			<div class="header">
				<a href="#menu">
				<svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="#FFFFFF" d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" />
</svg></a>
			</div>
			<nav id="menu">
            <ul>
            <li class="ver-menu"><a href="index.html">หน้าแรก</a></li>
            <li class="ver-menu"><a href="promotion.html">โปรโมชั่น</a></li>
            <li class="ver-menu"><a href="about.html">เกี่ยวกับเรา</a></li>
            <li class="ver-menu"><a href="product.html">สินค้าทั้งหมด</a>
              <ul>
              	<?php
              	
				foreach($this->category as $cate){
					?>
						<li><a href="<?php echo Yii::app()->createUrl("site/group",array("id"=>$cate->id)); ?>"><?php echo $cate->group_2;?></a></li>
					<?php
				}
              	?>
				

              </ul>
            </li>
            <li><a href="news.html">ข่าวสารและกิจกรรม</a></li>
            <li><a href="contact.html">ติดต่อเรา</a></li>
            <li><a href="#"> ข้อกำหนดและเงื่อนไข </a></li>
            <li><a href="#"> เข้าสู่ระบบ </a></li>
            </ul>
			</nav>
</div>


<div id="wrapper">
<div id="header">
<div id="header-inner">
<div class="header-box">
<div id="header-inner-logo">
<a href="index.html"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.svg"></a>
</div>

<div class="header-row">
<div class="header-col">
<i class="icon-condition"> ข้อกำหนดและเงื่อนไข</i>
</div>
<div class="header-col">
<i class="icon-login"> เข้าสู่ระบบ</i>
</div>
<div class="header-col">
<i class="icon-cart"> <span class="cart-amount">99</span></i>
</div>

</div>

<div class="header-row">
<nav>
          <ul class="flexnav">
          	<li class="ver-menu"><a href="index.html">หน้าแรก</a></li>
            <li class="ver-menu"><a href="promotion.html">โปรโมชั่น</a></li>
            <li class="ver-menu"><a href="about.html">เกี่ยวกับเรา</a></li>
            <li class="ver-menu"><a href="product.html">สินค้าทั้งหมด</a>
              <ul>
              	<?php
              		foreach($this->category as $cate){
              			?>
              				<li><a href="<?php echo Yii::app()->createUrl("site/group",array("id"=>$cate->id)); ?>"><?php echo $cate->group_2;?></a></li>
              			<?php
					}
              	?>
				
				
              </ul>
            </li>
            <li><a href="news.html">ข่าวสารและกิจกรรม</a></li>
            <li><a href="contact.html">ติดต่อเรา</a></li>
          </ul>
        </nav>
        </div>
</div>
</div>
</div>
<?php
if(Yii::app()->controller->action->id=="index"){
	?>
	<div class="banner-box">
		<div class="gallery js-flickity" data-flickity-options='{ "autoPlay": 4000 , "wrapAround": true, "imagesLoaded": true}'>
		<?php
		foreach($this->banner as $banner){
			?>
			<div class="hero-gallery__cell hero-gallery__cell--1">
		      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/banner/<?php echo $banner["id"]; ?>/<?php echo $banner["img_src"]; ?>" alt=""/>
		    </div>
			<?php
		}
			?>
		  </div>
		</div>
	<?php
}else{
	?>
	<div class="navbar">
<div class="navbar-box">
<a href="index.html">หน้าแรก</a><!-- InstanceBeginEditable name="navpage" --> สินค้า<!-- InstanceEndEditable --> 
</div>
</div>
	<?php
}
?>


<div class="mainsite-box">
	<?php echo $content; ?>
</div>

<div class="footer-box">
<div class="footer-box-row">
<div class="footer-box-col">
<div class="footer-menu">
<div class="footer-title">
Catagory
</div>
<ul>
	<?php
              		foreach($this->category as $cate){
              			?>
              				<li><a href="<?php echo Yii::app()->createUrl("site/group",array("id"=>$cate->id)); ?>"><?php echo $cate->group_2;?></a></li>
              			<?php
					}
              	?>
</ul>
</div>
</div>

<div class="footer-box-col">
<div class="footer-menu">
<div class="footer-title">
Site Map
</div>
<ul>
<li><a href="./index.html">หน้าแรก</a></li>
<li><a href="./about.html">เกี่ยวกับเรา</a></li>
<li><a href="./product.html">สินค้าทั้งหมด</a></li>
<li><a href="./news.html">ข่าวสารและกิจกรรม</a></li>
<li><a href="./condition.html">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="./contact.html">ติดต่อเรา</a></li>
</ul>
</div>
</div>

<div class="footer-box-col">
<div>
<div class="footer-title">
ช่องทางการติดตาม
</div>
<a href="https://www.facebook.com/dpower.official2014/" target="_blank"><img src="<?php echo Yii::app()->request->baseUrl; ?>//images/facebook.png" width="32" height="32" alt=""/> </a>
<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/twitter.png" width="32" height="32" alt=""/> 
<img src="<?php echo Yii::app()->request->baseUrl; ?>//images/youtube.png" width="32" height="32" alt=""/>
</div>

<div>&nbsp;</div>

<div>
<div class="footer-title">
สมัครรับข่าวสาร
</div>
<input type="text" placeholder="email" class="enews-inp" maxlength="50"><button class="btn-org">Submit</button>
</div>

</div>
</div>
</div>

</div>

<script src="js/flickity-docs.min.js"></script>        
<script type="text/javascript"> 
	$(function() { 
		$('#page').css("display","none"); 
		$('nav#menu').mmenu(); 
	}); 
</script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.flexnav.min.js" type="text/javascript"></script>
<script type="text/javascript">$(".flexnav").flexNav();</script>

<a href="#0" class="cd-top">Top</a>
</body>
</html>