<?php $this->beginContent('//layouts/main'); ?>

<?php
// Image gallery
$modelImageGallery = new GalleryImage();
$data["img_gallery"] = $modelImageGallery->searchAlbumForIndex()->data;
?>

<?php echo $content; ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.3&appId=239626496134755";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="side-box">
    <div class="gallery-box">
        <div class="gallery-box-title">
            <div class="gallery-box-title-txt">
                Photo <span class="txt-org">Gallery</span>
            </div>
            <div class="gallery-box-more">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/GalleryImage/">ภาพทั้งหมด »</a>
            </div>
        </div>
        <div class="gallery-content">
            <ul>
                <?php
                foreach ($data["img_gallery"] as $row) {
                    ?>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/GalleryImageDetail/<?php echo $row->gallery->id; ?>" ><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/album/<?php echo $row->gallery->id; ?>/<?php echo $row->file_name; ?>" width="" height=""  alt=""/></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="follow-box">
<!--        <iframe src="http://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FBangkok-Inter-Food-CoLtd%2F216450991777657%3Ffref%3Dts&amp;width=300&amp;height=480&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:480px;" allowTransparency="true"></iframe>-->
    <div class="fb-page" data-href="https://www.facebook.com/biffoodservice" data-width="300" data-height="480" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/biffoodservice"><a href="https://www.facebook.com/biffoodservice">Biffoodservice</a></blockquote></div></div>
    </div><br/><br/>
    <div class="news-letter-box-content">
        <a href="http://track.thailandpost.co.th/tracking/default.aspx" target="_blank"><img border="0" src="<?php echo Yii::app()->request->baseUrl; ?>/images/ems_track_and_trace.jpg" style="border:solid 1px #dddddd;" /></a>
    </div>
    <br/><br/>
    <div class="news-letter-box">
        <div class="news-letter-box-title">
            <div class="news-letter-box-title-txt">
                E-News<span class="txt-org">letter</span>
            </div>
        </div>
        <div class="news-letter-box-content">
            <p><input type="text" id="txtSubscribeEmail" placeholder="กรอกอีเมลล์ของคุณเพื่อสมัครรับข่าวสาร"></p>
            <p><button id="btnSubscribe" type="button">Submit</button></p>
        </div>

    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        // Submit email for subscribe
        $('#btnSubscribe').click(function () {
            var subEmail = $('#txtSubscribeEmail').val().trim();
            if (subEmail == "") {
                alert('คุณยังไม่ได้กรอก Email สำหรับสมัครรับข่าวสาร');
                $('#txtSubscribeEmail').focus();
                return;
            } else {
                $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/SubscribeNew", {email: subEmail},
                function (data) {
                    if (data.result == "success")
                    {
                        alert("<?php echo Label::$submit_complete; ?>");
                        $('#txtSubscribeEmail').val('');
                    } else {
                        alert(data.message);
                    }
                }, 'json');
            }
        });
    });
</script>

<?php $this->endContent(); ?>