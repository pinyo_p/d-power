<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'Training';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  
  <script type="text/javascript" language="javascript">
	$(function() {
		$( "#tabs" ).tabs();
		CKEDITOR.replace('Training_object_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_who_interest_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_benefit_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_course_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_payment_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_contact_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		
		CKEDITOR.replace('Training_object_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_who_interest_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_benefit_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_course_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_payment_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Training_contact_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
	});
	
	</script>
  
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
      </tr>
      
        <tr>
      <td style="padding-left:30px">
      Code :
     <br />
      <?php echo $form->textField($model,'training_code'); ?> 
      </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม :<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date]',
					'value'=>$model->start_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด :<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
          <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 2 :<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date2]',
					'value'=>$model->start_date2,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 2:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date2]',
					'value'=>$model->end_date2,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 3 :<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date3]',
					'value'=>$model->start_date3,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 3:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date3]',
					'value'=>$model->end_date3,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 4:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date4]',
					'value'=>$model->start_date4,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 4:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date4]',
					'value'=>$model->end_date4,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 5:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date5]',
					'value'=>$model->start_date5,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 5:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date5]',
					'value'=>$model->end_date5,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 6:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date6]',
					'value'=>$model->start_date6,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 6:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date6]',
					'value'=>$model->end_date6,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 7:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date7]',
					'value'=>$model->start_date7,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 7:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date7]',
					'value'=>$model->end_date7,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 8:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date8]',
					'value'=>$model->start_date8,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 8:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date8]',
					'value'=>$model->end_date8,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 9:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date9]',
					'value'=>$model->start_date9,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 9:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date9]',
					'value'=>$model->end_date9,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 10:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date10]',
					'value'=>$model->start_date10,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 10:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date10]',
					'value'=>$model->end_date10,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 11:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date11]',
					'value'=>$model->start_date11,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 11:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date11]',
					'value'=>$model->end_date11,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
      
            <tr>
        <td align="left" style="padding-left:30px">วันที่เริ่ม 12:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[start_date12]',
					'value'=>$model->start_date12,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      <tr>
        <td align="left" style="padding-left:30px">วันที่สิ้นสุด 12:<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Training[end_date12]',
					'value'=>$model->end_date12,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
        </td>
      </tr>
      
      
      
      
      <tr>
      <td style="padding-left:30px">
      เวลารวม :
     <br />
      <?php echo $form->textField($model,'times'); ?> ชั่วโมง
      </td>
      </tr>
      
        <tr>
      <td style="padding-left:30px">
      รวมวันทั้งหมด :
     <br />
      <?php echo $form->textField($model,'days'); ?> วัน
      </td>
      </tr>
      
      
	<tr>
      <td style="padding-left:30px">
      เวลาอบรม : 
     <br />
      <?php echo $form->textField($model,'duration'); ?>
      *ตัวอย่างเช่น 10:00 - 18:00 น.
      </td>
      </tr>      
      
      <tr>
      <td style="padding-left:30px">
     ราคา :
     <br />
      <?php echo $form->textField($model,'price'); ?> บาท
      </td>
      </tr>
      <tr>
      <td style="padding-left:30px">
      ภาษา :
     <br />
      <?php echo $form->textField($model,'language'); ?>
      </td>
      </tr>
      
      <tr>
      <td style="padding-left:30px">
      ผู้สอน :
     <br />
      <?php echo $form->textField($model,'teacher'); ?>
      </td>
      </tr>
      <tr><td style="padding-left:30px">
      Status :<br />
      <?php echo $form->dropDownList($model,'status',array('0'=>'เตรียมการอบรม','1'=>'เปิดให้ลงทะเบียน','2'=>'ปิดลงทะเบียน','3'=>'อยู่ในระหว่างอบรม','4'=>'ปิดอบรมแล้ว'), array()); ?>
      </td>
      </tr>
      <tr><td><br />
<br />
</td>
</tr>
      
      
      <tr>
        <td align="center" class="add_data">
        <table style="text-align:left"><tr><td>
            <div id="tabs" width='90%' >
                <ul>
                    <li><a href="#tabs-1">ภาษาไทย</a></li>
                    <li><a href="#tabs-2">ภาษาอังกฤษ</a></li>
                </ul>
                <div id="tabs-1">
                	ชื่อหลักสูตร (TH)<br />
                    <?php echo $form->textField($model,'subject_th',array('style'=>'width:450px')); ?><br /><br />
รายละเอียดหลักสูตร (TH)<br />
<?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'benefit_th',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?><br />
<br />
                    วัตถุประสงค์ (TH)<br />
                    <?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'object_th',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?>

                <br /><br />

               หัวข้อการอบรม (TH)<br />
                    <?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'who_interest_th',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?><br /><br />
			
คุณสมบัติผู้เข้าอบรม (TH)<br />
<?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'course_th',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?>
<br />
<br />

วิธีการสมัครและชำระเงิน (TH)<br />
<?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'payment_th',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?><br />
<br />


ติดต่อสอบถาม (TH)<br />
<?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'contact_th',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?>

                </div>
                
                
                <div id="tabs-2">
                  ชื่อหลักสูตร (EN)<br />
                    <?php echo $form->textField($model,'subject_en',array('style'=>'width:450px')); ?><br /><br />
รายละเอียดหลักสูตร (EN)<br />
<?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'benefit_en',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?><br />
<br />
                    วัตถุประสงค์ (EN)<br />
                    <?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'object_en',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?>

                <br /><br />

               หัวข้อการอบรม (EN)<br />
                    <?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'who_interest_en',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?><br /><br />
			
คุณสมบัติผู้เข้าอบรม (EN)<br />
<?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'course_en',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?>
<br />
<br />

วิธีการสมัครและชำระเงิน (EN)<br />
<?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'payment_en',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?><br />
<br />


ติดต่อสอบถาม (EN)<br />
<?php 
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model'=>$model,
            'attribute'=>'contact_en',
            'language'=>'en',
            'editorTemplate'=>'full',
            'height'=>'700px',
            )); ?>
            
            
                </div>
                
            </div>
            </td>
            </tr>
            </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/traininglist';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>