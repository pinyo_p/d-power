<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'สาขาของ KPT Group';
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<table width="100%">
    <tr>
        <td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
    </tr>
    <tr>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="add_data">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th width="200" align="right">ชื่อบริษัท (ภาษาไทย) :</th>
                    <td width="600"><?php echo $form->textField($model, 'name_2', array('style' => 'width:95%')); ?></td>
                </tr>
                <tr>
                    <th  align="right">ชื่อบริษัท (ภาษาอังกฤษ) :</th>
                    <td><?php echo $form->textField($model, 'name_1', array('style' => 'width:95%')); ?></td>
                </tr>
                <tr>
                    <th  align="right">ที่อยู่บริษัท (ภาษาไทย) :</th>
                    <td><?php echo $form->textArea($model, 'address_2',array('style' => 'width:95%;height:60px;')); ?></td>
                </tr>
                <tr>
                    <th  align="right">ที่อยู่บริษัท (ภาษาอังกฤษ) :</th>
                    <td><?php echo $form->textArea($model, 'address_1',array('style' => 'width:95%;height:60px;')); ?></td>
                </tr>
                <tr>
                    <th  align="right">เบอร์โทร :</th>
                    <td><?php echo $form->textField($model, 'phone_no', array('style' => 'width:150px')); ?></td>
                </tr>
                <tr>
                    <th  align="right">Fax :</th>
                    <td><?php echo $form->textField($model, 'fax_no', array('style' => 'width:150px')); ?></td>
                </tr>
                <tr>
                    <th  align="right">Email :</th>
                    <td><?php echo $form->textField($model, 'email', array('style' => 'width:150px')); ?></td>
                </tr>

                <tr>
                    <th  align="right">Latitude :</th>
                    <td><?php echo $form->textField($model, 'latitude', array('style' => 'width:150px')); ?></td>
                </tr>
                <tr>
                    <th  align="right">Longitude :</th>
                    <td><?php echo $form->textField($model, 'longitude', array('style' => 'width:150px')); ?></td>
                </tr>


                <tr>
                    <th  align="right">จัดเรียง :</th>
                    <td><?php echo $form->textField($model, 'sort_order', array('style' => 'width:50px')); ?></td>
                </tr>


                <tr>
                    <th  align="right">รูปภาพ :</th>
                    <td>
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/branch/' . $model->main_image;
                        if (file_exists(Yii::app()->basePath . '/../images/branch/' . $model->main_image) && $model->main_image != "") {
                            ?>
                            <img src="<?php echo $file; ?>" height="83" />
                            <?php
                        }
                        ?>
                        <input type="file" name="main_image" id='main_image' /></td>
                </tr>
                <tr>
                    <th  align="right">แผนที่ :</th>
                    <td>
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/branch/' . $model->map_image;
                        if (file_exists(Yii::app()->basePath . '/../images/branch/' . $model->map_image) && $model->map_image != "") {
                            ?>
                            <img src="<?php echo $file; ?>" height="83" />
                            <?php
                        }
                        ?>
                        <input type="file" name="map_image" id='map_image' /></td>
                </tr>
            </table>
            <br />
            <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/branchlist'; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>