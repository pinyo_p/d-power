
<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'รายการผู้ลงทะเบียน ' . $param['title'] ;
?>
<script language="javascript">
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteTrainee/",{
									id:objId
									},function(data){
										if(data=="OK")
											$("#productlist-form").submit();
										else
											alert(data);
									});
	}
}
function deleteAll()
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$("#act").val("deleteall");
		$("#productlist-form").submit();
		$("#act").val("search");
	}
}
function change_status(objStatus,objId)
{
	str_status = $(objStatus).val();
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/changeTraineeStatus/",{
									id:objId,
									status:str_status,
									},function(data){
										if(data=="OK")
											$("#productlist-form").submit();
										else
											alert(data);
									});
}

function searchit()
{

		$("#productlist-form").submit();

}
function gotoPage(objPage)
{
	$("#page").val(objPage);
	$("#productlist-form").submit();
}

function export_data()
{
	var fullname = $("#Course_fullname").val();
	var company_name = $("#Course_company_name").val();
	var sale_name = $("#Course_sale_name").val();
	var status = $("#Course_status").val();
	var start_date = $("#Course_start_date").val();
	var end_date = $("#Course_end_date").val();Course_training_id
	var training_id = $("#Course_training_id").val();
	window.open('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/ExportTrainee/?fullname=' + fullname +'&company_name=' + company_name +'&sale_name=' + sale_name +'&status=' + status +'&start_date=' + start_date +'&end_date=' + end_date + '&training_id=' + training_id);
}
function toggle_j(objId)
{
	$("#j_" + objId).toggle();
}
</script>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'productlist-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center"><br />
<span class="text4">ค้นหา ผู้ลงทะเบียน <?php echo $param['title'];?></span><br />
&nbsp;<br /></td>
      </tr>
       <tr>
        <td align="center" width="100%" class="tabletest">
         <table  border="0" cellspacing="0" cellpadding="0">
   
            <tr>
              <th align="right">ชือ-นามสกุล :</th>
              <td ><?php echo $form->textField($model,'fullname'); ?></td>
               <th align="right">ชื่อบริษัท :</th>
              <td ><?php echo $form->textField($model,'company_name'); ?></td>
            </tr>
            <tr>
            </tr>
            <tr>
              <th align="right">ชื่อ Sale :</th>
              <td ><?php echo $form->textField($model,'sale_name'); ?></td>
              <th width="120"><div align="right"><span class="h1">สถานะ :</span></div></th>
                          <td width="280" colspan="2">
                          <?php
echo $form->dropDownList($model, 'status',array('' => '-- เลือกสถานะ --','0'=>'มาใหม่','1'=>'ติดต่อแล้ว'));
																						  ?>
                          </td>
              
            </tr>
            
            <tr>
                          <th width="120"><div align="right"><span class="h1">วันที่สมัคร :</span></div></th>
                          <td width="280"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Course[start_date]',
					'value'=>$model->start_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td>
                          <th width="120"><div align="right"><span class="h1">ถีง :</span></div></th>
                          <td width="280"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Course[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td>
                        </tr>

            
             <tr>
             <th align="right">Course :</th>
              <td ><?php echo $form->dropDownList($model,'training_id', CHtml::listData(Training::model()->findAll(array('order' => 'subject_th ASC')), 'id', 'subject_th'), array('empty'=>'เลือก Course')); ?>  </td>
                          <td width="120" colspan="4" align="center">
                          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" alt="Submit button">&nbsp;<a href="javascript:export_data();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_export.png" alt="Export"></a></td>
                        </tr>
           </table>
        </td>
        </tr>
        <tr>
        <td align="center"> <br /><br /><br />
<span class="text4"><?php echo $this->breadcrumbs;?></span><br />
&nbsp;<br /></td>
      </tr>
        
      <tr>
        <td align="center" width="100%" class="tabletest">
        
       
          
        
          <table border="0" cellpadding="0" cellspacing="1">
            <tr>
              <th width="60">เลือก</th>
               <th >Course</th>
               <th >วันที่</th>
               <th >จำนวนคน</th>
              <th >ชื่อ - นามสกุล</th>
              <th >ชื่อบริษัท</th>
              <th >Email</th>
              <th >เบอร์มือถือ</th>
              <th >ชื่อผู้ขาย</th>
              <th width="60">สถานะ</th>
              <th width="60">ลบ</th>
            </tr>
            <?php 
			$a = 0;
			foreach($data as $row){
			?>
            <tr valign="top" style="vertical-align:top">
              <td align="center" style="vertical-align:top">
              <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id;?>' /></td>
              
               <td align="center" style="vertical-align:top">
              <?php  echo $row->course['subject_th'];?>
              </td>
               <td align="left" style="vertical-align:top">
             <?php echo $row->training_date;?>
              </td>
              <td align="left" nowrap="nowrap" style="vertical-align:top">
              <?php echo $row->training_person;?>
              <?php
			  $joinner = Joinner::Model()->findAll('course_id=:course_id',array('course_id'=>$row->id));
			  
			  if(count($joinner)>0){
				  $ii=1;
				  echo " <a href='javascript:toggle_j($a)'>Show/Hide</a><div id='j_$a' width='100%'>";
				  foreach($joinner as $j)
				  {
					  ?>
                      <table width="100%"><tr><td colspan="2">คนที่ <?php echo $ii;?> </td>
                      </tr>
                      <tr><td>ชื่อ/สกุล  <?php echo $j->fullname;?> </td><td> ตำแหน่ง <?php echo $j->position;?></td></tr>
                      <tr><td> Email <?php echo $j->email;?></td><td>โทรศัพท์ <?php echo $j->phoneno;?></td></tr>
                      </table>
                      <?php
					  $ii++;
				  }
				  echo "</div>
				  <script language='javascript'>\ntoggle_j($a);\n</script>\n
				  ";
			  }
			  $a++;
			  ?>
              
              </td>
              <td style="vertical-align:top">
             <?php echo $row->fullname;?>
              </td>
              <td style="vertical-align:top">
             <?php echo $row->company_name;?>
              </td>
              <td style="vertical-align:top">
             <?php echo $row->email;?>
              </td>
              <td style="vertical-align:top">
             <?php echo $row->mobile;?>
              </td>
              <td style="vertical-align:top">
             <?php echo $row->sale_name;?>
              </td>
             
             
             
              <td  align="center" style="vertical-align:top"><select name="select3" class="textb" id="status_<?php echo $row->id;?>" onchange='change_status(this,"<?php echo $row->id;?>")' style="width:120px">
                            <option value="">- เลือกสถานะ -</option>
                            <option value="0" <?php echo ($row->status==0? " selected='selected' ": "")?>>มาใหม่</option>
                            <option value="1" <?php echo ($row->status==1? " selected='selected' ": "")?>>ติดต่อแล้ว</option>
                          </select></td>
              <td align="center"  style="vertical-align:top">
              <a href='javascript:deleteit("<?php echo $row->id;?>");'>
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
            </tr>
           <?php
           }
		   
		   ?>
          </table>
         <?php
								  if($param['max_page']>1){
									  ?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page =  $param['page']+1;?> / <?php echo $param['max_page'];?></td>
                          <td style="border:none;"><div class="nav_page">
                             <ul>
                                      <li onclick="gotoPage('<?php echo $page-1;?>')">&laquo;</li>
                                      <?php 
$start_page = ($page>2?$page-2:1);
									 $end_page = ($param['max_page']>($page+2)?$page+2:$param['max_page']);
									 if($end_page<=5)
									 	$end_page = 5;
									else if(($end_page - $page)<5)
										$start_page = ($end_page -4);		
									if($end_page>$param['max_page'])
										$end_page = $param['max_page'];	
									  for($i=$start_page-1;$i<ceil($end_page);$i++)
									  {
										  $class = "";
										  if(($i+1)==$page)
										  	$class = '  class="navselect"';
										else
											$class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
										  ?>
                                          <li <?php echo $class;?>><?php echo $i+1;?></li>
                                          <?php
									  }
									  ?>
                                      <li onclick="gotoPage('<?php echo $page+1;?>')">&raquo;</li>
                                    </ul>
                          </div></td>
                          <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                          <table width="200"  style="border:none;"><tr><td style="border:none;">
                          <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                    <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                    <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png" width="39" height="22" /></a></td></tr></table>
                                    
                                    
                                    </td>
                        </tr>
                      </table>
                      <?php
								  }
								  ?>
        </td>
      </tr>
      <tr>
        <td align="center">
        
                        
                      
        <br />
          <br />
          <br />
         <a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br /></td>
      </tr>
      
    </table>
            <?php $this->endWidget(); ?>