<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'จัดการข้อมูล Banner';
?>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteBanner/", {
                id: objId
            }, function (data) {
                if (data.indexOf("OK") > -1)
                    location.href = '<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BannerList';
                else if(data.indexOf("DENIED") > -1)
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
                else
                    alert(data);
            });
        }
    }
    function deleteAll()
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $("#banklist-form").submit();
        }
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'banklist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' value="deleteall" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4">จัดการข้อมูล Banner</span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">เลือก</th>
                    <th >Banner</th>
                    <th width="60">แก้ไข</th>
                    <th width="60">ลบ</th>
                </tr>
                <?php
                foreach ($data as $row) {
                    if ($row->img_src != "") {
                        ?>
                        <tr>
                            <td align="center">
                                <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' /></td>
                            <td align="center">
                                <?php
                                $file = Yii::app()->request->baseUrl . '/images/banner/'.$row->id.'/' . $row->img_src;
                                if (file_exists(Yii::app()->basePath . '/../images/banner/'.$row->id.'/' . $row->img_src) && $row->img_src != "") {
                                    ?><br />
                                    <a href="<?php echo $row->url; ?>" target="_blank" >
                                        <img src="<?php echo $file; ?>" height="83" />
                                    </a><br />
                                    URL: <?php echo $row->url; ?>
                                    <?php
                                }
                            }
                            ?>
                        </td>


                        <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Banner/<?php echo $row->id; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
                        <td align="center">
                            <a href='javascript:deleteit("<?php echo $row->id; ?>");'>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br />
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Banner/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>