
<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'แบรนด์สินค้า';
?>
<script language="javascript">
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteBrand/",{
									id:objId
									},function(data){
										if(data=="OK")
											location.href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BrandList';
										else
											alert(data);
									});
	}
}
function deleteAll()
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$("#banklist-form").submit();
	}
}
</script>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'banklist-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <input type="hidden" name='act' value="deleteall" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center"><br />
<span class="text4"><?php echo $this->breadcrumbs;?></span><br />
&nbsp;<br /></td>
      </tr>
      <tr>
        <td align="center" width="100%" class="tabletest">
          <table border="0" cellpadding="0" cellspacing="1">
            <tr>
              <th width="60">เลือก</th>
              <th >Logo</th>
              <th width="200" >คำอธิบาย</th>
              <th >Brand</th>
              <th >Group</th>
              <th width="60">แก้ไข</th>
              <th width="60">ลบ</th>
            </tr>
            <?php 
			foreach($data as $row){
			?>
            <tr>
              <td align="center">
              <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id;?>' /></td>
               <td align="center">
               <?php
			  $file = Yii::app()->request->baseUrl . '/images/brand/' . $row->brand_logo;
			  if(file_exists(Yii::app()->basePath . '/../images/brand/' .  $row->brand_logo) && $row->brand_logo != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="83" />
                  <?php
			  }else{
			  ?>
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
              <?php
			  }
			  ?>
              </td>
               <td align="left">
               <?php 
			   echo Yii::short_text($row->brand_desc_th,200);
			   ?>
              </td>
              <td align="center">
              <?php echo $row->name_en; ?> / <?php echo $row->name_th; ?>
              </td>
              <td align="center">
              <?php echo $row->group['brand_th']; ?> 
              </td>
              <td  align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Brand/<?php echo $row->id;?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
              <td align="center">
              <a href='javascript:deleteit("<?php echo $row->id;?>");'>
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
            </tr>
           <?php
           }
		   ?>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br />
          <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Brand/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br /></td>
      </tr>
      
    </table>
            <?php $this->endWidget(); ?>