<?php 
$this->pageTitle="Admin Panel::District"; 
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">อำเภอ</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            
            <tr>
              <td></td>
                </tr>
                <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_district',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                <tr>
                  <td><br />
                    <br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">เพิ่มอำเภอ</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                </table></td>
                </tr>
                <tr>
                  <td><br />
                    <br />
                    <br />
                    
                    <table width="300" border="0" align="center" cellpadding="0" cellspacing="0" class="form1">
                    
                    
                   <tr>
                          <td width="120"><div align="right"><span class="h1">เลือกภาค :</span></div></td>
                          <td><?php echo $form->dropDownList($model,'continent_id', CHtml::listData(Continent::model()->findAll(array('order' => 'thai_name ASC')), 'id', 'thai_name'), array('empty'=>'ภูมิภาค',	
																																										   'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getprovince/' ,
				'data'=>array('District_continent_id'=>'js:this.value'),
      			'success'=>'js:function(html){jQuery("#District_province_id").html(html);}',),
	  )); ?></td>
                          </tr>
                        <tr>
                          <td width="120"><div align="right"><span class="h1">เลือกจังหวัด :</span></div></td>
                          <td><?php echo $form->dropDownList($model,'province_id', $param['province'], array('empty'=>'กรุณาเลือกภูมิภาคก่อน')); ?></td>
                          </tr>
                      <tr>
                        <td width="100" align="right">ชื่ออำเภอ (ไทย): &nbsp;</td>
                        <td align="left"> <?php echo $form->textField($model,'thai_name'); ?><div class="form"><?php echo $form->error($model,'thai_name'); ?></div></td>
                      </tr>
                       <tr>
                        <td width="100" align="right">ชื่ออำเภอ (อังกฤษ): &nbsp;</td>
                        <td align="left"> <?php echo $form->textField($model,'eng_name'); ?></td>
                      </tr>
                      <tr>
                        <td width="100" align="center">&nbsp;</td>
                        <td align="left"><a href="javascript:$('#frm_district').submit();"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="" width="52" height="25" /></a>&nbsp;<a href="<?php echo Yii::app()->createUrl('/admin/District/');?>"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a></td>
                      </tr>
                    </table>
                    
                    <br />
                    <br />
                    <br />
                    <br /></td>
                </tr>
                <?php $this->endWidget(); 
					
				?>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
        