<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'ประวัติรายการสั่งซื้อสินค้า';
?>
<script language="javascript">

    function searchit()
    {
        $("#productlist-form").submit();
    }
    function gotoPage(objPage)
    {
        $("#page").val(objPage);
        $("#productlist-form").submit();
    }
    function showDetail(dt_start,dt_end)
    {
        window.open('<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ExportHistoryOrder?dt_start=' + dt_start + '&dt_end=' + dt_end, 'member_detail', 'width=600,height=400');
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'productlist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4">ค้นหารายการประวัติสั่งซื้อสินค้า</span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">





            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="right">ช่วงเวลา :</th>
                    <td>


                        <table border="0" style="border:none;">
                            <tr>
                                <td width="280" style="border:none;">
                                    <?php
                                    $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
                                        'name' => 'MemberOrder[start_date]',
                                        'value' => $model->start_date,
                                        'options' => array(
                                            'showAnim' => 'fold',
                                            'dateFormat' => 'yy-mm-dd',
                                            'altFormat' => 'yy-mm-dd',
                                            'changeMonth' => 'true',
                                            'changeYear' => 'true',
                                            'showOn' => "both",
                                            'yearRange' => "'" . date("Y") - 70 . ':' . date("Y") - 10 . "'",
                                            'buttonImage' => Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",
                                            'buttonImageOnly' => "true",
                                        ),
                                        'htmlOptions' => array(
                                            'style' => 'height:20px; width:70px;'
                                        ),
                                            )
                                    );
                                    ?> &nbsp;&nbsp;ถึง &nbsp;&nbsp;
                                    <?php
                                    $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
                                        'name' => 'MemberOrder[end_date]',
                                        'value' => $model->end_date,
                                        'options' => array(
                                            'showAnim' => 'fold',
                                            'dateFormat' => 'yy-mm-dd',
                                            'altFormat' => 'yy-mm-dd',
                                            'changeMonth' => 'true',
                                            'changeYear' => 'true',
                                            'showOn' => "both",
                                            'yearRange' => "'" . date("Y") - 70 . ':' . date("Y") - 10 . "'",
                                            'buttonImage' => Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",
                                            'buttonImageOnly' => "true",
                                        ),
                                        'htmlOptions' => array(
                                            'style' => 'height:20px; width:70px;'
                                        ),
                                            )
                                    );
                                    ?>
                                </td>
                                <td style="border:none;">
                                    <a href="#" onclick="searchit()"><input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" width="55" height="26" style="border:none;" /></a>
                                    <a href="#" onclick="searchit()"><input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_export.png" height="26" style="border:none;"/></a>
                                </td>
                            </tr>
                        </table>


                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center"> <br /><br /><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>

    <tr>
        <td align="center" width="100%" class="tabletest">




            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">No.</th>
                    <th  width="120">ชื่อผู้ติดต่อ</th>
                    <th >ชื่อบริษัท</th>
                    <th >เบอร์โทรศัพท์</th>
                    <th >รายการสินค้า</th>
                    <th>หมายเหตุ</th>
                    <th  width="120">สถานะ</th>

                    <th width="80">วันที่สั่งซื้อ</th>
                </tr>
                <?php
                $i = ($model->current_page) * $model->display_perpage;

                foreach ($data as $row) {
                    ?>
                    <tr>
                        <td align="center">
                            <?php echo ++$i; ?> 
                        </td>
                        <td align="left">
                            <a href='javascript:showDetail("<?php echo $row->id; ?>")'><?php echo $row->fullname; ?> </a>

                        </td>
                        <td align="left">
                            <?php echo $row->company_name; ?> 
                        </td>
                        <td align="left">
                            <?php echo $row->phone_no; ?>  
                        </td>
                        <td align="left">
                            <a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ProductOrderList/<?php echo $row->id; ?>'>
                                รายการสินค้า
                            </a>
                        </td>
                        <td><?php echo $row->remark; ?></td>
                        <td align="center" valign="middle">
                            <table style="border:none;"><tr valign="top"><td style="border:none; text-align:right">
                                        <select name="status_<?php echo $row->id; ?>" id="status_<?php echo $row->id; ?>">
                                            <option value="0" <?php echo ($row->status == "0" ? " selected='selected'" : ""); ?>>รายการใหม่</option>
                                            <option value="1" <?php echo ($row->status == "1" ? " selected='selected'" : ""); ?>>ติดต่อแล้ว</option>
                                            <option value="2" <?php echo ($row->status == "2" ? " selected='selected'" : ""); ?>>ชำระเงินแล้ว</option>
                                            <option value="3" <?php echo ($row->status == "3" ? " selected='selected'" : ""); ?>>ยกเลิก</option>
                                            <option value="4" <?php echo ($row->status == "4" ? " selected='selected'" : ""); ?>>รอจัดส่ง</option>
                                        </select>
                                    </td><td style="border:none;">
                                        <a href='javascript:;' onclick="change_status('<?php echo $row->id; ?>', 'status_<?php echo $row->id; ?>')">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />
                                        </a>
                                    </td>
                                </tr>
                            </table>



                        </td>

                        <td  align="center">
                            <?php echo substr($row->create_date, 0, 10); ?>
                        </td>

                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
            if ($param['max_page'] > 1) {
                ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                        <td style="border:none;"><div class="nav_page">
                                <ul>
                                    <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
                                    <?php
                                    $start_page = ($page > 2 ? $page - 2 : 1);
                                    $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
                                    if ($end_page <= 5)
                                        $end_page = 5;
                                    else if (($end_page - $page) < 5)
                                        $start_page = ($end_page - 4);
                                    if ($end_page > $param['max_page'])
                                        $end_page = $param['max_page'];
                                    for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                                        $class = "";
                                        if (($i + 1) == $page)
                                            $class = '  class="navselect"';
                                        else
                                            $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                                        ?>
                                        <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                        <?php
                                    }
                                    ?>
                                    <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                                </ul>
                            </div></td>
                        <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                            <table width="200"  style="border:none;"><tr><td style="border:none;">
                                        <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                        <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                        <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/go.png" width="39" height="22" /></a></td></tr></table>


                        </td>
                    </tr>
                </table>
                <?php
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="center">



            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>