<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'จัดการข้อมูล Training';

function setDate($start_date,$end_date)
{
	$str = "";
	if($start_date != "" && $end_date !="")
		$str = $start_date . ' - ' . $end_date;
	if($start_date == $end_date)
		$str = $start_date;
	return $str;
}

function showDate($row)
{
	$arr_date[] = setDate($row->start_date,$row->end_date);
	$arr_date[] = setDate($row->start_date2,$row->end_date2);
	$arr_date[] = setDate($row->start_date3,$row->end_date3);
	$arr_date[] = setDate($row->start_date4,$row->end_date4);
	$arr_date[] = setDate($row->start_date5,$row->end_date5);
	$arr_date[] = setDate($row->start_date6,$row->end_date6);
	$arr_date[] = setDate($row->start_date7,$row->end_date7);
	$arr_date[] = setDate($row->start_date8,$row->end_date8);
	$arr_date[] = setDate($row->start_date9,$row->end_date9);
	$arr_date[] = setDate($row->start_date10,$row->end_date10);
	$arr_date[] = setDate($row->start_date11,$row->end_date11);
	$arr_date[] = setDate($row->start_date12,$row->end_date12);
	$arr = array();
	foreach($arr_date as $d)
	{
		if($d != "")
			$arr[] =$d;
	}
	$data = implode("<br />",$arr);
	return $data;
}

?>
<script language="javascript">
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteTraing/",{
									id:objId
									},function(data){
										if(data=="OK")
											location.href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/TrainingList';
										else
											alert(data);
									});
	}
}
function deleteAll()
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$("#banklist-form").submit();
	}
}
</script>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'banklist-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <input type="hidden" name='act' value="deleteall" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center"><br />
<span class="text4">ข้อมูล Training</span><br />
&nbsp;<br /></td>
      </tr>
      <tr>
        <td align="center" width="100%" class="tabletest">
          <table border="0" cellpadding="0" cellspacing="1">
            <tr>
              <th width="15%">เลือก</th>
              <th >หัวข้อ</th>
              <th >วันที่</th>
              <th >เวลา</th>
              <th >ราคา</th>
              <th width="15%">แก้ไข</th>
              <th width="15%">ลบ</th>
            </tr>
            <?php 
			foreach($data as $row){
			?>
            <tr>
              <td width="15%" align="center">
              <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id;?>' /></td>
              <td align="center">
              <?php echo $row->subject_th; ?>
              </td>
              <td align="center">
              <?php echo showDate($row); ?> 
              </td>
              <td align="center">
              <?php echo $row->duration; ?>
              </td>
              <td align="center">
              <?php echo $row->price; ?>
              </td>

              
              <td width="15%" align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Training/<?php echo $row->id;?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
              <td width="15%" align="center">
              <a href='javascript:deleteit("<?php echo $row->id;?>");'>
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
            </tr>
           <?php
           }
		   ?>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br />
          <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Training/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br /></td>
      </tr>
      
    </table>
            <?php $this->endWidget(); ?>