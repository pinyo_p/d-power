<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'Jobs';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
    <script type="text/javascript" language="javascript">
	$(function() {
		$( "#tabs" ).tabs();
		CKEDITOR.replace('Joblist_skill', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Joblist_jobdetail', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
	});
	
	</script>
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table  border="0" cellspacing="0" cellpadding="0" >
            <tr>
                          <th valign="top" align="right"><span class="txt_green txt_bold">ตำแหน่ง    :</span></th>
                          <td align="left" valign="top"><?php echo $form->textField($model,'position'); ?></td>
                          </tr>
                        <tr>
                          <th valign="top" align="right"><span class="txt_green txt_bold">การศึกษา     :</span></th>
                          <td align="left" valign="top"><?php echo $form->textField($model,'education'); ?></td>
                          </tr>
                        <tr>
                          <th  valign="top" align="right"><span class="txt_green txt_bold">ความสามารถ     :</span></th>
                          <td width="400" align="left" valign="top">
                          <div style="width:600px">
                          <?php 
		Yii::import('ext.ckeditor.*');
		$this->widget('application.extensions.ckeditor.CKEditor', array(
'model'=>$model,
'attribute'=>'skill',
'language'=>'en',
'editorTemplate'=>'full',
'height'=>'800px',
)); ?></div>
                          </td>
                          </tr>
                        <tr>
                          <th valign="top" align="right"><span class="txt_green txt_bold">ประสบการณ์     :</span></th>
                          <td align="left" valign="top"><p>
                           <?php echo $form->textField($model,'experience'); ?>
                          </p></td>
                        </tr>
                        <tr>
                          <th valign="top" align="right"><span class="txt_green txt_bold">ประเภท    :</span></th>
                          <td align="left" valign="top"><p>
                           <?php echo $form->textField($model,'type'); ?>
                          </p></td>
                        </tr>
                        <tr>
                          <th valign="top" align="right"><span class="txt_green txt_bold">รายละเอียดงาน      :</span></th>
                          <td align="left" valign="top">
                           <div style="width:600px">
                          <?php 
		Yii::import('ext.ckeditor.*');
		$this->widget('application.extensions.ckeditor.CKEditor', array(
'model'=>$model,
'attribute'=>'jobdetail',
'language'=>'en',
'editorTemplate'=>'full',
'height'=>'800px',
)); ?></div>        
                         </td>
                        </tr>
                        <tr>
                          <th align="right"><span class="txt_green txt_bold">
                          Publish Date :</span>
                          </th>
                          <td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Joblist[publish_date]',
					'value'=>$model->publish_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                          </td>
                        </tr>
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/JobList';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>