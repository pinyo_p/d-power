<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'บัญชีธนาคารของเรา';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table width="50%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <th width="40%" align="right">ชื่อธนาคาร :</th>
              <td width="75%"><?php echo $form->textField($model,'bank_name'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">สาขา :</th>
              <td width="75%"><?php echo $form->textField($model,'acc_branch'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">ชื่อบัญชี :</th>
              <td width="75%"><?php echo $form->textField($model,'acc_name'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">เลขบัญชี :</th>
              <td width="75%"><?php echo $form->textField($model,'acc_no'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">ประเภทบัญชี :</th>
              <td width="75%"><?php echo $form->textField($model,'acc_type'); ?></td>
            </tr>
            
            
            
             <tr>
              <th width="40%" align="right">จัดเรียง :</th>
              <td width="75%"><?php echo $form->textField($model,'sort_order',array('style'=>'width:50px')); ?></td>
            </tr>
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/branchlist';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>