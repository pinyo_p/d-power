<?php 
$this->pageTitle="Admin Panel::Manage Group"; 

?>
<script language="javascript">
function deleteit(objId)
{
	var txtRemark = prompt('หมายเหตุ');
	$.post('<?php echo Yii::app()->createUrl('/admin/CancelBuyer/');?>',{
											 	id:objId,
												remark:txtRemark,
											 },function(data){
												 if(data=="OK")
												 	location.reload();
												else
													alert(data);
											 });
	
}
</script>
<link type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/smoothness/jquery-ui-1.8.4.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.13.custom.min.js"></script>



<div align="center">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'buyer',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>



<div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">ผู้ซื้อ</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ผู้ซื้อ</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table>
<br /><br />
<table bgcolor="#00CC00"  cellpadding="10" cellspacing="1">
<tr style="color:white;font-weight:bold" valign="top">
<td colspan="4">
ข้อมูลผู้ซื้อ
</td>
</tr>
 <tr bgcolor="white">
<td>
ชื่อ 
</td>
<td>
<?php echo $form->textField($buyer,'first_name'); ?> <?php echo $form->error($buyer,'first_name'); ?>
</td>
<td>นามสกุล
</td>
<td>
<?php echo $form->textField($buyer,'last_name'); ?> <?php echo $form->error($buyer,'last_name'); ?>
</td>
</tr>
 <tr bgcolor="white">
<td>
ราคาที่ขายได้
</td>
<td colspan="3">
<?php echo $form->textField($buyer,'price'); ?> <?php echo $form->error($buyer,'price'); ?>
</td>
</tr>
 <tr bgcolor="white">
 <td>
 วันที่ขายได้</td>
 <td colspan="3">
<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$buyer, 
                    'name'=>'Buyer[sale_date]',
					'value'=>$buyer->sale_date,
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),)); ?>
 </td>
 </tr>
 <tr bgcolor="white">
 <td>วิธีการขาย</td>
 <td>
<?php echo $form->dropDownList($buyer,'sale_type', array('1'=>'ซื้อตรง',2=>'ประมูล')); ?>
 </td>
 <td>
 หมายเหตุ
 </td>
 <td>
<?php echo $form->textField($buyer,'remark'); ?> <?php echo $form->error($buyer,'remark'); ?>
 </td>
 </tr>
  <tr bgcolor="white">
 <td colspan="4" align="center">
 <div class="row buttons">
		<a href="javascript:;" onclick="$('#buyer').submit()">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" width="52" height="25" /></a>
	</div>
 </td>
 </tr>
</table>
</div>

<?php $this->endWidget(); ?>
