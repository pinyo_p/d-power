<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = ' รายการข่าวสาร & โปรโมชั่น';
?>

<script type="text/javascript" language="javascript">
    var roxyFileman = '<?php echo Yii::app()->request->baseUrl; ?>/fileman/pagefile.html';
    $(function () {
        $("#tabs").tabs();
        CKEDITOR.replace('Content_content_2', {
            //filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
        CKEDITOR.replace('Content_content_1', {
            //filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
    });

</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'news-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<table width="100%">
    <tr>

        <td align="center"><h3 class="underline"><?php echo (isset($_GET["id"])) ? "แก้ไข" : "เพิ่ม"; ?>ข้อมูล</h3></td>
    </tr>
    <tr>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="add_data">

            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th width="40%" align="right" style="vertical-align: top;">เนื้อหา :</th>
                    <td>

                        <div style="width:800px;  text-align:left">

                            <div id="tabs" >
                                <ul>
                                    <li><a href="#tabs-1">ภาษาไทย</a></li>
                                    <li><a href="#tabs-2">ภาษาอังกฤษ</a></li>
                                </ul>
                                <div id="tabs-1">
                                    หัวข้อ (ภาษาไทย)<br />
                                    <?php echo $form->textField($model, 'title_2', array('style' => 'width:90%')); ?>
                                    <?php echo $form->error($model,'title_2'); ?>
                                    <br /><br />
                                    เนื้อหาย่อ (ภาษาไทย)<br />
                                    <?php echo $form->textArea($model, 'short_content_2', array('style' => 'width:98%;height:100px;')); ?>
                                    <br /><br />
                                    เนื้อหาทั้งหมด (ภาษาไทย)<br />
                                    <?php
                                    Yii::import('ext.ckeditor.*');
                                    $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $model,
                                        'attribute' => 'content_2',
                                        'language' => 'en',
                                        'editorTemplate' => 'full',
                                        'height' => '800px',
                                    ));
                                    ?>
                                </div>
                                <div id="tabs-2">
                                    หัวข้อ (ภาษาอังกฤษ)
                                    <br />
                                    <?php echo $form->textField($model, 'title_1', array('style' => 'width:90%;')); ?>
                                    <?php echo $form->error($model,'title_1'); ?>
                                    <br /><br />
                                    เนื้อหาย่อ (ภาษาอังกฤษ)<br />
                                    <?php echo $form->textArea($model, 'short_content_1', array('style' => 'width:98%;height:100px;')); ?>
                                    <br /><br />
                                    เนื้อหา (ภาษาอังกฤษ)<br />
                                    <?php
                                    Yii::import('ext.ckeditor.*');
                                    $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $model,
                                        'attribute' => 'content_1',
                                        'language' => 'en',
                                        'editorTemplate' => 'full',
                                        'height' => '600px',
                                    ));
                                    ?>
                                </div>

                            </div>
                            
                        </div>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">จัดเรียง :</th>
                    <td width="75%"> 
                        <?php echo $form->textField($model, 'sort_order', array('style' => 'width:50px')); ?><?php echo $form->error($model, 'sort_order'); ?>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">ภาพหลัก :</th>
                    <td width="75%"> 
                        <?php
                                    $file = Yii::app()->request->baseUrl . '/images/content/' . $model->attr1;
                                    if (file_exists(Yii::app()->basePath . '/../images/content/' . $model->attr1) && $model->attr1 != "") {
                                        ?><br />
                                        <img src="<?php echo $file; ?>" height="83" />
                                        <?php
                                    }
                                    ?><br /> 
                                    <input type="file" name="files" id='files' />
                    </td>
                </tr>
            </table>

            <br />
            <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/contentnewslist'; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
    </tr>

</table>
<?php $this->endWidget(); ?>