<?php 
$this->pageTitle="Admin Panel::Disclaimer"; 
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true,uploadURI : '<?php echo Yii::app()->request->baseUrl;?>/nicUpload.php'}) });
	$(function() {
		$( "#tabs" ).tabs();
	});
	function submitme()
	{
		var nicE_en = new nicEditors.findEditor('Disclaimer_disclaimer_en');
		var nicE_th = new nicEditors.findEditor('Disclaimer_disclaimer_th');
		content_en = nicE_en.getContent();
		content_th = nicE_th.getContent();
		$("#Disclaimer_disclaimer_en").val(content_en);
		$("#Disclaimer_disclaimer_th").val(content_th);
		$("#Disclaimer").submit();
		
	}
</script>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'Disclaimer',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">เงื่อนไขการใช้บริการ</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt"> เงื่อนไขการใช้บริการ</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td>
              <table width="600" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>
                          
                          <div id="tabs" class="bend_tab">
                            <ul>
                              <li>
                              <a href="#tabs-1"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/th.png" width="16" height="11" /> &nbsp;ภาษาไทย</a></li>
                              <li>
                              <a href="#tabs-2">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/en.png" width="17" height="11" />&nbsp; ภาษาอังกฤษ</a></li>
                            </ul>
                            <div id="tabs-1">
                            <p>
                             <?php echo $form->textArea($model,'disclaimer_th',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                            </p>
                            </div>
                            <div id="tabs-2">
                            <p>
                              <?php echo $form->textArea($model,'disclaimer_en',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                            </p>
                            </div>
                            <center>
                            <p><a href="javascript:;" onclick="submitme()">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" width="52" height="25" /></a></p>
                         </center>
                            
                          </div>
                          
                          </td>
                        </tr>
                      </table>
                      <br />
  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
         <?php $this->endWidget(); ?>