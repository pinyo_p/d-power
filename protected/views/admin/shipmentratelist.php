<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายการอัตราค่าขนส่ง';
?>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteShipmentRate?shiptype=<?php echo $_POST["ddlType"];?>", {
                id: objId
            }, function (data) {
                if (data.indexOf("OK") > -1)
                    location.href = '<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ShipmentRateList?shiptype=<?php echo $_POST["ddlType"];?>';
                else if(data.indexOf("DENIED") > -1)
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
                else
                    alert(data);
            });
        }
    }
    function deleteAll()
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $("#banklist-form").submit();
        }
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'banklist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' value="deleteall" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">



    <tr>
        <td align="center"><br />
            <span class="text4">ค้นหารายการอัตราค่าขนส่ง</span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="right" style="width:200px;">ประเภทการจัดส่ง :</th>
                    <td>
                        <select name="ddlType" id="ddlType">
                            <option value="in_reg" <?php echo ($_POST["ddlType"] == 'in_reg' || $_GET["shiptype"] == 'in_reg')?"selected='selected'":""; ?>>ลงทะเบียน (ในประเทศ)</option>
                            <option value="in_ems" <?php echo ($_POST["ddlType"] == 'in_ems' || $_GET["shiptype"] == 'in_ems')?"selected='selected'":""; ?>>EMS (ในประเทศ)</option>
                            <option value="in_gen" <?php echo ($_POST["ddlType"] == 'in_gen' || $_GET["shiptype"] == 'in_gen')?"selected='selected'":""; ?>>ธรรมดา (ในประเทศ)</option>
                        </select

                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center"> <br /><br /><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">เลือก</th>
                    <th>ประเภทจัดส่ง</th>
                    <th >จากน้ำหนัก (กรัม)</th>
                    <th >ถึงน้ำหนัก (กรัม)</th>
                    <th >อัตราค่าขนส่ง</th>
                    <th width="60">แก้ไข</th>
                    <th width="60">ลบ</th>
                </tr>
<?php
foreach ($data as $row) {
    ?>
                    <tr>
                        <td align="center">
                            <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' />
                        </td>
                        <td>
    <?php
    switch ($row->shipping_type) {
        case "in_reg":
            echo "ลงทะเบียน (ในประเทศ)";
            break;
        case "in_ems":
            echo "EMS (ในประเทศ)";
            break;
        case "in_gen":
            echo "ธรรมดา (ในประเทศ)";
            break;
    }
    ?>
                        </td>
                        <td align="center"><?php echo number_format($row->weight_from, 0, ".", ","); ?></td>
                        <td align="center"><?php echo number_format($row->weight_to, 0, ".", ","); ?></td>
                        <td align="center"><?php echo number_format($row->cost_price, 0, ".", ","); ?></td>
                        <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ShipmentRate/<?php echo $row->id; ?>?shiptype=<?php echo $_POST["ddlType"]; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
                        <td align="center">
                            <a href='javascript:deleteit("<?php echo $row->id; ?>");'>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
                    </tr>
    <?php
}
?>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br />
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ShipmentRate?shiptype=<?php echo $_POST["ddlType"]; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<script type="text/javascript">
    $(document).ready(function(){
       $('#ddlType').change(function(){
            $("#banklist-form").submit(); 
       });
    });
    </script>
<?php $this->endWidget(); ?>