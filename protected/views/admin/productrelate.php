
<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายการสินค้า';
?>
<script language="javascript">

    function addAll()
    {

        $("#act").val("addall");
        $("#productlist-form").submit();

    }


    function searchit()
    {

        $("#productlist-form").submit();

    }
    function gotoPage(objPage)
    {
        $("#page").val(objPage);
        $("#productlist-form").submit();
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'productlist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4">ค้นหาสินค้า</span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="right">ชื่อสินค้า :</th>
                    <td ><?php echo $form->textField($model, 'product_name'); ?></td>
                    <th align="right">รหัสสินค้า :</th>
                    <td ><?php echo $form->textField($model, 'product_code'); ?></td>
                </tr>
                <tr>
                    <th align="right">หมวดหมู่ :</th>
                    <td ><?php echo $form->dropDownList($model, 'group_id', CHtml::listData(ProductGroup::model()->findAll(array('order' => 'group_2 ASC')), 'id', 'group_2'), array('empty' => 'เลือก หมวดหมู่',
                        ));
?><?php echo $form->error($model, 'group_id'); ?></td>
                    <th align="right">สถานะ :</th>
                    <td> <?php echo $form->dropDownList($model, 'status', array('0' => 'รออนุมัติ', '1' => 'ประกาศขาย', '2' => 'ยกเลิก'), array('empty' => ' ทุกประเภท',
                        ));
?></td>
                </tr>
                <tr>
                    <th align="right"></th>
                    <td ></td>
                    <td  colspan="2" align="center"><input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" width="55" height="26" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center"> <br /><br /><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>

    <tr>
        <td align="center" width="100%" class="tabletest">




            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">เลือก</th>
                    <th  width="120">Logo</th>
                    <th width="120" >รหัสสินค้า</th>
                    <th >ชื่อสินค้า</th>
                </tr>
<?php
foreach ($data as $row) {
    ?>
                    <tr>
                        <td align="center">
                            <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' /></td>
                        <td align="center">
                            <?php
                            $file = Yii::app()->request->baseUrl . '/images/product/' . $row->pic1;
                            if (file_exists(Yii::app()->basePath . '/../images/product/' . $row->pic1) && $row->pic1 != "") {
                                ?>
                                <img src="<?php echo $file; ?>" height="83" />
                                <?php
                            } else {
                                ?>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
        <?php
    }
    ?>
                        </td>
                        <td align="left">
                            <?php echo $row->product_name_2; ?> / <?php echo $row->product_name_1; ?>
                        </td>
                        <td align="left">
                    <?php echo $row->product_code; ?> / <?php echo $row->product_code2; ?><br />
                    <?php echo iconv_substr(trim(strip_tags($row->product_desc_2)), 0, 100, "UTF-8") . "..."; ?>
                        </td>
                    </tr>
                <?php
            }
            ?>
            </table>
<?php
if ($param['max_page'] > 1) {
    ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                        <td style="border:none;"><div class="nav_page">
                                <ul>
                                    <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
                                    <?php
                                    $start_page = ($page > 2 ? $page - 2 : 1);
                                    $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
                                    if ($end_page <= 5)
                                        $end_page = 5;
                                    else if (($end_page - $page) < 5)
                                        $start_page = ($end_page - 4);
                                    if ($end_page > $param['max_page'])
                                        $end_page = $param['max_page'];
                                    for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                                        $class = "";
                                        if (($i + 1) == $page)
                                            $class = '  class="navselect"';
                                        else
                                            $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                                        ?>
                                        <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                        <?php
                                    }
                                    ?>
                                    <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                                </ul>
                            </div></td>
                        <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                            <table width="200"  style="border:none;"><tr><td style="border:none;">
                                        <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                        <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                        <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png" width="39" height="22" /></a></td></tr></table>


                        </td>
                    </tr>
                </table>
                <?php
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="center">



            <br />
            <br />
            <br />
            <a href="javascript:addAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/ProductRelateList/' . $_GET['id']; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a></td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>