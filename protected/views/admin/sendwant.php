<?php 
$this->pageTitle="Admin Panel::ความต้องการฝากขายทรัพย์สิน"; 
function g($val,$caption)
{
	return (trim($val)!=""?$caption . " " . $val:"");
}
function g2($val,$caption)
{
	return (trim($val)!="" && $val!="0"?$val . $caption:"");
}
function getDirection($sortby,$model)
{
	$dir = 1;
	if($sortby==$model->sort_by && $model->sort_dir == 1)
		$dir = 2;
	return $dir;
}
function getDirectionIcon($sortby,$model)
{
	$img = "";
	if($sortby==$model->sort_by){
		if($model->sort_dir=="2")
		{
			$img = "sort_botton_down.png";
		}else{
			$img = "sort_botton_up.png";
		}
	}
	if($img != "")
	{
		$img = "<img src='" . Yii::app()->request->baseUrl . "/images/$img' />";
	}
	return $img;
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteWant/');?>',{id:objId},function(data){
																							  location.reload();
																							   });
	}
	
}
function searchit()
{
	if($("#Property_start_price").val()!="" && $("#Property_end_price").val()!="" && ($("#Property_start_price").val()>$("#Property_end_price").val()  ))
	{
		alert('ราคาเริ่มต้นต้องน้อยกว่าราคาสิ้นสุด');
	}else{
		$("#frm_search").submit();
	}
}
function sendit()
{
	$("#act").val("sendit");

	$("#frm_search").submit();
}
</script>
<style type="text/css">
.hd a{
	color:white;
}
</style>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt;<a href="#" class="link_green"> NPA</a> &gt; <a href="#" class="link_green">ความต้องการฝากขายทรัพย์สิน</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ความต้องการฝากขายทรัพย์สิน [รายชื่อผู้ฝาก]</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td  class="nparesult_table_content">
              <center>
              <br />
              
<br /><br />
</center>
              

              
              <table width="100%" border="0" cellpadding="0" cellspacing="2">
              <tr class="nparesult_table_header" style="padding:5px">
              <td colspan="6" align="center" style="padding:5px">ข้อมูลลูกค้า</td>
              <td colspan="7" align="center" style="padding:5px">ความต้องการ</td>
              </tr>
                <tr  class="nparesult_table_header">
                  <td width="5%" align="center" valign="top" class="rowa">ลำดับ</td>
                  <td align="center" valign="top" class="rowa">ชื่อ</td>
                  <td align="center" valign="top" class="rowa">นามสกุล</td>
                  <td align="center" valign="top" class="rowa">เบอร์โทรศัพท์</td>
                  <td align="center" valign="top" class="rowa">อีเมลล์</td>
                   <td align="center" valign="top" class="rowa">ราคา</td>
                   <td align="center" valign="top" class="rowa">ประเภท</td>
                  <td align="center" valign="top" class="rowa">จังหวัด</td>
                  <td align="center" valign="top" class="rowa">อำเภอ</td>
                  <td align="center" valign="top" class="rowa">ตำบล</td>
                  <td align="center" valign="top" class="rowa">ถนน</td>
                  <td width="5%" align="center" valign="top" class="rowa">สถานะ</td>
                </tr>
                <?php
				$i =1;
				foreach($data2 as $row){
				?>
                <tr>
                  <td width="5%" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i++;?></span></td>
                  <td align="center" valign="top" class="rowa"><?php echo $row->first_name;?></td>
                  <td align="center" valign="top" class="rowa"><?php echo $row->last_name;?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->phone_no;?></td>
                  <td align="center" valign="top" class="rowa"><?php echo $row->email;?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->start_price . ' - ' . $row->end_price;?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->category['product_group'];?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->p['thai_name'];?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->d['thai_name'];?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->ta['thai_name'];?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->road;?></td>
                  <td class="rowa" width="5%" align="center" valign="top">
                  <?php
				  if($row->status=="0")
				  	echo "ฝากความต้องการใหม่";
					else
					echo "ส่งข้อมูลแล้ว";
				  
				  ?>
                  </td>
                </tr>
                <?php
				}
				?>
              </table>
              <center>
              <br /><br />
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ความต้องการฝากขายทรัพย์สิน [ค้นหารายการทรัพย์]</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                </table>
                             <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_search',
	'enableClientValidation'=>true,
	'action' => Yii::app()->createUrl('/admin/sendwant/'),
	 
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<input type="hidden" value="search" id="act" name="act" />
<input type="hidden" value="<?php echo $param['in'];?>" id="in" name="in" />


<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0" class="Searchnpa">
                        <tr>
                          <td width="30%" height="38"><span class="txt_bold">รหัสทรัพย์สิน :</span>
                            <?php echo $form->textField($model,'product_code',array('style'=>"width:185px",'class'=>"txt_bold")); ?></td>
                          <td width="16%" align="right"><span class="txt_bold">ราคาประกาศขายขั้นต่ำ :&nbsp;</span></td>
                          <td width="16%"><?php echo $form->textField($model,'start_price',array('style'=>'width:100px;','class'=>"txt_bold")); ?>
                            &nbsp;บาท</td>
                          <td width="38%" rowspan="3">
                          <table class="txt_bold"><tr>
                          <td>ภาค :</td><td><?php echo $form->dropDownList($model,'continent', CHtml::listData(Continent::model()->findAll(array('order' => 'thai_name ASC')), 'id', 'thai_name'), array('empty'=>'ภูมิภาค',	
																																										   'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getprovince/' ,
      			'success'=>'js:function(html){jQuery("#Property_province").html(html);jQuery("#Property_district").html("<option value=0>เลือกจังหวัด</option>");}',),
	  )); ?></td>
      <td>จังหวัด :</td><td>
      <?php echo $form->dropDownList($model,'province', $param['province'], array('empty'=>'ทุกจังหวัด',																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getdistrict/' ,
      			'success'=>'js:function(html){jQuery("#Property_district").html(html);jQuery("#Property_tambol").html("<option value=0>เลือกอำเภอ</option>");}',),
	  )); ?>
      </td></tr><tr>
      <td>อำเภอ :</td><td>
                          <?php echo $form->dropDownList($model,'district',$param['district'], array('empty'=>'ทุกอำเภอ',
																																											   																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/gettambol/' ,
      			'success'=>'js:function(html){jQuery("#Property_tambol").html(html);}',),)); ?>
                          </td><td>ตำบล
 :                         </td><td>
                           <?php echo $form->dropDownList($model,'tambol', $param['tambol'], array('empty'=>'ทุกตำบล')); ?>
                            </td></tr><tr><td>ถนน :</td><td>
                           <?php echo $form->textField($model,'road',array('style'=>"width:110px",)); ?> </td>
                            </tr>
      
      </table></td>
                        </tr>
                        <tr>
                          <td width="30%"><span class="txt_bold">ประเภททรัพย์สิน : </span>
                             <?php echo $form->dropDownList($model,'product_type', CHtml::listData(ProductGroup::model()->findAll(array('order' => 'product_group ASC')), 'id', 'product_group'), array('empty'=>'ทุกประเภท',																																												  				 
	  )); ?></td>
                          <td width="16%" align="right"><span class="txt_bold">ถึง&nbsp;</span></td>
                          <td width="16%"><?php echo $form->textField($model,'end_price',array('style'=>'width:100px;')); ?> 
                            &nbsp;บาท</td>
                          
                        </tr>
                        <tr>
                          <td width="30%"><p><span class="txt_bold">สถานะประกาศขาย :</span>
                          <?php echo $form->dropDownList($model,'status',array(1=>'ซื้อตรง',2=>'ประมูล',3=>'ขายแล้ว',4=>'รออนุมัติ',5=>'อนุมัตแล้วรอประกาศ'), array('empty'=>'- ทุกประเภท -',																																												  				 
	  )); 
						  ?>
                          </p></td>
                          <td width="16%" align="right"><span class="txt_bold">วันที่ประกาศขายตั้งแต่ :&nbsp;</span></td>
                          <td width="16%"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
					'value'=>$model->start_date,
                    'name'=>'Property[start_date]',
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'width:70px'
    ),)); ?></td>
                          
                        </tr>
                        <tr>
                          <td width="30%"><span class="txt_bold">คำค้นหาที่เกี่ยวข้อง :</span>
                            <?php echo $form->textField($model,'key_word',array('style'=>'width:153px;')); ?></td>
                          <td width="16%" align="right"><span class="txt_bold">ถึง&nbsp;</span></td>
                          <td width="16%"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
                    'name'=>'Property[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'width:70px'
    ),)); ?>
    
    </td>
                          <td width="38%"><span class="txt_bold">ขนาดพื้นที่ :</span>&nbsp; จำนวน&nbsp;&nbsp;
                            <?php echo $form->textField($model,'area_start',array('style'=>'width:80px;')); ?> 
                            &nbsp;&nbsp;ถึง&nbsp;&nbsp;
                            <?php echo $form->textField($model,'area_end',array('style'=>'width:80px;')); ?> 
                            &nbsp;ไร่</td>
                        </tr>
                        
                        <tr>
                          <td width="30%">&nbsp;</td>
                          <td width="30%" colspan="2">&nbsp;</td>
                          <td width="38%" align="right">
                         
                          <a href="javascript:;" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/npa_search.png" alt="" width="62" height="25" /></a> <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Admin/PropertyList/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/npa_cancle.png" alt="" width="62" height="25" /></a></td>
                        </tr>
                      </table>
                      <br />
                      <table bgcolor="#78CF2D" width="100%" cellpadding="10" cellspacing="1">
<tr class="hd" style="background-color:#78CF2D;color:white;font-weight:bold" valign="top">

<td>
<input type="checkbox" name="check_all" id='check_all' />
</td>

<td>รูปประกอบ</td>
<td width="70px">
<a href='javascript:sortit("1","<?php echo getDirection(1,$model);?>")'>รหัสทรัพย์<br /><?php echo getDirectionIcon(1,$model);?></a>
</td>
<td><a href='javascript:sortit("2","<?php echo getDirection(2,$model);?>")'>ประเภททรัพย์สิน<br /><?php echo getDirectionIcon(2,$model);?></a></td>
<td>
ที่ตั้ง
</td>
<td><a href='javascript:sortit("3","<?php echo getDirection(3,$model);?>")'>จังหวัด<br /><?php echo getDirectionIcon(3,$model);?></a></td>
<td>เนื้อที่</td>
<td><a href='javascript:sortit("4","<?php echo getDirection(4,$model);?>")'>ราคาขั้นต่ำ<br /><?php echo getDirectionIcon(4,$model);?></a></td>
<td>
<a href='javascript:sortit("5","<?php echo getDirection(5,$model);?>")'>สถานะ<br /><?php echo getDirectionIcon(5,$model);?></a>
</td>

</tr>

<?php
foreach($data as $row)
{
	?>
    <tr bgcolor="white">
  


	<td>
		<input type="checkbox" id='chk_<?php echo $row['id'];?>' name="check_box[]" value="<?php echo $row['id'];?>" class="check_all" />
        </td>
  
<td>
<?php
echo CHtml::image(Yii::app()->request->baseUrl . "/images/npa/" . $row->id ."/"  . $row->main_image , "",array("width"=>"100px"));
?>
</td>
<td align="center"><b>
<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->id;?>/?type=1'  target="_blank" >
<?php echo $row->product_code;?>
</a></b>
</td>
<td>
<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->id;?>/?type=1'  target="_blank" >
<?php
echo $row->category['product_group'] ;
?>
</a>

</td><td>
<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->id;?>/?type=1'  target="_blank" >
 <?php echo g($row->road,'ถนน');?> <?php echo g($row->ta['thai_name'],'ต.');?> 
    <?php echo g($row->d['thai_name'],'อ.');?></a></td>
 <td><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->id;?>/?type=1'  target="_blank" ><?php echo g($row->p['thai_name'],' ');?></a></td>
 <td><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->id;?>/?type=1'  target="_blank" ><?php echo g2($row->area_rai," ไร่");?> <?php echo g2($row->area_ngan," งาน");?> <?php echo g2($row->area_wa," ตร.ว.");?></a></td>
 <td>
<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->id;?>/?type=1'  target="_blank" > <?php echo number_format($row->price,0,".",",");?> </a>
 </td>
<td><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->id;?>/?type=1'  target="_blank" >
<?php
switch($row->status)
{
	case "0":echo "รอการอนุมัติ";break;
	case "1":{
		if($row->sale_type=="0")
			echo "<img src='" . Yii::app()->request->baseUrl . "/images/buy4.gif' height='30' />" ;
		else
			echo "<img src='" . Yii::app()->request->baseUrl . "/images/button_status_6_auction21.gif' height='30' />";
		}break;
	case "2":echo "<img src='" . Yii::app()->request->baseUrl . "/images/button_status_6_sold2.gif' height='30' />";break;
	case "3":echo "ยกเลิกแล้ว";break;
}
?></a>
</td>
</tr>
    <?php
	
//	print_r($row);
//	echo "<hr />";
}
?>
<tr>
<td colspan="9" bgcolor="white">
<center>
        <br /><a href='javascript:sendit()'>
         <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/b_save.png" />
         </a>
         <br /><br />
        </center>
</td>
</tr>
 <?php $this->endWidget(); ?>
           
          </table>
        
        <script language="javascript">
$("#check_all").click(function(){
							   $(".check_all").attr("checked",$('#check_all').is(':checked'));
							   });
</script>