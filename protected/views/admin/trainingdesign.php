<?php
$this->pageTitle=Yii::app()->name . ' - Intro Page';
$b = "Training Page";

$this->breadcrumbs = $b;

		  function setDate($start_date,$end_date,$i)
		  {
			  	$e = "";
				$s = "";
				if(trim($start_date)!=""&&trim($end_date)!=""){
					$d = explode("-",$start_date);
					$d2 = explode("-",$end_date);

					if($d[1]==$i)
					{
						$s = $d[2] . ' - ' . $d2[2] ;
						if($start_date == $end_date)
							$s = $d[2];
					}
					
				}
				return $s;
		  }
		  function showDate($row,$i)
		  {
				$s[] = setDate($row->start_date,$row->end_date,$i);
				$s[] = setDate($row->start_date2,$row->end_date2,$i);
				$s[] = setDate($row->start_date3,$row->end_date3,$i);
				$s[] = setDate($row->start_date4,$row->end_date4,$i);
				$s[] = setDate($row->start_date5,$row->end_date5,$i);
				$s[] = setDate($row->start_date6,$row->end_date6,$i);
				$s[] = setDate($row->start_date7,$row->end_date7,$i);
				$s[] = setDate($row->start_date8,$row->end_date8,$i);
				$s[] = setDate($row->start_date9,$row->end_date9,$i);
				$s[] = setDate($row->start_date10,$row->end_date10,$i);
				$s[] = setDate($row->start_date11,$row->end_date11,$i);
				$s[] = setDate($row->start_date12,$row->end_date12,$i);
				$arr = array();
				foreach($s as $r)
				{
					if(trim($r) != "")
						$arr[] = $r;
				}
				$str = implode(",",$arr);
				return $str;
				
		  }
		  ?>
          <style type="text/css">
		  .sideborder {
	border-top: solid 1px #CCC;
	}
	
.training_table table {
	border: 1px solid #ddd;
	border-collapse:collapse;
	}
	
.training_table table th {
	border: 1px solid #ddd;
	padding: 3px;
	background: #eee;
	height: 30px;
	font-weight: bold;
	}

.training_table table td {
	border: 1px solid #ddd;
	padding: 5px 3px 5px 3px;
	}
	
.centeralign {
	text-align: center;}

.traintopic a,.traintopic a:visited,.traintopic a:active {
	color:#006d2f !important;
	text-decoration: none;
	font-size: 12px;
	}
	
.traintopic a:hover{
	color:#ed1c24 !important;
	text-decoration: none;
	}
	
.trainstatus {
	font-weight: bold;
	}
	
.open {
	color: #0054a6;
	}
	
.regis {
	color: #598527;
	}
	
.regis_close {
	color: #ed1c24;
	}
	
.training {
	color: #f26522;
	}
	
.closed {
	color: #630460;
	}
		  </style>
<script type="text/javascript" language="javascript">
	$(function() {
		$( "#tabs" ).tabs();
		$( "#tabs2" ).tabs();
		CKEDITOR.replace('Content_content_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Content_content_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		
		CKEDITOR.replace('Content2_content_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Content2_content_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
	});
	
	</script>

<div class="form">
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'content-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
<br  /><br  /><br  />
<center>
<div style="width:800px;  text-align:left">

<div id="tabs" >
	<ul>
		<li><a href="#tabs-1">ภาษาไทย</a></li>
		<li><a href="#tabs-2">ภาษาอังกฤษ</a></li>
	</ul>
	<div id="tabs-1">
		<?php 
		Yii::import('ext.ckeditor.*');
		$this->widget('application.extensions.ckeditor.CKEditor', array(
'model'=>$model,
'attribute'=>'content_th',
'language'=>'en',
'editorTemplate'=>'full',
'height'=>'800px',
)); ?>
	</div>
	<div id="tabs-2">
		<?php 
		Yii::import('ext.ckeditor.*');
		$this->widget('application.extensions.ckeditor.CKEditor', array(
'model'=>$model,
'attribute'=>'content_en',
'language'=>'en',
'editorTemplate'=>'full',
'height'=>'600px',
)); ?>
	</div>
	
</div>

<br />
<table width="100%" style="background-color:white;"><tr><td class="main-column-bg-index ">
            <div class="main-column-content-index">
             

            
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                <td>
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                      <tr>
                        <td align="center" class="trainstatus">สถานะการอบรม : <span class="open">เตรียมการอบรม</span> | <span class="regis">เปิดให้ลงทะเบียน</span> | <span class="regis_close">ปิดลงทะเบียน</span> | <span class="training">อยู่ในระหว่างอบรม</span> | <span class="closed">ปิดอบรมแล้ว</span></td>
                      </tr>
                </table><br />

                </td>
                </tr>
                <tr>
                 <td class="job-content-header" colspan="16">
                  Training
                  </td>
                 </tr>
                 <tr><td class="training_table">
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                  
                  </tr>
                    <tr>
                      <th>Code</th>
                      <th>Name</th>
           <th >Days</th>
            <th >Fee(฿)</th>
             	 	 	
            <?php
			for($i = 1;$i<=12;$i++)
			{
				?>
                <th ><?php echo date("M",mktime(0,0,0,$i,1,2011)); ?></th>
                <?php
			}
			?>
                    </tr>
					<?php
					


					  foreach($data as $row){
					  ?>
                    <tr valign="top">
                    	<td> <?php echo $row->training_code; ?> </td>
                      <td class="job-content-txt">
                      
                      <a class='link_green' target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/trainingdetail/<?php echo $row->id;?>"><?php echo Yii::d($row->subject_en,$row->subject_th);?>
                </a><br />
 
                       
                        
                      
                        </td>
                        <td align="center" class="job-content-txt">
              <?php echo $row->days; ?>
              </td>
              <td align="center" class="job-content-txt" width="100">
              <?php echo number_format($row->price,0,".",","); ?>
              </td>
             <?php
			 
			 $c = "open";
			switch($row->status)
			{
				case "0":$c="open";break;
				case "1":$c="regis";break;
				case "2":$c="regis_close";break;
				case "3":$c="training";break;
				case "4":$c="closed";break;
			}
			 
			for($i = 1;$i<=12;$i++)
			{
				
				$s = showDate($row,$i);
				
				?>
                <td style="white-space:nowrap"><span class="<?php echo $c;?>"><?php echo $s; ?></span></td>
                <?php
			}
			?>
                    </tr>
                   
                     <?php
					  }
					  ?>
                  </table></td>
                </tr>
              </table>
            
              
                        </div>
                </td>
                </tr>
                </table>    
<br />

<div id="tabs2" >
	<ul>
		<li><a href="#tabs-2-1">ภาษาไทย</a></li>
		<li><a href="#tabs-2-2">ภาษาอังกฤษ</a></li>
	</ul>
	<div id="tabs-2-1">
		<?php 
		Yii::import('ext.ckeditor.*');
		$this->widget('application.extensions.ckeditor.CKEditor', array(
'language'=>'en',
'editorTemplate'=>'full',
'height'=>'800px',
'name'=>'Content2_content_th',
'value'=>$model2->content_th
)); ?>
	</div>
	<div id="tabs-2-2">
		<?php 
		Yii::import('ext.ckeditor.*');
		$this->widget('application.extensions.ckeditor.CKEditor', array(
'language'=>'en',
'editorTemplate'=>'full',
'height'=>'600px',
'name'=>'Content2_content_en',
'value'=>$model2->content_en
)); ?>
	</div>
	
</div>
<br /><br />


<center>
<input type='image' src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />
</center>
</div>

</center>
<?php $this->endWidget(); ?>
</div><!-- form -->
