<?php 
$this->pageTitle="Admin Panel::Export Data "; 
function check_menu2($menu_id)
{
	$group_id = Yii::app()->user->getValue("group_id");
	if($group_id=="16")
	{
		return " style='visibility:hidden;position:absolute' ";
	}
	else
	{
		return "";
	}
}

?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	$(function(){
			   $("#check_all").click(function(){
							   $(".check_all").attr("checked",$('#check_all').is(':checked'));
							   });
			   });
	function exportAll()
	{
		$("#npaimport").attr('action','<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/NpaExportAll');
		$("#npaimport").submit();
		$("#npaimport").attr('action','<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/NpaExport');
	}
</script>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'npaimport',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">Export</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td><td>
                      
                      </td>
                      <td class="topix_header"><div class="topix_headtxt">Export</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td>
              <table width="99%" border="0"  cellpadding="0" cellspacing="0" class="Searchnpa">
                        <tr>
                          <td width="30%" height="38"><span class="txt_bold">รหัสทรัพย์สิน :</span>
                            <?php echo $form->textField($model,'product_code',array('style'=>"width:185px",'class'=>"txt_bold")); ?></td>
                          <td width="16%" align="right"><span class="txt_bold">ราคาประกาศขายขั้นต่ำ :&nbsp;</span></td>
                          <td width="16%"><?php echo $form->textField($model,'start_price',array('style'=>'width:100px;','class'=>"txt_bold")); ?>
                            &nbsp;บาท</td>
                          <td width="38%" rowspan="3">
                          <table class="txt_bold"><tr>
                          <td>ภาค :</td><td><?php echo $form->dropDownList($model,'continent', CHtml::listData(Continent::model()->findAll(array('order' => 'thai_name ASC')), 'id', 'thai_name'), array('empty'=>'ภูมิภาค',	
																																										   'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getprovince/' ,
      			'success'=>'js:function(html){jQuery("#Property_province").html(html);jQuery("#Property_district").html("<option value=0>เลือกจังหวัด</option>");}',),
	  )); ?></td>
      <td>จังหวัด :</td><td>
      <?php echo $form->dropDownList($model,'province', $param['province'], array('empty'=>'ทุกจังหวัด',																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getdistrict/' ,
      			'success'=>'js:function(html){jQuery("#Property_district").html(html);jQuery("#Property_tambol").html("<option value=0>เลือกอำเภอ</option>");}',),
	  )); ?>
      </td></tr><tr>
      <td>อำเภอ :</td><td>
                          <?php echo $form->dropDownList($model,'district',$param['district'], array('empty'=>'ทุกอำเภอ',
																																											   																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/gettambol/' ,
      			'success'=>'js:function(html){jQuery("#Property_tambol").html(html);}',),)); ?>
                          </td><td>ตำบล
 :                         </td><td>
                           <?php echo $form->dropDownList($model,'tambol', $param['tambol'], array('empty'=>'ทุกตำบล')); ?>
                            </td></tr><tr><td>ถนน :</td><td>
                           <?php echo $form->textField($model,'road',array('style'=>"width:110px",)); ?> </td>
                            </tr>
      
      </table></td>
                        </tr>
                        <tr>
                          <td width="30%"><span class="txt_bold">ประเภททรัพย์สิน : </span>
                             <?php echo $form->dropDownList($model,'product_type', CHtml::listData(ProductGroup::model()->findAll(array('order' => 'product_group ASC')), 'id', 'product_group'), array('empty'=>'ทุกประเภท',																																												  				 
	  )); ?></td>
                          <td width="16%" align="right"><span class="txt_bold">ถึง&nbsp;</span></td>
                          <td width="16%"><?php echo $form->textField($model,'end_price',array('style'=>'width:100px;')); ?> 
                            &nbsp;บาท</td>
                          
                        </tr>
                        <tr>
                          <td width="30%"><p><span class="txt_bold">สถานะประกาศขาย :</span>
                          <?php echo $form->dropDownList($model,'status',array(1=>'ซื้อตรง',2=>'ประมูล',3=>'ขายแล้ว',4=>'รออนุมัติ',5=>'อนุมัตแล้วรอประกาศ'), array('empty'=>'- ทุกประเภท -',																																												  				 
	  )); 
						  ?>
                          </p></td>
                          <td width="16%" align="right"><span class="txt_bold">วันที่ประกาศขายตั้งแต่ :&nbsp;</span></td>
                          <td width="16%"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
					'value'=>$model->start_date,
                    'name'=>'Property[start_date]',
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'width:70px'
    ),)); ?></td>
                          
                        </tr>
                        <tr>
                          <td width="30%"><span class="txt_bold">คำค้นหาที่เกี่ยวข้อง :</span>
                            <?php echo $form->textField($model,'key_word',array('style'=>'width:153px;')); ?></td>
                          <td width="16%" align="right"><span class="txt_bold">ถึง&nbsp;</span></td>
                          <td width="16%"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
                    'name'=>'Property[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'width:70px'
    ),)); ?></td>
                          <td width="38%"><span class="txt_bold">ขนาดพื้นที่ :</span>&nbsp; จำนวน&nbsp;&nbsp;
                            <?php echo $form->textField($model,'area_start',array('style'=>'width:80px;')); ?> 
                            &nbsp;&nbsp;ถึง&nbsp;&nbsp;
                            <?php echo $form->textField($model,'area_end',array('style'=>'width:80px;')); ?> 
                            &nbsp;ไร่</td>
                        </tr>
                        
                        
                      </table>
				<center><br />
                <div align="right"><a href='javascript:exportAll();' class="link_search">ส่งออกทั้งหมด</a></div>
                <br /><br />
                <table width="100%" style="text-align:left">
                <tr style="background-color:#78CF2D;color:white;">
                <td colspan="4">
                <table width="100%"><tr><td>
                <input type="checkbox" name="check_all" id='check_all' /><label for="check_all"></label>
                เลือก Fields </label></td><td align="right"><input type="checkbox" name="lang_en" id='lang_en' value="en" /><label for="lang_en">
                ภาษาอังกฤษ</label></td></tr></table>
                </td>

                </tr> 
                <tr>
                <td>
                <input type="checkbox" class="check_all" name="product_code" />รหัสทรัพย์
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="product_type"  />ประเภททรัพย์
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="land_license"  />เลขที่ดิน
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="rawang" />ระวาง
                </td>
                </tr>
                
                <tr>
                <td>
                <input type="checkbox" class="check_all" name="area" />เนื้อที่
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="address" />ที่ตั้ง
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="tambol" />ตำบล
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="district" />อำเภอ
                </td>
                </tr>
                
                <tr>
                <td>
                <input type="checkbox" class="check_all" name="province" />จังหวัด
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="lat-long" />พิกัด
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="near_road" />ติดถนน
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="deep" />ลึก
                </td>
                </tr>
                
                <tr>
                <td>
                <input type="checkbox" class="check_all" name="enter" />การเข้าถึง 
                </td>
                <td> <input type="checkbox" class="check_all" name="product_limit" />ข้อจำกัด</td>
                <td>
                 <input type="checkbox" class="check_all" name="product_desc" />
                 รายละเอียด
                </td>
                <td><input type="checkbox" class="check_all" name="more_detail" />
                  รายละเอียดเพิ่มเติม</td>
                </tr>
                
                <tr>
                <td><input type="checkbox" class="check_all" name="remark" />
                  หมายเหตุ</td>
                <td><input type="checkbox" class="check_all" name="other_desc" />
                  รายละเอียดอื่นๆ</td>
                <td>
                 <input type="checkbox" class="check_all" name="price" />ราคา
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="sale_type" />ประเภทการขาย
                </td>
                </tr>
                
               
                <tr>
                <td>
                <input type="checkbox" class="check_all" name="publish_date" />วันที่ประกาศ
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="end_date" />วันสิ้นสุดประกาศ
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="bid_date" />วันเริ่มประมูล
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="bid_place" />สถานที่ยื่นซอง
                </td>
                </tr>
                
                <tr>
                <td>
                <input type="checkbox" class="check_all" name="create_date" />วันที่ลงประกาศ
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="create_by" />ผู้ลงประกาศ
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="update_date" />วันที่แก้ไขล่าสุด
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="update_by" />ผู้ที่แก้ไขล่าสุด
                </td>
                </tr>
                
                <tr>
                <td>
                <input type="checkbox" class="check_all" name="approve_date" />วันที่อนุมัติ
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="approve_by" />ผู้อนุมัติ
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="delete_date" />วันที่ลบ
                </td>
                <td>
                 <input type="checkbox" class="check_all" name="delete_by" />ผู้ที่ลบ
                </td>
                </tr>
                <tr>
                <td>
                <input type="checkbox" class="check_all" name="attach_document" />ประเภทเอกสารสิทธิ์ , เลขที่เอกสารสิทธิ์
                </td>
                </tr>
               	<tr>
                <td colspan="4" align="center">
             <br /><br />
             <input type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/export_excel.png" alt="Submit button" style="visibility:hidden;position:absolute;" class='lmm_38_4'>

                </tr>
               	<tr>
               	  <td colspan="4" align="center">                
             	  </tr>

                </table>
                </center>
                
              </td>
            </tr>
           
          </table>
<?php
if(isset($_FILES['import_file']) && $_FILES['import_file']['tmp_name']!='')
{
	?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ผลการดำเนินการ</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                    <tr>
                    <td>
                    </td>
                    <td>
                        <table>
                        <td>
                        เพิ่มทั้งสิ้น 1,573 รายการ
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <div class="ui-state-highlight">
                        <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        สำเร็จจำนวน 1,500 รายการ
                        </div>
                         </td>
                        </tr>
                        <tr>
                        <td>
                        <div class="ui-widget">
                        <div class="ui-state-error">
                        <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        ไม่สำเร็จจำนวน 73 รายการ<br />
                        โดย <br />
                        1. รหัสจังหวัดไม่ถูกต้อง 3 รายการคือ รหัสทรัพย์ 8Z7871,8Z7872,8Z7873<br />
                        2. รหัสประเทภทรัพย์ไม่ถูกต้อง 2 รายการ คือ รหัสทรัพย์ 8Z8792,8Z5578
                        </div>
                        </div>
                        </td>
                        </tr>
                        </table>
                    </td>
                    </tr>
                    
                  </table>
                  
    <?php
}
?>
<?php $this->endWidget(); ?>
 