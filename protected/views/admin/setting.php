<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'ตั้งค่าเว็บไซต์';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
<table width="100%">
<tr>
<td align="center"><h3 class="underline">ตั้งค่าเว็บไซต์</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table width="50%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <th width="40%" align="right">Tel :</th>
              <td width="75%"><?php echo $form->textField($model,'tel'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">Fax :</th>
              <td width="75%"><?php echo $form->textField($model,'fax'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">Email  :</th>
              <td width="75%"><?php echo $form->textField($model,'email'); ?></td>
            </tr>
             <tr>
              <th width="40%" align="right">Support Email  :</th>
              <td width="75%"><?php echo $form->textField($model,'support_email'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">Repair Email  :</th>
              <td width="75%"><?php echo $form->textField($model,'repair_email'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">Training Email  :</th>
              <td width="75%"><?php echo $form->textField($model,'training_email'); ?></td>
            </tr>
            
             <tr>
              <th width="40%" align="right">HR Email  :</th>
              <td width="75%"><?php echo $form->textField($model,'hr_email'); ?></td>
            </tr>
             <tr>
              <th width="40%" align="right">HR Telephone  :</th>
              <td width="75%"><?php echo $form->textField($model,'hr_tel'); ?></td>
            </tr>
             <tr>
              <th width="40%" align="right">HR Fax  :</th>
              <td width="75%"><?php echo $form->textField($model,'hr_fax'); ?></td>
            </tr>
            
            
            <tr>
              <th width="40%" align="right">Facebook :</th>
              <td width="75%"><?php echo $form->textField($model,'facebook'); ?></td>
            </tr>
            
            <tr>
              <th width="40%" align="right">Line (QR Code) :</th>
              <td width="75%"><?php
			  $file = Yii::app()->request->baseUrl . '/images/logo/' . $model->line;
			  if(file_exists(Yii::app()->basePath . '/../images/logo/' .  $model->line) && $model->line != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="83" />
                  <?php
			  }
			  ?>
              <input type="file" name="line" id='line' /></td>
            </tr>
            <tr>
              <th width="40%" align="right">Facebook (QR Code) :</th>
              <td width="75%"><?php
			  $file = Yii::app()->request->baseUrl . '/images/logo/' . $model->instragram;
			  if(file_exists(Yii::app()->basePath . '/../images/logo/' .  $model->instragram) && $model->instragram != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="83" />
                  <?php
			  }
			  ?>
              <input type="file" name="instragram" id='instragram' /></td>
            </tr>
            <tr class="hide_it">
              <th width="40%" align="right">Twitter :</th>
              <td width="75%"><?php echo $form->textField($model,'twitter'); ?></td>
            </tr>
            
            
            
             <tr>
              <th width="40%" align="right">Email Url :</th>
              <td width="75%"><?php echo $form->textField($model,'email_url'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">Logo :</th>
              <td width="75%">
              <?php
			  $file = Yii::app()->request->baseUrl . '/images/logo/' . $model->logo;
			  if(file_exists(Yii::app()->basePath . '/../images/logo/' .  $model->logo) && $model->logo != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="83" />
                  <?php
			  }
			  ?>
              <input type="file" name="logo" id='logo' /></td>
            </tr>
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/branchlist';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>