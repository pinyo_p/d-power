<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/admin/css/style.css" rel="stylesheet" type="text/css" />
        <?php
        $cs = Yii::app()->clientScript;
        $cs->scriptMap = array(
            'jquery.js' => Yii::app()->request->baseUrl . '/protected/vendors/js/jquery-1.8.2.min.js',
            'jquery-ui.min.js' => Yii::app()->request->baseUrl . '/protected/vendors/js/jquery-ui-1.8.24.custom.min.js',
        );
        Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/protected/vendors/css/eggplant/jquery-ui-1.8.24.custom.css', 'screen'
        );
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        ?>
        <?php
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/Admin/login');
        }
        ?>

        <style type="text/css">
            body{
                background:none;
            }
        </style>

    </head>

    <body>
        <center>
            <table><tr><td rowspan="5" valign="top" class="main_bg">

                        <table border="0" cellpadding="5" cellspacing="3">
                            <?php if ($model->member_type == "2") { ?>
                                <tr>
                                    <th  width="200">ชื่อบริษัท,หจก.,ร้านอาหาร</th>
                                    <td bgcolor="white" width="400"><?php echo $model->company_name; ?></td>
                                </tr>
                                <tr>
                                    <th  width="200">ประเภท</th>
                                    <td bgcolor="white" width="400">
                                        <?php 
                                        switch($model->company_type){
                                            case "THA":echo "ร้านอาหารไทย";break;
                                            case "CHS":echo "ร้านอาหารจีน";break;
                                            case "JPN":echo "ร้านอาหารญี่ปุ่น";break;
                                            case "ITS":echo "ร้านอาหารอิตาเลี่ยน";break;
                                            case "FT":echo "ร้านอาหารสไตล์ฟิวชั่น";break;
                                            case "KOR":echo "ร้านอาหารเกาหลี";break;
                                            case "BG":echo "ร้านเบเกอรี่";break;
                                            case "HT":echo "โรงแรม";break;
                                            case "RS":echo "ภัตตาคาร";break;
                                            case "OT":echo "อื่นๆ";break;
                                            default:echo "ไม่ระบุ";break;
                                        } 
                                        ?>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <th  width="200">ชื่อ-สกุล</th>
                                    <td bgcolor="white" width="400"><?php echo $model->first_name . " " . $model->last_name; ?></td>
                                </tr>
                                <tr>
                                    <th  width="120">วันเกิด</th>
                                    <td bgcolor="white"><?php echo $model->birth_day . " " . $model->last_name; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <th >ที่อยู่</th>
                                <td bgcolor="white"><?php echo $model->address; ?></td>
                            </tr>

                            <tr>
                                <th >จังหวัด</th>
                                <td bgcolor="white"><?php echo $param["ProvinceName"];?></td>
                            </tr>
                            <tr>
                                <th >รหัสไปรษณีย์</th>
                                <td bgcolor="white"><?php echo $model->zipcode; ?></td>
                            </tr>
<tr>
                                <th >หมายเลขโทรศัพท์</th>
                                <td bgcolor="white"><?php echo $model->phone_no; ?></td>
                            </tr>
                                <tr>
                                <th >Email</th>
                                <td bgcolor="white"><?php echo $model->email; ?></td>
                            </tr>
                                <tr>
                                <th >LINE ID</th>
                                <td bgcolor="white"><?php echo $model->line_id; ?></td>
                            </tr>
<tr>
                                <th >หมายเลขประจำตัวผู้เสียภาษี</th>
                                <td bgcolor="white"><?php echo $model->tax_no; ?></td>
                            </tr>
                                <tr>
                                <th >วันที่สมัคร</th>
                                <td bgcolor="white"><?php echo $model->create_date; ?></td>
                            </tr>
                                <tr>
                                <th >วันที่เข้าระบบครั้งล่าสุด</th>
                                <td bgcolor="white"><?php echo $model->last_login; ?></td>
                            </tr>
                                <tr>
                                <th >จำนวนแต้มสะสม</th>
                                <td bgcolor="white"><?php echo $model->total_score; ?></td>
                            </tr>
                        </table>
                    </td></tr></table>
        </center>
        <br /><br />
    </body>
</head>