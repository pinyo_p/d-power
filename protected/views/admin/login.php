<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/admin/css/style.css" rel="stylesheet" type="text/css" />
        <script language="javascript">
            function login()
            {
                document.getElementById("login-form").submit();
            }
            function recovery_pwd(){
                var userName = $('#LoginForm_username').val().trim();
                if(userName == ''){
                    alert('คุณยังไม่ได้ระบุ Username !!!');
                    return false;
                }
                $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/recoveryuserpwd/", {
                    username: userName
                }, function (data) {
                    alert(data);
                });
            }
        </script>
    </head>

    <body>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <table width="400" border="0" align="center" cellpadding="0" cellspacing="3" class="loginbox">
            <tr>
                <td class="head">เข้าสู่ระบบ</td>
            </tr>
            <tr>
                <td><br />
                    <table width="100%" border="0" cellspacing="5" cellpadding="0">
                        <tr>
                            <td align="center">

                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'login-form',
                                    'enableClientValidation' => true,
                                    'clientOptions' => array(
                                        'validateOnSubmit' => true,
                                    ),
                                ));
                                ?>


                                <table><tr><td nowrap="nowrap">
                                            <?php echo $form->labelEx($model, 'username'); ?></td><td nowrap="nowrap">
<?php echo $form->textField($model, 'username', array('style' => 'width:100px')); ?></td><td nowrap="nowrap">
<?php echo $form->error($model, 'username'); ?></td>
                                    </tr><tr>
                                        <td nowrap="nowrap">

                                            <?php echo $form->labelEx($model, 'password'); ?></td><td nowrap="nowrap">
<?php echo $form->passwordField($model, 'password', array('style' => 'width:100px')); ?></td><td nowrap="nowrap">
<?php echo $form->error($model, 'password'); ?></td>
                                    </tr>
                                </table>
                                <br />
                                <input type='submit' style='display:none' />
                                <a href="javascript:login()" onclick="login()" class="button_login">Login</a><br /><br />
                                <div style="display: none;">
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/arrow.png" width="12" height="12" /> 
                                <a href='javascript:;'  class="link_w" onclick="recovery_pwd();">ลืมรหัสผ่าน</a>
                                </div>
<?php $this->endWidget(); ?>

                            </td>
                        </tr>

                    </table></td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />

    </body>
</html>

