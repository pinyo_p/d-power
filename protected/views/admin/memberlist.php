<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายชื่อสมาชิก';
?>
<script language="javascript">
    function changeit(obj, objId)
    {
        if ($(obj).val() != "") {
            $.post('<?php echo Yii::app()->request->baseUrl; ?>/index.php/Admin/ChangeJobbedStatus/', {
                id: objId,
                status: $(obj).val(),
            }, function (data) {
                if (data == "OK")
                {
                    location.reload();
                } else {
                    alert(data);
                }
            });
        }
    }

    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่")) {
            $.post('<?php echo Yii::app()->request->baseUrl; ?>/index.php/Admin/DeleteMember/', {id: objId}, function (data) {
                if (data == "OK")
                    location.reload();
                 else if(data == "DENIED")
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
            });
        }
    }
    function deleteitAll()
    {
        if (confirm("ต้องการลบรายการที่เลือกใช่หรือไม่")) {
            $("#act").val('deleteAll');
            $("#jobbee").submit();
        }
    }
    function export_data()
    {
        var first_name = $("#Member_first_name").val();
        var last_name = $("#Member_last_name").val();
        var email = $("#Member_email").val();
        var phone_no = $("#Member_phone_no").val();
        var start_date = $("#Member_start_date").val();
        var end_date = $("#Member_end_date").val();
        window.open('<?php echo Yii::app()->request->baseUrl; ?>/index.php/Admin/ExportMember/?first_name=' + first_name + '&last_name=' + last_name + '&email=' + email + '&phone_no=' + phone_no + '&start_date=' + start_date + '&end_date=' + end_date);
    }
    function searchit()
    {

        $("#jobbee").submit();

    }
    function gotoPage(objPage)
    {
        $("#page").val(objPage);
        $("#jobbee").submit();
    }
    function showDetail(objId)
    {
        window.open('<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/MemberDetail/' + objId, 'member_detail', 'width=600,height=400');
    }

</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'jobbee',
    'enableClientValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => false,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?> 

<input name="act" type="hidden" id='act' value="search"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

    <tr>
        <td align="center"><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td ><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td class="tabletest"> <input type="hidden" name='act' id='act' value="search" />
                        <br />
                        <table width="700" border="0" align="center" cellspacing="1" >
                            <tr>
                                <th width="120"><div align="right"><span class="h1">ชื่อ :</span></div></th>
                            <td><?php echo $form->textField($model, 'first_name'); ?></td>
                            <th width="120"><div align="right"><span class="h1">นามสกุล :</span></div></th>
                            <td><?php echo $form->textField($model, 'last_name'); ?></td>
                </tr>
                <tr>
                    <th width="120"><div align="right"><span class="h1">email :</span></div></th>
                <td><?php echo $form->textField($model, 'email'); ?></td>
                <th width="120"><div align="right"><span class="h1">เบอร์โทร :</span></div></th>
                <td><?php echo $form->textField($model, 'phone_no'); ?></td>
    </tr>
    <tr>
        <th width="120"><div align="right"><span class="h1">วันที่สมัคร :</span></div></th>
<td width="280"><?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
        'name' => 'Member[start_date]',
        'value' => $model->start_date,
        'options' => array(
            'showAnim' => 'fold',
            'dateFormat' => 'yy-mm-dd',
            'altFormat' => 'yy-mm-dd',
            'changeMonth' => 'true',
            'changeYear' => 'true',
            'showOn' => "both",
            'buttonImage' => Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",
            'buttonImageOnly' => "true",
        ),
        'htmlOptions' => array(
            'style' => 'height:20px;'
        ),
            )
    );
    ?></td>
<th width="120"><div align="right"><span class="h1">ถีง :</span></div></th>
<td width="280"><?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
        'name' => 'Member[end_date]',
        'value' => $model->end_date,
        'options' => array(
            'showAnim' => 'fold',
            'dateFormat' => 'yy-mm-dd',
            'altFormat' => 'yy-mm-dd',
            'changeMonth' => 'true',
            'changeYear' => 'true',
            'showOn' => "both",
            'buttonImage' => Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",
            'buttonImageOnly' => "true",
        ),
        'htmlOptions' => array(
            'style' => 'height:20px;'
        ),
            )
    );
    ?></td>
</tr>

<tr>
    <td width="120" colspan="4" align="center">
        <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" alt="Submit button">
        <a href="javascript:export_data();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_export.png" alt="Export"></a>

    </td>
</tr>

</table>

</td>
</tr>
<tr>
    <td class="tabletest">

<center>
    <br /><br />
    <table width="100%" border="0" cellspacing="3" cellpadding="3" style="width:99%">
        <tr>
            <th width="36" align="center" valign="top" class="">เลือก</th>
            <th width="37" align="center" valign="top" class="txt_bold">ลำดับ</th>
            <th width="100" align="center" valign="top" class="txt_bold">ชื่อ</th>
<!--            <th width="85" align="center" valign="top" class="txt_bold">วันเกิด</th>-->
            <th width="92" align="center" valign="top" class="txt_bold">เบอร์โทร</th>
            <th width="115" align="center" valign="top" class="txt_bold">Email</th>
<!--            <th width="135" align="center" valign="top" class="txt_bold">ที่อยู่</th>-->
<!--            <th width="115" align="center" valign="top" class="txt_bold">วันที่สมัคร</th>-->
             <?php
                    if ($this->checkUserPermission('member', '5')){                
                    ?>
            <th>Line ID</th>
            <th>เลขประจำตัวผู้เสียภาษี</th>
            <th width="115" align="center" valign="top" class="txt_bold">แสดงรหัส</th>
                    <?php } ?>
            <th width="19" align="center" valign="top" class="txt_bold">ลบ</th>
        </tr>


        <?php
        $i = 1;
        foreach ($data as $row) {
            ?>
            <tr>
                <td class="rowa" width="36" align="center" valign="top">
                    <input type="checkbox" name="jobbee_id[]" id="checkbox" value="<?php echo $row->id; ?>" /></td>
                <td width="37" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i++; ?></span></td>

                <td class="rowa" width="160" align="center" valign="top">
                    <a href='javascript:showDetail("<?php echo $row->id; ?>")'><?php echo $row->first_name . ' ' . $row->last_name; ?> </a>
                    
                </td>
<!--                <td class="rowa" width="51" align="center" valign="top"><?php echo $row->birth_day; ?></td>-->
                <td class="rowa" width="78" align="center" valign="top"><?php echo $row->phone_no; ?></td>
                <td class="rowa" width="77" align="center" valign="top"><?php echo $row->email; ?></td>
<!--                <td class="rowa" width="135" align="center" valign="top"><?php echo $row->address . ' ' . $row->p['thai_name']; ?></td>-->
<!--                <td class="rowa" width="115" align="center" valign="top"><?php echo $row->create_date; ?></td>-->
                <td><?php echo $row->line_id; ?></td>
                <td><?php echo $row->tax_no; ?></td>
                 <?php
                    if ($this->checkUserPermission('member', '5')){                
                    ?>
                <td class="rowa" width="19" align="center" valign="top"><a href='javascript:alert("<?php echo $row->password; ?>")'>แสดงรหัส</a></td>
                    <?php } ?>
                <td class="rowa" width="19" align="center" valign="top"><a href='javascript:deleteit("<?php echo $row->id; ?>")'><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" border="0" /></a></td>
            </tr>
            <?php
        }
        ?>
    </table>

    <?php
    if ($param['max_page'] > 1) {
        ?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                <td style="border:none;"><div class="nav_page">
                        <ul>
                            <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
                            <?php
                            $start_page = ($page > 2 ? $page - 2 : 1);
                            $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
                            if ($end_page <= 5)
                                $end_page = 5;
                            else if (($end_page - $page) < 5)
                                $start_page = ($end_page - 4);
                            if ($end_page > $param['max_page'])
                                $end_page = $param['max_page'];
                            for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                                $class = "";
                                if (($i + 1) == $page)
                                    $class = '  class="navselect"';
                                else
                                    $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                                ?>
                                <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                <?php
                            }
                            ?>
                            <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                        </ul>
                    </div></td>
                <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                    <table width="200"  style="border:none;"><tr><td style="border:none;">
                                <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png"  height="22" /></a></td></tr></table>


                </td>
            </tr>
        </table>
        <?php
    }
    ?>
</center>
</td>
</tr>
<tr>
    <td class="nparesult_table_content" align="center"><br />
        &nbsp;<a href="javascript:deleteitAll()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a> </td>
</tr>
</table>

</td>
</tr>
</table>


<?php $this->endWidget(); ?>