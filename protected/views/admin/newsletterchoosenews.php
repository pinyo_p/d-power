
<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'เลือกข่าวสารสำหรับส่ง';
?>
<script language="javascript">
    $(document).ready(function () {
        $('#btnBack').click(function(){
           location.href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/SendNewsList";
        });
        $('#btnSendNews').click(function () {
            if (confirm("ต้องการส่งข่าวสารใช่หรือไม่?"))
            {
                $("#act").val("sendnews");
                $("#contentlist-form").submit();
                $("#act").val("search");
            }
        });
    });

    function searchit()
    {
        $("#contentlist-form").submit();
    }

    function gotoPage(objPage)
    {
        $("#page").val(objPage);
        $("#contentlist-form").submit();
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'contentlist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4">ค้นหาข่าวสาร & โปรโมชั่น</span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="right">ชื่อเรื่อง :</th>
                    <td>
                        <input type="text" id="txtTitle" name="txtTitle" style="width:90%;" value="<?php echo (isset($_POST["txtTitle"])) ? $_POST["txtTitle"] : ""; ?>" />
                    </td>
                    <th align="right">เนื้อหา :</th>
                    <td >
                        <input type="text" id="txtTitle" name="txtContent" style="width:90%;" value="<?php echo (isset($_POST["txtContent"])) ? $_POST["txtContent"] : ""; ?>" />
                    </td>
                </tr>
                <tr>
                    <th align="right"></th>
                    <td >
<!--                        <select id="ddlStatus" name="ddlStatus">
                            <option value="all">-- ทั้งหมด --</option>
                            <option value="1" 
                        <?php
                        if (isset($_POST["ddlStatus"])) {
                            if ($_POST["ddlStatus"] == '1')
                                echo "selected='selected'";
                        }
                        ?>>ใช้งาน</option>
                            <option value="0" 
                        <?php
                        if (isset($_POST["ddlStatus"])) {
                            if ($_POST["ddlStatus"] == '0')
                                echo "selected='selected'";
                        }
                        ?>>ไม่ใช้งาน</option>
                        </select>-->
                    </td>
                    <th align="right"></th>
                    <td align="center">
                        <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" width="55" height="26" />
                    </td>
                </tr>
            </table>



        </td>
    </tr>
    <tr>
        <td align="center"> <br /><br /><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>

    <tr>
        <td align="center" width="100%" class="tabletest">

            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">เลือก</th>
                    <th width="120">รูปภาพ</th>
                    <th>ชื่อเรื่อง</th>
                    <th>เนื้อหาย่อ</th>
                </tr>
                <?php
                foreach ($data as $row) {
                    ?>
                    <tr>
                        <td align="center">
                            <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' /></td>

                        <td align="left">
                            <?php
                            $file = Yii::app()->request->baseUrl . '/images/content/' . $row->attr1;
                            if (file_exists(Yii::app()->basePath . '/../images/content/' . $row->attr1) && $row->attr1 != "") {
                                ?>
                                <img src="<?php echo $file; ?>" width="118" />
                                <?php
                            } else {
                                ?>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
                                <?php
                            }
                            ?>
                        </td>
                        <td align="left">
                            <?php echo $row->title_2; ?>
                        </td>
                        <td valign="middle">
                            <?php echo iconv_substr(trim(strip_tags($row->short_content_2)), 0, 100, "UTF-8") . "..."; ?>
                        </td>


                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
            if ($param['max_page'] > 1) {
                ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                        <td style="border:none;"><div class="nav_page">
                                <ul>
                                    <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
                                    <?php
                                    $start_page = ($page > 2 ? $page - 2 : 1);
                                    $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
                                    if ($end_page <= 5)
                                        $end_page = 5;
                                    else if (($end_page - $page) < 5)
                                        $start_page = ($end_page - 4);
                                    if ($end_page > $param['max_page'])
                                        $end_page = $param['max_page'];
                                    for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                                        $class = "";
                                        if (($i + 1) == $page)
                                            $class = '  class="navselect"';
                                        else
                                            $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                                        ?>
                                        <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                        <?php
                                    }
                                    ?>
                                    <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                                </ul>
                            </div></td>
                        <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                            <table width="200"  style="border:none;"><tr><td style="border:none;">
                                        <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                        <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                        <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png"  height="22" /></a></td></tr></table>


                        </td>
                    </tr>
                </table>
                <?php
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="center">



            <br />
            <br />
            <br />

            <button id="btnSendNews" onclick="return false;">ส่งข่าวสาร</button>&nbsp;&nbsp;
            <button id="btnBack" onclick="return false;">ย้อนกลับ</button>
        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>