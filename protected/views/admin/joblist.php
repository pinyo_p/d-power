
<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'ตำแหน่งงาน';
?>
<script language="javascript">
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteJob/",{
									id:objId
									},function(data){
										if(data=="OK")
											location.href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/JobList';
										else
											alert(data);
									});
	}
}
function deleteAll()
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$("#banklist-form").submit();
	}
}
</script>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'banklist-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <input type="hidden" name='act' value="deleteall" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center"><br />
<span class="text4"><?php echo $this->breadcrumbs;?></span><br />
&nbsp;<br /></td>
      </tr>
      <tr>
        <td align="center" width="100%" class="tabletest">
          <table border="0" cellpadding="0" cellspacing="1">
            <tr>
              <th width="5%" align="center" valign="top" >ลำดับ</th>
                  <th width="10%" align="center" valign="top">ตำแหน่ง</th>
                  <th width="15%" align="center" valign="top">การศึกษา</th>
                  <th align="center" valign="top">ความสามารถ</th>
                  <th width="5%" align="center" valign="top" >ประสบการณ์</th>
                  <th width="5%" align="center" valign="top" >ประเภท</th>
                  <th width="20%" align="center" valign="top">รายละเอียดงาน</th>
                  <th width="5%" align="center" valign="top" >แก้ไข</th>
                  <th width="5%" align="center" valign="top" >ลบ</th>
            </tr>
            <?php 
			$i=0;
			foreach($data as $row){
			?>
            <tr>
               <td width="5%" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo ++$i;?></span></td>
                  <td width="10%" align="center" valign="top" class="rowa"> <?php echo $row->position;?> </td>
                  <td width="15%" align="center" valign="top" class="rowa"> <?php echo $row->education;?></td>
                  <td align="left" valign="top" class="rowa"><?php echo Yii::short_text($row->skill,100);?></td>
                  <td width="5%" align="center" valign="top" class="rowa"><?php echo $row->experience;?></td>
                  <td width="5%" align="center" valign="top" class="rowa"> <?php echo $row->type;?> </td>
                  <td width="20%" align="left" valign="top" class="rowa"><?php echo Yii::short_text($row->jobdetail,100);?></td>
                  <td class="rowa" width="5%" align="center" valign="top"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Admin/Job/<?php echo $row->id;?>">แก้ไข</a></td>
                  <td class="rowa" width="5%" align="center" valign="top"><a href="javascript:deleteit('<?php echo $row->id;?>')">ลบ</a></td>
            </tr>
           <?php
           }
		   ?>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br />
          <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Job/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;</td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br /></td>
      </tr>
      
    </table>
            <?php $this->endWidget(); ?>