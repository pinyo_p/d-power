<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'Banner Slide';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
<table width="100%">
<tr>
<td align="center"><h3 class="underline"><?php echo (isset($_GET["id"]))?"แก้ไข":"เพิ่ม"; ?>ข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table width="50%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <th width="40%" align="right">Sort Order :</th>
              <td width="75%"><?php echo $form->textField($model,'sort_order',array('style'=>'width:50px')); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">รูป Banner Slide :</th>
              <td width="75%">
			<?php
			  $file = Yii::app()->request->baseUrl . '/images/banner_intro/'.$model->id.'/' . $model->img_src;
			  if(file_exists(Yii::app()->basePath . '/../images/banner_intro/'.$model->id.'/' .  $model->img_src) && $model->img_src != ""){
				  ?><br />
                   <img src="<?php echo $file; ?>" width="280" />
                  <?php
			  }
			  ?><br /> 
              <input type="file" name="pic" id='pic' />
              </td>
            </tr>
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/bannerslidelist';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>