<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'หมวดหมู่สินค้า';
?>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteProductGroup/", {
                id: objId
            }, function (data) {
                if (data.indexOf("OK") > -1)
                    location.href = '<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ProductGroupList';
                else if(data.indexOf("DENIED") > -1)
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
                else
                    alert(data);
            });
        }
    }
    function deleteAll()
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $("#act").val("deleteall");
            $("#productgrouplist-form").submit();
        }
    }
    function searchit()
    {
        $("#act").val("search");
        $("#productgrouplist-form").submit();

    }
    function gotoPage(objPage)
    {
        $("#act").val("search");
        $("#page").val(objPage);
        $("#productgrouplist-form").submit();
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'productgrouplist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>
    <tr  class='hide_it'>
        <td align="center"><br />
            <span class="text4">ค้นหา หมวดหมู่สินค้า</span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table class='hide_it'  border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <th align="right">หมวดหมู่สินค้า :</th>
                    <td ><?php echo $form->dropDownList($model, 'brand_id', CHtml::listData(Brand::model()->findAll(array('order' => 'name_th ASC')), 'id', 'name_th'), array('empty' => 'เลือก Group',
));
?></td>
                    <td  colspan="2" align="center"><input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" width="55" height="26" /></td>
                </tr>
            </table>
            <br /><br />
            <br />

        </td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="15%">เลือก</th>
                    <th >Logo</th>
                    <th class='hide_it'>Brand</th>
                    <th >หมวดหมู่</th>
                    <th>ภายใต้หมวด</th>
                    <th width="15%">แก้ไข</th>
                    <th width="15%">ลบ</th>
                </tr>
                <?php
                foreach ($data as $row) {
                    ?>
                    <tr>
                        <td width="15%" align="center">
                            <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' /></td>
                        <td align="center">
                            <?php
                            $file = Yii::app()->request->baseUrl . '/images/group/' . $row->logo;
                            if (file_exists(Yii::app()->basePath . '/../images/group/' . $row->logo) && $row->logo != "") {
                                ?>
                                <img src="<?php echo $file; ?>" height="83" />
                                <?php
                            } else {
                                ?>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
                                <?php
                            }
                            ?>
                        </td>
                        <td align="center"  class='hide_it'>
                            <?php echo $row->brand['name_th']; ?>
                        </td>
                        <td align="center">
                            <?php echo $row->group_2; ?> / <?php echo $row->group_1; ?>
                        </td>
                        <td align="center"><?php echo ($row->parent_product_group != null)?$row->parent_product_group->group_1:"-"; ?></td>
                        <td width="15%" align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ProductGroup/<?php echo $row->id; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
                        <td width="15%" align="center">
                            <a href='javascript:deleteit("<?php echo $row->id; ?>");'>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>

            <?php
            if ($param['max_page'] > 1) {
                ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                        <td style="border:none;"><div class="nav_page">
                                <ul>
                                    <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
                                    <?php
                                    $start_page = ($page > 2 ? $page - 2 : 1);
                                    $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
                                    if ($end_page <= 5)
                                        $end_page = 5;
                                    else if (($end_page - $page) < 5)
                                        $start_page = ($end_page - 4);
                                    if ($end_page > $param['max_page'])
                                        $end_page = $param['max_page'];
                                    for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                                        $class = "";
                                        if (($i + 1) == $page)
                                            $class = '  class="navselect"';
                                        else
                                            $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                                        ?>
                                        <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                        <?php
                                    }
                                    ?>
                                    <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                                </ul>
                            </div></td>
                        <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                            <table width="200"  style="border:none;"><tr><td style="border:none;">
                                        <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                        <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                        <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png" height="22" /></a></td></tr></table>


                        </td>
                    </tr>
                </table>
    <?php
}
?>

        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br />
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ProductGroup/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>