<?php 
$this->pageTitle="Admin Panel::ความต้องการฝากขายทรัพย์สิน"; 
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteWant/');?>',{id:objId},function(data){
																							  location.reload();
																							   });
	}
}
</script>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt;<a href="#" class="link_green"> NPA</a> &gt; <a href="#" class="link_green">ความต้องการฝากขายทรัพย์สิน</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ความต้องการฝากขายทรัพย์สิน</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td  class="nparesult_table_content">
              <center>
              <br />
              <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_search',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input type="hidden" name="act" id="act" value="" />
<table>
<tr><td align="right"><div class="txt_bold">
ชื่อ : </div></td><td><?php echo $form->textField($model,'first_name'); ?></td><td align="right"><div class="txt_bold">นามสกุล</div></td><td><?php echo $form->textField($model,'last_name'); ?></td>

</tr><tr>
<td align="right"><div class="txt_bold">เบอร์โทรศัพท์ : </div></td>
<td><?php echo $form->textField($model,'phone_no'); ?></td>
<td align="right"><div class="txt_bold">อีเมลล์ : </div></td>
<td><?php echo $form->textField($model,'email'); ?></td>
</tr><tr>
<td align="right"><div class="txt_bold">ราคา : </div></td>
<td><?php echo $form->textField($model,'price'); ?></td>
<td align="right"><div class="txt_bold">ประเภท : </div></td>
<td><?php echo $form->dropDownList($model,'product_type', CHtml::listData(ProductGroup::model()->findAll(array('order' => 'product_group ASC')), 'id', 'product_group'), array('empty'=>'ทุกประเภท',																																												  				 
	  )); ?></td>
</tr>
<tr>
<td align="right"><div class="txt_bold">จังหวัด : </div></td>
<td><?php echo $form->dropDownList($model,'province', CHtml::listData(Provinces::model()->findAll(array('order' => 'thai_name ASC')), 'province_id', 'thai_name'), array('empty'=>'Select Province',																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getdistrict/' ,
      			'success'=>'js:function(html){jQuery("#Want_district").html(html);jQuery("#Want_tambol").html("<option value=0>Select District First</option>");}',),
	  )); ?></td>
      <td align="right"><div class="txt_bold">อำเภอ : </div></td><td><?php echo $form->dropDownList($model,'district', $param['district'], array('empty'=>'Select Province First',
																																											   																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/GetWantTambol/' ,
      			'success'=>'js:function(html){jQuery("#Want_tambol").html(html);}',),)); ?></td>
</tr>
<tr>
<td align="right"><div class="txt_bold">ตำบล : </div></td><td><?php echo $form->dropDownList($model,'tambol',$param['tambol'], array('empty'=>'Select District First')); ?></td><td align="right"><div class="txt_bold">ถนน : </div></td><td><?php echo $form->textField($model,'road'); ?></td>
</tr>

<tr>
<td align="right"><div class="txt_bold">วันที่ฝาก ตั้งแต่ : </div></td>
<td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
                    'name'=>'Want[start_date]',
					'value'=>$model->start_date,
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'width:70px'
    ),)); ?>*ปปปป-ดด-วว</td>
<td align="right"><div class="txt_bold">ถึง : </div></td>
<td>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
                    'name'=>'Want[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'width:70px'
    ),)); ?>*ปปปป-ดด-วว
</td>
</tr>

<tr><td align="right"><div class="txt_bold">สถานะ : </div></td>

<td>
 <?php echo $form->dropDownList($model,'status',array(0=>'ฝากความต้องการใหม่',1=>'ส่งข้อมูลแล้ว'), array('empty'=>'ทุกสถานะ',																																												  				 
	  )); 
						  ?>
</td>
<td></td><td> <a href="javascript:$('#act').val('search');$('#frm_search').submit();" ><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/npa_search.png" alt="" width="62" height="25" /></a></td>
</tr>
</table>
<?php $this->endWidget(); ?>
<br /><br />
</center>
              
               <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_search',
	'enableClientValidation'=>true,
	'action' => Yii::app()->createUrl('/admin/sendwant/'),
	 
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
              
              <table width="100%" border="0" cellpadding="0" cellspacing="2">
              <tr class="nparesult_table_header" style="padding:5px">
              <td colspan="6" align="center" style="padding:5px">ข้อมูลลูกค้า</td>
              <td colspan="8" align="center" style="padding:5px">ความต้องการ</td>
              </tr>
                <tr  class="nparesult_table_header">
                  <td width="5%" align="center" valign="top" class="rowa">เลือก</td>
                  <td width="5%" align="center" valign="top" class="rowa">ลำดับ</td>
                  <td align="center" valign="top" class="rowa">ชื่อ</td>
                  <td align="center" valign="top" class="rowa">นามสกุล</td>
                  <td align="center" valign="top" class="rowa">เบอร์โทรศัพท์</td>
                  <td align="center" valign="top" class="rowa">อีเมลล์</td>
                   <td align="center" valign="top" class="rowa">ราคา</td>
                   <td align="center" valign="top" class="rowa">ประเภท</td>
                  <td align="center" valign="top" class="rowa">จังหวัด</td>
                  <td align="center" valign="top" class="rowa">อำเภอ</td>
                  <td align="center" valign="top" class="rowa">ตำบล</td>
                  <td align="center" valign="top" class="rowa">ถนน</td>
                  <td width="5%" align="center" valign="top" class="rowa">สถานะ</td>
                  <td align="center">วันที่ฝาก</td>
                </tr>
                <?php
				$i =1;
				foreach($data as $row){
				?>
                <tr>
                  <td class="rowa" width="5%" align="center" valign="top"><input type="checkbox" name="want_id[]" value="<?php echo $row->id;?>" id="checkbox" /></td>
                  <td width="5%" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i++;?></span></td>
                  <td align="center" valign="top" class="rowa"><?php echo $row->first_name;?></td>
                  <td align="center" valign="top" class="rowa"><?php echo $row->last_name;?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->phone_no;?></td>
                  <td align="center" valign="top" class="rowa"><?php echo $row->email;?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->start_price . ' - ' . $row->end_price;?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->category['product_group'];?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->p['thai_name'];?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->d['thai_name'];?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->ta['thai_name'];?></td>
                  <td align="left" valign="top" class="rowa"><?php echo $row->road;?></td>
                  <td class="rowa" width="5%" align="center" valign="top">
                  <?php
				  if($row->status=="0")
				  	echo "ใหม่";
					else
					echo "<a href='" . Yii::app()->request->baseUrl . "/index.php/admin/showmatch/" . $row->id . "'>ส่งแล้ว</a>";
				  
				  ?>
                  </td>
                  <td nowrap="nowrap" class="rowa">
                  <?php echo $row->create_date;?>
                  </td>
                </tr>
                <?php
				}
				?>
              </table>
              <center>
              <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/go.png" />
              <?php $this->endWidget(); ?>
              </center>
              </td>
            </tr>
           
          </table>
        