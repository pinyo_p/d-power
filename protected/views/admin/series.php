<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'Series';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล <span class="errorMessage">* รายการที่เพิ่มจะไม่แสดงจนกว่าจะมีรายการสินค้าในกลุ่มนี้</span></h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table  border="0" cellspacing="0" cellpadding="0">
          
          <tr>
              <th width="200" align="right">Brand :</th>
              <td ><?php echo $form->dropDownList($model,'brand_id', CHtml::listData(Brand::model()->findAll(array('order' => 'name_th ASC')), 'id', 'name_th'), array('empty'=>'เลือก Brand',	'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getgroup/' ,
      			'success'=>'js:function(html){jQuery("#Series_group_id").html(html);}',),																																										  				 
	  )); ?></td>
            </tr>
          
          <tr>
              <th width="200" align="right">Group :</th>
              <td > <?php echo $form->dropDownList($model,'group_id',$param['group'], array('empty'=>'ทุก Group',																																												  				 
	  )); ?></td>
            </tr>
            <tr>
              <th align="right">Series (ภาษาไทย) :</th>
              <td ><?php echo $form->textField($model,'name_th'); ?></td>
            </tr>
            <tr>
              <th align="right">Series (ภาษาอังกฤษ) :</th>
              <td><?php echo $form->textField($model,'name_en'); ?></td>
            </tr>
            <tr>
              <th width="200" align="right">จัดเรียง :</th>
              <td > <?php echo $form->textField($model,'sort_order'); ?></td>
            </tr>
            
               
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/serieslist';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>