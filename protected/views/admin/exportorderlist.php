<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            .head1{
                font-size:18px;font-weight:bold;
            }
            .head2{
                font-size:16px;font-weight:bold;
            }
            .txtright{text-align:right;}
            .txtcenter{text-align:center;}
            .txtbold{font-weight: bold;}
            table tr th{font-weight: bold;}
        </style>
    </head>
    <body>
        <div class="head1">ข้อมูลสรุปยอดสั่งซื้อสินค้า</div>
        <div class="head2">ตั้งแต่วันที่ : <?php echo $date_period; ?></div>
        <div class="head2">สถานะ : <?php echo $statusname; ?></div>
        <br/>
        <table border="1">
            <tr style="background:#ddd;">
                <th class="txtcenter" style="width:100px;">วันที่</th>
                <th class="txtcenter" style="width:100px;">รหัสสั่งซื้อ</th>
                <th class="" style="width:300px;">รายการสินค้า</th>
                <th class="txtcenter" style="width:100px;">จำนวน</th>
                <th style="min-width:180px;">ชื่อลูกค้า</th>
                <th style="width:300px;">ที่อยู่ที่จัดส่งสินค้า</th>
                <th class="txtcenter">สถานะ</th>
                <th class="txtright" style="width:100px;">ยอดซื้อสินค้า</th>
                <th class="txtright" style="width:100px;">ค่าขนส่ง</th>
                <th class="txtright" style="width:100px;">รวมทั้งสิ้น</th>                              
            </tr>
            <?php
            if (count($mOrder) > 0) {
                $totalBuy = 0;
                $totalShipping = 0;
                $totalNet = 0;
                foreach ($mOrder as $rOrder) {
                    ?>
                    <tr>
                        <td class="txtcenter"><?php echo substr($rOrder["create_date"], 0, 10); ?></td>
                        <td class="txtcenter"><?php echo substr("000000000" . $rOrder["id"], -9); ?></td>
                        <td>
                            <?php
                            $order_id = $rOrder["id"];
                            $modelCart = Cart::model()->findAll('order_id=:order_id',array(':order_id' => $order_id));
                            $rowNo = 1;
                            foreach($modelCart as $rowCart){
                                $modelProduct = Product::model()->findByPk($rowCart["product_id"]);
                                if(count($modelProduct) > 0){
                                    echo "<div>".$rowNo.") ".$modelProduct->product_name_2."&nbsp;&nbsp;&nbsp;</div>";
                                    $rowNo++;
                                }
                            }
                            ?>
                        </td>
                        <td class="txtcenter">
                            <?php
                            $order_id = $rOrder["id"];
                            $modelCart = Cart::model()->findAll('order_id=:order_id',array(':order_id' => $order_id));

                            foreach($modelCart as $rowCart){
                                echo "<div>".$rowCart->amount."</div>";
                            }
                            ?>
                        </td>
                        <td><?php echo $rOrder["fullname"] . $rOrder["company_name"]; ?></td>
                        <td><?php echo $rOrder["address"]; ?></td>
                        <td class="txtcenter">
                            <?php
                            $statusName = "";
                            switch ($rOrder["status"]) {
                                case "4":$statusName = "รอการจัดส่ง";
                                    break;
                                case "5":$statusName = "จัดส่งแล้ว";
                                    break;
                                case "6":$statusName = "จัดส่งไม่สำเร็จ";
                                    break;
                                case "7":$statusName = "ยกเลิกการจัดส่ง";
                                    break;
                            }
                            echo $statusName;
                            ?>
                        </td>
                        <td class="txtright">
                            <?php
                            $total = Yii::app()->db->createCommand("select sum(amount*price) from tb_cart where status='1' and order_id='" . $rOrder["id"] . "' ")->queryScalar();
                            $totalBuy += $total;
                            echo number_format($total, 0, ".", ",");
                            ?>
                        </td>
                        <td class="txtright">
                            <?php
                            $shipping = Yii::app()->db->createCommand("select shipping_fee from tb_cart_info where order_id='" . $rOrder["id"] . "' ")->queryScalar();
                            $totalShipping += $shipping;
                            echo number_format($shipping, 0, ".", ",");
                            ?>
                        </td>
                        <td class="txtright">
                            <?php
                            echo number_format($total + $shipping, 0, ".", ",");
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td colspan="7" class="txtcenter" style="font-weight: bold;">รวมทั้งสิ้น</td>
                    <td class="txtright" style="font-weight: bold;"><?php echo number_format($totalBuy, 0, ".", ",");?></td>
                    <td class="txtright" style="font-weight: bold;"><?php echo number_format($totalShipping, 0, ".", ",");?></td>
                    <td class="txtright" style="font-weight: bold;"><?php echo number_format($totalBuy+$totalShipping, 0, ".", ",");?></td>
                </tr>
                <?php
            } else {
                ?>
                <tr><td colspan="8" class="txtcenter"><i>-- No result found --</i></td></tr>
                <?php
            }
            ?>

        </table>
    </body>
</html>
