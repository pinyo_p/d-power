<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'คู่มือสินค้า';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <script type="text/javascript" language="javascript">
	$(function() {
		$( "#tabs" ).tabs();
		CKEDITOR.replace('Content_content_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Content_content_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
	});
	
	</script>
    
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <th width="40%" align="right">ชื่อไฟล์(ภาษาไทย) :</th>
              <td width="75%"><?php echo $form->textField($model,'filename_th'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">ชื่อไฟล์(ภาษาอังกฤษ) :</th>
              <td width="75%"><?php echo $form->textField($model,'filename_en'); ?></td>
            </tr>
             
            
            <tr>
              <th width="40%" align="right">Cover :</th>
              <td width="75%">
			<?php
			  $file = Yii::app()->request->baseUrl . '/download/cover_' . $model->cover;
			  if(file_exists(Yii::app()->basePath . '/../download/cover_' .  $model->cover) && $model->cover != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="83" /><br /> 
                  <?php
			  }
			  ?>
              <input type="file" name="cover" id='cover' />
              </td>
            </tr>
            
            <tr>
              <th width="40%" align="right">File :</th>
              <td width="75%">
	 <?php
              $file = Yii::app()->request->baseUrl . '/download/file_' . $model->filesrc;
			  if(file_exists(Yii::app()->basePath . '/../download/file_' .  $model->filesrc) && $model->filesrc != ""){
				  ?>
                   <a href="<?php echo $file; ?>" target='_blank'>Download</a><br />
                  <?php
			  }
			  ?>
              <input type="file" name="file" id='file' />
              </td>
            </tr>
            
            
            
            
            
            
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/ManualList';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>