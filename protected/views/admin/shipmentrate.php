<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'ข้อมูลอัตราค่าขนส่ง';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
<table width="100%">
<tr>
<td align="center"><h3 class="underline"><?php echo (isset($_GET["id"]))?"แก้ไข":"เพิ่ม"; ?>ข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table width="50%" border="0" cellspacing="0" cellpadding="0">
              <tr>
              <th width="40%" align="right">ประเภท :</th>
              <td width="75%">
                  <?php
                    switch ($_GET["shiptype"]){
                        case "in_reg":
                            echo "ลงทะเบียน (ในประเทศ)";
                            break;
                        case "in_ems":
                            echo "EMS (ในประเทศ)";
                            break;
                        case "in_gen":
                            echo "ธรรมดา (ในประเทศ)";
                            break;
                        
                    }
                  ?>
              </td>
            </tr>
            <tr>
              <th width="40%" align="right">จากน้ำหนัก (กรัม) :</th>
              <td width="75%"><?php echo $form->textField($model,'weight_from',array('style'=>'width:250px')); ?>
              <?php echo $form->error($model, 'weight_from'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">ถึงน้ำหนัก (กรัม) :</th>
              <td width="75%"><?php echo $form->textField($model,'weight_to',array('style'=>'width:250px')); ?>
              <?php echo $form->error($model, 'weight_tos'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">อัตราค่าขนส่ง :</th>
              <td width="75%"><?php echo $form->textField($model,'cost_price',array('style'=>'width:250px')); ?>
              <?php echo $form->error($model, 'cost_price'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">จัดเรียง :</th>
              <td width="75%"><?php echo $form->textField($model,'sort_order',array('style'=>'width:100px')); ?>
              <?php echo $form->error($model, 'sort_order'); ?></td>
            </tr>
            
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/shipmentratelist?shiptype='.$_GET["shiptype"];?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>