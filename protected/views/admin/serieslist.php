<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'จัดการข้อมูล Series';
?>
<script language="javascript">
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteSeries/",{
									id:objId
									},function(data){
										if(data=="OK")
											location.href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/SeriesList';
										else
											alert(data);
									});
	}
}
function deleteAll()
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$("#act").val("deleteall");
		$("#banklist-form").submit();
		$("#act").val("search");
	}
}
function searchit()
{

		$("#banklist-form").submit();

}
</script>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'banklist-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
    <input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
        <td align="center"><br />
<span class="text4">ค้นหา Series</span><br />
&nbsp;<br /></td>
      </tr>
       <tr>
        <td align="center" width="100%" class="tabletest">
         <table  border="0" cellspacing="0" cellpadding="0">
            
            <tr>
            <th align="right">Brand :</th>
              <td >
			  
			  
			  <?php echo $form->dropDownList($model,'brand_id', CHtml::listData(Brand::model()->findAll(array('order' => 'name_th ASC')), 'id', 'name_th'), array('empty'=>'เลือก Brand',	'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getgroup/' ,
      			'success'=>'js:function(html){jQuery("#Series_group_id").html(html);}',),																																										  				 
	  )); ?></td>
            
            
            
              <th align="right">หมวดหมู่สินค้า :</th>
              <td >
			  
			  
			  <?php echo $form->dropDownList($model,'group_id',array(), array('empty'=>'ทุก Group',																																												  				 
	  )); ?></td>
              <td  colspan="2" align="center"><input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" width="55" height="26" /></td>
            </tr>
           </table>
        </td>
        </tr>
      <tr>
        <td align="center"><br />
<span class="text4">รายชื่อ Series</span><br />
&nbsp;<br /></td>
      </tr>
      <tr>
        <td align="center" width="100%" class="tabletest">
          <table border="0" cellpadding="0" cellspacing="1">
            <tr>
              <th width="15%">เลือก</th>
              <th >ชื่อ (ภาษาไทย)</th>
              <th >ชื่อ (ภาษาอังกฤษ)</th>
              <th>หมวดหมู่</th>
              <th width="15%">แก้ไข</th>
              <th width="15%">ลบ</th>
            </tr>
            <?php 
			foreach($data as $row){
			?>
            <tr>
              <td width="15%" align="center">
              <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id;?>' /></td>
              <td align="center">
              <?php echo $row->name_th; ?>
              </td>
              <td align="center">
              <?php echo $row->name_en; ?>
              </td>
              <td align="center">
              <?php echo $row->group['group_th']; ?>
              </td>
             
              
              <td width="15%" align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Series/<?php echo $row->id;?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
              <td width="15%" align="center">
              <a href='javascript:deleteit("<?php echo $row->id;?>");'>
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
            </tr>
           <?php
           }
		   ?>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br />
          <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Series/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br /></td>
      </tr>
      
    </table>
            <?php $this->endWidget(); ?>