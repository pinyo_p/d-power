<?php
$i = 0;
foreach($data as $row)
{
	$arr_book[$i] = $row;
	$i++;
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="page_header">&#8226; วารสาร</td>
  </tr>
  <tr>
    <td valign="top" class="page_content"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="300" align="center" valign="top"><p><img src="<?php echo Yii::app()->request->baseUrl;?>/download/COP/<?php echo $arr_book[0]->image_cover;?>" width="198" height="252" /></p>
          <p><?php echo $arr_book[0]->filename;?></p></td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="txt_pink txt_bold">เรื่องเด่นประจำฉบับ</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><p><?php echo $arr_book[0]->filedesc;?></p></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href='<?php echo Yii::app()->request->baseUrl;?>/download/COP/<?php echo $row->filesrc;?>' target='_blank'><img src="<?php echo Yii::app()->request->baseUrl;?>/images/more.jpg" width="77" height="24" /></a></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="page_header "><span class="txt_pink">ฉบับย้อนหลัง</span></td>
  </tr>
  <tr>
    <td valign="top" class="page_content"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <?php
	  for($ii=1;$ii<count($arr_book);$ii++){
		  ?>
          <td width="33%" align="center"><a href='<?php echo Yii::app()->request->baseUrl;?>/download/COP/<?php echo $arr_book[$ii]->filesrc;?>' target='_blank'><p><img src="<?php echo Yii::app()->request->baseUrl;?>/download/COP/<?php echo $arr_book[$ii]->image_cover;?>"  height="172" /></p>
          <p><?php echo $arr_book[$ii]->filename;?></p></a></td>
          <?php
		  if($ii % 3 == 0)
		  {
			  echo "</tr><tr>";
		  }
	  }
	  ?>
        
      </tr>
      
    </table></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr style="visibility:hidden;position:absolute;">
    <td valign="top" class="page_content"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="160" class="txt_bold"><span class="txt_pink ">หน้าที่ :</span> 1 / 8</td>
        <td><div class="nav_page">
          <ul>
            <li>&laquo;</li>
            <li class="navselect">1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
            <li>&raquo;</li>
          </ul>
        </div></td>
        <td width="160" align="right" class="txt_bold"><span class="txt_pink">ไปหน้าที่ :</span>
          <input name="textfield5" type="text" id="textfield5" size="2" />
          <a href="#"><img src="images/go.png" width="39" height="22" /></a></td>
      </tr>
    </table></td>
  </tr>
</table>
