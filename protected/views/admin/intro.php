<?php
$this->pageTitle = Yii::app()->name . ' - Intro Page';
$b = "Intro Page";

$this->breadcrumbs = $b;
?>
<script type="text/javascript" language="javascript">
    $(function () {
        var roxyFileman = '<?php echo Yii::app()->request->baseUrl; ?>/fileman/pagefile.html';
        $("#tabs").tabs();
        CKEDITOR.replace('Content_content_1', {
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
        CKEDITOR.replace('Content_content_2', {
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
    });

</script>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'content-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>
    <br  /><br  /><br  />
    <center>
        <div style="width:800px;  text-align:left">

            <div id="tabs" >
                <ul>
                    <li><a href="#tabs-1">ภาษาไทย</a></li>
                    <li><a href="#tabs-2">ภาษาอังกฤษ</a></li>
                </ul>
                <div id="tabs-1">
                    <?php
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
                        'model' => $model,
                        'attribute' => 'content_2',
                        'language' => 'en',
                        'editorTemplate' => 'full',
                        'height' => '800px',
                    ));
                    ?>
                </div>
                <div id="tabs-2">
                    <?php
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
                        'model' => $model,
                        'attribute' => 'content_1',
                        'language' => 'en',
                        'editorTemplate' => 'full',
                        'height' => '600px',
                    ));
                    ?>
                </div>

            </div>
            <br /><br />
            <table width="50%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th width="40%" align="right">ใช้งาน :</th>
                    <td width="75%"><?php echo $form->radioButtonList($model, 'attr1', array('1' => 'ใช้งาน', '0' => 'ไม่ใช้งาน',), array('separator' => '')); ?></td>
                </tr>
            </table><br />
            <br />

            <center>
                <input type='image' src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />
            </center>
        </div>

    </center>
<?php $this->endWidget(); ?>
</div><!-- form -->
