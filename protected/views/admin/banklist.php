<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'จัดการข้อมูลบัญชีธนาคาร';
?>
<script language="javascript">
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteBank/",{
									id:objId
									},function(data){
										if(data=="OK")
											location.href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BankList';
										else
											alert(data);
									});
	}
}
function deleteAll()
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
	{
		$("#banklist-form").submit();
	}
}
</script>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'banklist-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <input type="hidden" name='act' value="deleteall" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center"><br />
<span class="text4">รายชื่อบัญชีธนาคาร</span><br />
&nbsp;<br /></td>
      </tr>
      <tr>
        <td align="center" width="100%" class="tabletest">
          <table border="0" cellpadding="0" cellspacing="1">
            <tr>
              <th width="15%">เลือก</th>
              <th >ธนาคาร</th>
              <th >สาขา</th>
              <th>ชื่อบัญชี</th>
              <th>ประเภทบัญชี</th>
              <th>เลขที่บัญชี</th>
              <th width="15%">แก้ไข</th>
              <th width="15%">ลบ</th>
            </tr>
            <?php 
			foreach($data as $row){
			?>
            <tr>
              <td width="15%" align="center">
              <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id;?>' /></td>
              <td align="center">
              <?php echo $row->bank_name; ?>
              </td>
              <td align="center">
              <?php echo $row->acc_branch; ?>
              </td>
              <td align="center">
              <?php echo $row->acc_name; ?>
              </td>
              <td align="center">
              <?php echo $row->acc_type; ?>
              </td>
              <td align="center">
              <?php echo $row->acc_no; ?>
              </td>
              
              <td width="15%" align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Bank/<?php echo $row->id;?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
              <td width="15%" align="center">
              <a href='javascript:deleteit("<?php echo $row->id;?>");'>
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
            </tr>
           <?php
           }
		   ?>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br />
          <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/Bank/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
      </tr>
      <tr>
        <td align="center"><br />
          <br />
          <br /></td>
      </tr>
      
    </table>
            <?php $this->endWidget(); ?>