<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'แบรนด์';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table  border="0" cellspacing="0" cellpadding="0">
          <tr>
              <th width="40%" align="right">Group :</th>
              <td width="75%"><?php echo $form->dropDownList($model,'group_id', CHtml::listData(BrandGroup::model()->findAll(array('order' => 'brand_th ASC')), 'id', 'brand_th'), array('empty'=>'เลือก Group',																																												  				 
	  )); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">ชื่อแบรนด์ (ภาษาไทย) :</th>
              <td width="75%"><?php echo $form->textField($model,'name_th'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">ชื่อแบรนด์ (ภาษาอังกฤษ) :</th>
              <td width="75%"><?php echo $form->textField($model,'name_en'); ?></td>
            </tr>
            
            <tr>
              <th width="40%" align="right">คำอธิบาย(ภาษาไทย) :</th>
              <td width="75%"><?php echo $form->textArea($model,'brand_desc_th',array('rows'=>10,'cols'=>40)); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">คำอธิบาย (ภาษาอังกฤษ) :</th>
              <td width="75%"><?php echo $form->textArea($model,'brand_desc_en',array('rows'=>10,'cols'=>40)); ?></td>
            </tr>
             <tr>
              <th width="40%" align="right">จัดเรียง :</th>
              <td width="75%"><?php echo $form->textField($model,'sort_order',array('style'=>'width:50px')); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">จัดเรียงเมนู :</th>
              <td width="75%"><?php echo $form->textField($model,'menu_order',array('style'=>'width:50px')); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">Logo :</th>
              <td width="75%">
              <?php
			  $file = Yii::app()->request->baseUrl . '/images/brand/' . $model->brand_logo;
			  if(file_exists(Yii::app()->basePath . '/../images/brand/' .  $model->brand_logo) && $model->brand_logo != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="83" />
                  <?php
			  }
			  ?>
              <input type="file" name="brand_logo" id='brand_logo' /></td>
            </tr>
               
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/brandlist';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>