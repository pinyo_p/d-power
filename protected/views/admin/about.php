<?php 
$this->pageTitle="Admin Panel::Login"; 
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true,uploadURI : '<?php echo Yii::app()->request->baseUrl;?>/nicUpload.php'}) });
</script>

<div align="center">
<table style="text-align:left"><tr><td colspan="2">
<h1>รู้จัก Sam</h1>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'AboutSam',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

</td>
</tr>
	<tr>
                <th>
		<?php echo $form->labelEx($model,'title'); ?></th>
<td>
		<?php echo $form->textField($model,'title',array('style'=>'width:500px;')); ?>
		<?php echo $form->error($model,'title'); ?>
	<td>&nbsp;</td>
    </tr>

	<tr>
                <th>
		<?php echo $form->labelEx($model,'content_th'); ?></th>
<td>		<?php echo $form->textArea($model,'content_th',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
		<?php echo $form->error($model,'content_th'); ?>
	<td>&nbsp;</td>
    </tr>
    <tr>
                <th><?php echo $form->labelEx($model,'แสดงผลในเมนู'); ?></th>
                <td><?php echo $form->checkBox($model,'show_in_menu',  array()); ?><?php echo $form->error($model,'show_in_menu'); ?></td>
                <td>&nbsp;</td>
    </tr>

<tr>
<td>
</td>
<td>
	<div class="row buttons">
		<?php
	$this->widget('zii.widgets.jui.CJuiButton', array(
    'buttonType'=>'submit',
    'name'=>'btnSubmit',
    'value'=>'1',
    'caption'=>'Submit',
));
	?>
    
	</div>
<?php $this->endWidget(); ?>
</td>
</tr>
</table>
</div>