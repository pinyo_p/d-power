<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'แบนเนอร์วันสำคัญ';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <script type="text/javascript" language="javascript">
	$(function() {
		$( "#tabs" ).tabs();
		CKEDITOR.replace('Content_content_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Content_content_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
	});
	
	</script>
    
<table width="100%">
<tr>
<td align="center"><h3 class="underline"><?php echo (isset($_GET["id"]))?"แก้ไข":"เพิ่ม"; ?>ข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table border="0" cellspacing="0" cellpadding="0">

             
            <tr>
              <th width="100" align="right">วัน :</th>
              <td width="600">
                <?php echo $form->dropDownList($model,'day_id', CHtml::listData(Day::model()->findAll(array('order' => 'day_name ASC')), 'id', 'day_name'), array('empty'=>'-- เลือกวัน --',)); ?>
              </td>
            </tr>
            
            <tr>
              <th  align="right">อัพโหลด :</th>
              <td ><?php
			  $file = Yii::app()->request->baseUrl . '/images/banner_day/' . $model->img_src;
			  if(file_exists(Yii::app()->basePath . '/../images/banner_day/' .  $model->img_src) && $model->img_src != ""){
				  ?><br />
                   <img src="<?php echo $file; ?>" width="598" />
                  <?php
			  }
			  ?><br /> 
              <input type="file" name="img_src" id='img_src' /></td>
            </tr>
             
            
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;
          <a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/BannerDayList'; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>