<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายการสินค้า';
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
$id = (isset($_GET['id']) ? $_GET['id'] : "0");
?>
<script language="javascript">
    function changeIt(objTxt, objId, objProductId)
    {
        $.post('<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/UpdateCart/',
                {
                    id: objId,
                    val: $(objTxt).val(),
                    product_id: objProductId
                },
        function (data) {
            $("#price_" + objId).text(data.price);
            $("#total_" + objId).text(data.total);
            var total = 0;
            $('.sub_total').each(
                    function () {
                        val = $(this).text();
                        val = val.replace(",", "");
                        total += parseInt(val);

                    });
            $("#grand_total").text(addCommas(total));
            $("#net_total").text(addCommas(total));
//													   alert(data.total);
        }, 'json');

    }
    function deleteIt(objId)
    {
        $.post('<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/DeleteCart/', {id: objId},
        function (data)
        {
            if (data == "OK")
                location.reload();
            else
                alert(data);
        });
    }
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
</script>
<table width="100%"><tr><td class="main-column-bg-index ">
            <div class="main-column-content-index">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" class="txt_bold">จำนวนสินค้าในตะกร้า : <span class="txt_green"><?php echo $param['count_item']; ?></span> รายการ</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" width="100%" class="tabletest">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr class="txt_bold bg-gray2">
                                    <th colspan="5" style="text-align:left;">เลขที่ใบสั่งซื้อ : <span style="color:#DD7700"><?php echo substr("000000000" . $_REQUEST["id"], -9) ?></span></th>
                                </tr>
                                <tr class="txt_bold bg-gray2">
                                    <th width="150" align="center">รูปภาพสินค้า</th>
                                    <th  align="center">ชื่อสินค้า</th>
                                    <th width="100" align="center">ราคา/หน่วย</th>
                                    <th width="100" align="center">จำนวน</th>
                                    <th width="100" align="center">รวมเป็นราคา</th>
                                </tr>
                                <?php
                                $grand_total = 0;
                                foreach ($data as $row) {
                                    $grand_total += ($row->price * $row->amount);
                                    ?>
                                    <tr>
                                        <td align="center">
                                            <?php
                                            $file = Yii::app()->request->baseUrl . '/images/product/' . $row->Product['pic1'];
                                            if (file_exists(Yii::app()->basePath . '/../images/product/' . $row->Product['pic1']) && $row->Product['pic1'] != "") {
                                                ?>
                                                <img src="<?php echo $file; ?>" height="75" width="93"  />
                                                <?php
                                            } else {
                                                ?>
                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
                                                <?php
                                            }
                                            ?>

                                        </td>
                                        <td  align="center" class="txt_green txt_bold"><?php echo Yii::d($row->Product['product_name_1'], $row->Product['product_name_2']); ?></td>
                                        <td width="100" align="center" class="txt_bold"><span id='price_<?php echo $row->id; ?>'><?php echo number_format($row->price, 2, ".", ","); ?></span> บ.</td>
                                        <td width="100" align="center"><?php echo $row->amount; ?>
                                        </td>
                                        <td width="100" align="center" class="txt_bold"><span id='total_<?php echo $row->id; ?>' class="sub_total"><?php
                                                echo number_format(($row->price * $row->amount), 2, ".", ",");
                                                ?></span> บ.</td>

                                    </tr>
                                    <?php
                                }
                                $shippingFee = $param['shipping_fee'];
                                $netTotal = $grand_total + $shippingFee;
                                ?>
                                <tr class="txt_bold bg-gray2">
                                    <th style="text-align:left;">น้ำหนักสินค้าที่จัดส่ง </th>
                                    <th colspan="4" style="text-align:left;"><?php echo $param['total_weight']; ?> กก.</th>
                                </tr>
                                <tr class="txt_bold bg-gray2">
                                    <th style="text-align:left;">รูปแบบการจัดส่ง </th>
                                    <th colspan="4" style="text-align:left;">
                                        <?php
                                        switch ($param["shiptype"]) {
                                            case "in_reg":echo "จัดส่งพัสดุแบบลงทะเบียน";
                                                break;
                                            case "in_ems":echo "จัดส่งพัสดุแบบ EMS";
                                                break;
                                            case "in_gen":echo "จัดส่งพัสดุแบบธรรมดา";
                                                break;
                                        }
                                        ?>
                                    </th>
                                </tr>
                            </table>
                            </div></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="400" align="center">

                                    </td>
                                    <td width="20" valign="top">&nbsp;</td>
                                    <td valign="top" class="tabletest">

                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr >
                                                <th width="60%" height="30" class="txt_bold hline">รวมยอดสั่งซื้อ</th>

                                                <td height="30" align="right" class="txt_price hline"><span id="grand_total"><?php echo number_format($grand_total, 2, ".", ","); ?></span> บาท</td>
                                            </tr>
                                            <tr>
                                                <th width="60%" height="30" class="txt_bold hline">ค่าจัดส่ง</th>
                                                <td height="30" align="right" class="txt_price hline"><span id="net_total"><?php echo number_format($shippingFee, 2, ".", ","); ?></span> บาท</td>
                                            </tr>
                                            <tr>
                                                <th width="60%" height="30" class="txt_bold dhline">รวมราคาสุทธิ</th>
                                                <td height="30" align="right" class="txt_price dhline"><span id="net_total"><?php echo number_format($netTotal, 2, ".", ","); ?></span> บาท</td>
                                            </tr>
                                            <tr>
                                                <th width="60%" height="30" class="txt_bold dhline">จำนวนแต้ม</th>
                                                <td height="30" align="right" class="txt_price dhline"><span id="net_total"><?php echo $param["score"]; ?></span></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="padding:20px;" class="add_data">                            
                            <fieldset style="border:solid 1px #ccc">
                                <legend style="font-weight:bold;">ข้อมูลการจัดส่ง</legend>
                                <div style="padding:10px;">
                                <table width="100%">
                                    <tr>
                                        <th style="font-weight:bold;">หมายเลขพัสดุที่จัดส่ง : </th>
                                        <td>
                                            <input type="text" id="txtPackageNo" name="txtPackageNo" style="width:300px;" value="<?php echo $modelOrder->package_no; ?>" />
                                        </td>
                                    </tr>
                                </table>
                                </div>
                            </fieldset>
                            <br/><br/>
                    <center>
                       
                        <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" style="border:none;">
                        
                        <a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ShippingList'>
                            <img id="btnBack" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png"/>
                        </a>
                        
                        
                    </center>
                    </td>
                    </tr>
                </table>



            </div>
        </td>
    </tr>
</table>    
<?php $this->endWidget(); ?>