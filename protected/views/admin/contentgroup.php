<?php 
$part = Yii::app()->user->getValue("part");
$p = "";
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Manage User"; 
$str = "";
if($part=="OUT")
{
	switch($_GET['id'])
	{
		case "1": $str = "ประกาศ";break;
		case "2": $str = "บทความ";break;
		case "3": $str = "ผลการประเมิน";break;
		case "4": $str = "คำถามที่ถามบ่อย";break;
		case "5": $str = "ติดต่อ";break;
	}
}else if($part=="COP"){
	switch($_GET['id'])
	{
		case "1": $str = "รู้จัก SAM";break;
		case "2": $str = "ข่าวสาร";break;
		case "3": $str = "ประกาศ";break;
		case "4": $str = "ประชาสัมพันธ์";break;
		case "5": $str = "หน่วยงานที่เกี่ยวข้อง";break;
		case "6": $str = "ติดต่อ";break;
	}
}
?>
<script language="javascript">
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
		window.location = "<?php echo Yii::app()->request->baseUrl . "/index.php/Admin/DeleteContentGroup/";?>" + objId + "/?parent=<?php echo $_GET['id'];?>";
	}
	//Yii::app()->request->baseUrl . "/index.php/Admin/DeleteContentGroup/".$data->id . "/?parent=" . $_GET["id"]'
}
</script>
<style type="text/css">
.lmm_67_2{
	text-align:center;
	width:70px;

}
</style>
<div align="center">

<div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green"><?php echo $str;?></a> &gt; <a href="#" class="link_green">กลุ่ม<?php echo $str;?></a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">กลุ่ม<?php echo $str;?></div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table>
<table style="text-align:left" width="100%"><tr><td>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usergroup',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
<center>
	<table><tr valign="middle"><td align="right">
		<div class="form"><?php echo $form->labelEx($model,'group_name_th'); ?></div></td><td> :  </td><td valign="middle">
		<div class="form"><?php echo $form->textField($model,'group_name_th',array('style'=>'width:200px')); ?></div></td><td>
		<div class="form"><?php echo $form->error($model,'group_name_th'); ?></div></td>
        </tr>
        <tr valign="middle"><td align="right">
		<div class="form"><?php echo $form->labelEx($model,'group_name_en'); ?></div></td><td> :  </td><td valign="middle">
		<?php echo $form->textField($model,'group_name_en',array('style'=>'width:200px')); ?></td><td>
		<?php echo $form->error($model,'group_name_en'); ?></td>
        </tr>
        </table>
	
    
    	<input type="image" <?php Yii::hideit();?> class="lmm_67_1" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" alt="Submit button">
	</center>
<?php $this->endWidget(); ?>
</div><!-- form -->

<?php
$row=1;
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
	 'htmlOptions'=>array('width'=>'100%'),
   'columns'=>array(
        array(
			   'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + (++$row)',
			'header'=>'ลำดับ'
			,
			'htmlOptions'=>array('style'=>'text-align:center;width:30px')
			),          // display the 'title' attribute
        'group_name_th',
		'group_name_en',
		
		array(
             'class'=>'CLinkColumn',
			'label'=>'จัดการข้อมูล',
			 'urlExpression'=>'Yii::app()->request->baseUrl . "/index.php/Admin/NoticeList/".$data->id . "/?parent=" . $_GET["id"]',
			'header'=>'เพิ่มข้อมูล',
			'htmlOptions'=>array('style'=>'text-align:center;width:70px')
			),
		array(
             'class'=>'CLinkColumn',
			'label'=>'แก้ไขชื่อ',
			 'urlExpression'=>'Yii::app()->request->baseUrl . "/index.php/Admin/UpdateContentGroup/".$data->id . "/?parent=" . $_GET["id"]',
			'header'=>'แก้ไข',
			'htmlOptions'=>array('style'=>'text-align:center;width:30px;visibility:hidden;','class'=>"lmm_67_2")
			),
		array(
			  'name'=>'delete',
			  'type'=>'raw', 
             'value'=>'CHtml::link("ลบ","javascript:deleteit(" . $data->id . ")")' ,
			 'header'=>'ลบ',
			 'htmlOptions'=>array('width'=>'30px','style'=>'text-align:center;visibility:hidden;','class'=>"lmm_67_3")
			),
		

    ),
));
?>

</td>
</tr>
</table>
