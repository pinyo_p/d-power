<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="2"><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">รายงาน</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <?php
			$part = Yii::app()->user->getValue("part");
			if($part=="COP"){
			?>
            
            <tr>
              <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                  <td class="topix_header"><div class="topix_headtxt">รายงานหน้า Corporate</div></td>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
              <td width="15%">&nbsp;</td>
              <td width="85%"><ul class="report">
                <li><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/report/web_export.php">รายงานสถิติการเข้าชมเว็บไซต์ www.sam.or.th</a></li>
                <li><a target="_blank" target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/report/article_export.php">รายงานสถิติการเข้าชมหมวดบทความต่างๆ </a></li>
                <li><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/report/article_total.php">รายงานสถิติการเข้าชมบทความต่างๆ </a></li>
                <li><a href="#">รายงานสถิติการเข้าใช้งานระบบ </a></li>
              </ul></td>
            </tr>
            
            <?php
			}
			?>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            
            <?php
			
			if($part=="NPA"){
			?>
            
            <tr>
              <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/report/images/backend/images/topix_01.png" width="5" height="35" /></td>
                  <td class="topix_header"><div class="topix_headtxt">รายงานหน้า NPA</div></td>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/report/images/backend/images/topix_03.png" width="5" height="35" /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><ul class="report">
                <li><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/report/buyer_total.php">รายงานการขายทรัพย์</a></li>
                <li><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/report/seller_export.php">รายงานรายละเอียดการขายทรัพย์</a></li>
                <li><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/report/buyer_export.php">รายงานสรุปการขายทรัพย์ แยกตามเดือน</a></li>
                <li><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/report/buyer_group_export.php">รายงานสรุปการขายทรัพย์ แยกตามประเภททรัพย์ </a></li>
                <li><a target="_blank" href="<?php echo Yii::app()->request->baseUrl; ?>/report/product_view_export.php">รายงานการเข้าชมทรัพย์สิน แยกตามรายการทรัพย์</a></li>
                <li><a href="#">รายงานการจำหน่ายทรัพย์สิน แยกตามนายหน้า</a></li>
              </ul></td>
            </tr>

			<?php
			}?>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
          </table>
        