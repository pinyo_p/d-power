
<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'จัดการผู้ใช้งาน';
?>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteUser/", {
                id: objId
            }, function (data) {
                if (data.indexOf("OK") > -1)
                    $("#productlist-form").submit();
                else if(data.indexOf("DENIED") > -1)
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
                else
                    alert(data);
            });
        }
    }
    function deleteAll()
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $("#act").val("deleteall");
            $("#productlist-form").submit();

        }
    }
    function change_status(objId, objStatus)
    {
        str_status = $("#" + objStatus).val();
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/changeUserStatus/", {
            id: objId,
            status: str_status,
        }, function (data) {
            if (data == "OK")
                $("#productlist-form").submit();
            else if(data == "DENIED")
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
            else
                alert(data);
        });
    }

    function searchit()
    {

        $("#productlist-form").submit();

    }
    function gotoPage(objPage)
    {
        $("#page").val(objPage);
        $("#productlist-form").submit();
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'productlist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4">ค้นหาผู้ใช้งาน</span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="right">ชื่อ/นามสกุล/Username :</th>
                    <td ><?php echo $form->textField($model, 'key_word'); ?></td>
                    <th align="right">กลุ่มผู้ใช้งาน :</th>
                    <td ><?php echo $form->dropDownList($model, 'group_id', CHtml::listData(UserGroup::model()->findAll(array('order' => 'group_name ASC')), 'id', 'group_name'), array('empty' => 'เลือก กลุ่มผู้ใช้งาน',
));
?><?php echo $form->error($model, 'group_id'); ?></td>
                </tr>

                <tr>
                    <th align="right">สถานะ :</th>
                    <td ><?php
                        echo $form->dropDownList($model, 'status', array('0' => 'ใช้งาน', '1' => 'ยกเลิก'), array('empty' => ' สถานะ',
                        ));
                        ?></td>
                    <td  colspan="2" align="center"></td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" width="55" height="26" />
                    </td>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center"> <br /><br /><br />
            <span class="text4">รายชื่อผู้ใช้งาน</span><br />
            &nbsp;<br /></td>
    </tr>

    <tr>
        <td align="center" width="100%" class="tabletest">




            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">เลือก</th>
                    <th >ชื่อ - นามสกุล</th>
                    <th >Username</th>
                    <th >กลุ่มผู้ใช้งาน</th>
                    <th  >สถานะ</th>
                    <?php
                    if ($this->checkUserPermission('user', '5')){                
                    ?>
                    <th >แสดงรหัสผ่าน</th>
                    <?php } ?>
                    <th width="60">แก้ไข</th>
                    <th width="60">ลบ</th>
                </tr>
                <?php
                foreach ($data as $row) {
                    ?>
                    <tr>
                        <td align="center">
                            <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' /></td>

                        <td align="left">
    <?php echo $row->first_name; ?> <?php echo $row->last_name; ?>
                        </td>
                        <td align="left">
    <?php echo $row->username; ?> 
                        </td>
                        <td align="left">
    <?php echo $row->group['group_name']; ?> 
                        </td>
                        <td align="center" valign="middle">
                            <table style="border:none;"><tr valign="top"><td style="border:none; text-align:right">
                                        <select name="status_<?php echo $row->id; ?>" id='status_<?php echo $row->id; ?>'>
                                            <option value="0" <?php echo ($row->status == "0" ? " selected='selected'" : ""); ?> >ใช้งาน</option>
                                            <option value="1" <?php echo ($row->status == "1" ? " selected='selected'" : ""); ?>>ยกเลิก</option>
                                        </select>
                                    </td>
                                    <td style="border:none;">
                                        <a href='javascript:;' onclick="change_status('<?php echo $row->id; ?>', 'status_<?php echo $row->id; ?>')">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />
                                        </a>
                                    </td>
                                </tr>
                            </table>



                        </td>
                        <?php
                    if ($this->checkUserPermission('user', '5')){                
                    ?>
                        <td align="left">
                            <a href='javascript:alert("<?php echo $row->password; ?>")'>แสดงรหัสผ่าน</a>
                        </td>
                    <?php } ?>
                        <td  align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/User/<?php echo $row->id; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
                        <td align="center">
                            <a href='javascript:deleteit("<?php echo $row->id; ?>");'>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
            if ($param['max_page'] > 1) {
                ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                        <td style="border:none;"><div class="nav_page">
                                <ul>
                                    <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
                                    <?php
                                    $start_page = ($page > 2 ? $page - 2 : 1);
                                    $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
                                    if ($end_page <= 5)
                                        $end_page = 5;
                                    else if (($end_page - $page) < 5)
                                        $start_page = ($end_page - 4);
                                    if ($end_page > $param['max_page'])
                                        $end_page = $param['max_page'];
                                    for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                                        $class = "";
                                        if (($i + 1) == $page)
                                            $class = '  class="navselect"';
                                        else
                                            $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                                        ?>
                                        <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                        <?php
                                    }
                                    ?>
                                    <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                                </ul>
                            </div></td>
                        <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                            <table width="200"  style="border:none;"><tr><td style="border:none;">
                                        <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                        <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                        <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png" width="39" height="22" /></a></td></tr></table>


                        </td>
                    </tr>
                </table>
                <?php
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="center">



            <br />
            <br />
            <br />
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/User/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>