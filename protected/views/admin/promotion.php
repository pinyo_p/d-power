<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'Promotion';
?>
<script type="text/javascript" language="javascript">
	$(function() {
		$( "#tabs" ).tabs();
		CKEDITOR.replace('Content_content_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Content_content_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
	});
	
	</script>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
        
        
        
          <div style="width:800px;  text-align:left">

<div id="tabs" >
	<ul>
		<li><a href="#tabs-1">ภาษาไทย</a></li>
		<li><a href="#tabs-2">ภาษาอังกฤษ</a></li>
	</ul>
	<div id="tabs-1">
    หัวข้อ(ภาษาไทย)<br />
    <?php echo $form->textField($model,'title_th',array('style'=>'width:450px')); ?>
    <br /><br />
    เนื้อหา(ภาษาไทย)<br />
		<?php 
		Yii::import('ext.ckeditor.*');
		$this->widget('application.extensions.ckeditor.CKEditor', array(
'model'=>$model,
'attribute'=>'content_th',
'language'=>'en',
'editorTemplate'=>'full',
'height'=>'800px',
)); ?>
	</div>
	<div id="tabs-2">
    หัวข้อ(ภาษาอังกฤษ)
    <br />
    <?php echo $form->textField($model,'title_en',array('style'=>'width:450px')); ?>
    <br /><br />
    เนื้อหา(ภาษาอังกฤษ)<br />
		<?php 
		Yii::import('ext.ckeditor.*');
		$this->widget('application.extensions.ckeditor.CKEditor', array(
'model'=>$model,
'attribute'=>'content_en',
'language'=>'en',
'editorTemplate'=>'full',
'height'=>'600px',
)); ?>
	</div>
    
    <div style="padding-left:15px">
	Sort <br /> 
     <?php echo $form->textField($model,'sort_order',array('style'=>'width:150px')); ?>
              
    </div>
    <div style="padding-left:15px">
	ภาพหลัก 
	<?php
			  $file = Yii::app()->request->baseUrl . '/images/content/' . $model->attr1;
			  if(file_exists(Yii::app()->basePath . '/../images/content/' .  $model->attr1) && $model->attr1 != ""){
				  ?><br />
                   <img src="<?php echo $file; ?>" height="83" />
                  <?php
			  }
			  ?><br /> 
              <input type="file" name="files" id='files' />
              
    </div>
    <br />
</div>
<br /><br />
</div>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/promotionlist';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>