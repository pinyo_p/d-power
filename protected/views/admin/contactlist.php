
<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายการผู้ลงทะเบียน ' . $param['title'];
?>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteContact/<?php echo $_GET["id"] ?>", {
                id: objId
            }, function (data) {
                if (data.indexOf("OK") > -1)
                    $("#productlist-form").submit();
                else if(data.indexOf("DENIED") > -1)
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
                else
                    alert(data);
            });
        }
    }
    function deleteAll()
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $("#act").val("deleteall");
            $("#productlist-form").submit();
            $("#act").val("search");
        }
    }
    function change_status(objStatus, objId)
    {
        str_status = $(objStatus).val();
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/changeContactStatus/<?php echo $_GET["id"] ?>", {
            id: objId,
            status: str_status,
        }, function (data) {
            if (data == "OK")
                $("#productlist-form").submit();
            else if(data == "DENIED")
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
            else
                alert(data);
        });
    }

    function searchit()
    {

        $("#productlist-form").submit();

    }
    function gotoPage(objPage)
    {
        $("#page").val(objPage);
        $("#productlist-form").submit();
    }

    function export_data()
    {
        var fullname = $("#Contact_fullname").val();
        var company_name = $("#Contact_company_name").val();
        var subject = $("#Contact_subject").val();
        var detail = $("#Contact_detail").val();
        var start_date = $("#Contact_start_date").val();
        var end_date = $("#Contact_end_date").val();
        var status = $("#Contact_status").val();
        window.open('<?php echo Yii::app()->request->baseUrl; ?>/index.php/Admin/ExportContact/?fullname=' + fullname + '&company_name=' + company_name + '&subject=' + subject + '&status=' + status + '&start_date=' + start_date + '&end_date=' + end_date + '&detail=' + detail + '&type=<?php echo $_GET['id']; ?>');
    }

</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'productlist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4">ค้นหา ผู้ลงทะเบียน <?php echo $param['title']; ?></span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table  border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <th align="right">ชือ-นามสกุล :</th>
                    <td ><?php echo $form->textField($model, 'fullname'); ?></td>
                    <th align="right">ชื่อบริษัท :</th>
                    <td ><?php echo $form->textField($model, 'company_name'); ?></td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <th align="right">หัวข้อ:</th>
                    <td ><?php echo $form->textField($model, 'subject'); ?></td>
                    <th align="right">รายละเอียด :</th>
                    <td ><?php echo $form->textField($model, 'detail'); ?></td>

                </tr>

                <tr>
                    <th width="120"><div align="right"><span class="h1">วันที่สมัคร :</span></div></th>
                <td width="280"><?php
$this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
    'name' => 'Contact[start_date]',
    'value' => $model->start_date,
    'options' => array(
        'showAnim' => 'fold',
        'dateFormat' => 'yy-mm-dd',
        'altFormat' => 'yy-mm-dd',
        'changeMonth' => 'true',
        'changeYear' => 'true',
        'showOn' => "both",
        'buttonImage' => Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",
        'buttonImageOnly' => "true",
    ),
    'htmlOptions' => array(
        'style' => 'height:20px;'
    ),
        )
);
?></td>
                <th width="120"><div align="right"><span class="h1">ถีง :</span></div></th>
                <td width="280"><?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
                        'name' => 'Contact[end_date]',
                        'value' => $model->end_date,
                        'options' => array(
                            'showAnim' => 'fold',
                            'dateFormat' => 'yy-mm-dd',
                            'altFormat' => 'yy-mm-dd',
                            'changeMonth' => 'true',
                            'changeYear' => 'true',
                            'showOn' => "both",
                            'buttonImage' => Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",
                            'buttonImageOnly' => "true",
                        ),
                        'htmlOptions' => array(
                            'style' => 'height:20px;'
                        ),
                            )
                    );
                    ?></td>
    </tr>


    <tr>
        <th width="120"><div align="right"><span class="h1">สถานะ :</span></div></th>
<td width="280" colspan="2">
<?php
echo $form->dropDownList($model, 'status', array('' => '-- เลือกสถานะ --', '0' => 'มาใหม่', '1' => 'ติดต่อแล้ว'));
?>
</td>

<td width="120" colspan="4" align="center">
    <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" alt="Submit button">&nbsp;<a href="javascript:export_data();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_export.png" alt="Export"></a></td>
</tr>
</table>
</td>
</tr>
<tr>
    <td align="center"> <br /><br /><br />
        <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
        &nbsp;<br /></td>
</tr>

<tr>
    <td align="center" width="100%" class="tabletest">




        <table border="0" cellpadding="0" cellspacing="1">
            <tr>
                <th width="60">เลือก</th>

                <th >ชื่อ - นามสกุล</th>
                <th >ชื่อบริษัท</th>
                <th >Email</th>
                <th >เบอร์มือถือ</th>
                <th >หัวข้อ</th>
                <th width="60">สถานะ</th>
                <th width="60">ลบ</th>
            </tr>
<?php
foreach ($data as $row) {
    ?>
                <tr>
                    <td align="center">
                        <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' /></td>



                    <td>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ContactDetail/<?php echo $row->id; ?>" target="_blank" >
    <?php echo $row->fullname; ?>
                        </a>
                    </td>
                    <td>
    <?php echo $row->company_name; ?>
                    </td>
                    <td>
    <?php echo $row->email; ?>
                    </td>
                    <td>
                        <?php echo $row->tel; ?>
                    </td>

                    <td align="center">
    <?php echo $row->subject; ?>
                    </td>

                    <td  align="center"><select name="select3" class="textb" id="status_<?php echo $row->id; ?>" onchange='change_status(this, "<?php echo $row->id; ?>")' style="width:120px">
                            <option value="">- เลือกสถานะ -</option>
                            <option value="0" <?php echo ($row->status == 0 ? " selected='selected' " : "") ?>>มาใหม่</option>
                            <option value="1" <?php echo ($row->status == 1 ? " selected='selected' " : "") ?>>ติดต่อแล้ว</option>
                        </select></td>
                    <td align="center">
                        <a href='javascript:deleteit("<?php echo $row->id; ?>");'>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
                </tr>
    <?php
}
?>
        </table>
<?php
if ($param['max_page'] > 1) {
    ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                    <td style="border:none;"><div class="nav_page">
                            <ul>
                                <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
            <?php
            $start_page = ($page > 2 ? $page - 2 : 1);
            $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
            if ($end_page <= 5)
                $end_page = 5;
            else if (($end_page - $page) < 5)
                $start_page = ($end_page - 4);
            if ($end_page > $param['max_page'])
                $end_page = $param['max_page'];

            for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                $class = "";
                if (($i + 1) == $page)
                    $class = '  class="navselect"';
                else
                    $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                ?>
                                    <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                    <?php
                                }
                                if ($page != $end_page) {
                                    ?>
                                    <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div></td>
                    <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                        <table width="200"  style="border:none;"><tr><td style="border:none;">
                                    <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                    <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                    <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png" height="22" /></a></td></tr></table>


                    </td>
                </tr>
            </table>
    <?php
}
?>
    </td>
</tr>
<tr>
    <td align="center">



        <br />
        <br />
        <br />
        <a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
</tr>
<tr>
    <td align="center"><br />
        <br />
        <br /></td>
</tr>

</table>
<?php $this->endWidget(); ?>