<?php 
$this->pageTitle="Admin Panel::ความต้องการฝากขายทรัพย์สิน"; 
function g($val,$caption)
{
	return (trim($val)!=""?$caption . " " . $val:"");
}
function g2($val,$caption)
{
	return (trim($val)!="" && $val!="0"?$val . $caption:"");
}
function getDirection($sortby,$model)
{
	$dir = 1;
	if($sortby==$model->sort_by && $model->sort_dir == 1)
		$dir = 2;
	return $dir;
}
function getDirectionIcon($sortby,$model)
{
	$img = "";
	if($sortby==$model->sort_by){
		if($model->sort_dir=="2")
		{
			$img = "sort_botton_down.png";
		}else{
			$img = "sort_botton_up.png";
		}
	}
	if($img != "")
	{
		$img = "<img src='" . Yii::app()->request->baseUrl . "/images/$img' />";
	}
	return $img;
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteWant/');?>',{id:objId},function(data){
																							  location.reload();
																							   });
	}
	
}
function searchit()
{
	if($("#Property_start_price").val()!="" && $("#Property_end_price").val()!="" && ($("#Property_start_price").val()>$("#Property_end_price").val()  ))
	{
		alert('ราคาเริ่มต้นต้องน้อยกว่าราคาสิ้นสุด');
	}else{
		$("#frm_search").submit();
	}
}
function sendit()
{
	$("#act").val("sendit");

	$("#frm_search").submit();
}
</script>
<style type="text/css">
.hd a{
	color:white;
}
</style>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt;<a href="#" class="link_green"> NPA</a> &gt; <a href="#" class="link_green">ฝากความต้องการทรัพย์สิน</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ฝากความต้องการทรัพย์สิน [รายการทรัพย์ที่ส่งแล้ว]</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td  class="nparesult_table_content">
              
                             <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_search',
	'enableClientValidation'=>true,
	'action' => Yii::app()->createUrl('/admin/sendwant/'),
	 
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>


<br />
                      <table bgcolor="#78CF2D" width="100%" cellpadding="10" cellspacing="1">
<tr class="hd" style="background-color:#78CF2D;color:white;font-weight:bold" valign="top">

<td>
ลำดับที่
</td>

<td>รูปประกอบ</td>
<td width="70px">
รหัสทรัพย์
</td>
<td>ประเภททรัพย์สิน</td>
<td>
ที่ตั้ง
</td>
<td>จังหวัด</td>
<td>เนื้อที่</td>
<td>ราคาขั้นต่ำ</td>
<td>
สถานะ</a>
</td>

</tr>

<?php
$i=0;
foreach($data as $row)
{
	$i++;
	?>
    <tr bgcolor="white">
  


	<td>
		<?php echo $i;?>
        </td>
  
<td>
<?php
echo CHtml::image(Yii::app()->request->baseUrl . "/images/npa/" . $row->property['id'] ."/"  . $row->property['main_image'] , "",array("width"=>"100px"));
?>
</td>
<td align="center"><b>
<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->property['id'];?>'  target="_blank" >
<?php echo $row->property['product_code'];?>
</a></b>
</td>
<td>

<?php
echo $row->property->category['product_group'] ;
?>


</td><td>

 <?php echo g($row->property['road'],'ถนน');?> <?php echo g($row->property->ta['thai_name'],'ต.');?> 
    <?php echo g($row->property->d['thai_name'],'อ.');?></td>
 <td> <?php echo g($row->property->p['thai_name'],' ');?></td>
 <td><?php echo g2($row->property->area_rai," ไร่");?> <?php echo g2($row->property->area_ngan," งาน");?> <?php echo g2($row->property->area_wa," ตร.ว.");?></td>
 <td>
 <?php echo number_format($row->property->price,0,".",",");?> 
 </td>
<td>
<?php
switch($row->property->status)
{
	case "0":echo "รอการอนุมัติ";break;
	case "1":{
		if($row->property->sale_type=="0")
			echo "<img src='" . Yii::app()->request->baseUrl . "/images/buy4.gif' height='30' />" ;
		else
			echo "<img src='" . Yii::app()->request->baseUrl . "/images/button_status_6_auction21.gif' height='30' />";
		}break;
	case "2":echo "<img src='" . Yii::app()->request->baseUrl . "/images/button_status_6_sold2.gif' height='30' />";break;
	case "3":echo "ยกเลิกแล้ว";break;
}
?>
</td>
</tr>
    <?php
	
//	print_r($row);
//	echo "<hr />";
}
?>
<tr>
<td colspan="9" bgcolor="white">
<center>
        <br /><a href='javascript:history.go(-1)'>
         <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/cancle2.png" />
         </a>
         <br /><br />
        </center>
</td>
</tr>
 <?php $this->endWidget(); ?>
           
          </table>
        
        <script language="javascript">
$("#check_all").click(function(){
							   $(".check_all").attr("checked",$('#check_all').is(':checked'));
							   });
</script>