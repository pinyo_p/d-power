<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/admin/css/style.css" rel="stylesheet" type="text/css" />
        <?php
        $cs = Yii::app()->clientScript;
        $cs->scriptMap = array(
            'jquery.js' => Yii::app()->request->baseUrl . '/protected/vendors/js/jquery-1.8.2.min.js',
            'jquery-ui.min.js' => Yii::app()->request->baseUrl . '/protected/vendors/js/jquery-ui-1.8.24.custom.min.js',
        );
        Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/protected/vendors/css/eggplant/jquery-ui-1.8.24.custom.css', 'screen'
        );
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        ?>
        <?php
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/Admin/login');
        }
        ?>

        <style type="text/css">
            body{
                background:none;
            }
        </style>

    </head>

    <body>
        <center>
            <table><tr><td rowspan="5" valign="top" class="main_bg">

                        <table border="0" cellpadding="0" cellspacing="1">
                            <tr>
                                <th  width="120">ชื่อผู้ติดต่อ</th><td bgcolor="white"><?php echo $model->fullname; ?></td>
                            </tr><tr>
                                <th >ชื่อบริษัท</th><td bgcolor="white"><?php echo $model->company_name; ?></td>
                            </tr>

                            <tr>
                                <th >ที่อยู่</th><td bgcolor="white"><?php echo $model->address; ?>  <?php echo $model->Provinces['thai_name']; ?> <?php echo $model->zipcode; ?></td>
                            </tr>


                            <tr>
                                <th >เบอร์โทรศัพท์</th><td bgcolor="white"><?php echo $model->phone_no; ?></td>
                            </tr>
                            <tr>
                                <th >หมายเหตุ</th><td bgcolor="white"><?php echo $model->remark; ?></td>
                            </tr>


                        </table>
                    </td></tr></table>
        </center>
        <br /><br />
    </body>
</head>