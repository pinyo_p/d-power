<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'หมวดหมู่สินค้า';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table width="50%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <th width="40%" align="right">ชื่อหมวดหมู่ (ภาษาไทย) :</th>
              <td width="75%"><?php echo $form->textField($model,'brand_th'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">ชื่อหมวดหมู่ (ภาษาอังกฤษ) :</th>
              <td width="75%"><?php echo $form->textField($model,'brand_en'); ?></td>
            </tr>
            
            
            
            
             <tr>
              <th width="40%" align="right">จัดเรียง :</th>
              <td width="75%"><?php echo $form->textField($model,'sort',array('style'=>'width:50px')); ?></td>
            </tr>
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/brandgrouplist';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>