﻿<?php 
$part = Yii::app()->user->getValue("part");
$p = "";
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Manage User"; 

?>

<script language="javascript">
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
		window.location = "<?php echo Yii::app()->request->baseUrl . "/index.php/Admin/DeleteUserGroup/";?>" + objId;
	}
}

</script>
<div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ผู้ใช้งาน</a> &gt; <a href="#" class="link_green">กลุ่มผู้ใช้งาน</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">กลุ่มผู้ใช้งาน</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table>
<table style="text-align:left" width="100%"><tr><td>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usergroup',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
<center>
	<table><tr valign="middle"><td>
		<div class="form"><?php echo $form->labelEx($model,'group_name'); ?></div></td><td> :  </td><td valign="middle">
		<div class="form"><?php echo $form->textField($model,'group_name',array('style'=>'width:200px')); ?></div></td><td>
		<div class="form"><?php echo $form->error($model,'group_name'); ?></div></td>
        </tr>
        </table>
	
    
    	<input type="image"  style="visibility:hidden;position:absolute;" class='lmm_21_1' src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" alt="Submit button">
	</center>
<?php $this->endWidget(); ?>
</div><!-- form -->

<?php
$row=1;
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
	 'htmlOptions'=>array('width'=>'100%'),
   'columns'=>array(
        array(
			   'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + (++$row)',
			'header'=>'ลำดับ'
			,
			'htmlOptions'=>array('width'=>'30px','style'=>'text-align:center')
			),          // display the 'title' attribute
        'group_name',
		
		array(
             'class'=>'CLinkColumn',
			'label'=>'แก้ไข',
			 'urlExpression'=>'Yii::app()->request->baseUrl . "/index.php/Admin/UpdateUserGroup/".$data->id',
			'header'=>'แก้ไข',
			'htmlOptions'=>array('width'=>'30px')
			),
	
		array(
			  'name'=>'premission',
			  'type'=>'raw', 
             'value'=>'CHtml::link("ลบ","javascript:deleteit(" . $data->id . ")")',
			 'header'=>'ลบ',
			 'htmlOptions'=>array('width'=>'30px')
			),
		array(
             'class'=>'CLinkColumn',
			'label'=>'กำหนดสิทธิ์',
			 'urlExpression'=>'Yii::app()->request->baseUrl . "/index.php/Admin/PremissionUsers/".$data->id',
			'header'=>'กำหนดสิทธิ์',
			'htmlOptions'=>array('width'=>'70px','onclick'=>'checkit()')
			),
 
    ),
));
?>

</td>
</tr>
</table>
