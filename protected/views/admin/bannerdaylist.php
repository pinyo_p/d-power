
<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'จัดการแบนเนอร์วันสำคัญ';
?>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteBannerDay/", {
                id: objId
            }, function (data) {
                if (data.indexOf("OK") > -1)
                    $("#productlist-form").submit();
                else if(data.indexOf("DENIED") > -1)
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
                else
                    alert(data);
            });
        }
    }
    function deleteAll()
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $("#act").val("deleteall");
            $("#productlist-form").submit();

        }
    }
    function chooseit(objId)
    {
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/chooseBannerDay/", {
            id: objId
        }, function (data) {
            if (data == "OK")
                $("#productlist-form").submit();
             else if(data == "DENIED")
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
            else
                alert(data);
        });
    }

    function searchit()
    {

        $("#productlist-form").submit();

    }
    function gotoPage(objPage)
    {
        $("#page").val(objPage);
        $("#productlist-form").submit();
    }
    function update_banner_day()
    {
        var obj_status = $(":checked").val();
        var obj_s_date = $("#start_date").val();
        var obj_e_date = $("#end_date").val();


        $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/updateBannerDay/", {
            status: obj_status,
            s_date: obj_s_date,
            e_date: obj_e_date
        }, function (data) {
            if (data == "OK")
                $("#productlist-form").submit();
            else
                alert(data);
        });
    }
    $(function () {

        $("#start_date").datepicker({'showAnim': 'fold', 'dateFormat': 'yy-mm-dd', 'altFormat': 'yy-mm-dd', 'changeMonth': 'true', 'changeYear': 'true', 'showOn': 'both', 'yearRange': '-80\'', 'buttonImage': '<?php echo Yii::app()->request->baseUrl; ?>/images/icon/rdDatePicker.gif', 'buttonImageOnly': 'true'});
        $("#end_date").datepicker({'showAnim': 'fold', 'dateFormat': 'yy-mm-dd', 'altFormat': 'yy-mm-dd', 'changeMonth': 'true', 'changeYear': 'true', 'showOn': 'both', 'yearRange': '-80\'', 'buttonImage': '<?php echo Yii::app()->request->baseUrl; ?>/images/icon/rdDatePicker.gif', 'buttonImageOnly': 'true'});
    });
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'productlist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4"><img src = '<?php echo Yii::app()->request->baseUrl; ?>/images/banner_day/<?php echo $param['banner']->content_2; ?>' width="400" /></span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center">
            สถานะ : <input type="radio" id='enable' value="1" name="attr1" <?php echo ($param['banner']->attr1 == "1" ? " checked='checked' " : "") ?> /><label for="enable">ใช้งาน</label> <input type="radio" id='disable' value="0" name="attr1" <?php echo ($param['banner']->attr1 == "0" ? " checked='checked' " : "") ?>/><label for="disable">ไม่ใช้งาน</label></td>
    </tr>
    <tr>
        <td align="center">
            วันที่เริ่มต้น <input type="text" name="start_date" id="start_date" style="width:70px" value="<?php echo $param['banner']->attr2; ?>" /> 
        &nbsp;&nbsp;วันที่สิ้นสุด <input type="text" name="end_date" id="end_date" style="width:70px" value="<?php echo $param['banner']->attr3; ?>" />
        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <a href='javascript:update_banner_day()'><img 	src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" /></a>
        </td>
    </tr>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4">ค้นหา แบนเนอร์วันสำคัญ</span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="right">วัน :</th>
                    <td ><?php echo $form->dropDownList($model, 'day_id', CHtml::listData(Day::model()->findAll(array('order' => 'day_name ASC')), 'id', 'day_name'), array('empty' => '-- ทั้งหมด --', 'style' => 'width:100%'));
?></td>
                    <td colspan="4" align="center">
                        <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" width="55" height="26" />
                    </td>
                </tr>


            </table>
        </td>
    </tr>
    <tr>
        <td align="center"> <br /><br /><br />
            <span class="text4">รายการแบนเนอร์วันสำคัญ</span><br />
            &nbsp;<br /></td>
    </tr>

    <tr>
        <td align="center" width="100%" class="tabletest">




            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">เลือก</th>
                    <th width="200">วัน</th>
                    <th >รูป</th>
                    <th width="60">เลือก</th>
                    <th width="60">แก้ไข</th>
                    <th width="60">ลบ</th>
                </tr>
                <?php
                foreach ($data as $row) {
                    ?>
                    <tr>
                        <td align="center">
                            <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' /></td>

                        <td align="left">
                            <?php echo $row->day['day_name']; ?> 
                        </td>
                        <td align="center">
                            <?php
                            $file = Yii::app()->request->baseUrl . '/images/banner_day/' . $row->img_src;
                            if (file_exists(Yii::app()->basePath . '/../images/banner_day/' . $row->img_src) && $row->img_src != "") {
                                ?>
                                <img src="<?php echo $file; ?>" style="width:300px;" />
                                <?php
                            }
                            ?>
                        </td>
                        <td  align="center"><a href="javascript:chooseit('<?php echo $row->id; ?>')"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/true.png" height="16" /></a></td>

                        <td  align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BannerDay/<?php echo $row->id; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
                        <td align="center">
                            <a href='javascript:deleteit("<?php echo $row->id; ?>");'>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
            if ($param['max_page'] > 1) {
                ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                        <td style="border:none;"><div class="nav_page">
                                <ul>
                                    <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
                                    <?php
                                    $start_page = ($page > 2 ? $page - 2 : 1);
                                    $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
                                    if ($end_page <= 5)
                                        $end_page = 5;
                                    else if (($end_page - $page) < 5)
                                        $start_page = ($end_page - 4);
                                    if ($end_page > $param['max_page'])
                                        $end_page = $param['max_page'];
                                    for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                                        $class = "";
                                        if (($i + 1) == $page)
                                            $class = '  class="navselect"';
                                        else
                                            $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                                        ?>
                                        <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                        <?php
                                    }
                                    ?>
                                    <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                                </ul>
                            </div></td>
                        <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                            <table width="200"  style="border:none;"><tr><td style="border:none;">
                                        <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                        <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                        <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png" width="39" height="22" /></a></td></tr></table>


                        </td>
                    </tr>
                </table>
                <?php
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="center">



            <br />
            <br />
            <br />
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/BannerDay/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>