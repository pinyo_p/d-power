<?php 
$this->pageTitle="Admin Panel::Popup List"; 
$part = Yii::app()->user->getValue("part")
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	function deleteit(objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
		$.post('<?php echo Yii::app()->createUrl('/admin/DeletePopup/');?>',{id:objId},function(data){
																								  	window.location.reload();
																								   });
		}
	}
	function displayit(objId)
	{

		$.post('<?php echo Yii::app()->createUrl('/admin/DisplayMarquee/');?>',{id:objId},function(data){
																								   window.location.reload();
																								   });
	}
	function hideit(objId)
	{

		$.post('<?php echo Yii::app()->createUrl('/admin/HideMarquee/');?>',{id:objId},function(data){
																								   window.location.reload();
																								   });
	}
	function deleteitAll()
	{
		$("#act").val('deleteAll');
		$("#frm_marquee").submit();
	}
	function askToShowPopup(objPart,objId)
	{
		$.post('<?php echo Yii::app()->createUrl('/admin/AskForShowPopup/');?>',{
												 id:objId,
												 target_part:objPart,
												 },function(data){
																								  	 window.location.reload();
																								   });
	}
	function Approve(objPart,objId)
	{
		$.post('<?php echo Yii::app()->createUrl('/admin/ApprovePopup/');?>',{
												 id:objId,
												 target_part:objPart,
												 },function(data){
																								  	 window.location.reload();
																								   });
	}
	function Display(objPart,objId)
	{
		$.post('<?php echo Yii::app()->createUrl('/admin/DisplayPopup/');?>',{
												 id:objId,
												 target_part:objPart,
												 },function(data){
																								  	 window.location.reload();
																								   });
	}
	function CancelPopup(objPart,objId)
	{
		$.post('<?php echo Yii::app()->createUrl('/admin/CancelPopup/');?>',{
												 id:objId,
												 target_part:objPart,
												 },function(data){
																								  	 window.location.reload();
																								   });
	}
	
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">Popup List</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">Popup List</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><br />
              <?php 
					
					$form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_marquee',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input name="act" type="hidden" id='act'/>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center">
                      <br />
                      <table width="100%" border="0" cellspacing="0" cellpadding="3" class="nparesult_table_content">
                        <tr style="background-color:#78CF2D;color:white;" valign="middle">
                              <td width="38%" align="center" valign="top" class="txt_bold">ข้อความ</td>
                              <td width="23%" align="center" valign="top" class="txt_bold">การแสดงผลฝั่ง Corperate</td>
                              <td width="23%" align="center" valign="top" class="txt_bold">การแสดงผลฝั่ง NPA</td>
                              <td width="8%" align="center" valign="top" class="txt_bold">แก้ไข</td>
                              <td width="8%" align="center" valign="top" class="txt_bold">ลบ</td>
                             
                            </tr>
                            <?php
						$i=0;
						foreach($data as $row){
							$i++;
							if($i % 2 ==0)
								$class = "rowb";
							else
								$class = "rowa";
						?>
                        <tr valign="middle">
                             
                              
                              <td class="<?php echo $class;?>"  width="38%" align="left" valign="middle">
                              [<?php echo $row->part;?>] <?php echo strip_tags($row->popup_th);?> / <?php echo strip_tags($row->popup_en);?>
                              </td>
                              <td style="white-space:nowrap"  class="<?php echo $class;?>">
                              <br />
                              <?php
							  if($part=="NPA"){
								  if($row->display_cop=="0" && $row->display_npa=="1")
								  {
									  echo "<a href='javascript:askToShowPopup(\"COP\"," . $row->id . ")'  ";
									   Yii::hideit();
									   echo " class='lmm_16_5 link_search'>ส่งคำขอให้แสดงผลในหน้า Corperate</a>";
								  }else if($row->display_cop=="1")
								  {
								  		echo "กำลังแสดงผล";
								  }else if($row->display_cop=="2")
								  {
									  echo "รออนุมัติ";
								  }else if($row->display_cop=="3")
								  {
									  echo "รายการถูกยกเลิก";
								  }
							  }
							  if($part=="COP")
							  {
								  if($row->part=="NPA"){
									  if($row->display_cop == "1")
									  {
										  echo "<a href='javascript:CancelPopup(\"COP\",\"" .$row->id . "\")'  "; 
										  Yii::hideit();
										  echo "<a class='lmm_16_4 link_search'>ยกเลิกการแสดงผล</a>";
									  }else if($row->display_cop == "2")
									  {
										  echo "<a href='javascript:Approve(\"COP\",\"" .$row->id . "\")' " ;
										  Yii::hideit();
										  echo "<a class='lmm_16_6 link_search'>อนุมัติ</a> ";
									  }else if($row->display_cop == "3"){
										  echo "<a href='javascript:CancelPopup(\"COP\",\"" .$row->id . "\")' " . Yii::hideit() . " class='lmm_16_4 link_search'>ยกเลิกการแสดงผล</a>";
									  }
								  }else if($row->part=="COP")
								  {
									  if($row->display_cop == "1")
									  {
										  echo "<a href='javascript:CancelPopup(\"COP\",\"" .$row->id . "\")' " ;
										  Yii::hideit() ;
										  echo " class='lmm_16_4 link_search'>ยกเลิกการแสดงผล</a>";
									  }else if($row->display_cop =="0"){
										  echo "<a  href='javascript:Display(\"COP\",\"" .$row->id . "\")' " ;
										  Yii::hideit() ;
										  echo " class='lmm_16_4 link_search'>แสดงผล</a>";
									  }
								  }
							  }
							  
							  ?>
                              </td>
                              <td  style="white-space:nowrap" class="<?php echo $class;?>">
                              <br />
                              <?php
							  if($part=="COP"){
								  if($row->display_npa=="0" && $row->display_cop=="1")
								  {
									  echo "<a href='javascript:askToShowPopup(\"NPA\"," . $row->id . ")'  " ;
									  Yii::hideit() ;
									  echo " class='lmm_16_5 link_search'>ส่งคำขอให้แสดงผลในหน้า NPA</a>";
								  }else if($row->display_npa=="1")
								  {
								  		echo "กำลังแสดงผล";
								  }else if($row->display_npa=="2")
								  {
									  echo "รออนุมัติ";
								  }else if($row->display_npa=="3")
								  {
									  echo "รายการถูกยกเลิก";
								  }
							  }
							  /* NPA + NPA */
							  if($part=="NPA")
							  {
								  if($row->part=="COP"){
									  if($row->display_npa == "1")
									  {
										  echo "<a href='javascript:CancelPopup(\"NPA\",\"" .$row->id . "\")'  " ;
										  Yii::hideit() ;
										  echo " class='lmm_16_5 link_search'>ยกเลิกการแสดงผล</a>";
									  }else if($row->display_npa == "2")
									  {
										  echo "<a href='javascript:Approve(\"NPA\",\"" .$row->id . "\")'  " ;
										  Yii::hideit() ;
										  echo " class='lmm_16_6 link_search'>อนุมัติ</a> 
										  ";
									  }else if($row->display_npa == "3"){
										  echo "ยกเลิกแล้ว";
									  }
								  }else if($row->part=="NPA")
								  {
									  if($row->display_npa == "1")
									  {
										  echo "<a href='javascript:CancelPopup(\"NPA\",\"" .$row->id . "\")'  ";
										  Yii::hideit() ;
										  echo " class='lmm_16_4 link_search'>ยกเลิกการแสดงผล</a>";
									  }else if($row->display_npa =="0"){
										  echo "<a href='javascript:Display(\"NPA\",\"" .$row->id . "\")'  " ;
										  Yii::hideit();
										  echo " class='lmm_16_4 link_search'>แสดงผล</a>";
									  }
								  }
							  }
							  
							  ?>
                              </td>

                              <td class="<?php echo $class;?>" width="8%" align="center" valign="top">
                              <?php 
							  if($row->part== Yii::app()->user->getValue("part")){
								  ?>
                               <a href="<?php echo Yii::app()->createUrl('/Admin/Popup/' . $row->id);?> "  <?php Yii::hideit();?> class="lmm_16_1">
                              แก้ไข
                              </a>
                              <?php 
							  }
							  ?>
                              </td>
                              <td class="<?php echo $class;?>" width="8%" align="center" valign="top" >
                              <?php 
							  if($row->part== Yii::app()->user->getValue("part"))
							  {
								  ?>
                              <a href="javascript:deleteit('<?php echo $row->id;?>')"  <?php Yii::hideit();?> class="lmm_16_3">
                              ลบ
                              </a>
                              <?php 
							  }
							  ?>
                              </td>
                              
                            </tr>
                            <?php
						}
						?>
                            <tr>
                              <td class="" align="center" valign="top">&nbsp;</td>
                              <td width="23%" align="center" valign="top" >&nbsp;</td>
                              <td  width="23%" align="left" valign="top" >&nbsp;</td>
                              <td></td>
                           <td></td>
                        </tr>
                        <tr>
                          <td class="nparesult_table_content" align="center" colspan="7"><br />
                            <a href="<?php echo Yii::app()->createUrl('/Admin/Popup');?>" <?php Yii::hideit();?> class="lmm_16_1"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" width="52" height="25" /></a> &nbsp;
                           
                            </td>
                        </tr>
                      </table>
                 <?php $this->endWidget(); ?>
                    &nbsp;<br />
                    <br /></td>
                </tr>
              </table>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
        