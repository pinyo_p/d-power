﻿<?php 
$this->pageTitle="Admin Panel::Tambol"; 
?>
<script language="javascript">
$(function(){
$("#Tambol_district_id").change(function(){
											   $("#frm_district").submit();
											   });
		   });
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteTambol/');?>',{id:objId},function(data){
																							   $("#frm_district").submit();
																							   });
	}
}
function editit(objId)
{
	window.location.href='<?php echo Yii::app()->createUrl('/admin/UpdateTambol/?id=');?>' +  objId + '&continent_id=' + $("#frm_district #Tambol_continent_id").val() + '&province_id=' + $("#frm_district #Tambol_province_id").val() + '&district_id=' + $("#frm_district #Tambol_district_id").val();
}
function deleteitAll()
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
		$("#act").val('deleteAll');
		$("#frm_district").submit();
	}
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">ตำบล</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ข้อมูล<span class="txt_bold">ตำบล</span></div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td><?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_district',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input name="act" type="hidden" id='act'/>
                      <br />
                      <table width="320" border="0" align="center" cellpadding="2" cellspacing="3" class="form1">
                        <tr>
                          <td width="120"><div align="right"><span class="h1">เลือกภาค :</span></div></td>
                          <td><?php echo $form->dropDownList($model,'continent_id', CHtml::listData(Continent::model()->findAll(array('order' => 'thai_name ASC')), 'id', 'thai_name'), array('empty'=>'ภูมิภาค',	
																																										   'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getprovince/' ,
      			'success'=>'js:function(html){jQuery("#Tambol_province_id").html(html);}',),
	  )); ?></td>
                          </tr>
                        <tr>
                          <td width="120"><div align="right"><span class="h1">เลือกจังหวัด :</span></div></td>
                          <td><?php echo $form->dropDownList($model,'province_id', $param['province'], array('empty'=>'เลือกภูมิภาคก่อน',																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getdistrict/' ,
      			'success'=>'js:function(html){jQuery("#Tambol_district_id").html(html);}',),
	  )); ?></td>
                          </tr>
                        <tr>
                          <td width="120"><div align="right"><span class="h1">เลือกอำภอ :</span></div></td>
                          <td><?php echo $form->dropDownList($model,'district_id',$param['district'], array('empty'=>'เลือกจังหวัดก่อน',)); ?></td>
                          </tr>
                      </table>
                  
                  
                  </td>
                </tr>
                <tr>
                  <td><br />
                    <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3">
                     <tr style="background-color:#78CF2D;color:white;">
                          <td width="10px" align="center" valign="top" class="">เลือก</td>
                          <td width="10px" align="center" valign="top" class="txt_bold">ลำดับ</td>
                          <td align="center" valign="top" class="txt_bold">ชื่อตำบล</td>
                          <td width="6px" align="center" valign="top" class="txt_bold">แก้ไข</td>
                          <td width="6px" align="center" valign="top" class="txt_bold">ลบ</td>
                        </tr>
                        <?php
						$i=0;
						foreach($data as $row){
							$i++;
						?>
                    <tr>
                          <td class="rowa"  align="center" valign="top"><input type="checkbox" name="tambol_id[]" id="checkbox" value="<?php echo $row->id;?>" /></td>
                          <td align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i;?></span></td>
                          <td class="rowa" align="left" valign="top">
						  <?php echo $row->thai_name;?> / <?php echo $row->eng_name;?>
                          </td>
                          <td class="rowa" align="center" valign="top">
                          <a href="#" style="visibility:hidden;position:absolute;" class='lmm_12_2' onclick='editit("<?php echo $row->id;?>")'>แก้ไข</a></td>
                          <td class="rowa" align="center" valign="top"><a href="#"style="visibility:hidden;position:absolute;" class='lmm_12_3'  onclick="deleteit('<?php echo $row->id;?>')" >ลบ</a></td>
                        </tr>
                         
                   <?php
						}
						?>
                  </table>
                  <?php $this->endWidget(); ?>
                  </td>
                </tr>
                 <tr>
                      <td class="nparesult_table_content" align="center"><br />
                       <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/UpdateTambol" style="visibility:hidden;position:absolute;" class='lmm_12_1'><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" width="52" height="25" /></a>
                         &nbsp;&nbsp;<a href="javascript:deleteitAll()" style="visibility:hidden;position:absolute;" class='lmm_12_3'><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" width="52" height="25" /></a>&nbsp; <a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a></td>
                    </tr>
                    <?php 
					if($model->province_id>0){
					$form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_tambol',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                <tr>
                  <td><br />
                    <br />
                     
                    </td>
                </tr>
                <tr>
                  <td><br />
                    <br />
                    <br />
                   
                    <br />
                    <br />
                    <br />
                    <br /></td>
                </tr>
                 <?php $this->endWidget(); 
					}
				?>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
        