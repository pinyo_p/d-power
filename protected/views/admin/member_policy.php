<?php
$this->pageTitle = Yii::app()->name . ' - ประโยชน์จากการลงทะเบียน';
$b = "เนื้อหา";
switch ($model->content_code) {
    case "MemberPolicy":$b = 'ประโยชน์จากการสมัครสมาชิก';
        break;
    case "History":$b = 'ประวัติของเรา';
        break;
    case "ContactUs":$b = 'ติดต่อเรา';
        break;
    case "Policy":$b = 'เงื่อนไขการใช้งานเว็บไซต์';
        break;
    case "Protection":$b = 'การป้องกันข้อมูล';
        break;
    case "Shiping":$b = 'การจัดส่งสินค้า';
        break;
    case "Sitemap":$b = 'Site map';
        break;
    case "Footer":$b = 'Footer';
        break;
    case "BrandLink":$b = 'ตั้งค่า Brand Link';
        break;
    case "Shipping":$b = 'การจัดส่งสินค้า';
        break;
    case "Training":$b = 'Training';
        break;
    case "repair":$b = 'Repair';
        break;
    case "faq":$b = 'FAQ';
        break;
    case "Banner":$b = 'แบนเนอร์';
        break;
    case "Intro":$b = 'Intro Page';
        break;
    case "SalePolicy":$b = 'ข้อกำหนดและเงื่อนไขการขาย';
        break;
    case "credit":$b = 'คำขอเปิดบัญชีเครดิต';
        break;
    case "reward":$b = 'แลกคะแนนรับของรางวัล';
        break;
    case "quotationform":$b = 'ใบเสนอราคาและใบสั่งซื้อ';
        break;
    case "promotion":$b = 'โปรโมชั่น';
        break;
}
$this->breadcrumbs = $b;
?>
<script type="text/javascript" language="javascript">
    $(function () {
        var roxyFileman = '<?php echo Yii::app()->request->baseUrl; ?>/fileman/pagefile.html';
        $("#tabs").tabs();
        CKEDITOR.replace('Content_content_1', {
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
        CKEDITOR.replace('Content_content_2', {
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
    });

</script>

<div class="form">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'content-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    ));
                    ?>
    <br  /><br  /><br  />
    <center>
        <div style="width:800px;  text-align:left">

            <div id="tabs" >
                <ul>
                    <li><a href="#tabs-1">ภาษาไทย</a></li>
                    <li><a href="#tabs-2">ภาษาอังกฤษ</a></li>
                </ul>
                <div id="tabs-1">
                    <?php
                    Yii::import('ext.ckeditor.*');
                    $this->widget('application.extensions.ckeditor.CKEditor', array(
                        'model' => $model,
                        'attribute' => 'content_2',
                        'language' => 'en',
                        'editorTemplate' => 'full',
                        'height' => '800px',
                    ));
                    ?>
                </div>
                <div id="tabs-2">
<?php
Yii::import('ext.ckeditor.*');
$this->widget('application.extensions.ckeditor.CKEditor', array(
    'model' => $model,
    'attribute' => 'content_1',
    'language' => 'en',
    'editorTemplate' => 'full',
    'height' => '600px',
));
?>
                </div>

            </div>
            <br /><br />
            <center>
                <input type='image' src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />
            </center>
        </div>

    </center>
<?php $this->endWidget(); ?>
</div><!-- form -->
