<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายการการจัดส่งสินค้า';
?>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteProduct/", {
                id: objId
            }, function (data) {
                if (data.indexOf("OK") > -1)
                    $("#productlist-form").submit();
                else
                    alert(data);
            });
        }
    }
    function deleteAll()
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $("#act").val("deleteall");
            $("#productlist-form").submit();
            $("#act").val("search");
        }
    }
    function change_status(objId, objStatus)
    {
        str_status = $("#" + objStatus).val();
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/changeOrderStatus/1", {
            id: objId,
            status: str_status,
        }, function (data) {
            if (data == "OK")
                $("#productlist-form").submit();
            else if (data == "DENIED")
                alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
            else
                alert(data);
        });
    }

    function searchit()
    {
        $("#productlist-form").submit();
    }

    function exportdata() {
        var fullname = $('#MemberOrder_fullname').val().trim();
        var company = $('#MemberOrder_company_name').val().trim();
        var address = $('#MemberOrder_address').val().trim();
        var province = $('#MemberOrder_province').val().trim();
        var status = $('#MemberOrder_status').val();
        var dtstart = $('#MemberOrder_start_date').val();
        var dtend = $('#MemberOrder_end_date').val();
        window.open("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ExportOrderList?fullname=" + fullname + "&company=" + company + "&address=" + address + "&province=" + province + "&status=" + status + "&dtstart=" + dtstart + "&dtend=" + dtend, "_blank");
    }

    function gotoPage(objPage)
    {
        $("#page").val(objPage);
        $("#productlist-form").submit();
    }
    function showDetail(objId)
    {
        window.open('<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/OrderShippingDetail/' + objId, 'member_detail', 'width=600,height=400');
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'productlist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4">ค้นหาการจัดส่งสินค้า</span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="right">ชื่อลูกค้า :</th>
                    <td ><?php echo $form->textField($model, 'fullname'); ?></td>
                    <th align="right">ชื่อบริษัท :</th>
                    <td ><?php echo $form->textField($model, 'company_name'); ?></td>
                </tr>
                <tr>
                    <th align="right">ที่ยู่ :</th>
                    <td ><?php echo $form->textField($model, 'address'); ?></td>
                    <th align="right">จังหวัด :</th>
                    <td > <?php
                        echo $form->dropDownList($model, 'province', CHtml::listData(Provinces::model()->findAll(array('order' => 'thai_name ASC')), 'province_id', 'thai_name'), array('empty' => 'เลือกจังหวัด',
                        ));
                        ?></td>
                </tr>
                <tr>
                    <th align="right">สถานะ :</th>
                    <td ><?php
                        echo $form->dropDownList($model, 'status', array('4' => 'รอการจัดส่ง', '5' => 'จัดส่งแล้ว', '6' => 'จัดส่งไม่สำเร็จ', '7' => 'ยกเลิกการจัดส่ง'), array('empty' => ' ทุกสถานะ',
                        ));
                        ?></td>
                    <th align="right">วันที่ซื้อ :</th>
                    <td >
                        <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
                            'name' => 'MemberOrder[start_date]',
                            'value' => $model->start_date,
                            'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat' => 'yy-mm-dd',
                                'altFormat' => 'yy-mm-dd',
                                'changeMonth' => 'true',
                                'changeYear' => 'true',
                                'showOn' => "both",
                                'yearRange' => "'" . date("Y") - 70 . ':' . date("Y") - 10 . "'",
                                'buttonImage' => Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",
                                'buttonImageOnly' => "true",
                            ),
                            'htmlOptions' => array(
                                'style' => 'height:20px; width:70px;'
                            ),
                                )
                        );
                        ?> - 
                        <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model,
                            'name' => 'MemberOrder[end_date]',
                            'value' => $model->end_date,
                            'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat' => 'yy-mm-dd',
                                'altFormat' => 'yy-mm-dd',
                                'changeMonth' => 'true',
                                'changeYear' => 'true',
                                'showOn' => "both",
                                'yearRange' => "'" . date("Y") - 70 . ':' . date("Y") - 10 . "'",
                                'buttonImage' => Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",
                                'buttonImageOnly' => "true",
                            ),
                            'htmlOptions' => array(
                                'style' => 'height:20px; width:70px;'
                            ),
                                )
                        );
                        ?>
                    </td>
                </tr>
                <tr>

                    <td  colspan="4" align="center">
                        <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_search.png" width="55" height="26" />
                        <a href="#" onclick="exportdata()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_export.png" height="26" style="border:none;" /></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center"> <br /><br /><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>

    <tr>
        <td align="center" width="100%" class="tabletest">




            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">No.</th>
                    <th  width="120">ชื่อผู้ติดต่อ</th>
<!--                    <th >ชื่อบริษัท</th>-->
<!--                    <th >ที่อยู่</th>-->
                    <th width="200">หมายเหตุ</th>
                    <th style="text-align:center;">รายละเอียดสั่งซื้อ</th>                    
                    <th  width="120">สถานะ</th>
                    <th width="80">วันที่สั่งซื้อ</th>
                </tr>
                <?php
                $i = ($model->current_page) * $model->display_perpage;

                foreach ($data as $row) {
                    ?>
                    <tr>
                        <td align="center">
    <?php echo ++$i; ?> 
                        </td>
                        <td align="left">
                            <a href='javascript:showDetail("<?php echo $row->id; ?>")'><?php echo $row->fullname; ?> </a>
                        </td>
<!--                        <td align="left">
    <?php echo $row->company_name; ?> 
                        </td>-->
<!--                        <td align="left">
    <?php echo $row->address; ?>   <?php echo $row->Provinces['thai_name']; ?>  <?php echo $row->zipcode; ?> 
                        </td>-->
                        <td><?php echo $row->remark; ?></td>
                        <td align="center">
                            <a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ProductShippingList/<?php echo $row->id; ?>'>
                                รายละเอียดสั่งซื้อ
                            </a>
                        </td>
                        
                        <td align="center" valign="middle">
                            <table style="border:none;"><tr valign="top"><td style="border:none; text-align:right">
                                        <select name="status_<?php echo $row->id; ?>" id="status_<?php echo $row->id; ?>">

                                            <option value="4" <?php echo ($row->status == "4" ? " selected='selected'" : ""); ?>>รอการจัดส่ง</option>
                                            <option value="5" <?php echo ($row->status == "5" ? " selected='selected'" : ""); ?>>จัดส่งแล้ว</option>
                                            <option value="6" <?php echo ($row->status == "6" ? " selected='selected'" : ""); ?>>จัดส่งไม่สำเร็จ</option>
                                            <option value="7" <?php echo ($row->status == "7" ? " selected='selected'" : ""); ?>>ยกเลิกการจัดส่ง</option>

                                        </select>
                                    </td><td style="border:none;">
                                        <a href='javascript:;' onclick="change_status('<?php echo $row->id; ?>', 'status_<?php echo $row->id; ?>')">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />
                                        </a>
                                    </td>
                                </tr>
                            </table>



                        </td>

                        <td  align="center">
    <?php echo substr($row->create_date, 0, 10); ?>
                        </td>

                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
            if ($param['max_page'] > 1) {
                ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                        <td style="border:none;"><div class="nav_page">
                                <ul>
                                    <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
                                    <?php
                                    $start_page = ($page > 2 ? $page - 2 : 1);
                                    $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
                                    if ($end_page <= 5)
                                        $end_page = 5;
                                    else if (($end_page - $page) < 5)
                                        $start_page = ($end_page - 4);
                                    if ($end_page > $param['max_page'])
                                        $end_page = $param['max_page'];
                                    for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                                        $class = "";
                                        if (($i + 1) == $page)
                                            $class = '  class="navselect"';
                                        else
                                            $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                                        ?>
                                        <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                        <?php
                                    }
                                    ?>
                                    <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                                </ul>
                            </div></td>
                        <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                            <table width="200"  style="border:none;"><tr><td style="border:none;">
                                        <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                        <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                        <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/go.png" width="39" height="22" /></a></td></tr></table>


                        </td>
                    </tr>
                </table>
                <?php
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="center">



            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>