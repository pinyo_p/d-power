<?php

$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'กลุ่มบริษัทในเครือ';
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<table width="100%">
    <tr>
        <td align="center"><h3 class="underline"><?php echo (isset($_GET["id"]))?"แก้ไข":"เพิ่ม"; ?>ข้อมูล</h3></td>
    </tr>
    <tr>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="add_data">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th width="200" align="right">ชื่อบริษัท (ภาษาไทย) :</th>
                    <td><?php echo $form->textField($model, 'name_th',array('style'=>'width:400px;')); ?>
                    <?php echo $form->error($model,'name_th'); ?></td>
                </tr>
                <tr>
                    <th align="right">ชื่อบริษัท (ภาษาอังกฤษ) :</th>
                    <td><?php echo $form->textField($model, 'name_en',array('style'=>'width:400px;')); ?>
                    <?php echo $form->error($model,'name_en'); ?></td>
                </tr>
                <tr>
                    <th align="right">ที่อยู่บริษัท (ภาษาไทย) :</th>
                    <td><?php echo $form->textArea($model, 'address_th',array('style'=>'width:400px;height:50px;')); ?>
                    <?php echo $form->error($model,'address_th'); ?></td>
                </tr>
                <tr>
                    <th align="right">ที่อยู่บริษัท (ภาษาอังกฤษ) :</th>
                    <td><?php echo $form->textArea($model, 'address_en',array('style'=>'width:400px;height:50px;')); ?>
                    <?php echo $form->error($model,'address_en'); ?></td>
                </tr>
                <tr>
                    <th align="right">เบอร์โทร :</th>
                    <td><?php echo $form->textField($model, 'phone_no', array('style' => 'width:400px')); ?>
                    <?php echo $form->error($model,'phone_no'); ?>
                    </td>
                </tr>
                <tr>
                    <th align="right">Fax :</th>
                    <td><?php echo $form->textField($model, 'fax_no', array('style' => 'width:400px')); ?>
                    <?php echo $form->error($model,'fax_no'); ?></td>
                </tr>
                <tr>
                    <th align="right">Email :</th>
                    <td><?php echo $form->textField($model, 'email', array('style' => 'width:400px')); ?>
                    <?php echo $form->error($model,'email'); ?></td>
                </tr>
                <tr>
                    <th align="right">Website :</th>
                    <td><?php echo $form->textField($model, 'website', array('style' => 'width:400px')); ?>
                    <?php echo $form->error($model,'website'); ?></td>
                </tr>

                <tr>
                    <th align="right">Latitude :</th>
                    <td ><?php echo $form->textField($model, 'latitude', array('style' => 'width:400px')); ?>
                    <?php echo $form->error($model,'latitude'); ?></td>
                </tr>
                <tr>
                    <th align="right">Longitude :</th>
                    <td><?php echo $form->textField($model, 'longitude', array('style' => 'width:400px')); ?>
                    <?php echo $form->error($model,'longitude'); ?></td>
                </tr>
                <tr>
                    <th align="right">จัดเรียง :</th>
                    <td><?php echo $form->textField($model, 'sort_order', array('style' => 'width:50px')); ?></td>
                </tr>
                <tr>
                    <th align="right">รูปภาพ :</th>
                    <td>
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/partner/' . $model->logo;
                        if (file_exists(Yii::app()->basePath . '/../images/partner/' . $model->logo) && $model->logo != "") {
                            ?>
                            <img src="<?php echo $file; ?>" height="83" />
                            <?php
                        }
                        ?>
                            <div><input type="file" name="logo" id='logo' /></div></td>
                </tr>
                <tr>
                    <th align="right">แผนที่ :</th>
                    <td >
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/partner/' . $model->map;
                        if (file_exists(Yii::app()->basePath . '/../images/partner/' . $model->map) && $model->map != "") {
                            ?>
                            <img src="<?php echo $file; ?>" height="83" />
                            <?php
                        }
                        ?>
                            <div><input type="file" name="map" id='map' /></div></td>
                </tr>
            </table>
            <br />
            <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/partnerlist'; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>