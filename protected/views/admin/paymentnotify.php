<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายละเอียดการชำระเงิน';
?>

<style>
    th{
        text-align:right !important;
    }
    td{text-align: left !important;}
</style>

<div style="padding:50px 0px 0px 50px;" class="tabletest">
    <?php if(count($param['payment_notify'])>0){ ?>
    <?php foreach($param['payment_notify'] as $row){ ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th width="30%" height="25" class="txt_bold hline">เลขที่ใบสั่งซื้อ&nbsp;&nbsp;</th>
            <td height="25" align="right" class="txt_price hline">
               <?php echo substr("000000000" . ($row->order_id), -9);?>
            </td>
        </tr>
        <tr>
            <th width="30%" height="25" class="txt_bold hline">ยอดเงินที่โอน&nbsp;&nbsp;</th>
            <td height="25" align="right" class="txt_price hline">
               <?php echo number_format(Yii::currencyAmount($row->transfer_amount), 2, ".", ",");?> บาท
            </td>
        </tr>
        <tr>
            <th width="30%" height="25" class="txt_bold hline">วันเวลาที่โอน&nbsp;&nbsp;</th>
            <td height="25" align="right" class="txt_price hline">
              <?php echo $row->transfer_date.' '.$row->transfer_time; ?>
            </td>
        </tr>
        <tr>
            <th width="30%" height="25" class="txt_bold hline">โอนเช้าบัญชี&nbsp;&nbsp;</th>
            <td height="25" align="right" class="txt_price hline">
              <?php
                $mBank = Bank::model()->findByPk($row->bank_id);
                echo $mBank->bank_name.' - '.$mBank->acc_type.' (เลขที่บัญชี '.$mBank->acc_no.')';
              ?>
            </td>
        </tr>
        <tr>
            <th width="30%" height="25" class="txt_bold hline">ธนาคาร/สาขาต้นทางที่โอน&nbsp;&nbsp;</th>
            <td height="25" align="right" class="txt_price hline">
              <?php echo $row->member_bank; ?>
            </td>
        </tr>
        <tr>
            <th width="30%" height="25" class="txt_bold hline">หมายเหตุ/เพิ่มเติม&nbsp;&nbsp;</th>
            <td height="25" align="right" class="txt_price hline">
              <?php echo $row->remark; ?>
            </td>
        </tr>
        <tr>
            <th width="30%" height="25" class="txt_bold hline" valign="top">หลักฐาน/รูปใบสลิป&nbsp;&nbsp;</th>
            <td height="25" align="right" class="txt_price hline">
             <?php 
             if($row->transfer_document != '')
                 echo "<img src='".Yii::app()->request->baseUrl."/images/payment/".$row->transfer_document."' style='width:400px;' />";
             ?>
            </td>
        </tr>
        
    </table>
    <br/>
    <br/>
    <?php } ?>
    <?php }else{ ?>
    <div style="text-align:center;">-- ไม่มีรายการชำระเงิน --</div>
    <?php } ?>
    <br/><br/>
    <div style="text-align:center;"><a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/OrderList'>
                                            <img id="btnBack" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png"/>
                                        </a></div>
    <br/><br/><br/>
</div>