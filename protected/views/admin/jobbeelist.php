<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'ผู้สมัครงาน';
?>
<script language="javascript">
function changeit(obj,objId)
{
	if($(obj).val()!=""){
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/ChangeJobbedStatus/',{
								id:objId,
								status:$(obj).val(),
								},function(data){
									if(data=="OK")
									{
										location.reload();
									}else{
										alert(data);
									}
								});
	}
}

function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DeleteJobbee/',{id:objId},function(data){
																											  if(data=="OK")
																											  	location.reload();
																											  });
			   }
}
function deleteitAll()
	{
	
		if(confirm("ต้องการลบรายการที่เลือกใช่หรือไม่")){
			$("#act").val('deleteAll');
			$("#frm_notice").submit();
		}
	}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

            <tr>
        <td align="center"><br />
<span class="text4"><?php echo $this->breadcrumbs;?></span><br />
&nbsp;<br /></td>
      </tr>
            <tr>
              <td ><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                  <td class="tabletest"> <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jobbee',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                      <br />
                      <table width="700" border="0" align="center" cellspacing="1" >
                        <tr>
                          <th width="120"><div align="right"><span class="h1">ตำแหน่ง :</span></div></th>
                          <td width="280">
                          <?php echo $form->dropDownList($model,'jobid', CHtml::listData(Joblist::model()->findAll(array('order' => 'position ASC')), 'id', 'position'), array('empty'=>'เลือกตำแหน่ง',	 )); ?>
                          </td>
                          <th width="120"><div align="right"><span class="h1">คำค้น :</span></div></th>
                          <td><?php echo $form->textField($model,'key_word'); ?></td>
                        </tr>
                        <tr>
                          <th width="120"><div align="right"><span class="h1">วันที่สมัคร :</span></div></th>
                          <td width="280"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Jobbee[start_date]',
					'value'=>$model->start_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td>
                          <th width="120"><div align="right"><span class="h1">ถีง :</span></div></th>
                          <td width="280"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Jobbee[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td>
                        </tr>
                        <tr>
                          <th width="120"><div align="right"><span class="h1">สถานะ :</span></div></th>
                          <td width="280" colspan="2">
                          <?php
echo $form->dropDownList($model, 'status',array('' => '-- เลือกสถานะ --','0'=>'รอเรียกสัมภาษณ์','1'=>'สัมภาษณ์แล้ว','2'=>'รับเข้าทำงานแล้ว','3'=>'ไม่รับ'));
																						  ?>
                          </td>
                         
                          <td></td>
                        </tr>
                        <tr>
                          <td width="120" colspan="4" align="center">
                          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" alt="Submit button"></td>
                        </tr>

                      </table>
                  <?php $this->endWidget(); ?>
                  </td>
                </tr>
                <tr>
                  <td class="tabletest">
                  <?php 
					
					$form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_notice',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input name="act" type="hidden" id='act'/>
<center>
<br /><br />
                  <table width="100%" border="0" cellspacing="3" cellpadding="3" style="width:99%">
                    <tr>
                          <th width="36" align="center" valign="top" class="">เลือก</th>
                          <th width="37" align="center" valign="top" class="txt_bold">ลำดับ</th>
                          <th width="69" align="center" valign="top" class="txt_bold">รูป</th>
                          <th width="100" align="center" valign="top" class="txt_bold">ชื่อ</th>
                          <th width="85" align="center" valign="top" class="txt_bold">อายุ</th>
                          <th width="24" align="center" valign="top" class="txt_bold">เพศ</th>
                          <th width="92" align="center" valign="top" class="txt_bold">เบอร์โทร</th>
                          <th width="115" align="center" valign="top" class="txt_bold">Email</th>
                          <th width="135" align="center" valign="top" class="txt_bold">ตำแหน่ง</th>
                          <th width="115" align="center" valign="top" class="txt_bold">วันที่สมัคร</th>
                          <th width="126" align="center" valign="top" class="txt_bold">สถานะ</th>
                          <th width="19" align="center" valign="top" class="txt_bold">ลบ</th>
                    </tr>
                      
                    
                      <?php
					  $i=1;
					  foreach($data as $row){
					  ?>
                        <tr>
                          <td class="rowa" width="36" align="center" valign="top">
                          <input type="checkbox" name="jobbee_id[]" id="checkbox" value="<?php echo $row->id;?>" /></td>
                          <td width="37" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i++;?></span></td>
                          <td class="rowa"  width="69" align="center" valign="top">
                          <?php if($row->apply_image!=""){
						  		?>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/attach/jobs/<?php echo $row->apply_image;?>" width="70" height="64" />
                                <?php
						  }else{
						  ?>
                          <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/nopic.jpg" width="70" height="64" />
                          <?php }?>
                          <?php
						  if($row->resume != "")
						  {
							  ?>
                              <a href='<?php echo Yii::app()->request->baseUrl; ?>/attach/jobs/<?php echo $row->resume;?>' target="_blank">Resume</a>
                              <?php
						  }
						  ?>
                          </td>
                          <td class="rowa" width="160" align="center" valign="top"><?php echo $row->fullname;?></td>
                          <td class="rowa" width="51" align="center" valign="top"><?php echo $row->age;?></td>
                          <td class="rowa" width="50" align="center" valign="top"><?php echo ($row->sex=="1"?"Male":"Female");?></td>
                          <td class="rowa" width="78" align="center" valign="top"><?php echo $row->phoneno;?></td>
                          <td class="rowa" width="77" align="center" valign="top"><?php echo $row->email;?></td>
                          <td class="rowa" width="135" align="center" valign="top"><?php echo $row->jobs['position'];?></td>
                          <td class="rowa" width="115" align="center" valign="top"><?php echo $row->create_date;?></td>
                          <td class="rowa" width="126" align="center" valign="top">
                          <select name="select3" class="textb" id="status_<?php echo $row->id;?>" onchange='changeit(this,"<?php echo $row->id;?>")' style="width:120px">
                            <option value="">- เลือกสถานะ -</option>
                            <option value="0" <?php echo ($row->status==0? " selected='selected' ": "")?>>รอเรียกสัมภาษณ์</option>
                            <option value="1" <?php echo ($row->status==1? " selected='selected' ": "")?>>สัมภาษณ์แล้ว</option>
                            <option value="2" <?php echo ($row->status==2? " selected='selected' ": "")?>>รับเข้าทำงานแล้ว</option>
                            <option value="3" <?php echo ($row->status==3? " selected='selected' ": "")?>>ไม่รับ</option>
                          </select></td>
                          <td class="rowa" width="19" align="center" valign="top"><a href='javascript:deleteit("<?php echo $row->id;?>")'>ลบ</a></td>
                        </tr>
                       <?php
					  }
					  ?>
                      </table>
                      </center>
                      <?php $this->endWidget(); ?>
                      
                      </td>
                    </tr>
                    <tr>
                      <td class="nparesult_table_content" align="center"><br />
                       &nbsp;<a href="javascript:deleteitAll()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a> </td>
                    </tr>
                  </table>
                  
                  </td>
                </tr>
              </table>
              
              