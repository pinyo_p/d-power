﻿<?php 
$this->pageTitle="Admin Panel::Manage Property"; 
function g($val,$caption)
{
	return (trim($val)!=""?$caption . " " . $val:"");
}
function g2($val,$caption)
{
	return (trim($val)!="" && $val!="0"?$val . $caption:"");
}
function getDirection($sortby,$model)
{
	$dir = 1;
	if($sortby==$model->sort_by && $model->sort_dir == 1)
		$dir = 2;
	return $dir;
}
function getDirectionIcon($sortby,$model)
{
	$img = "";
	if($sortby==$model->sort_by){
		if($model->sort_dir=="2")
		{
			$img = "sort_botton_down.png";
		}else{
			$img = "sort_botton_up.png";
		}
	}
	if($img != "")
	{
		$img = "<img src='" . Yii::app()->request->baseUrl . "/images/$img' />";
	}
	return $img;
}
?>

<script language="javascript">
$(function() {
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo Yii::app()->request->baseUrl;?>/images/date.jpg",
			buttonImageOnly: true, 
			dateFormat:"yy-mm-dd",
		});
	});
function sethighligh(objFlag,objId)
{
	$.post("<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/updatehighlight/" + objId + "/" + objFlag,
								function(data)
							   {
								   location.reload();
							   });
}
function settooutter(objFlag,objId)
{
	$.post("<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/updateoutter/" ,{
								id:objId,
								flag:objFlag
								},
								function(data)
							   {
								   location.reload();
							   });
}
function searchit()
{
	if($("#Property_start_price").val()!="" && $("#Property_end_price").val()!="" && ($("#Property_start_price").val()>$("#Property_end_price").val()  ))
	{
		alert('ราคาเริ่มต้นต้องน้อยกว่าราคาสิ้นสุด');
	}else{
		$("#frm_search").submit();
	}
}
function gotoPage(objPage)
{
	$("#frm_search #page").val(objPage);
	//alert(objPage);
	$("#frm_search").submit();
}
function sortit(objId,objDir)
{
	$("#Property_sort_by").val(objId);
	$("#Property_sort_dir").val(objDir);
	$("#frm_search").submit();
}
function deleteit(objId)
{
	if(confirm('คุณต้องการลบรายการนี้ใช่หรือไม่?')){
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/deleteproperty/',{id:objId},function(data){
																												if(data=="OK")
																													$("#frm_search").submit();
																												else
																													alert(data);
																										});
	}
	
}
function doitall()
{
	if($("#cmd").val()!=""){

		if(confirm('คุณต้องการดำเนินการกับรายการที่เลือกทั้งหมดใช่หรือไม่?')){
			$("#act").val('doitall');
			$("#frm_search").submit();
		}
	}
}
</script>
<style type="text/css">
.hd td a{
	color:white;
	text-decoration:none;
}
.hd a{
	color:white;
}
</style>
<div align="center">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_search',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input type="hidden" name="act" id="act" />
<div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%" style="text-align:left" ><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">ค้นหารายการทรัพย์สิน<?php if($param['type']=="delete") echo "ที่ลบแล้ว"; ?></a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ค้นหารายการทรัพย์สิน<?php if($param['type']=="delete") echo "ที่ลบแล้ว"; ?></div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table>

<table width="100%">
<tr>
                  <td class="search_header"></td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="search_row">
                      <table width="99%" border="0"  cellpadding="0" cellspacing="0" class="Searchnpa">
                        <tr>
                          <td width="30%" align="left" height="38"><span class="txt_bold">รหัสทรัพย์สิน :</span>
                            <?php echo $form->textField($model,'product_code',array('style'=>"width:185px",'class'=>"txt_bold")); ?></td>
                          <td width="16%" align="right"><span class="txt_bold">ราคาประกาศขายขั้นต่ำ :&nbsp;</span></td>
                          <td width="16%" align="left"><?php echo $form->textField($model,'start_price',array('style'=>'width:100px;','class'=>"txt_bold")); ?>
                            &nbsp;บาท</td>
                          <td width="38%" rowspan="3">
                          <table class="txt_bold"><tr>
                          <td  align="right">ภาค :</td><td align="left"><?php echo $form->dropDownList($model,'continent', CHtml::listData(Continent::model()->findAll(array('order' => 'thai_name ASC')), 'id', 'thai_name'), array('empty'=>'ภูมิภาค',	
																																										   'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getprovince/' ,
      			'success'=>'js:function(html){jQuery("#Property_province").html(html);jQuery("#Property_district").html("<option value=0>เลือกจังหวัด</option>");}',),
	  )); ?></td>
      <td align="right">จังหวัด :</td><td align="left">
      <?php echo $form->dropDownList($model,'province', $param['province'], array('empty'=>'ทุกจังหวัด',																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getdistrict/' ,
      			'success'=>'js:function(html){jQuery("#Property_district").html(html);jQuery("#Property_tambol").html("<option value=0>เลือกอำเภอ</option>");}',),
	  )); ?>
      </td></tr><tr>
      <td align="right">อำเภอ :</td><td align="left">
                          <?php echo $form->dropDownList($model,'district',$param['district'], array('empty'=>'ทุกอำเภอ',
																																											   																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/gettambol/' ,
      			'success'=>'js:function(html){jQuery("#Property_tambol").html(html);}',),)); ?>
                          </td><td>ตำบล
 :                         </td><td align="left">
                           <?php echo $form->dropDownList($model,'tambol', $param['tambol'], array('empty'=>'ทุกตำบล')); ?>
                            </td></tr><tr><td align="right">ถนน :</td><td align="left">
                           <?php echo $form->textField($model,'road',array('style'=>"width:110px",)); ?> </td>
                            </tr>
      
      </table></td>
                        </tr>
                        <tr>
                          <td width="30%" align="left"><span class="txt_bold">ประเภททรัพย์สิน : </span>
                             <?php echo $form->dropDownList($model,'product_type', CHtml::listData(ProductGroup::model()->findAll(array('order' => 'product_group ASC')), 'id', 'product_group'), array('empty'=>'ทุกประเภท',																																												  				 
	  )); ?></td>
                          <td width="16%" align="right"><span class="txt_bold">ถึง&nbsp;</span></td>
                          <td width="16%" align="left"><?php echo $form->textField($model,'end_price',array('style'=>'width:100px;')); ?> 
                            &nbsp;บาท</td>
                          
                        </tr>
                        <tr>
                          <td width="30%" align="left"><p><span class="txt_bold">สถานะประกาศขาย :</span>
                          <?php echo $form->dropDownList($model,'status',array(1=>'ซื้อตรง',2=>'ประมูล',3=>'ขายแล้ว',4=>'รออนุมัติ',5=>'อนุมัตแล้วรอประกาศ'), array('empty'=>'- ทุกประเภท -',																																												  				 
	  )); 
						  ?>
                          </p></td>
                          <td width="16%" align="right"><span class="txt_bold">วันที่ประกาศขายตั้งแต่ :&nbsp;</span></td>
                          <td width="16%" align="left"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
					'value'=>$model->start_date,
                    'name'=>'Property[start_date]',
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'width:70px'
    ),)); ?></td>
                          
                        </tr>
                        <tr>
                          <td width="30%" align="left"><span class="txt_bold">คำค้นหาที่เกี่ยวข้อง :</span>
                            <?php echo $form->textField($model,'key_word',array('style'=>'width:153px;')); ?></td>
                          <td width="16%" align="right"><span class="txt_bold">ถึง&nbsp;</span></td>
                          <td width="16%" align="left"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
                    'name'=>'Property[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'width:70px'
    ),)); ?></td>
                          <td width="38%"><span class="txt_bold">ขนาดพื้นที่ :</span>&nbsp; จำนวน&nbsp;&nbsp;
                            <?php echo $form->textField($model,'area_start',array('style'=>'width:80px;')); ?> 
                            &nbsp;&nbsp;ถึง&nbsp;&nbsp;
                            <?php echo $form->textField($model,'area_end',array('style'=>'width:80px;')); ?> 
                            &nbsp;ไร่</td>
                        </tr>
                        
                        <tr>
                          <td width="30%">&nbsp;</td>
                          <td width="30%" colspan="2">&nbsp;</td>
                          <td width="38%" align="right">
                          <a href="<?php echo Yii::app()->createUrl('/admin/NewProperty');?>"  style="visibility:hidden;position:absolute;" class='lmm_34_1' ><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" width="52" height="25" /></a>
                          <a href="javascript:;" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/npa_search.png" alt="" width="62" height="25" /></a> <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Admin/PropertyList/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/npa_cancle.png" alt="" width="62" height="25" /></a></td>
                        </tr>
                      </table></td>
                      
                    </tr>
                    <tr>
                      <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td><span class="txt_bold"><span class="txt_pink"><br />
                        ผลการค้นหา :</span> <?php echo $param['row_amount'];?> รายการ</span></td>
                          <td align="right" class="txt_bold txt_pink">จำนวนที่แสดงผลต่อหน้า : 
                            <?php echo $form->textField($model,'display_perpage',array('style'=>'width:53px;')); ?>
                            เรียงตาม : 
                            <?php echo $form->dropDownList($model,'sort_by',array('0'=>'เลือกการเรียงลำดับ',1=>'รหัสทรัพย์สิน',2=>'ประเภททรัพย์สิน',3=>'จังหวัด',4=>'ราคาประกาศขายขั้นต่ำ',5=>'สถานะประกาศขาย')); 
						  ?>
                            จาก
                            <?php echo $form->dropDownList($model,'sort_dir',array(1=>'น้อยไปหามาก',2=>'มากไปหาน้อย')); 
						  ?><a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/go.png" width="39" height="22" /></a>
                          </td>
                          <td align="right" class="txt_bold txt_pink">
                          </td>
                        </tr>
                      </table></td>
                    </tr>
                    </table>
                    <br />
                    <br />

<table style="text-align:left"  width="100%">
<tr>
<td>
<table bgcolor="#78CF2D" width="100%" cellpadding="10" cellspacing="1">
<tr class="hd" style="background-color:#78CF2D;color:white;font-weight:bold" valign="top">
<?php 
if($param['type']=="list"){
	?>
<td>
<input type="checkbox" name="check_all" id='check_all' />
</td>
<?php
}?>
<td>รูปประกอบ</td>
<td width="70px">
<a href='javascript:sortit("1","<?php echo getDirection(1,$model);?>")'>รหัสทรัพย์<br /><?php echo getDirectionIcon(1,$model);?></a>
</td>
<td><a href='javascript:sortit("2","<?php echo getDirection(2,$model);?>")'>ประเภททรัพย์สิน<br /><?php echo getDirectionIcon(2,$model);?></a></td>
<td>
ที่ตั้ง
</td>
<td><a href='javascript:sortit("3","<?php echo getDirection(3,$model);?>")'>จังหวัด<br /><?php echo getDirectionIcon(3,$model);?></a></td>
<td>เนื้อที่</td>
<td><a href='javascript:sortit("4","<?php echo getDirection(4,$model);?>")'>ราคาขั้นต่ำ<br /><?php echo getDirectionIcon(4,$model);?></a></td>
<td>
<a href='javascript:sortit("5","<?php echo getDirection(5,$model);?>")'>สถานะ<br /><?php echo getDirectionIcon(5,$model);?></a>
</td>
<td width="120px">

<?php
if($param['type']=="list"){
?>
<select name="command" id='cmd'>
<option value="">โปรดเลือก</option>
<option value="approve">อนุมัติ</option>
<option value="delete">ลบ</option>
</select><input type="button" value="บันทึก" onclick="doitall()" />
<?php
}
?>

</td>
</tr>

<?php
foreach($data2 as $row)
{
	?>
    <tr bgcolor="white" style="vertical-align:top">
    <?php 
if($param['type']=="list"){
	?>


	<td>
		<input type="checkbox" id='chk_<?php echo $row['id'];?>' name="check_box[]" value="<?php echo $row['id'];?>" class="check_all" />
        </td>
   <?php
}?>     
<td>
<?php
echo CHtml::image(Yii::app()->request->baseUrl . "/images/npa/" . $row->id ."/"  . $row->main_image , "",array("width"=>"100px"));
?>
</td>
<td align="center"><b>
<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->id;?>/?type=1'  target="_blank" >
<?php echo $row->product_code;?>
</a></b>
</td>
<td>

<?php
echo $row->category['product_group'] ;
?>


</td><td>

 <?php echo g($row->road,'ถนน');?> <?php echo g($row->ta['thai_name'],'ต.');?> 
    <?php echo g($row->d['thai_name'],'อ.');?></td>
 <td><?php echo g($row->p['thai_name'],' ');?></td>
 <td><?php echo g2($row->area_rai," ไร่");?> <?php echo g2($row->area_ngan," งาน");?> <?php echo g2($row->area_wa," ตร.ว.");?></td>
 <td>
 <?php echo number_format($row->price,0,".",",");?> 
 </td>
<td>
<?php
switch($row->status)
{
	case "0":echo "รอการอนุมัติ";break;
	case "1":{
		if($row->sale_type=="0")
			echo "<img src='" . Yii::app()->request->baseUrl . "/images/buy4.gif' height='30' />" ;
		else
			echo "<img src='" . Yii::app()->request->baseUrl . "/images/button_status_6_auction21.gif' height='30' />";
		}break;
	case "2":echo "<img src='" . Yii::app()->request->baseUrl . "/images/button_status_6_sold2.gif' height='30' />";break;
	case "3":echo "ยกเลิกแล้ว";break;
}
?>
</td>
<td>

<?php 



	if($row['is_assign']!='1')
	echo "<a href='javascript:settooutter(\"1\",\"$row[id]\")'  class=\"link_search lmm_34_4\"  style=\"visibility:hidden;position:absolute;\" class=''>ส่งให้นายหน้า</a><br /><br />";
else 
	echo "<a href='javascript:settooutter(\"0\",\"$row[id]\")' class=\"link_search lmm_34_4\" style=\"visibility:hidden;position:absolute;\" class=''>ยกเลิกนายหน้า</a><br /><br />";


if($row['status']!="3"){
	if($row['is_highlight']=='0')
	echo "<a href='javascript:sethighligh(\"1\",\"$row[id]\")'  class=\"link_search lmm_34_4\"  style=\"visibility:hidden;position:absolute;\" class=''>ตั้งเป็นทรัพย์เด่น</a><br /><br />";
else 
	echo "<a href='javascript:sethighligh(\"0\",\"$row[id]\")' class=\"link_search lmm_34_4\" style=\"visibility:hidden;position:absolute;\" class=''>ยกเลิกทรัพย์เด่น</a><br /><br />";
	if($row['status']=="0"){
	?>
		<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/approveproperty/<?php echo $row['id'];?>' style="visibility:hidden;position:absolute;" class="link_search lmm_34_6">อนุมัติ</a>
        <br /><br />
<?php
	}
	if($row['status']!="3"){
?>
		<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/saleproperty/<?php echo $row['id'];?>'  style="visibility:hidden;position:absolute;" class='lmm_34_5 link_search'>บันทึกขาย</a><br /><br />
<?php
	}else{
?>
   <a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/saleproperty/<?php echo $row['id'];?>' style="visibility:hidden;position:absolute;" class='lmm_34_5 link_search'>ยกเลิกการขาย</a><br /><br />
<?php
	}
?>
		<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/updateproperty/<?php echo $row['id'];?>'  style="visibility:hidden;position:absolute;" class='lmm_34_2  link_search'>แก้ไข</a><br /><br />
		<a href='javascript:deleteit(<?php echo $row['id'];?>)'  style="visibility:hidden;position:absolute;" class='lmm_34_3 link_search'>ลบ</a><br /><br />

	<?php
}else{
	?>
    <a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/canceldeleteproperty/<?php echo $row['id'];?>'  style="visibility:hidden;position:absolute;" class='lmm_35_4 link_search'>นำกลับ</a><br /><br />
    <?php
}
	?>
	</td>
</tr>
    <?php
	
//	print_r($row);
//	echo "<hr />";
}
if(count($data2)<=0)
					{
?>

<tr style=" background-color:#FFF;font-weight:bold" valign="top">
<td colspan="10">

						<?php echo "<center><font color='red'><b>ไม่พบข้อมูลทรัพย์สินที่ท่านค้นหา</b></font></center>";?>
					
</td>
</tr>
<?php
					}?>
<tr style=" background-color:#FFF;font-weight:bold" valign="top">
<td colspan="10">
           <table width="100%" border="0" cellspacing="0" cellpadding="0" style="visibility:hidden;position:absolute;">
                        <tr>
                          <td width="160" class="txt_bold"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page =  $param['page']+1;?> / <?php echo $param['max_page'];?></td>
                          <td><div class="nav_page">
                             <ul>
                                      <li onclick="gotoPage('<?php echo $page-1;?>')">&laquo;</li>
                                      <?php 
									 
									  for($i=0;$i<ceil($param['max_page']);$i++)
									  {
										  $class = "";
										  if(($i+1)==$page)
										  	$class = '  class="navselect"';
										else
											$class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
										  ?>
                                          <li <?php echo $class;?>><?php echo $i+1;?></li>
                                          <?php
									  }
									  ?>
                                      <li onclick="gotoPage('<?php echo $page+1;?>')">&raquo;</li>
                                    </ul>
                          </div>
                          
                          </td>
                          <td width="160" align="right" class="txt_bold"><span class="txt_pink">ไปหน้าที่ :</span>
                                    <input name="page" type="text" id="page" size="2" />
                                    <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/go.png" width="39" height="22" /></a></td>
                        </tr>
                      </table>                                                                       
</td>
</tr>

</table>


</td>
</tr>
<tr>
                      <td height="40" valign="bottom">
                       <?php
								  if($param['max_page']>1){
									  ?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="160" class="txt_bold"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page =  $param['page']+1;?> / <?php echo $param['max_page'];?></td>
                          <td><div class="nav_page">
                             <ul>
                                      <li onclick="gotoPage('<?php echo $page-1;?>')">&laquo;</li>
                                      <?php 
									 
									  $start_page = ($page>5?$page-5:1);
									 $end_page = ($param['max_page']>($page+5)?$page+5:$param['max_page']);
									  for($i=$start_page-1;$i<ceil($end_page);$i++)
									  {
										  $class = "";
										  if(($i+1)==$page)
										  	$class = '  class="navselect"';
										else
											$class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
										  ?>
                                          <li <?php echo $class;?>><?php echo $i+1;?></li>
                                          <?php
									  }
									  ?>
                                      <li onclick="gotoPage('<?php echo $page+1;?>')">&raquo;</li>
                                    </ul>
                          </div></td>
                          <td width="160" align="right" class="txt_bold"><span class="txt_pink">ไปหน้าที่ :</span>
                                    <input name="page" type="text" id="page" size="2" />
                                    <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/go.png" width="39" height="22" /></a></td>
                        </tr>
                      </table>
                      <?php
								  }
								  ?>
                      </td>
                    </tr>
</table>
      <?php $this->endWidget(); ?>
</div>
<script language="javascript">
$("#check_all").click(function(){
							   $(".check_all").attr("checked",$('#check_all').is(':checked'));
							   });
</script>