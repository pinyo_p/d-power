<?php 
$part = Yii::app()->user->getValue("part");
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Manage User"; 
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	
	function deleteit(objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
		{
			$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DeleteUploadFile/',{
									id:objId
									},function(data){
										if(data=="OK")
											location.reload();
										else
											alert(data);
									});
		}
	}
	function deleteitAll()
	{
	
		if(confirm("ต้องการลบรายการที่เลือกใช่หรือไม่")){
			$("#act").val('deleteAll');
			$("#downloadlist").submit();
		}
	}
	function gotoPage(objPage)
	{
		$('#page').val(objPage);
		$('#upload').submit();
	}
	function sortit(objColumn,objOrder)
	{
		$("#order").val(objColumn);
		$('#order_by').val(objOrder);
		$('#upload').submit();
	}
	
	function toggle_prem(objId)
	{
		
		var attr = $('#' + objId).attr("style");
		
		$("#" + objId).toggle();
		/*
		if(attr=="display:none;")
			$('#' + objId).attr("style","display:static;");
		else
			$('#' + objId).attr("style","display:none;");*/
	}
	function cancelit(objId,objItemId)
	{
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/CancelDownloadPremission/',{
										id:objId,
										itemid:objItemId
										},function(data){
											if(data=="OK")
												window.location.reload();
											else
												alert(data);
										});
		////$('#download_premission').submit();
	}
	function cancelAll(objItemId)
	{
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/CancelDownloadPremissionByItemId/',{
										itemid:objItemId
										},function(data){
											if(data=="OK")
												window.location.reload();
											else
												alert(data);
										});
		////$('#download_premission').submit();
	}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">Download</a> &gt; <a href="#" class="link_green">Download</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">Download</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input type="hidden" value="<?php echo $model->current_page;?>" name="page" id='page' />
<input type="hidden" value="<?php echo $model->sort_direction;?>" name="order_by" id='order_by' />
<input type="hidden" value="<?php echo $model->sort_by;?>" name="order" id='order' />
<center>
<table>
<tr><td align="right">ชื่อไฟล์ :</td><td>
<?php echo $form->textField($model,'filename'); ?>
</td></tr>
<tr>
<td>เลือกกลุ่มไฟล์ : 
</td><td>
<?php
echo $form->dropDownList($model, 'group',array('' => '-- เลือกกลุ่ม --',
																'1'=>'แบบฟอร์มคดี ฝ่ายกฎหมาย','2'=>'หนังสือมอบอำนาจ',
																	  '3'=>'แบบฟอร์มการเบิกจ่าย','4'=>'โปรแกรมที่เกี่ยวข้อง',
																	  '5'=>'อัตราค่าวิชาชีพและค่าใช้จ่ายดำเนินคดี','6'=>'รายละเอียดประกอบการโอนเงินค่าจ้าง'
																 ));
																						  ?>





</td>
</tr>

<tr>
<td>
</td>
<td>
<a href="javascript:$('#upload').submit()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_search.png" width="52" height="25" /></a>

</td>
</tr>
</table>
</center>

<?php $this->endWidget(); ?>
                  
                  <br /><br />
                   <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'downloadlist',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input name="act" type="hidden" id='act'/>
                      <table width="100%" border="0" cellspacing="3" cellpadding="3">
                       <tr style="background-color:#78CF2D;color:white;">
                              <td width="30" align="center" valign="top" class="">เลือก</td>
                              <td width="30" align="center" valign="top" class="txt_bold">ลำดับ</td>
                              <td width="35%" align="center" valign="top" class="txt_bold">
                              <a href='javascript:sortit("filename","<?php if(strtolower($model->sort_direction)=="desc")
																			  echo 'asc';
																			  else 
																			  echo 'desc';
																			  ?>")' style="color:white;">
                              ไฟล์
                              <?php
							  if(strtolower($model->sort_by)=="filename"){
							  if(strtolower($model->sort_direction)=="desc")
								{
									echo "<img src='" . Yii::app()->request->baseUrl . "/images/sort_botton_down.png' />";
								}else{
									echo "<img src='" . Yii::app()->request->baseUrl . "/images/sort_botton_up.png' />";
								}
							  }
							  ?>
                              </a>
                              
                              </td>
                              <td width="17%" align="center" valign="top" class="txt_bold">
                              <a href='javascript:sortit("create_date","<?php if(strtolower($model->sort_direction)=="desc")
																			  echo 'asc';
																			  else 
																			  echo 'desc';
																			  ?>")' style="color:white;">
                              วันที่ อัพโหลด
                              <?php
							  if(strtolower($model->sort_by)=="create_date"){
							  if(strtolower($model->sort_direction)=="desc"  ) 
								{
									echo "<img src='" . Yii::app()->request->baseUrl . "/images/sort_botton_down.png' />";
								}else{
									echo "<img src='" . Yii::app()->request->baseUrl . "/images/sort_botton_up.png' />";
								}
							  }
							  ?>
                              </a>
                              </td>
                              <td width="15%" align="center" valign="top" class="txt_bold">หมวดหมู่</td>
                              <td  align="center" valign="top" class="txt_bold">ดาวน์โหลด(ครั้ง)</td>
                              
                              <td  align="center" valign="top" class="txt_bold">กำหนดสิทธิ์ดาวน์โหลด</td>
                               <td width="70" align="center" valign="top" class="txt_bold">แก้ไข</td>
                              <td width="70" align="center" valign="top" class="txt_bold">ลบ</td>
                            </tr>
                           <?php
						   $i=1;
						    $i = ($param['page'] * $param['display_perpage']) +1;
						   foreach($data as $row){
						   ?>
                            <tr>
                              <td width="30" align="center" valign="top" class="rowa"><span class="rowb">
                                <input type="checkbox" name="download_id[]" id="checkbox" value="<?php echo $row->id;?>" />
                              </span></td>
                              <td width="30" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i;?></span></td>
                              <td width="35%" align="left" valign="top" class="rowa"><span class="rowb">
							  <a href='<?php echo Yii::app()->request->baseUrl;?>/download/out/<?php echo $row->filesrc;?>' target='_blank'>
							  <?php echo $row->filename;?></a></span></td>
                              <td class="rowa" width="17%" align="center" valign="top"><?php echo $row->create_date;?></td>
                              <td width="15%" align="center" valign="top" class="txt_bold"><?php
							  $str = "";
							   switch($row->group)
								 {
									 case "1": $str = "แบบฟอร์มคดี ฝ่ายกฎหมาย";break;
									 case "2": $str = "หนังสือมอบอำนาจ";break;
									 case "3": $str = "แบบฟอร์มการเบิกจ่าย";break;
									 case "4": $str = "โปรแกรมที่เกี่ยวข้อง";break;
									 case "5": $str = "เอกสารอื่นๆ";break;
									  case "6": $str = "ข้อมูลทางการบัญชี";break;
									 
								 }
								 echo $str;
                              ?></td>
                              <td class="rowa" align="right" valign="top"><?php echo $row->download_count;?></td>
                               <td class="rowa"  align="center" valign="top" nowrap="nowrap"><a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DownloadPremission/<?php echo $row->id;?>'>กำหนดสิทธิ์ดาวน์โหลด</a>
                               <br  />
                               
                               <?php 
							   $user_data = $this->getPremissionUser($row->id)->data;
							   $count = count($user_data);
							   
							   if($count>0){
								   ?> 
                                   <a href='javascript:toggle_prem("fs_<?php echo $row->id;?>")'>
                                   สิทธิ์ <?php echo $count;?> รายการ 
                                   </a>
                                   <fieldset style="display:none;" id='fs_<?php echo $row->id;?>'>
                                   <table>
                                   <?php
								   foreach($user_data as $r){
									   ?>
                                       <tr>
                                       <td><?php echo $r->user['first_name'] . '&nbsp;' .  $r->user['last_name'] ;?></td>
                                       <td><a href='javascript:cancelit("<?php echo $r->user['id'];?>","<?php echo $row->id;?>")'>ยกเลิก</a>
                                       </tr>
                                       <?php
								   }
								   ?>
                                   </table>
                                   <a href='javascript:cancelAll("<?php echo $row->id;?>")'>ยกเลิกทั้งหมด</a>
                                   </fieldset>
                                   <?php
							   }
							   ?>
                               </td>
                               <td class="rowa"  align="center" valign="top"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/EditDownload/<?php echo $row->id;?>">แก้ไข</a></td>
                              <td class="rowa" align="center" valign="top"><a href="javascript:deleteit(<?php echo $row->id;?>)">ลบ</a></td>
                            </tr>
                            <?php
							$i++;
						   }
							?>
                          </table>
                          <?php $this->endWidget(); ?>
                          <br />
                          <?php
								  if($param['max_page']>1){
									  ?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="160" class="txt_bold"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page =  $param['page']+1;?> / <?php echo $param['max_page'];?></td>
                          <td><div class="nav_page">
                             <ul>
                                      <li onclick="gotoPage('<?php echo $page-1;?>')">&laquo;</li>
                                      <?php 
									 
									  $start_page = ($page>5?$page-5:1);
									 $end_page = ($param['max_page']>($page+5)?$page+5:$param['max_page']);
									  for($i=$start_page-1;$i<ceil($end_page);$i++)
									  {
										  $class = "";
										  if(($i+1)==$page)
										  	$class = '  class="navselect"';
										else
											$class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
										  ?>
                                          <li <?php echo $class;?>><?php echo $i+1;?></li>
                                          <?php
									  }
									  ?>
                                      <li onclick="gotoPage('<?php echo $page+1;?>')">&raquo;</li>
                                    </ul>
                          </div></td>
                          <td width="160" align="right" class="txt_bold"><span class="txt_pink">ไปหน้าที่ :</span>
                                    <input name="page" type="text" id="page" size="2" />
                                    <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/go.png" width="39" height="22" /></a></td>
                        </tr>
                      </table>
                      <?php
								  }
								  ?>
                    </td>
                        </tr>
                        
                        <tr>
                          <td class="nparesult_table_content" align="center"><br />
                          	<a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/EditDownload"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" width="52" height="25" /></a>
                           &nbsp;<a href="javascript:deleteitAll()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" width="52" height="25" /></a></td>
                        </tr>
                      </table>
                      </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
                    
                  </div></td>
                </tr>
              </table></td>
            </tr>
            
              </table></td>
            </tr>
          </table>
        