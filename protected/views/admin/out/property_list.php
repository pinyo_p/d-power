<?php 
$this->pageTitle="Admin Panel::Manage Group"; 
$max = count($data2);
?>
<?php
function g($val,$caption)
{
	return (trim($val)!=""?$caption . " " . $val:"");
}
function g2($val,$caption)
{
	return (trim($val)!="" && $val!="0"?$val . $caption:"");
}
?>
<link type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/smoothness/jquery-ui-1.8.4.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.13.custom.min.js"></script>
<script language="javascript">
function reorder(objId)
{
	var selbox = $("#orderlist_" + objId).val();
	window.location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/reorderhighlight/" + objId + "/" + selbox;
}
function setoutter(objFlag,objId)
{
	$.post("<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/updateoutter/" ,{
								id:objId,
								flag:objFlag
								},
								function(data)
							   {
								   location.reload();
							   });
}
function toggle_prem(objId)
	{
		var attr = $('#' + objId).attr("style");
		if(attr=="display:none;")
			$('#' + objId).attr("style","display:static;");
		else
			$('#' + objId).attr("style","display:none;");
	}
	function cancelit(objId,objItemId)
	{
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/CancelNpaPremission/',{
										id:objId,
										itemid:objItemId,
										},function(data){
											if(data=="OK")
												window.location.reload();
											else
												alert(data);
										});
		////$('#download_premission').submit();
	}
	function cancelAll(objItemId)
	{
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/CancelNpaPremissionByItemId/',{
										itemid:objItemId,
										},function(data){
											if(data=="OK")
												window.location.reload();
											else
												alert(data);
										});
		////$('#download_premission').submit();
	}
</script>

<div align="center">

<div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">ส่งให้นายหน้า</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ส่งให้นายหน้า</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table>
<br /><br />
<center>
<table style="text-align:left" width='100%'>
<tr>
<td>
<table bgcolor="#00CC00" width="90%" cellpadding="10" cellspacing="1">
<tr style="color:white;font-weight:bold" valign="top">

<td width="15%">รูปประกอบ</td>
<td width="75%">
รายการทรัพย์
</td>
<td width="10%">
ส่งให้นายหน้า
</td>

</tr>

<?php
foreach($data2 as $row)
{
	?>
    <tr bgcolor="white">

        
<td>
<?php
echo CHtml::image(Yii::app()->request->baseUrl . "/images/npa/" . $row['id'] ."/" . $row->main_image, "",array("width"=>"100px"));
?>
</td>
<td>
<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/npa/detail/<?php echo $row->id;?>/?type=1'  target="_blank" >
รหัสทรัพย์สิน : <?php echo $row->product_code;?> <br />ประเภททรัพย์สิน : <?php echo $row->category['product_group'];?><br />
<?php echo g($row->ta['thai_name'],'ต.');?> <?php echo g($row->d['thai_name'],'อ.');?> <?php echo g($row->p['thai_name'],'จ.');?><br />
     เนื้อที่ <?php echo g2($row->area_rai,' ไร่');?> <?php echo g2($row->area_ngan,' งาน');?> <?php echo g2($row->area_wa,' ตร.ว.');?><br />
                                ราคา <span class="txt_green"><?php echo number_format($row->price,0,".",",");?> บาท</span><br />
                                </a>
</td>

<td>
<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/SendToOutterUser/<?php echo $row->id;?>' >ส่งให้นายหน้า</a><br />
<?php 
							   $user_data = $this->getPremissionUserFromNpa($row->id)->data;
							   $count = count($user_data);
							   
							   if($count>0){
								   ?> 
                                   <a href='javascript:toggle_prem("fs_<?php echo $row->id;?>")'>
                                   สิทธิ์ <?php echo $count;?> รายการ 
                                   </a>
                                   <fieldset style="display:none;" id='fs_<?php echo $row->id;?>'>
                                   <table>
                                   <?php
								   foreach($user_data as $r){
									   ?>
                                       <tr>
                                       <td><?php echo $r->user['first_name'] . '&nbsp;' .  $r->user['last_name'] ;?></td>
                                       <td><a href='javascript:cancelit("<?php echo $r->user['id'];?>","<?php echo $row->id;?>")'>ยกเลิก</a>
                                       </tr>
                                       <?php
								   }
								   ?>
                                   </table>
                                   <a href='javascript:cancelAll("<?php echo $row->id;?>")'>ยกเลิกทั้งหมด</a>
                                   </fieldset>
                                   <?php
							   }
							   ?>
</td>

</tr>
    <?php
	
//	print_r($row);
//	echo "<hr />";
}
?>
</table>


</td>
</tr>
</table>
</center>
</div>
<script language="javascript">
$("#check_all").click(function(){
							   $(".check_all").attr("checked",$('#check_all').is(':checked'));
							   });
</script>