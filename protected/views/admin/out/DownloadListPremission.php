<?php 
$part = Yii::app()->user->getValue("part");
$p = "";
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Premission"; 
?>

<script language="javascript">
function setit(objId)
{
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/AddDownloadPremission/',{
									id:objId,
									itemid:<?php echo $_GET['id'];?>
									},function(data){
										if(data=="OK")
											$('#download_premission').submit();
										else
											alert(data);
									});
}
function cancelit(objId)
{
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/CancelDownloadPremission/',{
									id:objId,
									itemid:<?php echo $_GET['id'];?>
									},function(data){
										if(data=="OK")
											$('#download_premission').submit();
										else
											alert(data);
									});
}

</script>
<div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ผู้ใช้งาน</a> &gt; <a href="#" class="link_green">กำหนดสิทธิ์</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">กำหนดสิทธิ์ [
                      <?php
					  $download = bDownload::model()->findByPk($_REQUEST['id']);
					  echo $download->filename;
					  ?>
                      ]</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'download_premission',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
)); ?>
<center>
<table>
<tr><td>

<fieldset>
<legend>ค้นหา</legend>
<table>
<tr>
<td align="right"><?php echo $form->labelEx($model,'username'); ?> :</td>
<td><?php echo $form->textField($model,'username'); ?></td>
<td><?php echo $form->error($model,'username'); ?></td>
</tr>

<tr>
<td align="right"><?php echo $form->labelEx($model,'first_name'); ?> :</td>
<td><?php echo $form->textField($model,'first_name'); ?></td>
<td><?php echo $form->error($model,'first_name'); ?></td>
</tr>
<tr>
<td align="right"><?php echo $form->labelEx($model,'last_name'); ?> :</td>
<td><?php echo $form->textField($model,'last_name'); ?></td>
<td><?php echo $form->error($model,'last_name'); ?></td>
</tr>
<tr>
<td align="right"><?php echo $form->labelEx($model,'group_id'); ?> :</td>
<td><?php echo $form->dropDownList($model,'group_id', CHtml::listData(UserGroup::model()->findAll('part=:part',array(':part'=>Yii::app()->user->getValue("part")),array('order' => 'group_name ASC')), 'id', 'group_name'), array('empty'=>'เลือกกลุ่มผู้ใช้งาน',																																												  				 
	  )); ?></td>
<td><?php echo $form->error($model,'group_id'); ?></td>
</tr>

<tr>
<td colspan="2" align="center">

<input type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_search.png" alt="Submit button">
<a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/User/'><img src='<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png' /></a>
</td>
</tr>
</table>
</fieldset>
</td></tr>
</table>
</center>
<?php $this->endWidget(); ?>


<div align="center">
<table style="text-align:left" width="100%" >
<tr>
<td>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$model->search(),
	
	'summaryText'=>'',
	'htmlOptions'=>array('width'=>'100%'),
   'columns'=>array(
         array(
			   'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + (++$row)',
			'header'=>'ลำดับ'
			,
			'htmlOptions'=>array('style'=>'text-align:center;width:30px;')
			),          // display the 'title' attribute
        'username',
		'first_name',
		array(
            
			'header'=>'นามสกุล',
			'value'=>'$data->last_name',
			'htmlOptions'=>array('style'=>'text-align:center;width:150px;')
			),
		'group.group_name',
		array(
            
			'header'=>'email',
			'value'=>'$data->email',
			'htmlOptions'=>array('style'=>'text-align:center;width:150px;')
			),
		'mobile_no',
		array(
			  'name'=>'premission',
			  'type'=>'raw', 
             'value'=>array($this,'DownloadPremission'),
			 'htmlOptions'=>array('style'=>'text-align:center;')
			),

  
    ),
));
?>

</td>
</tr>
</table>
</div>