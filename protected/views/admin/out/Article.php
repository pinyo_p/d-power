<?php 
$part = Yii::app()->user->getValue("part");
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Manage User"; 
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	file_id=0;
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true,uploadURI : '<?php echo Yii::app()->request->baseUrl;?>/nicUpload.php'}) });
	$(function() {
		more_file();
		$( "#tabs" ).tabs();
		$("#tabs").removeClass('ui-widget ui-widget-content ui-corner-all ui-tabs-nav');
		$("#ul1").removeClass('ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all ');
		$("#ul1 li").removeClass('ui-state-default ui-corner-top ui-state-active ui-state-hover');
		$("#tabs-1").removeClass('ui-widget-content ui-corner-bottom');
		$("#tabs-2").removeClass('ui-widget-content ui-corner-bottom');  
	});
	function submitme()
	{
		var nicE_en = new nicEditors.findEditor('AboutSam_content_en');
		var nicE_th = new nicEditors.findEditor('AboutSam_content_th');
		content_en = nicE_en.getContent();
		content_th = nicE_th.getContent();
		$("#AboutSam_content_en").val(content_en);
		$("#AboutSam_content_th").val(content_th);
		$("#AboutSam").submit();
		
	}
	function more_file()
	{
		file_id++;
		$('#tb_file tr:last').after("<tr valign='middle' id='file_tr_" + file_id + "'><td align='right'>เอกสาร " + file_id + " :</td><td><input type='text' name='file_name[]' />&nbsp;&nbsp;<input type='file' name='file[]' /></td><td><a href='javascript:;' onclick='del_file(" + file_id + ")'><font color='black'>X</font></a></td></tr>");
	}
	function del_file(objId)
	{
		$('#file_tr_' + objId).remove();
	}
	function deletefile(objType,objRowId,objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
			$.post('<?php echo Yii::app()->createUrl('/admin/DeleteAttachFiles/');?>',
													 {
														 id:objId,
														 type:objType,
														},function(data)
													 {
														 if(data=="OK"){
															 $("#" + objRowId).hide();
														 }else{
															alert(data);
														 }
														});
		}
	}
</script>
<style type="text/css">
.ui-tabs .ui-tabs-panel{
	padding:0px;
}
.ui-tabs .ui-tabs-nav li a
{
	float:center;
	padding:0px;
	color:white;
}


.ui-tabs .ui-tabs-nav li
{
	text-align:center !important;
	padding:0px;
}

.bend_tab .ui-tabs-nav li 
{
	text-align:center;
	padding:0px;
}
</style>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'AboutSam',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">บทความ</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">บทความ</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td>
              <table width="700" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>
                          
                          <div id="tabs" class="bend_tab" style="text-align:left">
                            <ul id='ul1'>
                              <li id='li1' style="text-align:center">
                              
                              <a href="#tabs-1"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/th.png" width="16" height="11" /> &nbsp;ภาษาไทย</a></li>
                              <li>
                              <a href="#tabs-2">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/en.png" width="17" height="11" />&nbsp; ภาษาอังกฤษ</a></li>
                            </ul>
                            <div id="tabs-1">
                            
                            <p>
                            <table><tr><td width='150'>
                            หัวข้อ </td><td><?php echo $form->textField($model,'title_th',array('style'=>'width:600px;')); ?>
                            </td></tr><tr><td>
                            เนื้อหา </td><td>
                             <?php echo $form->textArea($model,'content_th',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                             </td>
                             </tr>
                            
                             </table>
                            </p>
                            </div>
                            <div id="tabs-2">
                            <p>
                            <table><tr><td width='150'>
                            หัวข้อ <br />(ภาษาอังกฤษ)  </td><td><?php echo $form->textField($model,'title_en',array('style'=>'width:600px;')); ?>
                            </td></tr><tr><td>
                            เนื้อหา <br />(ภาษาอังกฤษ)</td><td>
                              <?php echo $form->textArea($model,'content_en',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                             </td>
                             </tr>
                              
                             </table>
                            </p>
                            </div>
                            <table><tr><td width='80'>
                            วันที่เริ่มแสดง</td><td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'AboutSam[publish_date]',
					'value'=>$model->publish_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td>
             </tr>
             <tr><td>
             วันที่สิ้นสุด</td><td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'AboutSam[expire_date]',
					'value'=>$model->expire_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td></tr>
             <tr valign="top">
                <th>เอกสารประกอบ</th>
                <td colspan='2'>
                <fieldset>               
                <table id='tb_file'>
                <tr><td></td></tr>
                </table>
                <input type="button" value="เพิ่ม" onclick="more_file()" />
                <br /><?php
				
                if(isset($model->attach) && count($model->attach)>0){
					foreach($model->attach as $row)
					{
						$file_url = Yii::app()->request->baseUrl . "/attach/OUT/" . $row->npa_id . "/" . $row->file_src;
						echo '<div id="div_attach_' . $row->id . '"><b><a href="' . $file_url . '" target="_blank"><font color="black">' . $row->file_name  . "</font></a> </b><a href='javascript:deletefile(\"attach\",\"div_attach_" . $row->id . "\",\"" . $row->id . "\")'><font color='black'>ลบ</font></a></div>";
						echo "<br />";
					}
				}
					?>
                </fieldset>
                </td>
                <td>&nbsp;</td>
    </tr>
             </table>
                            <center>
                            <p><a href="javascript:;" onclick="submitme()">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" width="52" height="25" /></a></p>
                         </center>
                            
                          </div>
                          
                          </td>
                        </tr>
                        
                      </table>
                      <br />
  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
         <?php $this->endWidget(); ?>