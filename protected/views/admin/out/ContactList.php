<?php 
$part = Yii::app()->user->getValue("part");
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Manage User"; 
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	function changeStatus(objStatus,objId)
	{
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/UpdateContentStatus/',{
									id:objId,
									status:objStatus},function(data){
										if(data=="OK")
											location.reload();
										else
											alert(data);
									});
	}
	function changeDisplay(objId,objVal)
	{
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/UpdateContentDisplay/',{
									id:objId,
									status:objVal
									},function(data){
										if(data=="OK")
											location.reload();
										else
											alert(data);
									});
	}
	function deleteit(objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
		{
			$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DeleteContent/',{
									id:objId,
									},function(data){
										if(data=="OK")
											location.reload();
										else
											alert(data);
									});
		}
	}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">ติดต่อ</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ติดต่อ</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                      <table width="100%" border="0" cellspacing="3" cellpadding="3">
                       <tr style="background-color:#78CF2D;color:white;">
                              <td width="4%" align="center" valign="top" class="">เลือก</td>
                              <td width="4%" align="center" valign="top" class="txt_bold">ลำดับ</td>
                              <td width="15%" align="center" valign="top" class="txt_bold">หัวข้อ</td>
                              <td width="8%" align="center" valign="top" class="txt_bold">วันที่แสดงผล</td>
                              <td width="8%" align="center" valign="top" class="txt_bold">วันที่หมดอายุ</td>
                              <td width="10%" align="center" valign="top" class="txt_bold">สถานะการตรวจสอบ</td>
                              <td width="10%" align="center" valign="top" class="txt_bold">สถานะการอนุมัติ</td>

                              <td width="15%" align="center" valign="top" class="txt_bold">สถานะการแสดงผล</td>

                              <td width="6%" align="center" valign="top" class="txt_bold">แก้ไข</td>
                              <td width="6%" align="center" valign="top" class="txt_bold">ลบ</td>
                            </tr>
                           <?php
						   $i=1;
						   foreach($data as $row){
						   ?>
                            <tr>
                              <td width="4%" align="center" valign="top" class="rowa"><span class="rowb">
                                <input type="checkbox" name="checkbox2" id="checkbox2" />
                              </span></td>
                              <td width="4%" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i;?></span></td>
                              <td width="15%" align="left" valign="top" class="rowa"><span class="rowb">
							  <a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/outter/detail/<?php echo $row->id;?>/?type=1' target='_blank'>
							  <?php echo $row->title_th;?></a></span></td>
                              <td class="rowa" width="8%" align="center" valign="top"><?php echo $row->publish_date;?></td>
                               <td class="rowa" width="8%" align="center" valign="top"><?php echo $row->expire_date;?></td>
                               
                              <td class="rowa" width="10%" align="center" valign="top">
                               <?php if($row->status=="0"){?>
                              <select name="select5" onchange="changeStatus(1,<?php echo $row->id;?>)" class="textb" id="select5" style="width:120px">
                              <option>---- สถานะการตรวจสอบ ----</option>
                                <option>ตรวจสอบแล้ว</option>
                              </select>
                              <?php }else if($row->status>=1){
								  echo "ตรวจสอบแล้ว";
							  }?>
                              </td>
                               
                              <td class="rowa" width="10%" align="center" valign="top">
                              <?php if($row->status=="1"){
								  
								  ?>
                              <select name="select5" class="textb" onchange="changeStatus(2,<?php echo $row->id;?>)" id="select5" style="width:120px">
                                <option>- เลือกสถานะ -</option>
                                <option>อนุมัติ</option>
                              </select><?php }else{
								  if($row->status=="0")
								  	echo "รอการตรวจสอบ";
								else
									echo "อนุมัติแล้ว";
							  }
								  ?></td>
                               
                              <td class="rowa" width="15%" align="center" valign="top">
                              <?php
							  $show = 0;
							  $not_show = 0;
							  if($row->show_in_menu==1)
							  	$show = " selected='selected' ";
							else
								$not_show = " selected='selected' ";
							  ?>
                              <select name="select3" class="textb" id="select3_<?php echo $row->id;?>" onchange="changeDisplay(<?php echo $row->id;?>,this.value)"  style="width:120px">
                               <option value='1' <?php echo $show;?>>แสดงผล</option>
                                <option value='0' <?php echo $not_show;?>>ไม่แสดงผล</option>
                              </select></td>
                              <td class="rowa" width="6%" align="center" valign="top"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/Contact/<?php echo $row->id;?>/">แก้ไข</a></td>
                              <td class="rowa" width="6%" align="center" valign="top"><a href="javascript:deleteit(<?php echo $row->id;?>)">ลบ</a></td>
                            </tr>
                            <?php
							$i++;
						   }
							?>
                          </table>
                          </td>
                        </tr>
                        <tr>
                          <td class="nparesult_table_content" align="center"><br />
                          	
                            <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/Contact/"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" width="52" height="25" /></a> &nbsp;<a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" width="52" height="25" /></a></td>
                        </tr>
                      </table>
                      </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
                    
                  </div></td>
                </tr>
              </table></td>
            </tr>
            
              </table></td>
            </tr>
          </table>
        