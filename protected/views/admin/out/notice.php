<?php 
$part = Yii::app()->user->getValue("part");
$p="";
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Manage User"; 
$str = "";
if($part=="OUT")
{
	switch($_GET['parent'])
	{
		case "1": $str = "ประกาศ";break;
		case "2": $str = "บทความ";break;
		case "3": $str = "ผลการประเมิน";break;
		case "4": $str = "คำถามที่ถามบ่อย";break;
		case "5": $str = "ติดต่อ";break;
	}
}else if($part=="COP"){
	switch($_GET['parent'])
	{
		case "1": $str = "รู้จัก SAM";break;
		case "2": $str = "ข่าวสาร";break;
		case "3": $str = "ประกาศ";break;
		case "4": $str = "ประชาสัมพันธ์";break;
		case "5": $str = "หน่วยงานที่เกี่ยวข้อง";break;
		case "6": $str = "ติดต่อ";break;
	}
}
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	file_id=0;
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true,uploadURI : '<?php echo Yii::app()->request->baseUrl;?>/nicUpload.php'}) });
	$(function() {
		more_file();
		$( "#tabs" ).tabs();
		$("#tabs").removeClass('ui-widget ui-widget-content ui-corner-all ui-tabs-nav');
		$("#ul1").removeClass('ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all ');
		$("#ul1 li").removeClass('ui-state-default ui-corner-top ui-state-active ui-state-hover');
		$("#tabs-1").removeClass('ui-widget-content ui-corner-bottom');
		$("#tabs-2").removeClass('ui-widget-content ui-corner-bottom');  
		
	});
	function submitme(objType)
	{
		var nicE_en = new nicEditors.findEditor('AboutSam_content_en');
		var nicE_th = new nicEditors.findEditor('AboutSam_content_th');
		content_en = nicE_en.getContent();
		content_th = nicE_th.getContent();
		$("#AboutSam_content_en").val(content_en);
		$("#AboutSam_content_th").val(content_th);
		$('#act').val(objType);
		$("#AboutSam").submit();
		
	}
	function more_file()
	{
		file_id++;
		$('#tb_file tr:last').after("<tr valign='middle' id='file_tr_" + file_id + "'><td align='right'>เอกสาร " + file_id + " :</td><td><input type='text' name='file_name[]' />&nbsp;&nbsp;<input type='file' name='file[]' /></td><td><a href='javascript:;' onclick='del_file(" + file_id + ")'><font color='black'>X</font></a></td></tr>");
	}
	function del_file(objId)
	{
		$('#file_tr_' + objId).remove();
	}
	function deletefile(objType,objRowId,objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
			$.post('<?php echo Yii::app()->createUrl('/admin/DeleteAttachFiles/');?>',
													 {
														 id:objId,
														 type:objType,
														},function(data)
													 {
														 if(data=="OK"){
															 $("#" + objRowId).hide();
														 }else{
															alert(data);
														 }
														});
		}
	}
	<?php
	if(isset($_REQUEST['act'])&&$_REQUEST['act']=="saveopen"){
		?>
		window.open('<?php echo Yii::app()->request->baseUrl;?>/index.php/site/detail/<?php echo $model->id;?>');
		<?php
	}
	?>
</script>
<style type="text/css">
.ui-tabs .ui-tabs-panel{
	padding:0px;
}
.ui-tabs .ui-tabs-nav li a
{
	float:center;
	padding:0px;
	color:white;
}


.ui-tabs .ui-tabs-nav li
{
	text-align:center !important;
	padding:0px;
}

.bend_tab .ui-tabs-nav li 
{
	text-align:center;
	padding:0px;
}
</style>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'AboutSam',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input type="hidden" name="act" id='act'/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green"><?php echo $str;?></a> &gt; <a href="#" class="link_green"><?php echo $str;?></a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt"><?php echo $str;?></div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td>
              <table width="700" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                  
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>
                          
                          <div id="tabs" class="bend_tab" style="text-align:left">
                          <table><tr><td>
                            <ul id='ul1'>
                              <li id='li1' style="text-align:center">
                              
                              <a href="#tabs-1"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/th.png" width="16" height="11" /> &nbsp;ภาษาไทย</a></li>
                              <li>
                              <a href="#tabs-2">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/en.png" width="17" height="11" />&nbsp; ภาษาอังกฤษ</a></li>
                            </ul>
                            </td></tr><tr>
                            <td>
                            <div id="tabs-1">
                            
                            <p>
                            <table><tr><th width='170' align="right">
                          หัวข้อ  : <font color="red">*</font></th><td> <div class="form"><?php echo $form->textField($model,'title_th',array('style'=>'width:600px;')); ?>
                            <?php echo $form->error($model,'title_th'); ?></div>
                            </th></tr><tr><th align="right">
                            เนื้อหา : </th><td>
                             <?php echo $form->textArea($model,'content_th',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                             </td>
                             </tr>
                            <tr><th align="right">
                            รูปปก : 
                            </th>
                            <td>
                            <input type="file" name="pic_th" /><div class="form">
                            <?php echo $form->error($model,'pic_th'); ?></div>
                            </td>
                            </tr>
                             </table>
                            </p>
                            </div>
                            <div id="tabs-2">
                            <p>
                            <table><tr><th width='170' align="right">
                            หัวข้อ <br />(ภาษาอังกฤษ)  : </th><td><?php echo $form->textField($model,'title_en',array('style'=>'width:600px;')); ?>
                            </td></tr><tr><th align="right">
                            เนื้อหา <br />(ภาษาอังกฤษ) : </th><td>
                              <?php echo $form->textArea($model,'content_en',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                             </td>
                             </tr>
                              <tr><th align="right">
                            รูปปก  (ภาษาอังกฤษ) : 
                            </th>
                            <td>
                            <input type="file" name="pic_en" /><div class="form">
                            <?php echo $form->error($model,'pic_en'); ?></div>
                            </td>
                            </tr>
                             </table>
                            </p>
                            </div>
                            </td></tr></table>
                            
                            <?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    echo '<ul class="flashes">';
    foreach($flashMessages as $key => $message) {
        echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
    }
    echo '</ul>';
}
?>
                            <table>
                            <tr><th align="right">
                            จัดเรียง : </th><td>
                              <?php echo $form->textField($model,'sort_order',array('style'=>'width:40px;')); ?>
                             </td>
                             </tr>
                            <tr>
                            <th width='100' align="right">
                            วันที่เริ่มแสดง :</th><td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'AboutSam[publish_date]',
					'value'=>$model->publish_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td>
             </tr>
             <tr><th align="right">
             วันที่สิ้นสุด : </th><td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'AboutSam[expire_date]',
					'value'=>$model->expire_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td></tr>
             
             <?php
			 if( $_GET['group']=="8"  )
			 {
			 ?>
              <tr><th align="right">
             วันที่เริ่มซื้อเอกสาร : </th><td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'AboutSam[attr1]',
					'value'=>$model->attr1,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td></tr>
              <tr><th align="right">
             วันที่สิ้นสุดซื้อเอกสาร : </th><td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'AboutSam[attr2]',
					'value'=>$model->attr2,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td></tr>
             
             <tr><th align="right">
             วันที่กำหนดยื่นซอง : </th><td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'AboutSam[attr3]',
					'value'=>$model->attr3,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td></tr>
             <tr><th align="right">
             วันที่เปิดซอง : </th><td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'AboutSam[attr4]',
					'value'=>$model->attr4,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td></tr>
             
             
             <?php
			 }
			 ?>
             
             <?php
			 if($_GET['group']=="14" )
			 {
			 ?>
              <tr><th align="right">
             วันที่ทำกิจกรรม : </th><td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'AboutSam[attr1]',
					'value'=>$model->attr1,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td></tr>
             <tr><th align="right">
                            ประเภทกิจกรรม : 
                            </th>
                            <td>
                            <?php echo $form->radioButtonList($model,'attr2',array(0=>'งานกิจกรรม',1=>'งานขาย NPA'));?>
                            </td>
                            </tr>
             
             
             <?php
			 }
			 ?>
             <?php
			 if($_GET['group']=="34" )
			 {
			 ?>
             <tr><th align="right">
                            Link : </th><td>
                              <?php echo $form->textField($model,'attr1',array('style'=>'width:400px;')); ?>
                             </td>
                             </tr>
             <?php
			 }?>
             
             
             <?php
			 if($_GET['group']!="34" )
			 {
			 ?>
             <tr valign="top">
                <th align="right">เอกสารประกอบ : </th>
                <td colspan='2'>
                <fieldset>               
                <table id='tb_file'>
                <tr><td></td></tr>
                </table>
                <input type="button" value="เพิ่ม" onclick="more_file()" />
                <br /><?php
				
                if(isset($model->attach) && count($model->attach)>0){
					foreach($model->attach as $row)
					{
						$file_url = Yii::app()->request->baseUrl . "/attach/" .  $row->part ."/" . $row->npa_id . "/" . $row->file_src;
						echo '<div id="div_attach_' . $row->id . '"><b><a href="' . $file_url . '" target="_blank"><font color="black">' . $row->file_name  . "</font></a> </b><a href='javascript:deletefile(\"attach\",\"div_attach_" . $row->id . "\",\"" . $row->id . "\")'><font color='black'>ลบ</font></a></div>";
						echo "<br />";
					}
				}
					?>
                </fieldset>
                </td>
                <td>&nbsp;</td>
    </tr>
    <?php
			 }?>
             </table>
                            <center>
                            <p>
                            
                            <a href="javascript:;" onclick="submitme('save')">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" width="52" height="25" /></a>
                               <a href="javascript:;" onclick="submitme('savepreview')">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_saveopen.png"  height="25" /></a>
                              </p>
                         </center>
                            
                          </div>
                          
                          </td>
                        </tr>
                        
                      </table>
                      <br />
  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
         <?php $this->endWidget(); ?>