<?php 
$part = Yii::app()->user->getValue("part");
$p = "";
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Manage User"; 
$str = "";
if($part=="OUT")
{
	switch($_GET['parent'])
	{
		case "1": $str = "ประกาศ";break;
		case "2": $str = "บทความ";break;
		case "3": $str = "ผลการประเมิน";break;
		case "4": $str = "คำถามที่ถามบ่อย";break;
		case "5": $str = "ติดต่อ";break;
	}
}else if($part=="COP"){
	switch($_GET['parent'])
	{
		case "1": $str = "รู้จัก SAM";break;
		case "2": $str = "ข่าวสาร";break;
		case "3": $str = "ประกาศ";break;
		case "4": $str = "ประชาสัมพันธ์";break;
		case "5": $str = "หน่วยงานที่เกี่ยวข้อง";break;
		case "6": $str = "ติดต่อ";break;
	}
}
$prefix_id = 0;
switch($_GET['parent'])
{
	case "1":$prefix_id="23";break;
	case "2":$prefix_id="24";break;
	case "3":$prefix_id="25";break;
	case "4":$prefix_id="26";break;
	case "1":$prefix_id="23";break;
	case "1":$prefix_id="23";break;
	case "1":$prefix_id="23";break;
	case "1":$prefix_id="23";break;
}
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true,uploadURI : '<?php echo Yii::app()->request->baseUrl;?>/nicUpload.php'}) });
	function changeStatus(objStatus,objId)
	{
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/UpdateContentStatus/',{
									id:objId,
									status:objStatus},function(data){
										if(data=="OK")
											location.reload();
										else
											alert(data);
									});
	}
	function changeDisplay(objId,objVal)
	{
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/UpdateContentDisplay/',{
									id:objId,
									status:objVal
									},function(data){
										if(data=="OK")
											location.reload();
										else
											alert(data);
									});
	}
	function deleteit(objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
		{
			$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DeleteContent/',{
									id:objId,
									},function(data){
										if(data=="OK")
											location.reload();
										else
											alert(data);
									});
		}
	}
	function deleteitAll()
	{
	
		if(confirm("ต้องการลบรายการที่เลือกใช่หรือไม่")){
			$("#act").val('deleteAll');
			$("#frm_notice").submit();
		}
	}
	
</script>
<script language="javascript">
function gotoPage(objPage)
{
	window.location.href = '<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/NoticeList/<?php echo $_GET['id'];?>/?parent=<?php echo $_GET['parent'];?>&page=' + objPage;
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green"><?php echo $str;?></a> &gt; <a href="#" class="link_green"><?php echo $str;?></a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt"><?php echo $str;?></div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                   <?php 
					
					$form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_notice',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>



<input name="act" type="hidden" id='act'/>
<center> 
<table>
<tr><td>

<fieldset>
<legend>ค้นหา</legend>
<table>
<tr>
<td align="right"><?php echo $form->labelEx($model,'title_th'); ?> :</td>
<td><?php echo $form->textField($model,'title_th'); ?></td>
</tr>

<tr>
<td colspan="2" align="center">

<input type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_search.png"   alt="Submit button">
 <a class=" lmm_67_1  lmm_68_1" href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/Notice/?group=<?php echo $_GET['id'];?>&parent=<?php echo $_GET['parent'];?>"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" width="52" height="25" /></a>
</td>
</tr>
</table>
</fieldset>

</td>
</tr></table>
</center>
<br />

                      <table width="100%" border="0" cellspacing="3" cellpadding="3">
                       <tr style="background-color:#78CF2D;color:white;">
                              <td width="4%" align="center" valign="top" class="">เลือก</td>
                              <td width="4%" align="center" valign="top" class="txt_bold">ลำดับ</td>
                              <td width="15%" align="center" valign="top" class="txt_bold">หัวข้อ</td>
                              <td width="8%" align="center" valign="top" class="txt_bold">วันที่แสดงผล</td>
                              <td width="8%" align="center" valign="top" class="txt_bold">วันที่หมดอายุ</td>
                              <td width="10%" align="center" valign="top" class="txt_bold">สถานะการตรวจสอบ</td>
                              <td width="10%" align="center" valign="top" class="txt_bold">สถานะการอนุมัติ</td>

                              <td width="15%" align="center" valign="top" class="txt_bold">สถานะการแสดงผล</td>

                              <td width="6%" align="center" valign="top" class="txt_bold">แก้ไข</td>
                              <td width="6%" align="center" valign="top" class="txt_bold">ลบ</td>
                            </tr>
                           <?php
						   $i=1;
						   $i = ($param['page'] * $param['display_perpage']) +1;
						   foreach($data as $row){
						   ?>
                            <tr>
                              <td width="4%" align="center" valign="top" class="rowa"><span class="rowb">
                                <input type="checkbox" name="notice_id[]" id="checkbox" value="<?php echo $row->id;?>" />
                              </span></td>
                              <td width="4%" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i;?></span></td>
                              <td width="15%" align="left" valign="top" class="rowa"><span class="rowb">
							  <a href='<?php echo Yii::app()->request->baseUrl;?>/index.php/<?php
                              if($part=="OUT")
							  echo "outter";
							  else if($part=="COP")
							  	echo "site";
							  ?>/detail/<?php echo $row->id;?>/?type=1' target='_blank'>
							  <?php echo $row->title_th;?></a></span></td>
                              <td class="rowa" width="8%" align="center" valign="top"><?php echo $row->publish_date;?></td>
                               <td class="rowa" width="8%" align="center" valign="top"><?php echo $row->expire_date;?></td>
                               
                              <td class="rowa" width="10%" align="center" valign="top">
                               <?php if($row->status=="0"){?>
                              <select name="select5" onchange="changeStatus(1,<?php echo $row->id;?>)" class="textb" id="select5" style="width:120px">
                              <option>---- สถานะการตรวจสอบ ----</option>
                                <option>ตรวจสอบแล้ว</option>
                              </select>
                              <?php }else if($row->status>=1){
								  echo "ตรวจสอบแล้ว";
							  }?>
                              </td>
                               
                              <td class="rowa" width="10%" align="center" valign="top">
                              <?php if($row->status=="1"){
								  
								  ?>
                              <select name="select5" class="textb" onchange="changeStatus(2,<?php echo $row->id;?>)" id="select5" style="width:120px">
                                <option>- เลือกสถานะ -</option>
                                <option>อนุมัติ</option>
                              </select><?php }else{
								  if($row->status=="0")
								  	echo "รอการตรวจสอบ";
								else
									echo "อนุมัติแล้ว";
							  }
								  ?></td>
                               
                              <td class="rowa" width="15%" align="center" valign="top">
                              <?php
							  $show = 0;
							  $not_show = 0;
							  if($row->show_in_menu==1)
							  	$show = " selected='selected' ";
							else
								$not_show = " selected='selected' ";
							  ?>
                              <select name="select3" class="textb" id="select3_<?php echo $row->id;?>" onchange="changeDisplay(<?php echo $row->id;?>,this.value)"  style="width:120px">
                               <option value='1' <?php echo $show;?>>แสดงผล</option>
                                <option value='0' <?php echo $not_show;?>>ไม่แสดงผล</option>
                              </select></td>
                              <td class="rowa" width="6%" align="center" valign="top"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/Notice/<?php echo $row->id;?>/?group=<?php echo $_GET['id' ];?>&parent=<?php echo $_GET['parent'];?>"  <?php Yii::hideit();?> class="lmm_67_2 lmm_68_2" >แก้ไข</a></td>
                              <td class="rowa" width="6%" align="center" valign="top"><a <?php Yii::hideit();?> class=" lmm_68_3" href="javascript:deleteit(<?php echo $row->id;?>)">ลบ</a></td>
                            </tr>
                            <?php
							$i++;
						   }
							?>
                          </table>
                          <?php $this->endWidget(); ?>
                          </td>
                        </tr>
                        <tr>
                      <td height="40" valign="bottom">
                       <?php
								  if($param['max_page']>1){
									  ?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="160" class="txt_bold"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page =  $param['page']+1;?> / <?php echo $param['max_page'];?></td>
                          <td><div class="nav_page">
                             <ul>
                                      <li onclick="gotoPage('<?php echo $page-1;?>')">&laquo;</li>
                                      <?php 
									 
									  $start_page = ($page>5?$page-5:1);
									 $end_page = ($param['max_page']>($page+5)?$page+5:$param['max_page']);
									  for($i=$start_page-1;$i<ceil($end_page);$i++)
									  {
										  $class = "";
										  if(($i+1)==$page)
										  	$class = '  class="navselect"';
										else
											$class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
										  ?>
                                          <li <?php echo $class;?>><?php echo $i+1;?></li>
                                          <?php
									  }
									  ?>
                                      <li onclick="gotoPage('<?php echo $page+1;?>')">&raquo;</li>
                                    </ul>
                          </div></td>
                          <td width="160" align="right" class="txt_bold"><span class="txt_pink">ไปหน้าที่ :</span>
                                    <input name="page" type="text" id="page" size="2" />
                                    <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/go.png" width="39" height="22" /></a></td>
                        </tr>
                      </table>
                      <?php
								  }
								  ?>
                      </td>
                    </tr>
                        <tr>
                          <td class="nparesult_table_content" align="center"><br />
                          	
                            &nbsp;<a href="javascript:deleteitAll()" class="lmm_67_3  lmm_68_3"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" width="52" height="25" /></a></td>
                        </tr>
                      </table>
                      </td>
                </tr>
                
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            
          </table>
                    
                  </div></td>
                </tr>
              </table></td>
            </tr>
            
              </table></td>
            </tr>
            
          </table>
        