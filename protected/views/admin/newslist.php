<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'ข่าวสาร & กิจกรรม';
?>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteNews/", {
                id: objId
            }, function (data) {
                if (data.indexOf("OK") > -1)
                    location.href = '<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/NewsList';
                else if(data.indexOf("DENIED") > -1)
                    alert("คุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่นนี้");
                else
                    alert(data);
            });
        }
    }
    function deleteAll()
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $("#newslist-form").submit();
        }
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'newslist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' value="deleteall" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>

    <tr>
        <td align="center" width="100%" class="tabletest">
            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="50">เลือก</th>
                    <th>รูปภาพ</th>
                    <th>หัวข้อ</th>
                    <th>เนื้อหา</th>
                    <th width="80">View Count</th>
                    <th width="50">แก้ไข</th>
                    <th width="50">ลบ</th>
                </tr>
                <?php
                foreach ($data as $row) {
                    ?>
                    <tr>
                        <td align="center">
                            <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' /></td>
                        <td align="center">
                            <?php
                            $file = Yii::app()->request->baseUrl . '/images/content/' . $row->attr1;
                            if (file_exists(Yii::app()->basePath . '/../images/content/' . $row->attr1) && $row->attr1 != "") {
                                ?>
                                <img src="<?php echo $file; ?>" width="83" />
                                <?php
                            } else {
                                ?>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
                                <?php
                            }
                            ?>
                        </td>
                        <td align="center">
                            <?php echo $row->title_2; ?>
                        </td>
                        <td align="left">
                            <?php echo iconv_substr(trim(strip_tags($row->content_2)), 0, 100, "UTF-8") . "..."; ?>
                        </td>
                        <td align="center">
                            <?php echo number_format(ceil($row->view_count), 0, ".", ","); ?>
                        </td>


                        <td align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/News/<?php echo $row->id; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_edit.png" width="16" height="16" /></a></td>
                        <td  align="center">
                            <a href='javascript:deleteit("<?php echo $row->id; ?>");'>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br />
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/News/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a></td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>