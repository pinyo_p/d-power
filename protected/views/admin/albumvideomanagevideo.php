<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายการ Video';
?>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .col-sm-5{
        width:100%;
    }
    .bs-glyphicons {
        padding-left: 0;
        padding-bottom: 1px;
        margin-bottom: 20px;
        list-style: none;
        overflow: hidden;
    }
    .bs-glyphicons li {
        float: left;
        width: 250px;
        height: 260px;
        padding: 10px;
        margin: 0 -1px -1px 0;
        font-size: 12px;
        line-height: 1.4;
        text-align: center;
        border: 1px solid #ddd;
    }
    .bs-glyphicons .glyphicon {
        margin-top: 5px;
        margin-bottom: 10px;
        font-size: 24px;
    }
    .bs-glyphicons .glyphicon-class {
        display: block;
        text-align: center;
        word-wrap: break-word; /* Help out IE10+ with class names */
    }
    .bs-glyphicons li:hover {
        background-color: rgba(86,61,124,.1);
    }
</style>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("<?php echo Label::$confirm_delete; ?>"))
        {
            window.location = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteVideo/galleryid/<?php echo $_REQUEST["galleryid"] ?>/id/" + objId;
        }
    }
    function savecover(objId)
    {
        window.location = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/updateVideoCover/galleryid/<?php echo $_REQUEST["galleryid"] ?>/id/" + objId;
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'category-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ), 'htmlOptions' => array(
        'class' => 'validate', 'enctype' => 'multipart/form-data'
    )
        ));
?>
<section class="content">

    <div class="col-md-12">

        <!-- Custom Tabs (Pulled to the right) -->
        <div style="padding-left:70px;">

            <div>
                <ul class="bs-glyphicons">
                    <?php
                    $gal = $videos;

                    foreach ($gal as $g) {
                        try {
                            $url = "http://gdata.youtube.com/feeds/api/videos/" . $g->url;
                            $doc = new DOMDocument;
                            $doc->load($url);
                            $title = $doc->getElementsByTagName("title")->item(0)->nodeValue;
                            echo "<li><div id='o_dvimg_" . $g->id . "'><img id='o_img" . $g->id . "' src='http://img.youtube.com/vi/" . $g->url . "/0.jpg' style='max-width:200px; max-height:150px; padding:5px;'><br />
						<br /><a href='http://www.youtube.com/watch?v=" . $g->url . "' target='_blank'>$title</a>
                        <br /><br />";
                            if ($g->is_cover == 0)
                                echo "<input style='background-color: #78CF2D;border-color: #78CF2D;color:#fff;border-radius: 5px;cursor:pointer;' type='button' value='ตั้งเป็นภาพปก' onclick='savecover(" . $g->id . ")' />";
                            else
                                echo "<input style='background-color: #006600;border-color: #006600;color:#fff; border-radius: 5px;cursor:pointer;' type='button' value='ยกเลิกตั้งเป็นภาพปก' onclick='savecover(" . $g->id . ")' />";
                            echo "<input style='background-color: #f56954;border-color: #f4543c;color:#fff;border-radius: 5px;cursor:pointer;' type='button' value='ลบ Video' onclick='deleteit(" . $g->id . ")' /> </div></li>";
                        } catch (Exception $e) {
                            //echo 'Caught exception: ', $e->getMessage(), "\n";
                        }
                    }
                    ?>
                </ul>
            </div>

            <div class="box-footer">
                <h3 class="box-title">เพิ่ม Video </h3>
                <div id='image_list'></div>
                <input type='button' class='btn btn-info' onclick="moreimage()" value='เพิ่ม Video' />&nbsp;&nbsp; 
                <input type='submit' class='btn btn-info' value='อัพโหลด' />&nbsp;&nbsp; 
                <a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/AlbumVideoList'; ?>">
                    <input type="button" value="ย้อนกลับ"/>
                </a>
            </div>
            <br/><br/><br/>

        </div>

    </div>                     

</section>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/admin/js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

<script type="text/javascript">
                    var i = 0;
                    function moreimage() {
                        i++;
                        $("#image_list").append("<div id='dvimg_" + i + "'>URL : <input  class='img_up' onchange='readURL(this)' preview='img" + i + "' type='text'  name='img_gal[]' deletebutton='del_img_" + i + "' style='width:300px;' /><br /><br /><img id='img" + i + "' style='max-width:400px;padding:5px;'><br /><input type='button' class='btn btn-info' value='<?php echo Label::$cancel; ?>' id='del_img_" + i + "'  onclick='deleteItem(\"dvimg_" + i + "\")' /></div>");
                        $("#img" + i).hide();
                        $("#del_img_" + i).hide();
                    }

                    function moreattach() {
                        i++;
                        $("#attach_list").append("<input type='button' class='btn btn-info' value='<?php echo Label::$cancel; ?>' id='del_attach_" + i + "' /><input type='file'  name='attach[]' deletebutton='del_attach_" + i + "'  /><hr style='width:90%;' /></div>");
                        $("#del_attach_" + i).hide();
                    }

                    function readURL(input) {
                        var url = $(input).val();
                        var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
                        if (videoid != null) {
                            console.log("video id = ", videoid[1]);
                            $("#" + $(input).attr("preview")).attr('src', "http://img.youtube.com/vi/" + videoid[1] + "/0.jpg");
                            $("#" + $(input).attr("preview")).show();
                            $("#" + $(input).attr("deletebutton")).show();
                        }

                    }

                    function deleteItem(objId) {
                        $("#" + objId).remove();
                    }

</script>
<?php $this->endWidget(); ?>
