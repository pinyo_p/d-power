<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'สินค้า';
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
$id = (isset($_GET['id']) ? $_GET['id'] : "0");
?>
<script type="text/javascript" language="javascript">
    var roxyFileman = '<?php echo Yii::app()->request->baseUrl; ?>/fileman/pagefile.html';
    $(function () {
        $("#tabs").tabs();
        CKEDITOR.replace('Product_product_desc_2', {
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
        CKEDITOR.replace('Product_product_desc_1', {
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
    });
    function delete_spec()
    {
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deletespec", {id: '<?php echo $id; ?>'}, function (data) {

            $("#file_attach").hide();

        }, 'json');
    }
    function delete_image(objId)
    {
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteproductimage", {id: '<?php echo $id; ?>', image_id: objId}, function (data) {
            $("#dvImage" + objId).hide();

            //location.reload();

        }, 'json');
    }
</script>

<table width="100%">
    <tr>
        <td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
    </tr>
    <tr>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="add_data">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th width="40%" align="right">รหัสสินค้า :</th>
                    <td width="75%"><?php echo $form->textField($model, 'product_code'); ?><?php echo $form->error($model, 'product_code'); ?></td>
                </tr>
                <tr>
                    <th width="40%" align="right">รหัสสินค้า 2 :</th>
                    <td width="75%"><?php echo $form->textField($model, 'product_code2'); ?></td>
                </tr>
                <tr class="hide_it">
                    <th width="40%" align="right" >Brand :</th>
                    <td width="75%">
                        <?php
                        echo $form->dropDownList($model, 'brand_id', CHtml::listData(Brand::model()->findAll(array('order' => 'name_th ASC')), 'id', 'name_th'), array('empty' => 'เลือก Brand', 'ajax' => array(
                                'type' => 'POST',
                                'url' => Yii::app()->request->baseUrl . '/index.php/admin/getgroup/',
                                'success' => 'js:function(html){jQuery("#Product_group_id").html(html);}',),
                        ));
                        ?><?php echo $form->error($model, 'brand_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">หมวดหมู่สินค้า :</th>
                    <td width="75%">
                        <?php
                        echo $form->dropDownList($model, 'group_id', $param['group'], array('empty' => 'เลือก หมวดหมู่', 'ajax' => array(
                                'type' => 'POST',
                                'url' => Yii::app()->request->baseUrl . '/index.php/admin/getseries/',
                                'success' => 'js:function(html){jQuery("#Product_serie_id").html(html);}',),
                        ));
                        ?><?php echo $form->error($model, 'group_id'); ?>
                    </td>
                </tr>
                <tr  class="hide_it">
                    <th width="40%" align="right">Series :</th>
                    <td width="75%">
<?php echo $form->dropDownList($model, 'serie_id', $param['series'], array('empty' => 'เลือก หมวดหมู่ก่อน',
));
?><?php echo $form->error($model, 'serie_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">เนื้อหา :</th>
                    <td width="75%">
                        <div style="width:800px;  text-align:left">

                            <div id="tabs" >
                                <ul>
                                    <li><a href="#tabs-1">ภาษาไทย</a></li>
                                    <li><a href="#tabs-2">ภาษาอังกฤษ</a></li>
                                </ul>
                                <div id="tabs-1">
                                    ชื่อสินค้า(ภาษาไทย)<br />
                                    <?php echo $form->textField($model, 'product_name_2', array('style' => 'width:450px')); ?>
                                    <?php echo $form->error($model, 'product_name_2'); ?>
                                    <br /><br />
                                    รายละเอียด(ภาษาไทย)<br />
                                    <?php
                                    Yii::import('ext.ckeditor.*');
                                    $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $model,
                                        'attribute' => 'product_desc_2',
                                        'language' => 'en',
                                        'editorTemplate' => 'full',
                                        'height' => '800px',
                                    ));
                                    ?>
                                </div>
                                <div id="tabs-2">
                                    ชื่อสินค้า(ภาษาอังกฤษ)
                                    <br />
                                    <?php echo $form->textField($model, 'product_name_1', array('style' => 'width:450px')); ?>
                                    <br /><br />
                                    รายละเอียด(ภาษาอังกฤษ)<br />
                                    <?php
                                    Yii::import('ext.ckeditor.*');
                                    $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $model,
                                        'attribute' => 'product_desc_1',
                                        'language' => 'en',
                                        'editorTemplate' => 'full',
                                        'height' => '600px',
                                    ));
                                    ?>
                                </div>

                                <div style="padding-left:15px">


                                </div>

                            </div>
                            <br /><br />
                        </div>


                    </td>
                </tr>



                <tr>
                    <th width="40%" align="right">ราคา 1 :</th>
                    <td width="75%"> <?php echo $form->textField($model, 'price1', array('style' => 'width:150px')); ?>
                        ยอดสั่งซื้อขั้นต่ำ <?php echo $form->textField($model, 'amount1', array('style' => 'width:50px')); ?> ชิ้น
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">ราคา 2 :</th>
                    <td width="75%"> <?php echo $form->textField($model, 'price2', array('style' => 'width:150px')); ?>
                        ยอดสั่งซื้อขั้นต่ำ <?php echo $form->textField($model, 'amount2', array('style' => 'width:50px')); ?> ชิ้น
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">ราคา 3 :</th>
                    <td width="75%"> <?php echo $form->textField($model, 'price3', array('style' => 'width:150px')); ?>
                        ยอดสั่งซื้อขั้นต่ำ <?php echo $form->textField($model, 'amount3', array('style' => 'width:50px')); ?> ชิ้น
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">ราคา 4 :</th>
                    <td width="75%"> <?php echo $form->textField($model, 'price4', array('style' => 'width:150px')); ?>
                        ยอดสั่งซื้อขั้นต่ำ <?php echo $form->textField($model, 'amount4', array('style' => 'width:50px')); ?> ชิ้น
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">Stock :</th>
                    <td width="75%"> <?php echo $form->textField($model, 'stock', array('style' => 'width:50px')); ?>
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">Sale name :</th>
                    <td width="75%"> <?php echo $form->textField($model, 'sale_name', array('style' => 'width:250px')); ?>
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">Spec แนบ :</th>
                    <td width="75%">

                        <input type="file" name="attach" id='attach' /><br /> <?php
                        $file = Yii::app()->request->baseUrl . '/attach/product/' . $model->attach;
                        if (file_exists(Yii::app()->basePath . '/../attach/product/' . $model->attach) && $model->attach != "") {
                            ?><div id='file_attach'><br />
<?php echo $model->attach;?>
                                <a href="<?php echo $file; ?>" target="_blank">Download </a> | <a href='javascript:delete_spec();'>ลบออก</a>
                            </div>
    <?php
}
?>
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปหลัก :</th>
                    <td width="75%">
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic1;
                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic1) && $model->pic1 != "") {
                            ?><div id='dvImage1'><br />
                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(1);'>ลบออก</a>
                            </div>
    <?php
}
?><br /> 
                        <input type="file" name="pic1" id='pic1' />
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปประกอบ 1 :</th>
                    <td width="75%">
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic2;
                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic2) && $model->pic2 != "") {
                            ?> <div id='dvImage2'><br />

                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(2);'>ลบออก</a>
                            </div>
    <?php
}
?><br /> 
                        <input type="file" name="pic2" id='pic2' />
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปประกอบ 2 :</th>
                    <td width="75%">
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic3;
                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic3) && $model->pic3 != "") {
                            ?><div id='dvImage3'><br />
                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(3);'>ลบออก</a>
                            </div>
    <?php
}
?><br /> 
                        <input type="file" name="pic3" id='pic3' />
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปประกอบ 3 :</th>
                    <td width="75%">
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic4;
                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic4) && $model->pic4 != "") {
                            ?><div id='dvImage4'><br />
                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(4);'>ลบออก</a>
                            </div>
    <?php
}
?><br /> 
                        <input type="file" name="pic4" id='pic4' />
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปประกอบ 4 :</th>
                    <td width="75%">
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic5;
                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic5) && $model->pic5 != "") {
                            ?><div id='dvImage5'><br />
                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(5);'>ลบออก</a>
                            </div>
    <?php
}
?><br /> 
                        <input type="file" name="pic5" id='pic5' />
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปประกอบ 5 :</th>
                    <td width="75%">
                            <?php
                            $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic6;
                            if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic6) && $model->pic6 != "") {
                                ?><div id='dvImage6'><br />
                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(6);'>ลบออก</a></div>
    <?php
}
?><br /> 
                        <input type="file" name="pic6" id='pic6' />
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปประกอบ 6 :</th>
                    <td width="75%">
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic7;
                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic7) && $model->pic7 != "") {
                            ?><div id='dvImage7'><br />
                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(7);'>ลบออก</a>
                            </div>
    <?php
}
?><br /> 
                        <input type="file" name="pic7" id='pic7' />
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปประกอบ 7 :</th>
                    <td width="75%">
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic8;
                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic8) && $model->pic8 != "") {
                            ?><div id='dvImage8'><br />
                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(8);'>ลบออก</a>
                            </div>
    <?php
}
?><br /> 
                        <input type="file" name="pic8" id='pic8' />
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปประกอบ 8 :</th>
                    <td width="75%">
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic9;
                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic9) && $model->pic9 != "") {
                            ?><div id='dvImage9'><br />
                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(9);'>ลบออก</a>
                            </div>
    <?php
}
?><br /> 
                        <input type="file" name="pic9" id='pic9' />
                    </td>
                </tr>

                <tr>
                    <th width="40%" align="right">รูปประกอบ 9 :</th>
                    <td width="75%">
                        <?php
                        $file = Yii::app()->request->baseUrl . '/images/product/' . $model->pic10;
                        if (file_exists(Yii::app()->basePath . '/../images/product/' . $model->pic10) && $model->pic10 != "") {
                            ?><div id='dvImage10'><br />
                                <img src="<?php echo $file; ?>" height="83" /><br /> <a href='javascript:delete_image(10);'>ลบออก</a>
                            </div>
    <?php
}
?><br /> 
                        <input type="file" name="pic10" id='pic10' />
                    </td>
                </tr>



                <tr>
                    <th width="40%" align="right">สินค้าแนะนำ :</th>
                    <td width="75%"> <?php echo $form->checkBox($model, 'is_highlight', array('value' => 1, 'uncheckValue' => 0)); ?></td>
                </tr>

                <tr style="display:none;">
                    <th width="40%" align="right">สินค้ามาใหม่ :</th>
                    <td width="75%"> <?php echo $form->checkBox($model, 'is_new', array('value' => 1, 'uncheckValue' => 0)); ?></td>
                </tr>


                <tr>
                    <th width="40%" align="right">จัดเรียง :</th>
                    <td width="75%"><?php echo $form->textField($model, 'sort_order', array('style' => 'width:50px')); ?></td>
                </tr>
                
                <tr>
                    <th width="40%" align="right">น้ำหนัก (กก.) :</th>
                    <td width="75%"><?php echo $form->textField($model, 'weight_kg', array('style' => 'width:50px')); ?></td>
                </tr>
                <tr>
                    <th width="40%" align="right">ประเภทของสด :</th>
                    <td width="75%">
                        <?php echo $form->checkBox($model, 'is_fresh_type', array('value' => 1, 'uncheckValue' => 0)); ?>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">รายการสินค้าสำหรับร้านอาหาร :</th>
                    <td width="75%">
                        <?php echo $form->checkBox($model, 'is_for_restaurant', array('value' => 1, 'uncheckValue' => 0)); ?>
                    </td>
                </tr>
            </table>
            <br />
            <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/ProductList'; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>