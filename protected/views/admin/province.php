﻿<?php 
$this->pageTitle="Admin Panel::Province"; 
?>
<script language="javascript">
$(function(){
$("#Provinces_continent_id").change(function(){
											   $("#frm_continent").submit();
											   });
		   });
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteProvince/');?>',{id:objId},function(data){
																							   $("#frm_continent").submit();
																							   });
	}
}
function deleteitAll()
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่ ?")){
		$("#act").val('deleteAll');
		$("#frm_continent").submit();
	}
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">จังหวัด</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ข้อมูลจังหวัด</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td>
              <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_continent',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input name="act" type="hidden" id='act'/>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                      <br />
                      <table width="320" border="0" align="center" cellpadding="2" cellspacing="3" class="form1">
                        <tr>
                          <td width="120"><div align="right"><span class="h1">เลือกภาค :</span></div></td>
                          <td><?php echo $form->dropDownList($model,'continent_id', CHtml::listData(Continent::model()->findAll(array('order' => 'thai_name ASC')), 'id', 'thai_name'), array('empty'=>'ภูมิภาค',	
	
	  )); ?></td>
                          </tr>
                      </table>
                  </td>
                </tr>
                <tr>
                  <td><br />
                    <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
                    <tr  style="background-color:#78CF2D;color:white;">
                          <td width="20px" align="center" valign="top" class="txt_bold">เลือก</td>
                          <td width="20px" align="center" valign="top" class="txt_bold">ลำดับ</td>
                          <td align="center" valign="top" class="txt_bold">ชื่อจังหวัด</td>
                          <td width="20px" align="center" valign="top" class="txt_bold">แก้ไข</td>
                          <td width="20px" align="center" valign="top" class="txt_bold">ลบ</td>
                        </tr>
                        <?php
						$i=0;
						foreach($data as $row){
							$i++;
						?>
                        <tr>
                          <td class="rowa" width="20px" align="center" valign="top"><input type="checkbox" name="province_id[]" id="checkbox" value="<?php echo $row->province_id;?>" /></td>
                          <td width="20px" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i;?></span></td>
                          <td class="rowa"    valign="top"><?php echo $row->thai_name;?> / <?php echo $row->eng_name;?></td>
                          <td class="rowa" width="20px" align="center" valign="top"><a href="<?php echo Yii::app()->createUrl('/admin/UpdateProvince/' . $row->province_id );?>"  style="visibility:hidden;position:absolute;" class='lmm_10_2' >แก้ไข</td>
                          <td class="rowa" width="20px" align="center" valign="top"><a href="#" style="visibility:hidden;position:absolute;" class='lmm_10_3' onclick="deleteit('<?php echo $row->province_id;?>')" >ลบ</a></td>
                        </tr>
                        <?php
						}
						?>
                      </table></td>
                    </tr>
                    <tr>
                      <td class="nparesult_table_content" align="center"><br />
                        &nbsp;&nbsp;<a href="javascript:deleteitAll()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" width="52" height="25" /></a>&nbsp; <a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a></td>
                    </tr>
                  </table>
                   <?php $this->endWidget(); ?>
                  </td>
                </tr>
                <tr>
                  <td><br />
                    <br />
                    <?php 
					if($model->continent_id>0){
					$form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_province',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">เพิ่มจังหวัด</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                </table></td>
                </tr>
                <tr>
                  <td><br />
                    <br />
                    <br />
                    
                    
                    
                    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="form1">
                    <tr>
                        <td align="right">ชื่อจังหวัด : &nbsp;</td>
                        <td align="left">
                        
                         <?php echo $form->dropDownList($model,'continent_id', CHtml::listData(Continent::model()->findAll(array('order' => 'thai_name ASC')), 'id', 'thai_name'), array('empty'=>'-ภูมิภาค -',	
	
	  )); ?>
                        </td>
                      </tr>
                   
                      <tr>
                        <td align="right">ชื่อจังหวัด (ไทย): &nbsp;</td>
                        <td align="left">
                          <label for="textfield"></label>
                            <?php echo $form->textField($model,'thai_name'); ?><div class="form"><?php echo $form->error($model,'thai_name'); ?></div>
                        </td>
                      </tr>
                       <tr>
                        <td align="right">ชื่อจังหวัด (อังกฤษ): &nbsp;</td>
                        <td align="left">
                          <label for="textfield"></label>
                          <?php echo $form->textField($model,'eng_name'); ?>
                        </td>
                      </tr>
                      <tr>
                        <td  align="center"><br /></td>
                        <td align="left"><br /></td>
                      </tr>
                      <tr>
                        <td align="center">&nbsp;</td>
                        <td align="left">
                        <input type="image"  style="visibility:hidden;position:absolute;" class='lmm_10_1' src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" alt="Submit button">&nbsp;<a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a></td>
                      </tr>
                    </table>
                    <?php $this->endWidget();
					}
					?>
                    <br />
                    <br />
                    <br />
                    <br /></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
        