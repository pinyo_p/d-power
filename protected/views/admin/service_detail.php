<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'รายละเอียดคำร้องขอ';
?>
<br /><br /><br /><br /><br />
 <table width="80%" border="0" align="center" cellpadding="5" cellspacing="2" bgcolor="white">
                      
                      
                      <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">เรื่องที่ต้องการแจ้งปัญหา : </td>
                          <td valign="top"><?php echo $model->subject; ?></td>
                        </tr>
                       
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ชื่อบริษัทลูกค้า : </td>
                          <td valign="top"><?php echo $model->company_name; ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">ชื่อ-นามสกุล ลูกค้า : </td>
                          <td valign="top"><?php echo $model->full_name; ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">อีเมล์ : </td>
                          <td valign="top"><?php echo $model->email; ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">เบอร์โทรศัพท์ติดต่อกลับ : </td>
                          <td valign="top"><?php echo $model->phone_no; ?></td>
                        </tr>
                        <tr>
                          <td width="250" align="right" valign="top" bgcolor="#f8f8f8" class="txt_bold">รายละเอียด : </td>
                          <td valign="top"><?php echo str_replace(chr(13),"<br />",$model->detail);?></td>
                        </tr>
                       
                      </table>