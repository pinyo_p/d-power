
<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายการสินค้าใกล้เคียง';
?>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deleteProductRelate/", {
                id: objId
            }, function (data) {
                if (data == "OK")
                    $("#productlist-form").submit();
                else
                    alert(data);
            });
        }
    }
    function deleteAll()
    {
        if (confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
        {
            $("#act").val("deleteall");
            $("#productlist-form").submit();

        }
    }

</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'productlist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">


    <tr>
        <td align="center"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" /><br /><br /><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>

    <tr>
        <td align="center" width="100%" class="tabletest">




            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">เลือก</th>
                    <th  width="120">Logo</th>
                    <th width="120">รหัสสินค้า</th>
                    <th >ชื่อสินค้า</th>
                    <th width="60">ลบ</th>
                </tr>
                <?php
                foreach ($data as $row) {
                    ?>
                    <tr>
                        <td align="center">
                            <input name="p_id[]" type="checkbox" id="p_id" value='<?php echo $row->id; ?>' /></td>
                        <td align="center">
                            <?php
                            $file = Yii::app()->request->baseUrl . '/images/product/' . $row->product['pic1'];
                            if (file_exists(Yii::app()->basePath . '/../images/product/' . $row->product['pic1']) && $row->product['pic1'] != "") {
                                ?>
                                <img src="<?php echo $file; ?>" height="83" />
                                <?php
                            } else {
                                ?>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/logo_knp.jpg" width="117" height="83" />
                                <?php
                            }
                            ?>
                        </td>
                        <td align="left">
                            <?php echo $row->product['product_name_2']; ?> / <?php echo $row->product['product_name_1']; ?>
                        </td>
                        <td align="left">
                            <?php echo $row->product['product_code']; ?> / <?php echo $row->product['product_code2']; ?><br />
                            <?php echo iconv_substr(trim(strip_tags($row->product['product_desc_2'])), 0, 100, "UTF-8") . "..."; ?>
                        </td>
                        <td align="center">
                            <a href='javascript:deleteit("<?php echo $row->id; ?>");'>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/icon_del.gif" width="16" height="16" /></a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>

        </td>
    </tr>
    <tr>
        <td align="center">



            <br />
            <br />
            <br />
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/ProductRelate/<?php echo $_GET['id']; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_add.png" width="55" height="26" /></a>&nbsp;<a href="javascript:deleteAll();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/botton_del.png" width="55" height="26" /></a>
            <a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/ProductList'; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a>
        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>