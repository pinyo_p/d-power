<?php 
$this->pageTitle="Admin Panel::About Sam"; 
?>
<script language="javascript">
function deletealbum(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteAlbum/');?>',{id:objId},function(data){
																							   $("#photo_list").submit();
																							   });
	}
}
function deleteimage(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteImage/');?>',{id:objId},function(data){
																							   $("#photo_list").submit();
																							   });
	}
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt;<a href="#" class="link_green"> Corporate</a> &gt; <a href="#" class="link_green">Photo Gallery</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">Photo Gallery</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><table width="60%" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                  <td class="nparesult_table_content">
                  
                  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_album',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="150" valign="top" align="right"><span class="txt_green txt_bold">เพิ่มอัลบั้ม :</span></td>
                      <td valign="top"><?php echo $form->textField($model,'album_name'); ?></td>
                    </tr>
                   <tr>
                    <td align="right"><span class="txt_green txt_bold">
                    Publish Date :</span>
                    </td>
                    <td>
                     <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'PhotoAlbum[publish_date]',
					'value'=>$model->publish_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                    </td>
                    </tr>
                    <tr>
                    <td align="right">
                    <span class="txt_green txt_bold">End Date :</span>
                    </td>
                    <td>
                   <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'PhotoAlbum[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                    </td>
                    </tr>
                    <tr>
                      <td width="150" valign="top" align="right"><span class="txt_green txt_bold">รูปปกอัลบั้ม  :</span></td>
                      <td valign="top"><p>
                        <input type="file" name="fileField" id="fileField" />
                      </p>
                      <?php
					  if($model->album_cover!=""){
						  echo "<img src='" . Yii::app()->request->baseUrl . "/images/photo_album/" . $model->album_cover . "' width='500px' />";
					  }
					  ?>
                        <p class="txt_red">* ภาพไม่เกิน 1x1 Pixel และขนาดไม่เกิน 1 MB.</p></td>
                    </tr>
                    <tr>
                      <td width="150" valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150" valign="top">&nbsp;</td>
                      <td valign="top"><input type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="Submit button">&nbsp;<a onclick="document.theform.reset();return false;"
href="javascript:history.go(-1)"><img style="vertical-align:top" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" alt="Clear"></a></td>
                    </tr>
                  </table>
                  
       <?php $this->endWidget(); ?>           
                  </td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
              
                <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'photo_list',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
              
              
              
     <?php $this->endWidget(); ?> 
              </td>
            </tr>
          </table>
        