<?php 
$this->pageTitle="Admin Panel::Organize"; 
?>
<script language="javascript">
function changeit(obj,objId)
{
	if($(obj).val()!=""){
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/ChangeJobbedStatus/',{
								id:objId,
								status:$(obj).val(),
								},function(data){
									if(data=="OK")
									{
										location.reload();
									}else{
										alert(data);
									}
								});
	}
}

function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DeleteJobbee/',{id:objId},function(data){
																											  if(data=="OK")
								
										location.reload();
																											  });
			   }
}
function deleteitAll()
	{
	
		if(confirm("ต้องการลบรายการที่เลือกใช่หรือไม่")){
			$("#act").val('deleteAll');
			$("#frm_notice").submit();
		}
	}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ข้อมูลผู้สมัคร</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ข้อมูลผู้สมัคร</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td> <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jobbee',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                      <br />
                      <table width="700" border="0" align="center" cellpadding="2" cellspacing="3" class="form1">
                        <tr>
                          <td width="120"><div align="right"><span class="h1">ตำแหน่ง :</span></div></td>
                          <td width="280">
                          <?php echo $form->dropDownList($model,'jobid', CHtml::listData(Joblist::model()->findAll(array('order' => 'position ASC')), 'id', 'position'), array('empty'=>'เลือกตำแหน่ง',	 )); ?>
                          </td>
                          <td width="120"><div align="right"><span class="h1">คำค้น :</span></div></td>
                          <td><?php echo $form->textField($model,'key_word'); ?></td>
                        </tr>
                        <tr>
                          <td width="120"><div align="right"><span class="h1">วันที่สมัคร :</span></div></td>
                          <td width="280"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Jobbee[start_date]',
					'value'=>$model->start_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td>
                          <td width="120"><div align="right"><span class="h1">ถีง :</span></div></td>
                          <td width="280"><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Jobbee[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?></td>
                        </tr>
                        <tr>
                          <td width="120"><div align="right"><span class="h1">สถานะ :</span></div></td>
                          <td width="280">
                          <?php
echo $form->dropDownList($model, 'status',array('' => '-- เลือกสถานะ --','0'=>'รอเรียกสัมภาษณ์','1'=>'สัมภาษณ์แล้ว','2'=>'รับเข้าทำงานแล้ว','3'=>'ไม่รับ'));
																						  ?>
                          </td>
                          <td align="right">&nbsp;</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td width="120" colspan="4" align="center">
                          <input type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_search.png" alt="Submit button"></td>
                        </tr>
                        <tr>
                          <td colspan="4">&nbsp;</td>
                          </tr>
                      </table>
                  <?php $this->endWidget(); ?>
                  </td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="3" cellpadding="3">
                    <tr>
                      <td class="nparesult_table_header"><table width="100%" border="0" cellpadding="3" cellspacing="3">
                        <tr style="background-color:#78CF2D;color:white;">
                          <td width="36" align="center" valign="top" class="">เลือก</td>
                          <td width="37" align="center" valign="top" class="txt_bold">ลำดับ</td>
                          <td width="69" align="center" valign="top" class="txt_bold">รูป</td>
                          <td width="100" align="center" valign="top" class="txt_bold">ชื่อ</td>
                          <td width="85" align="center" valign="top" class="txt_bold">อายุ</td>
                          <td width="24" align="center" valign="top" class="txt_bold">เพศ</td>
                          <td width="92" align="center" valign="top" class="txt_bold">เบอร์โทร</td>
                          <td width="115" align="center" valign="top" class="txt_bold">Email</td>
                          <td width="135" align="center" valign="top" class="txt_bold">ตำแหน่ง</td>
                          <td width="115" align="center" valign="top" class="txt_bold">วันที่สมัคร</td>
                          <td width="126" align="center" valign="top" class="txt_bold">สถานะ</td>
                          <td width="19" align="center" valign="top" class="txt_bold">ลบ</td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td class="nparesult_table_content">
                      <?php 
					
					$form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_notice',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input name="act" type="hidden" id='act'/>
                      <table width="100%" border="0" cellpadding="0" cellspacing="2">
                      <?php
					  $i=1;
					  foreach($data as $row){
					  ?>
                        <tr>
                          <td class="rowa" width="36" align="center" valign="top">
                          <input type="checkbox" name="jobbee_id[]" id="checkbox" value="<?php echo $row->id;?>" /></td>
                          <td width="37" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i++;?></span></td>
                          <td class="rowa"  width="69" align="center" valign="top">
                          <?php if($row->apply_image!=""){
						  		?>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/attach/jobs/<?php echo $row->apply_image;?>" width="70" height="64" />
                                <?php
						  }else{
						  ?>
                          <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/nopic.jpg" width="70" height="64" />
                          <?php }?>
                          <?php
						  if($row->resume != "")
						  {
							  ?>
                              <a href='<?php echo Yii::app()->request->baseUrl; ?>/attach/jobs/<?php echo $row->resume;?>' target="_blank">Resume</a>
                              <?php
						  }
						  ?>
                          </td>
                          <td class="rowa" width="160" align="center" valign="top"><?php echo $row->fullname;?></td>
                          <td class="rowa" width="51" align="center" valign="top"><?php echo $row->age;?></td>
                          <td class="rowa" width="50" align="center" valign="top"><?php echo $row->sex;?></td>
                          <td class="rowa" width="78" align="center" valign="top"><?php echo $row->phoneno;?></td>
                          <td class="rowa" width="77" align="center" valign="top"><?php echo $row->email;?></td>
                          <td class="rowa" width="135" align="center" valign="top"><?php echo $row->jobs['position'];?></td>
                          <td class="rowa" width="115" align="center" valign="top"><?php echo $row->create_date;?></td>
                          <td class="rowa" width="126" align="center" valign="top">
                          <select name="select3" class="textb" id="status_<?php echo $row->id;?>" onchange='changeit(this,"<?php echo $row->id;?>")' style="width:120px">
                            <option value="">- เลือกสถานะ -</option>
                            <option value="0" <?php echo ($row->status==0? " selected='selected' ": "")?>>รอเรียกสัมภาษณ์</option>
                            <option value="1" <?php echo ($row->status==1? " selected='selected' ": "")?>>สัมภาษณ์แล้ว</option>
                            <option value="2" <?php echo ($row->status==2? " selected='selected' ": "")?>>รับเข้าทำงานแล้ว</option>
                            <option value="3" <?php echo ($row->status==3? " selected='selected' ": "")?>>ไม่รับ</option>
                          </select></td>
                          <td class="rowa" width="19" align="center" valign="top"><a href='javascript:deleteit("<?php echo $row->id;?>")'>ลบ</a></td>
                        </tr>
                       <?php
					  }
					  ?>
                      </table>
                      <?php $this->endWidget(); ?>
                      </td>
                    </tr>
                    <tr>
                      <td class="nparesult_table_content" align="center"><br />
                       &nbsp;<a href="javascript:deleteitAll()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" width="52" height="25" /></a> </td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
            </tr>
          </table>