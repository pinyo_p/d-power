<?php 
$this->pageTitle="Admin Panel::Setting "; 
function check_menu2($menu_id)
{
	$group_id = Yii::app()->user->getValue("group_id");
	if($group_id=="16")
	{
		return " style='visibility:hidden;position:absolute' ";
	}
	else
	{
		return "";
	}
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
</script>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'setting',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">ตั้งค่าเวบไซต์</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ตั้งค่าเวบไซต์</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td>
				<center>
                <table>
                <tr style="background-color:#78CF2D;color:white;">
                <td colspan="2" style="padding:10px;font-size:14px;font-weight:bold;">
                Twitter
                </td>
                </tr><tr><td>
                
                <table width="500" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                  <td class="nparesult_table_content"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:1px solid #CCC;border-left:1px solid #CCC">
                    <tr>
                      <td align="right" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">สถานะ <span class="txt_red">*</span> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><input type="radio" name="rd_twitter_status" id="radio" value="1" <?php echo ($model->twitter_show=="1"?" checked='checked' ":"");?> /> 
                        แสดง 
                          <input name="rd_twitter_status" type="radio" id="radio2" value="0" <?php echo ($model->twitter_show=="0"?" checked='checked' ":"");?> /> 
                          ไม่แสดง</td>
                    </tr>
                    <tr>
                      <td width="150" align="right" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">ลิงค์      <span class="txt_red">*</span> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><p>
                        <input type="text" name="twitter_link" id="textfield" value='<?php echo $model->twitter_url;?>'/>
                        </p></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">ไอคอน      <span class="txt_red">*</span> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="50%" height="50" valign="middle"><input type="radio" name="twitter_icon" id="radio3" value="1" <?php echo ($model->twitter_icon==1?" checked='checked' ":"");?>/> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/twitter_01.jpg" width="32" height="33" /></td>
                          <td width="50%" height="50" valign="middle"> <input type="radio" name="twitter_icon" id="radio4" value="2" <?php echo ($model->twitter_icon==2?" checked='checked' ":"");?>/>
                            <img src="<?php echo Yii::app()->request->baseUrl;?>/images/twitter_03.jpg" width="26" height="33" /></td>
                        </tr>
                        <tr>
                          <td width="50%" height="50" valign="middle"><input type="radio" name="twitter_icon" id="radio5" value="3" <?php echo ($model->twitter_icon==3?" checked='checked' ":"");?>/>
                            <img src="<?php echo Yii::app()->request->baseUrl;?>/images/twitter_05.jpg" width="80" height="18" /></td>
                          <td width="50%" height="50" valign="middle"><input type="radio" name="twitter_icon" id="radio6" value="4" <?php echo ($model->twitter_icon==4?" checked='checked' ":"");?>/>
                            <img src="<?php echo Yii::app()->request->baseUrl;?>/images/twitter_06.jpg" width="37" height="37" /></td>
                        </tr>
                        <tr>
                          <td width="50%" height="50" valign="middle"><input type="radio" name="twitter_icon" id="radio7" value="5"<?php echo ($model->twitter_icon==5?" checked='checked' ":"");?> />
                            <img src="<?php echo Yii::app()->request->baseUrl;?>/images/twitter_08.jpg" width="38" height="27" /></td>
                          <td width="50%" height="50" valign="middle"><input type="radio" name="twitter_icon" id="radio8" value="6" <?php echo ($model->twitter_icon==6?" checked='checked' ":"");?>/>
                            <img src="<?php echo Yii::app()->request->baseUrl;?>/images/twitter_11.jpg" width="43" height="43" /></td>
                        </tr>
                        <tr>
                          <td width="50%" height="50" valign="middle"><input type="radio" name="twitter_icon" id="radio9" value="7" />
                            <img src="<?php echo Yii::app()->request->baseUrl;?>/images/twitter_13.jpg" width="38" height="43" /></td>
                          <td width="50%" height="50" valign="middle">&nbsp;</td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
                <tr style="background-color:#78CF2D;color:white;">
                <td colspan="2" style="padding:10px;font-size:14px;font-weight:bold;">
                Facebook
                </td>
                </tr><tr><td>
                
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:1px solid #CCC;border-left:1px solid #CCC">
                    <tr>
                      <td align="right" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">สถานะ <span class="txt_red">*</span> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><input type="radio" name="face_book_show" id="radio" value="1" <?php echo ($model->facebook_show=="1"?" checked='checked' ":"");?>/> 
                        แสดง 
                          <input name="face_book_show" type="radio" id="radio2" value="0" <?php echo ($model->facebook_show=="0"?" checked='checked' ":"");?>/> 
                          ไม่แสดง</td>
                    </tr>
                    <tr>
                      <td width="150" align="right" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">ลิงค์      <span class="txt_red">*</span> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><p>
                        <input type="text" name="face_book_link" id="textfield"  value='<?php echo $model->facebook_url;?>'/>
                        </p></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">ไอคอน      <span class="txt_red">*</span> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td height="50" valign="middle"><input type="radio" name="facebook_icon" id="radio3" value="1"  <?php echo ($model->facebook_icon==1?" checked='checked' ":"");?>/> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/fb_icon1.jpg" width="33" height="33" /></td>
                          <td height="50" valign="middle"> <input type="radio" name="facebook_icon" id="radio4" value="2" <?php echo ($model->facebook_icon==2?" checked='checked' ":"");?>/>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/fb_icon2.jpg" width="33" height="33" /></td>
                        </tr>
                        <tr>
                          <td height="50" valign="middle"><input type="radio" name="facebook_icon" id="radio5" value="3" <?php echo ($model->facebook_icon==3?" checked='checked' ":"");?>/>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/fb_icon3.jpg" width="40" height="41" /></td>
                          <td height="50" valign="middle"><input type="radio" name="facebook_icon" id="radio6" value="4" <?php echo ($model->facebook_icon==4?" checked='checked' ":"");?>/>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/fb_icon4.jpg" width="46" height="46" /></td>
                        </tr>
                        <tr>
                          <td height="50" valign="middle"><input type="radio" name="facebook_icon" id="radio7" value="5" <?php echo ($model->facebook_icon==5?" checked='checked' ":"");?>/>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/fb_icon5.jpg" width="93" height="35" /></td>
                          <td height="50" valign="middle"><input type="radio" name="facebook_icon" id="radio8" value="6" <?php echo ($model->facebook_icon==6?" checked='checked' ":"");?>/>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/fb_icon6.jpg" width="89" height="35" /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
                
                </td>
                </tr>
                <tr style="background-color:#78CF2D;color:white;">
                <td colspan="2" style="padding:10px;font-size:14px;font-weight:bold;">
                Webmail
                </td>
                </tr><tr><td>
                
                
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:1px solid #CCC;border-left:1px solid #CCC">
                    <tr>
                      <td align="right" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">สถานะ </span><span class="txt_red">*</span><span class="txt_green txt_bold"> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><input type="radio" name="webmail_show" id="radio" value="1" <?php echo ($model->webmail_show=="1"?" checked='checked' ":"");?>/> 
                        แสดง 
                          <input name="webmail_show" type="radio" id="radio2" value="0" <?php echo ($model->webmail_show=="0"?" checked='checked' ":"");?>/> 
                          ไม่แสดง</td>
                    </tr>
                    <tr>
                      <td width="150" align="right" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">ลิงค์      <span class="txt_red">*</span> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><p>
                        <input type="text" name="webmail_link" id="textfield"  value='<?php echo $model->webmail_link;?>'/>
                        </p></td>
                    </tr>
                    <tr>
                      <td align="right" valign="top" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">ไอคอน <span class="txt_red">*</span> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><input type="file" name="webmail_file" id="fileField" /><?php 
					  if($model->webmail_icon!=""){
						  echo "<img src='" . Yii::app()->request->baseUrl . '/images/icon/' . $model->webmail_icon . "' />";
					  }
					  ?></td>
                    </tr>
                  </table>
                
                
                </td>
                </tr>
                
                <tr style="background-color:#78CF2D;color:white;">
                <td colspan="2" style="padding:10px;font-size:14px;font-weight:bold;">
                ตัววิ่ง
                </td>
                </tr><tr><td>
                
                
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:1px solid #CCC;border-left:1px solid #CCC">
                    <tr>
                      <td align="right" bgcolor="#f8f8f8"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><span class="txt_green txt_bold">ความเร็ว</span><span class="txt_red">*</span><span class="txt_green txt_bold"> :</span></td>
                      <td align="left"  style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;"><input type="text" name="cop_marquee_speed" id="radio" value='<?php echo $model->cop_marquee_speed;?>' /> </td>
                    </tr>
                    
                    
                  </table>
                
                
                </td>
                </tr>
                
                
                 <tr style="background-color:#78CF2D;color:white;">
                <td colspan="2" style="padding:10px;font-size:14px;font-weight:bold;">
                แลกลิ้ง
                </td>
                </tr><tr><td>
                
                
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:1px solid #CCC;border-left:1px solid #CCC">
                    <tr>
                      
                      <td align="center" colspan='2' style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;">
                      <a href='<?php echo  Yii::app()->request->baseUrl ;?>/index.php/Admin/LinkExchange' class="link_search">
                      ดูรายละเอียด
                      </a>
                       </td>
                    </tr>
                    
                    
                  </table>
                
                
                </td>
                </tr>
                
                <tr style="background-color:#78CF2D;color:white;">
                <td colspan="2" style="padding:10px;font-size:14px;font-weight:bold;">
                แสดงจำนวนผู้อ่านบทความ
                </td>
                </tr><tr><td>
                
                
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:1px solid #CCC;border-left:1px solid #CCC">
                    <tr>
                      
                      <td align="center" colspan='2' style="border-bottom:1px solid #CCC;border-right:1px solid #CCC; padding:10px;">
                     
                      <input type="checkbox" value="1" name="show_view_count" <?php echo ($model->show_view_count==1?" checked='checked' ":"");?> /><label for="show_view_count" > แสดงจำนวนผู้อ่านบทความ</label>
                     
                       </td>
                    </tr>
                    
                    
                  </table>
                
                
                </td>
                </tr>
                
                <tr>
                  <td align="center" class="nparesult_table_content"><a href="javascript:$('#setting').submit()"   <?php Yii::hideit();?> class="lmm_20_2"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" width="52" height="25" /> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a></td>
                </tr>
              </table>
                
                </td>

                </tr> 
               
                <tr>
                <td>
                </td>


                </table>
                </center>
                
              </td>
            </tr>
           
          </table>

<?php $this->endWidget(); ?>
 