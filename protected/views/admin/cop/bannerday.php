<?php 
$this->pageTitle="Admin Panel:Banner Day"; 
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	$(function() {
		$( "#tabs" ).tabs();
	});
	function submitme()
	{
		var nicE_en = new nicEditors.findEditor('Disclaimer_disclaimer_en');
		var nicE_th = new nicEditors.findEditor('Disclaimer_disclaimer_th');
		content_en = nicE_en.getContent();
		content_th = nicE_th.getContent();
		$("#Disclaimer_disclaimer_en").val(content_en);
		$("#Disclaimer_disclaimer_th").val(content_th);
		$("#Disclaimer").submit();
		
	}
	function setselected(objId)
{
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/SetSelectedBDay/',{
								id:objId,
								},function(data){
									if(data=="OK")
									{
										$('#bday_main').submit();
									}else{
										alert(data);
									}
								});
}

function updatebdaystatus()
{
	var bstatus=0;
	if($('#show_banner_day1').is(':checked')){
		bstatus=1
	}
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/updateBDayStatus/',{
								status:bstatus,
								},function(data){
									if(data=="OK")
									{
										$('#bday_main').submit();
									}else{
										alert(data);
									}
								});
}
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DeleteIndexFile/',{id:objId},function(data){
																											  if(data=="OK")
								
										$('#bday_main').submit();
																											  });
			   }
}
</script>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bday_main',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">แบนเนอร์วันสำคัญ</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><br />
                <br />
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">แบนเนอร์วันสำคัญ รูปแบบที่ใช้อยู่ปัจจุบัน</div></td>
                      <td width="5">
                      
                      <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="50%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center"><br />
                    <input type="radio" name="show_banner_day" id="show_banner_day1" value="radio" <?php echo ($param->setting->show_banner_day==1?" checked='checked' ":"");?>/>
                    <span class="txt_bold">เปิดระบบ</span>
                    <input type="radio" name="show_banner_day" id="show_banner_day2" value="radio"<?php echo ($param->setting->show_banner_day==0?" checked='checked' ":"");?> />
                    <span class="txt_bold">ปิดระบบ</span><br />
<a href="javascript:updatebdaystatus()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="" width="52" height="25" /></a></td>
                </tr>
              </table>
                <br />
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center">
                   <?php
				  $bday = CopIndex::model()->find("part='COP' and `group`>='2001' and `group`<='2019' and is_selected=1");
				  if(count($bday)>0){
				  ?>
                  <img src="<?php echo Yii::app()->request->baseUrl;?>/images/COP/index/<?php echo $bday->imgsrc;?>"  width="975" /><br />
                  <?php }?>
                    &nbsp;<br />
                    <br /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                  <td class="topix_header"><div class="topix_headtxt">เปลี่ยนแบนเนอร์วันสำคัญ</div></td>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                </tr>
              </table>
                <br />
                <br /></td>
            </tr>
            <tr>
              <td align="center">เลือกเดือนเพื่อแสดงวันสำคัญ , เทศกาล
                 <select name="group" class="textb" onchange="javascript:$('#bday_main').submit()" id="select2" >
                     <option value="2003" <?php echo ($param->group=="2003"?" selected='selected' ":"");?>>วันปีใหม่</option>
                 	<option value="2004" <?php echo ($param->group=="2004"?" selected='selected' ":"");?>>วันเด็ก</option>
                    <option value="2005" <?php echo ($param->group=="2005"?" selected='selected' ":"");?>>วันตรุษจีน</option>
                 	<option value="2006" <?php echo ($param->group=="2006"?" selected='selected' ":"");?>>วันวาเลนไทน์</option>
                    <option value="2007" <?php echo ($param->group=="2007"?" selected='selected' ":"");?>>วันสงกรานต์</option>
                 	<option value="2008" <?php echo ($param->group=="2008"?" selected='selected' ":"");?>>วันสุนทรภู่</option>
                      <option value="2002" <?php echo ($param->group=="2002"?" selected='selected' ":"");?>>วันเฉลิมพระชนมพรรษา 12 สิงหา</option>
                      <option value="2011" <?php echo ($param->group=="2011"?" selected='selected' ":"");?>>วันปิยะมหาราช</option>
                      <option value="2009" <?php echo ($param->group=="2009"?" selected='selected' ":"");?>>วันลอยกระทง</option>
                      <option value="2001" <?php echo ($param->group=="2001"?" selected='selected' ":"");?>>วันเฉลิมพระชนมพรรษา 5 ธันวา</option>
                      <option value="2010" <?php echo ($param->group=="2010"?" selected='selected' ":"");?>>วันคริสมาสต์</option>
                    </select>
                <br />
                &nbsp;</td>
            </tr>
            <tr>
              <td>
              
              <?php
				  		$i=1;
				  	foreach($data as $row){
				  ?>
              
              <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                
                <tr>
                  <td align="center">
                 
                  <img src="<?php echo Yii::app()->request->baseUrl;?>/images/COP/index/<?php echo $row->imgsrc;?>" width="975"  /><br />
                    &nbsp;</td>
                </tr>
                <tr>
                  <td align="center"><input type="radio" name="radio" id="radio" value="radio"  <?php echo ($row->is_selected=="1"?" checked='checked' ":"");?>/>
                    <label for="radio"></label>
                    แบนเนอร์รูปแบบที่ <?php echo $i;?><br />
                    <br /></td>
                </tr>
                <tr>
                    <td>
                    <center>
                    Publish Date :
                    <input type="text" style="width:70px" />&nbsp;<img src='<?php echo Yii::app()->request->baseUrl;?>/images/icon/rdDatePicker.gif' />
                    </center>
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <center>
                    End Date :
                    <input type="text" style="width:70px"  />&nbsp;<img src='<?php echo Yii::app()->request->baseUrl;?>/images/icon/rdDatePicker.gif' />
                    </center>
                    </td>
                    </tr>
                <tr>
                  <td align="center"><a href="javascript:setselected('<?php echo $row['id'];?>')"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="" width="52" height="25" /></a>&nbsp;<a href="javascript:deleteit('<?php echo $row->id;?>')"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" alt="" width="52" height="25" /></a></td>
                </tr>
              </table>
                <br />
                <br />
                <?php
						 
						  $i++;
					}
					?>
                </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                  <td class="topix_header"><div class="topix_headtxt">ใช้แบนเนอร์ที่สร้างเอง</div></td>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td align="center">
                <br />
                <?php $this->endWidget(); ?>
                <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bday_new_image',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                <table width="555" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="120" align="right">เลือกเดือน :</td>
                    <td align="left"> <select name="group" class="textb" id="select2" >
                     <option value="2003" <?php echo ($param->group=="2003"?" selected='selected' ":"");?>>วันปีใหม่</option>
                 	<option value="2004" <?php echo ($param->group=="2004"?" selected='selected' ":"");?>>วันเด็ก</option>
                    <option value="2005" <?php echo ($param->group=="2005"?" selected='selected' ":"");?>>วันตรุษจีน</option>
                 	<option value="2006" <?php echo ($param->group=="2006"?" selected='selected' ":"");?>>วันวาเลนไทน์</option>
                    <option value="2007" <?php echo ($param->group=="2007"?" selected='selected' ":"");?>>วันสงกรานต์</option>
                 	<option value="2008" <?php echo ($param->group=="2008"?" selected='selected' ":"");?>>วันสุนทรภู่</option>
                      <option value="2002" <?php echo ($param->group=="2002"?" selected='selected' ":"");?>>วันเฉลิมพระชนมพรรษา 12 สิงหา</option>
                      <option value="2011" <?php echo ($param->group=="2011"?" selected='selected' ":"");?>>วันปิยะมหาราช</option>
                      <option value="2009" <?php echo ($param->group=="2009"?" selected='selected' ":"");?>>วันลอยกระทง</option>
                      <option value="2001" <?php echo ($param->group=="2001"?" selected='selected' ":"");?>>วันเฉลิมพระชนมพรรษา 5 ธันวา</option>
                      <option value="2010" <?php echo ($param->group=="2010"?" selected='selected' ":"");?>>วันคริสมาสต์</option>
                    </select></td>
                  </tr>
                  <tr>
                    <td align="right">เลือกไฟล์แบนเนอร์ : </td>
                    <td align="left"><input type="file" name="file" id="file"  />
&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="120" align="center"><br /></td>
                    <td align="left"><span class="txt_red">รองรับไฟล์ .JPG , .GIF , .PNG ขนาดไม่เกิน 1MB</span><br /></td>
                  </tr>
                  <tr>
                    <td width="120" align="center">&nbsp;</td>
                    <td align="left"><p><br />
                      อัตราส่วนกว้าง x สูง  = 972 x 75 pixel<br />
                      </p></td>
                  </tr>
                </table>
                <?php $this->endWidget(); ?>
                <a href="javascript:$('#bday_new_image').submit()">
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="" width="52" height="25" /></a>&nbsp;<a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a><br />
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          