<?php 
$this->pageTitle="Admin Panel::About Sam"; 
?>
<script language="javascript">
function deletealbum(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteAlbum/');?>',{id:objId},function(data){
																							   $("#photo_list").submit();
																							   });
	}
}
function deleteimage(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteImage/');?>',{id:objId},function(data){
																							   $("#photo_list").submit();
																							   });
	}
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt;<a href="#" class="link_green"> Corporate</a> &gt; Video<a href="#" class="link_green"></a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">Video</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><table width="60%" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                  <td class="nparesult_table_content">
                  
                  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_album',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                  
                  <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3">
                        <tr>
                          <td class="nparesult_table_content"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td valign="top"><span class="txt_green txt_bold">ชื่อรูปภาพ   :</span></td>
                              <td align="left" valign="top"><?php echo $form->textField($model,'video_name'); ?></td>
                              <td valign="top">&nbsp;</td>
                              </tr>
                               <tr>
                              <td valign="top"><span class="txt_green txt_bold">URL  :</span></td>
                              <td align="left" valign="top"><?php echo $form->textField($model,'video_url'); ?></td>
                              <td valign="top">&nbsp;</td>
                              </tr>
                            <tr>
                              <td valign="top"><span class="txt_green txt_bold">คำอธิบายภาพ    :</span></td>
                              <td align="left" valign="top"><?php echo $form->textArea($model,'video_desc',array('rows'=>4,'cols'=>30,'style'=>'width:380px;height:150px')); ?></td>
                              <td valign="top">&nbsp;</td>
                              </tr>
                            <tr>
                              <td width="150" valign="top"><span class="txt_green txt_bold">เลือกรูปภาพ   :</span></td>
                              <td width="300" align="left" valign="top">
                              <?php
								  if($model->video_cover!=""){
									  echo "<img src='" . Yii::app()->request->baseUrl . "/images/video_album/" . $model->video_cover . "' width='100' /><br />";
								  }
							?>
                               
                              <p>
                                <input type="file" name="image" id="image" />
                                </p>
                                <p class="txt_red">* ภาพไม่เกิน 1x1 Pixel และขนาดไม่เกิน 1 MB.</p></td>
                              <td valign="top">&nbsp;</td>
                              </tr>
                            </table></td>
                        </tr>
                        <tr>
                          <td class="nparesult_table_content">&nbsp;</td>
                        </tr>
                        
                        <tr>
                          <td align="center" class="nparesult_table_content">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center" class="nparesult_table_content"><input type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" alt="Submit button">&nbsp;<a onclick="document.theform.reset();return false;"
href="#"><img style="vertical-align:top" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" alt="Clear"></a></td>
                        </tr>
                        </table>
                  
       <?php $this->endWidget(); ?>           
                  </td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
              
                <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'photo_list',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
              
              
              
     <?php $this->endWidget(); ?> 
              </td>
            </tr>
          </table>
        