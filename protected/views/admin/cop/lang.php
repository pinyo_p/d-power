<?php 
$this->pageTitle="Admin Panel::Disclaimer"; 
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	$(function() {
		$( "#tabs" ).tabs();
	});
	function submitme()
	{
		var nicE_en = new nicEditors.findEditor('Disclaimer_disclaimer_en');
		var nicE_th = new nicEditors.findEditor('Disclaimer_disclaimer_th');
		content_en = nicE_en.getContent();
		content_th = nicE_th.getContent();
		$("#Disclaimer_disclaimer_en").val(content_en);
		$("#Disclaimer_disclaimer_th").val(content_th);
		$("#Disclaimer").submit();
		
	}
</script>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'lang_main',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">ภาษา</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><br />
                <br />
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ตั้งค่าภาษาในหน้าเว็บ</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                </table></td>
            </tr>
            <tr>
              <td><br />
              <?php 
				$setting = $param->setting;
			  ?>
                <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0"></
                <tr>
                  <td align="center"><input type="checkbox" name="chk_show_text" <?php echo ($setting->lang_use_text=="1"?" checked='checked' ":"");?>id="radio8" value="radio" />
                    <span class="txt_bold">แสดงข้อความ</span>
                    <input type="checkbox" name="chk_show_icon" id="radio9" value="radio" <?php echo ($setting->lang_use_icon=="1"?" checked='checked' ":"");?>/>
                    <span class="txt_bold">แสดงไอคอน</span></td>
                </tr>
              </table>
                <br />
                <table width="80%" border="0" align="center" cellpadding="3" cellspacing="3">
                  <tr>
                    <td class="nparesult_table_header">
                    <table width="100%" border="0" cellpadding="3" cellspacing="3">
                       <tr style="background-color:#78CF2D;color:white;">
                        <td width="24%" align="center" valign="top" class="txt_bold">ภาษา</td>
                        <td width="30%" align="center" valign="top" class="txt_bold">ไอคอนภาษา</td>
                        <td width="25%" align="center" valign="top" class="txt_bold">ภาษาหน้าเว็บไซต์</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td class="nparesult_table_content"><table width="100%" border="0" cellpadding="0" cellspacing="2">
                    <?php 
					foreach($data as $row){
						?>
                      <tr>
                        <td width="10%" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $row->lang_txt;?></span></td>
                        <td class="rowa"  width="15%" align="center" valign="top"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/flag/<?php echo $row->lang_icon;?>" width="16" height="11" /></td>
                        <td class="rowa" width="10%" align="center" valign="top"><input name="default_lang" value="<?php echo $row->id;?>" type="radio" id="checkbox" <?php echo ($setting->default_lang==$row->id ?" checked='checked' ":"");?> />
                          Default</td>
                      </tr>
                      <?php
					}
					?>
                     
                    </table></td>
                  </tr>
                  <tr>
                    <td class="nparesult_table_content" align="center"><br />
                      <a href="javascript:$('#lang_main').submit()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" width="52" height="25"  <?php Yii::hideit();?> class="lmm_19_2"/></a> &nbsp;<a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a></td>
                  </tr>
                </table>
                <br /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                  <td class="topix_header"><div class="topix_headtxt">ใช้ไอคอนที่สร้างเอง</div></td>
                  <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                  </tr>
                </table></td>
            </tr>
            <?php $this->endWidget(); ?>
            <tr>
              <td align="center">
                <br />
                <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'newicon',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                <table width="400" border="0" align="center" cellpadding="0" cellspacing="4">
                  <tr>
                    <td width="120" align="right">เลือกภาษา :</td>
                    <td align="left"><select name="lang_icon" class="textb" id="select2" style="width:100px">
                      <option value="1">ภาษาไทย</option>
                      <option value="2">ภาษาอังกฤษ</option>
                    </select></td>
                  </tr>
                  <tr>
                    <td align="right">เลือกไฟล์ไอคอน : </td>
                    <td align="left"><input type="file" name="file" id="file"  />
&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="120" align="center"><br /></td>
                    <td align="left"><span class="txt_red">รองรับไฟล์ .JPG , .GIF , .PNG ขนาดไม่เกิน 1MB</span><br /></td>
                  </tr>
                  <tr>
                    <td width="120" align="center">&nbsp;</td>
                    <td align="left"><p><br />
                      อัตราส่วนกว้าง x สูง  = 17 x 11 pixel<br />
                      </p></td>
                  </tr>
                </table>
                <a href="javascript:$('#newicon').submit()"  <?php Yii::hideit();?> class="lmm_19_2"><br />
                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="" width="52" height="25" /></a>&nbsp;<a href="#">&nbsp;<img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a><br />
              </form></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <?php $this->endWidget(); ?>