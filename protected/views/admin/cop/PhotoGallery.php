<?php 
$this->pageTitle="Admin Panel::About Sam"; 
?>
<script language="javascript">
function deletealbum(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteAlbum/');?>',{id:objId},function(data){
																							   $("#photo_list").submit();
																							   });
	}
}
function deleteimage(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->createUrl('/admin/DeleteImage/');?>',{id:objId},function(data){
																							   $("#photo_list").submit();
																							   });
	}
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt;<a href="#" class="link_green"> Corporate</a> &gt; <a href="#" class="link_green">Photo Gallery</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">Photo Gallery</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><table width="60%" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                  <td class="nparesult_table_content">
                  
                  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_album',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="150" valign="top" align="right"><span class="txt_green txt_bold">เพิ่มอัลบั้ม :</span></td>
                      <td valign="top"><?php echo $form->textField($model,'album_name'); ?></td>
                    </tr>
                    <tr>
                    <td align="right"><span class="txt_green txt_bold">
                    Publish Date :</span>
                    </td>
                    <td>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'PhotoAlbum[publish_date]',
					'value'=>$model->publish_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                    </td>
                    </tr>
                    <tr>
                    <td align="right">
                    <span class="txt_green txt_bold">End Date :</span>
                    </td>
                    <td>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'PhotoAlbum[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                    </td>
                    </tr>
                    <tr>
                      <td width="150" valign="top" align="right"><span class="txt_green txt_bold">รูปปกอัลบั้ม  :</span></td>
                      <td valign="top"><p>
                        <input type="file" name="fileField" id="fileField" />
                      </p>
                        <p class="txt_red">* ภาพไม่เกิน 1x1 Pixel และขนาดไม่เกิน 1 MB.</p></td>
                    </tr>
                    <tr>
                      <td width="150" valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="150" valign="top">&nbsp;</td>
                      <td valign="top"><input type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" alt="Submit button">&nbsp;<a onclick="document.theform.reset();return false;"
href="#"><img style="vertical-align:top" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" alt="Clear"></a></td>
                    </tr>
                  </table>
                  
       <?php $this->endWidget(); ?>           
                  </td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                  <td class="nparesult_table_header"><table width="100%" border="0" cellpadding="3" cellspacing="3">
                    <tr>
                      <td width="10%" align="center" valign="top" class="txt_bold">ลำดับ</td>
                      <td width="20%" align="center" valign="top" class="txt_bold">รูปปกอัลบั้ม</td>
                      <td align="center" valign="top" class="txt_bold">ชื่ออัลบั้ม</td>
                      <td width="6%" align="center" valign="top" class="txt_bold">แก้ไข</td>
                      <td width="6%" align="center" valign="top" class="txt_bold">ลบ</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td class="nparesult_table_content"><table width="100%" border="0" cellpadding="0" cellspacing="2">
                    <?php
					$i=1;
					foreach($data as $row){
					?>
                    <tr>
                      
                      <td width="10%" align="center" valign="top" class="rowb"><span class="txt_bold"><?php echo $i++;?></span></td>
                      <td  width="20%" align="center" valign="top" class="rowb"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/photo_album/<?php echo $row->album_cover ;?>" width="90" /></td>
                      <td align="center" valign="top" class="rowb"><span class="rowa"><?php echo $row->album_name;?></span></td>
                      <td width="6%" align="center" valign="top" class="rowb"><span class="rowa"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Admin/EditAlbum/<?php echo $row->id;?>">แก้ไข</a></span></td>
                      <td width="6%" align="center" valign="top" class="rowb"><a href="javascript:deletealbum('<?php echo $row->id;?>')">ลบ</a></td>
                    </tr>
                    <?php
					}
					?>
                  </table></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
              
                <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'photo_list',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
              
              <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="nparesult_table_content"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">
                        <table border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="center" valign="top" class="">ภาพในอัลบั้ม : </td>
                            <td align="center" valign="top" class="txt_bold"><select name="album" id="select2" onchange="$('#photo_list').submit()">
                            <?php
							foreach($data as $row){
								$selected = "";
								if(isset($_POST['album']) && $row->id == $_POST['album'])
									$selected =  " selected='selected' ";
								?>
                                <option value="<?php echo $row->id;?>" <?php echo $selected;?>><?php echo $row->album_name;?></option>
                                <?php
							}?>
                              
                              </select></td>
                            </tr>
                          </table>
                        </div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                      </tr>
                    </table></td>
                </tr>
                <tr>
                  <td class="nparesult_table_content">&nbsp;</td>
                </tr>
                <tr>
                  <td>
                  
                  
                  <table width="100%" border="0" cellpadding="0" cellspacing="2">
                    <tr class="nparesult_table_header">
                      <td width="50" height="30" align="center" class="rowa">ลำดับ</td>
                      <td height="30" align="center" class="rowa">รูปภาพ</td>
                      <td height="30" align="center" class="rowa">ชื่อรูปภาพ</td>
                      <td height="30" align="center" class="rowa">คำอธิบาย</td>
                      <td width="100" height="30" align="center" class="rowa">แก้ไข</td>
                      <td width="100" height="30" align="center" class="rowa">ลบ</td>
                    </tr>
                    <tr>
                      <td align="center" class="rowa">&nbsp;</td>
                      <td align="center" class="rowa">&nbsp;</td>
                      <td align="center" class="rowa">&nbsp;</td>
                      <td class="rowa" align="center">&nbsp;</td>
                      <td class="rowa" align="center">&nbsp;</td>
                      <td class="rowa" align="center">&nbsp;</td>
                    </tr>
                    <?php 
					$album_id = ((isset($_POST['album']) && $_POST['album']!="")?$_POST['album']:"");
					$image_model = new Photo();
					$image_model->album_id = $album_id;
					$image_data = $image_model->searchByAlbum()->data;
					$i =1;
					foreach($image_data as $row){
					?>
                    <tr>
                      <td width="50" align="center" class="rowa"><span class="txt_bold"><?php echo $i++;?></span></td>
                      <td  width="200" align="center" class="rowa"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/photo_album/<?php echo $row->image_src ;?>" width="170" height="155" /></td>
                      <td width="20%" align="center" class="rowa"><span class="txt_bold"><?php echo $row->image_name; ?></span></td>
                      <td class="rowa" align="center"><span class="txt_bold"><?php echo $row->image_desc; ?></span></td>
                      <td class="rowa" width="100" align="center"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Admin/EditImage/<?php echo $row->id;?>">แก้ไข</td>
                      <td class="rowa" width="100" align="center"><a href="javascript:deleteimage('<?php echo $row->id;?>')">ลบ</a></td>
                    </tr>
                    <?php
					}
					?>
                  </table>
                  
                  
                  </td>
                </tr>
                <tr>
                  <td class="nparesult_table_content">&nbsp;</td>
                </tr>
                <tr>
                  <td class="nparesult_table_content">&nbsp;</td>
                </tr>
                <tr>
                  <td class="nparesult_table_content">&nbsp;</td>
                </tr>
                <tr>
                  <td class="nparesult_table_content"><table width="650" border="0" align="center" cellpadding="3" cellspacing="3">
                    <tr>
                      <td height="30" class="nparesult_table_header">&nbsp;&nbsp;เพิ่มรูปภาพ</td>
                    </tr>
                    <tr>
                      <td class="nparesult_table_content"><table width="100%" border="0" align="center" cellpadding="3" cellspacing="3">
                        <tr>
                          <td class="nparesult_table_content"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td valign="top"><span class="txt_green txt_bold">ชื่อรูปภาพ   :</span></td>
                              <td align="left" valign="top"><input type="text" name="photo_name" id="textfield4" /></td>
                              <td valign="top">&nbsp;</td>
                              </tr>
                            <tr>
                              <td valign="top"><span class="txt_green txt_bold">คำอธิบายภาพ    :</span></td>
                              <td align="left" valign="top"><textarea name="photo_desc" cols="30" rows="5" id="textfield5"></textarea></td>
                              <td valign="top">&nbsp;</td>
                              </tr>
                            <tr>
                              <td width="150" valign="top"><span class="txt_green txt_bold">เลือกรูปภาพ   :</span></td>
                              <td width="300" align="left" valign="top"><p>
                                <input type="file" name="image" id="image" />
                                </p>
                                <p class="txt_red">* ภาพไม่เกิน 1x1 Pixel และขนาดไม่เกิน 1 MB.</p></td>
                              <td valign="top">&nbsp;</td>
                              </tr>
                            </table></td>
                        </tr>
                        <tr>
                          <td class="nparesult_table_content">&nbsp;</td>
                        </tr>
                        
                        <tr>
                          <td align="center" class="nparesult_table_content">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center" class="nparesult_table_content"><input type="image" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" alt="Submit button">&nbsp;<a onclick="document.theform.reset();return false;"
href="#"><img style="vertical-align:top" src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" alt="Clear"></a></td>
                        </tr>
                        </table></td>
                    </tr>
                  </table></td>
                </tr>
              </table>
              
     <?php $this->endWidget(); ?> 
              </td>
            </tr>
          </table>
        