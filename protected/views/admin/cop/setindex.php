﻿<?php 
$this->pageTitle="Admin Panel::Set Index"; 
?>
<script language="javascript">
function setselected(objId)
{
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/SetSelectedIndex/',{
								id:objId,
								},function(data){
									if(data=="OK")
									{
										location.reload();
									}else{
										alert(data);
									}
								});
}
function deleteit(objId)
{
	if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DeleteIndexFile/',{id:objId},function(data){
																											  if(data=="OK")
								
										location.reload();
																											  });
			   }
}
function updateindex(objId)
{
	var txt_publish_date = $("#CopIndex_publish_date_" + objId).val();
	var txt_end_date = $("#CopIndex_end_date_" + objId).val();
	$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/UpdateIndex/',{
								id:objId,
								publish_date:txt_publish_date,
								end_date:txt_end_date,
								
								},function(data){
																											  if(data=="OK")
								
										location.reload();
																											  });
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">พื้นที่หน้าแรก</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">รูปที่ใช้งานอยู่ในพื้นที่หน้าแรก</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td align="center" class="bgsetting"><p ><br />
                    <img src="<?php echo Yii::app()->request->baseUrl;?>/images/banner_bottom.png" id='img_main' /><br />
                      &nbsp;</td>
                </tr>
                <tr>
                  <td align="center"><a href="#"></a></td>
                </tr>
                <tr>
                  <td><br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">รูปภาพที่มีให้เลือก</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
                </tr>
                
                <tr>
                  <td align="center">
                  
                  <?php
				  $i=1;
				  foreach($data as $row){
					  $selected = "";
					  if($row->is_selected=="1")
					  {
						  $selected = " checked='checked' ";
						  ?>
                          <script language="javascript">
						  $(function(){
						  	$('#img_main').attr('src','<?php echo Yii::app()->request->baseUrl;?>/images/COP/index/<?php echo $row->imgsrc;?>');
									 });
						  </script>
                          <?php
					  }
				  ?>
                  
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="center" class="bgsetting"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/COP/index/<?php echo $row->imgsrc;?>" /></td>
                        <td align="center" class="bgsetting"></td>
                        </tr>
                      <tr>
                        <td align="center"><input type="radio" <?php echo $selected;?> name="radio" id="radio" value="radio" onclick="setselected('<?php echo $row->id;?>')" class='lmm_3_4' />
                          <label for="radio" class='lmm_3_4'></label>
รูปแบบที่ <?php echo $i++;?><br /></td>
                        <td align="center"></td>
                        </tr>
                        <tr>
                    <td align="center"><span class="txt_green txt_bold">
                    Publish Date :</span>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'CopIndex_publish_date_' . $row->id ,
					'value'=>$row->publish_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                    </td>
                    </tr>
                    <tr>
                    <td align="center">
                    <span class="txt_green txt_bold">End Date :</span>
                   <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'CopIndex_end_date_' . $row->id,
					'value'=>$row->end_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                    </td>
                    </tr>
                      <tr>
                        <td align="center">
                        <a href='javascript:updateindex("<?php echo $row->id;?>")' <?php Yii::hideit();?> class="lmm_3_2">
                        <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="" width="52" height="25" />
                        </a>
                        <a href="javascript:deleteit('<?php echo $row->id;?>')"   <?php Yii::hideit();?> class="lmm_3_3">
                          <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" alt="" width="52" height="25" /></a></td>
                        <td align="center"></td>
                        </tr>
                     
                      <tr>
                        <td align="center"><p><br />
                          &nbsp;</p>
                          <p>&nbsp;<br />
                          </p></td>
                        <td align="center">&nbsp;</td>
                      </tr>
                     
                    </table>
                    <hr />
                  <?php
				  }
				  ?>
                    </td>
                </tr>
                <tr>
                  <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">อัพโหลดไฟล์</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td align="center">
                    <br />
                    <br />
                    
                    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'CopIndex',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                    
                    <table width="555" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="120" align="right">เลือกไฟล์แบ็คกราวน์ :</td>
                        <td align="left"><input type="file" name="file" id="file"  />
                        <div class="form"><?php echo $form->error($model,'imgsrc'); ?></div>
                          &nbsp;</td>
                      </tr>
                      <tr>
                    <td align="right"><span class=" ">
                    Publish Date :</span>
                    </td>
                    <td>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'CopIndex[publish_date]',
					'value'=>$model->publish_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                    </td>
                    </tr>
                    <tr>
                    <td align="right">
                    <span class=" ">End Date :</span>
                    </td>
                    <td>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'CopIndex[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                    </td>
                    </tr>
                      <tr>
                        <td width="120" align="center"><br /></td>
                        <td align="left"><span class="txt_red">รองรับไฟล์ .JPG , .GIF , .PNG  ขนาดไม่เกิน 1 MB</span><br /></td>
                      </tr>
                      <tr>
                        <td width="120" align="center">&nbsp;</td>
                        <td align="left"><p><br />
                          อัตราส่วนกว้าง x สูง  = 750 x 237 pixel<br />
                          </p></td>
                      </tr>
                    </table>
                    
                   
                    <br />
                    <br />
                    <a href="javascript:$('#CopIndex').submit()"  <?php Yii::hideit();?> class="lmm_3_1"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="" width="52" height="25" /></a>&nbsp;<a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a>
                     <?php $this->endWidget(); ?>
                 </td>
                </tr>
                <tr>
                  <td align="center">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
        