<?php 
$part = Yii::app()->user->getValue("part");
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Manage User"; 
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	
	function deleteit(objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
		{
			$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DeleteNewsLetter/',{
									id:objId,
									},function(data){
										if(data=="OK")
											location.reload();
										else
											alert(data);
									});
		}
	}
	function deleteitAll()
	{
	
		if(confirm("ต้องการลบรายการที่เลือกใช่หรือไม่")){
			$("#act").val('deleteAll');
			$("#downloadlist").submit();
		}
	}
	function exportit()
	{
	
			$("#act2").val('export');
			$("#upload").submit();
	}
	function gotoPage(objPage)
	{
		$('#page').val(objPage);
		$('#upload').submit();
	}
	
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">Newsletter</a> &gt; <a href="#" class="link_green">Newsletter</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">Newsletter</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input type="hidden" value="<?php echo $model->current_page;?>" name="page" id='page' />
<input type="hidden" value="" name="act2" id='act2' />
<center>
<table>
<tr><td align="right">ชื่อ-นามสกุล :</td><td>
<?php echo $form->textField($model,'fullname'); ?>
</td></tr>
<tr><td align="right">Email:</td><td>
<?php echo $form->textField($model,'email'); ?>
</td></tr>

<tr>
<td>
</td>
<td>
<a href="javascript:$('#upload').submit()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_search.png" width="52" height="25" /></a>
<a href="javascript:exportit()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/export_excel.png"  /></a>


</td>
</tr>
</table>
</center>

<?php $this->endWidget(); ?>
                  
                  <br /><br />
                   <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'downloadlist',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input name="act" type="hidden" id='act'/>
                      <table width="100%" border="0" cellspacing="3" cellpadding="3">
                       <tr style="background-color:#78CF2D;color:white;">
                              <td width="30" align="center" valign="top" class="">เลือก</td>
                              <td width="30" align="center" valign="top" class="txt_bold">ลำดับ</td>
                              <td width="35%" align="center" valign="top" class="txt_bold">ชื่อ - นามสกุล</td>
                              <td width="17%" align="center" valign="top" class="txt_bold">Email
                              </td>
                              <td width="15%" align="center" valign="top" class="txt_bold">ข่าวสารที่ต้องการ</td>
                              <td width="70" align="center" valign="top" class="txt_bold">ลบ</td>
                            </tr>
                           <?php
						   $i=1;
						    $i = ($param['page'] * $param['display_perpage']) +1;
						   foreach($data as $row){
						   ?>
                            <tr>
                              <td width="30" align="center" valign="top" class="rowa"><span class="rowb">
                                <input type="checkbox" name="newsletter_id[]" id="checkbox" value="<?php echo $row->id;?>" />
                              </span></td>
                              <td width="30" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i;?></span></td>
                              <td width="35%" align="left" valign="top" class="rowa"><span class="rowb">
                              <?php echo $row->fullname;?>
							  </span></td>
                              <td class="rowa" width="17%" align="center" valign="top"> <?php echo $row->email;?></td>
                              <td width="15%" align="center" valign="top" >
                              <?php
							  if($row->is_news==1)
							  	$s[] = "ข่าวสารทั่วไป";
								 if($row->is_npa=="1")
							  	$s[] = "ข่าวทรัพย์สินพร้อมขาย (NPA)";
								 if($row->is_notice=="1")
							  	$s[] = "ประกาศต่างๆ";
								 if($row->is_ebook=="1")
							  	$s[] = "วารสาร";
								 if($row->is_activity=="1")
							  	$s[] = "กิจกรรม";
								$str_letter = implode(', ',$s);
								echo $str_letter;
							  ?>
                              </td>
                              <td class="rowa" align="center" valign="top"><a href="javascript:deleteit(<?php echo $row->id;?>)">ลบ</a></td>
                            </tr>
                            <?php
							$i++;
						   }
							?>
                          </table>
                          <?php $this->endWidget(); ?>
                          <br />
                          <?php
								  if($param['max_page']>1){
									  ?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="160" class="txt_bold"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page =  $param['page']+1;?> / <?php echo $param['max_page'];?></td>
                          <td><div class="nav_page">
                             <ul>
                                      <li onclick="gotoPage('<?php echo $page-1;?>')">&laquo;</li>
                                      <?php 
									 
									  $start_page = ($page>5?$page-5:1);
									 $end_page = ($param['max_page']>($page+5)?$page+5:$param['max_page']);
									  for($i=$start_page-1;$i<ceil($end_page);$i++)
									  {
										  $class = "";
										  if(($i+1)==$page)
										  	$class = '  class="navselect"';
										else
											$class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
										  ?>
                                          <li <?php echo $class;?>><?php echo $i+1;?></li>
                                          <?php
									  }
									  ?>
                                      <li onclick="gotoPage('<?php echo $page+1;?>')">&raquo;</li>
                                    </ul>
                          </div></td>
                          <td width="160" align="right" class="txt_bold"><span class="txt_pink">ไปหน้าที่ :</span>
                                    <input name="page" type="text" id="page" size="2" />
                                    <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/go.png" width="39" height="22" /></a></td>
                        </tr>
                      </table>
                      <?php
								  }
								  ?>
                    </td>
                        </tr>
                        
                        <tr>
                          <td class="nparesult_table_content" align="center"><br />
                          	
                           &nbsp;<a href="javascript:deleteitAll()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" width="52" height="25" /></a></td>
                        </tr>
                      </table>
                      </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
                    
                  </div></td>
                </tr>
              </table></td>
            </tr>
            
              </table></td>
            </tr>
          </table>
        