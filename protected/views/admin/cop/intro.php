﻿<?php 
$this->pageTitle="Admin Panel::Disclaimer"; 
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	$(function() {
		$( "#tabs" ).tabs();
	});
	function submitme()
	{
		$("#act").val('new');
		var nicE_en = new nicEditors.findEditor('Intro_code_en');
		var nicE_th = new nicEditors.findEditor('Intro_code_th');
		content_en = nicE_en.getContent();
		content_th = nicE_th.getContent();
		$("#Intro_code_en").val(content_en);
		$("#Intro_code_th").val(content_th);
		$("#intro_main").submit();
		
	}
	function deleteit(objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
		$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/admin/DeleteLogoFile/' + objId ,{id:objId},function(data){
																												
									
											location.reload();
																												  });
				   }
	}
</script>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'intro_main',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input type="hidden" name="act" value="" id='act'/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt;<a href="#" class="link_green"> ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">หน้า Intro Page</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">หน้า Intro Page</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                  
                  
                      <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td align="center"><input type="radio" name="status" id="radio" value="1"  <?php echo ($model->status=="1"?" checked='checked' ":"");?> />
                            <span class="txt_bold">แสดงผล</span>
                            <input type="radio" name="status" id="radio2" value="2"  <?php echo ($model->status=="2"?" checked='checked' ":"");?> />
                            <span class="txt_bold">ไม่แสดงผล</span></td>
                        </tr>
                      </table>
                      <br />
                      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td height="25" align="left" bgcolor="#eee"><input type="radio" name="type" value="1" id="checkbox3"  <?php echo ($model->type=="1"?" checked='checked' ":"");?> />
                            <label for="checkbox3" class="txt_bold">แสดงตัวอักษร (กรุณาคลิกเลือกเมื่อต้องการให้แสดงตัวอักษร)</label></td>
                        </tr>
                      </table>
                      <br />
                      
                      
                      <table width="350" border="0" align="center" style="visibility:hidden;position:absolute" cellpadding="0" cellspacing="5" class="txt_bold setting_popup">
                        <tr>
                          <td width="160" align="right">สีพื้นหลังของ Intro page : &nbsp;</td>
                          <td><input type="text" name="textfield5" id="textfield5" />
                            &nbsp;<img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/icon_color_wheel.png" width="16" height="16" /></td>
                        </tr>
                      </table>
<br />
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                        <tr>
                          <td><div class="editor">
                            
                            <div id="tabs" class="bend_tab">
                            <ul>
                              <li>
                              <a href="#tabs-1"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/th.png" width="16" height="11" /> &nbsp;ภาษาไทย</a></li>
                              <li>
                              <a href="#tabs-2">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/en.png" width="17" height="11" />&nbsp; ภาษาอังกฤษ</a></li>
                            </ul>
                            <div id="tabs-1">
                            <p>
                             <?php echo $form->textArea($model,'code_th',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                            </p>
                            </div>
                            <div id="tabs-2">
                            <p>
                              <?php echo $form->textArea($model,'code_en',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                            </p>
                            </div>
                            <p></p>
                            <center>
                            <p></p>
                         </center>
                            
                          </div>
                            
                            <p class="txt_center">&nbsp;</p>
                           
                          </div></td>
                        </tr>
                    </table>
                      
                      
                      </td>
                </tr>
                <tr>
                  <td><br />
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                      <td height="25" align="left" bgcolor="#eee"><input type="radio" name="type" value="2" id="checkbox"  <?php echo ($model->type=="2"?" checked='checked' ":"");?> />
                        <label for="checkbox" class="txt_bold">แสดงรูปภาพ (กรุณาคลิกเลือกเมื่อต้องการให้แสดงรูปภาพ)</label></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="center">เลือกหมวดหมู่ :</span>&nbsp;
                    <select name="group" class="textb" onchange="javascript:$('#intro_main').submit()" id="select2" >
                      <option value="1001" <?php echo ($param->group=="1001"?" selected='selected' ":"");?>>วันเฉลิมพระชนมพรรษา 5 ธันวา</option>
                      <option value="1002" <?php echo ($param->group=="1002"?" selected='selected' ":"");?>>วันเฉลิมพระชนมพรรษา 12 สิงหา</option>
                    </select></td>
                  </tr>
                <tr>
                  <td><br />
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                     <?php
				  		$i=1;
				  	foreach($data as $row){
				  ?>
                        <td align="center" class="rowa"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/COP/index/<?php echo $row->imgsrc;?>" alt="" width="500" height="236" /><br />
                        <input type="radio"  <?php echo ($model->img==$row->imgsrc?" checked='checked' ":"");?>  name="imgsrc" id="radio" value="<?php echo $row->imgsrc;?>" />
                          Intro Page รูปแบบที่ <?php echo $i;?>
                          <br /><a href="javascript:deleteit('<?php echo $row->id;?>')" <?php Yii::hideit();?> class="lmm_15_3"><br />
                          <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" alt="" width="52" height="25" /></a>
                          &nbsp;</td>
                          
                          <?php
						  if($i % 2 == 0)
						  {
							  echo '</tr><tr>';
						  }
						  $i++;
					}
					?>
                        </tr>
                      
                      
                    </table>
                    <br /><br /><br />
                    <center>
                        <a href="javascript:submitme()"  <?php Yii::hideit();?> class="lmm_15_2"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="" width="52" height="25" /></a>
                        </center>
                       
                    </td>
                </tr>
                <tr>
                  <td align="center"><br />
                    <br />
                    <br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">อัพโหลดไฟล์ Intro Page</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table>
                    
                    
                    </td>
                  </tr>
                  
                  <?php $this->endWidget(); ?>
                <tr>
                  <td><br />
                    <br /><center><div class="form">
                    <?php echo $form->error($image_model,'imgsrc'); ?>
                    </div></center>
                     <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'intro_new_image',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                    <table width="420" border="0" align="center" cellpadding="1" cellspacing="5" class="setting_popup">
                      <tr>
                      <td width="120" align="right" valign="top"><span class="txt_bold">รูปภาพ Intro page : </span>&nbsp;</td>
                      <td align="left" class="form">
                        <input type="file" name="file" id="file"  />
                        
                        <span class="txt_red"><br />
                        รองรับไฟล์ .JPG , .GIF , .PNG ขนาดไม่เกิน 1MB</span> <br />
                      </td>
                    </tr>
                    <tr>
                      <td width="120" align="right"><span class="txt_bold">เลือกหมวดหมู่ :</span>&nbsp;&nbsp;</td>
                      <td align="left"> <select name="group_2" class="textb" id="group_2" >
                      <option value="1001">วันเฉลิมพระชนมพรรษา 5 ธันวา</option>
                      <option value="1002">วันเฉลิมพระชนมพรรษา 12 สิงหา</option>
                    </select>
                        <br /></td>
                    </tr>
                    <tr>
                      <td width="120" align="center">&nbsp;</td>
                      <td align="left"><br /><a href="javascript:$('#intro_new_image').submit()"  <?php Yii::hideit();?> class="lmm_15_1"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" alt="" width="52" height="25" /></a>&nbsp;<a href="#">&nbsp;<img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_cancel.png" width="52" height="25" /></a></td>
                    </tr>
                </table>
                 <?php $this->endWidget(); ?>
                </td>
                </tr>
                <tr>
                  <td><br />
                    <br /></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>