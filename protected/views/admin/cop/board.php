<?php 
$this->pageTitle="Admin Panel::Board"; 
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	$(function() {
		$( "#tabs" ).tabs();
	});
	function submitme()
	{
		var nicE_en = new nicEditors.findEditor('AboutSam_content_en');
		var nicE_th = new nicEditors.findEditor('AboutSam_content_th');
		content_en = nicE_en.getContent();
		content_th = nicE_th.getContent();
		$("#AboutSam_content_en").val(content_en);
		$("#AboutSam_content_th").val(content_th);
		$("#AboutSam").submit();
		
	}
</script>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'AboutSam',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">เนื้อหา</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><div class="bend_tab">
                    <ul>
                      <li id="selected"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/icon1.png" width="27" height="23" /> ที่มาขององค์กร</li>
                      <li><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/icon2.png" width="24" height="23" /> โครงสร้างองค์กร</li>
                      <li><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/icon3.png" width="28" height="23" /> รายชื่อกรรมการ</li>
                      <li><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/icon4.png" width="14" height="23" /> พันธกิจ / วิสัยทัศน์</li>
                    </ul>
                    </div></td>
                </tr>
                <tr>
                  <td><div class="editor">
                    <p>
                      <textarea name="textfield" rows="20" id="textfield" style="width:99%">ประวัติความเป็นมา
บริษัท บริหารสินทรัพย์สุขุมวิท จำกัด จัดตั้งขึ้นตามมติคณะรัฐมนตรี เมื่อวันที่ 18 เมษายน 2543 โดยได้จดทะเบียนเป็นบริษัทบริหารสินทรัพย์ ตามพระราชกำหนดบริษัทบริหารสินทรัพย์ พ.ศ.2541 และมีกองทุนเพื่อการฟื้นฟูและพัฒนาระบบสถาบันการเงิน (กองทุนฯ) เป็นผู้ถือหุ้นใหญ่ของบริษัท

เมื่อวันที่ 7 พฤษภาคม 2547 กองทุนฯ ได้มีมติให้มีการโอนกิจการ บริษัท บริหารสินทรัพย์เพชรบุรี จำกัด ให้กับ บริษัท บริหารสินทรัพย์สุขุมวิท จำกัด โดยให้ บริษัท บริหารสินทรัพย์เพชรบุรี จำกัด ขายลูกหนี้ที่ปรับโครงสร้างหนี้ (TDR) ให้แก่ธนาคารนครหลวงไทย จำกัด (มหาชน) และโอนสินทรัพย์ที่เหลือ หนี้สิน และภาระผูกพันทั้งหมดให้ บริษัท บริหารสินทรัพย์สุขุมวิท จำกัด 

นโยบายของบริษัท คือ บริหารสินทรัพย์ด้อยคุณภาพที่รับโอนจากธนาคารกรุงไทย จำกัด (มหาชน) ซึ่งรวมถึงลูกหนี้ที่รับโอนมาจากธนาคารมหานคร จำกัด (มหาชน) และบริษัท บริหารสินทรัพย์เพชรบุรี จำกัด ซึ่งรวมถึงลูกหนี้รับโอนมาจากธนาคารนครหลวงไทย จำกัด(มหาชน) และธนาคารศรีนคร จำกัด (มหาชน) นอกจากนี้ บริษัท ยังมีนโยบายที่จะประมูลซื้อสินทรัพย์ด้อยคุณภาพ และทรัพย์สินรอการขายจากสถาบันการเงินอื่น เพื่อมาบริหารจัดการอย่างมีคุณภาพ และประสิทธิภาพ

วัตถุประสงค์ของบริษัท คือ บริหารสินทรัพย์ด้อยคุณภาพให้เป็นหนี้ที่ดีมีคุณภาพ ซึ่งเป็นส่วนหนึ่งในมาตรการฟื้นฟูเศรษฐกิจของประเทศ เพื่อจำกัดความสูญเสียทางการเงินต่อกองทุนเพื่อการฟื้นฟูและพัฒนาระบบสถาบันการเงิน ให้เกิดขึ้นน้อยที่สุดเท่าที่จะทำได้ และเพื่อช่วยให้ลูกหนี้ที่มีศักยภาพ และมีความตั้งใจจริงในการชำระหนี้สามารถดำเนินธุรกิจต่อไปได้ รวมทั้งขยายโอกาสในการสร้างกำไรจากการบริหารสินทรัพย์ที่ประมูลซื้อและนำส่งกองทุนฯ เพื่อเป็นการช่วยฟื้นฟูระบบเศรษฐกิจโดยรวมของประเทศ โดยจะทำการแก้ไขหนี้และปรับปรุงโครงสร้างหนี้ ภายในระยะเวลาที่กำหนดโดยเร็ว</textarea>
                    </p>
                    <p class="txt_center"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/save.png" width="80" height="30" /></a></p>
                  </div></td>
                </tr>
              </table></td>
            </tr>
          </table>
         <?php $this->endWidget(); ?>