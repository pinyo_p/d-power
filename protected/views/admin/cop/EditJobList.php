<?php 
$this->pageTitle="Coperate Panel::Joblist "; 
function check_menu2($menu_id)
{
	$group_id = Yii::app()->user->getValue("group_id");
	if($group_id=="16")
	{
		return " style='visibility:hidden;position:absolute' ";
	}
	else
	{
		return "";
	}
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true,uploadURI : '<?php echo Yii::app()->request->baseUrl;?>/nicUpload.php'}) });
	function submitme()
	{
		var nicE_en = new nicEditors.findEditor('Joblist_skill');
		var nicE_th = new nicEditors.findEditor('Joblist_jobdetail');
		content_en = nicE_en.getContent();
		content_th = nicE_th.getContent();
		$("#Joblist_skill").val(content_en);
		$("#Joblist_jobdetail").val(content_th);
		$("#jobs").submit();
		
	}
</script>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt;<a href="#" class="link_green"> ร่วมงานกับเรา</a> &gt; <a href="#" class="link_green">ตำแหน่งงาน</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ตำแหน่งงาน</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td  class="nparesult_table_content">&nbsp;</td>
            </tr>
            <tr>
              <td  class="nparesult_table_content"><table width="800" border="0" align="center" cellpadding="3" cellspacing="3">
                <tr>
                  <td height="30" class="nparesult_table_header">&nbsp;&nbsp;เพิ่มตำแหน่งงาน</td>
                </tr>
                <tr>
                  <td class="nparesult_table_content">
                  
                  <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3">
                    <tr>
                      <td class="nparesult_table_content">
                      <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jobs',
	'enableClientValidation'=>false,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                      <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td valign="top" align="right"><span class="txt_green txt_bold">ตำแหน่ง    :</span></td>
                          <td align="left" valign="top"><?php echo $form->textField($model,'position'); ?></td>
                          </tr>
                        <tr>
                          <td valign="top" align="right"><span class="txt_green txt_bold">การศึกษา     :</span></td>
                          <td align="left" valign="top"><?php echo $form->textField($model,'education'); ?></td>
                          </tr>
                        <tr>
                          <td width="150" valign="top" align="right"><span class="txt_green txt_bold">ความสามารถ     :</span></td>
                          <td align="left" valign="top"><p>
                            <?php echo $form->textArea($model,'skill',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                          </p></td>
                          </tr>
                        <tr>
                          <td valign="top" align="right"><span class="txt_green txt_bold">ประสบการณ์     :</span></td>
                          <td align="left" valign="top"><p>
                           <?php echo $form->textField($model,'experience'); ?>
                          </p></td>
                        </tr>
                        <tr>
                          <td valign="top" align="right"><span class="txt_green txt_bold">ประเภท    :</span></td>
                          <td align="left" valign="top"><p>
                           <?php echo $form->textField($model,'type'); ?>
                          </p></td>
                        </tr>
                        <tr>
                          <td valign="top" align="right"><span class="txt_green txt_bold">รายละเอียดงาน      :</span></td>
                          <td align="left" valign="top"><p>
                             <?php echo $form->textArea($model,'jobdetail',array('rows'=>5,'cols'=>40,'style'=>'width:600px;height:400px')); ?>
                          </p></td>
                        </tr>
                        <tr>
                          <td align="right"><span class="txt_green txt_bold">
                          Publish Date :</span>
                          </td>
                          <td>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Joblist[publish_date]',
					'value'=>$model->publish_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?>
                          </td>
                        </tr>
                        
                      </table>
                      <?php $this->endWidget(); ?>
                      </td>
                    </tr>
                    <tr>
                      <td align="center" class="nparesult_table_content">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center" class="nparesult_table_content"><a href="javascript:submitme()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/b_save.png" width="52" height="25" /> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backend/b_cancel.png" width="52" height="25" /></a></td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
            </tr>
          </table>

 