<?php 
$part = Yii::app()->user->getValue("part");
switch($part)
{
	case "NPA": $p = "NPA Panel";break;
	case "COP": $p = "Coperate Panel";break;
	case "OUT": $p = "ผู้ให้บริการภายนอก";break;
}
$this->pageTitle="$p::Manage User"; 
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">

	function deleteit(objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่?"))
		{
			$.post('<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin/DeleteCopUploadFile/',{
									id:objId,
									},function(data){
										if(data=="OK")
											location.reload();
										else
											alert(data);
									});
		}
	}
	function deleteitAll()
	{
		
		if(confirm("ต้องการลบรายการที่เลือกใช่หรือไม่")){
			$("#act").val('deleteAll');
			$("#frm_marquee").submit();
		}
	}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="#" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">Coperate</a> &gt; <a href="#" class="link_green">วารสาร</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">วารสาร</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="form1">
                <tr>
                  <td>
                  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<center>
<table>
<tr><td align="right">ชื่อไฟล์ :</td><td>
<?php echo $form->textField($model,'filename'); ?>
</td></tr>
 <tr>
                              <td valign="top"><span class="txt_green txt_bold">คำอธิบายภาพ    :</span></td>
                              <td align="left" valign="top"><?php echo $form->textArea($model,'filedesc',array('rows'=>4,'cols'=>30,'style'=>'width:380px;height:150px')); ?></td>
                              <td valign="top">&nbsp;</td>
                              </tr>
<tr><td align="right">ภาพประกอบ :</td><td>
<input type="file" name="ss" />
</td></tr>
<tr><td align="right">
เลือกไฟล์ :</td><td><input type="file" name="file" />
</td>
</tr>
<tr>
<td>
</td>
<td>
<a href="javascript:$('#upload').submit()"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" width="52" height="25" /></a>
</td>
</tr>
</table>
</center>

<?php $this->endWidget(); ?>
                  
                  <br /><br />
                      <table width="100%" border="0" cellspacing="3" cellpadding="3">
                       <tr style="background-color:#78CF2D;color:white;">
                              <td width="30" align="center" valign="top" class="txt_bold">ลำดับ</td>
                              <td width="35%" align="center" valign="top" class="txt_bold">ไฟล์</td>
                              <td width="17%" align="center" valign="top" class="txt_bold">วันที่ อัพโหลด</td>
                              <td width="17%" align="center" valign="top" class="txt_bold">จำนวนครั้งดาวน์โหลด</td>
                              
                              <td width="15%" align="center" valign="top" class="txt_bold">ลบ</td>
                            </tr>
                           <?php
						   $i=1;
						   foreach($data as $row){
						   ?>
                            <tr>
                              <td width="30" align="center" valign="top" class="rowa"><span class="txt_bold"><?php echo $i;?></span></td>
                              <td width="35%" align="left" valign="top" class="rowa"><span class="rowb">
							  <a href='<?php echo Yii::app()->request->baseUrl;?>/download/COP/<?php echo $row->filesrc;?>' target='_blank'>
                              <img src='<?php echo Yii::app()->request->baseUrl;?>/download/COP/<?php echo $row->image_cover;?>' height="80" />
							  <?php echo $row->filename;?></a></span></td>
                              <td class="rowa" width="17%" align="center" valign="top"><?php echo $row->create_date;?></td>
                              <td class="rowa" width="17%" align="right" valign="top"><?php echo $row->view_count;?></td>
                              <td class="rowa" width="15%" align="center" valign="top"><a href="javascript:deleteit(<?php echo $row->id;?>)">ลบ</a></td>
                            </tr>
                            <?php
							$i++;
						   }
							?>
                          </table>
                    </td>
                        </tr>
                        <tr>
                          <td class="nparesult_table_content" align="center"><br />
                          	
                           &nbsp;<a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" width="52" height="25" /></a></td>
                        </tr>
                      </table>
                      </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
                    
                  </div></td>
                </tr>
              </table></td>
            </tr>
            
              </table></td>
            </tr>
          </table>
        