<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            .head1{
                font-size:18px;font-weight:bold;
            }
            .head2{
                font-size:16px;font-weight:bold;
            }
            .txtright{text-align:right;}
            .txtcenter{text-align:center;}
            .txtbold{font-weight: bold;}
            table tr th{font-weight: bold;}
        </style>
    </head>
    <body>
        <div class="head1">สรุปข้อมูลรายการสินค้า</div>
        <div class="head2">สถานะ : <?php echo $statusname; ?></div>
        <br/>
        <table border="1">
            <tr style="background:#ddd;">
                <th class="txtcenter" style="width:100px;">Code</th>
                <th style="min-width:180px;">ชื่อสินค้า</th>
                <th style="min-width:180px;">กลุ่มสินค้า</th>
                <th class="txtright" style="width:100px;">ราคาขาย</th>
                <th class="txtcenter" style="width:100px;">คงคลัง</th>
                <th class="txtcenter">สถานะ</th>                           
            </tr>
            <?php
            if (count($mProduct) > 0) {
                foreach ($mProduct as $rProduct) {
                    ?>
                    <tr>
                        <td class="txtcenter"><?php echo $rProduct["product_code"]; ?></td>
                        <td><?php echo $rProduct["product_name_2"]; ?></td>
                        <td><?php echo $rProduct["group_name"]; ?></td>
                        <td class="txtright"><?php echo number_format($rProduct["price1"], 0, ".", ","); ?></td>
                        <td class="txtcenter"><?php echo $rProduct["stock"]; ?></td>
                        <td class="txtcenter">
                            <?php
                            $statusName = "";
                            switch ($rProduct["status"]) {
                                case "0":$statusName = "รออนุมัติ";
                                    break;
                                case "1":$statusName = "ประกาศขาย";
                                    break;
                                case "2":$statusName = "ยกเลิก";
                                    break;
                            }
                            echo $statusName;
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr><td colspan="6" class="txtcenter"><i>-- No result found --</i></td></tr>
                <?php
            }
            ?>

        </table>
    </body>
</html>
