<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'จดหมายข่าวสาร';
?>
<script language="javascript">
    $(document).ready(function(){
        $('#btnSendNews').click(function(){
            location.href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/NewsletterChooseNews";
        })        
    })
    function searchit()
    {
        $("#act").val("search");
        $("#productgrouplist-form").submit();

    }
    function gotoPage(objPage)
    {
        $("#act").val("search");
        $("#page").val(objPage);
        $("#productgrouplist-form").submit();
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'productgrouplist-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<input type="hidden" name='act' id='act' value="search" />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center"><br />
            <span class="text4"><?php echo $this->breadcrumbs; ?></span><br />
            &nbsp;<br /></td>
    </tr>
    <tr>
        <td align="center" width="100%" class="tabletest">
            <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                    <th width="60">ลำดับ</th>
                    <th width="180">Email</th>
                    <th>ข่าวสารที่ส่ง</th>
                    <th width="150">วันที่ส่ง</th>
                </tr>
                <?php
                $curPage = $param['page'] + 1;
                if (($param['page'] + 1) == $param['max_page'])
                    $curPage = $param['max_page'];
                $index = ($param['display_perpage'] * $param['page']) + 1;
                
                foreach ($data as $row) {
                    ?>
                    <tr>
                        <td align="center"><?php echo $index; ?></td>
                        <td><?php echo $row->subscriber->email; ?></td>
                        <td><?php echo $row->content->title_2; ?></td>
                        <td><?php echo date_format(date_create($row->send_date), 'd-m-Y H:m:s'); ?></td>
                    </tr>
                    <?php
                    $index ++;
                }
                ?>
            </table>

            <?php           
            if ($param['max_page'] > 1) {
                ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="160" class="txt_bold" style="border:none;"><span class="txt_pink ">หน้าที่ :</span>  <?php echo $page = $param['page'] + 1; ?> / <?php echo $param['max_page']; ?></td>
                        <td style="border:none;"><div class="nav_page">
                                <ul>
                                    <li onclick="gotoPage('<?php echo $page - 1; ?>')">&laquo;</li>
                                    <?php
                                    $start_page = ($page > 2 ? $page - 2 : 1);
                                    $end_page = ($param['max_page'] > ($page + 2) ? $page + 2 : $param['max_page']);
                                    if ($end_page <= 5)
                                        $end_page = 5;
                                    else if (($end_page - $page) < 5)
                                        $start_page = ($end_page - 4);
                                    if ($end_page > $param['max_page'])
                                        $end_page = $param['max_page'];
                                    for ($i = $start_page - 1; $i < ceil($end_page); $i++) {
                                        $class = "";
                                        if (($i + 1) == $page)
                                            $class = '  class="navselect"';
                                        else
                                            $class = " style='cursor:pointer;' onclick='gotoPage(\"" . ($i + 1) . "\")' ";
                                        ?>
                                        <li <?php echo $class; ?>><?php echo $i + 1; ?></li>
                                        <?php
                                    }
                                    ?>
                                    <li onclick="gotoPage('<?php echo $page + 1; ?>')">&raquo;</li>
                                </ul>
                            </div></td>
                        <td width="200" align="right" class="txt_bold" style="border:none; vertical-align:middle;">
                            <table width="200"  style="border:none;"><tr><td style="border:none;">
                                        <span class="txt_pink">ไปหน้าที่ :</span></td><td style="border:none;">
                                        <input name="page" type="text" id="page" size="2" /></td><td style="border:none;">
                                        <a href="#" onclick="searchit()"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/button_search.png" height="22" /></a></td></tr></table>


                        </td>
                    </tr>
                </table>
                <?php
            }
            ?>

        </td>
    </tr>
    <tr>
        <td align="center">
            <br />
            <button id="btnSendNews" onclick="return false;">ส่งจดหมายข่าวสาร</button>
            <br />
        </td>
    </tr>
    <tr>
        <td align="center"><br />
            <br />
            <br /></td>
    </tr>

</table>
<?php $this->endWidget(); ?>