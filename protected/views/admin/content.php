<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'เนื้อหาเว็บไซต์';
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
$id = (isset($_GET['id']) ? $_GET['id'] : "0");
?>
<script type="text/javascript" language="javascript">
    var roxyFileman = '<?php echo Yii::app()->request->baseUrl; ?>/fileman/pagefile.html';
    $(function () {
        $("#tabs").tabs();
        CKEDITOR.replace('Content_content_2', {
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
        CKEDITOR.replace('Content_content_1', {
            filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'
        });
    });
</script>

<table width="100%">
    <tr>
        <td align="center"><h3 class="underline"><?php echo (isset($_GET["id"])) ? "แก้ไข" : "เพิ่ม"; ?>ข้อมูล</h3></td>
    </tr>
    <tr>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="add_data">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th width="40%" align="right" >หมวดหมู่ :</th>
                    <td width="75%">
                        <?php
                        echo $form->dropDownList($model, 'content_code', CHtml::listData(Content::model()->findAll(
                                                array(
                                                    'select' => 'content_code',
                                                    'condition' => "content_code <> 'News'",
                                                    'group' => 'content_code',
                                                    'distinct' => true,
                                                )
                                        ), 'content_code', 'content_code'), array('empty' => '-- เลือกหมวดหมู่ --',
                        ));
                        ?><?php echo $form->error($model, 'content_code'); ?>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right" style="vertical-align: top;">เนื้อหา :</th>
                    <td width="75%">
                        <div style="width:800px;  text-align:left">

                            <div id="tabs" >
                                <ul>
                                    <li><a href="#tabs-1">ภาษาไทย</a></li>
                                    <li><a href="#tabs-2">ภาษาอังกฤษ</a></li>
                                </ul>
                                <div id="tabs-1">
                                    ชื่อเรื่อง (ภาษาไทย)<br />
                                    <?php echo $form->textField($model, 'title_2', array('style' => 'width:100%;')); ?>
                                    <?php echo $form->error($model,'title_2'); ?>
<!--                                    <br /><br />-->
<!--                                    เนื้อหาย่อ (ภาษาไทย)<br />-->
                                    <?php
//                                    Yii::import('ext.ckeditor.*');
//                                    $this->widget('application.extensions.ckeditor.CKEditor', array(
//                                        'model' => $model,
//                                        'attribute' => 'short_content_2',
//                                        'language' => 'en',
//                                        'editorTemplate' => 'full',
//                                        'height' => '400px',
//                                    ));
                                    ?>
                                    <br /><br />
                                    เนื้อหาทั้งหมด (ภาษาไทย)<br />
                                    <?php
                                    Yii::import('ext.ckeditor.*');
                                    $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $model,
                                        'attribute' => 'content_2',
                                        'language' => 'en',
                                        'editorTemplate' => 'full',
                                        'height' => '1000px',
                                    ));
                                    ?>
                                </div>
                                <div id="tabs-2">
                                    ชื่อเรื่อง (ภาษาอังกฤษ)
                                    <br />
                                    <?php echo $form->textField($model, 'title_1', array('style' => 'width:100%;')); ?>
                                    <?php echo $form->error($model,'title_1'); ?>
<!--                                    <br /><br />
                                    เนื้อหาย่อ (ภาษาอังกฤษ)<br />-->
                                    <?php
//                                    Yii::import('ext.ckeditor.*');
//                                    $this->widget('application.extensions.ckeditor.CKEditor', array(
//                                        'model' => $model,
//                                        'attribute' => 'short_content_1',
//                                        'language' => 'en',
//                                        'editorTemplate' => 'full',
//                                        'height' => '400px',
//                                    ));
                                    ?>
                                    <br /><br />
                                    เนื้อหาทั้งหมด (ภาษาอังกฤษ)<br />
                                    <?php
                                    Yii::import('ext.ckeditor.*');
                                    $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $model,
                                        'attribute' => 'content_1',
                                        'language' => 'en',
                                        'editorTemplate' => 'full',
                                        'height' => '1000px',
                                    ));
                                    ?>
                                </div>
                                <div style="padding-left:15px">
                                </div>
                            </div>
                            <br /><br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">จัดเรียง :</th>
                    <td width="75%"> 
                        <?php echo $form->textField($model, 'sort_order', array('style' => 'width:50px')); ?><?php echo $form->error($model,'sort_order'); ?>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">สถานะ :</th>
                    <td width="75%"> 
                       <?php echo $form->dropDownList($model, 'status', array('1' => 'ใช้งาน', '0' => 'ไม่ใช้งาน'),array('style' => 'width:100px;')); ?>
                        <?php echo $form->error($model,'status'); ?>
                    </td>
                </tr>

            </table>
            <br />
            <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;
            <a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/ContentList'; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>