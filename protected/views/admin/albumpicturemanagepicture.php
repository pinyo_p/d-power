<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'รายการรูปภาพ';
?>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .col-sm-5{
        width:100%;
    }
    .bs-glyphicons {
        padding-left: 0;
        padding-bottom: 1px;
        margin-bottom: 20px;
        list-style: none;
        overflow: hidden;
    }
    .bs-glyphicons li {
        float: left;
        width: 250px;
        height: 230px;
        padding: 10px;
        margin: 0 -1px -1px 0;
        font-size: 12px;
        line-height: 1.4;
        text-align: center;
        border: 1px solid #ddd;
    }
    .bs-glyphicons .glyphicon {
        margin-top: 5px;
        margin-bottom: 10px;
        font-size: 24px;
    }
    .bs-glyphicons .glyphicon-class {
        display: block;
        text-align: center;
        word-wrap: break-word; /* Help out IE10+ with class names */
    }
    .bs-glyphicons li:hover {
        background-color: rgba(86,61,124,.1);
    }
</style>
<script language="javascript">
    function deleteit(objId)
    {
        if (confirm("<?php echo Label::$confirm_delete; ?>"))
        {
            window.location = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/deletePhoto/galleryid/<?php echo $_REQUEST["galleryid"] ?>/id/" + objId;
        }
    }
    function savecover(objId)
    {
        window.location = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/admin/updatePhotoCover/galleryid/<?php echo $_REQUEST["galleryid"] ?>/id/" + objId;
    }
</script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'category-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ), 'htmlOptions' => array(
        'class' => 'validate', 'enctype' => 'multipart/form-data'
    )
        ));
?>
<section class="content">

    <div class="col-md-12">

        <!-- Custom Tabs (Pulled to the right) -->
        <div style="padding-left:70px;">

            <div>
                <ul class="bs-glyphicons">
                    <?php
                    $gal = $photos;

                    foreach ($gal as $g) {
                        echo "<li><div id='o_dvimg_" . $g->id . "'><img id='o_img" . $g->id . "' src='" . Yii::app()->request->baseUrl . "/images/album/" . $g->gallery_id . "/" . $g->file_name . "' style='max-width:200px; max-height:120px; padding:5px;'><br />
                        <div style = 'padding:10px;text-align:left;'>
                        <div style='font-size:17px;'>" . $g->name_2 . "</div>
                        <div>" . $g->description_2 . "</div>
                        </div>";
                        if($g->is_cover == 0)
                            echo "<input style='background-color: #78CF2D;border-color: #78CF2D;color:#fff;border-radius: 5px;cursor:pointer;' type='button' value='ตั้งเป็นภาพปก' onclick='savecover(".$g->id.")' />";
                        else
                            echo "<input style='background-color: #006600;border-color: #006600;color:#fff;border-radius: 5px;cursor:pointer;' type='button' value='ยกเลิกตั้งเป็นภาพปก' onclick='savecover(".$g->id.")' />";
                        echo "<input style='background-color: #f56954;border-color: #f4543c;color:#fff;border-radius: 5px;cursor:pointer;' type = 'button' value = 'ลบรูปภาพ'  onclick = 'deleteit(" . $g->id . ")' /> </div></li>";
                    }
                    ?>
                </ul>
            </div>

            <div class="box-footer">
                <h3 class="box-title">เพิ่มรูปภาพ</h3>
                <div id='image_list'></div>
                <input type='button' class='btn btn-info' onclick="moreimage()" value='เพิ่มรูปภาพ' />&nbsp;&nbsp; 
                <input type='submit' class='btn btn-info' value='อัพโหลด' />&nbsp;&nbsp;
                <a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/AlbumPictureList'; ?>">
                    <input type="button" value="ย้อนกลับ"/>
                </a>
            </div>
            <br/><br/><br/>

        </div>

    </div>                     

</section>

<script src="<?php echo Yii::app()->request->baseUrl;
                    ?>/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>       
<script src="<?php echo Yii::app()->request->baseUrl; ?>/admin/js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>        
<script type="text/javascript">
                    var i = 0;
                    function moreimage() {
                        i++;
                        $("#image_list").append("<div id='dvimg_" + i + "'><img id='img" + i + "' style='max-width:400px;padding:5px;'><br /><input type='button' class='btn btn-info' value='<?php echo Label::$cancel; ?>' id='del_img_" + i + "'  onclick='deleteItem(\"dvimg_" + i + "\")' />\n\
                <div style=''><input type='text' name='img_title[]' placeholder='ชื่อภาพ' style='width:400px;' maxlength='1000' /><div style='height:10px;'></div><textarea name='img_desc[]' rows='4' cols='60' placeholder='คำอธิบาย'></textarea></div><input  class='img_up' onchange='readURL(this)' preview='img" + i + "' type='file'  name='img_gal[]' deletebutton='del_img_" + i + "' accept='image/x-png, image/gif, image/jpeg' /><br /><hr /></div>");
                        $("#img" + i).hide();
                        $("#del_img_" + i).hide();
                    }
                    function moreattach() {
                        i++;
                        $("#attach_list").append("<input type='button' class='btn btn-info' value='<?php echo Label::$cancel; ?>' id='del_attach_" + i + "' /><input type='file'  name='attach[]' deletebutton='del_attach_" + i + "'  /><br />*ขนาดภาพ ไม่เกิน 2MB<br /><hr /></div>");
                        $("#del_attach_" + i).hide();
                    }
                    function readURL(input) {

                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {

                                $("#" + $(input).attr("preview")).attr('src', e.target.result);
                                $("#" + $(input).attr("preview")).show();
                                $("#" + $(input).attr("deletebutton")).show();
                                $(input).hide();
                            }

                            reader.readAsDataURL(input.files[0]);

                        } else {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $($(input).attr("preview")).attr('src', e.target.result);
                            }

                            reader.readAsDataURL(input.files[0]);

                        }
                    }

                    function deleteItem(objId)
                    {
                        $("#" + objId).remove();
                    }

</script>
<?php $this->endWidget(); ?>
