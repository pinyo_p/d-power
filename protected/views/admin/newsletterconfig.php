<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'ตั้งค่าระบบรับข่าวสาร';
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<table width="100%">
    <tr>
        <td align="center"><h3 class="underline">ตั้งค่าระบบรับข่าวสาร</h3></td>
    </tr>
    <tr>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="add_data">
            <table width="50%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th width="40%" align="right">Email ผู้ส่ง :</th>
                    <td width="75%"><input type="text" id="txtShippingFee" name="txtSenderEmail" value="<?php echo $param["sender_email"];?>"  /></td>
                </tr>
<tr>
                    <th width="40%" align="right">ชื่อผู้ส่ง :</th>
                    <td width="75%"><input type="text" id="txtShippingFee" name="txtSenderName" value="<?php echo $param["sender_name"];?>"  /></td>
                </tr>
            </table>
            <br />
            <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />
            <br />
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>