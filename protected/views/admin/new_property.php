﻿<?php 
$this->pageTitle="Admin Panel::Manage Property (NPA)"; 
$doc_item = CHtml::listData(MasterDocument::model()->findAll(array('order' => 'document_name ASC')), 'id', 'document_name');
$doc_list = "";

foreach($doc_item as $item)
{
	$val = array_keys($doc_item, $item);
	$doc_list .= "<option value='" . $val[0] . "'>$item</option>";
}

function getDocument($selected)
{
	$doc_item = CHtml::listData(MasterDocument::model()->findAll(array('order' => 'document_name ASC')), 'id', 'document_name');
	$doc_list = "";
	foreach($doc_item as $item)
	{
		
		$val = array_keys($doc_item, $item);
		$s = "";
		if($selected == $val[0])
			$s = " selected='selected' ";
		$doc_list .= "<option value='" . $val[0] . "' $s >$item</option>";
	}
	return $doc_list;
}

//onsubmit="return doThis()"
?>


<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true,uploadURI : '<?php echo Yii::app()->request->baseUrl;?>/nicUpload.php'}) });
</script>


<script language="javascript">
	doc_id=0;
	image_id=0;
	file_id=0;
	vdo_id=0;
	$(function() {
		$( "#tabs" ).tabs();
		more_doc();
		more_file();
		more_vdo();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		more_image();
		
		
	});
	function more_doc()
	{
		doc_id++;
		$('#tb_doc tr:last').after("<tr valign='middle' id='doc_tr_" + doc_id + "'><td><select name='doc_name[]'><?php 
								  echo $doc_list;
								   ;?></select></td><td><input type='text' name='doc_no[]' /></td><td><a href='javascript:;' onclick='del_doc(" + doc_id + ")'>ลบ</a></td></tr>");
	}
	function del_doc(objId)
	{
		$('#doc_tr_' + objId).remove();
	}
	function more_image()
	{
		image_id++;
		$('#tb_image tr:last').after("<tr valign='middle' id='image_tr_" + image_id + "'><td align='right'>รูปภาพประกอบ " + image_id + " :</td><td><input type='file' name='image[]' /></td><td><a href='javascript:;' onclick='del_image(" + image_id + ")'>ลบ</a></td></tr>");
	}
	function del_image(objId)
	{
		$('#image_tr_' + objId).remove();
	}
	
	function more_file()
	{
		file_id++;
		$('#tb_file tr:last').after("<tr valign='middle' id='file_tr_" + file_id + "'><td align='right'>เอกสาร " + file_id + " :</td><td><input type='text' name='file_name[]' />&nbsp;&nbsp;<input type='file' name='file[]' /></td><td><a href='javascript:;' onclick='del_file(" + file_id + ")'>ลบ</a></td></tr>");
	}
	function del_file(objId)
	{
		$('#file_tr_' + objId).remove();
	}
	
	function more_vdo()
	{
		vdo_id++;
		$('#tb_vdo tr:last').after("<tr valign='middle' id='vdo_tr_" + vdo_id + "'><td align='right'>ลิงก์วีดีโอ " + vdo_id + " :</td><td><input type='text' name='vdo_url[]' />&nbsp;&nbsp;ภาพประกอบ<input type='file' name='vdo_ss[]' /></td><td><a href='javascript:;' onclick='del_vdo(" + vdo_id + ")'>ลบ</a></td></tr>");
	}
	function del_vdo(objId)
	{
		$('#vdo_tr_' + objId).remove();
	}
	function open_map()
	{
		var province = ($("#Property_province").val()=="") ? "":' ' + $("#Property_province option:selected").text();
		var district = ($("#Property_district").val()=="" || $("#Property_district").val()=="0") ? "":' ' + $("#Property_district option:selected").text();
		var tambol = ($("#Property_tambol").val()=="" || $("#Property_tambol").val()=="0") ? "":' ' + $("#Property_tambol option:selected").text();
		window.open('http://maps.google.co.th/maps?hl=th&q=' + tambol + district + province + '&gl=th');
	}
	
	function deletepic(objType,objRowId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
			$.post('<?php echo Yii::app()->createUrl('/admin/DeleteNpaImage/');?>',
													 {
														 id:<?php echo (isset($_GET['id'])?$_GET['id']:0);?>,
														 type:objType,
														},
														function(data)
													 {
														 if(data=="OK"){
															 $("." + objRowId).hide();
														 }else{
															alert(data);
														 }
														});
		}
	}
	function submitme(objType)
	{
		/*
		var nicE_en = new nicEditors.findEditor('Footer_footer_en');
		var nicE_th = new nicEditors.findEditor('Footer_footer_th');
		content_en = nicE_en.getContent();
		content_th = nicE_th.getContent();
		$("#Footer_footer_en").val(content_en);
		$("#Footer_footer_th").val(content_th);
		$("#Footer_footer_en").val(nicE_en.getContent());
		enter_th
		product_desc_th
		product_desc2_th
		product_limit_th
		remark_th
		other_th
		more_detail_th
		enter_en
		enter_en
		product_desc_en
		product_desc2_en
		product_limit_en
		remark_en
		other_en
		more_detail_en
		bid_place
		*/
		var nicE_enter_th = new nicEditors.findEditor('Property_enter_th');
		var nicE_product_desc_th = new nicEditors.findEditor('Property_product_desc_th');
		var nicE_product_desc2_th = new nicEditors.findEditor('Property_product_desc2_th');
		var nicE_product_limit_th = new nicEditors.findEditor('Property_product_limit_th');
		var nicE_remark_th = new nicEditors.findEditor('Property_remark_th');
		var nicE_other_th = new nicEditors.findEditor('Property_other_th');
		var nicE_more_detail_th = new nicEditors.findEditor('Property_more_detail_th');
		var nicE_enter_en = new nicEditors.findEditor('Property_enter_en');
		var nicE_product_desc_en = new nicEditors.findEditor('Property_product_desc_en');
		var nicE_product_desc2_en = new nicEditors.findEditor('Property_product_desc2_en');
		var nicE_product_limit_en = new nicEditors.findEditor('Property_product_limit_en');
		var nicE_remark_en = new nicEditors.findEditor('Property_remark_en');
		var nicE_other_en = new nicEditors.findEditor('Property_other_en');
		var nicE_more_detail_en = new nicEditors.findEditor('Property_more_detail_en');
		var nicE_bid_place = new nicEditors.findEditor('Property_bid_place');
		
		$("#Property_enter_th").val(nicE_enter_th.getContent());
		$("#Property_product_desc_th").val(nicE_product_desc_th.getContent());
		$("#Property_product_desc2_th").val(nicE_product_desc2_th.getContent());
		$("#Property_product_limit_th").val(nicE_product_limit_th.getContent());
		$("#Property_remark_th").val(nicE_remark_th.getContent());
		$("#Property_other_th").val(nicE_other_th.getContent());
		$("#Property_more_detail_th").val(nicE_more_detail_th.getContent());
		$("#Property_enter_en").val(nicE_enter_en.getContent());
		$("#Property_product_desc_en").val(nicE_product_desc_en.getContent());
		$("#Property_product_desc2_en").val(nicE_product_desc2_en.getContent());
		$("#Property_product_limit_en").val(nicE_product_limit_en.getContent());
		$("#Property_remark_en").val(nicE_remark_en.getContent());
		$("#Property_other_en").val(nicE_other_en.getContent());
		$("#Property_more_detail_en").val(nicE_more_detail_en.getContent());
		$("#Property_bid_place").val(nicE_bid_place.getContent());


		
		$("#type").val(objType);
		if($('#Property_publish_date').val()>$('#Property_end_date').val())
		{
			alert('วันที่เริ่มต้นต้องน้อยกว่าวันที่สิ้นสุด');
		}else if($('#Property_end_date').val()>$('#Property_bid_date').val() && $('#Property_bid_date').val()!=""){
			alert('วันที่เริ่มต้นต้องน้อยกว่าวันที่สิ้นสุด');
		}else{
			$("#new_property").submit();
		}
		
		//$("#new_property").submit();
		
	}
	function deletefile(objType,objRowId,objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
			$.post('<?php echo Yii::app()->createUrl('/admin/DeleteNpaFiles/');?>',
													 {
														 id:objId,
														 type:objType,
														},function(data)
													 {
														 if(data=="OK"){
															 $("#" + objRowId).hide();
														 }else{
															alert(data);
														 }
														});
		}
	}
	function update(objId)
	{
		var obj_doc_type = $("#edt_doc_type_" + objId ).val();
		var obj_doc_no = $("#edt_doc_no_" + objId).val();
		$.post('<?php echo Yii::app()->createUrl('/admin/UpdateDocNo/');?>',
													 {
														 id:objId,
														 doc_type:obj_doc_type,
														 doc_no:obj_doc_no
														},function(data)
													 {
														 if(data=="OK"){
															 alert('ปรับปรุงเสร็จสิ้น')
														 }else{
															alert(data);
														 }
														});
		
	}
	<?php
	if(isset($_REQUEST['type'])&&$_REQUEST['type']=="savepreview" && isset($_REQUEST['id']))
		echo "window.open('" .  Yii::app()->createUrl('/npa/detail/' . $_REQUEST['id']) . "/?type=1');\n";
	?>
	</script>
<div align="center">

<div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">ทรัพย์สิน</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ทรัพย์สิน</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table>
<table style="text-align:left"><tr><td>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'new_property',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

 <div class="row">
<table>
 <tr>
                <td colspan="3">
                <h3>ข้อมูลทรัพย์</h3>
                </td>
    </tr>
    <tr><td></td>
    <td colspan="2">
    <table style="visibility:hidden;position:absolute;">
    <tr>
                <th align="right">วันที่เพิ่มข้อมูล</th>
                <td>- </td>
                <th> ผู้เพิ่มข้อมูล</th>
                <td>-</td>
    </tr>
    <tr>
                <th align="right">วันที่ปรับปรุงข้อมูลล่าสุด</th>
                <td>-</td>
                <th> ผู้ปรับปรุงข้อมูลล่าสุด  </th>
                <td>-</td>
    </tr>
     <tr>
                <th align="right">วันที่อนุมัติ</th>
               <td>-</td>
                <th> ผู้อนุมัติ </th>
                <td>-</td>
    </tr>
     <tr>
                <th align="right">วันที่ลบ</th>
                <td>-</td>
                <th> ผู้ลบข้อมูล </th>
                <td>-</td>
    </tr>
    </table>
    <input type="hidden" name="type" id="type" />
    </td>
    </tr>
    <tr>
                <th><div class="form"><?php echo $form->labelEx($model,'product_code'); ?></div></th>
                <td><div class="form"><?php echo $form->textField($model,'product_code'); ?> <?php echo $form->error($model,'product_code'); ?></div></td>
                <td></td>
    </tr>
    
    <tr>
                <th><?php echo $form->labelEx($model,'ประเภททรัพย์'); ?></th>
                <td><?php echo $form->dropDownList($model,'product_type', CHtml::listData(ProductGroup::model()->findAll(array('order' => 'product_group ASC')), 'id', 'product_group'), array('empty'=>'Select Group',																																												  				 
	  )); ?><?php echo $form->error($model,'product_type'); ?></td>
    </tr>
    <tr style="visibility:hidden;position:absolute;">
                <th><?php echo $form->labelEx($model,'กำหนดเป็น ทรัพย์แนะนำ'); ?></th>
                <td><?php echo $form->checkBox($model,'is_highlight',  array()); ?><?php echo $form->error($model,'is_highlight'); ?></td>
                <td>&nbsp;</td>
    </tr>
    <tr>
                <th>ประเภทเอกสารสิทธิ์</th>
                <td colspan='2'>
                <fieldset>               
                <table id='tb_doc'>
                <tr>
                <td>ประเภทเอกสารสิทธิ์</td><td>เลขที่เอกสารสิทธิ์</td>
                </tr>
                 
                 
                
                </table>
                <div  class="row buttons">
                <input type="button" value="เพิ่ม" id='btnMore' onclick="more_doc()" /></div>
                <br />
                <?php
				if(isset($model->license) && count($model->license)>0){
					foreach($model->license as $row)
					{
						
						echo '<div id="div_doc_' . $row->id . '"><b><select id="edt_doc_type_' . $row->id . '">' . getDocument($row->license_name) . '</select></b> : <input type="text" style="width:400px" id="edt_doc_no_' . $row->id . '" value="' . $row->license_no . "\"/> <a href='javascript:update(\"" . $row->id . "\");'>ปรับปรุง</a>&nbsp;<a href='javascript:deletefile(\"document\",\"div_doc_" . $row->id . "\",\"" . $row->id . "\")'>ลบ</a></div>";
						echo "<br />";
					}
				}
				?>
                </fieldset>
                </td>
                <td>&nbsp;</td>
    </tr>
    <tr>
                <th><?php echo $form->labelEx($model,'จำนวนเอกสารสิทธิ์'); ?></th>
                <td><?php echo $form->textField($model,'document_amount'); ?><?php echo $form->error($model,'document_amount'); ?></td>
                <td>&nbsp;</td>
    </tr>
     <tr>
                <th><?php echo $form->labelEx($model,'เลขที่ดิน'); ?></th>
                <td><?php echo $form->textField($model,'land_license'); ?><?php echo $form->error($model,'land_license'); ?></td>
                <td>&nbsp;</td>
    </tr>
    <tr>
                <th><?php echo $form->labelEx($model,'ระวาง'); ?></th>
                <td><?php echo $form->textField($model,'rawang'); ?><?php echo $form->error($model,'rawang'); ?></td>
                <td>&nbsp;</td>
    </tr>
    
    <tr>
        <th>เนื้อที่</th>
        <td colspan="2">
        	<table><tr>
                
                <td><?php echo $form->textField($model,'area_rai',array('style'=>'width:50px;')); ?></td>
                <td><?php echo $form->labelEx($model,'ไร่'); ?></td>
                <td> <?php echo $form->error($model,'area_rai'); ?></td>
                
               
                <td><?php echo $form->textField($model,'area_ngan',array('style'=>'width:50px;')); ?></td>
                 <td><?php echo $form->labelEx($model,'งาน'); ?></td>
                <td> <?php echo $form->error($model,'area_ngan'); ?></td>
                
                
                <td><?php echo $form->textField($model,'area_wa',array('style'=>'width:50px;')); ?></td>
                <td><?php echo $form->labelEx($model,'ตร.ว.'); ?> ตร.ว.</td>
                <td> <?php echo $form->error($model,'area_wa'); ?></td>
                </tr>
                
                </table>

               </td>
               <td></td>
    </tr>
     <tr>
                <th><?php echo $form->labelEx($model,'พื้นที่'); ?></th>
                <td><?php echo $form->textField($model,'area_metre',array('style'=>'width:50px;')); ?> ตร.ม.<?php echo $form->error($model,'area_metre'); ?></td>
                <td> </td>
    </tr>
    
    <tr>
                <th><?php echo $form->labelEx($model,'บ้านเลขที่'); ?></th>
                <td><?php echo $form->textField($model,'home_no',array('style'=>'width:50px;')); ?><?php echo $form->error($model,'home_no'); ?> </td>
                <th style="text-align:left">หมู่ที่ <?php echo $form->textField($model,'moo',array('style'=>'width:50px;')); ?></th>
    </tr>
    
    <tr>
                <th><?php echo $form->labelEx($model,'หมู่บ้าน / โครงการ'); ?></th>
                <td><?php echo $form->textField($model,'project',array('style'=>'width:200px;')); ?><?php echo $form->error($model,'project'); ?> </td>
                <th style="text-align:left">ซอย <?php echo $form->textField($model,'soi',array('style'=>'width:200px;')); ?></th>
    </tr>
     <tr>
                <th><?php echo $form->labelEx($model,'ถนน'); ?></th>
                <td><?php echo $form->textField($model,'road'); ?> <?php echo $form->error($model,'road'); ?></td>
                <td> </td>
    </tr>
    
    <tr>
                <th><?php echo $form->labelEx($model,'ภาค'); ?></th>
                <td><span id='legion'></span></td>
                <td> </td>
    </tr>
    
     <tr>
                <th><?php echo $form->labelEx($model,'จังหวัด'); ?></th>
                <td><?php echo $form->dropDownList($model,'province', CHtml::listData(Provinces::model()->findAll(array('order' => 'thai_name ASC')), 'province_id', 'thai_name'), array('empty'=>'Select Province',																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/getdistrict/' ,
      			'success'=>'js:function(html){jQuery("#Property_district").html(html);jQuery("#Property_tambol").html("<option value=0>Select District First</option>");}',),
	  )); ?><?php echo $form->error($model,'province'); ?></td>
                <td> </td>
    </tr>
    <tr>
                <th><?php echo $form->labelEx($model,'อำเภอ'); ?></th>
                <td><?php echo $form->dropDownList($model,'district',$param['district'], array('empty'=>'Select Province First',
																																											   																																												  				'ajax' => array(
      			'type'=>'POST',
      			'url'=>Yii::app()->request->baseUrl . '/index.php/admin/gettambol/' ,
      			'success'=>'js:function(html){jQuery("#Property_tambol").html(html);}',),)); ?><?php echo $form->error($model,'district'); ?></td>
                <th> </th>
    </tr>
    <tr>
                <th><?php echo $form->labelEx($model,'ตำบล'); ?></th>
                <td><?php echo $form->dropDownList($model,'tambol',$param['tambol'], array('empty'=>'Select District First')); ?><?php echo $form->error($model,'tambol'); ?></td>
                <td>&nbsp;</td>
    </tr>
    
   
    <tr style="visibility:hidden;position:absolute;">
                <th><?php echo $form->labelEx($model,'รหัสไปรษณีย์'); ?></th>
                <td><?php echo $form->textField($model,'zipcode'); ?><?php echo $form->error($model,'zipcode'); ?></td>
                <td> </td>
    </tr>
    
       <tr>
                <th><?php echo $form->labelEx($model,'Latitude'); ?></th>
                <td><?php echo $form->textField($model,'latitude'); ?> <?php echo $form->error($model,'latitude'); ?></td>
                <td></td>
    </tr>
        <tr>
                <th><?php echo $form->labelEx($model,'longitude'); ?></th>
                <td><?php echo $form->textField($model,'longitude'); ?><?php echo $form->error($model,'longitude'); ?>
                <a href='javascript:;' onclick="open_map()" target="_blank">Google Map</a>
                </td>
                <td> </td>
    </tr>
    <tr>
    <td colspan="3">
    
    <div id="tabs">
	<ul>
		<li><a href="#tabs-1">TH</a></li>
		<li><a href="#tabs-2">EN</a></li>
	</ul>
	<div id="tabs-1">
		<p><?php echo $form->error($model,'deep'); ?>
        <table>
            <tr>
                    <td><?php echo $form->labelEx($model,'การเข้าถึง'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'enter_th',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'enter_th'); ?></td>
        </tr>
        <tr>
                    <td><?php echo $form->labelEx($model,'รายละเอียดทรัพย์สิน'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'product_desc_th',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'product_desc_th'); ?></td>
        </tr>
         <tr>
                    <td><?php echo $form->labelEx($model,'รายละเอียดทรัพย์สิน(เพิ่มเติม)'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'product_desc2_th',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'product_desc2_th'); ?></td>
        </tr>
        <tr>
                    <td><?php echo $form->labelEx($model,'ข้อจำกัดทรัพย์สิน'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'product_limit_th',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'product_limit_th'); ?></td>
        </tr>
        <tr>
                    <td><?php echo $form->labelEx($model,'หมายเหตุ'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'remark_th',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'remark_th'); ?></td>
        </tr>
         <tr>
                    <td><?php echo $form->labelEx($model,'อื่นๆ'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'other_th',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'other_th'); ?></td>
        </tr>
        <tr>
                    <td><?php echo $form->labelEx($model,'ข้อมูลเพิ่มเติม'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'more_detail_th',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'more_detail_th'); ?></td>
        </tr>
    </table>
        </p>
	</div>
	<div id="tabs-2">
		<p>
        <table>
            <tr>
                    <td><?php echo $form->labelEx($model,'การเข้าถึง(อังกฤษ)'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'enter_en',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'enter_en'); ?></td>
        </tr>
        <tr>
                    <td><?php echo $form->labelEx($model,'รายละเอียดทรัพย์สิน(อังกฤษ)'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'product_desc_en',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'product_desc_en'); ?></td>
        </tr>
         <tr>
                    <td><?php echo $form->labelEx($model,'รายละเอียดทรัพย์สิน(เพิ่มเติม)(อังกฤษ)'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'product_desc2_en',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'product_desc2_en'); ?></td>
        </tr>
        <tr>
                    <td><?php echo $form->labelEx($model,'ข้อจำกัดทรัพย์สิน(อังกฤษ)'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'product_limit_en',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'product_limit_en'); ?></td>
        </tr>
        <tr>
                    <td><?php echo $form->labelEx($model,'หมายเหตุ(อังกฤษ)'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'remark_en',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'remark_en'); ?></td>
        </tr>
         <tr>
                    <td><?php echo $form->labelEx($model,'อื่นๆ(อังกฤษ)'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'other_en',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'other_en'); ?></td>
        </tr>
        <tr>
                    <td><?php echo $form->labelEx($model,'ข้อมูลเพิ่มเติม(อังกฤษ)'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'more_detail_en',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'more_detail_en'); ?></td>
        </tr>
    </table>
        </p>
	</div>
	
</div>
    
    </td>
    </tr>
    
    <tr>
                <th><?php echo $form->labelEx($model,'หน้ากว้างติดถนน (เมตร)'); ?></th>
                <td><?php echo $form->textField($model,'street_side'); ?> <?php echo $form->error($model,'street_side'); ?></td>
                <td></td>
    </tr>
    <tr>
                <th><?php echo $form->labelEx($model,'ยาว (ลึก) สุด (เมตร)'); ?></th>
                <td><?php echo $form->textField($model,'deep'); ?></td>
                <td> </td>
    </tr>
   


    
   
    
    <tr>
                <th><?php echo $form->labelEx($model,'ผังเมือง (สี)'); ?></th>
                <td><?php echo $form->dropDownList($model,'town_plan', CHtml::listData(ColorZone::model()->findAll(array('order' => 'color ASC')), 'id', 'color'), array('empty'=>'Select Color',																																												  				 
	  )); ?><?php echo $form->error($model,'rawang'); ?>

      </td>
                <td> </td>
    </tr>

    
 
<tr>
                <th><?php echo $form->labelEx($model,'ราคาประกาศขายขั้นต่ำ'); ?></th>
                <td><?php echo $form->textField($model,'price'); ?><?php echo $form->error($model,'price'); ?></td>
                <td> </td>
    </tr>
        
<tr>
                <th><?php echo $form->labelEx($model,'การแสดงผล'); ?></th>
                <td><?php
				echo $form->radioButtonList($model, 'is_show', array('1'=>"แสดง",'0'=>"ซ่อน"),array(
                        'separator'=>'&nbsp;',
                        'labelOptions'=>array('style'=>'display: inline; margin-right: 10px; font-weight: normal;'),
                ));
                ?></td>
                <td> </td>
    </tr>
    
    <tr>
                <th><?php echo $form->labelEx($model,'วิธีขาย'); ?></th>
                <td><?php
				echo $form->radioButtonList($model, 'sale_type', array('1'=>"ประมูล",'0'=>"ซื้อตรง"),array(
                        'separator'=>'&nbsp;',
                        'labelOptions'=>array('style'=>'display: inline; margin-right: 10px; font-weight: normal;'),
                ));
                ?></td>
                <td> </td>
    </tr>
    

   
    <tr>
                <th><?php echo $form->labelEx($model,'วันที่เริ่มประกาศขาย'); ?></th>
                <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array('model'=>$model,
                    'name'=>'Property[publish_date]',
					'value'=>$model->publish_date,
                    'options'=>array(
        				'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",

    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
                   )
             ); ?><?php echo $form->error($model,'publish_date'); ?>*yyyy-mm-dd</td>
                <td> </td>
    </tr>
    
    <tr>
                <th><?php echo $form->labelEx($model,'วันที่สิ้นสุดการลงทะเบียน'); ?></th>
                <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
                    'name'=>'Property[end_date]',
					'value'=>$model->end_date,
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),)); ?><?php echo $form->error($model,'bid_date'); ?>*yyyy-mm-dd</td>
                <td> </td>
    </tr>
     <tr>
                <th><?php echo $form->labelEx($model,'วันที่ยื่นซองเสนอราคา'); ?></th>
                <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
                    'name'=>'Property[bid_date]',
					'value'=>$model->bid_date,
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),)); ?><?php echo $form->error($model,'bid_date'); ?>*yyyy-mm-dd</td>
                <td> </td>
    </tr>
    <tr>
                <th align="right"><?php echo $form->labelEx($model,'วันที่แสดงผล'); ?> :</th>
                <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',
              array(
					'model'=>$model, 
                    'name'=>'Property[sync_date]',
					'value'=>$model->sync_date,
                    'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd', 
						'altFormat'=>'yy-mm-dd',
						'changeMonth'=>'true', 
						'changeYear'=>'true',
						
						'showOn'=>"both",

			'buttonImage'=>Yii::app()->request->baseUrl . "/images/icon/rdDatePicker.gif",

			'buttonImageOnly'=>"true",
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),)); ?><?php echo $form->error($model,'sync_date'); ?>*yyyy-mm-dd</td>
                <td> </td>
    </tr>
    
     <tr>
                    <td><?php echo $form->labelEx($model,'สถานที่ยื่นซองประมูล'); ?><br />*ไม่เกิน 4000 ตัวอักษร</td>
                    <td><?php echo $form->textArea($model,'bid_place',array('rows'=>5,'cols'=>40,'style'=>'width:380px;height:150px')); ?></td>
                    <td> <?php echo $form->error($model,'bid_place'); ?></td>
        </tr>
    

    <tr valign="top">
                <th>ภาพประกอบ</th>
                <td colspan='2'>
                <fieldset>               
                <table id='tb_image'>
                <tr valign="top">
                <td align="right">รูปภาพหลัก :</td><td>
                <?php
				if(trim($model->main_image)!=""){
					echo CHtml::image(Yii::app()->request->baseUrl . "/images/npa/" . $model->id ."/"  . $model->main_image , "",array("width"=>"100px",'class'=>'img_main_image')) . "<br /><br /><a href='javascript:deletepic(\"main_image\",\"img_main_image img_main_image\")' class='link_search'>ลบออก</a><br /><br />";
				}
				?>
                <input type="file" name="image_main" id='image_main' />* ภาพควรเป็น 600x400 px
                
                </td>
                </tr>
                <tr>
                <td align="right">แผนที่ :</td><td>
                <?php
				if(trim($model->map)!=""){
					echo CHtml::image(Yii::app()->request->baseUrl . "/images/npa/" . $model->id ."/"  . $model->map , "",array("width"=>"100px",'class'=>'img_map')) . "<br /><br /><a href='javascript:deletepic(\"map\",\"img_map\")' class='link_search img_map'>ลบออก</a><br /><br />";
				}
				?>
                <input type="file" name="image_map" id='image_map' />* ภาพควรเป็น 600x400 px</td>
                </tr>
                <tr>
                <td align="right">ผังที่ดิน :</td><td>
                <?php
				if(trim($model->land_map)!=""){
					echo CHtml::image(Yii::app()->request->baseUrl . "/images/npa/" . $model->id ."/"  . $model->land_map , "",array("width"=>"100px",'class'=>'img_land_map')) . "<br /><br /><a href='javascript:deletepic(\"land_map\",\"img_land_map\")' class='link_search img_land_map'>ลบออก</a><br /><br />";
				}
				?>
                <input type="file" name="image_plan" id='image_plan' />* ภาพควรเป็น 600x400 px</td>
                </tr>
                <tr>
                <td align="right">ผังสิ่งปลูกสร้าง :</td><td>
                <?php
				if(trim($model->building_map)!=""){
					echo CHtml::image(Yii::app()->request->baseUrl . "/images/npa/" . $model->id ."/"  . $model->building_map , "",array("width"=>"100px",'class'=>'img_building_map')) . "<br /><br /><a href='javascript:deletepic(\"building_map\",\"img_building_map\")' class='link_search img_building_map'>ลบออก</a><br /><br />";
				}
				?>
                <input type="file" name="image_build" id='image_build' />* ภาพควรเป็น 600x400 px</td>
                </tr>
                 
                 
                
                </table>
                <input type="button" value="เพิ่ม" onclick="more_image()" /><br />
                <table width="100%"><tr>
                <?php
				if(isset($model->npa_image))
				{
					if(count($model->npa_image)>0){
						$npa_image = $model->npa_image;
						$i= 0;
						foreach($npa_image as $img)
						{
							$image_url = Yii::app()->request->baseUrl . "/images/npa/" . $img->npa_id . "/" . $img->img;
							?> <td align="center">
                            <div id='div_img_<?php echo $img->id;?>'>
                            รูปภาพประกอบ <?php echo ++$i;?><br />
                            <a href='<?php echo $image_url;?>' target="_blank">
                            <img src='<?php echo $image_url;?>' height="90px" width='90px'/></a><br /><a href='javascript:deletefile("image","div_img_<?php echo $img->id;?>","<?php echo $img->id;?>")'>ลบ</a>
                           </div>
                           </td>
<?php
	if($i%3==0)
	{
		echo "</tr><tr>";
	}
						}
					}
				}
				?>
                </tr></table>
                </fieldset>
                </td>
                <td>&nbsp;</td>
    </tr>
    
    <tr valign="top">
                <th>เอกสารประกอบ</th>
                <td colspan='2'>
                <fieldset>               
                <table id='tb_file'>
                <tr><td></td></tr>
                </table>
                <input type="button" value="เพิ่ม" onclick="more_file()" />
                <br /><?php
                if(isset($model->attach) && count($model->attach)>0){
					foreach($model->attach as $row)
					{
						$file_url = Yii::app()->request->baseUrl . "/attach/npa/" . $row->npa_id . "/" . $row->file_src;
						echo '<div id="div_attach_' . $row->id . '"><b><a href="' . $file_url . '" target="_blank">' . $row->file_name  . "</a> </b><a href='javascript:deletefile(\"attach\",\"div_attach_" . $row->id . "\",\"" . $row->id . "\")'>ลบ</a></div>";
						echo "<br />";
					}
				}
					?>
                </fieldset>
                </td>
                <td>&nbsp;</td>
    </tr>
     <tr valign="top">
                <th>ลิงก์วีดีโอที่เกี่ยวข้อง</th>
                <td colspan='2'>
                <fieldset>               
                <table id='tb_vdo'>
                <tr><td></td></tr>
                </table>
                <input type="button" value="เพิ่ม" onclick="more_vdo()" />
                <br />
                <input type="file" name="vdo" />
<?php
                if(isset($model->vdo) && count($model->vdo)>0){
					foreach($model->vdo as $row)
					{
						$file_url = Yii::app()->request->baseUrl . "/vdo/npa/" . $row->npa_id . "/" . $row->cover;
						$vdo_url = str_replace("http://http://","http://","http://" . $row->URL );
						
						echo '<div id="div_vdo_' . $row->id . '"><b><a href="' . $vdo_url. '" target="_blank"><img src="' . $file_url  . "\" width='50px' height='50px'></a> </b><a href='javascript:deletefile(\"vdo\",\"div_vdo_" . $row->id . "\",\"" . $row->id . "\")'>ลบ</a></div>";
						echo "<br />";
					}
				}
					?>
                </fieldset>
                </td>
                <td>&nbsp;</td>
    </tr>
    <tr>
    <td></td>
    <td>
    <div class="row buttons">
		<a href="javascript:;" onclick="submitme('save')">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_save.png" width="52" height="25" /></a>
                              <a href="javascript:;" onclick="submitme('saveback')">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_saveback.png" height="25" /></a>
                              <a href="javascript:;" onclick="submitme('savepreview')">
                              <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_saveopen.png"  height="25" /></a>
	</div>
    </td>
    <td></td>
    </tr>
    </table>

            </div>
    	
	
<?php $this->endWidget(); ?>
</div><!-- form -->


</td>
</tr>
</table>
</div>