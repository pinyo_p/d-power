<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'ตั้งค่าระบบซื้อขาย';
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<table width="100%">
    <tr>
        <td align="center"><h3 class="underline">ตั้งค่าระบบซื้อขาย</h3></td>
    </tr>
    <tr>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="add_data">
            <table width="70%" border="0" cellspacing="0" cellpadding="0">
                <tr style="display:none;">
                    <th width="30%" align="right">อัตราค่าจัดส่งสินค้า :</th>
                    <td width="70%"><input type="text" id="txtShippingFee" name="txtShippingFee" value="<?php echo $param["shipping_fee"]; ?>"  /></td>
                </tr>
                <tr>
                    <th align="right">อัตราแลกเปลี่ยนเงินดอลลาร์ :</th>
                    <td><input type="text" id="txtDollarRate" name="txtDollarRate" value="<?php echo $param["dollar_rate"]; ?>"  /></td>
                </tr>
                <tr>
                    <th align="right">จำนวนยอดซื้อต่อ 1 แต้ม</th>
                    <td>
                        <input type="text" id="txtScoreRate" name="txtScoreRate" value="<?php echo $param["score_rate"]; ?>"  /> บาท
                    </td>
                </tr>
                <tr>
                    <th align="right">น้ำหนักกล่องสำหรับบรรจุจัดส่ง</th>
                    <td>
                        <input type="text" id="txtBoxWeight" name="txtBoxWeight" value="<?php echo $param["box_weight"]; ?>"  /> กรัม
                    </td>
                </tr>
                <tr>
                    <th align="right">จำนวนน้ำหนักเกิน</th>
                    <td>
                        <input type="text" id="txtWeightOver" name="txtWeightOver" value="<?php echo $param["weight_over"]; ?>"  /> กรัม, ให้เลือกส่งพัสดุแบบ EMS หรือแบบพัสดุธรรมดา
                    </td>
                </tr>
            </table>
            <br />
            <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />
            <br />
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>