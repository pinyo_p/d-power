<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'อัลบั้ม Video';
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
$id = (isset($_GET['id']) ? $_GET['id'] : "0");
?>
<script type="text/javascript" language="javascript">
    $(function () {
        $("#tabs").tabs();
    });
</script>

<table width="100%">
    <tr>
        <td align="center"><h3 class="underline"><?php echo (isset($_GET["id"])) ? "แก้ไข" : "เพิ่ม"; ?>ข้อมูล</h3></td>
    </tr>
    <tr>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="add_data">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th width="40%" align="right" style="vertical-align: top;">เนื้อหา :</th>
                    <td width="75%">
                        <div style="width:800px;  text-align:left">

                            <div id="tabs" >
                                <ul>
                                    <li><a href="#tabs-1">ภาษาไทย</a></li>
                                    <li><a href="#tabs-2">ภาษาอังกฤษ</a></li>
                                </ul>
                                <div id="tabs-1">
                                    ชื่ออัลบั้ม (ภาษาไทย)<br />
                                    <?php echo $form->textField($model, 'name_2', array('style' => 'width:100%;')); ?>
                                    <?php echo $form->error($model,'name_2'); ?>
                                    <br /><br />
                                    คำอธิบาย (ภาษาไทย)<br />
                                    <?php echo $form->textArea($model, 'description_2', array('style' => 'width:98%;height:100px;')); ?>
                                </div>
                                <div id="tabs-2">
                                    ชื่ออัลบั้ม (ภาษาอังกฤษ)
                                    <br />
                                    <?php echo $form->textField($model, 'name_1', array('style' => 'width:100%;')); ?>
                                    <?php echo $form->error($model,'name_1'); ?>
                                    <br /><br />
                                    คำอธิบาย (ภาษาอังกฤษ)<br />
                                    <?php echo $form->textArea($model, 'description_1', array('style' => 'width:98%;height:100px;')); ?>
                                </div>
                                <div style="padding-left:15px">
                                </div>
                            </div>
                            <br /><br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">Title Keyword :</th>
                    <td width="75%"> 
                        <?php echo $form->textField($model, 'title_keyword', array('style' => 'width:600px')); ?>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">Meta Keyword :</th>
                    <td width="75%"> 
                        <?php echo $form->textField($model, 'meta_keyword', array('style' => 'width:600px')); ?>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">Meta Description :</th>
                    <td width="75%"> 
                        <?php echo $form->textField($model, 'meta_desc', array('style' => 'width:600px')); ?>
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">แสดงผลหน้าแรก :</th>
                    <td width="75%"> 
                       <?php echo $form->checkBox($model,'is_index', array('value'=>1, 'uncheckValue'=>0)); ?>                     
                    </td>
                </tr>
                <tr>
                    <th width="40%" align="right">สถานะ :</th>
                    <td width="75%"> 
                       <?php echo $form->dropDownList($model, 'status', array('1' => 'ใช้งาน', '0' => 'ไม่ใช้งาน'),array('style' => 'width:100px;')); ?>
                    </td>
                </tr>

            </table>
            <br />
            <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;
            <a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/AlbumVideoList'; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>