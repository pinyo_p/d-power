<?php 
$this->pageTitle="Admin Panel::Marquee"; 
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
	function deleteit(objId)
	{
		if(confirm("ต้องการลบรายการนี้ใช่หรือไม่")){
		$.post('<?php echo Yii::app()->createUrl('/admin/DeleteMarquee/');?>',{id:objId},function(data){
																								   window.location.reload();
																								   });
		}
	}
	function displayit(objId)
	{

		$.post('<?php echo Yii::app()->createUrl('/admin/DisplayMarquee/');?>',{id:objId},function(data){
																								   window.location.reload();
																								   });
	}
	function hideit(objId)
	{

		$.post('<?php echo Yii::app()->createUrl('/admin/HideMarquee/');?>',{id:objId},function(data){
																								   window.location.reload();
																								   });
	}
	function deleteitAll()
{
	
	if(confirm("ต้องการลบรายการที่เลือกใช่หรือไม่")){
		$("#act").val('deleteAll');
		$("#frm_marquee").submit();
	}
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div class="navi_bar">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="50%"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/home.png" width="16" height="14" /> <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/Admin" class="link_green">หน้าแรก</a> &gt; <a href="#" class="link_green">ตั้งค่าเว็บไซต์</a> &gt; <a href="#" class="link_green">ข้อความวิ่ง</a></td>
                    <td width="50%" align="right"><a href="#" class="link_green">ช่วยเหลือ</a> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/qust.png" width="31" height="32" /></td>
                  </tr>
                </table>
              </div></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_01.png" width="5" height="35" /></td>
                      <td class="topix_header"><div class="topix_headtxt">ตั้งค่าข้อความวิ่ง</div></td>
                      <td width="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/images/topix_03.png" width="5" height="35" /></td>
                    </tr>
                  </table></td>
            </tr>
            <tr>
              <td><br />
              <?php 
					
					$form=$this->beginWidget('CActiveForm', array(
	'id'=>'frm_marquee',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'target'=>'_blank',
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<input name="act" type="hidden" id='act'/>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center">
                      <br />
                      <table width="100%" border="0" cellspacing="0" cellpadding="3">
                        <tr style="background-color:#78CF2D;color:white;">
                              <td width="4%" align="center" valign="top" class="">เลือก</td>
                              <td width="4%" align="center" valign="top" class="txt_bold">ลำดับ</td>
                              <td width="35%" align="center" valign="top" class="txt_bold">ข้อความ</td>
                              <td width="10%" align="center" valign="top" class="txt_bold">แสดงผล</td>
                              <td width="6%" align="center" valign="top" class="txt_bold">แก้ไข</td>
                              <td width="6%" align="center" valign="top" class="txt_bold">ลบ</td>
                            </tr>
                            <?php
						$i=0;
						foreach($data as $row){
							$i++;
						?>
                        <tr>
                              <td class="rowa" width="4%" align="center" valign="top"><input type="checkbox" name="marquee_id[]" id="checkbox" value="<?php echo $row->id;?>" /></td>
                              <td width="4%" align="center" valign="top" class="rowa">
                              <?php echo $row->sort_order;?>
                              </td>
                              <td class="rowa"  width="35%" align="left" valign="top">
                              <?php echo strip_tags($row->text_th);?> / <?php echo strip_tags($row->text_en);?>
                              </td>
                              <td class="rowa" width="10%" align="center" valign="top">
                              <?php
							  if($row->display==0){
								  ?>
                              <a href='javascript:displayit("<?php echo $row->id;?>")' <?php Yii::hideit();?> class="lmm_18_4">ไม่แสดงผล</a>
                             	<?php
							  }else{
								?>
                                 <a href='javascript:hideit("<?php echo $row->id;?>")'  <?php Yii::hideit();?> class="lmm_18_4">แสดงผล</a>
                                <?php } ?>
                              </td>
                              <td class="rowa" width="6%" align="center" valign="top">
                               <a href="<?php echo Yii::app()->createUrl('/Admin/UpdateMarquee/' . $row->id);?> " <?php Yii::hideit();?> class="lmm_18_2">
                              แก้ไข
                              </a>
                              </td>
                              <td class="rowa" width="6%" align="center" valign="top">
                              <a href="javascript:deleteit('<?php echo $row->id;?>')"  <?php Yii::hideit();?> class="lmm_18_3">
                              ลบ
                              </a></td>
                            </tr>
                            <?php
						}
						?>
                            <tr>
                              <td class="rowb" align="center" valign="top">&nbsp;</td>
                              <td width="4%" align="center" valign="top" class="rowb">&nbsp;</td>
                              <td  width="15%" align="left" valign="top" class="rowb">&nbsp;</td>
                              <td width="10%" align="center" valign="top" class="rowb">&nbsp;</td>
                              <td width="6%" align="center" valign="top" class="rowb">&nbsp;</td>
                              <td width="6%" align="center" valign="top" class="rowb">&nbsp;</td>
                           
                        </tr>
                        <tr>
                          <td class="nparesult_table_content" align="center" colspan="7"><br />
                            <a href="<?php echo Yii::app()->createUrl('/Admin/newmarquee');?>"  <?php Yii::hideit();?> class="lmm_18_1"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_add.png" width="52" height="25"  /></a> &nbsp;
                            <a href="javascript:deleteitAll()"  <?php Yii::hideit();?> class="lmm_18_3"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/backend/b_del.png" width="52" height="25" /></a>
                            </td>
                        </tr>
                      </table>
                 <?php $this->endWidget(); ?>
                    &nbsp;<br />
                    <br /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
        