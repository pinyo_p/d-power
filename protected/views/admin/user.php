<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'User';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
  <script type="text/javascript" language="javascript">
	$(function() {
		$( "#tabs" ).tabs();
		CKEDITOR.replace('Content_content_th', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
		CKEDITOR.replace('Content_content_en', {
			filebrowserBrowseUrl: '<?php echo Yii::app()->request->baseUrl; ?>/Filemanager/index.html',
		});
	});
	
	</script>
    
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล</h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <th width="40%" align="right">Username :</th>
              <td width="75%"><?php echo $form->textField($model,'username'); ?></td>
            </tr>
             <tr>
              <th width="40%" align="right">Password :</th>
              <td width="75%"><?php echo $form->passwordField($model,'password'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">กลุ่มผู้ใช้ :</th>
              <td width="75%">
                <?php echo $form->dropDownList($model,'group_id', CHtml::listData(UserGroup::model()->findAll(array('order' => 'group_name ASC')), 'id', 'group_name'), array('empty'=>'เลือก กลุ่มผู้ใช้',																																												  				 
	  )); ?>
              </td>
            </tr>
            <tr>
              <th width="40%" align="right">ชื่อ :</th>
              <td width="75%"><?php echo $form->textField($model,'first_name'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">นามสกุล :</th>
              <td width="75%"><?php echo $form->textField($model,'last_name'); ?></td>
            </tr>
             
            
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/UserList';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>