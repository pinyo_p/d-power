<?php
$this->pageTitle=Yii::app()->name ;
$this->breadcrumbs = 'หมวดหมู่สินค้า';
?>
  <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); 


  ?>
<table width="100%">
<tr>
<td align="center"><h3 class="underline">เพิ่มข้อมูล <span class="errorMessage">* รายการที่เพิ่มจะไม่แสดงจนกว่าจะมีรายการสินค้าในกลุ่มนี้</span></h3></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="add_data">
          <table width="50%" border="0" cellspacing="0" cellpadding="0">
          <tr  class='hide_it'>
              <th width="40%" align="right">Brand :</th>
              <td width="75%">
                <?php echo $form->dropDownList($model,'brand_id', CHtml::listData(Brand::model()->findAll(array('order' => 'name_th ASC')), 'id', 'name_th'), array('empty'=>'เลือก Brand',																																												  				 
	  )); ?><?php echo $form->error($model,'brand_id'); ?>
              </td>
            </tr>
            <tr>
                <th width="40%" align="right">ภายใต้หมวดสินค้า :</td>
                <td width="75%">
                    <?php echo $form->dropDownList($model,'parent_id', 
                            CHtml::listData(ProductGroup::model()->findAll('parent_id=0',array('order' => 'sort_order ASC')), 'id', 'group_2'), array('empty'=>'-- ไม่ระบุ --',																																												  				 
	  )); ?>
                </td>
            </tr>
            <tr>
              <th width="40%" align="right">ชื่อหมวดหมู่ (ภาษาไทย) :</th>
              <td width="75%"><?php echo $form->textField($model,'group_2'); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">ชื่อหมวดหมู่ (ภาษาอังกฤษ) :</th>
              <td width="75%"><?php echo $form->textField($model,'group_1'); ?></td>
            </tr>
             <tr>
              <th width="40%" align="right">คำอธิบาย(ภาษาไทย) :</th>
              <td width="75%"><?php echo $form->textArea($model,'group_desc_2',array('rows'=>10,'cols'=>40)); ?></td>
            </tr>
            <tr>
              <th width="40%" align="right">คำอธิบาย (ภาษาอังกฤษ) :</th>
              <td width="75%"><?php echo $form->textArea($model,'group_desc_1',array('rows'=>10,'cols'=>40)); ?></td>
            </tr>
           
            <tr>
              <th width="40%" align="right">Logo :</th>
              <td width="75%">
              <?php
			  $file = Yii::app()->request->baseUrl . '/images/group/' . $model->logo;
			  if(file_exists(Yii::app()->basePath . '/../images/group/' .  $model->logo) && $model->logo != ""){
				  ?>
                   <img src="<?php echo $file; ?>" height="83" />
                  <?php
			  }
			  ?>
              <input type="file" name="logo" id='logo' /></td>
            </tr>
            <tr>
              <th width="40%" align="right">จัดเรียง :</th>
              <td width="75%"><?php echo $form->textField($model,'sort_order'); ?></td>
            </tr>
          </table>
          <br />
          <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png" width="55" height="26" />&nbsp;<a href="<?php echo Yii::app()->request->baseUrl . '/index.php/admin/ProductGroupList';?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_reset.png" width="55" height="26" /></a><br />
        </td>
        </tr>
        </table>
        <?php $this->endWidget(); ?>