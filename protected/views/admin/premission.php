<?php
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = 'Premission';
?>




<script type="text/javascript">

    $(function () {
<?php
foreach ($premission as $row) {
    echo '$("#mm_' . $row->menu_id . '").attr("checked","checked");' . "\n";
}
?>
    });
    function submitme()
    {

        $("#premission").submit();

    }
</script>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'premission',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

    <tr>
        <td class="tabletest"> 
            <br /><br />
            <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1">
                <tr>
                    <th width="304" align="center" valign="top" class="">เมนู</th>
                    <th align="center" valign="top" width='79'>เข้าถึง</th>
                    <th align="center" valign="top" width='96'>เพิ่ม</th>
                    <th align="center" valign="top" width='78'>แก้ไข</th>
                    <th align="center" valign="top" width='77'>ลบ</th>
                    <th align="center" valign="top" width='145'>อื่นๆ 1</th>
                    <th align="center" valign="top"  width='120'>อื่นๆ 2</th>
                    <th align="center" valign="top"  width='120'>อื่นๆ 3</th>
                </tr>
                <?php
                foreach ($menu as $row) {
                    ?>
                    <tr style="text-align:left">
                        <td valign="top" class=""><?php echo $row->menu_name; ?></td>
                        <td>
                            <?php
                            echo '<input type="checkbox" name="mm[]" value="' . $row->id . '" id="mm_' . $row->id . '"> <label for="mm_' . $row->id . '">เข้าถึง</label>';
                            ?>
                        </td>
                        <td valign="top" class="">
                            <?php
                            if ($row->is_new == 1)
                                echo '<input type="checkbox" name="mm[]" value="' . $row->id . '_1" id="mm_' . $row->id . '_1"> <label for="mm_' . $row->id . '_1">เพิ่ม</label>';
                            ?>
                        </td>
                        <td  valign="top" class="">
                            <?php
                            if ($row->is_edit == 1)
                                echo '<input type="checkbox" name="mm[]" value="' . $row->id . '_2" id="mm_' . $row->id . '_2"> <label for="mm_' . $row->id . '_2">แก้ไข</label>';
                            ?></td>
                        <td  valign="top" class=""><?php
                            if ($row->is_delete == 1)
                                echo '<input type="checkbox" name="mm[]" value="' . $row->id . '_3" id="mm_' . $row->id . '_3"> <label for="mm_' . $row->id . '_3">ลบ</label>';
                            ?></td>
                        <td  valign="top" class=""><?php
                            if ($row->other1 == 1)
                                echo '<input type="checkbox" name="mm[]" value="' . $row->id . '_4" id="mm_' . $row->id . '_4"> <label for="mm_' . $row->id . '_4">' . $row->other_specify1 . '</label>';
                            ?></td>
                        <td  valign="top" class=""><?php
                            if ($row->other2 == 1)
                                echo '<input type="checkbox" name="mm[]" value="' . $row->id . '_5" id="mm_' . $row->id . '_5"> <label for="mm_' . $row->id . '_5">' . $row->other_specify2 . '</label>';
                            ?></td>
                        <td valign="top" class=""><?php
                            if ($row->other3 == 1)
                                echo '<input type="checkbox" name="mm[]" value="' . $row->id . '_6" id="mm_' . $row->id . '_6"> <label for="mm_' . $row->id . '_6">' . $row->other_specify3 . '</label>';
                            ?></td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td colspan="8" align="center">
                        <a href="javascript:;" onclick="submitme()">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/admin/images/button_save.png"/></a>
                    </td>
            </table>
        </td>
    </tr>
</table>
<?php
//<input type="checkbox" name="mm[]" value="7" id="mm_7">
$this->endWidget();
?>
 