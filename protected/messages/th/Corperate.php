<?php
return array(
			'company_name'=>'บริษัท บริหารสินทรัพย์สุขุมวิท จำกัด',
			'member_login'=>'สมาชิกเข้าสู่ระบบ',
			'language'=>'ภาษา',
			'home'=>'หน้าแรก',
			'about_us'=>'เกี่ยวกับเรา',
			'news'=>'ข่าวสาร',
			'announcement'=>'ประกาศ',
			'public_relation'=>'ประชาสัมพันธ์',
			'contact_us'=>'ติดต่อเรา',
			'ebook'=>'วารสาร',
			'image_gallery'=>'แกลอรี่รูปภาพ',
			'vdo'=>'วีดีโอ',
			'link_exchange'=>'แลกลิ้งค์',
			'job_apply'=>'ร่วมงานกับเรา',
			'product_category'=>'หมวดหมู่สินค้าและบริการ',
			'product'=>'สินค้า',
			'product_detail'=>'รายละเอียดสินค้า',
);
?>