<?php
return array(
	      	'company_name'=>'Sukhumvit Asset Management',
		  	'member_login'=>'Member Login',
			'language'=>'Language',
			'home'=>'Home',
			'about_us'=>'About Us',
			'news'=>'News',
			'announcement'=>'ประกาศ',
			'public_relation'=>'ประชาสัมพันธ์',
			'contact_us'=>'Contact Us',
			'ebook'=>'E-Book',
			'image_gallery'=>'Image Gallery',
			'vdo'=>'Video',
			'link_exchange'=>'Link Exchange',
			'job_apply'=>'Our Jobs',
			'product_category'=>'Product Category',
			'product'=>'Product',
			'product_detail'=>'Product Detail',
);
?>