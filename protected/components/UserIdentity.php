<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        /*
          $users=array(
          // username => password
          'demo'=>'demo',
          'admin'=>'admin',
          );
         */

        $username = $this->username;
        $password = $this->password;
        $connection = Yii::app()->db;
        $sql = "select * from tb_user where username = :username and password=:password";
        $command = $connection->createCommand($sql);
        $command->bindParam(":username", $username, PDO::PARAM_STR);
        $command->bindParam(":password", $password, PDO::PARAM_STR);
        $row = $command->queryAll();

        if (count($row) <= 0) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if ($row[0]['status'] != "0") {
            $this->errorCode = 99;
        } else {
            //$premission = Premission::model()->findAll('user_id=:user_id', array(':user_id'=>$row[0]['group_id']));
            Yii::app()->user->setValue("first_name", $row[0]['first_name']);
            Yii::app()->user->setValue("last_name", $row[0]['last_name']);
            Yii::app()->user->setValue("group_id", $row[0]['group_id']);
            Yii::app()->user->setValue("last_login", $row[0]['last_login']);
            //Yii::app()->user->setValue("premission",$premission);
            //$this->premission = $premission;
            $this->errorCode = self::ERROR_NONE;
            
            Yii::app()->session->add('adm_login','1');
        }
        return !$this->errorCode;
    }

}
