<?php

/**
 * Yii bootstrap file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008-2011 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 * @version $Id: yii.php 2799 2011-01-01 19:31:13Z qiang.xue $
 * @package system
 * @since 1.0
 */
require(dirname(__FILE__) . '/YiiBase.php');

/**
 * Yii is a helper class serving common framework functionalities.
 *
 * It encapsulates {@link YiiBase} which provides the actual implementation.
 * By writing your own Yii class, you can customize some functionalities of YiiBase.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @version $Id: yii.php 2799 2011-01-01 19:31:13Z qiang.xue $
 * @package system
 * @since 1.0
 */
class Yii extends YiiBase {

    function h($text) {
        return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
    }

    function y($key) {
        return Yii::t('Corperate', $key);
    }

    function d($text_en, $text_th) {
        if (strtolower(Yii::app()->language) == "en" && trim(strip_tags($text_en)) != "")
            return $text_en;
        else
            return $text_th;
    }

    function text_lang($arr_field) {
        $txt = "";
        switch (strtolower(Yii::app()->language)) {
            case "th":
            case "th_th":
                $txt = $arr_field[1];
                break;
            case "en":
            case "en_us":
                $txt = $arr_field[0];
                break;
            // more lang here ....
        }
        return $txt;
    }

    function short_text($txt, $limit) {
        $str = $txt;
        $str = trim(strip_tags($txt));
        if (strlen($str) >= $limit)
            $str = iconv_substr($str, 0, $limit, "UTF-8") . "  ...";
        return $str;
    }

    /**
     * This is the shortcut to CHtml::link()
     */
    function l($text, $url = '#', $htmlOptions = array()) {
        return CHtml::link($text, $url, $htmlOptions);
    }

    public function hideit() {
        echo " style='visibility:hidden;position:absolute;' ";
    }

    function allow_send_email() {
        return true;
    }
    
    public function prefixMailSubject() {
        return "";
    }
    
    public function getDomain(){
        return "http://www.biffoodservice.com";
    }

    function currencySign() {
        if(Yii::app()->session['currency'] == "baht")
            return "฿";
        else
            return "$";
    }
    function currencyAmount($bahtAmount) {
        $amount = $bahtAmount;
        if(Yii::app()->session['currency'] != "baht")
            $amount = number_format(($bahtAmount / Yii::app()->session['dollar_ext_rate']), 2, ".", ",");      
        return $amount;
    }

}
